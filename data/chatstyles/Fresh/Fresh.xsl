<?xml version='1.0'?>
<!--
  'Fresh' style for KMess.
  Copyright (C) 2006, Diederik van der Boor
  Copyright (c) 2006, Benjamin Deveze

  Design based on 'Fresh' by Benjamin Deveze
  http://www.kde-look.org/content/show.php?content=43099

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
-->
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:kmess="http://www.kmess.org/xmlns/ChatStyles/v1/" >
  <xsl:output method="html"
              encoding="utf-8"
              indent="no"
              omit-xml-declaration="yes"
              doctype-public="-//W3C//DTD HTML 4.01 Transitional//EN"
              doctype-system="http://www.w3.org/TR/html4/loose.dtd" />

  <!--
    Declare external parameters with default values
  -->
  <xsl:param name="basepath" select="'.'" />
  <xsl:param name="csspath"  select="'Fresh.css'" />


  <!--
    Template for the page body
  -->
  <xsl:template match="messageRoot">
    <html id="ChatMessageView">
      <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <base href="{$basepath}" />
        <style type="text/css">
          /* standard colors for compatibility with dark color schemes */
          body      { font-size: 10pt; margin: 0; padding: 5px; background-color: #fff; color: #000; }
          a:link    { color: blue; }
          a:visited { color: purple; }
          a:hover   { color: red; }
          a:active  { color: red; }
        </style>
        <style type="text/css">
          /* import additional style */
          @import "<xsl:value-of select="$csspath" />";
        </style>
      </head>
      <body>
        <div id="messageRoot">
          <!-- chat messages are appended here by KMess -->
          <xsl:apply-templates />
        </div>
      </body>
    </html>
  </xsl:template>

  <!--
    Template for normal messages
  -->
  <xsl:template match="message">
    <xsl:choose>

      <!--
        normal chat message,
        - incoming message from a contact.
        - outgoing message from the user.
        - incoming offline-im message from a contact.
      -->
      <xsl:when test="@type='incoming' or @type='outgoing' or @type='offlineIncoming'">
        <div class="{body/@dir}">
          <div class="{@type}">

            <!-- user avatar -->
            <div class="avatar">
              <div class="avatarSpace">
                <xsl:choose>
                  <xsl:when test="from/contact/displayPicture/@url">
                    <img src="{from/contact/displayPicture/@url}" height="48" alt="" />
                  </xsl:when>
                  <xsl:otherwise>
                    <img src="buddy_icon.png" height="48" width="48" alt="" />
                  </xsl:otherwise>
                </xsl:choose>
              </div>
            </div>

            <dl>
              <!-- message header -->
              <dt dir="{from/contact/displayName/@dir}">
                <!-- show name -->
                <span class="name">
                  <xsl:value-of disable-output-escaping="yes" select="from/contact/displayName/@text"/>
                  <xsl:text>:</xsl:text>
                </span>
              </dt>

              <!-- message body -->
              <dd dir="{body/@dir}">
                <!-- show time? -->
                <xsl:if test="@time">
                  <span class="time">
                    <xsl:if test="@type='offlineIncoming'">
                      <xsl:value-of select="@date"/> &#160;
                    </xsl:if>
                    <xsl:value-of select="@time"/>
                  </span>
                </xsl:if>

                <div class="messageContent">
                  <xsl:value-of disable-output-escaping="yes" select="body/@fontBefore"/>
                  <xsl:value-of disable-output-escaping="yes" select="body"/>
                  <xsl:value-of disable-output-escaping="yes" select="body/@fontAfter"/>
                </div>
              </dd>
            </dl>

            <!-- clear all floats -->
            <div class="messageFooter" style="clear:both"></div>

          </div>
        </div>
      </xsl:when>

      <!--
        other messages, like:
        - application message (e.g. invite for file transfer)
        - notification message (e.g. file received)
        - system message (error messages, e.g. invitations KMess does not support)
      -->
      <xsl:when test="@type='application' or @type='presence' or @type='notification' or @type='system'">
        <div class="{body/@dir}">
          <div class="{@type}" dir="{body/@dir}">

            <!-- this style also displays the time for other messages -->
            <xsl:if test="@time">
              <span class="time"><xsl:value-of select="@time"/></span>
            </xsl:if>

            <div class="messageContent">
              <xsl:value-of disable-output-escaping="yes" select="body"/>
            </div>
          </div>
        </div>
      </xsl:when>

    </xsl:choose>
  </xsl:template>


  <!--
    Message group template
    This is used to group follow-up messages from the same contact.
  -->
  <xsl:template match="messagegroup">
    <div class="messagegroup">
      <div class="{message[1]/body/@dir}">
        <div class="{message[1]/@type}">

          <!-- user avatar -->
          <div class="avatar">
            <div class="avatarSpace">
              <!-- use the last message, so the icon updates when the buddy changes their icon
                   with the commonly used "look at my icon now" message -->
              <xsl:choose>
                <xsl:when test="message[last()]/from/contact/displayPicture/@url">
                  <img src="{message[last()]/from/contact/displayPicture/@url}" height="48" alt="" />
                </xsl:when>
                <xsl:otherwise>
                  <img src="buddy_icon.png" height="48" width="48" alt="" />
                </xsl:otherwise>
              </xsl:choose>
            </div>
          </div>

          <dl>
            <!-- message header -->
            <dt dir="{message[1]/from/contact/displayName/@dir}">
              <!-- show name -->
              <span class="name">
                <xsl:value-of disable-output-escaping="yes" select="message[1]/from/contact/displayName/@text"/>
                <xsl:text>:</xsl:text>
              </span>
            </dt>

            <!-- message body -->
            <xsl:for-each select="message">
              <dd dir="{body/@dir}">
                <!-- add extra attribute for css -->
                <xsl:attribute name="class">
                  <xsl:choose>
                    <xsl:when test="position() = last()"><xsl:text>last-child</xsl:text></xsl:when>
                    <xsl:when test="position() = 1"><xsl:text>first-child</xsl:text></xsl:when>
                    <xsl:otherwise><xsl:text>middle-child</xsl:text></xsl:otherwise>
                  </xsl:choose>
                </xsl:attribute>

                <!-- show time? -->
                <xsl:if test="@time">
                  <span class="time"><xsl:value-of select="@time"/></span>
                </xsl:if>

                <div class="messageContent">
                  <xsl:value-of disable-output-escaping="yes" select="body/@fontBefore"/>
                  <xsl:value-of disable-output-escaping="yes" select="body"/>
                  <xsl:value-of disable-output-escaping="yes" select="body/@fontAfter"/>
                </div>
              </dd>
            </xsl:for-each>
          </dl>

          <!-- clear all floats -->
          <div class="messageFooter" style="clear:both"></div>

        </div>
      </div>
    </div>
  </xsl:template>

</xsl:stylesheet>
