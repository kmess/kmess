<?xml version='1.0'?>
<!--
  'Colourful-Enclosed' style for KMess.
  Copyright (C) 2006, Diederik van der Boor
  Copyright (c) 2005, Jess Hall

  Design based on 'Colourful enclosed' by Jess Hall
  http://www.kde-look.org/content/show.php?content=19968

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
-->
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:kmess="http://www.kmess.org/xmlns/ChatStyles/v1/" >
  <xsl:output method="html"
              encoding="utf-8"
              indent="no"
              omit-xml-declaration="yes"
              doctype-public="-//W3C//DTD HTML 4.01 Transitional//EN"
              doctype-system="http://www.w3.org/TR/html4/loose.dtd" />

  <!--
    Declare external parameters with default values
  -->
  <xsl:param name="basepath" select="'.'" />
  <xsl:param name="csspath"  select="'Colourful-Enclosed.css'" />


  <!--
    Template for the page body
  -->
  <xsl:template match="messageRoot">
    <html id="ChatMessageView">
      <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <base href="{$basepath}" />
        <style type="text/css">
          /* standard colors for compatibility with dark color schemes */
          body      { font-size: 10pt; margin: 0; padding: 5px; background-color: #fff; color: #000; }
          a:link    { color: blue; }
          a:visited { color: purple; }
          a:hover   { color: red; }
          a:active  { color: red; }
        </style>
        <style type="text/css">
          /* import additional style */
          @import "<xsl:value-of select="$csspath" />";
        </style>
      </head>
      <body>
        <div id="messageRoot">
          <!-- chat messages are appended here by KMess -->
          <xsl:apply-templates />
        </div>
      </body>
    </html>
  </xsl:template>


  <!--
    Template for normal messages
  -->
  <xsl:template match="message">
    <xsl:choose>

      <!--
        normal chat message,
        - incoming message from a contact.
        - outgoing message from the user.
        - incoming offline-im message from a contact.
      -->
      <xsl:when test="@type='incoming' or @type='outgoing' or @type='offlineIncoming'">
        <div class="{body/@dir}">
          <div class="{@type}">

            <dl>
              <!-- message header -->
              <dt dir="{from/contact/displayName/@dir}" class="{from/contact/displayName/@dir}">
                <!-- show time? -->
                <!-- time will be floated to the right, so it must be before the other text -->
                <xsl:if test="@time">
                  <span class="time">
                    <xsl:if test="@type='offlineIncoming'">
                      <xsl:value-of select="@date"/> &#160;
                    </xsl:if>
                    <xsl:value-of select="@time"/>
                  </span>
                </xsl:if>

                <!-- show name -->
                <span class="name">
                  <xsl:value-of disable-output-escaping="yes" select="from/contact/displayName/@text"/>
                  <xsl:text>:</xsl:text>
                </span>
              </dt>

              <!-- message body -->
              <dd dir="{body/@dir}">
                <xsl:value-of disable-output-escaping="yes" select="body/@fontBefore"/>
                <xsl:value-of disable-output-escaping="yes" select="body"/>
                <xsl:value-of disable-output-escaping="yes" select="body/@fontAfter"/>
              </dd>
            </dl>

          </div>
        </div>
      </xsl:when>

      <!--
        other messages, like:
        - application message (e.g. invite for file transfer)
        - notification message (e.g. file received)
        - system message (error messages, e.g. invitations KMess does not support)
      -->
      <xsl:when test="@type='application' or @type='presence' or @type='notification' or @type='system'">
        <div class="{body/@dir}">
          <div class="{@type}" dir="{body/@dir}">

            <xsl:value-of disable-output-escaping="yes" select="body"/>

          </div>
        </div>
      </xsl:when>

    </xsl:choose>
  </xsl:template>


  <!--
    Message group template
    This is used to group follow-up messages from the same contact.
  -->
  <xsl:template match="messagegroup">
    <div class="messagegroup">
      <div class="{message[1]/body/@dir}">
        <div class="{message[1]/@type}">

          <dl>
            <!-- message header -->
            <dt dir="{message[1]/from/contact/displayName/@dir}">
              <!-- show time? -->
              <xsl:if test="message[1]/@time">
                <span class="time"><xsl:value-of select="message[1]/@time"/> - <xsl:value-of select="message[last()]/@time"/></span>
              </xsl:if>

              <!-- show name -->
              <span class="name">
                <xsl:value-of disable-output-escaping="yes" select="message[1]/from/contact/displayName/@text"/>
                <xsl:text>:</xsl:text>
              </span>
            </dt>

            <!-- message body -->
            <xsl:for-each select="message">
              <dd dir="{body/@dir}">
                <!-- add extra attribute for css -->
                <xsl:attribute name="class">
                  <xsl:choose>
                    <xsl:when test="position() = last()"><xsl:text>last-child</xsl:text></xsl:when>
                    <xsl:when test="position() = 1"><xsl:text>first-child</xsl:text></xsl:when>
                    <xsl:otherwise><xsl:text>middle-child</xsl:text></xsl:otherwise>
                  </xsl:choose>
                </xsl:attribute>

                <xsl:value-of disable-output-escaping="yes" select="body/@fontBefore"/>
                <xsl:value-of disable-output-escaping="yes" select="body"/>
                <xsl:value-of disable-output-escaping="yes" select="body/@fontAfter"/>
              </dd>
            </xsl:for-each>
          </dl>

        </div>
      </div>
    </div>
  </xsl:template>

</xsl:stylesheet>
