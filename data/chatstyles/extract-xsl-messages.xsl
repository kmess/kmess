<?xml version='1.0'?>
<!--
  KMess transformation sheet to extract messages.
  Copyright (C) 2006, Diederik van der Boor

  Generates output for xgettext.
  This program is started with xsltproc
  by the 'extract-xsl-messages' shell script.
-->
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:kmess="http://www.kmess.org/xmlns/ChatStyles/v1/" >
  <xsl:output method="text"
              encoding="utf-8"
              indent="no"
              omit-xml-declaration="yes" />

  <!--
    parameter with file name
  -->
  <xsl:param name="filename" select="'kmess/styles/unknown-xsl'" />


  <!--
    Trick to get \n character
  -->
  <xsl:variable name="crlf"><xsl:text>
</xsl:text></xsl:variable>


  <!--
    Process root node, search for <xsl:text> nodes.
  -->
  <xsl:template match="/xsl:stylesheet">
    <xsl:apply-templates select="//xsl:text" />
  </xsl:template>


  <!--
    Process <xsl:text> nodes.
  -->
  <xsl:template match="xsl:text">

    <!-- trip the node text leafs and simplyfy space -->
    <xsl:variable name="trimmed" select=" normalize-space( text() ) " />

    <!-- ignore <xsl:text> </xsl:text> nodes, output others for xgettext format -->
    <xsl:if test=" $trimmed != '' and  @kmess:translate = 'true' ">
      <!-- line 1 -->
      <xsl:text>//i18n: file </xsl:text>
      <xsl:value-of select="$filename" />
      <xsl:text> line 0</xsl:text>
      <xsl:value-of select="$crlf" />

      <!-- line 2 -->
      <xsl:text>// xgettext: no-c-format</xsl:text>
      <xsl:value-of select="$crlf" />

      <!-- line 3 -->
      <xsl:text>i18n("</xsl:text>
      <xsl:value-of select=" $trimmed " />
      <xsl:text>");</xsl:text>
      <xsl:value-of select="$crlf" />
    </xsl:if>
  </xsl:template>
</xsl:stylesheet>
