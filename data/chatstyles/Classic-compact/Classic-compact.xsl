<?xml version='1.0'?>
<!--
  KMess Classic-compact chat style
  Copyright (C) 2006, Diederik van der Boor

  Based on the original KMess chat style.

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
-->
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:kmess="http://www.kmess.org/xmlns/ChatStyles/v1/" >
  <xsl:output method="html"
              indent="no"
              omit-xml-declaration="yes"
              encoding="utf-8" />

  <!--
    Normal chat message.
    This is used to process individual chat messages.
  -->
  <xsl:template match="message">
      <xsl:choose>

        <!-- incoming and outgoing chat messages -->
        <xsl:when test="@type='incoming' or @type='outgoing' or @type='offlineIncoming'">
          <div dir="{body/@dir}">

            <!-- contact name part -->
            <span dir="{from/contact/displayName/@dir}">
              <xsl:attribute name="style">
                <xsl:choose>
                  <xsl:when test="from/contact/displayName/@dir='ltr'">padding-right: 0.3em;</xsl:when>
                  <xsl:otherwise>padding-left: 0.3em;</xsl:otherwise>
                </xsl:choose>
              </xsl:attribute>

              <font>
                <!-- contact name color -->
                <xsl:attribute name="color">
                  <xsl:choose>
                    <xsl:when test="@type='incoming' or @type='offlineIncoming'">#D60000</xsl:when>
                    <xsl:otherwise>#006A00</xsl:otherwise>
                  </xsl:choose>
                </xsl:attribute>

                <!-- show time? -->
                <xsl:if test="@time">
                  <xsl:if test="@type='offlineIncoming'">
                    <xsl:value-of select="@date"/> &#160;
                  </xsl:if>
                  <xsl:value-of select="@time"/>
                  <xsl:text> </xsl:text>
                </xsl:if>

                <!-- show contact name -->
                <xsl:value-of disable-output-escaping="yes" select="from/contact/displayName/@text"/>
                <xsl:text> says: </xsl:text>
              </font>
            </span>

            <!-- show message -->
            <xsl:value-of disable-output-escaping="yes" select="body/@fontBefore"/>
            <xsl:value-of disable-output-escaping="yes" select="body"/>
            <xsl:value-of disable-output-escaping="yes" select="body/@fontAfter"/>
          </div>
        </xsl:when>

        <!-- application message (e.g. invite for file transfer) -->
        <xsl:when test="@type='application'">
          <div dir="{body/@dir}">
            <font color="blue">
              <xsl:value-of disable-output-escaping="yes" select="body"/>
            </font>
          </div>
        </xsl:when>


        <!-- presence message (e.g. contact gone offline) -->
        <xsl:when test="@type='presence'">
          <div dir="{body/@dir}">
            <font color="gray">
              <xsl:value-of disable-output-escaping="yes" select="body"/>
            </font>
          </div>
        </xsl:when>

        <!-- notification message (e.g. file received) -->
        <xsl:when test="@type='notification'">
          <div dir="{body/@dir}">
            <font color="purple">
              <xsl:value-of disable-output-escaping="yes" select="body"/>
            </font>
          </div>
        </xsl:when>

        <!-- system message (error messages, e.g. invitations KMess does not support)  -->
        <xsl:when test="@type='system'">
          <div dir="{body/@dir}">
            <hr size="2" color="red"/>
            <font color="red"><xsl:value-of disable-output-escaping="yes" select="body"/></font>
            <hr size="2" color="red"/>
          </div>
        </xsl:when>

      </xsl:choose>
  </xsl:template>


  <!--
    Message group template
    This is used to group follow-up messages from the same contact.
  -->
  <xsl:template match="messagegroup">
    <div class="messagegroup">

      <!-- contact name part -->
      <span dir="{message[1]/from/contact/displayName/@dir}">
        <xsl:attribute name="style">
          <xsl:choose>
            <xsl:when test="message[1]/from/contact/displayName/@dir='ltr'">float:left; clear:both; padding-right: 0.3em;</xsl:when>
            <xsl:otherwise>float:right; clear:both; padding-left: 0.3em;</xsl:otherwise>
          </xsl:choose>
        </xsl:attribute>

        <font>
          <!-- contact name color -->
          <xsl:attribute name="color">
            <xsl:choose>
              <xsl:when test="message[1]/@type='incoming' or message[1]/@type='offlineIncoming'">#D60000</xsl:when>
              <xsl:otherwise>#006A00</xsl:otherwise>
            </xsl:choose>
          </xsl:attribute>

          <!-- show time? -->
          <xsl:if test="message[1]/@time">
            <xsl:value-of select="message[1]/@time"/>
            <xsl:text> </xsl:text>
          </xsl:if>

          <!-- show contact name -->
          <xsl:value-of disable-output-escaping="yes" select="message[1]/from/contact/displayName/@text"/>
          <xsl:text> says: </xsl:text>
        </font>
      </span>

      <!-- show message -->
      <xsl:for-each select="message">
        <!-- show time for each followup message? -->
        <!--
        <xsl:if test="position() != 1 and @time">
          <font>
            <xsl:attribute name="color">
              <xsl:choose>
                <xsl:when test="@type='incoming' or @type='offlineIncoming'">#D60000</xsl:when>
                <xsl:otherwise>#006A00</xsl:otherwise>
              </xsl:choose>
            </xsl:attribute>
            <xsl:value-of select="@time"/>
            <xsl:text>: </xsl:text>
          </font>
        </xsl:if>
        -->

        <!-- show message -->
        <div dir="{body/@dir}">
          <xsl:value-of disable-output-escaping="yes" select="body/@fontBefore"/>
          <xsl:value-of disable-output-escaping="yes" select="body"/>
          <xsl:value-of disable-output-escaping="yes" select="body/@fontAfter"/>
        </div>
      </xsl:for-each>
    </div>
  </xsl:template>

</xsl:stylesheet>
