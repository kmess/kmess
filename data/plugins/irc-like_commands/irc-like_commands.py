#!/usr/bin/env kross
# -*- coding: utf-8 -*-

#*************************************************************************#
# This file is part of the KMess project.                                 #
# (C) Copyright 2008 Sjors Gielen <sjors@kmess.org>                       #
# (C) Copyright 2010 Daniel E. Moctezuma <democtezuma@gmail.com>          #
#                                                                         #
# This program is free software; you can redistribute it and/or modify    #
# it under the terms of the GNU General Public License as published by    #
# the Free Software Foundation; either version 3 of the License, or       #
# (at your option) any later version.                                     #
#                                                                         #
# This program is distributed in the hope that it will be useful,         #
# but WITHOUT ANY WARRANTY; without even the implied warranty of          #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           #
# GNU General Public License for more details.                            #
#                                                                         #
# You should have received a copy of the GNU General Public License       #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.   #
#*************************************************************************#

# Original C++ version: Sjors Gielen
# Python port: Daniel E. Moctezuma

import Chat
import ContactList
import Session

import Kross

trans = Kross.module( "kdetranslation" )

def messageAboutToSend( command ):
    if len( command ) > 2 and command[ 2 ] != '/':

        if command[ 0:2 ] == '//':
            Chat.sendMessage( command[ 1:len( command ) ] )

        elif command[ 0 ] == '/':
            if handle( command[ 1:len( command ) ] ) == False:
                return
            else:
                Chat.clearMessageEditor()



def handle( command ):
    if command == '' or command.strip( ' ' ) == '':
        return False

    # This is used in the '/me' command
    # 'arguments' is the message after '/me' like in IRC.
    arguments = command[ command.find( ' ' ) + 1:len( command ) ]

    # This is used when the '/status' command is typed
    # it needs an argument what will be the status (e.g. /status online).
    argument = command[ command.find( ' ' ) + 1:len( command ) ].lower()

    # This is the shorcut command as in /online, /brb, /busy, etc.
    command = command[ 0:command.find( ' ' ) ].lower()

    if command == 'status' and argument.count( ' ' ) >= 1:
        forms = Kross.module( 'forms' )
        forms.showMessageBox( 'Sorry', trans.i18n( 'Incorrect /status syntax' ),
          trans.i18n( 'You used an incorrect syntax for the /status command.\nThe correct syntax is: /status online|away|idle|brb|busy|lunch|phone|invisible.\nYou can also use shortcuts like /online or /phone.') )

        return True

    status = command + ' ' + argument
    command = argument

    if ( command == 'online' or status == 'status online' ) or ( command == 'back' or status == 'status back' ):
        Session.setStatus( 'online' )
        return True
    elif command == 'away' or status == 'status away':
        Session.setStatus( command )
        return True
    elif command == 'idle' or status == 'status idle':
        Session.setStatus( command )
        return True
    elif command == 'brb' or status == 'status brb':
        Session.setStatus( command )
        return True
    elif command == 'busy' or status == 'status busy':
        Session.setStatus( command )
        return True
    elif command == 'lunch' or status == 'status lunch':
        Session.setStatus( command )
        return True
    elif command == 'phone' or status == 'status phone':
        Session.setStatus( command )
        return True
    elif command == 'invisible' or status == 'status invisible':
        Session.setStatus( command )
        return True


    elif command == 'block':
        if Chat.getNumberOfContactsInChat() != 1:
            forms = Kross.module( 'forms' )
            forms.showMessageBox( 'Sorry', trans.i18n( 'Cannot use /block command' ),
              trans.i18n( 'You cannot use the /block command in a group chat.' ) )

            return True

        ContactList.blockContact( ContactList.getContactHandleInCurrentChat() )

    elif command == 'unblock':
        if Chat.getNumberOfContactsInChat() != 1:
            forms = Kross.module( 'forms' )
            forms.showMessageBox( 'Sorry', trans.i18n( 'Cannot use /unblock command' ),
              trans.i18n( 'You cannot use the /unblock command in a group chat.' ) )

            return True

        ContactList.unblockContact( ContactList.getContactHandleInCurrentChat() )

    elif command == 'me':
        message = "* " + Session.getFriendlyName() + " " + arguments
        Chat.sendMessage( message )

    else:
        if status.count( '/' ) >= 1:
            return False

        forms = Kross.module( 'forms' )
        forms.showMessageBox( 'Sorry', trans.i18n( 'Unknown command' ),
          trans.i18n( 'You entered an unknown command, if you did not want this\nmessage to be a command, prepend it with another "/".' ) )

        return True
