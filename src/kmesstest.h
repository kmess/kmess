/***************************************************************************
                          kmesstest.h  -  description
                             -------------------
    begin                : Sun Jan 5 2003
    copyright            : (C) 2003 by Mike K. Bennett
    email                : mkb137b@hotmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef KMESSTEST_H
#define KMESSTEST_H

#include "kmessdebug.h"  // for KMESSTEST

#include <QObject>


// forward declarations
class Chat;
class ChatWindow;
class KMessApplication;
class MainWindow;
namespace KMess
{
  class MsnSession;
};



/**
 * @brief A class responsible for testing the other classes.
 *
 * This class can be used by uncommenting a few lines in main.cpp.
 * New functions can be added to implement various test cases,
 * like opening chat windows without logging in.
 * Most of these functions are included in an <code>\#ifdef</code> block,
 * so they are not compiled in the release version.
 *
 * @author Mike K. Bennett
 * @ingroup Debug
 */
class KMessTest : public QObject
{
  // No signals/slots yet, but keep the option open
  Q_OBJECT

  public:
    // The constructor
               KMessTest( MainWindow *mainWindow );
    // The destructor
    virtual   ~KMessTest();
    // The main testing function
    void       runTest( const QString &testname );
    // Cleans up and exits kmess if necessary
    void       endTest();

  private: // Private methods
    void       testAddContactDialog();
    void       testChatHistoryDialog();
    void       testChallengeHandler();
    void       testChatWindow();
    void       testConnect();
    void       testContactAddedUserDialog();
    void       testContactList();
    void       testCrash();
    void       testGroupsAndContacts();
    void       testMsnPlus();
    void       benchmarkParser();
    void       testNotificationConnection();
    void       testNotifications();
    void       testNowListening();
    void       testSettings();
    void       testSoapConnection();
    void       testSwitchboardConnection();
    void       testSystemTray();
    void       testTransferWindow();
    void       testUpnpConnection();
    void       testXslTransformation();
    void       testOfflineMessages();

  private slots:
    void       testContactAddedUserDialogSlot( const QString &handle, const QStringList &groupIds, const int code );
    void       testContactListSlot();
    void       testChatWindowSlot();
    void       testMsnPlusInteractive();

  private: // Private attributes
    // Control whether or not to quit after performing the test
    bool       quitAfterTest_;
    // A pointer to the main widget, also used to gain access to all its attributes
    MainWindow  *mainWindow_;
    // Pointer to the kmess application object
    KMessApplication *kmessApp_;
};

#endif
