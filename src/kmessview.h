/***************************************************************************
                          kmessview.h  -  description
                             -------------------
    begin                : Thu Jan 9 2003
    copyright            : (C) 2003 by Mike K. Bennett
                           (C) 2005 by Diederik van der Boor
    email                : mkb137b@hotmail.com
                           vdboor --at-- codingdomain.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef KMESSVIEW_H
#define KMESSVIEW_H

#include "ui_kmessview.h"

// Forward declarations
class Contact;

namespace KMess
{
  class MsnContact;
};


/**
 * @brief The main view area, displaying the user status area and contact list.
 *
 * This class implements the actions of the inherited user interface class.
 * It contains the contact list view, user status and email displays.
 * The KMessViewInterface class is automatically generated from it's <code>.ui</code> file.
 *
 * @author Mike K. Bennett
 * @ingroup Root
 */
class KMessView : public QWidget, private Ui::KMessView
{
  Q_OBJECT

  friend class KMessTest;

  public:
    // The constructor
                     KMessView( QWidget *parent=0 );
    // The destructor
    virtual         ~KMessView();
    // The currently playing song was changed.
    void             changedSong( const QString &artist, const QString &album, const QString &track, bool playing );
    // The "show history box" menu item has been toggled.
    void             toggleShowHistoryBox( bool show );
    // The "show search in contact list" menu item has been toggled.
    void             toggleShowSearchFrame( bool show );

  private: // Private methods

  public slots:
    // Show the context menu
    void             showContextMenu( const QPoint &point );

  private slots: // Private slots
    // Called when a contact is now online/offline
    void             slotContactChangedStatus( KMess::MsnContact *contact, bool isInitialStatus = false );
    // The email label was clicked so open the user's email client or webmail
    void             slotEmailLabelClicked();
    // Update the friendly name from the inline editing label
    void             updateFriendlyName( bool refreshOnly = false );
    // Update the personal message from the inline editing label
    void             updatePersonalMessage( bool refreshOnly = false );
    // Update the users display pic
    void             slotUpdateDisplayPicture();
    // Change whether the email label is displayed based on account settings.
    void             slotUpdateEmailDisplay();
    // Update the email count.
    void             slotUpdateNoEmails();
    // Update the user's status.
    void             slotUpdateUserStatus();

  signals:
    // Show the settings of the current account.
    void             showSettings();
    // Notify about a change in the selection within the contact list view
    void             selectionChanged( const QItemSelection &selection );
};

#endif
