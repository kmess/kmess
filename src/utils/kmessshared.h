/***************************************************************************
                          kmessshared.h  -  description
                             -------------------
    begin                : Thu May 8 2008
    copyright            : (C) 2008 by Valerio Pilo
    email                : valerio@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef KMESSSHARED_H
#define KMESSSHARED_H

#include <QPixmap>


// Forward declaration
class KUrl;
class QDateTime;



/**
 * @brief Shared globally useful methods for KMess
 *
 * This class is just a container for many methods which are useful throughout the entire
 * application.
 *
 * @author Valerio Pilo <valerio@kmess.org>
 * @ingroup Utils
 */
class KMessShared
{

  public: // Public static methods
    // Verify if an email address is well-formed
    static bool            validateEmail( const QString &email );
    // Open the given URL respecting the user's preference
    static void            openBrowser( const KUrl &url );
    // Open the given URL mail respecting the user's preference
    static void            openEmailClient( const QString &mailto = "", const QString &folder = "Inbox" );
    // Select the next available file
    static bool            selectNextFile( const QString &directory, const QString &baseName, QString &suffix, const QString &extension, int &startingNumber );
    // Draws an icon with an overlay from two pixmaps (width and height -1, the defaults, mean take the width and height from the icon itself)
    static QPixmap         drawIconOverlay( const QPixmap icon, const QPixmap overlay, int width = -1, int height = -1, float sizeFactor = 0.5, float iconSizeFactor = 1.0 );
    // Returns the time passed in a fuzzy format.
    static QString         fuzzyTimeSince( const QDateTime &datetime );
    // Truncate an UTF-8 multibyte string to a fixed byte size
    static QString         fixStringToByteSize( const QString &string, quint32 size );

  private:
    // Create the html file to request the given service
    static QString         createRequestFile( const QString &mailto, const QString &folder );
};

#endif
