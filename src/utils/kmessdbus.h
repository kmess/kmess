/***************************************************************************
                          kmessdbus.cpp  -  description
                             -------------------
    begin                : Wed Jun 25 2008
    copyright            : (C) 2008 by Ruben Vandamme
    email                : vandammeru gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef KMESSDBUS_H
#define KMESSDBUS_H

#include <QObject>
#include <QMetaType>
#include <QtCore/QMetaType>
#include <QtDBus/QDBusArgument>
#include "../contact/contact.h"
#include "kmessdbusdatatypes.h"


// Forward declarations
namespace KMess
{
  class MsnSession;
};



class KMessDBus: public QObject
{
  Q_OBJECT
  Q_CLASSINFO( "D-Bus Interface", "org.kmess.remoteControl" )

  public: // Public methods
    // The constructor
                     KMessDBus( KMess::MsnSession *parent );
    // The destructor
    virtual         ~KMessDBus();

  public slots:
    void            connect();
    void            disconnect();
    bool            isConnected();
    void            setStatus( int status );
    int             getStatus();
    QString         getFriendlyName( int formattingMode = -1 );
    DBusContactList getList();
    void            requestChat( QString handle );
    void            startFileTransfer( QString handle, QString filename );

  private:
    KMess::MsnSession *session_;

};

#endif
