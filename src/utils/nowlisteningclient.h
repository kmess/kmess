/***************************************************************************
                          nowlisteningclient.h -  description
                             -------------------
    begin                : Sat Nov 4 2006
    copyright            : (C) 2006 by Diederik van der Boor
    email                : "vdboor" --at-- "codingdomain.com"
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef NOWLISTENINGCLIENT_H
#define NOWLISTENINGCLIENT_H

#include <QMutex>
#include <QTimer>


// Forward declarations
class QDBusInterface;



/**
 * @brief  Helper class to query common media players for "now listening to" information.
 *
 * It uses D-Bus calls to other applications to query the information.
 * The applications are polled for information every 5 seconds.
 * The KMess class holds a reference to this helper class,
 * and calls setEnabled() to start or stop the detection.
 * When the current song changed, the changedSong() signal will be fired.
 *
 * @author Diederik van der Boor, Valerio Pilo
 * @ingroup Utils
 */
class NowListeningClient : public QObject
{
  Q_OBJECT

  public:
    // The construtor
                       NowListeningClient();
                       ~NowListeningClient();

  public:
    // Enable or disable the update interval timer.
    void               setEnabled( bool enable );

  public slots:
    // Update the current song
    void               slotUpdate();
    // Query Amarok 1 for track information
    bool               queryAmarok1();
    // Query Player mpris compliant for track information
    bool               queryMprisPlayers();
    // Query KsCD for track information
    bool               queryKsCD();
    // Query Juk for track information
    bool               queryJuk();

  public: // Public structures

    struct MprisPlayerStatus
    {
      MprisPlayerStatus() { Status = hasShuffle = hasRepeat = hasPlaylistRepeat = -1; };

      int Status;            // 0 = Playing, 1 = Paused, 2 = Stopped.
      int hasShuffle;        // 0 = Playing linearly , 1 = Playing randomly.
      int hasRepeat;         // 0 = Go to the next element once the current has finished playing , 1 = Repeat the current element
      int hasPlaylistRepeat; // 0 = Stop playing once the last element has been played, 1 = Never give up playing
    };


  private:
    // The active album
    QString            album_;
    // The active artist
    QString            artist_;
    // Whether the user is currently playing a title.
    bool               playing_;
    // The interval timer
    QTimer             timer_;
    // The active track
    QString            track_;
    // Mutex used to avoid multiple requests from being active at once
    QMutex             mutex_;

  signals:
    void               changedSong( const QString &artist, const QString &album, const QString &track, bool playing );
};



#endif
