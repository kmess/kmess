/***************************************************************************
                          kmessdbus.cpp  -  description
                             -------------------
    begin                : Wed Jun 25 2008
    copyright            : (C) 2008 by Ruben Vandamme
    email                : vandammeru gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "kmessdbusadaptor.h"
#include "kmessdbus.h"
#include "../account.h"
#include "../kmessapplication.h"
#include "../kmessglobal.h"
#include "../mainwindow.h"
#include "../kmessdebug.h"
#include "../chat/chatmaster.h"
#include "../utils/richtextparser.h"

#include <QtDBus/QDBusMetaType>



KMessDBus::KMessDBus( KMess::MsnSession *parent )
 : QObject( (QObject*) parent ),
   session_( parent )
{
  new RemoteControlAdaptor( this );
  QDBusConnection dbus = QDBusConnection::sessionBus();
  dbus.registerObject( "/remoteControl", this );

  KMESSDBUS_REGISTER_DATATYPES
}



// The destructor
KMessDBus::~KMessDBus()
{
#ifdef KMESSDEBUG_KMESSDBUS
  kmDebug() << "DESTROYED.";
#endif
}



// Connect
void KMessDBus::connect()
{
#ifdef KMESSDEBUG_KMESSDBUS
  kmDebug() << "Connect() called";
#endif

  if( ! globalSession->isLoggedIn() )
  {
    MainWindow::instance()->checkAutologin();
  }
}



// Disconnect
void KMessDBus::disconnect()
{
#ifdef KMESSDEBUG_KMESSDBUS
  kmDebug() << "Disconnect() called";
#endif

  if( globalSession->isLoggedIn() )
  {
    globalSession->logOut();
  }
}



// Check if KMess is connected
bool KMessDBus::isConnected()
{
#ifdef KMESSDEBUG_KMESSDBUS
  kmDebug() << "isConnected() called";
#endif

  return globalSession->isLoggedIn();
}



// Set the status
void KMessDBus::setStatus( int status )
{
#ifdef KMESSDEBUG_KMESSDBUS
  kmDebug() << "setStatus( int ) called";
#endif

  if( globalSession->isLoggedIn() )
  {
    globalSession->self()->changeStatus( (KMess::MsnStatus) status );
  }
}



// Get the status
int KMessDBus::getStatus()
{
#ifdef KMESSDEBUG_KMESSDBUS
  kmDebug() << "getStatus() called";
#endif

  return globalSession->self()->status();
}



// Get friendly name
QString KMessDBus::getFriendlyName( int formattingMode )
{
#ifdef KMESSDEBUG_KMESSDBUS
  kmDebug() << "getFriendlyname( int ) called";
#endif

  if ( Account::connectedAccount == 0 )
  {
#ifdef KMESSDEBUG_KMESSDBUS
    kmDebug() << "KMess is not connected; returning QString().";
#endif
    return QString();
  }

  if(formattingMode < 0 || formattingMode > 2 )
  {
#ifdef KMESSDEBUG_KMESSDBUS
    kmDebug() << "Unknown formattingMode supplied (" << formattingMode << "), defaulting";
#endif

    return Account::connectedAccount->getFriendlyName();
  }
  else
  {
    return Account::connectedAccount->getFriendlyName( FormattingMode( formattingMode ) );
  }
}


// Comparison function for qSort()
static bool _compareDBusContact( const DBusContact &contact1, const DBusContact &contact2 )
{
  return QString::compare( contact1.friendlyName, contact2.friendlyName, Qt::CaseInsensitive ) < 0;
}



// Get contactlist
DBusContactList KMessDBus::getList()
{
#ifdef KMESSDEBUG_KMESSDBUS
  kmDebug() << "getList() called";
#endif

  QList<DBusContact> list;
  DBusContact dbusContact;

  foreach( KMess::MsnContact *contact, globalSession->contactList()->contacts() )
  {
    // Don't show offline contacts
    if( contact->isOffline() ) continue;

    dbusContact = DBusContact();
    dbusContact.friendlyName = Contact( contact)->getFriendlyName();
    dbusContact.handle       = contact->handle();
    dbusContact.status       = contact->status();
    list.append( dbusContact );
  }

  qSort( list.begin(), list.end(), _compareDBusContact );

  return list;
}


// Request Chat
void KMessDBus::requestChat( QString handle )
{
#ifdef KMESSDEBUG_KMESSDBUS
  kmDebug() << "startChat() called";
#endif

  KMessApplication::instance()->chatMaster()->requestChat( handle );
}



// Start a filetransfer
void KMessDBus::startFileTransfer( QString handle, QString filename )
{
#ifdef KMESSDEBUG_KMESSDBUS
  kmDebug() << "startFileTransfer() called";
  kmDebug() << "sending file " << filename << " to " << handle;
#endif

#ifdef __GNUC__
#warning TODO: Start file transfer over DBus
#endif
  //KMessApplication::instance()->chatMaster()->startChatAndFileTransfer( handle, filename );
}



#include "kmessdbus.moc"
