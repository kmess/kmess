/***************************************************************************
                    richtextparser.h -  parses MSN and Plus messages
                             -------------------
    begin                : April 30, 2008
    copyright            : (C) 2008 by Valerio Pilo
                           (C) 2009 by Sjors Gielen
    email                : valerio@kmess.org
                           sjors@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef RICHTEXTPARSER_H
#define RICHTEXTPARSER_H

#include "../emoticon.h"

#include <QRegExp>
#include <QStringList>
#include <QHash>


#define MSN_PLUS_STRINGCACHESIZE     100


// Forward declarations
class EmoticonManager;


/**
 * String types available when dealing with formatting
 *
 * Use one of these flags to obtain a string parsed the way you need it
 */
enum FormattingMode
{
    STRING_ORIGINAL             = 0 /// Unchanged string
  , STRING_CLEANED                  /// All formatting tags removed
  , STRING_CLEANED_ESCAPED          /// Same as STRING_CLEANED, but html-escaped
  , STRING_FORMATTED                /// All formatting tags translated to HTML
  , STRING_CHAT_SETTING             /// Tags parsed or removed depending on the user's chat window formatting option
  , STRING_CHAT_SETTING_ESCAPED     /// Same as STRING_CHAT_SETTING, but html-escaped if the option is off
  , STRING_LIST_SETTING             /// Tags parsed or removed depending on the user's contact list formatting option
  , STRING_LIST_SETTING_ESCAPED     /// Same as STRING_LIST_SETTING, but html-escaped if the option is off
};



/**
 * @brief Implements features from the MSN Plus extension to KMess
 *
 * Implements features from the "MSN Plus! Live" extension for MSN Messenger
 * and Windows Live Messenger. These features can be: custom formatting in
 * nick names, personal messages and in chat text; sending/receiving of
 * little sound clips; contact pinging; special colored phrases which trigger
 * a sound; various IRC-style commands, like /me; et cetera.
 * Some of them are worth adding in KMess, some others are kind of useless but
 * someday they could be implemented.
 *
 * Currently the only implemented feature is text formatting, which is the
 * most used and also the most annoying to not have; reading friendly names
 * with all the tags and codes really IS annoying.
 *
 * @ingroup Utils
 * @author Valerio Pilo <valerio@kmess.org>
 * @author Antonio Nastasi <sifcenter@gmail.com>
 * @author Sjors Gielen <dazjorz@kmess.org>
 */
class RichTextParser
{
  friend class KMessTest;

  public:
    // These flags are given to parseMsnString for the options, but they are also used
    // for regexps!
    // ( PARSE_EMOTICONS, EMOTICONS_SMALL, PARSE_LINKS, PARSE_MSNPLUS )
    enum FormattingOptions {
      PARSE_GEEKLINKS = 1   /// Whether to parse geek links (kmess.org, etc)
    , PARSE_NORMLINKS = 2   /// Whether to parse normal links (http://kmess.org/, etc)
    , PARSE_EMAIL     = 4   /// Whether to parse e-mail addresses
    , PARSE_MSNEMO    = 8   /// Whether to parse the normal MSN emoticons
    , PARSE_CUSTOM    = 16  /// Whether to parse custom emoticons
    , PARSE_PENDING   = 32  /// Whether to parse pending emoticons (emoticons that are being transferred)
    , PARSE_LONGWORDS = 64  /// Whether to insert optional line breaks inside long words

    // These will not be used for regexps; they are for simplicity.
    , PARSE_LINKS     = 7   /// Whether to parse links and e-mail addresses (old argument: 4 showLinks)
    , PARSE_EMOTICONS = 56  /// Whether to parse emoticons (the normal MSN ones, custom, and pending emoticons) (old argument: 2 showEmoticons)
    , PARSE_MSNPLUS   = 128  /// Whether to parse MSN Plus tags (old argument: 5 showFormatting)
    , KEEP_PLUSTAGS   = 256  /// Whether to keep MSN Plus tags
    , EMOTICONS_SMALL = 512  /// Whether to show small emoticons (if not given, they will be large) (old argument: 3 showSmallEmoticons)
    , DISALLOW_EMO_LINKS = 1024 /// Whether *not* to allow emoticon adding links (old reversed argument: 6 !allowEmoticonLinks)
    };

  public: // Public methods

    // Delete the instance of the rich text parser
    static void            destroy();
    // Return a singleton instance of the rich text parser
    static RichTextParser *instance();
    // Return the given string with MSN Plus! formatting stripped out
    QString                getCleanString( const QString &string );
    // Return the given string with links and emoticons translated to HTML
    QString                parseMsnString( const QString &text, FormattingOptions options, const QString &handle = *((QString*)0), QStringList &pendingEmoticonTags = *((QStringList*)0) );

  private: // Private methods
    // Constructor
                    RichTextParser();
    // Destructor
    virtual        ~RichTextParser();
    // Return the given string with MSN Plus! formatting parsed
    QString         getFormattedString( const QString &string );
    // Turns color codes (english color names, RGB triplets, MSN Plus! palette colors) into an HTML RGB color code
    QString         getHtmlColor( const QString& color );
    // Turns a string into a gradient colored one, using Qt HTML tags
    QString         getHtmlGradient( const QString& text, const QString& start, const QString& end );
    // Replace the Messenger Plus characters with HTML markup
    QString         parseMsnPlusString( const QString &text );


  private: // Private properties

    // QHash maps for storing the string already parsed
    QHash<QString, QString> cleanedStringsCache_;
    QHash<QString, QString> formattedStringsCache_;

    // Regular expression to match colors
    QRegExp         colorMatch_;
    // The last assigned ID for pending emoticon placeholders
    int             lastPendingEmoticonId_;
    // The instance of the rich text parser singleton
    static RichTextParser *instance_;
    // An instance of the Emoticon Manager
    EmoticonManager *emoticonManager_;
    // List of MSN Plus colors
    QStringList     predefinedColors_;
    // List of all regexps that never change
    QHash<int, QRegExp> regexps_;
    // List of two-letter extensions which aren't country-code TLD's
    QStringList invalidCcTldList_;
    // List of valid non-countrycode TLD's (com, org, net, ...)
    QStringList topLevelDomainList_;
    // Escaped string for an emoticon placeholder image
    QString pendingEmoticonPlaceholder_;

};



/**
 * @brief A collection of strings to use with formatting
 *
 * A little class to hold together three strings: the original one, a formatted and a cleaned up version of it;
 * it also allows for very easy manipulation.
 *
 * @author Valerio Pilo <valerio@kmess.org>
 */
class FormattedString
{
  public:
    // Constructor, can be told to not parse the initial string but just save it (during KMess initialization)
    FormattedString( const QString &string = QString(), bool parseName = true );
    // Set a string, which will be formatted according to the given flags
    void setString( const QString &string );
    // Returns the specified version of the string
    const QString &getString( FormattingMode mode = STRING_CLEANED ) const;

    // Returns the cleaned up version of the set string
    inline const QString &  getCleaned() const { return cleaned_;   }
    // Returns the formatted version of the set string
    inline const QString &getFormatted() const { return formatted_; }
    // Returns the original string
    inline const QString & getOriginal() const { return original_;  }
    // Changes the formatting options which will be used for the string
    inline void setOptions( bool showEmoticons = true, bool showSmallEmoticons = true, bool showLinks = false )
    {
      showEmoticons_      = showEmoticons;
      showLinks_          = showLinks;
      showSmallEmoticons_ = showSmallEmoticons;
    }

  private:
    // HTML-Escaped version of the string
    QString               escaped_;
    // cleaned up version of the string
    QString               cleaned_;
    // The formatted version of the string
    QString               formatted_;
    // The original string
    QString               original_;
    // Whether to show emoticons in the formatted string
    bool                  showEmoticons_;
    // Whether to make links clickable or not
    bool                  showLinks_;
    // Whether to display the emoticons big or small
    bool                  showSmallEmoticons_;
};



#endif
