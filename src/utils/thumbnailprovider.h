/***************************************************************************
                          thumbnailprovider.h  -  description
                             -------------------
    begin                : Sun Jan 21 2007
    copyright            : (C) 2007 by Pedro Ferreira
                           (C) 2007 by Diederik van der Boor
    email                : pedro.ferreira@fe.up.pt
                           "vdboor" --at-- "codingdomain.com"
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef THUMBNAILPROVIDER_H
#define THUMBNAILPROVIDER_H

#include <QBuffer>
#include <QImage>
#include <QStringList>

#include <KUrl>


// Forward declarations
class KFileItem;



/**
 * @brief Utility class to generate thumbnails.
 *
 * The generated thumbnails are used for file transfer invitations.
 *
 * @author Pedro Ferreira, Diederik van der Boor
 * @ingroup Utils
 */
class ThumbnailProvider : public QObject
{
  Q_OBJECT

  public:
    // Constructor
    explicit               ThumbnailProvider( const QString &fileName, int size = 96 );
    // Return whether an image could be created
    bool                   isSuccessful() const;
    // Return the image data
    const QByteArray      &getData() const;
    // Return the image
    const QImage          &getImage() const;
    // Return the HTML image tag.
    QString                getImageTag( const QString &altText = QString() ) const;
    // Generate a fallback image.
    void                   generateFallbackImage();

  private slots:
    // Called when a preview is available.
    void                   slotGotPreview( const KFileItem& item, const QPixmap &preview );
    // Called when a preview won't be generated.
    void                   slotFailed();
    // Called when a preview could not be generated.
    void                   slotFailed( const KFileItem &item );

  private:
    // Store the image in the instance fields.
    void                   storeImage( const QImage &image );

  private:
    /// The list of enabled plugins
    QStringList            enabledPlugins_;
    /// The file name
    QString                fileName_;
    /// The file list (required to avoid KIO crashes)
    KUrl::List             fileList_;
    /// Whether an error occured
    bool                   resultError_;
    /// The requested size
    int                    size_;
    /// The thumbnail image
    QImage                 thumbnailImage_;
    /// The thumbnail data
    QByteArray             thumbnailData_;

  signals:
    /**
     * @brief Indicate the KIO preview job completed.
     */
    void                   gotResult();
};

#endif
