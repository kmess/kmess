/***************************************************************************
                          idletimer.h  -  description
                             -------------------
    begin                : Tue Oct 22 2002
    copyright            : (C) 2002 by Mike K. Bennett
    email                : mkb137b@hotmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef IDLETIMER_H
#define IDLETIMER_H

#include <QObject>


// Forward declarations
class XAutoLock;



/**
 * @brief Wrapper for XAutoLock to fire idle-events based on account settings.
 *
 * The XAutoLock class is used to detect whether the user is currently idle.
 * The updateWatcher() method reads the account settings to determine
 * whether the idle detection should be active.
 *
 * @author Mike K. Bennett
 * @ingroup Utils
 */
class IdleTimer : public QObject  {
   Q_OBJECT

  public:
    // The constructor
                     IdleTimer();
    // The destructor
                    ~IdleTimer();

  public slots:
    // Reset the watcher based on the account's settings.
    void             updateWatcher();

  private: // Private attributes
    // The idle detector
    XAutoLock       *watcher_;

  signals: // Signals
    // Signal user activity when the watcher detects it.
    void             activity();
    // Signal user inactivity when the watcher detects it.
    void             timeout();
};

#endif
