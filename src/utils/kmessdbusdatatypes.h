/***************************************************************************
                          kmessdbusdatatypes.h -  description
                             -------------------
    begin                : Thursday July 09 2008
    copyright            : (C) 2008 by Ruben Vandamme
    email                : vandammeru gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef KMESSDBUSDATATYPES_H
#define KMESSDBUSDATATYPES_H

#include <QMetaType>
#include <QString>
#include <QtDBus/QDBusArgument>

#define KMESSDBUS_REGISTER_DATATYPES\
  qDBusRegisterMetaType<DBusContact>();\
	qDBusRegisterMetaType<DBusContactList>();



class DBusContact {
  public:
    QString       friendlyName;
    QString       handle;
    int           status;
};

QDBusArgument       &operator<<( QDBusArgument &argument      , const DBusContact &entry );
const QDBusArgument &operator>>( const QDBusArgument &argument, DBusContact &entry       );

typedef QList<DBusContact> DBusContactList;

Q_DECLARE_METATYPE( DBusContact );
Q_DECLARE_METATYPE( DBusContactList );

#endif
