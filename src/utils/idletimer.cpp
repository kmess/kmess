/***************************************************************************
                          idletimer.cpp  -  description
                             -------------------
    begin                : Tue Oct 22 2002
    copyright            : (C) 2002 by Mike K. Bennett
    email                : mkb137b@hotmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "idletimer.h"

#include "../account.h"
#include "../kmessdebug.h"
#include "xautolock.h"



// The constructor
IdleTimer::IdleTimer()
: QObject()
{
  watcher_ = new XAutoLock();

  // Connect the timer to signal when the user is away.
  connect( watcher_, SIGNAL(  timeout() ),
           this,     SIGNAL(  timeout() ) );
  connect( watcher_, SIGNAL( activity() ),
           this,     SIGNAL( activity() ) );

  watcher_->stopTimer();
}



// The destructor
IdleTimer::~IdleTimer()
{
  delete watcher_;
#ifdef KMESSDEBUG_IDLETIMER
  kmDebug() << "DESTROYED.";
#endif
}



// Reset the watcher based on the account's settings.
void IdleTimer::updateWatcher()
{
#ifdef KMESSDEBUG_IDLETIMER
  kmDebug();
#endif

  watcher_->setTimeOut( Account::connectedAccount->getSettingInt( "AutoIdleTime" ) * 60 );
  if( Account::connectedAccount->getSettingBool( "AutoIdleEnabled" ) )
  {
#ifdef KMESSDEBUG_IDLETIMER
    kmDebug() << "Start the timer.";
#endif
    watcher_->startTimer();
  }
  else
  {
#ifdef KMESSDEBUG_IDLETIMER
    kmDebug() << "Stop the timer.";
#endif
    watcher_->stopTimer();
  }
#ifdef KMESSDEBUG_IDLETIMER
  kmDebug() << "Done updateWatcher().";
#endif
}

#include "idletimer.moc"
