 /***************************************************************************
                          xautolock.h  - user inactivity timer
                             -------------------
    begin                : Mon 01 Dec 2003
    copyright            : (C) 2002 by Michael Curtis
    email                : michael@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/


#ifndef XAUTOLOCK_H
#define XAUTOLOCK_H

#include <QTimer>

#ifdef HAVE_XSCREENSAVER
  #include <X11/Xlib.h>
  #include <X11/extensions/scrnsaver.h>
#else
  // prevent errors in compilation
  struct XScreenSaverInfo;
#endif

/**
 * @brief Idle detection using X11 extensions
 *
 * This code is based on XAutoLock from the KDE Project,
 * but heavily modified and rewritten since.
 *
 * @author Michael Curtis
 * @ingroup Utils
 */
class XAutoLock : public QObject
{
  Q_OBJECT

  public:
    // Constructor
    XAutoLock();
    // Destructor
    virtual ~XAutoLock();
    // Set the timeout
    void setTimeOut( unsigned int timeOut );
    // Start the idle timer
    void startTimer();
    // Stop the idle timer
    void stopTimer();
    // Get screen saver status by asking the MIT-SCREEN-SAVER extension (if available)
    bool isMitScreenSaverActive();
    // Get screen saver status by asking the KDE Screen Saver or X extensions
    bool isScreenSaverActive();

  public slots:
    // The main function which checks for idle status
    void checkIdle();

  signals:
    // Emitted when the user comes out of idle
    void activity();
    // Emitted when the user goes into idle
    void timeout();

  // Private Methods
  private:
    // Get idle time by detecting time since last mouse movement
    unsigned int getMouseIdle();
    // Get idle time by asking the MIT-SCREEN-SAVER extension (if available)
    unsigned long getMitIdle();
    // Reset the timer (after we've just become active again, for example)
    void resetTimer();

  private:
    // Are we timing?
    bool              active_;
    // Idle?
    bool              idle_;
    // Time since we last noticed we were idle
    unsigned int      idleTime_;
    // Time snice we last checked idle status (to prevent *strangeness*)
    unsigned int      lastCheck_;
    // Is the MIT-SCREEN-SAVER extension available?
    bool              mitAvailable_;
    // A block of data for the MIT-SCREEN-SAVER
    XScreenSaverInfo *mitInfo_;
    // The timer to check idle status periodically
    QTimer           *timer_;
    // Time of inactivity before we consider user idle
    unsigned int      triggerTime_;
};

#endif
