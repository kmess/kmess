/***************************************************************************
                          kmessdbusdatatypes.cpp -  description
                             -------------------
    begin                : Thursday July 09 2008
    copyright            : (C) 2008 by Ruben Vandamme
    email                : vandammeru gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "kmessdbusdatatypes.h"

QDBusArgument &operator << ( QDBusArgument &argument, const DBusContact &account )
{
	argument.beginStructure();
	argument << account.friendlyName
           << account.handle
           << account.status;
	argument.endStructure();

	return argument;
}



const QDBusArgument &operator >> ( const QDBusArgument &argument, DBusContact &account )
{
	argument.beginStructure();
	argument >> account.friendlyName
			     >> account.handle
			     >> account.status;
	argument.endStructure();

	return argument;
}
