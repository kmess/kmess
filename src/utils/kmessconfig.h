/***************************************************************************
                          kmessconfig.h  -  description
                             -------------------
    begin                : Thu May 1 2008
    copyright            : (C) 2008 by Antonio Nastasi
    email                : sifcenter@gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef KMESSCONFIG_H
#define KMESSCONFIG_H

#include <KConfig>
#include <KConfigGroup>

#include <QStringList>
#include <QHash>
#include <QTemporaryFile>

namespace KMess
{
  class MsnObject;
}


/**
 * @brief Central query location for config paths.
 *
 * @author Antonio Nastasi
 * @ingroup Utils
 */
class KMessConfig
{

  public: // Public methods
    // Return a singleton instance of the current config
    static KMessConfig *instance();
     // Delete the instance of the contact config
    static void         destroy();

    // Recursively delete an account's configuration directory and all of its contents
    bool                destroyConfigDir( const QString &fullDirPath );
    // Select a group from an account's general config
    KConfigGroup        getAccountConfig( const QString &accountHandle, const QString &groupName );
    // Return an account's data directory
    const QString       getAccountDirectory( const QString &accountHandle );
    // Return the accounts directory
    const QString       getAccountsDirectory();
    //Return a list of saved account handles
    QStringList         getAccountsList();
    // Select a group from an account's contact list config
    KConfigGroup        getContactListConfig( const QString &accountHandle, const QString &groupName );
    // Select a group from the application-wide config
    KConfigGroup        getGlobalConfig( const QString &groupName );
    // Return the path for an MsnObject in the cache.
    QString             getMsnObjectFileName(const KMess::MsnObject& msnObject);

  private: // Private methods
                        KMessConfig();
                       ~KMessConfig();
    // Return an invalid config file
    KConfigGroup        getInvalidConfig();
    // Warn the user about a configuration problem
    void                warnUser();

  private: // Private attributes
    static KMessConfig         *instance_;
    // A temporary config file to use on emergency
    QTemporaryFile             *tempConfigFile_;
};

#endif
