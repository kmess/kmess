/***************************************************************************
                          kmesssendmenuitem.h -  description
                             -------------------
    begin                : Wednesday July 10 2008
    copyright            : (C) 2008 by Ruben Vandamme
    email                : vandammeru gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/


#ifndef KMESSSENDMENUITEM_H
#define KMESSSENDMENUITEM_H

#include "../kmessdbusdatatypes.h"
#include <KAction>
#include <kactioncollection.h>
#include <QDBusInterface>



class KMessSendMenuItem: public KAction
{
  public: // Public methods
    // Constructor
                    KMessSendMenuItem( DBusContact contact, QDBusInterface *dbusInterface, KActionCollection* actionCollection );
    // Destructor
    virtual        ~KMessSendMenuItem();

  public: // Public attributes
    // Contains necesarry information about contact
    DBusContact     contact_;
    // DBus interface where to connect to
    QDBusInterface *dbusInterface_;
};

#endif
