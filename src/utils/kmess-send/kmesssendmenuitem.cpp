/***************************************************************************
                          kmesssendmenuitem.cpp -  description
                             -------------------
    begin                : Wednesday July 10 2008
    copyright            : (C) 2008 by Ruben Vandamme
    email                : vandammeru gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "kmesssendmenuitem.h"
#include "../../kmessdebug.h"
#include "../../contact/status.h"

#include <KMess/NetworkGlobals>

// Constructor
KMessSendMenuItem::KMessSendMenuItem( DBusContact contact, QDBusInterface *dbusInterface, KActionCollection* actionCollection )
 : KAction(
     *new KIcon( Status::getIcon( (KMess::MsnStatus)contact.status ) ),
                 contact.friendlyName.length() > 30 ? contact.friendlyName.left( 25 ).append( "..." ) : contact.friendlyName,
                 actionCollection
   ),
   contact_( contact ),
   dbusInterface_( dbusInterface )
{
#ifdef KMESSDEBUG_KMESSSENDPLUGIN
  kmDebug() << "Creating menuitem for" << contact.friendlyName << "with status" << contact.status;
#endif
}



// Destructor
KMessSendMenuItem::~KMessSendMenuItem()
{
#ifdef KMESSDEBUG_KMESSSENDPLUGIN
  kmDebug() << "DESTROYED.";
#endif
}
