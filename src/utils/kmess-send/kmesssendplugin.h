/***************************************************************************
                          kmesssendplugin.h -  description
                             -------------------
    begin                : Wednesday July 09 2008
    copyright            : (C) 2008 by Ruben Vandamme
    email                : vandammeru gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _KMESSSENDPLUGIN_H_
#define _KMESSSENDPLUGIN_H_

#include <konq_popupmenu.h>
#include <konq_popupmenuplugin.h>
#include <QStringList>



class QStringList;
class QDBusInterface;

class KMessSendPlugin : public KonqPopupMenuPlugin
{
   Q_OBJECT

public:
  // Constructor
  KMessSendPlugin (KonqPopupMenu *popupMenu, const QStringList & list);
  // Destructor
  virtual        ~KMessSendPlugin();
  // Setup the menu
  virtual void    setup( KActionCollection *actionCollection, const KonqPopupMenuInformation &popupMenuInfo, QMenu *menu );

private slots:
   // Send the selected files
   void           slotSendFile();

private:
   // Selected files
   QStringList    itemList_;
};

#endif
