/***************************************************************************
                          kmesstest.cpp  -  description
                             -------------------
    begin                : Sun Jan 5 2003
    copyright            : (C) 2003 by Mike K. Bennett
    email                : mkb137b@hotmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifdef __GNUC__
#warning Since SVN commit r5492, KMessTest is completely disabled. All tests should be rewritten as unit tests for the library or the application.
#endif

#include "kmesstest.h"

//#include "network/soap/httpsoapconnection.h"
//#include "network/soap/msnappdirectoryservice.h"
//#include "network/utils/upnp/manager.h"
//#include "network/connections/msnnotificationconnection.h"
//#include <KMess/OfflineMessage>
//#include <KMess/TextMessage>
//#include <KMess/MsnChat>
//#include <KMess/MsnContact>

#include "chat/chat.h"
#include "chat/chatmaster.h"
#include "chat/chatview.h"
#include "chat/chatwindow.h"
#include "chat/contactswidget.h"
#include "chat/xsltransformation.h"
#include "contact/contact.h"
#include "contact/contactextension.h"
#include "contact/group.h"
#include "dialogs/addcontactdialog.h"
#include "dialogs/chathistorydialog.h"
#include "dialogs/contactaddeduserdialog.h"
#include "dialogs/transferentry.h"
#include "dialogs/transferwindow.h"
#include "notification/contactstatusnotification.h"
#include "notification/systemtraywidget.h"
#include "settings/accountsettingsdialog.h"
#include "utils/nowlisteningclient.h"
#include "utils/richtextparser.h"
#include "accountsmanager.h"
#include "emoticonmanager.h"
#include "kmessglobal.h"
#include "kmessapplication.h"
#include "kmessview.h"
#include "mainwindow.h"

#include <QTime>
#include <QFile>
#include <QImage>

#include <KMessageBox>



// The constructor
KMessTest::KMessTest( MainWindow *mainWindow )
 : QObject( mainWindow )
 , quitAfterTest_(false)
 , mainWindow_( mainWindow )
{
  // Used to quit. Could also be useful to the tests
  kmessApp_ = KMessApplication::instance();

  setObjectName("KMessTest");
}



// The destructor
KMessTest::~KMessTest()
{
  kmDebug() << "DESTROYED";
}



#define  TESTCASE(name, action) testcases.append( name ); if( testName == name ) { action; return; }


/**
 * The main testing function.
 * Runs a given test based on a command line argument.
 */
void KMessTest::runTest( const QString &testName )
{
  // Add the tests, which can be run from debug builds using:
  // ./kmess --runtest <name>

  kmDebug() << "Running test:" << testName;
  kmDebug() << "----------------------------------------";
  kmDebug();

  QStringList testcases;

  TESTCASE( "transferwindow",      testTransferWindow() );
  TESTCASE( "chathistory",         testChatHistoryDialog() );
  TESTCASE( "chatwindow",          testChatWindow() );
  TESTCASE( "challenge",           testChallengeHandler() );
  TESTCASE( "crash",               testCrash() );
  TESTCASE( "addcontact",          testAddContactDialog() );
  TESTCASE( "contactaddeduser",    testContactAddedUserDialog() );
  TESTCASE( "msnplus",             testMsnPlus() );
  TESTCASE( "msnplusinteractive",  testMsnPlusInteractive() );
  TESTCASE( "parserbenchmark",     benchmarkParser() );
  TESTCASE( "ns",                  testNotificationConnection() );
  TESTCASE( "contactlist",         testContactList() );
  TESTCASE( "connect",             testConnect() );
  TESTCASE( "settings",            testSettings() );
  TESTCASE( "sb",                  testSwitchboardConnection() );
  TESTCASE( "notifications",       testNotifications() );
  TESTCASE( "nowlistening",        testNowListening() );
  TESTCASE( "soap",                testSoapConnection() );
  TESTCASE( "upnp",                testUpnpConnection() );
  TESTCASE( "xslt",                testXslTransformation() );
  TESTCASE( "systemtray",          testSystemTray() );
  TESTCASE( "offlinemessages",     testOfflineMessages() );
  qSort( testcases );
  kmDebug() << "Test case not found, available cases:";
  kmDebug() << testcases.join( ", " );

  quitAfterTest_ = true;
}



// Cleans up and schedules quitting kmess if necessary
void KMessTest::endTest()
{
  kmDebug();
  kmDebug() << "----------------------------------------";

  if( quitAfterTest_ )
  {
    // We can't quit (and also clean up the memory) now, since
    // the event loop hasn't started yet. Delay it a bit.
    kmDebug() << "Test case ended, quitting KMess.";

    kmessApp_->setQuitSelected( true );
    QTimer::singleShot( 1000, mainWindow_, SLOT( menuQuit() ) );
  }
}



void KMessTest::testChatHistoryDialog()
{
  testGroupsAndContacts();
  // TODO: add some temporary chat history for this account?

  QPointer<ChatHistoryDialog> dialog = new ChatHistoryDialog( mainWindow_ );

//   dialog->setContact( "test@test.com" );
  dialog->exec();

  if( dialog != 0 )
  {
    quitAfterTest_ = true;
  }
}



void KMessTest::testTransferWindow()
{
  int id;
  KIconLoader *loader = KIconLoader::global();
  TransferWindow *instance = TransferWindow::getInstance();

  id = instance->addEntry( "Filename1", 1234567890, true, QImage( loader->iconPath( "list-add", 48, false ) ) );
  kmDebug() << "Added ID: " << id;

  id = instance->addEntry( "Filename2", 34567890, false, QImage( loader->iconPath( "list-remove", 48, false ) ) );
  kmDebug() << "Added ID: " << id;

  id = instance->addEntry( "Filename3", 567890, true );
  kmDebug() << "Added ID: " << id;

//   TransferWindow::destroy();
}


void KMessTest::testChatWindow()
{
  testGroupsAndContacts();

  ChatWindow *window = new ChatWindow;
  window->initialize();

  // Contact-started chat
  if( 1 )
  {
    KMess::MsnChat *msnChat = globalSession->createMsnChat( "contact7@kmess.org" );
    msnChat->slotContactJoinedChat( "contact7@kmess.org" );

    Chat *chat = new Chat;
    chat->setMsnChat( msnChat );
    window->addChatTab( chat, true );
    window->show();

    KMess::TextMessage *message = new KMess::TextMessage( msnChat, 0, true );
    message->setMessage( "Testing incoming chats! :D :D\r\n" );

    msnChat->gotMessage( message );
  }
#if 0
    // User-started chat
  if( 0 )
  {
    MsnSwitchboardConnection *sb1 = new MsnSwitchboardConnection( "contact1@kmess.org" );
    globalSession->chatMaster_->switchboardConnections_.append( sb1 );
    Chat-gone-Information chatInfo1( "contact1@kmess.org", Chat-gone-Information::CONNECTION_OFFLINE );
    sb1->start( chatInfo1 );

    globalSession->chatMaster_->createChat( sb1 );
//     sb1->parseChatMessage( "contact1@kmess.org", "Contact 1", "", message );
  }

  // Contact-started group chat
  if( 0 )
  {
    MsnSwitchboardConnection *sb3 = new MsnSwitchboardConnection( "contact2@kmess.org" );
    Chat-gone-Information chatInfo3( "contact2@kmess.org", "127.0.0.1", 1234, "5678.8765", "-1", Chat-gone-Information::CONNECTION_OFFLINE );
    sb3->start( chatInfo3 );
    globalSession->chatMaster_->switchboardConnections_.append( sb3 );
    sb3->contactJoined( "contact1@kmess.org", "Contact 1", 0 );
    sb3->contactJoined( "contact2@kmess.org", "Contact 2", 0 );
    sb3->contactJoined( "contact3@kmess.org", "Contact 3", 0 );
    sb3->contactJoined( "contact4@kmess.org", "Contact 4", 0 );
    sb3->contactJoined( "contact5@kmess.org", "Contact 5", 0 );
    sb3->contactJoined( "contact6@kmess.org", "Contact 6", 0 );
    sb3->contactJoined( "contact7@kmess.org", "Contact 7", 0 );
    sb3->contactJoined( "contact8@kmess.org", "Contact 8", 0 );
    sb3->contactJoined( "contact9@kmess.org", "Contact 9", 0 );
    sb3->contactJoined( "contact10@kmess.org", "Contact 10", 0 );

    globalSession->chatMaster_->createChat( sb3 );
    sb3->parseChatMessage( "contact2@kmess.org", "Contact 2", "", message );
    sb3->parseChatMessage( "contact5@kmess.org", "Contact 5", "", message );
  }

  // Wink
  if( 0 )
  {
    MsnSwitchboardConnection *sb4 = new MsnSwitchboardConnection( "contact7@kmess.org" );
    sb4 = globalSession->chatMaster_->createSwitchboardConnection( sb4, "contact7@kmess.org" );
  //    globalSession->chatMaster_->switchboardConnections_.append( sb4 );
    Chat-gone-Information chatInfo4( "contact7@kmess.org", "127.0.0.1", 1234, "5678.8765", "-1", ChatInformation::CONNECTION_CHAT );
    sb4->start( chatInfo4 );
    sb4->contactJoined( "contact7@kmess.org", "Contact 7", 0 );
    Chat *chat = globalSession->chatMaster_->createChat( sb4 );

    // This is the wink "guitar smash"
    MimeMessage winkMessage( QString(
      "MIME-Version: 1.0\r\nContent-Type: text/x-msnmsgr-datacast\r\n\r\nID: 2\r\nData: <msnobj Creator=\""
      "contact7@kmess.org\" Size=\"35574\" Type=\"8\" Location=\"AzBXDxf+Tcdvcans2TCRnoXh_jg=\" Friendly="
      "\"dw==\" SHA1D=\"AzBXDxf+Tcdvcans2TCRnoXh/jg=\" SHA1C=\"5+JhvDZcpUpXNSkYttmciHP7a1U=\" stamp=\""
      "MIIInQYJKoZIhvcNAQcCoIIIjjCCCIoCAQExCzAJBgUrDgMCGgUAMCwGCSqGSIb3DQEHAaAfBB1BekJYRHhmK1RjZHZjYW5zMlR"
      "DUm5vWGgvamc9P6CCBrQwggawMIIFmKADAgECAgoFFrAoAAEAAAAJMA0GCSqGSIb3DQEBBQUAMHwxCzAJBgNVBAYTAlVTMRMwEQ"
      "YDVQQIEwpXYXNoaW5ndG9uMRAwDgYDVQQHEwdSZWRtb25kMR4wHAYDVQQKExVNaWNyb3NvZnQgQ29ycG9yYXRpb24xJjAkBgNVB"
      "AMTHU1TTiBDb250ZW50IEF1dGhlbnRpY2F0aW9uIENBMB4XDTA1MDIyNTE4MDM0OFoXDTA1MDMxNTE4MTM0OFowUTESMBAGA1UE"
      "ChMJTWljcm9zb2Z0MQwwCgYDVQQLEwNNU04xLTArBgNVBAMTJDZhMjRkMTEwLWM2YmQtNDc2Ni1iMGNkLTU4YTBlMzc5NDlhNzC"
      "BnzANBgkqhkiG9w0BAQEFAAOBjQAwgYkCgYEA0NIN35EKdW/q7ZaDI7IasI6m0T/ta19mxTBsyBA6C10NZ7TRM86QXw5WisYKBx"
      "S3jbupgo2D5hTHd4c7MStRM0efUgJDLTyzM7Kojem+FF16FYgEYaOeI2pSFZhP5I731B0k5jRwzMpQ6wlKg86diKv8PFUwOiOws"
      "41gfjCymvcCAwEAAaOCA+EwggPdMB0GA1UdDgQWBBTyHJ1dnGaT1pVQmP/YelVTNvYmMTAfBgNVHSUEGDAWBggrBgEFBQcDCAYK"
      "KwYBBAGCNzMBAzCCAksGA1UdIASCAkIwggI+MIICOgYJKwYBBAGCNxUvMIICKzBJBggrBgEFBQcCARY9aHR0cHM6Ly93d3cubWl"
      "jcm9zb2Z0LmNvbS9wa2kvc3NsL2Nwcy9NaWNyb3NvZnRNU05Db250ZW50Lmh0bTCCAdwGCCsGAQUFBwICMIIBzh6CAcoATQBpAG"
      "MAcgBvAHMAbwBmAHQAIABkAG8AZQBzACAAbgBvAHQAIAB3AGEAcgByAGEAbgB0ACAAbwByACAAYwBsAGEAaQBtACAAdABoAGEAd"
      "AAgAHQAaABlACAAaQBuAGYAbwByAG0AYQB0AGkAbwBuACAAZABpAHMAcABsAGEAeQBlAGQAIABpAG4AIAB0AGgAaQBzACAAYwBl"
      "AHIAdABpAGYAaQBjAGEAdABlACAAaQBzACAAYwB1AHIAcgBlAG4AdAAgAG8AcgAgAGEAYwBjAHUAcgBhAHQAZQAsACAAbgBvAHI"
      "AIABkAG8AZQBzACAAaQB0ACAAbQBhAGsAZQAgAGEAbgB5ACAAZgBvAHIAbQBhAGwAIABzAHQAYQB0AGUAbQBlAG4AdABzACAAYQ"
      "BiAG8AdQB0ACAAdABoAGUAIABxAHUAYQBsAGkAdAB5ACAAbwByACAAcwBhAGYAZQB0AHkAIABvAGYAIABkAGEAdABhACAAcwBpA"
      "GcAbgBlAGQAIAB3AGkAdABoACAAdABoAGUAIABjAG8AcgByAGUAcwBwAG8AbgBkAGkAbgBnACAAcAByAGkAdgBhAHQAZQAgAGsA"
      "ZQB5AC4AIDALBgNVHQ8EBAMCB4AwgaEGA1UdIwSBmTCBloAUdeBjdZAOPzN4/ah2f6tTCLPcC+qhcqRwMG4xCzAJBgNVBAYTAlV"
      "TMRMwEQYDVQQIEwpXYXNoaW5ndG9uMRAwDgYDVQQHEwdSZWRtb25kMR4wHAYDVQQKExVNaWNyb3NvZnQgQ29ycG9yYXRpb24xGD"
      "AWBgNVBAMTD01TTiBDb250ZW50IFBDQYIKYQlx2AABAAAABTBLBgNVHR8ERDBCMECgPqA8hjpodHRwOi8vY3JsLm1pY3Jvc29md"
      "C5jb20vcGtpL2NybC9wcm9kdWN0cy9NU05Db250ZW50Q0EuY3JsME4GCCsGAQUFBwEBBEIwQDA+BggrBgEFBQcwAoYyaHR0cDov"
      "L3d3dy5taWNyb3NvZnQuY29tL3BraS9jZXJ0L01TTkNvbnRlbnRDQS5jcnQwDQYJKoZIhvcNAQEFBQADggEBAHDQBEIIaOolQ2d"
      "E9BH/5LHdW9r+j2kW5n77eXxVimhSVKrFSPzTSjAdtSt0Co+88nXpcJABZq+v1sIVQLFh79hJyyXes1tokNE4aqdpsI6/Eqzrfs"
      "5LtTxPmGqxPTP5EaEKQTlCX1S7j2hosLAumG6SvrCNWpqNDXk34ZOzMfJkxBAHK85QTzX7vEgQWkCCygS88VkqXO78L2bFk5vw/"
      "vxhqfSBskEmn35U3apsEfzJTm4QPpE+WveskYLZUkg3PTMEpS+53+ZnvAk40rbzJhhdO8lfFCL4RNU9igYa8TiRznuxQ7Fq+Pcl"
      "PGUVa7onZafyGy1zMbIgKWiWX7A5TqIxggGQMIIBjAIBATCBijB8MQswCQYDVQQGEwJVUzETMBEGA1UECBMKV2FzaGluZ3RvbjE"
      "QMA4GA1UEBxMHUmVkbW9uZDEeMBwGA1UEChMVTWljcm9zb2Z0IENvcnBvcmF0aW9uMSYwJAYDVQQDEx1NU04gQ29udGVudCBBdX"
      "RoZW50aWNhdGlvbiBDQQIKBRawKAABAAAACTAJBgUrDgMCGgUAoF0wGAYJKoZIhvcNAQkDMQsGCSqGSIb3DQEHATAcBgkqhkiG9"
      "w0BCQUxDxcNMDUwMzA3MjAxMTQ4WjAjBgkqhkiG9w0BCQQxFgQUBwu2BOj5y3gX2kZjt+EkLKEnUI4wDQYJKoZIhvcNAQEBBQAE"
      "gYBODy/vmAxlqGaFxjpUop5d3D0biHIFlo2TmnOJBIkRREq4MtKkqjfYLtrNSefGKpxrbqQz2vT78bSedGktHvoqmHwR2F/IzcM"
      "qaANFOJd5MeXbjCloHbYxgK9fcMV0eEYyPRARSQbb1FdkQMeaMxoWJy6q63TD7RAMgcrQeLJobA==\"/>\r\n" ) );
    chat->getChatWindow()->resize( 800, 650 );
    sb4->parseMimeMessage( QStringList()<<"MSG"<<"contact7@kmess.org"<<"%5Bb%5D%5Bc%3D%2333A%5DVale%E2%84%A2%5B%2Fc%3D%233A3%5D%5B%2Fb%5D"<<"1523", winkMessage );
  }

#endif

  QTimer *test = new QTimer(this);
  connect(test, SIGNAL(timeout()), this, SLOT(testChatWindowSlot()));
  connect(mainWindow_->actionDisconnect_, SIGNAL(triggered(bool)), test, SLOT(stop()));
//   test->start( 6000 );
}



void KMessTest::testChatWindowSlot()
{
  if( KMessApplication::instance()->chatMaster()->msnChats_.isEmpty() )
  {
    QTimer *timer = static_cast<QTimer*>( sender() );
    if( timer ) timer->stop();
    return;
  }

  static int i = 0;
//   ContactList *list = mainWindow_->msnNotificationConnection_->contactList_;
//   list->getContact( "contact1@kmess.org" )->setPersonalStatus( QString("nuovo pm %1" ).arg( i++) );

//   globalSession->chatMaster_->chats_.first()->slotReceivedNudge( globalSession->getContactList()->getContact( "contact1@kmess.org" ) );
//   globalSession->chatMaster_->chats_.first()->contactLeft( globalSession->getContactList()->getContact( "contact1@kmess.org" ),false );

  KMess::MsnChat *msnchat = KMessApplication::instance()->chatMaster()->msnChats_.first();
  if( msnchat == 0 )
  {
    // KMess should be quitting...
    return;
  }

  KMess::TextMessage *message = new KMess::TextMessage( msnchat, 0, true );
  message->setMessage( QString( "Testing incoming chats %1" ).arg( i++ ) );

  msnchat->gotMessage( message );
}



void KMessTest::testCrash()
{
  QObject *object = 0;
  object->objectName();
}


void KMessTest::testAddContactDialog()
{
  QString handle;
  QStringList groupsId;
  new AddContactDialog( handle, groupsId );

  kmDebug() << "adding " << handle << "on groups: " << groupsId;
}



void KMessTest::testChallengeHandler()
{
#ifdef __GNUC__
#warning TODO: this should be a library unittest
#endif
#if 0
  MSNChallengeHandler handler;
  // Tested with
  // PRODUCT_KEY "ILTXC!4IXB5FB*PX"
  // PRODUCT_ID  "PROD0119GSJUC$18"
  kmDebug() << handler.computeHash( "TEST"); // "299ef8849928ab29512748d378cb93b0"
  kmDebug() << handler.computeHash( "KMessTest"); // "6e7fbd3c1457c4192d18bc9d19ef4e3e"
  kmDebug() << handler.computeHash( "KMessKMessKmess"); // "736d5611bdd0f6f60b93343e169c6192"
#endif
}



void KMessTest::testContactAddedUserDialog()
{
  //set up groups to populate listwidget
  testGroupsAndContacts();

  ContactAddedUserDialog *dialog = new ContactAddedUserDialog( "someone@hotmail.com", "SomeNickName" );

  connect( dialog, SIGNAL(                     userChoice(QString,QStringList,int) ),
           this,   SLOT  ( testContactAddedUserDialogSlot(QString,QStringList,int) ) );

  dialog->show();
}


void KMessTest::testContactAddedUserDialogSlot( const QString &handle, const QStringList &groupsId, const int code )
{
  switch( (ContactAddedUserDialog::ReturnCode) code )
  {
    case ContactAddedUserDialog::ADD:
      kmDebug() << "result: Add contact" << handle;
      kmDebug() << "result: Adding in groups" << groupsId;
      break;

    case ContactAddedUserDialog::ALLOW:
      kmDebug() << "result: Allow contact" << handle;
      break;

    case ContactAddedUserDialog::BLOCK:
      kmDebug() << "result: Block contact" << handle;
      break;

    default:
      kmDebug() << "result: undefined, contact:" << handle;
      break;
  }
}


void KMessTest::testNotifications()
{
  testGroupsAndContacts();

  ContactList *list = globalSession->msnNotificationConnection_->contactList_;
  Contact *contact1 = list->getContact( "contact1@kmess.org" );
  Contact *contact2 = list->getContact( "contact2@kmess.org" );
  Contact *contact3 = list->getContact( "contact3@kmess.org" );

  contact1->setStatus( KMess::OnlineStatus  );
  contact2->setStatus( KMess::OfflineStatus );
  contact3->setStatus( KMess::BusyStatus    );

  ContactStatusNotification *cs = new ContactStatusNotification( NotificationManager::instance() );
  cs->notify( contact1, true );
  cs->notify( contact2, true );
  cs->notify( contact3, true );
}


void KMessTest::testNowListening()
{
  NowListeningClient *client = new NowListeningClient();
  client->setEnabled( true );
}


void KMessTest::testNotificationConnection()                { quitAfterTest_ = true; }
void KMessTest::testConnect()                               { quitAfterTest_ = true; }
void KMessTest::testSettings()                              { quitAfterTest_ = true; }
void KMessTest::testSwitchboardConnection()                 { quitAfterTest_ = true; }



void KMessTest::testGroupsAndContacts()
{
  Account *account = new Account();
  account->setGuestAccount( true );
  account->setHandle("test@kmess.org");
  // We can't set an initial personal message or friendly name here, we have
  // to do that below using globalSession.
  Account::connectedAccount = account;

  AccountsManager::instance()->addAccount( Account::connectedAccount );
  EmoticonManager::instance()->loadThemes( Account::connectedAccount->getHandle() );

  //Account::connectedAccount->setLoginInformation("test@kmess.org", "Testing Account", "test" );
  SettingsList sessionSettings;
  sessionSettings[ "EmailEnabled"  ] = true;
  sessionSettings[ "AccountHandle" ] = Account::connectedAccount->getHandle();
  globalSession->setSessionSettings( sessionSettings );
  globalSession->setDisplayName( "KMess is running a test!" );
  globalSession->setPersonalMessage( "omg! this [u]rocks![/u] :D " );
  globalSession->setStatus( KMess::PhoneStatus );
  globalSession->setSessionSetting( "EmailCount", 1000 );

  SettingsList settings;
  settings[ "AutoIdleEnabled"         ] = true;
  settings[ "AutoIdleTime"         ] = 5;
  settings[ "NotificationsHideWhenBusy"         ] = true;
  settings[ "ContactListPictureSize"   ] = 32;
  settings[ "ContactListBackgroundEnabled"    ] = true;
  settings[ "NowListeningEnabled"      ] = true;
  settings[ "ContactListEmailAsNamesShow" ] = false;
  settings[ "ContactListTextFormattingEnabled"         ] = true;
  settings[ "ContactListDisplayMode"   ] = Account::VIEW_MIXED; // VIEW_MIXED VIEW_BYSTATUS VIEW_BYGROUP
  settings[ "ContactListAllowedGroupShow"    ] = false;
  settings[ "ContactListEmptyGroupsShow"      ] = false;
  settings[ "ContactListOfflineGroupShow" ] = true;
  settings[ "ContactListRemovedGroupShow"         ] = false;
  settings[ "ChatUserDisplayPictureEnabled"   ] = true;
  settings[ "FontUser"    ] = QFont( "Monospace", 9 );
  settings[ "FontUserColor"      ] = "#FF9900";
  settings[ "FontContact" ] = QFont( "Serif", 12 );
  settings[ "FontContactColor"      ] = "#99FF00";
  settings[ "ChatSessionMessagesEnabled" ] = true;
  settings[ "FontContactForce"      ] = true;
  settings[ "EmoticonsEnabled" ] = true;
  settings[ "ChatShowWinks"      ] = true;
  settings[ "FontEffectsEnabled" ] = true;
  settings[ "ChatTextFormattingEnabled"      ] = true;
  settings[ "ChatShakeNudge" ] = true;
  settings[ "ChatTimestampEnabled"      ] = true;
  settings[ "ChatTimestampDateEnabled" ] = false;
  settings[ "ChatTimestampSecondsEnabled"      ] = true;
  settings[ "ChatGroupFollowUpMessagesEnabled" ] = true;
  settings[ "ChatTabbingMode" ] = 0;
  settings[ "ChatStyle" ] = "Fresh";
  settings[ "LoggingEnabled"         ] = false;
  settings[ "LoggingToFileEnabled"   ] = false;
  settings[ "LoggingToFileFormat"    ] = Account::EXPORT_HTML;
  settings[ "LoggingToFilePath"      ] = "/tmp/kmess-test";
  settings[ "LoggingToFileStructure" ] = Account::SINGLEDIRECTORY;
  settings[ "WelcomeDialogShown" ] = true;
  Account::connectedAccount->setSettings( settings );

  KMessApplication::instance()->nowListeningClient_->setEnabled( false );

  Group *group;
  Contact *contact;
  ContactList *list = globalSession->msnNotificationConnection_->contactList_;

  list->reset( true );

  group = list->addGroup( "friends", "My Friends" );
  group = list->addGroup( "work", "Work Mates" );
  group->setExpanded( false );
  group = list->addGroup( "family", "Family" );
  group = list->addGroup( "empty", "Empty Group");

  list->getGroupById( SpecialGroups::INDIVIDUALS )->setExpanded( false );
//   list->getGroupById( SpecialGroups::OFFLINE )->setExpanded( false );

  contact = list->addContact( "contact1@kmess.org", "Contact 1", 11, QStringList()<<"friends"<<"work", "c1" );
  contact->manageEmoticonBlackList( true, "lol" );
  contact->manageEmoticonBlackList( true, "&lt;aaaa" );
  contact->manageEmoticonBlackList( true, "&#39;aaaasdsdfa" );

  // Pending emoticons
  contact->addEmoticonDefinition( "-pend", "1hashhashhash" );
  contact->addEmoticonDefinition( "-ing", "2shhashhashha" );

  // Received emoticons
  contact->addEmoticonDefinition( "-rece", "3ashhashhashh" );
  contact->addEmoticonDefinition( "-ived", "4hhashhashhas" );
  contact->addEmoticonFile( "3ashhashhashh", KIconLoader::global()->iconPath( "user-online", KIconLoader::Small ) );
  contact->addEmoticonFile( "4hhashhashhas", KIconLoader::global()->iconPath( "user-busy", KIconLoader::Small ) );

  list->changeContactStatus( "contact1@kmess.org", KMess::OnlineStatus, "[c=red]Contact 1[/c] :) <one&amp;two!> & three four five six seven", 3, QString(), false );
  contact->setPersonalStatus( "Contact 1 :) -- www.google.it - <sdf> [u]adgdf dfgfsdf sdgf hgegagdsf dfdfg[/u] asd &amp;" );
  contact = list->addContact( "contact2@kmess.org", "Contact 2", 3, QStringList()<<"family", "c2" );
  list->changeContactStatus( "contact2@kmess.org", KMess::OfflineStatus, QString::fromUtf8("انتقدت منظمة حقوقيتقدت منظمة حقوقيتقدت منظمة حقوقيتقدت منظمة حقوقيتقدت منظمة حقوقية"), 3, QString(), false );
  contact = list->addContact( "contact3@kmess.org", "Contact 3", 3, QStringList(), "c3" );
  contact = list->addContact( "contact4@kmess.org", "Contact 4", 3, QStringList(), "c4" );
  list->changeContactStatus( "contact4@kmess.org", KMess::BusyStatus, "Contact 4", 3, QString(), false );
  contact = list->addContact( "contact5@kmess.org", "Contact 5", 3, QStringList()<<"work", "c5" );
  list->changeContactStatus( "contact5@kmess.org", KMess::PhoneStatus, "Contact 5", 3, QString(), false );
  contact->setPersonalStatus( "Contact 5 Status" );
  contact = list->addContact( "contact6@kmess.org", "Contact 6 www.google.it", 3, QStringList()<<"work"<<"friends", "c6" );
  list->changeContactStatus( "contact6@kmess.org", KMess::OfflineStatus, "Contact 6 www.google.it", 3, QString(), false );
  contact = list->addContact( "contact7@kmess.org", "<b> Contact 7 </b>", 3, QStringList()<<"friends", "c7" );
  list->changeContactStatus( "contact7@kmess.org", KMess::AwayStatus, "<b> Contact 7 </b>", 1342210084, QString(), false );
  contact->setPersonalStatus( "Contact 7 Status", "Music", "KMess - The KMess Song" );
  contact = list->addContact( "contact8@kmess.org", "Contact 8", 3, QStringList(), "c8" );
  list->changeContactStatus( "contact8@kmess.org", KMess::OnlineStatus, "Contact 8", 3, QString(), false );
  contact = list->addContact( "contact9@kmess.org", "Contact 9", 3, QStringList()<<"work", "c9" );
  list->changeContactStatus( "contact9@kmess.org", KMess::BusyStatus, "Contact 9", 3, QString(), false );
  contact = list->addContact( "contact10@kmess.org", "Contact 10", 3, QStringList()<<"work", "c10" );
  list->changeContactStatus( "contact10@kmess.org", KMess::OfflineStatus, "Contact 10", 3, QString(), false );
}



void KMessTest::testContactList()
{
  testGroupsAndContacts();

//   ContactList *list = mainWindow_->msnNotificationConnection_->contactList_;
//   list->dump();


  mainWindow_->loggedIn();
  mainWindow_->window()->show();
  mainWindow_->systemTrayWidget_->statusChanged();

  // Show the Now Listening field
//   mainWindow_->view_->changedSong( "White Stripes, The", "Elephant", "Seven Nation Army", true );
  mainWindow_->view_->slotUpdateEmailDisplay();
  mainWindow_->view_->slotUpdateUserStatus();

  QTimer *test = new QTimer(this);
  connect(test, SIGNAL(timeout()), this, SLOT(testContactListSlot()));
  connect(mainWindow_->actionDisconnect_, SIGNAL(triggered(bool)), test, SLOT(stop()));
//   test->start( 5000 );
}


// A generic method to use as throwaway slot for delayed code
void KMessTest::testContactListSlot()
{
  kmDebug() << "TEST - Switching contact status";
  ContactList *list = globalSession->msnNotificationConnection_->contactList_;

  static int i=0;
  static bool statusSwitch = true;
  KMess::MsnStatus first = KMess::AwayStatus, second = KMess::OfflineStatus;

/*
  if( i == 1 )
  {
    QTimer *timer = static_cast<QTimer*>( sender() );
    if( timer ) timer->stop();
  }
*/

  if( statusSwitch )
  {
    qSwap( first, second );
  }
  statusSwitch = ! statusSwitch;
  i++;

  if( i == 3 )
    list->addContact( "contact11@kmess.org", "Contact 11", 3, QStringList(), "c11" );
  if( i == 5 )
    list->changeContactStatus( "contact11@kmess.org", KMess::BrbStatus, "Contact 11", 3, QString(), false );

  list->getContact( "contact5@kmess.org")->setPersonalStatus( QString("Contact 5 Status: %1").arg( i ), "Music", QString("Chemical Brothers - Got Glint? %1").arg( i ) );
  list->getContact( "contact8@kmess.org")->setPersonalStatus( QString("Contact 8 Status: %1").arg( i ) );
  list->changeContactStatus( "contact5@kmess.org", first, QString("Contact 5 name #%1").arg( i ), 3, QString(), true );
  list->changeContactStatus( "contact8@kmess.org", second, QString("Contact 8 name #%1").arg( i ), 3, QString(), true );
}



void KMessTest::testMsnPlus()
{
  quitAfterTest_ = true;

  RichTextParser *parser = RichTextParser::instance();
  QStringList strings;

  strings << QString::fromUtf8( "[c=#0080FF][u][c=39][b](S) [c=29] Fede  [/c](S)[/b][/c=39] forza azzurriiiiiii[/u][/c]" );
  strings << QString::fromUtf8( "[c=50][a=50]Opera[/a][/c][c=0][a=0]tore ec[/a][/c][c=4][a=4]ologico[/a][/c]" );
  strings << QString::fromUtf8( "[c=4]t[a=#0000]est[/c=44][/a]" );
  strings << QString::fromUtf8( "[c=3]te[a=2]st:)[/a=12]er[/c=4]" );
  strings << QString::fromUtf8( "[a=red]test[a=green]:DeroWithCo[/a=blue]lorssadfasd dfg sdfsdaf sdf [/a=yellow]");
  strings << QString::fromUtf8( "[c=#660000],.-~*'¨¯¨'*·~-.¸-).[i]Simo[/i].(-,.-~*'¨¯¨'*·~-.¸[/c=#00FFF]" );
  strings << QString::fromUtf8( "[b][a=1][c=4]- Emet -:) [/c][/a][/b]" );
  strings << QString::fromUtf8( "[b][a=1][c=10]- :) [T]ester <b>boldExploit</b> :P -[/c][/a][/b]" );
  strings << QString::fromUtf8( "[b][c=green]Vale - New kid on the block! :D[/c=blue][/b]" );
 // strings << QString::fromUtf8( "�$#F37AF0Vanitosa �$ �$#0A79F5Da strega cattiva a principessa..." );
  foreach( QString original, strings )
  {
    QString cleaned = parser->getCleanString( original );
    QString str     = parser->parseMsnString( original,
        ( RichTextParser::FormattingOptions )( RichTextParser::PARSE_EMOTICONS | RichTextParser::EMOTICONS_SMALL | RichTextParser::PARSE_LINKS | RichTextParser::PARSE_MSNPLUS ) );
    str             = parser->getFormattedString( str );

    KMessageBox::information( mainWindow_, "<html>" + original + "<br><br>" + cleaned  + "<br><br>" + str  + "</html>" );
  }
}



void KMessTest::benchmarkParser()
{
  quitAfterTest_ = true;

  QString str, str2, str3;
  QStringList strings;
  QTime startTime;
  int i, timeTaken, olds, oldms, old2s, old2ms, news, newms, oldtotal = 0, old2total = 0, newtotal = 0;
  const int timesToBenchmark = 50000;

  RichTextParser *parser = RichTextParser::instance();

  strings << QString::fromUtf8( "This string contains nothing special, but it is quite long nonetheless. The idea is "
                                "that nothing is changed in this string, so the parser reading efficiency is checked." );
  strings << QString::fromUtf8( "[c=#0080FF][u][c=39][b](S) [c=29] Fede  [/c](S)[/b][/c=39] forza azzurriiiiiii[/u][/c]" );
  strings << QString::fromUtf8( "[b][a=1][c=10]- :) [T]ester <b>boldExploit</b> :P -[/c][/a][/b]" );
  strings << QString::fromUtf8( ":D:D:D:D:D:D:D:D:D:D:D:D:D:D:D:D:D:D:D:D:D:D:D:D:D:D:D:D:D:D:D:D:D:D:D:D:D:D:D:D:D:D"
                                ":D:D:D:D:D:D:D:D:D:D:D:D:D:D:D:D:D:D:D:D:D:D:D:D:D:D:D:D:D:D:D:D:D:D:D:D:D:D:D:D:D:D"
                                ":D:D:D:D:D:D:D:D:D:D:D:D:D:D:D:D:D:D:D:D:D:D:D:D:D:D:D:D:D:D:D:D:D:D:D:D:D:D:D:D:D:D" );
  strings << QString::fromUtf8( "This is a looooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo"
                                "oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo"
                                "ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooong word" );
  strings << QString::fromUtf8( "http://kmess.org/ http://kmess.org/ http://kmess.org/ http://kmess.org/ http://kmess.org/" );
  strings << QString::fromUtf8( "dazjorz.com javascript.js test.nl http://google.com/ test@example.com");
  strings << QString::fromUtf8( "www.google.com google.com bugs@kmess.org http://www.kmess.org http://www.kmess.org/weird_url-with?random&stuff");
  foreach( QString origstr, strings )
  {
    timeTaken = 0;
    kmDebug() << timesToBenchmark << "times, string:" << origstr;

    startTime = QTime::currentTime();
    for( i = 0; i < timesToBenchmark; ++i)
    {
      str3 = parser->parseMsnString( origstr,
        ( RichTextParser::FormattingOptions )( RichTextParser::PARSE_EMOTICONS | RichTextParser::EMOTICONS_SMALL | RichTextParser::PARSE_LINKS | RichTextParser::PARSE_MSNPLUS ) );
    }
    timeTaken = QTime::currentTime().msecsTo( startTime );
    newtotal += (-timeTaken);
    news      = (-timeTaken) / 1000;
    newms     = (-timeTaken) % 1000;
    kmDebug() << "New parser:" << news << "s" << newms << "msec";

    if( str != str2 or str2 != str3 )
    {
      kmWarning() << "Result mismatch!";
      kmWarning() << "String one: " << str;
      kmWarning() << "String two: " << str2;
      kmWarning() << "String three: " << str3;
    }
  }

  kmDebug() << "** Grand Totals: **";
  olds  = oldtotal / 1000;
  oldms = oldtotal % 1000;
  old2s = old2total / 1000;
  old2ms= old2total % 1000;
  news  = newtotal / 1000;
  newms = newtotal % 1000;
  kmDebug() << "New parser:          " << news << "s " << newms << "msec";
}



void KMessTest::testMsnPlusInteractive()
{
  static bool initialized = false;

  if( !initialized )
  {
    testGroupsAndContacts();
  }

  static QDialog *dialog = new QDialog(mainWindow_);
  static QVBoxLayout *verticalLayout = new QVBoxLayout(dialog);
  static QLabel *label = new QLabel("Input string:",dialog);
  static QTextEdit *input = new QTextEdit(dialog);
  static QLabel *oldSourceLabel = new QLabel(dialog);
  static QLabel *oldCleanLabel = new QLabel(dialog);
  static QLabel *oldHtmlLabel = new QLabel(dialog);
  static QLabel *newSourceLabel = new QLabel(dialog);
  static QLabel *newCleanLabel = new QLabel(dialog);
  static QLabel *newHtmlLabel = new QLabel(dialog);

  static RichTextParser *parser = RichTextParser::instance();

  if( !initialized)
  {
    initialized = true;

    verticalLayout->addWidget(label);
    verticalLayout->addWidget(input);
    verticalLayout->addWidget(oldSourceLabel);
    verticalLayout->addWidget(oldCleanLabel);
    verticalLayout->addWidget(oldHtmlLabel);
    verticalLayout->addWidget(newSourceLabel);
    verticalLayout->addWidget(newCleanLabel);
    verticalLayout->addWidget(newHtmlLabel);
    dialog->setLayout(verticalLayout);

    oldSourceLabel->setTextFormat( Qt::PlainText );
    newSourceLabel->setTextFormat( Qt::PlainText );
    oldSourceLabel->setWordWrap( true );
    oldCleanLabel->setWordWrap( true );
    oldHtmlLabel ->setWordWrap( true );
    newSourceLabel->setWordWrap( true );
    newCleanLabel->setWordWrap( true );
    newHtmlLabel ->setWordWrap( true );
    oldSourceLabel->setTextInteractionFlags(Qt::TextBrowserInteraction);
    newSourceLabel->setTextInteractionFlags(Qt::TextBrowserInteraction);
    oldSourceLabel->setFrameShape(QFrame::Box);
    newSourceLabel->setFrameShape(QFrame::Box);

    connect( input, SIGNAL( textChanged() ),
             this,  SLOT  ( testMsnPlusInteractive() ) );
    dialog->show();
  }
  else
  {
    QString text;
    QString cleaned, parsed;

    QString handle( "contact1@kmess.org" );
    QStringList pending;
    // globalSession->getContact( handle )->getPendingEmoticonPattern();

    // New parser
    text = input->document()->toPlainText();
    text = parser->parseMsnString( text,
        ( RichTextParser::FormattingOptions )( RichTextParser::PARSE_EMOTICONS | RichTextParser::EMOTICONS_SMALL | RichTextParser::PARSE_LINKS | RichTextParser::PARSE_MSNPLUS ),
        "contact1@kmess.org", pending );

    cleaned = parser->getCleanString( text );
    parsed  = parser->getFormattedString( text );
    newSourceLabel->setText( "New source:  " + cleaned );
    newCleanLabel->setText( "New text:  " + cleaned );
    newHtmlLabel ->setText( "New HTML:  " + parsed );
  }
}


void KMessTest::testSoapConnection()
{
  MsnAppDirectoryService *appDir = new MsnAppDirectoryService( globalSession );
  appDir->queryServiceList( MsnAppDirectoryService::GAMES );
}



void KMessTest::testSystemTray()
{
  quitAfterTest_ = true;

  mainWindow_->systemTrayWidget_->displayCloseMessage();
}



void KMessTest::testUpnpConnection()
{
#ifdef __GNUC__
#warning upnp is disabled.
#endif
  //UPnP::Manager *manager = UPnP::Manager::instance();

  //Q_UNUSED(manager);
}


void KMessTest::testXslTransformation()
{
  // A simple test using XSL/XML data of the KMess website
  QString wwwRoot( "/srv/www/vhosts/kmess/htdocs" );

  // Load XSL
  XslTransformation *xsl = new XslTransformation();
  xsl->setStylesheet( wwwRoot + "/resources/xsl/kmess.xsl" );

  // Open XML
  QFile file( wwwRoot + "/index.xml" );
  bool open = file.open(QIODevice::ReadOnly);
  if( ! open)
  {
    kmWarning() << "Could not open file: " << file.fileName();
    return;
  }

  // Load XML
  QByteArray fileData = file.readAll();
  QString xml( QString::fromUtf8(fileData.data(), fileData.size()) );
  file.close();

  // Transform XML
  kmDebug() << "XSL transformation result:";
  kmDebug() << xsl->convertXmlString(xml);

  delete xsl;
}



void KMessTest::testOfflineMessages()
{
  // The MSN servers *sometimes* send the offline messages before the contact list.
  // 1. make sure we have an empty contact list
  // 2. receive an offline message from nonexistant@kmess.org
  // 3. add contacts to the contact list
  // 4. receive an offline message from an existing contact

  Account *account = Account::connectedAccount;
  account->setGuestAccount( true );

  ContactList *list = globalSession->msnNotificationConnection_->contactList_;
  // Don't add any groups or contacts.
  list->reset( true );

#ifdef __GNUC__
#warning Can't test offline messages ATM, turn this piece of code into a library unittest:
#endif
#if 0
  // An offline message sent yesterday, from an unknown user, received before
  // the contact list was ever created
  QString address ("nonexistant@kmess.org");
  KMess::MsnContact *contact = new KMess::MsnContact( "1:" + address );
  KMess::OfflineMessage *message = new KMess::OfflineMessage( 0, contact, true );
  message->setMessage( QString( "This message should come from the e-mail "
                                "address ").append( address ) );

  globalSession->chatFactory_->message( message );

  // re-add the contact list
  testGroupsAndContacts();

  // send another offline message
  address = "contact1@kmess.org";
  QString name( "Contact 1! :)" );

  // emulate some of MsnNotificationConnection::receivedOfflineIm() behaviour.
  contact = new KMess::MsnContact( "1:" + address );
  contact->setDisplayName( name );

  message = new KMess::OfflineMessage( 0, contact, true );
  message->setMessage( QString( "This message should come from a proper contact whose name "
                                "is" ).append( name ) );
  globalSession->chatFactory_->message( message );
#endif
}


#include "kmesstest.moc"
