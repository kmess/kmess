/***************************************************************************
                          kmessview.h  -  description
                             -------------------
    begin                : Thu Jan 9 2003
    copyright            : (C) 2003 by Mike K. Bennett
                           (C) 2005 by Diederik van der Boor
    email                : mkb137b@hotmail.com
                           vdboor --at-- codingdomain.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CONTACTLISTWIDGET_H
#define CONTACTLISTWIDGET_H

#include "ui_contactlistwidget.h"

#include <QPixmap>
#include <QTimer>
#include <QToolTip>
#include <QWidget>

// Forward declarations
class Contact;
class ContactListDisplayModel;
class Group;

class QItemSelectionModel;
class QSignalMapper;
class QToolTip;
class QTreeView;

class KAction;
class KActionMenu;
class KMenu;

#include <KMess/ContactListModelItem>

/**
 * @brief The contact list widget.
 *
 * This class implements the contact list viewing widget.
 *
 * @author Mike K. Bennett
 * @author Valerio Pilo
 * @ingroup Root
 */
class ContactListWidget : public QWidget, private Ui::ContactListWidget
{
  Q_OBJECT

  friend class KMessTest;

  public:
    // The constructor
                     ContactListWidget( QWidget *parent = 0 );
    // The destructor
    virtual         ~ContactListWidget();
    // Initialize the widget with a model
    void             setModel( QAbstractItemModel *viewModel );

  protected:
    // The personal status message received an event.
    bool             eventFilter( QObject *obj, QEvent *ev );

  private: // Private methods
    // Get the specified item's or the current item's data
    const KMess::ModelDataList getItemData( const QModelIndex &index = QModelIndex() );
    // Initialize the contact and group context menus
    bool                initializeActions();
    // return the group at the given sort position in the view.
    Group              getGroupAtSortPosition( int position );

  public slots: // Public slots
    // Show the context menu
    void             showContextMenu( const QPoint &point );
    // The "show search in contact list" menu item has been toggled.
    void             toggleShowSearchFrame( bool show );

  private slots: // Private slots
    // Show the contact & group tool tip
    void             showToolTip( const QPoint &point );
    // Send an email to the current contact.
    void             slotEmailContact();
    // A group was expanded or collapsed
    void             slotGroupChanged( const QModelIndex &index );
    // An item was clicked
    void             slotItemClicked( const QModelIndex &index, const bool accidental = false );
    // An item was single clicked
    void             slotItemSingleClicked( const QModelIndex &index );
    // An item was double clicked
    void             slotItemDoubleClicked( const QModelIndex &index );
    // Perform the "add contact" action
    void             slotContactAdd();
    // Perform the "allow contact" action
    void             slotContactAllow();
    // Perform the "block contact" action
    void             slotContactBlock();
    // Perform the "copy contact to a group" action
    void             slotContactAddToGroup( const QString &groupId );
    // Perform the "move contact between groups" action
    void             slotContactMove( const QString &groupId );
    // Perform the "remove contact" action
    void             slotContactRemove();
    // Perform the "remove contact from group" action
    void             slotContactRemoveFromGroup();
    // Perform the "unblock contact" menu action
    void             slotContactUnblock();
    // Perform the "remove group" action
    void             slotGroupRemove();
    // Perform the "rename group" action
    void             slotGroupRename();
    // Forward the "start chat" menu action
    void             slotForwardStartChat();
    // Move the selected group up or down one position.
    void             slotMoveGroup();
    // Generate the lists of actions for the "copy/move contact to group" menus
    void             rebuildActions();
    // Delay a bit updating the groups status
    void             slotScheduleGroupUpdate();
    // Slot for searching contact
    void             slotSearchContact( const QString& searchFor );
    // Display the chat history of the current contact
    void             slotShowChatHistory();
    // Display the profile of the current contact
    void             slotShowContactProfile();
    // Display the properties of the current contact
    void             slotShowContactProperties();
    // Update the expanded status of the list groups
    void             slotUpdateGroups();
    // Update the contact list widgets
    void             slotUpdateView();
    // Copy some details of the contact to the clipboard
    void             copyText();

  private: // Private attributes
    // KActions used in the contact popup menu
    KAction         *addContact_;
    KAction         *allowContact_;
    KAction         *blockContact_;
    KAction         *unblockContact_;
    KAction         *chatWithContact_;
    KAction         *msnProfile_;
    KAction         *chatHistory_;
    KAction         *removeContact_;
    KAction         *removeFromGroup_;
    KAction         *emailContact_;
    KAction         *contactProperties_;
    // KActions uses in the group popup menu
    KAction         *moveGroupDown_;
    KAction         *moveGroupUp_;
    KAction         *removeGroup_;
    KAction         *renameGroup_;
    // KAction uses in the copy popup menu
    KActionMenu     *popupCopyMenu_;
    KAction         *popupCopyFriendlyName_;
    KAction         *popupCopyPersonalMessage_;
    KAction         *popupCopyHandle_;
    KAction         *popupCopyMusic_;
    QList<KAction*>  groupCopyLinkActionsList_;
    // The menu of actions possible to perform on a given contact
    KMenu      *contactActionPopup_;
    // The menu of actions possible to perform on a given group
    KMenu      *groupActionPopup_;
    // KSelectActions used in the contact popup menu
    KActionMenu     *moveContactToGroup_;
    KActionMenu     *copyContactToGroup_;
    // Signal mapper needed to identify the single copy actions
    QSignalMapper   *contactCopyMapper_;
    // Signal mapper needed to identify the single move actions
    QSignalMapper   *contactMoveMapper_;
    // List of groups between which the user can copy contacts
    QList<KAction*>  groupCopyActionsList_;
    // List of groups between which the user can move contacts
    QList<KAction*>  groupMoveActionsList_;
    // The background pixmap
    QPixmap          backgroundPixmap_;
    // Selection model used to access selected items in the list
    QItemSelectionModel *selectionModel_;
    // Timed used to posticipate update events
    QTimer           updateTimer_;
    // Quick ref to the tree view's filter model
    ContactListDisplayModel *viewModel_;

  signals:
    // Notify about a change in the selection
    void             selectionChanged( const QItemSelection &selection );
};

#endif
