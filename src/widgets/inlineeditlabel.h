/***************************************************************************
                          inlineeditlabel.h  -  description
                             -------------------
    begin                : Tue Dec 29 2009
    copyright            : (C) 2009 by Valerio Pilo
    email                : valerio@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef INLINEEDITLABEL_H
#define INLINEEDITLABEL_H

#include "ui_inlineeditlabel.h"

#include "utils/richtextparser.h"

#include <QString>
#include <QWidget>



/**
 * @brief An inline editing label.
 *
 * This is normally a label widget. By clicking on it, the label's contents can be changed,
 * because the label turns into a line edit.
 * The label supports rich text (and it is enabled by default), so emoticons,
 * links etc can be used.
 *
 * @author Valerio Pilo
 */
class InlineEditLabel : public QWidget, private Ui::InlineEditLabel
{
  Q_OBJECT

  public:
    // Constructor
                         InlineEditLabel( QWidget *parent = 0 );
    // The destructor
    virtual             ~InlineEditLabel();
    // The label or the lineedit received an event
    bool                 eventFilter( QObject *obj, QEvent *event );
    // Change the text formatting mode for the label
    void                 setFormattingMode( FormattingMode mode );
    // Limit the amount of text which can be input
    void                 setMaxLength( int length );
    // Set the text to show
    void                 setText( const QString &text, bool labelOnly = false );
    // Retrieve the current text
    const QString &      text();

  private: // Private methods
    // Switch the widget to the label or the lineedit
    void                 enableEditMode( bool editMode );

  private: // Private attributes
    // Maximum length in bytes of the text in the input field
    int                  maxLength_;
    // Text formatting mode for the label
    FormattingMode       mode_;
    // The current text
    FormattedString      text_;

  signals:
    // Label text was changed
    void                 changed();

};



#endif
