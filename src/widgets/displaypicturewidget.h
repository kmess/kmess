/***************************************************************************
                          displaypicturewidget.h  -  display picture widget
                             -------------------
    begin                : Tue Jul 28 2009
    copyright            : (C) 2009 by Valerio Pilo <valerio@kmess.org>
                           (C) 2009 by Ruben Vandamme <ruben@kmess.org>
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef DISPLAYPICTUREWIDGET_H
#define DISPLAYPICTUREWIDGET_H

#include "ui_displaypicturewidget.h"

#include <QFrame>
#include <QMovie>



/**
 * @brief The display picture widget.
 *
 * This class implements a display picture frame.
 *
 * @author Valerio Pilo
 * @ingroup Widgets
 */
class DisplayPictureWidget : public QFrame, private Ui::DisplayPictureWidget
{
  Q_OBJECT

  Q_PROPERTY( QString clickoverlay  READ clickOverlay  WRITE setClickOverlay  )
  Q_PROPERTY( QString normaloverlay READ normalOverlay WRITE setNormalOverlay )

  friend class KMessTest;


  public:

    // The constructor
                     DisplayPictureWidget( QWidget *parent = 0 );
    // The destructor
    virtual         ~DisplayPictureWidget();
    // Set the click overlay
    const QString   &clickOverlay ();
    // Set the default overlay
    const QString   &normalOverlay ();
    // Get the current picture's path
    const QString   &getPicture() const;
    // Force the frame to be highlighted
    void             setHighlighted( bool highlight );
    // Set the picture to show when the given picture isn't valid
    void             setDefaultPicture( const QString &defaultPicture );
    // Show a picture into the frame
    void             setPicture( const QString &picture );
    // Change the picture size
    void             setSize( const int size );
    // Set the click overlay
    void             setClickOverlay ( const QString & style );
    // Set the default overlay
    void             setNormalOverlay ( const QString & style );


  protected:

    // The widget received an event
    bool             eventFilter( QObject *obj, QEvent *ev );


  private: // Private properties

    // Whether the picture was forcibly highlighted
    bool             highlighted_;
    // The current picture's image data
    QPixmap          picture_;
    // The current picture path
    QString          picturePath_;
    // The current picture's movie data
    QMovie           movie_;
    // Semi-transparent widget overlaid to the picture to highlight it
    QWidget         *overlay_;


  private: // Static private properties

    // Default display picture
    QString          defaultPicturePath_;


  private: // Static private properties

    // Overlay widget's style sheet
    static QString  overlayStyle_;

    QString  clickOverlayStyle_;
    QString  normalOverlayStyle_;

  signals:

    // The widget has been clicked with the left mouse button
    void             leftClicked();
    // The widget has been clicked with the right mouse button
    void             rightClicked();

};



#endif
