/***************************************************************************
     gradientelidelabel.cpp -  adds a gradient-like elide to long labels
                             -------------------
    begin                : Thursday May 14 2009
    copyright            : (C) 2009 by Adam Goossens
    email                : fontknocker@gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef GRADIENTELIDELABEL_H
#define GRADIENTELIDELABEL_H

#include <QLabel>

/**
 * This class is a subclass of QLabel that provides gradient Elide support.
 * It properly supports rich text in the underlying QLabel.
 * 
 * It does this by forcing the underlying QLabel to draw itself onto a pixmap
 * and then compositing this pixmap with a transparent gradient.
 * 
 * It will only draw the gradient if the text width exceeds the width of the label.
 * 
 * @author Adam Goossens (fontknocker@gmail.com)
 */
class GradientElideLabel : public QLabel
{
  public:
    GradientElideLabel(QWidget *parent = 0);
    GradientElideLabel(const QString &text, QWidget *parent = 0);

    ~GradientElideLabel();

    // overridden from QLabel. Does the gradient magic.
    void paintEvent(QPaintEvent *event);

  public slots:
    void setText( const QString &text );

  private:
    QString cleanText;

};

#endif
