/***************************************************************************
     dynamictooltip.cpp -  a tooltip capable of showing rich text and animations
                             -------------------
    begin                : Wednesday Sep 2 2009
    copyright            : (C) 2009 by Valerio Pilo
    email                : valerio@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "dynamictooltip.h"

#include "../contact/contact.h"
#include "../contact/group.h"
#include "../contact/contactextension.h"
#include "../utils/richtextparser.h"
#include "../utils/kmessshared.h"
#include "../emoticonmanager.h"
#include "../kmessdebug.h"

#include "ui_dynamictooltipcontact.h"
#include "ui_dynamictooltipgroup.h"

#include <KMess/Utils>
#include <KMess/ContactListModelItem>

#include <QApplication>
#include <QBitmap>
#include <QDesktopWidget>
#include <QMutexLocker>
#include <QPainter>
#include <QPaintEvent>
#include <QToolTip>

#include <KColorScheme>

#ifdef Q_WS_X11
  #include <QX11Info>
  #include <X11/Xlib.h>
  #include <X11/extensions/shape.h>
#endif

#include "../contact/status.h"


// Static member initialization
DynamicToolTip *DynamicToolTip::instance_( 0 );
QMutex          DynamicToolTip::instanceMutex_;



// Constructor for model indices tooltips
DynamicToolTip::DynamicToolTip( const QModelIndex &index, const QPoint &pos, QWidget *sourceWidget, const QRect &rect )
: QWidget( 0, Qt::ToolTip )
, pos_( pos )
, rect_( rect )
, widget_( sourceWidget )
{
#ifdef Q_WS_X11
  if( QX11Info::isCompositingManagerRunning() )
  {
    // FIXME Doesn't work - if on, the background is not painted
    // when compositing is active
//     setAttribute( Qt::WA_TranslucentBackground );
  }
#endif

  // Close any other open tooltip
  if( instance_ )
  {
    QMutexLocker locker( &instanceMutex_ );

    delete instance_;
  }
  instance_ = this;

#ifdef KMESSDEBUG_DYNAMICTOOLTIP
  kmDebug() << "CREATED: Instance" << this;
#endif

  qApp->installEventFilter( this );

  // Read the data off the model index
  const KMess::ModelDataList& itemData( index.data().toMap() );

  // Do nothing if the index is not valid
  if( ! index.isValid() || itemData.isEmpty() )
  {
#ifdef KMESSDEBUG_DYNAMICTOOLTIP
    kmDebug() << "The index was not valid";
#endif
    deleteLater();
    return;
  }

  bool result = false;
  switch( itemData[ "type" ].toInt() )
  {
    case KMess::ContactListModelItem::ItemContact:
      result = displayContactToolTip( itemData );
      break;

    case KMess::ContactListModelItem::ItemGroup:
      result = displayGroupToolTip( itemData );
      break;

    default:
      break;
  }

  if( result )
  {
    show();
  }
  else
  {
    deleteLater();
  }
}



// Destructor
DynamicToolTip::~DynamicToolTip()
{
  qApp->removeEventFilter( this );

  instance_ = 0;

#ifdef KMESSDEBUG_DYNAMICTOOLTIP
  kmDebug() << "DELETED: Instance" << this;
#endif
}



// Display the contact tooltip
bool DynamicToolTip::displayContactToolTip( const KMess::ModelDataList &item )
{
  // Don't display wrong item types
  if( item[ "type" ] != KMess::ContactListModelItem::ItemContact )
  {
    return false;
  }

  // We'll need to access a couple core components
  RichTextParser  *richText  = RichTextParser ::instance();
  EmoticonManager *emoticons = EmoticonManager::instance();

  // Create the UI and show it into the tooltip
  Ui::DynamicToolTipContact *ui = new Ui::DynamicToolTipContact;
  ui->setupUi( this );

  // Rich text rendering options for the tooltip: Escape HTML, replace standard
  // emoticons, but don't show links or formatting
  RichTextParser::FormattingOptions richTextOptions = (RichTextParser::FormattingOptions)
                                                      ( RichTextParser::PARSE_EMOTICONS
                                                      | RichTextParser::EMOTICONS_SMALL );


  Contact contact( item[ "handle" ].toString() );
  // Add the display picture (if there is any), or the default KMess logo
  ui->displayPicture_->setPicture( contact->getContactPicturePath() );


  // Add the contact's real name - the alias is not shown:
  // you can read it in the contact list, after all
  QString friendlyName( contact->getTrueFriendlyName() );

  friendlyName = richText->parseMsnString( friendlyName, richTextOptions );
  ui->friendlyName_->setText( friendlyName );


  // Add status
  KMess::MsnStatus status = (KMess::MsnStatus) item[ "status" ].toInt();

  ui->statusIcon_->setPixmap( Status::getIcon( status ) );
  ui->contactStatusLabel_->setText( Status::getName( status ) );


  // Add the personal message
  QString personalMessage( item[ "personalMessage" ].toString() );

  if( ! personalMessage.isEmpty() )
  {
    personalMessage = richText->parseMsnString( personalMessage, richTextOptions );
    ui->personalMessage_->setText( personalMessage );
  }


  // If the contact is listening to music, show it
  const QString& mediaType  ( item[ "mediaType"   ].toString() );
  const QString& mediaString( item[ "mediaString" ].toString() );
  if( ! mediaString.isEmpty() )
  {
    QString mediaEmoticon;

    // Determine icon for special media types
    if( mediaType == "Music" )
    {
      // A music note
      mediaEmoticon = emoticons->getReplacement( "(8)", true /* small */ );
    }
    else if( mediaType == "Gaming" )
    {
      // The XBox
      mediaEmoticon = emoticons->getReplacement( "(xx)", true /* small */ );
    }

    ui->nowListening_->setText( mediaEmoticon + KMess::Utils::htmlEscape( mediaString ) );
  }


  // Hide if necessary the message about our presence in this contact's list
  // Update: Always hide the message, hasUser is not reliable anymore
  //if( item[ "hasUser" ].toBool() )
  {
    ui->userNotInContactsList_->hide();
  }

  // Add the handle
  ui->emailAddressLabel_->setText( item[ "handle" ].toString() );

  // Add the client identifier
  const QString& clientName( item[ "clientName" ].toString() );
  if( ! clientName.isEmpty() )
  {
    ui->clientLabel_->setText( KMess::Utils::htmlEscape( clientName ) );
  }


  // Add info whether the contact is blocked
  QString blockedString;
  if( item[ "isBlocked" ].toBool() )
  {
    blockedString = i18n( "Yes" );
  }
  else
  {
    blockedString = i18n( "No" );
  }
  ui->blockedLabel_->setText( blockedString );


  // Add last seen date
  QString lastSeenString;
  const QDateTime& lastSeen( contact->getExtension()->getLastSeen() );

  if( status != KMess::OfflineStatus )
  {
    lastSeenString = i18n( "Connected" );
  }
  else if( lastSeen.toTime_t() == 0 || lastSeen.toTime_t() == UINT_MAX || lastSeen.isNull() )
  {
    lastSeenString = i18n( "Not seen yet" );
  }
  else
  {
    lastSeenString = KMessShared::fuzzyTimeSince( lastSeen );
  }
  ui->lastSeenLabel_->setText( lastSeenString );


  // Add last message date
  QString lastMessageString;
  const QDateTime& lastMessage( contact->getExtension()->getLastMessageDate() );

  if( lastMessage.toTime_t() == 0 || lastMessage.toTime_t() == UINT_MAX || lastMessage.isNull() )
  {
    lastMessageString = i18n( "No messages yet" );
  }
  else
  {
    lastMessageString = KMessShared::fuzzyTimeSince( lastMessage );
  }
  ui->lastMessageLabel_->setText( lastMessageString );

  return true;
}



// Display the group tooltip
bool DynamicToolTip::displayGroupToolTip( const KMess::ModelDataList &item )
{
  // Don't display wrong item types
  if( item[ "type" ] != KMess::ContactListModelItem::ItemGroup )
  {
    return false;
  }

  // We'll need to access the rich text parser
  RichTextParser *richText = RichTextParser::instance();

  // Create the UI and show it into the tooltip
  Ui::DynamicToolTipGroup *ui = new Ui::DynamicToolTipGroup;
  ui->setupUi( this );

  // Rich text rendering options for the tooltip: Escape HTML, replace standard
  // emoticons, but don't show links or formatting
  RichTextParser::FormattingOptions richTextOptions = (RichTextParser::FormattingOptions)
                                                      ( RichTextParser::PARSE_EMOTICONS
                                                      | RichTextParser::EMOTICONS_SMALL );

  // Add the group name
  QString groupName( Group( item[ "id" ].toString() )->getName() );

  groupName = richText->parseMsnString( groupName, richTextOptions );
  ui->groupName_->setText( groupName );

  QString text;

  // Non-empty and non-special groups also have an Online contacts count.
  // (Individuals is treated as a non-special group)
  if( ( ! item[ "isSpecialGroup" ].toBool() || item[ "id" ].toString() == KMess::SpecialGroups::INDIVIDUALS )
  &&      item[ "totalContacts"  ].toInt() > 0 )
  {
    text = QString( "%1, %2" ).arg( i18ncp( "Contact counters in normal group tooltip, first part",
                                            "%1 contact", "%1 contacts",
                                            item[ "totalContacts"  ].toInt() ) )
                              .arg( i18ncp( "Contact counters in normal group tooltip, second part",
                                            "%1 online", "%1 online",
                                            item[ "onlineContacts" ].toInt() ) );
  }
  else
  {
    text = i18ncp( "Contacts count in special group tooltip",
                   "%1 contact", "%1 contacts", item[ "totalContacts" ].toInt() );
  }
  ui->groupInfo_->setText( text );

  return true;
}



// Catches the associated widget's events
bool DynamicToolTip::eventFilter( QObject *watched, QEvent *event )
{
  switch( event->type() )
  {
    // Delete the tooltip when the cursor goes outside of the app
    case QEvent::Leave:
      hide();
      break;

    case QEvent::WindowActivate:
    case QEvent::WindowDeactivate:
    case QEvent::MouseButtonPress:
    case QEvent::MouseButtonRelease:
    case QEvent::MouseButtonDblClick:
    case QEvent::Wheel:
      delete this;
      break;

    case QEvent::MouseMove:
      // If the cursor went out of the rectangle for which the tooltip was created,
      // destroy the tip
      if(   watched == widget_
      &&  ! rect_.isNull()
      &&  ! rect_.contains( static_cast<QMouseEvent*>( event )->pos() ) )
      {
        hide();
      }
      break;

    // Keep the compiler happy
    default:
      break;
  }

  return QObject::eventFilter( watched, event );
}



// Hides and deletes the tooltip
void DynamicToolTip::hide()
{
  QWidget::hide();
  deleteLater();
}



// Helper method to make an arc in paintEvent()
static inline void arc( QPainterPath &path, qreal cx, qreal cy, qreal radius, qreal angle, qreal sweeplength )
{
  path.arcTo( cx-radius, cy-radius, radius * 2, radius * 2, angle, sweeplength );
}



// Paints the tooltip and its contents
void DynamicToolTip::paintEvent( QPaintEvent *event )
{
  // This code is mostly taken from Dolphin

  const QRect rect( event->rect() );

  // FIXME We also receive paint events for our children, but they don't need
  // any custom painting. Skip them for now
  if( rect.topLeft().x() != 0 ||  rect.topLeft().y() != 0 )
  {
#ifdef KMESSDEBUG_DYNAMICTOOLTIP
  kmDebug() << "Skipping sub-item painting";
#endif
    return;
  }

#ifdef KMESSDEBUG_DYNAMICTOOLTIP
  kmDebug() << "Painting on rect:" << rect;
#endif

  QPainterPath path;
  const qreal radius = 10;
  path.moveTo( rect.left(), rect.top() + radius );
  arc( path, rect.left () + radius, rect.top   () + radius, radius, 180, -90 );
  arc( path, rect.right() - radius, rect.top   () + radius, radius,  90, -90 );
  arc( path, rect.right() - radius, rect.bottom() - radius, radius,   0, -90 );
  arc( path, rect.left () + radius, rect.bottom() - radius, radius, 270, -90 );
  path.closeSubpath();

  QBitmap bitmap( rect.size() );
  bitmap.fill( Qt::color0 );

  QPainter p( &bitmap );
  p.setPen( QPen( Qt::color1, 1 ) );
  p.setBrush( Qt::color1 );
  p.drawPath( path );
  p.end();

  QRegion region( bitmap );

#ifdef Q_WS_X11
  if( QX11Info::isCompositingManagerRunning() )
  {
    kmDebug() << "Setting X region";
    XShapeCombineRegion( x11Info().display(),
                         winId(),
                         ShapeInput,
                         0, 0,
                         region.handle(),
                         ShapeSet );
  }
  else
  {
    kmDebug() << "Setting standard region";
    setMask( region );
  }
#else
  kmDebug() << "Not running X11, setting standard region";
  setMask( region );
#endif

  QPainter painter( this );

  painter.setRenderHints(   QPainter::Antialiasing
                          | QPainter::SmoothPixmapTransform
                          | QPainter::TextAntialiasing,
                          true );

//   painter.setPen( Qt::NoPen );
  painter.setPen( QToolTip::palette().brush( QPalette::ToolTipBase ).color() );

  painter.drawPath( path );
}



// Positions and shows the tooltip
void DynamicToolTip::show()
{
  adjustSize();
  updateGeometry();

  // The code for this method was taken from QToolTip.


  // Get the number of the screen where the tooltip will be placed
  int screenNumber;
  QDesktopWidget *desktop = QApplication::desktop();
  if( desktop->isVirtualDesktop() )
  {
    screenNumber = desktop->screenNumber( pos_ );
  }
  else
  {
    screenNumber = desktop->screenNumber( widget_ );
  }

#ifdef Q_WS_MAC
  QRect screen( desktop->availableGeometry( screenNumber ) );
#else
  QRect screen( desktop->screenGeometry( screenNumber ) );
#endif

#ifdef KMESSDEBUG_DYNAMICTOOLTIP
  kmDebug() << "Original position:" << pos_ << "screen:" << screenNumber << "," << screen;
#endif

  QPoint p( pos_ );

  // Show the tooltip away from the cursor by a little
  p += QPoint(  2,
#ifdef Q_WS_WIN
               21
#else
               16
#endif
             );

  if( p.x() + width() > screen.x() + screen.width() )
  {
    p.rx() -= 4 + width();
  }

  if( p.y() + height() > screen.y() + screen.height() )
  {
    p.ry() -= 24 + height();
  }

  if( p.y() < screen.y() )
  {
    p.setY( screen.y() );
  }

  if( p.x() + width() > screen.x() + screen.width() )
  {
    p.setX( screen.x() + screen.width() - width() );
  }

  if( p.x() < screen.x() )
  {
    p.setX( screen.x() );
  }

  if( p.y() + height() > screen.y() + screen.height() )
  {
    p.setY( screen.y() + screen.height() - height() );
  }

#ifdef KMESSDEBUG_DYNAMICTOOLTIP
  kmDebug() << "Showing tooltip at:" << p << "item rect:" << rect_;
#endif

  move( p );
  QWidget::show();
}



// Display a model index tooltip
void DynamicToolTip::show( const QModelIndex &index, const QPoint &pos, QWidget *sourceWidget, const QRect &rect )
{
  if( instance_)
  {
    if( instance_->widget_ == sourceWidget && instance_->rect_ == rect )
    {
      return;
    }
  }

  // The tooltip deletes itself when the cursor goes out of the rect
  new DynamicToolTip( index, pos, sourceWidget, rect );
}



#include "dynamictooltip.moc"
