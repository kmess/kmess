/***************************************************************************
     dynamictooltip.h -  a tooltip capable of showing rich text and animations
                             -------------------
    begin                : Wednesday Sep 2 2009
    copyright            : (C) 2009 by Valerio Pilo
    email                : valerio@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef DYNAMICTOOLTIP_H
#define DYNAMICTOOLTIP_H

#include <QMutex>
#include <QWidget>


// Forward declarations
class QModelIndex;

#include <KMess/ContactListModelItem>

/**
 * A dynamic contact tool tip.
 *
 * This class implements a fancy-looking and themeable contact tooltip, which also is
 * capable of showing animations.
 *
 * @author Valerio Pilo <valerio@kmess.org>
 */
class DynamicToolTip : public QWidget
{
  Q_OBJECT


  public: // Public static methods

    // Display a model index tooltip
    static void            show( const QModelIndex &index, const QPoint &pos, QWidget *sourceWidget, const QRect &rect );
    // Display the generic tooltip
//     static void            show( const QString &text, const QPoint &pos, QWidget *sourceWidget );


  private: // Private constructors

    // Constructor for model indices tooltips
                           DynamicToolTip( const QModelIndex &index, const QPoint &pos, QWidget *sourceWidget, const QRect &rect );
    // Constructor for simple text tooltips
//                            DynamicToolTip( const QString &text, const QPoint &pos, QWidget *sourceWidget );
    // Destructor
                          ~DynamicToolTip();


  private: // Private methods

    // Display the contact tooltip
    bool                   displayContactToolTip( const KMess::ModelDataList &item );
    // Display the group tooltip
    bool                   displayGroupToolTip( const KMess::ModelDataList &item );
    // Display the generic tooltip
//     bool                   displayHtmlToolTip( const QString &text );
    // Catches the associated widget's events
    bool                   eventFilter( QObject *watched, QEvent *event );
    // Hides and deletes the tooltip
    void                   hide();
    // Paints the tooltip and its contents
    void                   paintEvent( QPaintEvent *event );
    // Positions and shows the tooltip
    void                   show();


  private: // Private properties

    // Position where the tooltip will appear
    QPoint                 pos_;
    // Area within which the tooltip is valid
    QRect                  rect_;
    // Widget which requested the tooltip
    QWidget               *widget_;


  private: // Private static properties

    // Instance of the tooltip
    static DynamicToolTip *instance_;
    // Mutex to avoid the contemporary creation of instances
    static QMutex          instanceMutex_;


};



#endif
