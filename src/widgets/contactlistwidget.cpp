/***************************************************************************
                          contactlistwidget.cpp - display a contact list model
                             -------------------
    begin                : Thu Jul 23 2009
    copyright            : (C) 2003 by Mike K. Bennett
                           (C) 2005 by Diederik van der Boor
                           (C) 2009 by Valerio Pilo
    email                : mkb137b@hotmail.com
                           vdboor --at-- codingdomain.com
                           project@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "contactlistwidget.h"

#include "../contact/contact.h"
#include "../contact/group.h"
#include "../contact/status.h"
#include "../dialogs/chathistorydialog.h"
#include "../dialogs/contactpropertiesdialog.h"
#include "../utils/kmessshared.h"
#include "../utils/kmessconfig.h"
#include "../account.h"
#include "../kmessapplication.h"
#include "../kmessdebug.h"
#include "../kmessglobal.h"
#include "../mainwindow.h"
#include "../model/contactlistdisplaymodel.h"
#include "../chat/chatmaster.h"
#include "contactlistdelegate.h"
#include "dynamictooltip.h"

#include <QClipboard>
#include <QKeyEvent>
#include <QImage>
#include <QItemSelectionModel>
#include <QPainter>
#include <QPixmap>
#include <QSignalMapper>
#include <QStringList>
#include <QToolTip>

#include <KAction>
#include <KActionMenu>
#include <KApplication>
#include <KGlobal>
#include <KIconLoader>
#include <KIconEffect>
#include <KInputDialog>
#include <KLocale>
#include <KMenu>
#include <KMessageBox>
#include <KRun>
#include <KUrl>

#include <KMess/ContactListModelItem>
#include <KMess/MsnGroup>

#ifdef KMESSDEBUG_CONTACTLISTMODELTEST
  #include "../../tests/modeltest/modeltest.h"
#endif

#ifdef KMESSDEBUG_CONTACTLISTWIDGET
  #define KMESSDEBUG_CONTACTLISTWIDGET_GENERAL

  #ifdef KMESSDEBUG_CONTACTLISTMODEL
    #define KMESSDEBUG_CONTACTLISTWIDGET_ITEM_VISIBILITY
  #endif
#endif


// The constructor
ContactListWidget::ContactListWidget( QWidget *parent )
  : QWidget( parent ),
    Ui::ContactListWidget(),
    contactCopyMapper_(0),
    contactMoveMapper_(0),
    viewModel_(0)
{
  setupUi( this );

  // Set up the search frame
  searchIcon_->setPixmap( KIconLoader::global()->loadIcon( "edit-find-user", KIconLoader::Small ) );
  searchFrame_->hide();

  // Connect the search edit line with slot for searching
  connect( searchEdit_, SIGNAL(       textChanged(QString) ),
           this,        SLOT  ( slotSearchContact(QString) ) );

  // Set up the group update timer
  updateTimer_.setSingleShot( true );
  connect( &updateTimer_, SIGNAL(          timeout() ),
           this,          SLOT  ( slotUpdateGroups() ) );

  // Set the owner window to always show tooltips on request,
  // even if it doesn't have focus.
  window()->setAttribute( Qt::WA_AlwaysShowToolTips );

  // Enable the right-click context menu
  contactListView_->setContextMenuPolicy( Qt::CustomContextMenu );
  connect( contactListView_, SIGNAL( customContextMenuRequested(QPoint) ),
           this,               SLOT(            showContextMenu(QPoint) ) );

  // Connect signals for the connected account
  connect( Account::connectedAccount, SIGNAL( changedDisplaySettings()    ),
           this,                      SLOT  ( slotUpdateGroups()          ) );
  connect( Account::connectedAccount, SIGNAL( changedViewMode()           ),
           this,                      SLOT  ( slotUpdateGroups()          ) );
  connect( Account::connectedAccount, SIGNAL( changedContactListOptions() ),
           this,                      SLOT  ( slotUpdateView()            ) );

  // Install event filters on the view to add extra features
  contactListView_->installEventFilter( this );
  contactListView_->viewport()->installEventFilter( this );

  // Set the contact list view's properties
#if QT_VERSION >= 0x040400
  contactListView_->setHeaderHidden( true );
#else
  contactListView_->header()->hide();
#endif
  contactListView_->setAnimated( true );
  contactListView_->setDragEnabled( true );
  contactListView_->setDropIndicatorShown( true );
  contactListView_->setDragDropOverwriteMode( false );

  // Activate the custom list item painter
  contactListView_->setItemDelegate( new ContactListDelegate( contactListView_ ) );

  // TODO: support drag/drop contacts from another application!
  contactListView_->setDragDropMode( QAbstractItemView::InternalMove );
  contactListView_->viewport()->setAcceptDrops( true );
  contactListView_->setAcceptDrops( true );

  // Connect the list's signals
  connect( contactListView_, SIGNAL(               clicked(QModelIndex) ),
           this,               SLOT( slotItemSingleClicked(QModelIndex) ) );
  connect( contactListView_, SIGNAL(         doubleClicked(QModelIndex) ),
           this,               SLOT( slotItemDoubleClicked(QModelIndex) ) );
  connect( contactListView_, SIGNAL(             collapsed(QModelIndex) ),
           this,               SLOT(      slotGroupChanged(QModelIndex) ) );
  connect( contactListView_, SIGNAL(              expanded(QModelIndex) ),
           this,               SLOT(      slotGroupChanged(QModelIndex) ) );

  // Connect signals for the contactlist
  connect( globalSession->contactList(), SIGNAL(     groupAdded(KMess::MsnGroup*) ),
           this,          SLOT( rebuildActions()             ) );
  connect( globalSession->contactList(), SIGNAL(   groupRenamed(KMess::MsnGroup*) ),
           this,          SLOT( rebuildActions()             ) );
  connect( globalSession->contactList(), SIGNAL(   groupRemoved(KMess::MsnGroup*) ),
           this,          SLOT( rebuildActions()             ) );

  // Update the group counters when contacts come online or go offline
  connect( globalSession, SIGNAL(           contactOnline(KMess::MsnContact*,bool) ),
           this,          SLOT  ( slotScheduleGroupUpdate()              ) );
  connect( globalSession, SIGNAL(          contactOffline(KMess::MsnContact*) ),
           this,          SLOT  ( slotScheduleGroupUpdate()              ) );

  // Create a proxy model to filter and sort the raw contact list view
  viewModel_ = new ContactListDisplayModel( this );

  // Create a selection model to get data about the list selections
  selectionModel_ = new QItemSelectionModel( viewModel_ );

  // Connect the list to the models
  contactListView_->setModel( viewModel_ );
  contactListView_->setSelectionModel( selectionModel_ );

  // Connect the selection model's signals
  connect( selectionModel_, SIGNAL( selectionChanged(QItemSelection,QItemSelection) ),
           this,            SIGNAL( selectionChanged(QItemSelection)                ) );

  initializeActions();

  // Set the list stylesheet
  slotUpdateView();
}



// The destructor
ContactListWidget::~ContactListWidget()
{
  updateTimer_.stop();

  // Delete the contact actions for copy/move to group
  qDeleteAll( groupCopyActionsList_ );
  qDeleteAll( groupMoveActionsList_ );
  qDeleteAll( groupCopyLinkActionsList_ );
  delete contactCopyMapper_;
  delete contactMoveMapper_;

  contactListView_->setModel( 0 );

  delete selectionModel_;
  delete viewModel_;
  selectionModel_ = 0;
  viewModel_ = 0;

#ifdef KMESSDEBUG_CONTACTLISTWIDGET_GENERAL
  kmDebug() << "DESTROYED.";
#endif
}



// Get the specified item's or the current item's data
const KMess::ModelDataList ContactListWidget::getItemData( const QModelIndex &index )
{
  const QModelIndexList& selection( selectionModel_->selectedIndexes() );
  QModelIndex itemIndex( index );

  // If the index is not valid, get the current selection
  if( ! itemIndex.isValid() && ! selection.isEmpty() )
  {
    // Only one index can be returned at once by the contact list (has single selection mode)
    itemIndex = QModelIndex( selection.first() );
  }

  // The index or the selection aren't valid, abort.
  if( ! itemIndex.isValid() || itemIndex.model() != viewModel_ )
  {
#ifdef KMESSDEBUG_CONTACTLISTWIDGET_ITEM_VISIBILITY
    kmDebug() << "Skipping an invalid item at index:" << index;
#endif
    return KMess::ModelDataList();
  }

  // Get the item data
  const QVariant& data( itemIndex.data() );

  // If the data is not a variant map, abort
  if( ! data.canConvert( QVariant::Map ) )
  {
#ifdef KMESSDEBUG_CONTACTLISTWIDGET_ITEM_VISIBILITY
    kmDebug() << "Skipping an item with invalid data at index:" << index;
#endif
    return KMess::ModelDataList();
  }

  // Convert the variant data to a normal map
  const KMess::ModelDataList& itemData( data.toMap() );

  // Do nothing if there's no data
  if( itemData.isEmpty() )
  {
#ifdef KMESSDEBUG_CONTACTLISTWIDGET_ITEM_VISIBILITY
      kmDebug() << "Skipping an empty item at index:" << index;
#endif
    return KMess::ModelDataList();
  }

  int type = itemData[ "type" ].toInt();

  // Do nothing if the item is not of a valid type
  if(    type != KMess::ContactListModelItem::ItemGroup
      && type != KMess::ContactListModelItem::ItemContact )
  {
    return KMess::ModelDataList();
  }

  return itemData;
}



// The personal status message received an event.
bool ContactListWidget::eventFilter(QObject *obj, QEvent *event)
{
  if( obj != contactListView_
  &&  obj != contactListView_->viewport() )
  {
#ifdef KMESSDEBUG_CONTACTLISTWIDGET_GENERAL
    kmWarning() << "Received event '" << event->type() << "' from object '" << obj->objectName() << "'.";
#endif
    return false;
  }

  QEvent::Type eventType = event->type();

  if( obj == contactListView_->viewport() )
  {
    if( eventType == QEvent::ToolTip )
    {
      showToolTip( QCursor::pos() );
      return true;
    }
    else if( eventType == QEvent::Wheel )
    {
      // Check if the ctrl is pressed
      const QWheelEvent *wheelEvent = static_cast<QWheelEvent*>( event );
      if( wheelEvent->modifiers() != Qt::ControlModifier )
      {
        return false;
      }

      // Get the current size diplay picture
      Account *account = Account::connectedAccount;
      int currentSize = account->getSettingInt( "ContactListPictureSize" );
      bool increment = ( wheelEvent->delta() > 0 );

      // Set the new mode
      int newMode = 0;
      switch( currentSize )
      {
        case 0:
          newMode = ( increment ? 32 : 0 );
          break;
        case 32:
          newMode = ( increment ? 40 : 0 );;
          break;
        case 40:
          newMode = ( increment ? 48 : 32 );;
          break;
        case 48:
          newMode = ( increment ? 48 : 40 );;
          break;
        default:
          break;
      }

      account->setSetting( "ContactListPictureSize", newMode );
    }
  }
  else if( obj == this )
  {
    if( eventType == QEvent::KeyPress )
    {
      int key = static_cast<QKeyEvent*>( event )->key();

      // Detect enter key press to open a chat with the selected contact
      if( key == Qt::Key_Return || key == Qt::Key_Enter )
      {
        const QModelIndexList& selection( selectionModel_->selectedIndexes() );

        // Get the currently selected item
        if( ! selection.isEmpty() )
        {
          slotItemClicked( selection.first(), false );
        }
      }
    }
  }

  return false;  // don't stop processing.
}



// Initialize the contact and group context menus
bool ContactListWidget::initializeActions()
{
  // Contact context menu actions
  chatWithContact_   = new KAction( KIcon("user-group-new"),   i18n("Cha&t"), this );
  emailContact_      = new KAction( KIcon("mail-message-new"), i18n("&Send Email"), this );
  msnProfile_        = new KAction( KIcon("preferences-desktop-user"),    i18n("&View Profile"), this );
  chatHistory_       = new KAction( KIcon("chronometer"),      i18n("Show Chat &History..."), this );
  contactProperties_ = new KAction( KIcon("user-properties"),  i18n("&Properties"), this );

  addContact_        = new KAction( KIcon("list-add"),         i18n("&Add Contact"), this );
  allowContact_      = new KAction( KIcon("dialog-ok-apply"),  i18n("A&llow Contact"), this );
  blockContact_      = new KAction( KIcon("dialog-cancel"),    i18n("&Block Contact"), this );
  unblockContact_    = new KAction( KIcon("dialog-ok"),        i18n("&Unblock Contact"), this );
  removeContact_     = new KAction( KIcon("list-remove-user"), i18n("&Delete Contact"), this );
  removeFromGroup_   = new KAction( KIcon("list-remove"),      i18n("&Remove From Group"), this );

  popupCopyFriendlyName_    = new KAction(                     i18n("&Friendly Name"),    this );
  popupCopyPersonalMessage_ = new KAction(                     i18n("&Personal Message"), this );
  popupCopyHandle_          = new KAction(                     i18n("&Email Address"),    this );
  popupCopyMusic_           = new KAction(                     i18n("Song &Name"),        this );

  // Group context menu actions
  moveGroupDown_ = new KAction( KIcon("arrow-down"),  i18n("Move Group &Down"), this );
  moveGroupUp_   = new KAction( KIcon("arrow-up"),    i18n("Move Group &Up"),   this );
  removeGroup_   = new KAction( KIcon("edit-delete"), i18n("Re&move Group"),    this );
  renameGroup_   = new KAction( KIcon("edit"),        i18n("Re&name Group"),    this );

  // Connect the contact context menu actions
  connect( chatWithContact_,   SIGNAL(activated()),   this,  SLOT(slotForwardStartChat())      );
  connect( emailContact_,      SIGNAL(activated()),   this,  SLOT(slotEmailContact())          );
  connect( msnProfile_,        SIGNAL(activated()),   this,  SLOT(slotShowContactProfile())    );
  connect( chatHistory_,       SIGNAL(activated()),   this,  SLOT(slotShowChatHistory())       );
  connect( contactProperties_, SIGNAL(activated()),   this,  SLOT(slotShowContactProperties()) );

  connect( addContact_,        SIGNAL(activated()),   this,  SLOT(slotContactAdd())     );
  connect( allowContact_,      SIGNAL(activated()),   this,  SLOT(slotContactAllow())   );
  connect( blockContact_,      SIGNAL(activated()),   this,  SLOT(slotContactBlock())   );
  connect( unblockContact_,    SIGNAL(activated()),   this,  SLOT(slotContactUnblock()) );
  connect( removeContact_,     SIGNAL(activated()),   this,  SLOT(slotContactRemove())  );
  connect( removeFromGroup_,   SIGNAL(activated()),   this,  SLOT(slotContactRemoveFromGroup()));

  connect( popupCopyFriendlyName_,    SIGNAL(triggered(bool)),   this,  SLOT( copyText()     ) );
  connect( popupCopyPersonalMessage_, SIGNAL(triggered(bool)),   this,  SLOT( copyText()     ) );
  connect( popupCopyHandle_,          SIGNAL(triggered(bool)),   this,  SLOT( copyText()     ) );
  connect( popupCopyMusic_,           SIGNAL(triggered(bool)),   this,  SLOT( copyText()     ) );

  // Connect the group context menu actions
  connect( moveGroupDown_, SIGNAL(activated()), this, SLOT(   slotMoveGroup() ) );
  connect( moveGroupUp_,   SIGNAL(activated()), this, SLOT(   slotMoveGroup() ) );
  connect( removeGroup_,   SIGNAL(activated()), this, SLOT(   slotGroupRemove() ) );
  connect( renameGroup_,   SIGNAL(activated()), this, SLOT(   slotGroupRename() ) );

  // Create the signal mappers needed to assign all actions to the move/copy contact slots
  contactCopyMapper_ = new QSignalMapper( this );
  contactMoveMapper_ = new QSignalMapper( this );
  connect( contactCopyMapper_, SIGNAL( mapped(QString) ), this, SLOT( slotContactAddToGroup(QString) ) );
  connect( contactMoveMapper_, SIGNAL( mapped(QString) ), this, SLOT(       slotContactMove(QString) ) );

  // Initialize the sub menus to copy/move contacts
  copyContactToGroup_ = new KActionMenu( i18n("&Copy to Group"), this );
  moveContactToGroup_ = new KActionMenu( i18n("&Move to Group"), this );

  // Initialize Copy sub popups
  popupCopyMenu_ = new KActionMenu( i18n("&Copy"), this );
  popupCopyMenu_ ->addAction( popupCopyFriendlyName_ );
  popupCopyMenu_ ->addAction( popupCopyPersonalMessage_ );
  popupCopyMenu_ ->addAction( popupCopyHandle_ );
  popupCopyMenu_ ->addAction( popupCopyMusic_ );
  popupCopyMenu_->addSeparator();

  // Initialize the contact context menu
  // If you re-organize this menu, also consider updating ContactFrame to make them visually similar.
  contactActionPopup_ = new KMenu( "KMess", this );

  contactActionPopup_->addAction( chatWithContact_ );
  contactActionPopup_->addAction( emailContact_ );
  contactActionPopup_->addAction( msnProfile_ );
  contactActionPopup_->addAction( chatHistory_ );
  contactActionPopup_->addAction( popupCopyMenu_ );

  contactActionPopup_->addSeparator();

  contactActionPopup_->addAction( moveContactToGroup_ );
  contactActionPopup_->addAction( copyContactToGroup_ );
  contactActionPopup_->addAction( removeFromGroup_ );

  contactActionPopup_->addSeparator();

  contactActionPopup_->addAction( addContact_ );
  contactActionPopup_->addAction( allowContact_ );
  contactActionPopup_->addAction( removeContact_ );

  contactActionPopup_->addAction( blockContact_ );
  contactActionPopup_->addAction( unblockContact_ );

  contactActionPopup_->addSeparator();

  contactActionPopup_->addAction( contactProperties_ );

  // Initialize the group context menu
  groupActionPopup_ = new KMenu( "KMess", this );

  groupActionPopup_->addAction( moveGroupUp_ );
  groupActionPopup_->addAction( moveGroupDown_ );
  groupActionPopup_->addSeparator();

  groupActionPopup_->addAction( renameGroup_ );
  groupActionPopup_->addAction( removeGroup_ );

  return true;
}



// Generate the lists of actions for the "copy/move contact to group" menus
void ContactListWidget::rebuildActions()
{
  // Remove all copy and move actions
  foreach( KAction *action, groupCopyActionsList_ )
  {
    copyContactToGroup_->removeAction( action );
    groupCopyActionsList_.removeAll( action );
    delete action;
  }
  foreach( KAction *action, groupMoveActionsList_ )
  {
    moveContactToGroup_->removeAction( action );
    groupMoveActionsList_.removeAll( action );
    delete action;
  }

  // Create the list of groups between which contacts can be moved & copied
  GroupsList groupsList( globalSession->contactList()->groups() );
  foreach( KMess::MsnGroup *group, groupsList )
  {
    // Disallow copying and moving to special groups
    if( group->isSpecialGroup() )
    {
      continue;
    }

    // Since we need both a name to display and a group ID to distinguish groups,
    // but KActions only support the name, we'll use the Qt objectName to store
    // their internal id
    const QString& id( group->id() );
    KAction *copyAction = new KAction( group->name(), copyContactToGroup_ );
    KAction *moveAction = new KAction( group->name(), moveContactToGroup_ );
    copyAction->setObjectName( id );
    moveAction->setObjectName( id );

    copyContactToGroup_->addAction( copyAction );
    moveContactToGroup_->addAction( moveAction );

    connect( copyAction, SIGNAL(triggered(bool)), contactCopyMapper_, SLOT(map()) );
    connect( moveAction, SIGNAL(triggered(bool)), contactMoveMapper_, SLOT(map()) );
    contactCopyMapper_->setMapping( copyAction, id );
    contactMoveMapper_->setMapping( moveAction, id );

    groupCopyActionsList_.append( copyAction );
    groupMoveActionsList_.append( moveAction );
  }
}



// Initialize the widget with a model
void ContactListWidget::setModel( QAbstractItemModel *viewModel )
{
#ifdef KMESSDEBUG_CONTACTLISTWIDGET_GENERAL
  kmDebug() << "Changing contact list view model:" << viewModel;
#endif

  viewModel_->setSourceModel( viewModel );

  // Update the view with the new data
  slotUpdateGroups();
}



// A group was expanded or collapsed
void ContactListWidget::slotGroupChanged( const QModelIndex &index )
{
  // When searching, the expanded status of the groups is altered, but the settings
  // should not be applied to the actual groups
  if( ! viewModel_->filterRegExp().isEmpty() )
  {
    return;
  }

  // Do nothing if the group is empty
  if( index.child( 0, 0 ) == QModelIndex() )
  {
    return;
  }

#ifdef KMESSDEBUG_CONTACTLISTMODEL
  kmDebug() << "Changed index:" << index;
  kmDebug() << "Current index at that row:" << viewModel_->index( index.row(), 0, QModelIndex() );
#endif

  // Skip invalid items
  if( ! index.isValid()
  ||  ! viewModel_->hasIndex( index.row(), 0, QModelIndex() )
  ||  ! index.data().canConvert( QVariant::Map ) )
  {
#ifdef KMESSDEBUG_CONTACTLISTWIDGET_ITEM_VISIBILITY
    kmDebug() << "Skipping an invalid index:" << index;
#endif
    return;
  }

  // Get the data from the selected index
  const KMess::ModelDataList& itemData( index.data().toMap() );

  // Do nothing if the input index was not valid
  if( itemData.isEmpty() || itemData[ "type" ] != KMess::ContactListModelItem::ItemGroup )
  {
    return;
  }

  KMess::MsnGroup *group = globalSession->contactList()->group( itemData[ "id" ].toString() );

  // Stop if the group had not been found
  if( ! group )
  {
    return;
  }

  Group guiGroup( group );
  guiGroup->setExpanded( contactListView_->isExpanded( index ) );

  // Save the changed setting
  contactsManager->saveGroup( group );
}



// Show the context menu
void ContactListWidget::showContextMenu( const QPoint &point )
{
  // Get the data from the selected index
  const KMess::ModelDataList& itemData( getItemData( contactListView_->indexAt( point ) ) );

  // The index was empty
  if( itemData.isEmpty() )
  {
    return;
  }

  // Depending on the item type, show the appropriate menu
  if( itemData[ "type" ] == KMess::ContactListModelItem::ItemContact )
  {
    const QString& handle( itemData[ "handle" ].toString() );
    const Contact contact ( globalSession->contactList()->contact( handle ) );

    bool isBlocked = contact->isBlocked();
    bool isFriend  = contact->isFriend();
    QList<KMess::MsnGroup*> contactGroups = contact->msnContact()->groups();

    // Only if not added yet
    addContact_    ->setVisible( ! isFriend );

    // Only allow if contact is on the "removed" group.
    allowContact_  ->setVisible( ! isFriend && ! contact->isAllowed() && ! isBlocked );

    // Only one of these is enabled:
    blockContact_  ->setVisible( ! isBlocked );
    unblockContact_->setVisible(   isBlocked );

    removeContact_ ->setVisible(   isFriend  );

    // Only show the group management options when it makes sense to; contacts
    // in the Allowed and Removed groups are technically not part of your friends list
    if( itemData[ "group" ] == KMess::SpecialGroups::ALLOWED
    ||  itemData[ "group" ] == KMess::SpecialGroups::REMOVED )
    {
      copyContactToGroup_->setVisible( false );
      moveContactToGroup_->setVisible( false );
      removeFromGroup_   ->setVisible( false );
    }
    else if(    Account::connectedAccount->getSettingInt( "ContactListDisplayMode" ) == Account::VIEW_BYSTATUS
                || ( Account::connectedAccount->getSettingInt( "ContactListDisplayMode" ) == Account::VIEW_MIXED
                     && ! contact->isOnline() )
           )
    {
      copyContactToGroup_->setVisible( true );
      moveContactToGroup_->setVisible( false );
      removeFromGroup_   ->setVisible( false);
    }
    else if( contactGroups.isEmpty() )
    {
      moveContactToGroup_->setVisible( true );
      copyContactToGroup_->setVisible( false );
      removeFromGroup_   ->setVisible( false );
    }
    else
    {
      moveContactToGroup_->setVisible( isFriend );
      copyContactToGroup_->setVisible( isFriend );
      removeFromGroup_   ->setVisible( isFriend );
    }

    // Show all copy and move actions
    foreach( KAction *action, groupCopyActionsList_ )
      action->setVisible( true );
    foreach( KAction *action, groupMoveActionsList_ )
      action->setVisible( true );

    // Then hide only the invalid choices
    int enabledCopyActions = groupCopyActionsList_.count();
    int enabledMoveActions = groupMoveActionsList_.count();
    foreach( KMess::MsnGroup *g, contactGroups )
    {
      foreach( KAction *action, groupCopyActionsList_ )
      {
        // See initContactPopup() for the choice of using objectName to store the group ID
        if( action->objectName() == g->id() )
        {
          enabledCopyActions--;
          action->setVisible( false );
          break;
        }
      }
      foreach( KAction *action, groupMoveActionsList_ )
      {
        // See initContactPopup() for the choice of using objectName to store the group ID
        if( action->objectName() == g->id() )
        {
          enabledMoveActions--;
          action->setVisible( false );
          break;
        }
      }
    }

    // If no actions have been enabled in a menu, disable it too
    if( enabledCopyActions <= 0 )
    {
      copyContactToGroup_->setVisible( false );
    }
    if( enabledMoveActions <= 0 )
    {
      moveContactToGroup_->setVisible( false );
    }

    // Clear the previous actions to avoid duplication of links
    qDeleteAll( groupCopyLinkActionsList_ );
    groupCopyLinkActionsList_.clear();

     // If detailed contact information is available, search for links
    if( contact.isValid() )
    {
      // Append friendly name and personal message so cycle only one time to search links
      const QString& nameAndPm( contact->getFriendlyName() + ' ' + contact->getPersonalMessage() );
      QStringList links;

      //Initialize index, link RegExp and found boolean for insert separator only at first found link
      int pos = 0;
      QRegExp linkRegExp( "(http://|https://|ftp://|sftp://|www\\..)\\S+" );

      while( ( pos = linkRegExp.indexIn( nameAndPm, pos ) ) != -1 )
      {
        // Grep the found link and update the position for the next cycle
        QString foundLink( linkRegExp.cap( 0 ) );
        pos += linkRegExp.matchedLength();

        // Skip duplicated links
        if( links.contains( foundLink ) )
        {
          continue;
        }
        links.append( foundLink );

        // Put the found link into KAction, replace & with && to avoid shortcut problem and use tooltip
        // for future grep of link ( to avoid shortcut problem too )
        KAction *popupCopyLink = new KAction( foundLink.replace( '&', "&&" ), this );
        popupCopyLink->setToolTip( foundLink );
        connect( popupCopyLink, SIGNAL( triggered( bool ) ), this, SLOT( copyText() ) );

        // Append current KAction to the copy link list
        groupCopyLinkActionsList_.append( popupCopyLink );

        // Add action to copy menu
        popupCopyMenu_->addAction( popupCopyLink );
      }
    }

    // Forbid chatting with yourself
    chatWithContact_->setEnabled( handle != globalSession->sessionSettingString( "AccountHandle" ) );

    // Set tooltip ( informations ) of other copy actions
    popupCopyFriendlyName_   ->setToolTip( contact->getFriendlyName() );
    popupCopyPersonalMessage_->setToolTip( contact->getPersonalMessage() );
    popupCopyHandle_         ->setToolTip( contact->getHandle() );
    popupCopyMusic_          ->setToolTip( contact->msnContact()->media() );
    // Hide the unavailable options
    popupCopyPersonalMessage_->setVisible( ! contact->getPersonalMessage   ().isEmpty() );
    popupCopyMusic_          ->setVisible( contact->msnContact()->media().type != KMess::MediaNone );

    // Show the popup
    contactActionPopup_->popup( QCursor::pos() );
  }
  else if( itemData[ "type" ] == KMess::ContactListModelItem::ItemGroup )
  {
    const Group group ( globalSession->contactList()->group( itemData[ "id" ].toString() ) );

    // No context menu for special groups
    if( ! group.isValid() || group->isSpecialGroup() )
    {
      return;
    }

    int pos = group->getSortPosition();

    moveGroupUp_  ->setEnabled( getGroupAtSortPosition( pos - 1 ).isValid() );
    moveGroupDown_->setEnabled( getGroupAtSortPosition( pos + 1 ).isValid() );

    // Show the popup
    groupActionPopup_->popup( QCursor::pos() );
  }
  else
  {
    kmWarning() << "Invalid type of list item" << itemData[ "type" ];
  }
}



// Copy some details of the contact to the clipboard.
void ContactListWidget::copyText()
{
  // Grep the action sender for copy the tooltip that contains the information
  KAction *action = static_cast<KAction*>( const_cast<QObject*>( sender() ) );
  kapp->clipboard()->setText( action->toolTip() );
}



// Show the contact & group tool tip
void ContactListWidget::showToolTip( const QPoint &point )
{
  const QModelIndex& index( contactListView_->indexAt( contactListView_->mapFromGlobal( point ) ) );

  // Do nothing if the index is not valid
  if( ! index.isValid() )
  {
    return;
  }

  // The tooltip takes care of showing the right tooltip for the current item
  DynamicToolTip::show( index, point, this, contactListView_->visualRect( index ) );
}



// Email the current contact
void ContactListWidget::slotEmailContact()
{
  // Get the data from the currently selected index
  const KMess::ModelDataList& itemData( getItemData() );

  // The index was not valid, do nothing
  if( itemData.isEmpty() || itemData[ "type" ] != KMess::ContactListModelItem::ItemContact )
  {
    return;
  }

  // Send mail to that account
  KMessShared::openEmailClient( itemData[ "handle" ].toString() );
}


// An item was double clicked
void ContactListWidget::slotItemDoubleClicked( const QModelIndex &index )
{
  // Detect accidental clicks
  if( KGlobalSettings::singleClick() )
  {
#ifdef KMESSDEBUG_CONTACTLISTWIDGET_GENERAL
    kmDebug() << "Accidental double click detected, skipping click event.";
#endif
    return;
  }

  slotItemClicked( index );
}



void ContactListWidget::slotItemSingleClicked( const QModelIndex &index )
{
  bool accidental = false;
  if( ! KGlobalSettings::singleClick() )
  {
#ifdef KMESSDEBUG_CONTACTLISTWIDGET_GENERAL
    kmDebug() << "Accidental single click detected, skipping click event.";
#endif

    accidental = true;
  }

  slotItemClicked( index, accidental );
}



// An item was clicked
void ContactListWidget::slotItemClicked( const QModelIndex &index, const bool accidental )
{
  // Get the data from the selected index
  const KMess::ModelDataList& itemData( getItemData( index ) );

  // The index was empty
  if( itemData.isEmpty() )
  {
    kmWarning() << "Invalid list index given!";
    return;
  }

  switch( itemData[ "type" ].toInt() )
  {
    // Expand/collapse the groups when clicked
    case KMess::ContactListModelItem::ItemGroup:
    {
      // Do nothing if the group is empty
      if( index.child( 0, 0 ) == QModelIndex() )
      {
        return;
      }

      Group guiGroup( itemData[ "id" ].toString() );
      guiGroup->setExpanded( ! guiGroup->isExpanded() );

      // Keep the group expansion state volatile while filtering (don't save it)
      if( ! viewModel_->filterRegExp().isEmpty() )
      {
        contactListView_->setExpanded( index, ! contactListView_->isExpanded( index ) );
      }
      else
      {
        contactListView_->setExpanded( index, guiGroup->isExpanded() );
      }
      break;
    }

    //  Request a chat with that contact
    case KMess::ContactListModelItem::ItemContact:
      if( ! accidental )
      {
        // Tell the current session we'd like to start a chat.
        KMessApplication::instance()->chatMaster()->requestChat( itemData[ "handle" ].toString() );
      }
      break;


    default:
#ifdef KMESSDEBUG_CONTACTLISTWIDGET_GENERAL
      kmWarning() << "Nothing to do!";
#endif
      break;
  }
}



/**
 * Given a target group, which group is next in the list (either above or below)?
 */
Group ContactListWidget::getGroupAtSortPosition( int position )
{
  foreach(KMess::MsnGroup *g, globalSession->contactList()->groups())
  {
    Group group ( g );
    if ( group->getSortPosition() == position )
    {
      return group;
    }
  }

  return Group();
}



/**
 * Moves a group in the contact list up or down one place.
 *
 * Whether it moves up or down depends on the sender of the signal.
 */
void ContactListWidget::slotMoveGroup()
{
  // Get the group data from the selected index
  const KMess::ModelDataList& itemData( getItemData() );
  Group group ( globalSession->contactList()->group( itemData[ "id" ].toString() ) );
  if( ! group || itemData.isEmpty() )
  {
    return;
  }

  int sortPosition = group->getSortPosition();

  if ( sender() == moveGroupDown_ )
  {
    sortPosition++;
  }
  else
  {
    sortPosition--;
  }

  Group replacement = getGroupAtSortPosition( sortPosition );

  int oldPos = group->getSortPosition();
  int newPos = sortPosition;

  replacement->setSortPosition( oldPos );
  group->setSortPosition( newPos );

  viewModel_->invalidate();
}




// Perform the "add contact" action
void ContactListWidget::slotContactAdd()
{
  // Get the data from the currently selected index
  const KMess::ModelDataList& itemData( getItemData() );

  // The index was not empty
  if( itemData.isEmpty() || itemData[ "type" ] != KMess::ContactListModelItem::ItemContact )
  {
    return;
  }

  QString handle( itemData[ "handle" ].toString() );
  KMess::MsnContact *contact = globalSession->contactList()->contact( handle );
  int result = KMessageBox::No;

  if ( contact && contact->isBlocked() )
  {
    result = KMessageBox::questionYesNoCancel( this
                                          , i18n( "<html><b>%1</b> is currently blocked. Unblock them after adding them to the contact list?</html>",
                                            handle )
                                          , i18n( "Add Contact" )
                                          , KStandardGuiItem::yes()
                                          , KStandardGuiItem::no()
                                          , KStandardGuiItem::cancel()
                                          , "UnblockAfterAdd"
                                          );
  }
  
  if ( result == KMessageBox::Cancel )
  {
    return;
  }
  
  // Add that contact
  globalSession->contactList()->addContact( contact );
  
  // unblock if desired.
  if ( result == KMessageBox::Yes )
  {
    globalSession->contactList()->unblockContact( contact );
  }
}



// Perform the "allow contact" action
void ContactListWidget::slotContactAllow()
{
  // Get the data from the currently selected index
  const KMess::ModelDataList& itemData( getItemData() );

  // The index was not empty
  if( itemData.isEmpty() || itemData[ "type" ] != KMess::ContactListModelItem::ItemContact )
  {
    return;
  }

  // Allow that contact
  globalSession->contactList()->unblockContact( globalSession->contactList()->contact( itemData[ "handle" ].toString() ) );
}



// Perform the "block contact" action
void ContactListWidget::slotContactBlock()
{
  // Get the data from the currently selected index
  const KMess::ModelDataList& itemData( getItemData() );

  // The index was not empty
  if( itemData.isEmpty() || itemData[ "type" ] != KMess::ContactListModelItem::ItemContact )
  {
    return;
  }

  // Block that contact
  globalSession->contactList()->blockContact( globalSession->contactList()->contact( itemData[ "handle" ].toString() ) );
}



// Perform the "copy contact to a group" action
void ContactListWidget::slotContactAddToGroup( const QString &newGroupId )
{
  // Get the data from the currently selected index
  const KMess::ModelDataList& itemData( getItemData() );

  // The index was not empty
  if( itemData.isEmpty() || itemData[ "type" ] != KMess::ContactListModelItem::ItemContact )
  {
    return;
  }

  // Copy that contact
  globalSession->contactList()->addContactToGroup( globalSession->contactList()->contact( itemData[ "handle" ].toString() ), 
                                                   globalSession->contactList()->group( newGroupId ) );
}



// Perform the "move contact between groups" action
void ContactListWidget::slotContactMove( const QString &newGroupId )
{
  // Get the data from the currently selected index
  const KMess::ModelDataList& itemData( getItemData() );

  // The selection was not valid
  if( itemData.isEmpty() || itemData[ "type" ] != KMess::ContactListModelItem::ItemContact )
  {
    return;
  }

  // Get the old group ID: can be nothing if it's moving from a special group
  QString oldGroupId;
  if( itemData[ "isInSpecialGroup" ].toBool() == false )
  {
    oldGroupId = itemData[ "group" ].toString();
  }

  // If the old and new groups are the same one, do nothing
  if( oldGroupId == newGroupId )
  {
    return;
  }

  // Move that contact
  globalSession->contactList()->moveContact( globalSession->contactList()->contact( itemData[ "handle" ].toString() ), 
                                             globalSession->contactList()->group( oldGroupId ), 
                                             globalSession->contactList()->group( newGroupId ) );
}



// Perform the "remove contact" action
void ContactListWidget::slotContactRemove()
{
  // Get the data from the currently selected index
  const KMess::ModelDataList& itemData( getItemData() );

  // The index was empty
  if( itemData.isEmpty() || itemData[ "type" ] != KMess::ContactListModelItem::ItemContact )
  {
    return;
  }

  KMess::MsnContact *contact = globalSession->contactList()->contact( itemData[ "handle" ].toString() );
  const QString &handle( itemData[ "handle" ].toString() );
  
  if ( ! contact )
  {
    KMessageBox::error( this, 
                          i18n( "<html>The contact <b>%1</b> is not on your contact list.</html>", handle ),
                          i18n( "Remove Contact" )
                        );
    return;
  }
  
  KDialog *d = new KDialog();
  d->setButtons( KDialog::Cancel | KDialog::Yes | KDialog::Ok );
  d->setButtonGuiItem( KDialog::Yes, KGuiItem(i18n("Remove"), "edit-delete") );
  d->setButtonGuiItem( KDialog::Ok, KGuiItem(i18n("Remove and Block"), "list-remove") );
  
  bool removeFromWL = false;
  int result = KMessageBox::createKMessageBox( d,
                                               QMessageBox::Question
                                               , i18n( "<html>Are you sure you want to remove the "
                                                      "contact <b>%1</b> from your contact list?</html>",
                                                      handle )
                                               , QStringList()
                                               , i18n( "Remove this contact from my Windows Live address book also" )
                                               , &removeFromWL
                                               , KMessageBox::Dangerous );
  /*
  int result = KMessageBox::warningYesNoCancel( this
                                              , i18n( "<html>Are you sure you want to remove the "
                                                      "contact <b>%1</b> from your contact list?</html>",
                                                      handle )
                                              , i18n( "Remove Contact" )
                                              , KGuiItem(i18n("Remove"), "edit-delete")           // Yes
                                              , KGuiItem(i18n("Remove and Block"), "list-remove") // No
                                              , KStandardGuiItem::cancel()
                                              , QString()
                                              , KMessageBox::Dangerous
                                              );*/

  switch( result )
  {
    // User has pressed "Remove and block"
    case KDialog::Ok :
      globalSession->contactList()->removeContact( contact, 
                                                   ( removeFromWL ? KMess::RemoveFromWindowsLive : KMess::RemoveFromMessenger ), 
                                                   KMess::RemoveAndBlock );
      break;

    // User has pressed "Remove only"
    case KDialog::Yes :
      globalSession->contactList()->removeContact( contact,
                                                   ( removeFromWL ? KMess::RemoveFromWindowsLive : KMess::RemoveFromMessenger ) );
      break;

    // User has canceled the action, do nothing
    case KDialog::Cancel:
    default:
      break;
  }
}



// Perform the "remove contact from group" action
void ContactListWidget::slotContactRemoveFromGroup()
{
  // Get the data from the currently selected index
  const KMess::ModelDataList& itemData( getItemData() );

  // The index was invalid
  if( itemData.isEmpty() )
  {
    return;
  }

  // Get the ID of the group which contained this instance of the contact
  const KMess::ModelDataList& parentItemData( getItemData( selectionModel_->selectedIndexes().first().parent() ) );
  if( parentItemData.isEmpty() || parentItemData[ "type" ] != KMess::ContactListModelItem::ItemGroup )
  {
    return;
  }

  // Remove that contact
  globalSession->contactList()->removeContactFromGroup( globalSession->contactList()->contact( itemData[ "handle" ].toString() ),
                                                        globalSession->contactList()->group( parentItemData[ "id" ].toString() ) );
}



// Perform the "remove group" action
void ContactListWidget::slotGroupRemove()
{
  // Get the data from the currently selected index
  const KMess::ModelDataList& itemData( getItemData() );

  // The index was invalid
  if( itemData.isEmpty() || itemData[ "type" ] != KMess::ContactListModelItem::ItemGroup )
  {
    return;
  }


  const QString &groupId( itemData[ "id" ].toString() );

  KMess::MsnGroup *group = globalSession->contactList()->group( groupId );

  // If the group was not found, cancel
  if( group == 0 )
  {
    kmDebug() << "Couldn't find a matching group.";
    return;
  }

  // If the group was not empty, bail also out. We would receive an error from the server, otherwise
  if( ! group->isEmpty() )
  {
    // TODO Allow the user to choose between: 1) delete or delete+block all contacts in the group;
    // 2) remove all contacts from the group; 3) cancel the operation.
    KMessageBox::error( this,
                        i18nc( "dialog text", "The group <b>%1</b> is not empty! First remove all contacts from it, then try again!",
                               group->name() ),
                        i18nc( "dialog title", "Group Removal" ) );
    return;
  }

  // Prompt to remove
  int result = KMessageBox::warningContinueCancel( this
                                                 , i18nc( "dialog text",
                                                          "<html>Are you sure you want to remove "
                                                          "the group <b>%1</b> from your contact "
                                                          "list?</html>",
                                                          group->name() )
                                                 , i18nc( "dialog title", "Group Removal" )
                                                 , KGuiItem( i18nc( "dialog button", "Remove" ),
                                                             "edit-delete" )
                                                 , KStandardGuiItem::cancel()
                                                 , QString()
                                                 , KMessageBox::Dangerous
                                                 );

  if( result == KMessageBox::Continue )
  {
    globalSession->contactList()->removeGroup( group );
  }
}



// Perform the "rename group" action
void ContactListWidget::slotGroupRename()
{
  // Get the data from the currently selected index
  const KMess::ModelDataList& itemData( getItemData() );

  // The index was invalid
  if( itemData.isEmpty() || itemData[ "type" ] != KMess::ContactListModelItem::ItemGroup )
  {
    return;
  }

  const QString &groupId( itemData[ "id" ].toString() );

  KMess::MsnGroup *group = globalSession->contactList()->group( groupId );

  // If the group was not found, cancel
  if ( group == 0 )
  {
    kmDebug() << "Couldn't find a matching group.";
    return;
  }

  // Special groups cannot be altered or removed
  if( group->isSpecialGroup() )
  {
    KMessageBox::error( 0, i18n( "This is a special group, which cannot be changed." ) );
    return;
  }

  bool okPressed = false;

  // Otherwise, get the new group name..
  const QString& currentName( group->name() );

  // Launch a dialog to get a new group name
  const QString& newGroupName( KInputDialog::getText( i18n( "Rename Group" ),
                                                      i18n( "Enter a new name for this group:" ),
                               currentName,
                               &okPressed,
                               this ) );

  if( okPressed && currentName != newGroupName )
  {
    // Request that the group be renamed
    globalSession->contactList()->renameGroup( group, newGroupName );
  }
}



// Forward the "start chat" menu action
void ContactListWidget::slotForwardStartChat()
{
  // Only one index can be returned at once by the contact list (has single selection mode)
  slotItemClicked( selectionModel_->selectedIndexes().first() );
}



// Perform the "unblock contact" menu action
void ContactListWidget::slotContactUnblock()
{
  // Get the data from the currently selected index
  const KMess::ModelDataList& itemData( getItemData() );

  // The index was not empty
  if( itemData.isEmpty() || itemData[ "type" ] != KMess::ContactListModelItem::ItemContact )
  {
    return;
  }

  // Unblock that contact
  globalSession->contactList()->unblockContact( globalSession->contactList()->contact( itemData[ "handle" ].toString() ) );
}



// Delay a bit updating the groups status
void ContactListWidget::slotScheduleGroupUpdate()
{
#ifdef KMESSDEBUG_CONTACTLISTWIDGET_ITEM_VISIBILITY
  kmDebug() << "Scheduling another groups update";
#endif
  updateTimer_.start( 250 );
}



// Slot for searching contact
void ContactListWidget::slotSearchContact( const QString& searchFor )
{
#ifdef KMESSDEBUG_CONTACTLISTWIDGET_ITEM_VISIBILITY
  kmDebug() << "Search expression changed to:" << searchFor;
#endif

  QString searchExpression;
  const QString& regexpMagic( "regexp:" );

  // If the search string starts with "regexp:", allow searching with a regexp :)
  if( searchFor.startsWith( regexpMagic ) )
  {
    searchExpression = searchFor.mid( regexpMagic.length() );

    // Colorize the search bar when using regexps, according to if they're valid or invalid
    if( QRegExp( searchExpression ).isValid() )
    {
      searchEdit_->setStyleSheet( "background-color: #bbffbb" );
    }
    else
    {
      searchEdit_->setStyleSheet( "background-color: #ffbbbb" );
    }
  }
  else
  {
    // Remove trailing spaces and other unneeded (invisible) characters, then escape the searchstring
    searchExpression = QRegExp::escape( searchFor.simplified() );
    searchEdit_->setStyleSheet( "" );
  }

  // True if the user was filtering for something and wanted to filter for something else;
  // used to maintain the group expansion states while refining the search terms
  bool isFilteringAgain = ( ! searchExpression.isEmpty() && ! viewModel_->filterRegExp().isEmpty() );

  viewModel_->setFilterRegExp( searchExpression );

  // Show or hide groups which do or do not contain results

  for( int row = viewModel_->rowCount() - 1; row >= 0; --row )
  {
    const QModelIndex& index( viewModel_->index( row, 0, QModelIndex() ) );

    // Skip invalid items
    if( ! index.isValid() )
    {
#ifdef KMESSDEBUG_CONTACTLISTWIDGET_ITEM_VISIBILITY
      kmDebug() << "Skipping invalid index" << index;
#endif
      continue;
    }

    // Access the index's data
    const KMess::ModelDataList& itemData( getItemData( index ) );

    // Skip items which have no children
    if( itemData.isEmpty() || itemData[ "type" ].toInt() != KMess::ContactListModelItem::ItemGroup )
    {
#ifdef KMESSDEBUG_CONTACTLISTWIDGET_ITEM_VISIBILITY
      kmDebug() << "Skipping non-group index" << index;
      kmDebug() << "-- Index data:" << itemData;
#endif
      continue;
    }

    // Hide groups which don't contain results
    if( ! searchFor.isEmpty() )
    {
      bool isHidden = ( ! viewModel_->hasChildren( index ) );

      contactListView_->setRowHidden( row, QModelIndex(), isHidden );

      if( ! isFilteringAgain )
      {
        contactListView_->setExpanded( index, true );
      }
    }
    // When the search terms are removed, enable back all groups
    else
    {
      contactListView_->setRowHidden( row, QModelIndex(), false );

      Group g( itemData[ "id" ].toString() );
      contactListView_->setExpanded( index, g->isExpanded() );
    }
  }
}



// Display the chat history of the current contact
void ContactListWidget::slotShowChatHistory()
{
  // Get the data from the currently selected index
  const KMess::ModelDataList& itemData( getItemData() );

  // The index was not empty
  if( itemData.isEmpty() || itemData[ "type" ] != KMess::ContactListModelItem::ItemContact )
  {
    return;
  }

  ChatHistoryDialog *dialog = new ChatHistoryDialog( this );

  // Show the selected contact. If no log exists
  // (no chats with that person yet), then don't show the History dialog
  if( dialog->setContact( itemData[ "handle" ].toString() ) )
  {
    dialog->show();
  }
  else
  {
    if( Account::connectedAccount->getSettingBool( "LoggingEnabled" ) )
    {
      KMessageBox::sorry(	this,
                          i18n( "No chat logs could be found for this contact." ),
                          i18n( "No chat history found" ) );
    }
    else
    {
      KMessageBox::sorry( this,
                          i18n( "No chat logs could be found for this contact. Note that new chats are not logged; if you want your chats to be logged, you can enable it in your account settings." ),
                          i18n( "No chat history found") );
    }

    delete dialog;
  }
}



// Display the profile of the current contact
void ContactListWidget::slotShowContactProfile()
{
  // Get the data from the currently selected index
  const KMess::ModelDataList& itemData( getItemData() );

  // The index was not empty
  if( itemData.isEmpty() || itemData[ "type" ] != KMess::ContactListModelItem::ItemContact )
  {
    return;
  }

  const QString& handle ( itemData[ "handle" ].toString() );
  KMessShared::openBrowser( KUrl( "http://members.msn.com/default.msnw?mem=" + handle + "&mkt=" + KGlobal::locale()->language() + "&pgmarket" ) );
}



// Display the properties of the current contact
void ContactListWidget::slotShowContactProperties()
{
  // Get the data from the currently selected index
  const KMess::ModelDataList& itemData( getItemData() );

  // The index was not empty
  if( itemData.isEmpty() || itemData[ "type" ] != KMess::ContactListModelItem::ItemContact )
  {
    return;
  }

  // Get the contact data to pass to the properties dialog
  KMess::MsnContact *contact = globalSession->contactList()->contact( itemData[ "handle" ].toString() );

  // If no corresponding contact has been found, do nothing
  if( ! contact )
  {
    return;
  }

  ContactPropertiesDialog *dialog = new ContactPropertiesDialog( this );
  dialog->launch( contact );
  delete dialog;
}



// Update the expanded status of the list groups
void ContactListWidget::slotUpdateGroups()
{
  // When searching, the expanded status of the groups is altered, but the settings
  // should not be applied to the actual groups
  if( ! viewModel_->filterRegExp().isEmpty() )
  {
    return;
  }

  // first, force the view model to retrieve any changed
  // viewmode settings and update itself.
  viewModel_->invalidate();

#ifdef KMESSDEBUG_CONTACTLISTWIDGET_ITEM_VISIBILITY
  kmDebug() << "Updating groups expanded status for" << viewModel_->rowCount() << "rows";
#endif

  for( int row = viewModel_->rowCount() - 1; row >= 0; row-- )
  {
    const QModelIndex& index( viewModel_->index( row, 0, QModelIndex() ) );

#ifdef KMESSDEBUG_CONTACTLISTWIDGET_ITEM_VISIBILITY
  kmDebug() << "Current row:" << row << "of" << viewModel_->rowCount();
  kmDebug() << "Index:" << index;
  kmDebug() << "Model has it?" << viewModel_->hasIndex( row, 0, QModelIndex() );
#endif

    // Skip invalid items
    if( ! index.isValid()
    ||  ! viewModel_->hasIndex( row, 0, QModelIndex() )
    ||  ! index.data().canConvert( QVariant::Map ) )
    {
#ifdef KMESSDEBUG_CONTACTLISTWIDGET_ITEM_VISIBILITY
      kmDebug() << "Skipping an invalid item at row" << row << ":" << index;
#endif
      continue;
    }

    // Access the index's data
    const KMess::ModelDataList& itemData( index.data().toMap() );

    // Skip items which have no children
    if( itemData.isEmpty() || itemData[ "type" ].toInt() != KMess::ContactListModelItem::ItemGroup )
    {
#ifdef KMESSDEBUG_CONTACTLISTWIDGET_ITEM_VISIBILITY
      kmDebug() << "Skipping a non-group at row" << row << ":" << index;
      kmDebug() << "Skipped item's details:" << itemData;
#endif
      continue;
    }

    // Set the expanded status
    Group group( itemData[ "id" ].toString() );
    contactListView_->setExpanded( index, group->isExpanded() );

#ifdef KMESSDEBUG_CONTACTLISTWIDGET_ITEM_VISIBILITY
    kmDebug() << "Group" << itemData[ "name" ].toString() << "is"
             << ( group->isExpanded() ? "expanded" : "contracted" );
#endif
  }

#ifdef KMESSDEBUG_CONTACTLISTWIDGET_ITEM_VISIBILITY
  kmDebug() << "Update done.";
#endif
}



// Update the contact list widgets
void ContactListWidget::slotUpdateView()
{
  // TODO do this properly, propably using properties in the stylesheet
  QString birdStyle;

  // Set a background pixmap (but allow not to, by popular request)
  if( ! Account::connectedAccount->getSettingBool( "ContactListBackgroundEnabled" ) )
  {
     birdStyle = "QTreeView { "
                 "  background-image: url(" +
                 KIconLoader::global()->iconPath( "empty", KIconLoader::User ) + ");"
                 "}"
                 ;
  }

  setStyleSheet( birdStyle );
}



// The "show search in contact list" menu item has been toggled.
void ContactListWidget::toggleShowSearchFrame( bool show )
{
  if( show )
  {
    searchFrame_->show();
    searchEdit_->setFocus();
  }
  else
  {
    // Remove the query when closing the search bar
    searchEdit_->clearFocus();
    searchEdit_->setText( QString() );

    searchFrame_->hide();
    contactListView_->setFocus();
  }
}



#include "contactlistwidget.moc"
