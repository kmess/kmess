/***************************************************************************
                          contactlistdelegate.cpp - description
                             -------------------
    begin                : Sat Feb 16 2008
    copyright            : (C) 2008 by Valerio Pilo
    email                : valerio@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "contactlistdelegate.h"

#include "../account.h"
#include "../emoticonmanager.h"
#include "../contact/status.h"
#include "../contact/contact.h"
#include "../contact/group.h"
#include "../utils/richtextparser.h"
#include "../kmessdebug.h"
#include "../kmessglobal.h"

#include <QApplication>
#include <QLabel>
#include <QModelIndex>
#include <QPainter>
#include <QTextDocument>
#include <QFontMetrics>

#include <KIconLoader>
#include <KLocale>
#include <KGlobalSettings>

#include <KMess/ContactListModelItem>

/// Default spacing between items
#define ITEM_SPACE    4


/**
 * Constructor
 *
 * Creates two labels which will be used to paint the contact list elements at the location of every item
 *
 * @param parent  The parent object, usually a QTreeView
 */
ContactListDelegate::ContactListDelegate( QWidget *parent )
    : QStyledItemDelegate( parent )
    , iconLoader_( KIconLoader::global() )
{
  // Set up the group label style
  fontBold_.setBold( true );

  // create the text document used to render HTML text labels.
  textDocument_ = new QTextDocument();
  textDocument_->setDocumentMargin( 0.0 );

  QTextOption noWrap;
  noWrap.setWrapMode( QTextOption::NoWrap );
  noWrap.setAlignment( Qt::AlignTop );

  textDocument_->setDefaultTextOption( noWrap );

  // Cache the icons for the various media types
  mediaEmoticonMusic_  = EmoticonManager::instance()->getReplacement( "(8)",  true );
  mediaEmoticonGaming_ = EmoticonManager::instance()->getReplacement( "(xx)", true );
}



/**
 * Destructor
 */
ContactListDelegate::~ContactListDelegate()
{
  delete textDocument_;
}



/**
 * Paint an item of the contact list
 *
 * This method is called whenever a contact list item needs do be drawn.
 * The 'index' parameter is used to extract the data from the list item, and depending on the list item type
 * (contact or group mainly) it decides what needs to be drawn and how.
 *
 * @param painter Painter item, we'll use it to draw our item's contents
 * @param option  Contains some details about the item we have to draw
 * @param index   Points to the actual data to be represented
 */
void ContactListDelegate::paint( QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index ) const
{
  QStyledItemDelegate::paint( painter, option, index );

  // Only render column zero
  if( index.column() != 0 )
  {
    return;
  }

  QString text;

  // Save the painter state to avoid messing up further painting operations with it
  painter->save();

  // Reset coordinate translation in the painter (defaults are relative to the parent's window) - and make rendering prettier possibly
  painter->translate( 0, 0 );
  painter->setRenderHints( QPainter::Antialiasing | QPainter::SmoothPixmapTransform |
                           QPainter::TextAntialiasing, true );

  // Read the actual data off the model index
  const KMess::ModelDataList& itemData( index.data().toMap() );

  // We're only able to display groups and contacts
  if( itemData[ "type" ].toInt() != KMess::ContactListModelItem::ItemGroup
  &&  itemData[ "type" ].toInt() != KMess::ContactListModelItem::ItemContact )
  {
    // Restore the painter to the previous state
    painter->restore();

    kmWarning() << "Unknown type of item:" << itemData[ "type" ].toInt();
    kmWarning() << "Index:" << index;
    kmWarning() << "Data:" << itemData;
    return;
  }

  int displayPictureSize = Account::connectedAccount->getSettingInt( "ContactListPictureSize" );

  // Load the required pixmaps: their sizes will be used to compute the final
  // rendering positions

  QPixmap iconPixmap;
  QPixmap picturePixmap;

  // Group pixmaps
  if( itemData[ "type" ].toInt() == KMess::ContactListModelItem::ItemGroup )
  {
    QString groupIconName;
    bool showOfflineContacts = Account::connectedAccount->getSettingBool( "ContactListOfflineGroupShow" );

    Group guiGroup( itemData[ "id" ].toString() );

    // Only show the expansion arrow if there are contacts to show
    if( (   showOfflineContacts && itemData[ "totalContacts"  ].toInt() > 0 )
    ||  ( ! showOfflineContacts && itemData[ "onlineContacts" ].toInt() > 0 ) )
    {
      if( guiGroup->isExpanded() )
      {
        groupIconName = "arrow-down";
      }
      else if( QApplication::isRightToLeft() )
      {
        groupIconName = "arrow-left";
      }
      else
      {
        groupIconName = "arrow-right";
      }
    }
    else
    {
      // Show only arrow down if there aren't contacts to show in group
      groupIconName = "arrow-down";
    }

    iconPixmap = iconLoader_->loadIcon( groupIconName
                                      , KIconLoader::NoGroup
                                      , KIconLoader::SizeSmall );
  }
  else // Contact pixmaps
  {
    // Only load the picture if it must be shown
    if( displayPictureSize != 0 )
    {
      Contact contact ( itemData[ "handle" ].toString() );
      picturePixmap = contact->getScaledDisplayPicture();
    }

    // Determine which status pixmap to load
    KMess::Flags statusFlags = ( itemData[ "isBlocked" ].toBool() ? KMess::FlagBlocked : KMess::FlagNone );
    iconPixmap = Status::getIcon( (KMess::MsnStatus) itemData[ "status" ].toInt(), statusFlags );
  }


  // Compute the positions where the objects will be painted
  QPoint originPoint;

  if( QApplication::isLeftToRight() )
  {
    originPoint = option.rect.topLeft();
    originPoint.rx()++;
  }
  else
  {
    originPoint = option.rect.topRight();
    originPoint.rx()--;
  }
  originPoint.ry()++;

  QPoint iconPoint( originPoint );
  QPoint picturePoint( originPoint );
  // Labels get adjusted to 2 pixels below the top border, because otherwise, they get painted
  // too close to it
  QRect labelRect( option.rect.adjusted( ITEM_SPACE, 2, -ITEM_SPACE, 0 ) );

  // Group item positions
  if( itemData[ "type" ].toInt() == KMess::ContactListModelItem::ItemGroup )
  {
    if( QApplication::isLeftToRight() )
    {
      // iconPoint is painted at originPoint
      labelRect.setLeft( iconPixmap.width() + ITEM_SPACE );
    }
    else
    {
      iconPoint.rx() -= iconPixmap.width();
      labelRect.setRight( iconPoint.x() - ITEM_SPACE );
    }
  }
  else // Contact item positions
  {
    if( QApplication::isLeftToRight() )
    {
      // picturePoint is painted at originPoint

      // Show the icon, then the label
      if( displayPictureSize == 0 )
      {
        labelRect.setLeft( iconPoint.x() + iconPixmap.width() + ITEM_SPACE );
      }
      else if( displayPictureSize < 48 ) // Show the picture, the icon, then the label
      {
        iconPoint.rx() = iconPoint.x() + picturePixmap.width() + ITEM_SPACE;
        labelRect.setLeft( iconPoint.x() + iconPixmap.width() + ITEM_SPACE );
      }
      else // Show the picture and the label; then show the icon over the picture, at its bottom left corner
      {
        iconPoint.ry() += displayPictureSize - KIconLoader::SizeSmall;
        labelRect.setLeft( iconPoint.x() + picturePixmap.width() + ITEM_SPACE );
      }
    }
    else
    {
      // Show the icon, then the label
      if( displayPictureSize == 0 )
      {
        iconPoint.rx() -= iconPixmap.width();
        labelRect.setRight( iconPoint.x() - ITEM_SPACE );
      }
      else if( displayPictureSize < 48 ) // Show the picture, the icon, then the label
      {
        picturePoint.rx() -= picturePixmap.width();
        iconPoint.rx() = picturePoint.x() - iconPixmap.width() - ITEM_SPACE;
        labelRect.setRight( iconPoint.x() - ITEM_SPACE );
      }
      else // Show the picture and the label; then show the icon over the picture, at its bottom right corner
      {
        picturePoint.rx() -= picturePixmap.width();
        iconPoint.rx() -= iconPixmap.width();
        iconPoint.ry() += displayPictureSize - KIconLoader::SizeSmall;
        labelRect.setRight( picturePoint.x() - ITEM_SPACE );
      }
    }
  }

  // Update: Don't colorize the line, hasUser is not reliable anymore
/*
  // If the contact hasn't us in its CL, colorize the line
  if(   itemData[ "type" ].toInt() == KMess::ContactListModelItem::ItemContact
  &&  ! itemData[ "hasUser" ].toBool() ) // The contact does not have us in its list
  {
    painter->fillRect( option.rect, QColor( 255,0,0,20 ) );
  }
*/

  // Paint the images
  if( ! picturePixmap.isNull() )
  {
    painter->drawPixmap( picturePoint, picturePixmap );
  }
  painter->drawPixmap( iconPoint, iconPixmap );

  // Paint the label: first the group case
  if( itemData[ "type" ].toInt() == KMess::ContactListModelItem::ItemGroup )
  {
    QString text;
    Group group ( itemData[ "id" ].toString() ) ;
    if( ! itemData[ "isSpecialGroup" ].toBool()
    ||    itemData[ "id" ] == KMess::SpecialGroups::INDIVIDUALS )
    {
      text = QString( "%1 (%2/%3)" ).arg( Qt::escape( group->getName() ) )
                                    .arg( itemData[ "onlineContacts" ].toString() )
                                    .arg( itemData[ "totalContacts"  ].toString() );
    }
    else
    {
      text = QString( "%1 (%2)" ).arg( Qt::escape( group->getName() ) )
                                 .arg( itemData[ "totalContacts"  ].toString() );
    }

    // The group names are always shown in bold font, to be more visible
    painter->setFont( fontBold_ );

    QFontMetrics fm( painter->font() );
    QTextOption textOpt;
    textOpt.setWrapMode( QTextOption::NoWrap );

    if( ( fm.width( text ) + labelRect.height() ) > labelRect.width() )
    {
      QImage label( labelRect.size(), QImage::Format_ARGB32 );
      label.fill( Qt::transparent );

      // create a linear gradient, white to transparent
      QLinearGradient gradient( QPoint( 0, 0 ), labelRect.bottomRight() );

      if( QApplication::isLeftToRight() )
      {
        gradient.setColorAt( 0.90, Qt::white       );
        gradient.setColorAt( 1.00, Qt::transparent );
      }
      else
      {
        gradient.setColorAt( 0.10, Qt::white       );
        gradient.setColorAt( 0.00, Qt::transparent );
      }

      QPainter pLabel( &label );
      pLabel.translate( -labelRect.topLeft() );
      pLabel.setFont( fontBold_ );
      pLabel.setRenderHints( QPainter::TextAntialiasing, true );

      pLabel.drawText( labelRect, text, textOpt );

      pLabel.setCompositionMode( QPainter::CompositionMode_DestinationIn );
      pLabel.fillRect( labelRect, QBrush( gradient ) );

      painter->drawImage( labelRect, label );
    }
    else
    {
      painter->drawText( labelRect, text, textOpt );
    }
  }
  else // then the contact case
  {
    // Get the contact information.
    QString friendlyName, personalMessage;
    Contact contact( itemData[ "handle" ].toString() );

    // Parse the name and message of the contact, adding emoticons and formatting
    if( Account::connectedAccount->getSettingBool( "ContactListTextFormattingEnabled" ) )
    {
      if( ! Account::connectedAccount->getSettingBool( "ContactListEmailAsNamesShow" ) )
      {
        friendlyName  = contact->getFriendlyName( STRING_FORMATTED );
      }
      else
      {
        friendlyName = contact->getHandle();
      }

      personalMessage = itemData[ "personalMessage" ].toString();
    }
    else
    {
      if( ! Account::connectedAccount->getSettingBool( "ContactListEmailAsNamesShow" ) )
      {
        friendlyName = itemData[ "friendly" ].toString();
        friendlyName = RichTextParser::instance()->parseMsnString( friendlyName,
            ( RichTextParser::FormattingOptions ) ( RichTextParser::PARSE_EMOTICONS | RichTextParser::EMOTICONS_SMALL ) );
      }
      else
      {
        friendlyName = itemData[ "handle" ].toString();
      }

      // Display the emoticons alone when MSN Plus formatting is disabled: the formatted
      // version has emoticons already.
      personalMessage = itemData[ "personalMessage" ].toString();
      personalMessage = RichTextParser::instance()->parseMsnString( personalMessage,
          ( RichTextParser::FormattingOptions ) ( RichTextParser::PARSE_EMOTICONS | RichTextParser::EMOTICONS_SMALL ) );
    }

    // If it's empty, then the user wants to see the email in the contact list
    if( friendlyName.isEmpty() )
    {
      friendlyName = itemData[ "handle" ].toString();
    }

    QString messageString;

    const QString& mediaString( itemData[ "mediaString" ].toString() );
    if( ! mediaString.isEmpty() )
    {
      // Determine icon for the various media types
      const QString& mediaType  ( itemData[ "mediaType" ].toString() );
      if( mediaType == "Music" )
      {
        messageString = mediaEmoticonMusic_ + "<i>" + Qt::escape( mediaString ) + "</i>";
      }
      else if( mediaType == "Gaming" || mediaType == "Game" )
      {
        messageString = mediaEmoticonGaming_ + "<i>" + Qt::escape( mediaString ) + "</i>";
      }
      else
      {
        // unknown media type, fallback to personal message.
        messageString = "<i>" + personalMessage + "</i>";
      }
    }
    else if( ! personalMessage.isEmpty() ) // When there's no media, show the PM
    {
      messageString = "<i>" + personalMessage + "</i>";
    }

    // Draw the text widget
    if( messageString.isEmpty() )
    {
      // Only person name
      text = friendlyName;
    }
    else if( ! displayPictureSize )
    {
      // Person with message after it
      text = friendlyName + "<small><font style='color:palette(window-text)'> - " + messageString + "</font></small>";
    }
    else
    {
      // Message below,
      text = friendlyName + "<br /><small><font style='color:palette(window-text)'>" + messageString + "</font></small>";
    }

    painter->translate( labelRect.topLeft() );
    labelRect.translate( - labelRect.topLeft() );

    textDocument_->setHtml( text );

    QFontMetrics fm( painter->font() );
    const int labelWidth = fm.width( text );

    // Paint directly when it fits in the available space...
    if( ( labelWidth + labelRect.height() ) <= labelRect.width() )
    {
      // The + rect.height() is to have a nicer effect when resizing the window :)
      textDocument_->drawContents( painter );
    }
    // ...otherwise (when the text goes over the available space) fade it away
    else
    {
      // Create a transparent image as big as the text label
      QPixmap labelPixmap( labelRect.width(), labelRect.height() );
      labelPixmap.fill( Qt::transparent );

      // create a linear gradient, white to transparent
      QLinearGradient gradient( QPoint( 0, 0 ), labelRect.bottomRight() );
      if( QApplication::isLeftToRight() )
      {
        gradient.setColorAt( 0.90, Qt::white       );
        gradient.setColorAt( 1.00, Qt::transparent );
      }
      else
      {
        gradient.setColorAt( 0.10, Qt::white       );
        gradient.setColorAt( 0.00, Qt::transparent );
      }

      // The painter will be used to blend the label with the gradient
      QPainter pixmapPainter( &labelPixmap );
      pixmapPainter.setRenderHints( QPainter::Antialiasing | QPainter::SmoothPixmapTransform | QPainter::TextAntialiasing,
                        true );

      // Adjust the label to the contained text
      textDocument_->adjustSize();

      // When using a right-to-left aligned language (like Arabic) the label needs to be
      // moved on the right side of the available space to honor the alignment.
      QPoint offset( 0, 0 );
      if( QApplication::isRightToLeft() )
      {
        offset.rx() = labelRect.width() - textDocument_->idealWidth();
      }

      // Paint the label over the image
      textDocument_->drawContents( &pixmapPainter );

      // Blend together the image and the gradient
      pixmapPainter.setCompositionMode( QPainter::CompositionMode_DestinationIn );
      pixmapPainter.fillRect( labelRect, QBrush( gradient ) );

      // Paint the resulting image over the widget
      painter->drawPixmap( labelRect, labelPixmap );
    }
  }

  // Restore the painter to the previous state
  painter->restore();
}



/**
 * Return an hint about the size of an item
 *
 * @param option  Contains some details about the item we have to determine size of
 * @param index   Points to the actual list item
 * @return The ize for the given index
 */
QSize ContactListDelegate::sizeHint( const QStyleOptionViewItem &option, const QModelIndex &index ) const
{
  Q_UNUSED( option );

  // Read the actual data off the model index
  const KMess::ModelDataList& itemData( index.data().toMap() );

  QFont generalFont = KGlobalSettings::generalFont();
  QFontMetricsF metrics( generalFont );
  
  QSize size( KIconLoader::SizeSmall + 2, KIconLoader::SizeSmall + 4 );

  // at least as large as necessary to display all of the text
  if ( size.height() < metrics.height() )
  {
    size.setHeight( metrics.height() );
  }
  
  if( itemData.isEmpty() )
  {
    return size;
  }

  int picturesDimension = Account::connectedAccount->getSettingInt( "ContactListPictureSize" );
  switch( itemData[ "type" ].toInt() )
  {
    case KMess::ContactListModelItem::ItemContact:
      if( picturesDimension > 0 )
      {
        QSize pictureSize( size );
        if( picturesDimension <= 32 )
        {
          // Fix the margin
          pictureSize.rheight() = 36;
        }
        else
        {
          pictureSize.rheight() = picturesDimension + 2;
        }

        qreal fontHeight = metrics.height() * 2; // x2 to account for friendly name and psm.
        if ( fontHeight > pictureSize.height() )
        {
          pictureSize.setHeight( fontHeight );
        }
        
        return pictureSize;
      }
      else
      {
        return size;
      }

    case KMess::ContactListModelItem::ItemGroup:
    default:
      return size;
  }
}

