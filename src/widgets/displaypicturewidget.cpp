/***************************************************************************
                          displaypicturewidget.h  -  display picture widget
                             -------------------
    begin                : Tue Jul 28 2009
    copyright            : (C) 2009 by Valerio Pilo <valerio@kmess.org>
                           (C) 2009 by Ruben Vandamme <ruben@kmess.org>
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "displaypicturewidget.h"

#include "../kmessdebug.h"

#include <QMouseEvent>

#include <KStandardDirs>


/**
 * Minimum and maximum frame size
 *
 * The frame size must be within these two values.
 */
#define FRAME_SIZE_MINIMUM        32
#define FRAME_SIZE_MAXIMUM        96



// The constructor
DisplayPictureWidget::DisplayPictureWidget( QWidget *parent )
: QFrame( parent )
, Ui::DisplayPictureWidget()
, highlighted_( false )
{
  setupUi( this );

  installEventFilter( this );

  // Create an overlay to highlight the picture
  overlay_ = new QWidget( label_ );
  QVBoxLayout *layout = new QVBoxLayout( label_ );
  layout->addWidget( overlay_ );
  layout->setMargin( 0 );
  overlay_->hide();
}



// The destructor
DisplayPictureWidget::~DisplayPictureWidget()
{
  // overlay_ is deleted by label_ because it's its child

  // Clean up the movie, or it could be deleted while it is still painting
  movie_.stop();
  label_->setMovie( 0 );

#ifdef KMESSDEBUG_DISPLAYPICTUREWIDGET
  kmDebug() << "DESTROYED.";
#endif
}



// Get the current picture's path
const QString &DisplayPictureWidget::getPicture() const
{
  return picturePath_;
}



// Show a picture into the frame
bool DisplayPictureWidget::eventFilter( QObject *obj, QEvent *event )
{
  Q_UNUSED( obj );

  QEvent::Type eventType = event->type();

  // Don't highlight the picture if it already is
  if( eventType == QEvent::Enter && ! highlighted_ )
  {
    overlay_->show();
  }
  else if( eventType == QEvent::Leave && ! highlighted_ )
  {
    overlay_->hide();
  }
  // Handle click events
  else if( eventType == QEvent::MouseButtonPress )
  {
    overlay_->setStyleSheet( clickOverlayStyle_ );
  }
  else if( eventType == QEvent::MouseButtonRelease )
  {
    overlay_->setStyleSheet( normalOverlayStyle_ );

    QMouseEvent *mouseEvent = static_cast<QMouseEvent*>( event );

    // If the mouse release was done outside the widget, ignore the action
    if( ! geometry().contains( mouseEvent->pos() ) )
    {
      return false;
    }

    // Signal that we've been clicked
    if( mouseEvent->button() == Qt::LeftButton )
    {
      emit leftClicked();
    }
    else if( mouseEvent->button() == Qt::RightButton )
    {
      emit rightClicked();
    }
  }

  return false;  // don't stop processing.
}



// Force the picture to be highlighted
void DisplayPictureWidget::setHighlighted( bool highlight )
{
  overlay_->setVisible( highlight );

  highlighted_ = highlight;
}



// Set the picture to show when the given picture isn't valid
void DisplayPictureWidget::setDefaultPicture( const QString &defaultPicturePath )
{
#ifdef KMESSDEBUG_DISPLAYPICTUREWIDGET
    kmDebug() << "Changing default picture:" << defaultPicturePath;
#endif

  defaultPicturePath_ = defaultPicturePath;
}



// Show a picture into the frame
void DisplayPictureWidget::setPicture( const QString &picturePath )
{
#ifdef KMESSDEBUG_DISPLAYPICTUREWIDGET
    kmDebug() << "Changing picture:" << picturePath;
#endif

  movie_.setFileName( picturePath );

  if( ! movie_.isValid() )
  {
#ifdef KMESSDEBUG_DISPLAYPICTUREWIDGET
    kmDebug() << "File is not valid! Setting default picture";
#endif

    // If there is no default picture set, abort
    if( defaultPicturePath_.isEmpty() )
    {
      return;
    }

    // If we're already loading the default picture, and it doesn't exist either, give up loading
    if( picturePath == defaultPicturePath_ )
    {
      return;
    }

    // Fall back to the default picture
    setPicture( defaultPicturePath_ );
    return;
  }

  picturePath_ = picturePath;

  // Reset the movie so it shows a frame (initially it doesn't show anything)
  movie_.jumpToFrame( 0 );

  // Save the still version of the movie
  picture_ = movie_.currentPixmap();

  if( QMovie::supportedFormats().contains( movie_.format() ) )
  {
#ifdef KMESSDEBUG_DISPLAYPICTUREWIDGET
    kmDebug() << "Image is animated, setting up movie";
#endif
    label_->setMovie( &movie_ );
    movie_.start();
  }
  else
  {
#ifdef KMESSDEBUG_DISPLAYPICTUREWIDGET
    kmDebug() << "Image is still, setting up picture";
#endif
    label_->setPixmap( picture_ );
    movie_.stop();
  }
}



// Change the picture size
void DisplayPictureWidget::setSize( const int size )
{
  int newSize;

  // Avoid excessively big or small pictures from breaking the layout
  if( size < FRAME_SIZE_MINIMUM )
  {
    newSize = FRAME_SIZE_MINIMUM;
  }
  else if( size > FRAME_SIZE_MAXIMUM )
  {
    newSize = FRAME_SIZE_MAXIMUM;
  }
  else
  {
    newSize = size;
  }

#ifdef KMESSDEBUG_DISPLAYPICTUREWIDGET
  kmDebug() << "Changing size to" << newSize << "(requested:" << size << ")";
#endif

  label_->setFixedSize( newSize, newSize );
}



// Get the click overlay
const QString &DisplayPictureWidget::clickOverlay()
{
  return clickOverlayStyle_;
}



// Get the default overlay
const QString &DisplayPictureWidget::normalOverlay()
{
  return normalOverlayStyle_;
}



// Set the click overlay
void DisplayPictureWidget::setClickOverlay( const QString &style )
{
  clickOverlayStyle_= style;
}



// Set the default overlay
void DisplayPictureWidget::setNormalOverlay( const QString &style )
{
  overlay_->setStyleSheet( style );
  normalOverlayStyle_= style;
}



#include "displaypicturewidget.moc"
