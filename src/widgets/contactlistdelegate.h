/***************************************************************************
                          contactlistdelegate.h -  description
                             -------------------
    begin                : Sat Feb 16 2008
    copyright            : (C) 2008 by Valerio Pilo
    email                : valerio@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CONTACTLISTDELEGATE_H
#define CONTACTLISTDELEGATE_H

#include <QStyledItemDelegate>


// Forward declarations
class KIconLoader;
class QFont;
class QLabel;
class QTextDocument;



/**
 * Displays a contact list item using its model
 *
 * This class is used by a Contact List view to display a single contact
 * or group. It receives the data to display from the Contact List model and paints
 * it directly.
 *
 * @author Valerio Pilo <valerio@kmess.org>
 * @ingroup Root
 */
class ContactListDelegate : public QStyledItemDelegate
{
  public:

    // Constructor
                    ContactListDelegate( QWidget *parent = 0 );
    // Destructor
                   ~ContactListDelegate();


  public: // Public methods

    // Paint an item of the contact list
    void            paint( QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index ) const;
    // Return an hint about the size of an item
    QSize           sizeHint( const QStyleOptionViewItem &option, const QModelIndex &index ) const;


  private: // Private properties

    // Bold font
    QFont           fontBold_;
    // Icon loader to make painting a bit faster
    KIconLoader    *iconLoader_;
    // Cached emoticon replacement for the now listening icon
    QString         mediaEmoticonMusic_;
    // Cached emoticon replacement for the now playing icon
    QString         mediaEmoticonGaming_;
    // QTextDocument for rich text painting
    QTextDocument   *textDocument_;

};



#endif
