/***************************************************************************
                          emoticonmanager.h - handles emoticon themes for current account
                             -------------------
    begin                : Tue April 10 2007
    copyright            : (C) 2007 by Valerio Pilo
    email                : valerio@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef EMOTICONMANAGER_H
#define EMOTICONMANAGER_H

#include "emoticon.h"
#include "emoticontheme.h"

#include <QRegExp>



/**
 * @brief Data class for the list of emoticon definitions and replacements.
 *
 * This class maintains the list of the global/standard emoticons.
 * MSN Messenger uses a fixed set of emoticon/smiley codes and replacements.
 * The rich text parser can be used to replace all emoticons in a message.
 * To implement a custom search-replace algorithm, use the getPattern() / getReplacements() methods
 * to retrieve all needed data.
 *
 * @author Michael Curtis, Diederik van der Boor, Valerio Pilo
 * @ingroup Root
 */
class EmoticonManager : public QObject
{
  Q_OBJECT

  public:  // Public methods
    // Return the picture file names of all emoticons
    const QHash<QString,QString> &getFileNames( bool getCustomTheme = false );
    // Return a QHash to map shortcut to data hash
    const QHash<QString,QString> &getHashes();
    // Return the search pattern to find emoticons in an HTML document
    const QRegExp                &getHtmlPattern( bool getCustomTheme = false );
    // Return the HTML replacement codes for all emoticons in a theme
    const QHash<QString,QString> &getHtmlReplacements( bool isSmall = false, bool getCustomTheme = false );
    // Return a QStringList of emoticons
    const QStringList            &getList( bool getCustomTheme );
    // Return the search pattern to find emoticons in a text
    const QRegExp                &getPattern( bool getCustomTheme = false );
    // Return one replacement code for the given emoticon
    QString                      getReplacement( const QString &code, bool isSmall = false, bool getCustomTheme = false );
    // Return the replacement codes for all emoticons in a theme
    const QHash<QString,QString> &getReplacements( bool isSmall = false, bool getCustomTheme = false );
    // Return a pointer to the emoticons theme
    EmoticonTheme                *getTheme( bool getCustomTheme = false );
    // Return the path where the theme's emoticons are located
    const QString                &getThemePath( bool getCustomTheme = false );
    // Returns true if a custom emoticon has already been added
    bool                         emoticonIsAdded( QString dataHash );
     // Load an account's emoticon themes
    void                        loadThemes( const QString &handle );
    // Replace the custom emoticon theme with a new one
    void                         replaceCustomTheme( EmoticonTheme *newTheme );

  public:  // Public static methods
    // Delete the instance of the emoticon manager
    static void                  destroy();
    // Return a singleton instance of the emoticon manager
    static EmoticonManager      *instance();

  private:  // Private methods
    // Constructor
                                 EmoticonManager();
    // Destructor
    virtual                     ~EmoticonManager();

  private slots:
    // The emoticon theme has changed, update our settings
    void                         slotChangedEmoticonSettings();

  private:  // Private members
    // The user's custom emoticons theme
    EmoticonTheme               *customTheme_;
    // The standard emoticons theme
    EmoticonTheme               *standardTheme_;

  private:  // Private static members
    // The instance of the emoticon manager singleton
    static EmoticonManager      *instance_;

  signals:
    // Signal that we've been changed and the UI needs to be refreshed
    void                          updated();
};



#endif
