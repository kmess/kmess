/***************************************************************************
                          accountsmanager.h -  description
                             -------------------
    begin                : Sat May 3 2008
    copyright            : (C) 2008 by Valerio Pilo
    email                : valerio@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef ACCOUNTSMANAGER_H
#define ACCOUNTSMANAGER_H

#include <QList>
#include <QHash>

#include <KWallet/Wallet>

// Forward declarations
class Account;



/**
 * @brief The class responsible for keeping a list of saved user accounts
 *
 * @author Valerio Pilo <valerio@kmess.org>
 * @ingroup Root
 */
class AccountsManager : public QObject
{
  Q_OBJECT

  public: // Public methods

    // Verify is there is a saved account with the specified handle
    bool                    contains( Account *account ) const;
    // Return the account for a given handle
    Account *               getAccount( const QString &handle );
    // Get the list of all saved accounts
    const QList<Account *>  &getAccounts() const;
    // Return the account selected for automatic login, if any
    Account                 *getAutoLoginAccount();
    // Read passwords for all accounts
    void                    readPasswords( bool block );
    // Save passwords for all accounts (only actually saves when there are accounts to be updated!)
    void                    savePasswords( bool block );
    // Change the account selected for automatic login
    void                    setAutoLoginAccount( const Account *account );

  private: // Private methods

    // Load the properties from disk
    void                    readProperties();
    // Save the properties to disk
    void                    saveProperties();


  public slots:

    // Add a new account to the list
    void                    addAccount( Account *account );
    // An account's settings have been changed
    void                    changeAccount( Account *account, QString oldHandle, QString oldFriendlyName );
    // Delete the given account
    void                    deleteAccount( Account *account );


  public:  // Public static methods

    // Delete the instance of the accounts manager
    static void             destroy();
    // Return a singleton instance of the accounts manager
    static AccountsManager *instance();


  private:  // Private methods

    // Constructor
                            AccountsManager();
    // Destructor
                           ~AccountsManager();
    // Initialize the KWallet password manager
    void                    initializePasswordManager( bool block = false );
    // Set up the passwordManager_ after initialization
    void                    setupPasswordManager();


  private slots:

    // Read the passwords from the just opened wallet
    void                    slotWalletOpen( bool success );

  private: // Private properties

    // A list of user accounts
    QList<Account*>         accounts_;
    // Account used for automatic login
    QString                 autoLoginAccount_;
    // Whether a passwords read is scheduled or not
    bool                    doPasswordRead_;
    // Whether a passwords write is scheduled or not
    bool                    doPasswordWrite_;

  private:  // Private static members

    // The instance of the accounts manager singleton
    static AccountsManager *instance_;
    // The instance of the KWallet password manager
    KWallet::Wallet        *passwordManager_;
    // The list of passwords read from the wallet. Used to check if the passwords have changed
    QHash<const Account*,QString> originalPasswords_;

  signals:
    // A new account has been created
    void                    accountAdded( Account *account );
    // An account's properties have changed
    void                    accountChanged( Account *account, QString oldHandle, QString oldFriendlyName );
    // An account has been deleted
    void                    accountDeleted( Account *account );
    // The password manager has read the passwords
    void                    passwordsReady();
};



#endif
