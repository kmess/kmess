/***************************************************************************
                          emoticon.h  -  description
                             -------------------
    begin                : Mon Apr 15 2002
    copyright            : (C) 2002 by Mike K. Bennett
    email                : mkb137b@hotmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef EMOTICON_H
#define EMOTICON_H

#include <QStringList>


/**
 * Emoticons default size
 *
 * Original MSN emoticons have a size of 19x19 pixels, but to embed emoticons within text we'll
 * need to shrink them some more
 */
#ifndef EMOTICONS_DEFAULT_SIZE
  #define EMOTICONS_DEFAULT_SIZE    14
#endif

/**
 * Emoticons maximum size
 *
 * We reduce emoticons which are too big to a reasonable size. Aspect ratio is kept: this value
 * is applied to the largest dimension of the images.
 */
#ifndef EMOTICONS_MAX_SIZE
  #define EMOTICONS_MAX_SIZE        128
#endif



/**
 * @brief Data class for an emoticon definition.
 *
 * This class represents an emoticon picture file along with the text shortcuts that translate into that picture.
 * Normal emoticon pictures are searched for in two folders: first in the one where is located the theme selected
 * by the user; then in the other where there's the default KMess theme. This way, if an emoticon is not present
 * in a theme that the user has chosen, the standard KMess emoticon is kept in its place.
 * A common emoticon which most themes don't have is the 'party' one, which in MSN is a smiley with a party hat
 * on it. You'll often see the KMess default version of this emoticon when you choose another theme.
 *
 * But the emoticons can also be created with a specific path where to look for the image file; a method useful for
 * custom emoticons. They don't have a default icon in the default KMess theme, so no defaults are searched for.
 *
 * For standard emoticons, file paths are updated by the EmoticonTheme class when the theme changes.
 *
 * Multiple emoticon shortcuts can be added to the emoticon with the addShortcut() method.
 * KMess supports for the emoticons all the image file types which are supported by KDE; but only some extensions
 * are looked for. If you want to add another image type for your emoticons, look at the updatePath() method.
 *
 * @author Mike K. Bennett
 * @ingroup Root
 */
class Emoticon
{
  public:
    // Constructor for custom emoticons
                             Emoticon( const QString &pictureName, const QString &tooltip, const QString &pictureDirectory, bool isCustomTheme );
    // Create an exact copy of this emoticon
                             Emoticon( const Emoticon &other );
    // Destructor
    virtual                 ~Emoticon();

    // Add a text shortcut which will translate to this emoticon
    void                     addShortcut( const QString &shortcut );
    // Get the picture file name
    const QString            &getPictureName() const;
    // Get the full path of the picture file
    const QString            &getPicturePath() const;
    // Get the data hash for the picture file contents (may be empty)
    const QString            &getDataHash() const;
    // Get a suitable bit of HTML which represents the emoticon
    const QString            getHtml( bool isSmall ) const;
    // Get a regular expression pattern which will match any of the text shortcuts of the emoticon
    const QString            getPattern() const;
    // Get the first shortcut associated to this emoticon
    const QString            &getShortcut() const;
    // Get all the shortcuts associated to this emoticon
    const QStringList        &getShortcuts() const;
    // Get the associated tooltip
    const QString            &getTooltip() const;
    // Find if this emoticon really contains a valid picture
    bool                     isValid() const;
    // Change the name of the emoticon picture
    void                     setPictureName( const QString &pictureName, const QString &pictureDirectory );
    // Delete all the shortcuts and replace them with a single one
    void                     setShortcut( const QString &shortcut );
    // Delete all the shortcuts and replace them with another list
    void                     setShortcuts( const QStringList &shortcuts );
    // Change the associated tooltip
    void                     setTooltip( const QString &pictureTooltip );
    // Update the internal emoticon data
    void                     update();

  public: // Public static methods
    // Return whether a string is suitable for use as an emoticon shortcut
    static bool              isValidShortcut( const QString &shortcut );

  private: // Private attributes
    // The directory where the custom emoticon image is located. Unused for standard emoticons
    QString                  pictureDirectory_;
    // The file name of the emoticon picture
    QString                  pictureName_;
    // Contains the full path for the current emoticon picture, present for convenience
    QString                  picturePath_;
    // The hash for this emoticon (sha1d) - used to compare with other emoticons
    QString                  dataHash_;
    // The height of the emoticon
    int                      height_;
    // Whether this Emoticon represents a standard KMess emoticon or a custom (personal) one
    bool                     isCustomEmoticon_;
    // The file name of the initial emoticon image
    QString                  originalPictureName_;
    // The list of shortcuts that represent the emoticon, eg ":-)" and ":)"
    QStringList              shortcuts_;
    // A name for the emoticon, eg "Happy"
    QString                  tooltip_;
    // Does the image exist and can it be used?
    bool                     valid_;
    // The width of the emoticon
    int                      width_;
};

#endif
