/***************************************************************************
                          mainwindow.h  -  description
                             -------------------
    begin                : Sun Jan  5 15:18:36 CST 2003
    copyright            : (C) 2003 by Mike K. Bennett
    email                : mkb137b@hotmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "contact/contact.h"
#include "contact/group.h"
#include "contact/status.h"
#include "kmessdebug.h"

#include <KMess/NetworkGlobals>

#include <QHash>
#include <QPointer>
#include <QShowEvent>

#include <KXmlGuiWindow>
#include <Solid/Networking>


// Forward declarations
class QAuthenticator;
class QLabel;
class QItemSelection;
class QNetworkProxy;

class KAction;
class KActionMenu;
class KSelectAction;
class KSqueezedTextLabel;
class KToggleAction;

class Account;
class AccountAction;
class Contact;
class ContactList;
class KMessTest;
class KMessView;
class InitialView;
class LoginView;
class SystemTrayWidget;
class ContactAddedUserDialog;



/**
 * @brief The main window of the application.
 *
 * This class represents the main application window.
 *
 * @author Mike K. Bennett
 * @ingroup Root
 */
class MainWindow : public KXmlGuiWindow
{
  Q_OBJECT

  friend class KMessDBus;
  friend class KMessTest;


  public: // Public methods

    // Request a status change
    void                           changeStatus( KMess::MsnStatus newStatus, QString autoReplyMessage = QString() );
    // Autologin with the first account that has autologin enabled
    void                           checkAutologin( const QString &handle = QString() );
    // Save a contact
    void                           saveContact( Contact &contact );
    // Save a group
    void                           saveGroup( Group *group );
    // Get application style.
    QString                        applicationStyle();
    // Set application style.
    void                           setApplicationStyle ( const QString &styleName );
    // Switch the view to the initial login screen
    void                           switchViewToInitialScreen();

  public: // Public static methods

    // Return a singleton instance of the KMess class
    static MainWindow             *instance();

  private: // Private methods
    // The construtor
                                   MainWindow( QWidget* parent = 0 );
    // The destructor
                                  ~MainWindow();
    // Create the menus
    void                           createMenus();
    // Enable/disable menus based on whether or not the application is connected to the server.
    void                           enableMenus( bool connected );
    // Initialize the system tray widget
    bool                           initSystemTrayWidget();
    // Load contact list data.
    void                           loadContactList();
    // Reject quitting unless the quit menu was pressed
    bool                           queryExit();
    // Tell the user that KMess hides in the systray
    bool                           queryClose();
    // Load the application's global state
    void                           readGlobalProperties( KConfig *sessionConfig );
    // Read in account and other properties
    void                           readProperties( const KConfigGroup &config = *((const KConfigGroup *)0) );
    // Save the application's global state
    void                           saveGlobalProperties( KConfig *sessionConfig );
    // Save account and other properties
    void                           saveProperties( KConfigGroup &config = *((KConfigGroup *)0) );
    // Initiate a connection to the server. Use connectWithAccount() to connect, not this method.
    void                           startConnection( Account *account, const QString &password = QString() );
    // Switch the view to the contact list
    void                           switchViewToContactList();


  public slots:

    // Add the account and create the GUI elements
    void                           slotAccountAdded( Account *account );
    // An account's settings have been changed
    void                           slotAccountChanged( Account *account, QString oldHandle, QString oldFriendlyName );
    // Delete the given account from the UI
    void                           slotAccountDeleted( Account *account );
    // Rebuild the disconect menu; an endpoint changed.
    void                           rebuildDisconnectMenu();

  protected:
    // Make sure that the window is visible on Mac
    void                           showEvent( QShowEvent *e );

  private slots: // Private slots

    // "Add a new contact" was selected from the menu.
    void                           addNewContact();
    // "Add a new group" was selected from the menu.
    void                           addNewGroup();
    // The current display picture settings have changed.
    void                           changedDisplayPicture();
    // The current display settings have changed
    void                           changedDisplaySettings();
    // A view pictures mode has been selected from the menu.
    void                           changedListPictureSize( int mode );
    // The logging settings were changed.
    void                           changedLoggingSettings();
    // The current now listening settings have changed.
    void                           changedNowListeningSettings();
    // The status was changed
    void                           changedStatus( Account *account = 0 );
    // The currently playing song was changed.
    void                           changedSong( const QString &artist, const QString &album, const QString &track, bool playing );
    // A status was selected from the menu.
    void                           changeStatus( QAction *action );
    // A view mode has been selected from the menu.
    void                           changeViewMode( int mode );
    // "Add new account" has been selected from the menu.
    void                           createNewAccount();
    // Connect to the server with the given account
    void                           connectWithAccount( Account *account );
    // Connect to the server with the given account, possibly temporary or new.
    void                           connectWithAccount( QString handle, QString password, bool rememberAccount, bool rememberPassword, bool autologin, KMess::MsnStatus initialStatus );
    // Disconnect was selected from the menu.
    void                           disconnectClicked( QAction *action );
    // The program is no longer connected to the notification server
    void                           disconnected( KMess::DisconnectReason reason );
    // A connection has been made with the notification server.
    void                           loggedIn();
    // Close has been selected from the menu.
    void                           menuClose();
    // Quit was selected from the menu.
    void                           menuQuit();
    // Ask the user to authenticate on a proxy
    void                           proxyAuthenticate( const QNetworkProxy &proxy, QAuthenticator *authenticator );
    // The user wants to reconnect
    void                           reconnect();
    // Set the caption
    void                           setCaptionToUser();
    // Show the chat history dialog and, if requested, that of a specific contact
    void                           showChatHistory( const QString &handle = QString() );
    // Show a "Contact added you" dialog
    void                           showContactAddedUserDialog( KMess::MsnContact *contact );
    // Show the context-sensitive menu item
    void                           showContextMenu();
    // The "Show Global Settings" option was clicked.
    void                           showGlobalSettings();
    // Show "export contact list dialog"
    void                           showListExportDialog();
    // "Show menu bar" was toggled.
    void                           showMenuBar();
    // Open the network window
    void                           showNetworkWindow();
    // Show the settings dialog for a given account
    void                           showSettingsForAccount( Account *account );
    // Show the settings dialog for the current account.
    void                           showSettingsForCurrentAccount();
    // "Show status bar" was toggled.
    void                           showStatusBar();
    // Open the transfer manager
    void                           showTransferWindow();
    // Show the user's MSN profile.
    void                           showUserProfile();
    // The connection attempt failed.
    void                           slotConnectionFailed( KMess::StatusMessage reason );
    // Detect changes in the status of the internet connection
    void                           slotConnectionStatusChanged( Solid::Networking::Status newStatus );
    // The user was presented the "contact added user" dialog and has made a choice
    void                           slotContactAddedUserChoice( const KMess::MsnContact* contact, const GroupsList groups, const int code );
    // Record the network library states
    void                           slotStatusEvent( KMess::StatusMessageType type, KMess::StatusMessage message, QVariant data );
    // Change the status bar message.
    void                           statusMessage( QString message, bool isError );
    // The "show allowed contacts" menu item has been toggled.
    void                           toggleShowAllowed( bool show );
    // The "show empty groups" menu item has been toggled.
    void                           toggleShowEmpty( bool show );
    // The "show history box" menu item has been toggled.
    void                           toggleShowHistoryBox( bool show );
    // The "show offline contacts" menu item has been toggled.
    void                           toggleShowOffline( bool show );
    // The "Show removed contacts" menu item has been toggled.
    void                           toggleShowRemoved( bool show );
    // The "show search in contact list" menu item has been toggled.
    void                           toggleShowSearchFrame( bool show );
    // The "sort alphabetical" item has been toggled.
    void                           toggleSortAlphabetical( bool alpha );
    // Show the context-sensitive menu item
    void                           updateContextMenu( const QItemSelection &selection );
    // Increment and update the online timer.
    void                           updateOnlineTimer();
    // The user has gone idle
    void                           userIsIdle();
    // The user is no longer idle
    void                           userIsNotIdle();


  private: // Private attributes

    // The current application style
    QString                        applicationStyle_;
    // The collection of actions, allowing their customization
    KActionCollection             *actionCollection_;
    // The menu items of the connect menu
    QHash<Account*,AccountAction*> connectMenuItems_;
    // Active instance of the contact added user dialog
    QPointer<ContactAddedUserDialog> contactAddedUserDialog_;
    // The initial login view widget
    InitialView                   *initialView_;

    // Whether the current status message is a warning or not
    bool                           isErrorStatus_;
    // Whether the user has forced the Idle status or if it's been changed automatically
    bool                           isIdleForced_;
    // The last account which was connected
    QString                        lastConnectedAccount_;
    // Last status event from the library
    KMess::StatusMessage           lastStatusEvent_;
    // Current message
    QString                        message_;
    // Minutes online
    unsigned int                   onlineTime_;
    // Online interval timer
    QTimer                        *onlineTimer_;
    // Status messages label
    KSqueezedTextLabel            *statusLabel_;
    // Online timer
    QLabel                        *statusTimer_;
    // The system tray widget
    SystemTrayWidget              *systemTrayWidget_;
    // The main view widget
    KMessView                     *view_;


  private: // Private static members

    // Instance for this class
    static MainWindow            *instance_;


  private: // Private attributes, GUI actions

    // The menu for "connect..." with a profile
    KActionMenu                   *actionConnectMenu_;
    // The action to show the context-sensitive menu
    KAction                       *actionContextMenu_;
    // The "disconnect" menu item
    KMenu                         *actionDisconnectMenu_;
    // Export list action
    KAction                       *actionExportList_;
    // View list pictures mode
    KSelectAction                 *actionListPictureSize_;
    // Action to add a contact
    KAction                       *actionNewContact_;
    // Action to create a group
    KAction                       *actionNewGroup_;
    // Action to quit from KMess
    KAction                       *actionQuit_;
    // Action to toggle the allowed contacts group
    KToggleAction                 *actionShowAllowed_;
    // Action to toggle empty groups
    KToggleAction                 *actionShowEmpty_;
    // Action to show the chat history dialog
    KAction                       *actionShowChatHistory_;
    // The menu action for global settings
    KAction                       *actionShowGlobalSettings_;
    // Action to toggle the history box
    KToggleAction                 *actionShowHistoryBox_;
    // Action to toggle the offline contacts group
    KToggleAction                 *actionShowOffline_;
    // Action to show the user profile
    KAction                       *actionShowProfile_;
    // Action to toggle the removed contacts group
    KToggleAction                 *actionShowRemoved_;
    // Action to toggle the contact list search bar
    KToggleAction                 *actionShowSearch_;
    // Action to display the settings for the connected account
    KAction                       *actionShowSettings_;
    // Action to toggle the menu bar
    KToggleAction                 *actionShowMenuBar_;
    // Show the network window
    KAction                       *actionShowNetworkWindow_;
    // Toggle actions for showing and hiding the status bar
    KToggleAction                 *actionShowStatusBar_;
    // Show the transfer manager
    KAction                       *actionShowTransferWindow_;
    // The selection menu for the user's status
    KMenu                         *actionStatus_;
    // Menu for selecting the view mode
    KSelectAction                 *actionViewMode_;
    KActionMenu                   *actionViewModeMenu_;
    KToggleAction                 *actionViewAlphabetical_;

};



#endif
