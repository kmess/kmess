/***************************************************************************
                          kmessglobal.h  -  description
                             -------------------
    begin                : Wed Jul 22 2009
    copyright            : (C) 2009 by Sjors Gielen
    email                : sjors@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef KMESSGLOBAL_H
#define KMESSGLOBAL_H

#include <KMess/MsnSession>
#include <KMess/ContactList>
#include <KMess/Self>

#include "contact/contactsmanager.h"


/**
 * @brief Global pointer for the current KMess::MsnSession object.
 *
 * KMess::MsnSession is *not* a singleton. There can be more instances of one
 * MsnSession in an application. However, since KMess was originally built
 * around having only one user logged in at a time, we will keep this behaviour
 * for now. In vanilla KMess, there will be only one MsnSession at a time,
 * and classes can reach it through this extern variable, which is given a
 * value in kmessglobal.cpp.
 */
extern KMess::MsnSession *globalSession;


/**
 * @brief Global pointer for the current ContactsManager instance.
 *
 * As above, not a singleton. One reference, globally accessible.
 */
extern ContactsManager *contactsManager;

// cheeky helper
#define CHAIN_SIGNAL( source, target, signal ) connect( source, SIGNAL( signal ), target, SIGNAL( signal ) )
#endif
