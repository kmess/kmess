/***************************************************************************
                          accountaction.cpp  -  description
                             -------------------
    begin                : Tue Oct 22 2002
    copyright            : (C) 2002 by Mike K. Bennett
    email                : mkb137b@hotmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "accountaction.h"

#include "account.h"
#include "kmessdebug.h"



// The constructor
AccountAction::AccountAction(Account *account, QWidget *parent )
 : KAction(parent),
   account_(account)
{
  setObjectName( "AccountAction[" + account->getHandle() + ']' );

  // The idea is to have an action that sends the account when its
  //  normal "activated" signal is sent.
  // Then connect this action's regular "activated" signal to the action's
  //  slotActivated slot, which will then emit an "activated(account)"
  //  signal.
  connect( this,     SIGNAL(           activated() ),
           this,     SLOT  (       slotActivated() ) );

  // Initialize the text by calling update.
  updateText();
}



AccountAction::~AccountAction()
{
}



// Emit the group whtn the action is selected
void AccountAction::slotActivated()
{
  emit activated( account_ );
}



// Update the action's text based on the account.
void AccountAction::updateText()
{
  if ( account_ == 0 )
  {
    return;
  }

  setText( account_->getHandle() );
}

#include "accountaction.moc"
