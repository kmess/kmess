/***************************************************************************
                     kmessglobal.cpp  -  Global pointers for KMess
                             -------------------
    begin                : Wed Jul 22 2009
    copyright            : (C) 2009 by Sjors Gielen
    email                : sjors@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

// DO *NOT* include kmessglobal.h here! We can't have conflicting definitions
// of globalSession (and globalAccount when it's moved here).

#include <KMess/MsnSession>

#include "contact/contactsmanager.h"

/**
 * The global pointer actually gets its value here.
 *
 * @see kmessglobal.h
 */
KMess::MsnSession *globalSession;


/**
 * The global pointer to the ContactsManager instance that belongs to the 
 * above globalSession.
 */
ContactsManager *contactsManager;
