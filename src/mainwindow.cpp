/***************************************************************************
                          mainwindow.cpp  -  description
                             -------------------
    begin                : Sun Jan  5 15:18:36 CST 2003
    copyright            : (C) 2003 by Mike K. Bennett
    email                : mkb137b@hotmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "mainwindow.h"

#include "../contrib/likeback-frontend/likeback.h"
#include "chat/chat.h"
#include "contact/contact.h"
#include "contact/group.h"
#include "contact/status.h"
#include "dialogs/addcontactdialog.h"
#include "dialogs/awaymessagedialog.h"
#include "dialogs/chathistorydialog.h"
#include "dialogs/contactaddeduserdialog.h"
#include "dialogs/contactaddeduserwidget.h"
#include "dialogs/listexportdialog.h"
#include "dialogs/transferwindow.h"
#include "dialogs/welcomedialog.h"
#include "notification/notificationmanager.h"
#include "settings/accountsettingsdialog.h"
#include "settings/globalsettingsdialog.h"
#include "utils/idletimer.h"
#include "utils/kmessconfig.h"
#include "utils/kmessshared.h"
#include "utils/nowlisteningclient.h"
#include "utils/richtextparser.h"
#include "account.h"
#include "accountsmanager.h"
#include "accountaction.h"
#include "emoticon.h"        // workaround for mingw-g++ compiler bug "extraneous `char' ignored"
#include "emoticontheme.h"   // idem
#include "emoticonmanager.h"
#include "initialview.h"
#include "kmessglobal.h"
#include "kmessapplication.h"
#include "kmessdebug.h"
#include "kmessview.h"
#include "config-kmess.h"

#include <KMess/MsnSession>
#include <KMess/MsnGroup>
#include <KMess/MPOPEndpoint>

#include <QAuthenticator>
#include <QNetworkProxy>
#include <QResource>

#include <KActionCollection>
#include <KActionMenu>
#include <KConfig>
#include <KGlobal>
#include <KGlobalSettings>
#include <KInputDialog>
#include <KLocale>
#include <KMessageBox>
#include <KMenu>
#include <KMenuBar>
#include <KPasswordDialog>
#include <KSelectAction>
#include <KStandardDirs>
#include <KStatusBar>
#include <KToggleAction>

#include <KMess/Utils>

#ifdef KMESS_ENABLE_KDE4_TRAY
  #include "notification/newsystemtraywidget.h"
#else
  #include "notification/systemtraywidget.h"
#endif

#ifdef KMESSTEST
  #include "dialogs/networkwindow.h"
#endif


// Maximum number of pings the KMess library is allowed to miss
// before giving up and disconnecting
#define MAX_PINGS_LOST 2


// Initialize the singleton instance to zero
MainWindow* MainWindow::instance_(0);



// The constructor
MainWindow::MainWindow( QWidget *parent )
: KXmlGuiWindow( parent )
, applicationStyle_( "" )
, actionCollection_( actionCollection() )
, contactAddedUserDialog_( 0 )
, initialView_( 0 )
, isErrorStatus_( true )
, lastStatusEvent_( KMess::SessionDisconnected )
, view_( 0 )
{
  // Create the menus
  createMenus();

  // Build the gui from xml (without toolbar support)
  setupGUI( ( Keys | Create | Save ), "kmessinterfaceui.rc" );

  // Autosave all GUI settings
#if KDE_IS_VERSION( 4, 0, 70 )
  setAutoSaveSettings( KMessConfig::instance()->getGlobalConfig( "ContactListWindow" ),
                       true /* save WindowSize */ );
#else
  setAutoSaveSettings( "ContactListWindow", true /* save WindowSize */ );
#endif

  // Initialize the status bar
  statusLabel_ = new KSqueezedTextLabel( this );
  statusBar()->addWidget( statusLabel_, 2 );

  statusTimer_ = new QLabel( this );
  statusTimer_->setText( "00:00" );
  statusBar()->addPermanentWidget( statusTimer_, 0 );

  // Only show the status bar when we're connected
  statusBar()->hide();

  // Create the online timer
  onlineTimer_ = new QTimer( this );
  connect( onlineTimer_, SIGNAL(           timeout() ),
           this,         SLOT  ( updateOnlineTimer() ) );

  if ( ! initSystemTrayWidget() )
  {
    kmDebug() << "Couldn't initialize the system tray widget.";
    return;
  }

#if KMESS_DISABLE_LIKEBACK == 0

  // Enable LikeBack.
  // The bar will only be enabled by default in debugging builds.
  bool showLikeBackBar = false;
#if defined( KMESSTEST ) && ! defined( Q_WS_MAC )
  showLikeBackBar = true;
#endif

  LikeBack *likeBack = new LikeBack( LikeBack::AllButtons, showLikeBackBar );
  likeBack->setServer( "www.kmess.org", "/likeback/send.php", 80 );
  likeBack->setWindowNamesListing( LikeBack::AllWindows );

  QStringList acceptedLocales;
  acceptedLocales << "en" << "nl" << "it" << "de";
  likeBack->setAcceptedLanguages( acceptedLocales );
  likeBack->createActions( actionCollection() );

#endif

  // Use Solid to find out whether we're connected or not, and disable the Connect menu if we are not
  connect( Solid::Networking::notifier(), SIGNAL(               statusChanged(Solid::Networking::Status) ),
           this,                          SLOT  ( slotConnectionStatusChanged(Solid::Networking::Status) ) );
  slotConnectionStatusChanged( Solid::Networking::status() );

  // Connect the changing of status of the user to updating of the settings of now listening support
  connect( globalSession,             SIGNAL(               changedStatus() ),
           this,                      SLOT  ( changedNowListeningSettings() ));

  // Connect current account
  connect( globalSession,   SIGNAL(                    loggedIn()                              ),
           this,            SLOT  (                    loggedIn()                              ) );
  connect( globalSession,   SIGNAL(                   loggedOut(KMess::DisconnectReason)       ),
           this,            SLOT  (                disconnected(KMess::DisconnectReason)       ) );

  // handle this specially - loggedOut() is NOT emitted for failed connection
  // attempts.
  connect( globalSession,   SIGNAL(            connectionFailed(KMess::StatusMessage)          ),
           this,            SLOT  (        slotConnectionFailed(KMess::StatusMessage)          ) );

  connect( globalSession,   SIGNAL(               changedStatus()                              ),
           this,            SLOT  (               changedStatus()                              ) );
  connect( globalSession,   SIGNAL(            contactAddedUser(KMess::MsnContact*)            ),
           this,            SLOT  (  showContactAddedUserDialog(KMess::MsnContact*)      ) );
  connect( globalSession,   SIGNAL( proxyAuthenticationRequired(QNetworkProxy,QAuthenticator*) ),
           this,            SLOT  (           proxyAuthenticate(QNetworkProxy,QAuthenticator*) ) );
  connect( globalSession,   SIGNAL( statusEvent(KMess::StatusMessageType,KMess::StatusMessage,QVariant) ),
           this,            SLOT  ( slotStatusEvent(KMess::StatusMessageType,KMess::StatusMessage,QVariant) ) );

  // Connect the account management signals
  AccountsManager *accountsManager = AccountsManager::instance();
  connect( accountsManager, SIGNAL(       accountAdded(Account*)                 ),
           this,            SLOT  (   slotAccountAdded(Account*)                 ) );
  connect( accountsManager, SIGNAL(     accountChanged(Account*,QString,QString) ),
           this,            SLOT  ( slotAccountChanged(Account*,QString,QString) ) );
  connect( accountsManager, SIGNAL(     accountDeleted(Account*)                 ),
           this,            SLOT  ( slotAccountDeleted(Account*)                 ) );

  // Connect the timer to signal when the user is away.
  IdleTimer *idleTimer = KMessApplication::instance()->idleTimer();
  connect( idleTimer, SIGNAL(       timeout() ),
           this,      SLOT  (    userIsIdle() ) );
  connect( idleTimer, SIGNAL(      activity() ),
           this,      SLOT  ( userIsNotIdle() ) );

  // Fill up the account lists in the UI
  const QList<Account*> &list( accountsManager->getAccounts() );
  foreach( Account *account, list )
  {
    slotAccountAdded( account );
  }

  // Connect the action to show the connected account settings
  connect( actionShowSettings_, SIGNAL(                     triggered(bool) ),
           this,                SLOT  ( showSettingsForCurrentAccount()     ) );

  // Connect the Now Listening client
  NowListeningClient *client = KMessApplication::instance()->nowListeningClient();
  connect( client, SIGNAL( changedSong(QString,QString,QString,bool) ),
           this,   SLOT  ( changedSong(QString,QString,QString,bool) ) );

  // Read the accounts and properties
  readProperties();

  // Initially disable the menus as we're disconnected
  enableMenus( false );

  // When compiling in debug mode, the Network Window is always initialized on KMess start,
  // to record everything. In release mode, to avoid wasting resources, it will be initialized
  // only on first use.
#if ( KMESS_DEBUG == 1 )
  NetworkWindow::instance();
#endif

  // All done, mark as initialized
#ifdef KMESSDEBUG_MAINWINDOW
  kmDebug() << "Main class is now initialized.";
#endif
}



// The destructor
MainWindow::~MainWindow()
{
#ifdef KMESSDEBUG_MAINWINDOW
  kmDebug() << "shutting down KMess...";
#endif

  /*
   * The destructor is called after closeEvent(),
   * when KMess is quit by the user.
   *
   * If the KDE session ends, the destructor won't be called.
   * only saveProperties() will be called, and the execution ends.
   */

  // Saving properties manually.
  saveProperties();

  // Delete other created objects
  delete statusLabel_;
  delete systemTrayWidget_;

  NetworkWindow::destroy();

#ifdef KMESSDEBUG_MAINWINDOW
  kmDebug() << "DESTROYED.";
#endif
}



// "Add a new contact" was selected from the menu.
void MainWindow::addNewContact()
{
  GroupsList groups;
  QString newHandle;
  KMess::NetworkType network;

  // Launch a modal dialog to get the new contact handle and, optionally, group
  new AddContactDialog( newHandle, groups, network, this );

  // Do nothing if the user has clicked cancel
  if( newHandle.isEmpty() )
  {
    return;
  }
#ifdef KMESSDEBUG_MAINWINDOW
  kmDebug() << "Adding " << newHandle << "(network: " << network << ") to groups: " << groups;
#endif

  // Check if the contact is already in contact list
  KMess::MsnContact *contact = globalSession->contactList()->contact( newHandle );

  // If the contact is already in friendly list, stop the process
  if( contact != 0 && contact->isFriend() )
  {
    QMessageBox::information( this,
                              i18n( "Contact Information" ),
                              i18n( "<html>The contact <b>%1</b> is already in your contact list.</html>",
                                    newHandle ) );
    return;
  }

  // Ask the server to add the contact
  globalSession->contactList()->addContact( newHandle, network, groups );
}



// "Add a new group" was selected from the menu.
void MainWindow::addNewGroup()
{
#ifdef KMESSDEBUG_MAINWINDOW
  kmDebug();
#endif
  QString newGroupName;
  bool    okPressed = false;

  // Set a default group name
  newGroupName = i18n( "New Group" );

  // Launch a dialog to get a new group name
  newGroupName = KInputDialog::getText(i18nc( "Dialog box title", "Add a Group" ),
                                       i18n( "Enter a name for the new group:" ),
                                       newGroupName, &okPressed, this);

  if(okPressed)
  {
    // Check if the group already exists.
    foreach( KMess::MsnGroup *group, globalSession->contactList()->groups() )
    {
      if ( group->name() == newGroupName )
      {
        KMessageBox::information( this, i18n( "The group name already exists" ), i18n( "Group adding" ) );
        return;
      }
    }

    // Ask the server to add the group
    globalSession->contactList()->addGroup( newGroupName );
  }
}



// The current display picture settings have changed.
void MainWindow::changedDisplayPicture()
{
  // Check if the user wants to show the picture
  bool displayPictureEnabled = Account::connectedAccount->getSettingBool( "DisplayPictureEnabled" );
#ifdef KMESSDEBUG_MAINWINDOW
    kmDebug() << "Show the user display picture: " << displayPictureEnabled;
#endif

  if( ! displayPictureEnabled )
  {
    // Remove the current one
    globalSession->self()->setDisplayPicture( QString() );
    return;
  }

  // Update the msnobject to the new
  globalSession->self()->setDisplayPicture( Account::connectedAccount->getDisplayPicture() );
}



// The current display settings have changed
void MainWindow::changedDisplaySettings()
{
  // Set the pictures mode
  const Account *account = Account::connectedAccount;

  int newMode = 0;
  switch( account->getSettingInt( "ContactListPictureSize" ) )
  {
    case 0:
      break;
    case 32:
      newMode = 1;
      break;
    case 40:
      newMode = 2;
      break;
    case 48:
      newMode = 3;
      break;

    default:
      break;
  }

  actionListPictureSize_->setCurrentItem( newMode );
}


// A view pictures size has been selected from the menu.
void MainWindow::changedListPictureSize( int size )
{

  int newSize;
  switch( size )
  {
    case 0:
      newSize = 0;
      break;
    case 1:
      newSize = 32;
      break;
    case 2:
      newSize = 40;
      break;
    case 3:
      newSize = 48;
      break;

    default:
      break;
  }

  Account::connectedAccount->setSetting( "ContactListPictureSize", newSize );
}



// when the logging settings are changed in acct settings, make
// sure we inform the library of a change to our logging mode.
void MainWindow::changedLoggingSettings()
{
  KMess::ChatLoggingMode mode = ( Account::connectedAccount->getSettingBool( "LoggingEnabled" ) ? KMess::LoggingActive : KMess::LoggingInactive );
  KMess::Utils::setClientLogging( mode );
}



// The current now listening settings have changed.
void MainWindow::changedNowListeningSettings()
{
  bool enabled = false;

  if( Account::connectedAccount != 0 && globalSession != 0 )
  {
    KMess::MsnStatus status = globalSession->self()->status();

    // Check if the user is online and also if the user wants to show the listening media
    enabled = ( status != KMess::OfflineStatus && status != KMess::InvisibleStatus );
    enabled = enabled && Account::connectedAccount->getSettingBool( "NowListeningEnabled" );
  }

#ifdef KMESSDEBUG_MAINWINDOW
    kmDebug() << "Now Listening is now" << ( enabled ? "enabled" : "disabled" );
#endif

  NowListeningClient *client = KMessApplication::instance()->nowListeningClient();
  client->setEnabled( enabled );
}



// The currently playing song was changed.
void MainWindow::changedSong( const QString &artist, const QString &album, const QString &track, bool playing )
{
  if( ! globalSession->isLoggedIn() || ! view_ )
  {
    return;
  }

  if( ! playing )
  {
    // Update notification connection and KMessView.
    KMess::MediaData d;
    d.type = KMess::MediaNone;

    globalSession->self()->changeMedia( d );
  }
  else
  {
    // Update notification connection.
    KMess::MediaData d;
    d.type = KMess::MediaMusic;
    d.format = "{0} - {1}";
    d.formatArgs << track << artist << album;

    globalSession->self()->changeMedia( d );
  }

  // Update KMessView
  if ( view_ )
  {
    view_->changedSong( artist, album, track, playing );
  }
}



// The status was changed
void MainWindow::changedStatus( Account *account )
{
  KMess::MsnStatus newStatus;

  if( account != 0 )
  {
    newStatus = (KMess::MsnStatus) account->getSettingInt( "StatusInitial" );
  }
  else if ( globalSession != 0 )
  {
    newStatus = globalSession->self()->status();
  }
  else
  {
    kmWarning() << "Changed status dropdown without an account set!";
    return;
  }

#ifdef KMESSDEBUG_MAINWINDOW
  kmDebug() << "Changed status to" << Status::getCode( newStatus )
           << "Autoreply is" << globalSession->sessionSetting( "AutoReplyEnabled" );
#endif

  // Make sure the drop down list matches the user's status
  QList<KAction*> menuActions( actionStatus_->findChildren<KAction*>() );
  foreach( KAction *action, menuActions )
  {
    if( (KMess::MsnStatus) action->data().toInt() == newStatus )
    {
      action->setEnabled( false );
    }
    else
    {
      action->setEnabled( true );
    }
  }
}



// A status was selected from the menu.
void MainWindow::changeStatus( QAction *action )
{
  QString awayMessage;
  KMess::MsnStatus  newStatus = (KMess::MsnStatus) action->data().toInt();

  // The user has set Away with Auto Reply, let it choose the message
#ifdef __GNUC__
  #warning TODO: Away with auto reply.
#endif
  if( newStatus == KMess::AwayStatus )
  {
    awayMessage = Account::connectedAccount->getSettingString( "AutoReplyMessage" );

    // Create and show the away message dialog
    AwayMessageDialog *awayMessageDialog = new AwayMessageDialog( this );
    bool useAutoreply = awayMessageDialog->useMessage( awayMessage );
    if( ! useAutoreply )
    {
      awayMessage.clear();
    }

    delete awayMessageDialog;
  }

  // The user wants to disconnect
  if( newStatus == KMess::OfflineStatus )
  {
    globalSession->logOut();
    return;
  }

  changeStatus( newStatus, awayMessage );
}



// Request a status change
void MainWindow::changeStatus( KMess::MsnStatus newStatus, QString autoReplyMessage )
{
#ifdef KMESSDEBUG_MAINWINDOW
  kmDebug() << "Selected status:" << Status::getCode( newStatus );
#endif

  // Is the status is set manually, idle is forced.
  isIdleForced_ = true;

  // Set or unset the auto reply message
  if( autoReplyMessage.isEmpty() )
  {
    globalSession->setSessionSetting( "AutoReplyEnabled", false );
  }
  else
  {
    Account::connectedAccount->setSetting( "AutoReplyMessage", autoReplyMessage );
    globalSession->setSessionSetting( "AutoReplyEnabled", true );
  }

  // Change status
  globalSession->self()->changeStatus( newStatus );
}



// A view mode has been selected from the menu.
void MainWindow::changeViewMode( int mode )
{
#ifdef KMESSTEST
  KMESS_ASSERT( ( mode >= 0 ) || ( mode <= 2 ) );
#endif

  Account::connectedAccount->setSetting( "ContactListDisplayMode", (Account::ContactListDisplayMode) mode );
}



// Autologin with the first account that has autologin enabled
// This method is also called when restoring the session
void MainWindow::checkAutologin( const QString &handle )
{
  Account *loginAccount = 0;

#ifdef KMESSDEBUG_MAINWINDOW
  kmDebug() << "Checking auto login with handle:" << handle;
#endif

  // If no handle was given, connect to the selected auto login account (if any)
  if( handle.isEmpty() )
  {
    loginAccount = AccountsManager::instance()->getAutoLoginAccount();
  }
  else
  {
    loginAccount = AccountsManager::instance()->getAccount( handle );
  }

  bool doAutoLogin = ( loginAccount != 0 );

#ifdef KMESSDEBUG_MAINWINDOW
  kmDebug() << "Auto login will be performed?" << doAutoLogin;
  if( doAutoLogin )
  {
    kmDebug() << "Autologin account:" << loginAccount->getHandle();
  }
#endif

  // Wait for the user password if needed
  AccountsManager::instance()->readPasswords( doAutoLogin );

  if( doAutoLogin )
  {
    if( loginAccount->getPassword().isEmpty() )
    {
      KMessageBox::error( this,
                          i18nc( "dialog text",
                                 "<p>Cannot login automatically with account <b>%1</b>:<br/>"
                                 "you must first save the account password!</p>",
                                 loginAccount->getHandle() ),
                          i18nc( "dialog title", "Autologin Failed" ) );
      return;
    }

    // Set the login info in the initial view and connect
    // Using the reconnect() method here instead of startConnecting()
    // so KMess waits until the network is back up.
    // Also connect immediately if possible.
    initialView_->reconnect( loginAccount->getHandle(), true );

    // FIXME for 2.1: when using --autologin, don't display ".. to reconnect" but "to connect".
  }
}



// Connect to the server with the given account
void MainWindow::connectWithAccount(Account *account)
{
  if(KMESS_NULL(account)) return;

#ifdef KMESSDEBUG_MAINWINDOW
  kmDebug() << "Connecting account:" << account->getHandle();
#endif

  startConnection( account );
}



// Connect to the server with the given account, possibly temporary or new.
void MainWindow::connectWithAccount(QString handle, QString password, bool rememberAccount, bool rememberPassword, bool autologin, KMess::MsnStatus initialStatus )
{
#ifdef KMESSDEBUG_MAINWINDOW
  kmDebug() << "Connecting with account:" << handle;
#endif

  // Try to find an existing account
  Account *account = AccountsManager::instance()->getAccount( handle );

  // Not found, it's a new one: initialize it
  if( account == 0 )
  {
    account = new Account();

    //account->setLoginInformation( handle, handle /* don't set the password here */ );
    account->setHandle( handle );
    // TODO FIXME: we can't change the personal message here! Therefore,
    // the GUI will show "Unknown name" while logging in where it would
    // previously display the handle until the final friendly name is known.

    account->setGuestAccount( ! rememberAccount );
    AccountsManager::instance()->addAccount( account );
  }

  // Save the current information into Account class
  account->setSetting( "StatusInitial", initialStatus );
  account->setPassword( rememberPassword ? password : QString() );

  if( rememberAccount && rememberPassword && autologin )
  {
    AccountsManager::instance()->setAutoLoginAccount( account );
  }

  // Connect it
  startConnection( account, password );
}



// Create the menus
void MainWindow::createMenus()
{
  QStringList  states;
  QStringList  viewModes, listPictureSizes;
  KAction     *actionNewAccount, *actionClose, *actionQuit;

  // Create the actions for "Connect" menu
  actionStatus_             = Status::getStatusMenu();
  actionConnectMenu_        = new KActionMenu  ( KIcon("network-connect"),          i18n("&Connect"),         this );
  actionShowProfile_        = new KAction      ( KIcon("preferences-desktop-user"), i18n("Show My &Profile"), this );
  actionClose               = KStandardAction::close( this, SLOT( menuClose() ), actionCollection_ );
  actionQuit                = KStandardAction::quit ( this, SLOT(  menuQuit() ), actionCollection_ );

  // Create the actions for "View" menu
  actionShowAllowed_        = new KToggleAction(                                i18n("Show &Allowed Contacts"),   this );
  actionShowOffline_        = new KToggleAction(                                i18n("Show &Offline Contacts"),   this );
  actionShowRemoved_        = new KToggleAction(                                i18n("Show &Removed Contacts"),   this );
  actionShowEmpty_          = new KToggleAction(                                i18n("Show &Empty Groups"),       this );
  actionShowHistoryBox_     = new KToggleAction( KIcon("chronometer"),          i18n("Show &History Box"),        this );
  actionShowSearch_         = new KToggleAction( KIcon("edit-find-user"),       i18n("&Show Search Bar"),         this );
  actionListPictureSize_    = new KSelectAction( KIcon("view-list-tree"),       i18n("&Display Pictures Size"),   this );
  actionShowTransferWindow_ = new KAction      ( KIcon("document-open-remote"), i18n("Show File &Transfers..."), this );
  actionShowStatusBar_      = KStandardAction::showStatusbar( this, SLOT( showStatusBar() ), this );
  actionShowMenuBar_        = KStandardAction::showMenubar  ( this, SLOT(   showMenuBar() ), this );

  // Create the actions for "Actions" menu
  actionNewContact_         = new KAction( KIcon("list-add-user"),            i18n("New &Contact..."),         this );
  actionNewGroup_           = new KAction( KIcon("user-group-new"),           i18n("New &Group..."),           this );
  actionExportList_         = new KAction( KIcon("document-export"),          i18n("&Export Contact List..."), this );
  actionShowChatHistory_    = new KAction( KIcon("chronometer"),              i18n("Show Chat &History..."),   this );
  actionNewAccount          = new KAction( KIcon("user-identity"),            i18n("New &Account..."),         this );
  actionShowSettings_       = new KAction( KIcon("configure"),                i18n("Configure Account..."),    this );
  actionShowGlobalSettings_ = new KAction( KIcon("kmess"),                    i18n("Configure &KMess..."),     this );
  actionContextMenu_        = new KAction( KIcon("preferences-contact-list"), i18n("Show Selection &Menu"),    this );

  // create the actions for the "Sort Contacts by" menu.
  actionViewMode_           = new KSelectAction( this );
  actionViewModeMenu_       = new KActionMenu( KIcon("view-list-tree")       , i18n("&Sort Contacts by"), this );

  actionDisconnectMenu_     = new KMenu( this );
  actionDisconnectMenu_->setTitle( i18n("&Disconnect") );
  actionDisconnectMenu_->setIcon( KIcon("network-disconnect") );

  // Populate ViewMode select
  viewModes << i18n( "Group"          )  // Account::VIEW_BYGROUP
            << i18n( "Online/Offline" )  // Account::VIEW_BYSTATUS
            << i18n( "Mixed"          ); // Account::VIEW_MIXED

  actionViewMode_->setItems( viewModes );

  // add the actions that KSelectAction generates to the menu we're building.
  foreach( QAction *action, actionViewMode_->actions() )
  {
    actionViewModeMenu_->addAction( action );
  }

  // now create the alphabetical sort action.
  actionViewAlphabetical_ = new KToggleAction( KIcon( "view-sort-ascending" ), i18n( "&Alphabetical" ), this );

  // and add it.
  actionViewModeMenu_->addSeparator();
  actionViewModeMenu_->addAction( actionViewAlphabetical_ );

  // Populate PictureSize select
  listPictureSizes << i18n( "Do Not Display" )
                   << i18n( "Small"          )
                   << i18n( "Medium"         )
                   << i18n( "Large"          );
  actionListPictureSize_->setItems( listPictureSizes );

  // Add shortcut for Search in ContactList
  actionShowSearch_->setShortcut( QKeySequence::fromString( tr( "Ctrl+f" ) ) );

  // Disable the context menu, because there's no selection on start
  actionContextMenu_->setEnabled( false );
  // Disable the 'Show Account Settings' action, there is no current account on start
  actionShowSettings_->setEnabled( false );

  // Connect slots to signals for "Connect" menu
  connect( actionDisconnectMenu_,     SIGNAL(              triggered(QAction*)   ),
           this,                      SLOT  (      disconnectClicked(QAction*) ) );
  connect( actionShowProfile_,        SIGNAL(              triggered(bool)     ),
           this,                      SLOT  (        showUserProfile()         ) );
  connect( actionStatus_,             SIGNAL(              triggered(QAction*) ),
           this,                      SLOT  (           changeStatus(QAction*) ) );

  // Connect slots to signals for "View" menu
  connect( actionViewMode_,           SIGNAL(              triggered(int)      ),
           this,                      SLOT  (         changeViewMode(int)      ) );
  connect( actionListPictureSize_,    SIGNAL(              triggered(int)      ),
           this,                      SLOT  ( changedListPictureSize(int)      ) );
  connect( actionShowAllowed_,        SIGNAL(              triggered(bool)     ),
           this,                      SLOT  (      toggleShowAllowed(bool)     ) );
  connect( actionShowOffline_,        SIGNAL(              triggered(bool)     ),
           this,                      SLOT  (      toggleShowOffline(bool)     ) );
  connect( actionShowRemoved_,        SIGNAL(              triggered(bool)     ),
           this,                      SLOT  (      toggleShowRemoved(bool)     ) );
  connect( actionShowEmpty_,          SIGNAL(              triggered(bool)     ),
           this,                      SLOT  (        toggleShowEmpty(bool)     ) );
  connect( actionShowTransferWindow_, SIGNAL(              triggered(bool)     ),
           this,                      SLOT  (     showTransferWindow()         ) );
  connect( actionViewAlphabetical_,   SIGNAL(              triggered(bool)     ),
           this,                      SLOT  ( toggleSortAlphabetical(bool)    ) );

  // Connect slots to signals for "Actions" menu
  connect( actionNewContact_,         SIGNAL(              triggered(bool)     ),
           this,                      SLOT  (          addNewContact()         ) );
  connect( actionNewGroup_,           SIGNAL(              triggered(bool)     ),
           this,                      SLOT  (            addNewGroup()         ) );
  connect( actionExportList_,         SIGNAL(              triggered(bool)     ),
           this,                      SLOT  (   showListExportDialog()         ) );
  connect( actionShowHistoryBox_,     SIGNAL(              triggered(bool)     ),
           this,                      SLOT  (   toggleShowHistoryBox(bool)     ) );
  connect( actionShowSearch_,         SIGNAL(              triggered(bool)     ),
           this,                      SLOT  (  toggleShowSearchFrame(bool)     ) );
  connect( actionShowChatHistory_,    SIGNAL(              triggered(bool)     ),
           this,                      SLOT  (        showChatHistory()         ) );
  connect( actionNewAccount,          SIGNAL(              triggered(bool)     ),
           this,                      SLOT  (       createNewAccount()         ) );
  connect( actionShowGlobalSettings_, SIGNAL(              triggered(bool)     ),
           this,                      SLOT  (     showGlobalSettings()         ) );
  connect( actionContextMenu_,        SIGNAL(              triggered(bool)     ),
           this,                      SLOT  (        showContextMenu()         ) );


  // Add actions to actionCollection for "Connect" menu
  actionCollection_->addAction( "connect",         actionConnectMenu_          );
  actionCollection_->addAction( "disconnect",      actionDisconnectMenu_->menuAction() );
  actionCollection_->addAction( "status",          actionStatus_->menuAction() );
  actionCollection_->addAction( "showProfile",     actionShowProfile_          );

  // Add actions to actionCollection for "View" menu
  actionCollection_->addAction( "pictureSizes",    actionListPictureSize_      );
  actionCollection_->addAction( "showAllowed",     actionShowAllowed_          );
  actionCollection_->addAction( "showOffline",     actionShowOffline_          );
  actionCollection_->addAction( "showRemoved",     actionShowRemoved_          );
  actionCollection_->addAction( "showEmpty",       actionShowEmpty_            );
  actionCollection_->addAction( "showMenuBar",     actionShowMenuBar_          );
  actionCollection_->addAction( "showStatusBar",   actionShowStatusBar_        );
  actionCollection_->addAction( "showTransfers",   actionShowTransferWindow_   );

  // View Modes menu
  actionCollection_->addAction( "viewModeMenu",    actionViewModeMenu_ );

  /* NOTE Do not add actionShowStatusBar_ to the actionCollection with its default name,
   * "options_show_statusbar", because our own slot will never be called when the
   * menu action gets clicked. Maybe KDE overwrites it with another object?
   */

  // Add actions to actionCollection for "Actions" menu
  actionCollection_->addAction( "newContact",      actionNewContact_           );
  actionCollection_->addAction( "newGroup",        actionNewGroup_             );
  actionCollection_->addAction( "exportList",      actionExportList_           );
  actionCollection_->addAction( "showHistoryBox",  actionShowHistoryBox_       );
  actionCollection_->addAction( "showSearch",      actionShowSearch_           );
  actionCollection_->addAction( "chatHistory",     actionShowChatHistory_      );
  actionCollection_->addAction( "newAccount",      actionNewAccount            );
  actionCollection_->addAction( "settingsAccount", actionShowSettings_         );
  actionCollection_->addAction( "settingsGlobal",  actionShowGlobalSettings_   );
  actionCollection_->addAction( "contextMenu",     actionContextMenu_          );

  // Set up the Network Window action
  actionShowNetworkWindow_ = new KAction( KIcon( "network-workgroup" ), i18n( "Show &Network Window..." ), this );
  connect( actionShowNetworkWindow_, SIGNAL( triggered(bool) ), this, SLOT( showNetworkWindow() ) );
  actionCollection_->addAction( "showNetwork", actionShowNetworkWindow_ );
}



// "Add new account" has been selected from the menu.
void MainWindow::createNewAccount()
{
  // Show the settings dialog
  AccountSettingsDialog *accountSettingsDialog = AccountSettingsDialog::instance();
  accountSettingsDialog->loadSettings( 0 );
  accountSettingsDialog->show();
}



// Disconnect was selected from the menu.
void MainWindow::disconnectClicked( QAction *action )
{
  const QString data( action->data().toString() );

  // log out everywhere
  if( data == "EVERYWHERE" )
  {
    globalSession->logOutEverywhere();
    return;
  }
  // log out locally
  else if( data == globalSession->self()->localEndpoint()->guid() )
  {
    globalSession->logOut();
    return;
  }

  // Else, log out from a specific endpoint
  foreach( KMess::MPOPEndpoint *ep, globalSession->self()->endpoints() )
  {
    if( ep->guid() == data )
    {
      globalSession->logOutFrom( ep );
      return;
    }
  }
  // If that endpoint wasn't found (UI out of sync?), do nothing
}



/**
 * Called if the initial connection fails. If the failure was due
 * to the connection timing out we instruct initialView to attempt a reconnect.
 *
 * If the failure was due to invalid credentials, we have initialView
 * display a message.
 */
void MainWindow::slotConnectionFailed( KMess::StatusMessage reason )
{
  switchViewToInitialScreen();

  if ( reason == KMess::ErrorConnectionTimedOut )
  {
    const KMess::MsnStatus lastStatus = globalSession->self()->status();
    initialView_->setSelectedStatus( lastStatus );
    initialView_->reconnect();
  }
  else if ( reason == KMess::ErrorInvalidUserCredentials )
  {
    initialView_->statusMessage( i18n( "Account handle or password is incorrect" ) );
  }

  // we don't handle any other reasons...
}



// The program is not connected (initially) or no longer connected (after a disconnect) to the notification server.
void MainWindow::disconnected( KMess::DisconnectReason reason )
{
  // save the contact list first.
  contactsManager->saveContactList();

  // Updating the UI when quitting is not needed
  if( KMessApplication::instance()->quitSelected() )
  {
    return;
  }

#ifdef KMESSDEBUG_MAINWINDOW
  kmDebug() << "Updating UI to disconnected status";
#endif

#ifdef KMESSTEST
  KMESS_ASSERT( globalSession             != 0 );
#endif

  // do the switch before resetting connectedAccount.
  // otherwise there's a chance we'll crash when some of the list
  // drawing code tries to access Account::connectedAccount.
  switchViewToInitialScreen();

  Account *account = Account::connectedAccount;
  const KMess::MsnStatus lastStatus = globalSession->self()->status();

  // Record the account handle to be able to reconnect later
  lastConnectedAccount_ = globalSession->self()->handle();

  if( account )
  {
    if( account->isGuestAccount() )
    {
      AccountsManager::instance()->deleteAccount( account );
    }
    else
    {
      account->saveProperties();
    }

    // Disconnect all signals to this class
    account->disconnect( this );

    // and reset connectedAccount
    Account::connectedAccount = 0L;
    account = 0L;
  }

  // Save the emoticon custom theme
  EmoticonTheme *customTheme = EmoticonManager::instance()->getTheme( true );
  customTheme->saveTheme();

  // Update the GUI
  enableMenus( false );

#ifdef KMESSDEBUG_MAINWINDOW
  kmDebug() << "Swapping view to InitialView.";
#endif

  // Clear the window title from account names
  setCaption( QString() );

  // End now listening support
  changedNowListeningSettings();

  // Update the current account
  globalSession->setSessionSetting( "AutoReplyEnabled", false );

  // Finally, decide if it's the case to reconnect to the server
  if( reason == KMess::UnexpectedDisconnect )
  {
    initialView_->setSelectedStatus( lastStatus );
    initialView_->reconnect( lastConnectedAccount_ );
  }
  else if ( reason == KMess::OtherLocationDisconnect )
  {
    initialView_->statusMessage( i18n( "You were signed on from another location." ) );
  }
}



// Enable/disable menus based on whether or not the application is connected to the server.
void MainWindow::enableMenus( bool connected )
{
  if( connected )
  {
    //actionDisconnectMenu_->setIcon( KIcon("network-connect") );
    stateChanged( "connected" );

    statusBar()->setVisible( actionShowStatusBar_->isChecked() );
  }
  else
  {
    //actionDisconnectMenu_->setIcon( KIcon("network-disconnect") );
    stateChanged( "disconnected" );

    // Never show the status bar on the login screen
    statusBar()->hide();
  }

  // Enable showing the settings for this account now that we are connected
  actionShowSettings_->setEnabled( connected );
  // Enable exporting list for this connected account
  actionExportList_->setEnabled( connected );
}



/**
 * Return a singleton instance of the KMess class
 */
MainWindow* MainWindow::instance()
{
  if( instance_ == 0 )
  {
    // Assign the instance to the singleton variable
    instance_ = new MainWindow();
  }

  return instance_;
}



// Initialize the system tray widget
bool MainWindow::initSystemTrayWidget()
{
  // Create the widget
  systemTrayWidget_ = new SystemTrayWidget( this ) ;
  if ( systemTrayWidget_ == 0 )
  {
    kmDebug() << "Couldn't create system tray widget.";
    return false;
  }

  // Plug some of the menu actions into the system tray widget's menu.
  QMenu *menu = systemTrayWidget_->menu();
  menu->addAction( actionConnectMenu_ );
  menu->addMenu( actionDisconnectMenu_ );
  menu->addSeparator();
  menu->addMenu( actionStatus_ );
  menu->addSeparator();
  menu->addAction( actionShowSettings_ );
  menu->addAction( actionShowGlobalSettings_ );

  systemTrayWidget_->enable();

  return true;
}



void MainWindow::loadContactList()
{
#ifdef KMESSDEBUG_MAINWINDOW
  kmDebug() << "Loading properties for the contact list";
#endif

  Account *account = Account::connectedAccount;
  if ( account == 0 )
  {
    kmWarning() << "No account connected - something is very wrong.";
    return;
  }

  // sort out sort positions.
  // nice trick - QMap sorts ascending by key ;)
  QMap<int, Group> sortedGroups;
  foreach( KMess::MsnGroup *g, globalSession->contactList()->groups() )
  {
    Group kmessGroup( g );
    sortedGroups.insertMulti( kmessGroup->getSortPosition(), kmessGroup );
  }

  // in the qmap they're sorted ascending by whatever their saved
  // sort order is (which might be bogus). let's ensure it's a valid order.
  int sortPos = 0;

  // the order of the special groups :)
  QList<QString> specialGroupPos;
  specialGroupPos << KMess::SpecialGroups::ONLINE;
  specialGroupPos << KMess::SpecialGroups::INDIVIDUALS;
  specialGroupPos << KMess::SpecialGroups::FAVORITES;
  specialGroupPos << KMess::SpecialGroups::ALLOWED;
  specialGroupPos << KMess::SpecialGroups::REMOVED;
  specialGroupPos << KMess::SpecialGroups::OFFLINE;

  foreach( Group g, sortedGroups )
  {
    if ( ! g->isSpecialGroup() )
    {
      g->setSortPosition( sortPos++ );
    }
    else
    {
      int pos = specialGroupPos.indexOf( g->id() ) + 9000;
      pos = ( pos == 0 ? 9999 : pos );
      g->setSortPosition( pos );
    }
    kmDebug() << "Sort position of "<<g->msnGroup()->name() <<":"<<g->getSortPosition();
  }

  // and now the groups are sorted properly.

  // Flush any changes to disk
  contactsManager->saveProperties();
}



// A connection has been made with the notification server.
void MainWindow::loggedIn()
{
  // Enable/disable the menus
  enableMenus( true );

  Account *account = Account::connectedAccount;

  // now set our application data - we have a connected account to get chat logging
  // details from.
  changedLoggingSettings();

  KMess::ChatLoggingMode mode = ( account->getSettingBool("LoggingEnabled") ? KMess::LoggingActive : KMess::LoggingInactive );
  KMess::Utils::setClientLogging( mode );

  // load the contact list data.
  loadContactList();

  connect( globalSession->self(), SIGNAL(  endpointsChanged() ),
           this,                  SLOT  (  rebuildDisconnectMenu() ) );

  // update connectedAccount with the right data on change.
  connect( globalSession->self(),     SIGNAL(  displayNameChanged() ),
           account,                   SLOT  ( friendlyNameChanged() ) );
  connect( globalSession->self(),     SIGNAL( personalMessageChanged() ),
           account,                   SLOT  ( personalMessageChanged() ) );

  // Initialize the idle watcher
  IdleTimer *idleTimer = KMessApplication::instance()->idleTimer();
  connect( Account::connectedAccount, SIGNAL( changedTimerSettings() ),
           idleTimer,                 SLOT  (        updateWatcher() ) );

  // When logging settings change, change logging mode in library.
  connect( Account::connectedAccount, SIGNAL( changedLoggingSettings() ),
           this,                      SLOT  ( changedLoggingSettings() ) );
  connect( Account::connectedAccount, SIGNAL( changedDisplaySettings() ),
           this,                      SLOT  ( changedDisplaySettings() ) );

  // Initialize the tray icon status changer
  connect( globalSession,     SIGNAL( changedStatus() ),
           systemTrayWidget_, SLOT  ( statusChanged() ) );

  // Load themes
  EmoticonManager::instance()->loadThemes( account->getHandle() );

  // Set up the toggle and view mode tools to match the account settings
  actionShowAllowed_     ->setChecked( account->getSettingBool( "ContactListAllowedGroupShow" ) );
  actionShowOffline_     ->setChecked( account->getSettingBool( "ContactListOfflineGroupShow" ) );
  actionShowRemoved_     ->setChecked( account->getSettingBool( "ContactListRemovedGroupShow" ) );
  actionShowEmpty_       ->setChecked( account->getSettingBool( "ContactListEmptyGroupsShow"  ) );
  actionShowHistoryBox_  ->setChecked( account->getSettingBool( "UIHistoryBoxShow"            ) );
  actionViewAlphabetical_->setChecked( account->getSettingBool( "ContactListSortAlphabetical" ) );

  changedDisplaySettings();

  // Set the list's groups and contacts display mode
  actionViewMode_->setCurrentItem( account->getSettingInt( "ContactListDisplayMode" ) );

  // Check the 'Show search bar' option if the user has left it open the last login
  actionShowSearch_->setChecked( account->getSettingBool( "UISearchBarShow" ) );

  // Show the connected message
  statusMessage( i18n("Connected"), false );

  rebuildDisconnectMenu();

  // Load and show the contact list
  switchViewToContactList();

  // Set the caption
  setCaptionToUser();

  // If the KMess welcome dialog hasn't been shown for this user yet, show it
  if( ! account->getSettingBool( "WelcomeDialogShown" ) )
  {
    WelcomeDialog *welcomeDialog = new WelcomeDialog();
    welcomeDialog->show();
    // it will clean itself up later

    // It is not shown anymore if the user will save the wizard's settings
  }

  // Finally, save the settings
  saveProperties();
}



// Close has been selected from the menu.
void MainWindow::menuClose()
{
#ifdef KMESSDEBUG_MAINWINDOW
  kmDebug() << "'Close' was selected from the menu.";
#endif

  // Hide the window
  // Tell that KMess is still visible in the tray
  hide();
  systemTrayWidget_->displayCloseMessage();

#ifdef KMESSTEST
  KMESS_ASSERT( isHidden() );
#endif
}



// Quit was selected from the menu.
void MainWindow::menuQuit()
{
#ifdef KMESSDEBUG_MAINWINDOW
  kmDebug() << "'Quit' was selected from the menu.";
#endif

  // Tell the application manager we want to quit
  KMessApplication::instance()->setQuitSelected( true );

  close();  // Close this window, initiates quit
}



// Tell the user that KMess hides in the systray  Called automatically by KMainWindow::closeEvent
bool MainWindow::queryClose()
{
  KMessApplication *kmessApp = KMessApplication::instance();

  // Only allow KMess to quit if:
  // - the "quit" option was used from the menu
  // - the session manager wants to quit (we're called from KApplication::commitData())
  if( kmessApp->quitSelected() || kmessApp->sessionSaving() )
  {
#ifdef KMESSDEBUG_MAINWINDOW
    kmDebug() << "Accepting quit request.";
#endif

    // Make sure that substituent also return true
    kmessApp->setQuitSelected( true );

    // Prepare closing, MainWindow::queryClose() accepted the close request.
    // After this method returns, KApplication::aboutToQuit() also kicks in, handled by KMessApplication.

    // Allow the request
    return true;
  }
  else
  {
    // Tell that KMess is still visible in the tray
    systemTrayWidget_->displayCloseMessage();

#ifdef KMESSDEBUG_MAINWINDOW
    kmDebug() << "Rejecting quit request, hiding window";
#endif

    // Only hide, disallow event
    hide();
    return false;
  }
}



// Reject quitting unless the quit menu was pressed  Called automatically by KMainWindow::closeEvent
bool MainWindow::queryExit()
{
  KMessApplication *kmessApp = KMessApplication::instance();

  // With KDE 4 this function is also called when another KMainWindow closes.
  // Reject an attempt to quit if the user didn't select quit manually, or is logging out from KDE.
  // If a window still causes KMess to quit, use setAttribute( Qt::WA_QuitOnClose, false ) on it.
  if( kmessApp->quitSelected() || kmessApp->sessionSaving() )
  {
#ifdef KMESSDEBUG_MAINWINDOW
    kmDebug() << "Accepting exit request.";
#endif

    return true;
  }
  else
  {
    // For some reason, KDE keeps bugging us when the chat window closes.
    // Reject the request again.

#ifdef KMESSDEBUG_MAINWINDOW
    kmDebug() << "Rejecting exit request! Keep running with all windows hidden.";
#endif

    return false;
  }
}



// Get application style.
QString MainWindow::applicationStyle()
{
  return applicationStyle_;
}



// Set application style.
void MainWindow::setApplicationStyle( const QString &styleName )
{
  // TODO unset unused stylesheet resources

  if( styleName == applicationStyle_ )
  {
    // The selected style is the same as the current one, nothing to do
    return;
  }

#ifdef KMESSDEBUG_MAINWINDOW
  kmDebug() << "Setting application style to" << styleName;
#endif

  applicationStyle_ = styleName;

  KStandardDirs *dirs = KGlobal::dirs();

  // Find the resources file
  QString resourcesPath = dirs->findResource( "data", KMESS_NAME "/appstyles/"
    + styleName + ".rcc" );

  if( resourcesPath.isEmpty() )
  {
    qWarning() << "Could not find application style " << styleName;
    KMessageBox::sorry( this, i18n( "Could not find application style '%1'.", styleName ), i18n("KMess") );
    return;
  }
#ifdef KMESSDEBUG_MAINWINDOW
  else
  {
    qDebug() << "Loading application style from " << resourcesPath;
  }
#endif

  if( ! QResource::registerResource ( resourcesPath, "/appstyles/"+ styleName ) )
  {
    KMessageBox::sorry( this, i18n( "Could not load application style '%1'.", styleName ), i18n("KMess") );
    return;
  }

  // Open stylesheet from resources file
  QFile styleFile ( ":/" + styleName + ".qss" );

  if ( ! styleFile.open( QFile::ReadOnly ) )
  {
    KMessageBox::sorry( this, i18n( "The application style '%1' is broken.", styleName ), i18n("KMess") );
    return;
  }

  // Set stylesheet
  QString styleSheet = QString( styleFile.readAll() );
  KMessApplication::instance()->setStyleSheet( styleSheet );
}



// Ask the user to authenticate on a proxy
void MainWindow::proxyAuthenticate( const QNetworkProxy &proxy, QAuthenticator *authenticator )
{
#ifdef KMESSDEBUG_SHARED_PROXY_AUTH
  kmDebug() << "Authentication to proxy" << proxy.hostName() << "required.";
  kmDebug() << "Realm:" << authenticator->realm() << "Username:" << authenticator->user();
#else
  Q_UNUSED( proxy );
#endif

  // TODO port it to QInputDialog
  KPasswordDialog::KPasswordDialogFlags flags =
                                                KPasswordDialog::ShowUsernameLine
#if KDE_IS_VERSION( 4, 1, 0 )
                                              | KPasswordDialog::ShowDomainLine
                                              | KPasswordDialog::DomainReadOnly
                                              | KPasswordDialog::ShowAnonymousLoginCheckBox
#endif
                                              | KPasswordDialog::ShowKeepPassword;

  QPointer<KPasswordDialog> loginDialog = new KPasswordDialog( 0, flags );
  loginDialog->setPrompt( i18nc( "Dialog box message",
                                 "Enter your username and password to access the network proxy." ) );

  // The user canceled
  if( loginDialog->exec() != QDialog::Accepted || loginDialog == 0 )
  {
      return;
  }

  authenticator->setUser( loginDialog->username() );
  authenticator->setPassword( loginDialog->password() );
}



// Load the application's global state
void MainWindow::readGlobalProperties( KConfig *sessionConfig )
{
#ifdef KMESSDEBUG_MAINWINDOW
  kmDebug() << "Loading current state...";
#endif

  KConfigGroup settings( sessionConfig->group( "state" ) );

  // If the list window was visible, show it again
  setVisible( settings.readEntry( "isVisible", false ) );

  // If an account was connected, try connecting it again
  QString connectedAccount( settings.readEntry( "connectedAccount", QString() ) );

  // Delete the setting, if needed it'll be added again on session close
  settings.deleteEntry( "connectedAccount" );

  checkAutologin( connectedAccount );
}



// Read in account and other properties
void MainWindow::readProperties( const KConfigGroup &config )
{
#ifdef KMESSDEBUG_MAINWINDOW
  kmDebug() << "Reading properties";
#endif

  KConfigGroup group;

  // Choose the config group if it was not given
  if( &config == 0 )
  {
    group = KMessConfig::instance()->getGlobalConfig( "General" );
  }
  else
  {
    group = config;
  }

  // Resize the window to decent dimensions if there's no saved state.
  resize( 300, 500 );
  restoreWindowSize( group );

  // Pull in the window size and position
  QSize windowsize = group.readEntry( "Size", QSize() );
  if( ! windowsize.isEmpty() )
  {
    resize( windowsize );
  }
  QPoint position = group.readEntry( "Position", QPoint() );
  if( ! position.isNull() )
  {
    move( position );
  }

  bool bViewMenubar = group.readEntry( "ShowMenuBar", true );
  actionShowMenuBar_->setChecked( bViewMenubar );
  menuBar()->setVisible( bViewMenubar );

  actionShowStatusBar_->setChecked( group.readEntry( "ShowStatusBar", true ) );
  showStatusBar();
}



void MainWindow::rebuildDisconnectMenu()
{
  actionDisconnectMenu_->clear();

  // create the actions for "sign out from here" and "Sign out from everywhere"
  KAction *action = new KAction( i18nc( "%1 is the name of the local computer",
                                       "Sign out from here (%1)",
                                       globalSession->self()->localEndpoint()->name() )
                                 , actionDisconnectMenu_ );
  action->setData( globalSession->self()->localEndpoint()->guid().toString() );
  actionDisconnectMenu_->addAction( action );

  action = new KAction( i18n( "Sign out from everywhere" ), actionDisconnectMenu_ );
  action->setData( "EVERYWHERE" );
  actionDisconnectMenu_->addAction( action );

  actionDisconnectMenu_->addSeparator();

  // now add the actions for signing out of the other endpoints.
  foreach( KMess::MPOPEndpoint *ep, globalSession->self()->endpoints() )
  {
    if ( ep->guid() == globalSession->self()->localEndpoint()->guid() ) continue;

    QString title = i18nc( "%1 is the name of the remote computer", "Sign out from %1", ep->name() );

    action = new KAction( title, actionDisconnectMenu_ );
    action->setData( ep->guid().toString() );

    actionDisconnectMenu_->addAction( action );
  }
}

// The user wants to reconnect
void MainWindow::reconnect()
{
  if( lastConnectedAccount_.isEmpty() )
  {
#ifdef KMESSDEBUG_MAINWINDOW
    kmDebug() << "Unable to reconnect: invalid account.";
#endif
    return;
  }

#ifdef KMESSDEBUG_MAINWINDOW
  kmDebug() << "Reconnecting" << lastConnectedAccount_;
#endif

  initialView_->reconnect( lastConnectedAccount_, true );
}




// Save the application's global state
void MainWindow::saveGlobalProperties( KConfig *sessionConfig )
{
#ifdef KMESSDEBUG_MAINWINDOW
  kmDebug() << "Saving current state...";
#endif

  KConfigGroup settings( sessionConfig->group( "state" ) );

  QString connectedAccount;
  if( globalSession->isLoggedIn() )
  {
    connectedAccount = globalSession->sessionSettingString( "AccountHandle" );
  }

  settings.writeEntry( "isVisible",        isVisible()      );
  settings.writeEntry( "connectedAccount", connectedAccount );
}



// Save account and other properties
void MainWindow::saveProperties( KConfigGroup &config )
{
  KConfigGroup group;

  // Choose the config group if it was not given
  if( &config == 0 )
  {
    group = KMessConfig::instance()->getGlobalConfig( "General" );
  }
  else
  {
    group = config;
  }

#ifdef KMESSDEBUG_MAINWINDOW
  kmDebug() << "Saving properties";
#endif

  group.writeEntry( "Size",          size()                            );
  group.writeEntry( "Position",      pos()                             );
  group.writeEntry( "ShowMenuBar",   actionShowMenuBar_  ->isChecked() );
  group.writeEntry( "ShowStatusBar", actionShowStatusBar_->isChecked() );

  // Have all the accounts save their properties
  const QList<Account*> accountsList = AccountsManager::instance()->getAccounts();
  foreach( Account *account, accountsList )
  {
    // Avoid saving guest accounts
    if( account->isGuestAccount() )
    {
      continue;
    }

    // Save properties of accounts
    account->saveProperties();
  }

  // Save the passwords (blocking the flow, or KMess may quit before it's done)
  AccountsManager::instance()->savePasswords( true );

  // save the contact list
  contactsManager->saveContactList();

  // Write data now!
  group.sync();
}



// Set the caption
void MainWindow::setCaptionToUser()
{
  // On start, use the default caption
  if( globalSession == 0 )
  {
    setCaption( QString() );
    return;
  }

  QString friendlyName;

  if( ! globalSession->self()->passportVerified() )
  {
    // The account is not verified, use the email address
    friendlyName = globalSession->self()->handle();
  }
  else
  {
    // Get the friendly name, and avoid newlines which cause problems to the titlebar layout
    friendlyName = Account::connectedAccount->getFriendlyName().replace( "\n", " " );
  }

  // Set a standard caption if there is no friendly name yet (ie. first connection)
  if( friendlyName.isEmpty() )
  {
    setCaption( QString() );
    return;
  }

  // Set the caption
  setPlainCaption( i18nc( "Main window caption: switched order to easily distinguish it from chats", "KMess - %1", friendlyName ) );
}



// Show the chat history dialog and, if requested, that of a specific contact
void MainWindow::showChatHistory( const QString &handle )
{
  ChatHistoryDialog *dialog = new ChatHistoryDialog( this );

  dialog->setContact( handle );
  dialog->show();
}



// Show a "Contact added you" dialog
void MainWindow::showContactAddedUserDialog( KMess::MsnContact *contact )
{
#ifdef KMESSDEBUG_MAINWINDOW
  kmDebug() << "Contact" << contact << "added user, showing dialog";
#endif

  // Show a dialog and let the user choose whether to add, allow, or block the contact.
  if( ! contactAddedUserDialog_ )
  {
    contactAddedUserDialog_ = new ContactAddedUserDialog( contact );
  }
  else
  {
    contactAddedUserDialog_->addTab( contact );
  }

  connect( contactAddedUserDialog_,  SIGNAL(                 userChoice(KMess::MsnContact*,GroupsList,int) ),
           this,                     SLOT  ( slotContactAddedUserChoice(KMess::MsnContact*,GroupsList,int) ) );

  contactAddedUserDialog_->show();
}



// Show the context-sensitive menu item
void MainWindow::showContextMenu()
{
  if( KMESS_NULL( view_ ) ) return;
  if( KMESS_NULL( actionConnectMenu_ ) ) return;
  if( KMESS_NULL( actionContextMenu_ ) ) return;

  // HACK: Get where the triggered action is located, by asking a menu
  QRect actionPos( actionConnectMenu_->menu()->actionGeometry( actionContextMenu_ ) );

  view_->showContextMenu( actionPos.bottomRight() ); // Approximate to a side of the action, can't really do more
}



// Make sure that the window is visible on Mac
void MainWindow::showEvent( QShowEvent *e )
{
  Q_UNUSED( e );
  Q_ASSERT( isVisible() );

#ifdef Q_WS_MAC
  QRect r( KGlobalSettings::desktopGeometry( this ) );
  kmDebug() << "Desktop geometry: " << r << " widget pos: " << pos();
  if( !r.contains( pos() ) )
  {
    // Window is outside desktop geometry, set it to the right position
    kmDebug() << "Window is outside desktop geometry, setting it to "
             << r.topLeft();
    move( r.topLeft() );
  }
#endif
}



// The "Show Global Settings" option was clicked.
void MainWindow::showGlobalSettings()
{
  GlobalSettingsDialog *globalSettingsDialog = GlobalSettingsDialog::instance();

  globalSettingsDialog->show();
  globalSettingsDialog->raise();
}



void MainWindow::showListExportDialog()
{
  ListExportDialog *listExportDialog = new ListExportDialog();
  listExportDialog->show();
}



// "Show menu bar" was toggled.
void MainWindow::showMenuBar()
{
  // Just show the menubar if it was hidden
  if( ! menuBar()->isVisible() )
  {
    menuBar()->setVisible( true );
    return;
  }

  // Ask the user if he/she really wants to hide the menubar, to avoid mistakes
  // TODO: If the user disables the shortcut for the menu action, no shortcuts will be shown
  int res = KMessageBox::questionYesNo( this,
                                        i18nc( "Question dialog box message",
                                                "<html>Are you sure you want to hide the menu bar? "
                                                "You will be able to show it again by using this "
                                                "keyboard shortcut: <b>%1</b></html>",
                                                actionShowMenuBar_->shortcut().primary().toString( QKeySequence::NativeText ) ),
                                        i18nc( "Dialog box caption: hiding the menu bar", "Hiding the Menu" ),
                                        KStandardGuiItem::yes(),
                                        KStandardGuiItem::no(),
                                        "hideMenuBarQuestion" );

  if( res == KMessageBox::Yes )
  {
    menuBar()->setVisible( actionShowMenuBar_->isChecked() );
  }
  else
  {
    actionShowMenuBar_->setChecked( true );
  }
}



// Opens the network window
void MainWindow::showNetworkWindow()
{
  NetworkWindow::instance()->show();
}



// Show the settings dialog for a given account
void MainWindow::showSettingsForAccount( Account *account )
{
  // Show the settings dialog
  AccountSettingsDialog *accountSettingsDialog = AccountSettingsDialog::instance();
  accountSettingsDialog->loadSettings( account );
  accountSettingsDialog->show();
}



// Show the settings dialog for the current account.
void MainWindow::showSettingsForCurrentAccount()
{
  // Use the Account object so it's consistent with the other AccountAction events.
  if( KMESS_NULL(Account::connectedAccount) ) return;
  showSettingsForAccount( Account::connectedAccount );
}



// "Show status bar" was toggled.
void MainWindow::showStatusBar()
{
  statusBar()->setVisible( actionShowStatusBar_->isChecked() );
}



// Opens the transfer manager
void MainWindow::showTransferWindow()
{
  TransferWindow *transferWindow = TransferWindow::getInstance();
  transferWindow->show();
}



// Show the user's MSN profile.
void MainWindow::showUserProfile()
{
  if( globalSession == 0 )
  {
    return;
  }

  const QString url( globalSession->sessionSettingString( "UrlProfile" ) );
  KMessShared::openBrowser( KUrl( url ) );
}



// Add the account to the GUI
void MainWindow::slotAccountAdded( Account *account )
{
  // Check if the account is already present in the menus
  if( connectMenuItems_.contains( account ) )
  {
    return;
  }

  AccountAction *accountAction;

  // Make the action for the "Connect..." menu
  accountAction = new AccountAction( account, this );
  actionConnectMenu_->addAction( accountAction );
  connectMenuItems_[ account ] = accountAction;

  connect( accountAction, SIGNAL(          activated(Account*) ),
           this,          SLOT  ( connectWithAccount(Account*) ) );
}



// An account's settings have been changed
void MainWindow::slotAccountChanged( Account *account, QString oldHandle, QString oldFriendlyName )
{
  QString newHandle( account->getHandle() );

#ifdef KMESSDEBUG_MAINWINDOW
  kmDebug() << "Saving UI settings for" << newHandle;
#endif

  if( globalSession->isLoggedIn() )
  {
    bool isCurrentAccount = ( oldHandle == globalSession->sessionSettingString( "AccountHandle" ) );

    // If this is the current account and the friendlyname changed, update
    // it in the global session
    if( isCurrentAccount
    &&  globalSession->sessionSettingBool( "AccountVerified" )
    &&  account->getFriendlyName( STRING_ORIGINAL ) != oldFriendlyName )
    {
      globalSession->self()->setDisplayName( account->getFriendlyName( STRING_ORIGINAL ) );
    }
  }

  // Change the account name in the UI when it's been changed
  if( oldHandle == newHandle )
  {
    return;
  }

  // Update the menu items
  connectMenuItems_ [ account ]->updateText();
}



// Delete the given account from the UI
void MainWindow::slotAccountDeleted( Account *account )
{
  AccountAction *accountAction;
  if( KMESS_NULL(account) ) return;

  // Remove from connect menu.
  accountAction = connectMenuItems_[ account ];
  if( ! KMESS_NULL(accountAction) )
  {
    actionConnectMenu_->removeAction( accountAction );
    delete connectMenuItems_.take( account );
  }
}



/**
 * @brief Update the UI when the network connection status changes
 *
 * This method receives KDE's Solid network connection status changes, and updates the view's widgets
 * to reflect it.
 *
 * @param newStatus  The new status of the network connection
 */
void MainWindow::slotConnectionStatusChanged( Solid::Networking::Status newStatus )
{
#ifdef KMESSDEBUG_MAINWINDOW
  QString status;
  switch( newStatus )
  {
    case Solid::Networking::Unknown:       status = "Unknown";           break;
    case Solid::Networking::Unconnected:   status = "Unconnected";       break;
    case Solid::Networking::Disconnecting: status = "Disconnecting";     break;
    case Solid::Networking::Connecting:    status = "Connecting";        break;
    case Solid::Networking::Connected:     status = "Connected";         break;
    default:                               status = "Other (not known)"; break;
  }
  kmDebug() << "The network connection status has changed to:" << status << ".";
#endif

  // If Solid is unable to retrieve the status, assume we're connected
  bool isNetConnected = ( newStatus == Solid::Networking::Connected || newStatus == Solid::Networking::Unknown );

  // Do not disable the connection menu if we're connected already
  if( globalSession->isLoggedIn() )
  {
    // Do update the status message if the network is down
    if( newStatus == Solid::Networking::Unconnected )
    {
      // TODO: this slightly interferes with the "Pings lost..." message, and doesn't recover directly if solid indicates so.
      statusMessage( i18n("Connection could be down..."), false );
    }

    return;
  }

  // Update the menu
  actionConnectMenu_->setEnabled( isNetConnected );
}



// The user was presented the "contact added user" dialog and has made a choice
void MainWindow::slotContactAddedUserChoice( const KMess::MsnContact* contact, const GroupsList groups, const int code )
{
#ifdef KMESSDEBUG_MAINWINDOW
  kmDebug() << "Contact" << contact << "added user, who has choosen option:" << code
           << "Group list:" << groups;
#endif

  switch( (ContactAddedUserWidget::ReturnCode) code )
  {
    case ContactAddedUserWidget::ADD:
      // Add the contact to the allowed and friends lists
      globalSession->contactList()->addContact( contact, groups );
      break;

    case ContactAddedUserWidget::ALLOW:
      // Add the contact to the allowed list
      globalSession->contactList()->unblockContact( contact );
      break;

    case ContactAddedUserWidget::BLOCK:
      // Add the contact to the blocked list.
      globalSession->contactList()->blockContact( contact );
      break;

    case ContactAddedUserWidget::IGNORE:
    default:
      // Action ignored
      break;
  }
}



// Record the network library states
void MainWindow::slotStatusEvent( KMess::StatusMessageType type, KMess::StatusMessage message, QVariant data )
{
  Q_UNUSED( type );

  switch( message )
  {
    case KMess::WarningPingLost:
    {
      int lostPings = data.toInt();
      statusMessage( i18np( "1 ping lost", "%1 pings lost", lostPings ), false );

      // Disconnect when too many pings are lost
      if( lostPings > MAX_PINGS_LOST )
      {
        globalSession->logOut();
      }
      break;
    }

    default:
      break;
  }

  lastStatusEvent_ = message;
}



// Initiate a connection to the server. Use connectWithAccount() to connect, not this method.
void MainWindow::startConnection( Account *account, const QString &password )
{
  if( KMESS_NULL(account) ) return;

  Account::connectedAccount = account;

  // First disconnect, to unregister all contacts
  if( globalSession->isLoggedIn() )
  {
    globalSession->logOut();
  }

  // Update the initial view with account details and start the connection if everything is ok
  if( ! initialView_->startConnecting( account->getHandle(), false /* emitConnectionSignal */ ) )
  {
    show();
    return;
  }

  // first clear all private data, just in case.
  contactsManager->clear();

  Account::connectedAccount = account;

  // read configuration information for the given account.
  contactsManager->loadConfig( account );

  // Update the main view's controls to match this account's details.
  changedStatus( account );

  // Listen to Account changes, so we can react on them in the interface
  // Any connections made mere must be disconnected in ::disconnected()!
  connect( globalSession->self(),     SIGNAL(          displayNameChanged() ),
           systemTrayWidget_,         SLOT  (               statusChanged() ) );
  connect( globalSession->self(),     SIGNAL(          displayNameChanged() ),
           this,                      SLOT  (            setCaptionToUser() ));
  connect( account,                   SIGNAL( changedNowListeningSettings() ),
           this,                      SLOT  ( changedNowListeningSettings() ));
  connect( account,                   SIGNAL(     changedEmoticonSettings() ),
           EmoticonManager::instance(),SLOT(slotChangedEmoticonSettings() ) );
  connect( account,                   SIGNAL(      changedDisplayPicture() ),
           this,                      SLOT  (      changedDisplayPicture() ));

  // Allow users to cancel the login attempt
  //actionDisconnectMenu_->setEnabled( true );

  // Connect to the server
  const KMess::MsnStatus status =  static_cast<KMess::MsnStatus>( account->getSettingInt( "StatusInitial") );
  const QString &passwordToUse = ( password.isEmpty() ? account->getPassword() : password );

  // TODO we could add the initial display picture path as 4th parameter in logIn ( QString() by default )
  changedDisplayPicture();

  // the cache file for contact list data
  QString path = KMessConfig::instance()->getAccountDirectory( account->getHandle() ) + "/cache";
  globalSession->setCacheDir( path );

  globalSession->logIn( account->getHandle(), passwordToUse, status );
}



// Change the status bar message.
void MainWindow::statusMessage( QString message, bool isError )
{
  if( message != message_ )
  {
    statusLabel_->setText( message );
    message_ = message;

#ifdef KMESSDEBUG_MAINWINDOW
    kmDebug() << "Changed message to" << message;
#endif
  }

  // Do not alter the online timer when the status has not changed
  if( isError == isErrorStatus_ )
  {
    return;
  }

  if( isError )
  {
      onlineTimer_->stop();
      statusTimer_->setText( "00:00" );
  }
  else
  {
      onlineTime_ = 0;
      onlineTimer_->start( 60000 );
  }

  isErrorStatus_ = isError;
}



// Switch the view to the contact list
void MainWindow::switchViewToContactList()
{
  // Clean up views beforehand.
  // Whatever was the previous centralWidget will be deleted below. We mark
  // any objects that still exist here, for deletion, and set them to zero.
  // See also ::switchViewToInitialScreen
  if( view_ )
  {
    view_->deleteLater();
    view_ = 0;
  }
  if( initialView_ )
  {
    initialView_->deleteLater();
    initialView_ = 0;
  }

  // Create and initialize the view's widgets
  view_ = new KMessView( this );

  connect( view_,                      SIGNAL( showSettings()                           ),   // Show settings
           this,                       SLOT  ( showSettingsForCurrentAccount()          ) );
  connect( view_,                      SIGNAL(  selectionChanged(QItemSelection)        ),   // The selection has changed
           this,                       SLOT  ( updateContextMenu(QItemSelection)        ) );


  // Set it as the application's main widget
  setCentralWidget( view_ );
}



// Switch the view to the initial login screen
void MainWindow::switchViewToInitialScreen()
{
  // Just use the same initialView_ if it's already there
  if( initialView_ )
  {
    initialView_->reset();
    return;
  }

  // Clean up view_ if it exists
  if( view_ )
  {
    view_->deleteLater();
    view_ = 0;
  }

  // Create and set up the new widget
  initialView_ = new InitialView( this );

  // Connect it so it triggers the login process
  connect( initialView_,  SIGNAL(     connectWithAccount(QString,QString,bool,bool,bool,KMess::MsnStatus) ),
           this,          SLOT  (     connectWithAccount(QString,QString,bool,bool,bool,KMess::MsnStatus) ) );
  // Show settings
  connect( initialView_,  SIGNAL(           showSettings(Account*)                                 ),
           this,          SLOT  ( showSettingsForAccount(Account*)                                 ) );

  // Events
  connect( globalSession, SIGNAL(            statusEvent(KMess::StatusMessageType,KMess::StatusMessage,QVariant) ),
           initialView_,  SLOT  (            statusEvent(KMess::StatusMessageType,KMess::StatusMessage,QVariant) ) );

  // Set it as the window's main widget
  setCentralWidget( initialView_ );
}



// The "show allowed contacts" menu item has been toggled.
void MainWindow::toggleShowAllowed(bool show)
{
  Account::connectedAccount->setSetting( "ContactListAllowedGroupShow", show );
}



// The "show empty groups" menu item has been toggled.
void MainWindow::toggleShowEmpty( bool show )
{
  Account::connectedAccount->setSetting( "ContactListEmptyGroupsShow", show );
}



// The "show history box" menu item has been toggled.
void MainWindow::toggleShowHistoryBox(bool show)
{
  if( view_ != 0 )
  {
    view_->toggleShowHistoryBox( show );
  }
}


// The "show offline contacts" menu item has been toggled.
void MainWindow::toggleShowOffline(bool show)
{
  Account::connectedAccount->setSetting( "ContactListOfflineGroupShow", show );
}



// The "show removed contacts" menu item has been toggled.
void MainWindow::toggleShowRemoved(bool show)
{
  Account::connectedAccount->setSetting( "ContactListRemovedGroupShow", show );
}



// The "show search in contact list" menu item has been toggled.
void MainWindow::toggleShowSearchFrame(bool show)
{
  if( view_ != 0 )
  {
    view_->toggleShowSearchFrame( show );
  }
}



// The "Sort Alphabetical" menu item has been toggled.
void MainWindow::toggleSortAlphabetical( bool alpha )
{
  Account::connectedAccount->setSetting( "ContactListSortAlphabetical", alpha );
}



// Show the context-sensitive menu item
void MainWindow::updateContextMenu( const QItemSelection &selection )
{
  actionContextMenu_->setEnabled( ! selection.indexes().isEmpty() );
}



// Increment and update the online timer.
void MainWindow::updateOnlineTimer()
{
  QString timeText;

  onlineTime_ ++;

  timeText.sprintf( "%02i:%02i", onlineTime_ / 60, onlineTime_ % 60 );

  statusTimer_->setText( timeText );
}



// The user has gone idle
void MainWindow::userIsIdle()
{
  if( KMESS_NULL( globalSession ) ) return;

#ifdef KMESSDEBUG_IDLETIMER
  kmDebug() << "User is now idle, current status is" << globalSession->self()->status();
#endif

  // Only change the state if the user is currently set as online
  if( globalSession->self()->status() == KMess::OnlineStatus )
  {
#ifdef KMESSDEBUG_IDLETIMER
    kmDebug() << "Change status to Idle";
#endif
    // Request a status change to Idle
    globalSession->self()->changeStatus( KMess::IdleStatus );
    isIdleForced_ = false;
  }
#ifdef KMESSDEBUG_IDLETIMER
  else
  {
    kmDebug() << "No action taken";
  }
#endif
}



// The user is no longer idle
void MainWindow::userIsNotIdle()
{
  if( KMESS_NULL( globalSession ) ) return;

#ifdef KMESSDEBUG_IDLETIMER
  kmDebug() << "User is no longer idle, current status is" << globalSession->self()->status();
#endif

  // Only change the state if the user is currently set as idle AND the idle status was not forced by the user
  if( globalSession->self()->status() == KMess::IdleStatus && ! isIdleForced_ )
  {
#ifdef KMESSDEBUG_IDLETIMER
    kmDebug() << "Change status to Online";
#endif
    // Request a status change to Online
    globalSession->self()->changeStatus( KMess::OnlineStatus );
  }
#ifdef KMESSDEBUG_IDLETIMER
  else
  {
    kmDebug() << "No action taken";
  }
#endif
}



#include "mainwindow.moc"
