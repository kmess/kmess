/***************************************************************************
                          kmessview.cpp  -  description
                             -------------------
    begin                : Thu Jan 9 2003
    copyright            : (C) 2003 by Mike K. Bennett
                           (C) 2005 by Diederik van der Boor
    email                : mkb137b@hotmail.com
                           vdboor --at-- codingdomain.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "kmessview.h"

#include "contact/contact.h"

#include "utils/kmessshared.h"

#include "account.h"
#include "kmessdebug.h"
#include "kmessglobal.h"

#include <KIconEffect>
#include <KMenu>



// The constructor
KMessView::KMessView( QWidget *parent )
  : QWidget( parent ),
    Ui::KMessView()
{
  setupUi( this );

  contactListWidget_->setModel( globalSession->contactList()->contactListModel() );
  // Connect the selection model's signals
  connect( contactListWidget_, SIGNAL( selectionChanged(QItemSelection) ),
           this,               SIGNAL( selectionChanged(QItemSelection) ) );

  // Windows Live Messenger 8.5 and 2009 (latter only in the chat screen) don't
  // display any part further than 129 characters.
  personalMessageInput_->setMaxLength( 129 );

  friendlyNameInput_->setFormattingMode( STRING_LIST_SETTING );
  personalMessageInput_->setFormattingMode( STRING_LIST_SETTING );

  // Set an icon for the email label
  KIconLoader *loader = KIconLoader::global();
  emailPixmapLabel_       ->setPixmap( loader->loadIcon( "mail-mark-unread", KIconLoader::Small ) );
  nowListeningPixmapLabel_->setPixmap( loader->loadIcon( "speaker",          KIconLoader::Small ) );

  // Set the menu into button and connect the signals
  KMenu *menu = Status::getStatusMenu();
  statusButton_->setMenu( menu );
  connect( statusButton_, SIGNAL(  clicked() ),
           statusButton_, SLOT  ( showMenu() ) );

  // Enable the labels
  emailLabel_->setEnabled(true);

  const QFont &appFont( QApplication::font() );
  int appFontSize = appFont.pointSize();
  friendlyNameInput_   ->setStyleSheet( "QLabel { font-size: " + QString::number( appFontSize + 2 )  + "pt; }" );
  personalMessageInput_->setStyleSheet( "QLabel { font-size: " + QString::number( appFontSize - 1 )  + "pt; }" );

  // Hide the now listening until something is playing
  changedSong( QString(), QString(), QString(), false );

  Contact us( globalSession->self() );
  
  // Connect signals for the current session
  connect( globalSession,  SIGNAL( changedNoEmails()          ),    // Changed email count
           this,           SLOT  ( slotUpdateNoEmails()       ) );
  connect( us,   SIGNAL( statusChanged()            ),    // Changed status
           this, SLOT  ( slotUpdateUserStatus()     ) );

  // Connect signals for the connected account
  // Changed users display pic
  connect( Account::connectedAccount, SIGNAL( changedDisplayPicture()       ),
           this,                      SLOT  ( slotUpdateDisplayPicture()    ) );
  // Changed name
  connect( us,     SIGNAL(   displayNameChanged()         ),
           this,   SLOT  ( slotUpdateUserStatus()        ) );
  // Changed email display settings
  connect( Account::connectedAccount, SIGNAL( changedEmailDisplaySettings() ),
           this,                      SLOT  ( slotUpdateEmailDisplay()      ) );

  // These update the display pic size depending on the email & now listening options
  connect( Account::connectedAccount, SIGNAL( changedNowListeningSettings() ),
           this,                      SLOT  ( slotUpdateDisplayPicture()    ) );
  connect( Account::connectedAccount, SIGNAL( changedEmailDisplaySettings() ),
           this,                      SLOT  ( slotUpdateDisplayPicture()    ) );

  // Connect the display picture widget's signals to open the settings
  connect( userDisplayPicture_, SIGNAL(  leftClicked() ),
           this,                SIGNAL( showSettings() ) );
  connect( userDisplayPicture_, SIGNAL( rightClicked() ),
           this,                SIGNAL( showSettings() ) );

  // Connect signals for the history combobox and put the first string into it

  connect( globalSession, SIGNAL(            contactOnline(KMess::MsnContact*,bool) ),
           this,          SLOT  ( slotContactChangedStatus(KMess::MsnContact*,bool) ) );

  connect( globalSession, SIGNAL(           contactOffline(KMess::MsnContact*) ),
           this,          SLOT  ( slotContactChangedStatus(KMess::MsnContact*) ) );

  // Enable changing inline the friendly name and PM
  connect( friendlyNameInput_,    SIGNAL(               changed() ),
           this,                  SLOT  (    updateFriendlyName() ) );
  connect( personalMessageInput_, SIGNAL(               changed() ),
           this,                  SLOT  ( updatePersonalMessage() ) );

  historyBox_->insertItem( 0, Status::getIcon( KMess::OnlineStatus ),
                           i18n( "[%1] Logged in with %2", QTime::currentTime ().toString( "HH:mm:ss" ),
                           globalSession->sessionSettingString( "AccountHandle" ) ) );

  historyBox_->insertSeparator( 0 );

  // Enable the search box if the user has left it open at the last disconnection
  toggleShowSearchFrame( Account::connectedAccount->getSettingBool( "UISearchBarShow" ) );

  // Enable the history box
  toggleShowHistoryBox( Account::connectedAccount->getSettingBool( "UIHistoryBoxShow" ) );

  // Run the update functions with the current user data
  slotUpdateEmailDisplay();
  slotUpdateUserStatus();
  slotUpdateNoEmails();
  slotUpdateDisplayPicture();

  updateFriendlyName( true );
  updatePersonalMessage( true );
}



// The destructor
KMessView::~KMessView()
{
  // clear all private data
  // do this here so we are certain that no pending events area heading toward
  // the contact list view!
  contactsManager->clear();

#ifdef KMESSDEBUG_KMESSVIEW
  kmDebug() << "DESTROYED.";
#endif
}



// The currently playing song was changed.
void KMessView::changedSong( const QString &artist, const QString &/*album*/, const QString &track, bool playing )
{
  if( playing )
  {
    // Show status line
    if( ! nowListeningFrame_->isVisible() )
    {
      nowListeningFrame_->show();
      nowListeningFrame_->setMaximumHeight(40);
    }

    // Update label
    bool showDash = ( ! track.isEmpty() && ! artist.isEmpty() );
    const QString longTitle( track + ( showDash ? " - " : "" ) + artist );
    nowListeningLabel_->setText( longTitle );
    nowListeningLabel_->setToolTip( longTitle );
  }
  else
  {
    // Hide status line
    nowListeningFrame_->hide();
    nowListeningFrame_->setMaximumHeight(0);
  }
}



// Called when a contact is now online/offline
void KMessView::slotContactChangedStatus( KMess::MsnContact *c, bool isInitialStatus )
{
  if( c == 0 )
  {
    kmWarning() << "Contact is null!";
    return;
  }

  Contact contact( c );

  // One contact is now online/offline, put it in history combobox
  if( ! isInitialStatus )
  {
    QString friendlyname( contact->getFriendlyName() );
    QString handle( contact->getHandle() );
    const QString text( QString( "[%1] %2 (%3)" ).arg( QTime::currentTime().toString( "HH:mm:ss" ) )
                                                 .arg( friendlyname )
                                                 .arg( handle ) );

    const QIcon &icon( Status::getIcon( c->status() ) );

    // Remove oldest item if the count is >= 500 elements
    if( historyBox_->count() >= 500 )
    {
      historyBox_->removeItem( historyBox_->count() - 1 );
    }

    historyBox_->insertItem( 0, icon, text );
    historyBox_->setCurrentIndex ( 0 );
  }
}



// The "show history box" menu item has been toggled.
void KMessView::toggleShowHistoryBox( bool show )
{
  historyBox_->setVisible( show );

  // Save the current state to the Account
  Account::connectedAccount->setSetting( "UIHistoryBoxShow", show );
}



// The "show search in contact list" menu item has been toggled.
void KMessView::toggleShowSearchFrame( bool show )
{
  contactListWidget_->toggleShowSearchFrame( show );

  // Save the current state to the Account
  Account::connectedAccount->setSetting( "UISearchBarShow", show );
}



// The email label was clicked so open the user's email client or webmail
void KMessView::slotEmailLabelClicked()
{
  KMessShared::openEmailClient();
}



// Show the context menu
void KMessView::showContextMenu( const QPoint &point )
{
  contactListWidget_->showContextMenu( point );
}



/**
 * @brief Update the friendly name from the inline editing label.
 *
 * Sends to the server the new friendly name selected by clicking
 * the friendly name inline editing label.
 *
 * @param refreshOnly  When true, the FN widget is only updated with the
 *                     current account's name. When false, it is updated
 *                     with the default message, and nothing is sent to
 *                     the server.
 */
void KMessView::updateFriendlyName( bool refreshOnly )
{
  const QString& defaultTooltip( i18nc( "Default friendly name tooltip", "Click here to change your friendly name" ) );
  const QString& defaultName( Account::connectedAccount->getFriendlyName( STRING_ORIGINAL ).trimmed() );

  // When refreshing the message or when the focus losing isn't voluntary, don't update it on the server
  if( refreshOnly || ! isActiveWindow() || ! topLevelWidget()->isActiveWindow() )
  {
#ifdef KMESSDEBUG_KMESSVIEW
    kmDebug() << "Refreshing name only: initial update or non-intentional focus lose.";
#endif

    // Refresh the message
    friendlyNameInput_->setToolTip( defaultTooltip );
    friendlyNameInput_->setText( Contact( globalSession->self() ).getFriendlyName( STRING_ORIGINAL ) );
    return;
  }

  // Get message
  QString newName( friendlyNameInput_->text().trimmed() );

#ifdef KMESSDEBUG_KMESSVIEW
  kmDebug() << "Updating friendly name to:" << newName;
#endif

  // Set a default name (the email address) if the field is left empty
  if( newName.isEmpty() )
  {
    newName = Account::connectedAccount->getHandle();
  }

  // Only update the name if it's been changed
  if( defaultName == newName )
  {
    return;
  }


  // Notify changes to the MSN server
  globalSession->self()->setDisplayName( newName );

  // Save the changes
  friendlyNameInput_->setToolTip( newName );
}



/**
 * @brief Update the personal message from the inline editing label.
 *
 * Sends to the server the new personal message selected by clicking
 * the personal message inline editing label.
 *
 * @param refreshOnly  When true, the PM widget is only updated with the
 *                     current account's message. When false, it is updated
 *                     with the default message, and nothing is sent to
 *                     the server.
 */
void KMessView::updatePersonalMessage( bool refreshOnly )
{
  const QString& defaultMessage( i18nc( "Default personal message shown in the contact list", "[i]Click to set a personal message[/i]" ) );
  const QString& defaultTooltip( i18nc( "Default personal message tooltip", "Click here to insert a message to show to your contacts: they will see it along with your friendly name" ) );

  // When refreshing the message or when the focus losing isn't voluntary, don't update it on the server
  if( refreshOnly || ! isActiveWindow() || ! topLevelWidget()->isActiveWindow() )
  {
#ifdef KMESSDEBUG_KMESSVIEW
    kmDebug() << "Refreshing message only: initial update or non-intentional focus lose.";
#endif

    // Refresh the message
    const QString &message( Account::connectedAccount->getPersonalMessage( STRING_ORIGINAL ).trimmed() );
    if( message.isEmpty() )
    {
      personalMessageInput_->setToolTip( defaultTooltip );
      personalMessageInput_->setText( defaultMessage, true );
    }
    else
    {
      personalMessageInput_->setToolTip( message );
      personalMessageInput_->setText( message );
    }

    return;
  }

  // Get message
  const QString& message( personalMessageInput_->text().trimmed() );

#ifdef KMESSDEBUG_KMESSVIEW
  kmDebug() << "Updating personal message to:" << message;
#endif

  // Protect against weird situations where the label is seen as text.
  // The lost focus event is also sent when the while window looses focus
  if( message == defaultMessage )
  {
    kmWarning() << "Personal message label was about to be used as personal message!";
    return;
  }

  // Only update the message if it's been changed
  if( Account::connectedAccount->getPersonalMessage( STRING_ORIGINAL ) != message )
  {
    globalSession->self()->setPersonalMessage( message );
    personalMessageInput_->setToolTip( message );
  }

  // Restore the placeholder.
  if( message.isEmpty() )
  {
    personalMessageInput_->setToolTip( defaultTooltip );
    personalMessageInput_->setText( defaultMessage, true );
  }
}



// Update the users display pic
void KMessView::slotUpdateDisplayPicture()
{
  // Adjust the picture size depending on the rest of the widgets:
  // it should be small only when both now playing and new email info are off.
  int size = 96;
  if( ! Account::connectedAccount->getSettingBool( "NowListeningEnabled" )
  && ( ! Account::connectedAccount->getSettingBool( "ContactListEmailCountShow" ) || ! globalSession->sessionSettingBool( "EmailEnabled" ) ) )
  {
    size = 64;
  }
  userDisplayPicture_->setSize( size );

  // The user does not want to show a display pic, so just show an icon for their sign in status
  if( ! Account::connectedAccount->getSettingBool( "DisplayPictureEnabled" ) )
  {
    userDisplayPicture_->hide();
    return;
  }

  // Show the picture frame first
  userDisplayPicture_->show();

  // The picture is unchanged, do nothing
  const QString &pictureFileName( Account::connectedAccount->getDisplayPicture() );
  if( userDisplayPicture_->getPicture() == pictureFileName )
  {
#ifdef KMESSDEBUG_KMESSVIEW
    kmDebug() << "No need to change the display picture";
#endif
    return;
  }

#ifdef KMESSDEBUG_KMESSVIEW
  kmDebug() << "Changing display picture to path:" << pictureFileName;
#endif

  // Change the picture
  userDisplayPicture_->setPicture( pictureFileName );
}



// Change whether or not the email label is displayed based on account settings.
void KMessView::slotUpdateEmailDisplay()
{
  if ( Account::connectedAccount->getSettingBool( "ContactListEmailCountShow" ) && globalSession->sessionSettingBool( "EmailEnabled" ) )
  {
    emailFrame_->setHidden(false);
    //emailFrame_->setMargin(statusLayout_->margin());
    emailFrame_->setMaximumHeight(40);
  }
  else
  {
    emailFrame_->setHidden(true);
    //emailFrame_->setMargin(0);
    emailFrame_->setMaximumHeight(0);
  }
}



// Update the email count.
void KMessView::slotUpdateNoEmails()
{
  const QString& message( i18np( "%1 new email message",
                          "%1 new email messages",
                          globalSession->sessionSettingInt( "EmailCount" ) ) );

  // Update the label
  emailLabel_->setText( message );

  // Set the user's preferred email website.
  emailLabel_->setUrl( globalSession->sessionSettingString( "UrlWebmail" ) );
}



// Update the user's status.
void KMessView::slotUpdateUserStatus()
{
  // FIXME: Make sure this situation doesn't hit anymore:
  if( Account::connectedAccount == 0 )
  {
    kmWarning() << "connectedAccount is zero in slotUpdateUserStatus - skipping this call, but please fix it!";
    return;
  }

  const KMess::MsnStatus status( globalSession->self()->status() );

#ifdef KMESSDEBUG_KMESSVIEW
  kmDebug() << "Updating user status to:" << Status::getName( status );
#endif

  // Update the widgets
  updateFriendlyName( true );
  updatePersonalMessage( true );
  statusButton_->setText( Status::getName( status ) );
  statusButton_->setIcon( Status::getIcon( status ) );
}



#include "kmessview.moc"
