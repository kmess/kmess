/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright 2010 Daniel E. Moctezuma <democtezuma@gmail.com>          *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

#ifndef KMESSCONTACTLIST_H
#define KMESSCONTACTLIST_H

#include "../kmessglobal.h"
#include <KMess/MsnGroup>

#include "../utils/richtextparser.h"

#include <QObject>

/**
  * This class is one of the plugin interface classes.
  * It provides access to the Contact, ContactExtension, ContactPrivate and Group classes.
  *
  * @author Daniel E. Moctezuma
  * @ingroup Plugins
  */
class KMessContactList : public QObject
{
  Q_OBJECT

  public:
    // The constructor
                        KMessContactList( QObject* parent = 0 );

    // Return a list of all known contacts.
    ContactsList        getContacts() const;
    // Return a list of all known groups.
    GroupsList          getGroups()   const;

    // Return whether the contact has the user in his/her contact list.
    bool                hasUser( const QString &handle )          const;

    // Return whether the contact was allowed to see the user's presence.
    bool                isContactAllowed( const QString &handle ) const;
    // Return whether the contact was blocked.
    bool                isContactBlocked( const QString &handle ) const;
    // Return whether the contact is a friend.
    bool                isContactFriend( const QString &handle )  const;
    // Return whether the contact is currently offline.
    bool                isContactOffline( const QString &handle ) const;
    // Return whether the contact is currently online.
    bool                isContactOnline( const QString &handle )  const;
    // Return whether the contact has not accepted our friendship request.
    bool                isContactPending( const QString &handle ) const;
    // Return whether this contact is "unknown",
    // (i.e. they do not exist in any list in your address book).
    bool                isContactUnknown( const QString &handle ) const;
    // Return whether the group with the given ID is empty (i.e. has zero contacts in it).
    bool                isGroupEmpty( const QString &groupId )  const;
    // Return true if this is a special group.
    bool                isSpecialGroup( const QString &groupId ) const;

  public slots:
    // Request the service to add a contact to the friends list.
    void                addContact( KMess::MsnContact *contact, const QList<KMess::MsnGroup*> &groups = QList<KMess::MsnGroup*>() );
    void                addContact( const QString &handle, const KMess::NetworkType network = KMess::NetworkMsn, const QList<KMess::MsnGroup *> &groups = QList<KMess::MsnGroup*>() );
    // Request the service to add a contact to another group.
    void                addContactToGroup( KMess::MsnContact *contact, KMess::MsnGroup *group );
    // Request the service to add a new group.
    void                addGroup( const QString &name );
    // Request the service to block a contact.
    void                blockContact( const QString& handle );

    // Return the contact alias.
    const QString      	getContactAlternativeName( const QString &handle, FormattingMode mode )  const;
    // Return the contact's current media.
    const QString      	getContactCurrentMedia( const QString &handle )     const;
    // Return the path to the contact's picture.
    const QString      	getContactDisplayPicture( const QString &handle )   const;
    // Return the contact's friendly name.
    const QString      	getContactDisplayName( const QString &handle )     const;
    // Return the contacts' handle from the current chat.
    const QStringList   getContactsHandleInCurrentChat() const;
    // Return the list of user-defined groups where the contact can be found.
    const QStringList   getContactIsInGroups( const QString &handle )       const;
    // Return the contact's personal message.
    const QString      	getContactPersonalMessage( const QString &handle )  const;
    // Return the contact's current status.
    KMess::MsnStatus    getContactStatus( const QString &handle )           const;
    // Return the contact's true friendly name, regardless of extension.
    const QString       getContactTrueFriendlyName( const QString &handle, FormattingMode mode ) const;
    // Read when the last message from this contact has been received.
    const QDateTime&    getLastMessageDate( const QString &handle );
    // Read when the contact has been seen the last time.
    const QDateTime&    getLastSeen( const QString &handle );
    // Read the list of pictures which the contact has used.
    const QStringList&  getPictureList( const QString &handle ) const;
    // Return the name of this group (will automatically return a name for special groups).
    const QString       getGroupName( KMess::MsnGroup *msnGroup ) const;

    // Request the service to move a contact between two groups.
    void                moveContact( const KMess::MsnContact *contact, const KMess::MsnGroup *oldGroup, const KMess::MsnGroup *newGroup );
    // Request the service to rename a group.
    void                renameGroup( KMess::MsnGroup *group, QString &newName );

    // Change the contact alias.
    void               	setContactAlternativeName( const QString &handle, const QString &newAlternativeName );

    // Request the service to unblock a contact.
    void                unblockContact( const QString& handle );

  signals: // Public signals
    // Signal that the addition of a contact was successful.
    void                contactAdded( KMess::MsnContact *contact );
    // Signal that adding a contact to a group was successful.
    void                contactAddedToGroup( KMess::MsnContact *contact, KMess::MsnGroup *group );
    // Signal that a contact added you.
    void                contactAddedUser( KMess::MsnContact *contact );
    // Contact has gone offline.
    void                contactOffline( KMess::MsnContact *contact );
    // Contact has gone online.
    void                contactOnline( KMess::MsnContact *contact, bool initial = false );
    // Signal that the contact has changed his/her current media.
    void                contactChangedCurrentMedia();
    // Signal that the contact's picture path has changed.
    void                contactChangedDisplayPicture();
    // Signal that the contact may have changed it's personal message.
    void                contactChangedPersonalMessage();
    // Signal that the contact has changed status (could be online, offline, away, busy, etc).
    void                contactChangedStatus( KMess::MsnContact *contact, KMess::MsnStatus newStatus );

    // Signal that the addition of a group was successful.
    void                groupAdded( KMess::MsnGroup *group );
    // Signal that the contact may have moved to a different group.
    void                groupChanged();
    // Signal that the renaming of a contact was successful.
    void                groupRenamed( KMess::MsnGroup *group );
};

#endif
