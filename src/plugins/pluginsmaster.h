/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright 2009 Sjors Gielen <sjors@kmess.org>                       *
 * (C) Copyright 2010, 2011 Daniel E. Moctezuma <democtezuma@gmail.com     *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */


#ifndef PLUGINSMASTER_H
#define PLUGINSMASTER_H

//#include "ui_plugininfo.h"

#include "plugin.h"

#include <KPluginInfo>
#include <KActionCollection>
#include <KUrl>

#include <QListView>
#include <QModelIndex>
#include <QObject>
#include <QScriptValue>

class KAction;
class KJob;
class PluginsModel;
class QScriptEngine;

/**
  * This class handles all plugin stuff - it initialises Kross, loads plugins,
  * runs them, and handles any result. It's the one-stop go for the plugins.
  *
  * @author Sjors Gielen
  * @author Daniel E. Moctezuma
  * @ingroup Plugins
  */
class PluginsMaster : public QObject // , private Ui::PluginInfo
{
  Q_OBJECT

  friend class KMessTest;

  public: // Public methods
    // The constructor
                       PluginsMaster( QObject *parent = 0 );
    // The destructor
    virtual           ~PluginsMaster();

    // Return a list of the available plugins.
    const QList<Plugin*>&   getPluginList() const;
    // Return a list of the current running plugins.
    const QStringList       getRunningPluginList() const;

    // Load the plugins available.
    void               load();
    // Set the list view for the plugins.
    void               setListView( QListView* listView );
    // Set the plugin's model so the plugins can be displayed.
    void               setPluginsModel( PluginsModel* model );
    // Setup the run & stop actions.
    void               setupActions();

  public slots: // Public slots
    // Calls updateActions with the selectedPlugins as a parameter.
    void              dataChanged( const QModelIndex &from, const QModelIndex &to );
    // Calls updateActions with the selectedPlugins as a parameter.
    void              onSelectionChanged( const QItemSelection &selected, const QItemSelection &deselected );
    // Run the selected plugin in the plugin's model.
    void              runPlugin();
    // Shows the plugin's About dialog.
    void              showProperties( Plugin* plugin );
    // Stop the selected plugin in the plugin's model.
    void              stopPlugin();

  signals: // Public signals
    // Signal that the plugin's configure dialog is about to be shown.
    void              enableConfigure( bool on );
    // Signal that the plugin's properties are about to be shown.
    void              enableProperties( bool on );
    // Signal that the plugin was executed.
    void              enableRunPlugin( bool on );
    // Signal that the plugin was stopped.
    void              enableStopPlugin( bool on );

  private: // Private functions
    // Add a plugin from its desktop file and return a pointer to it.
    Plugin*           addPluginFromDesktopFile( const QString &dir, const QString &desktopFile );
    // Load the plugin's directory and return a pointer to it.
    Plugin*           loadPluginDir( const QString &dir );
    // Return a list of the selected plugins in the plugin's model.
    QModelIndexList   selectedPlugins();
    // Update the plugin's run & stop actions.
    void              updateActions( const QModelIndexList &selected );

  private: // Private attributes
    KActionCollection*           actionCollection_;
    // Run plugin action.
    KAction*                     runPlugin_;
    // Stop plugin action.
    KAction*                     stopPlugin_;
    // Pointer to the plugin's model.
    PluginsModel*                pluginsModel_;
    // List of the available plugins.
    QList<Plugin*>               plugins_;
    // List of the plugin's script file.
    QStringList                  pluginFilesList_;
    // Pointer to a QListView used by the PluginsModel class
    // to obtain the selected plugins list.
    QListView*                   listView_;
};

#endif
