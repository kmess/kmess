/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright 2010 Daniel E. Moctezuma <democtezuma@gmail.com>          *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

#ifndef KMESSCHAT_H
#define KMESSCHAT_H

#include "../chat/chat.h"
#include "../chat/winkswidget.h"

#include <QObject>

/**
  * @brief Chat Interface.
  *
  * This class is one of the plugin interface classes.
  * It provides access to the Chat, ChatMaster, ChatWindow and KMess::MsnChat classes.
  *
  * @author Daniel E. Moctezuma
  * @ingroup Plugins
  */
class KMessChat : public QObject
{
  Q_OBJECT

  public: // Public methods
    // The constructor
                        KMessChat( QObject* parent = 0 );

    // Return whether or not the contact is in this chat,
    // check also if the chat is exclusive with the contact.
    bool                isContactInChat( const QString &handle );
    // Return whether or not all contacts left the chat.
    bool                isEmpty( Chat *chat ) const;
    // Return whether or not only the given contact is in the chat.
    bool                isPrivateChatWith( const QString& handle, Chat *chat ) const;

    // Get all chats in all chat windows.
    QList<Chat*>        getActiveChats();
    // Return a list of the contacts in the chat.
    ContactsList        getContactsInChat( Chat *chat ) const;
    // Return the date and time the chat has started.
    QDateTime           getStartTime( const QString &handle, bool privateChat ) const;
    // Return the list of contacts which are typing.
    const QStringList   getTypingContacts( Chat *chat ) const;

  public slots:
    // Clear the chat window's message editor.
    void                clearMessageEditor();
    // Return the current chat.
    Chat*               getCurrentChat() const;
    // Return the text (in plain text) in the current chat.
    QString             getCurrentChatText();
    // Return the number of contacts in the current chat.
    int                 getNumberOfContactsInChat() const;

    // Invite a contact to the chat.
    void                inviteContacts( const QString &handle, const QStringList &contacts, bool privateChat );
    // Send a text message to the chat.
    void                sendChatMessage( const QString &handle, bool privateChat, const QString& message );
    // Send a text message to the current chat.
    void                sendChatMessageToCurrentChat( const QString &message );
    // Send an ink drawing to the chat.
    void                sendInkMessage( const QString &handle, bool privateChat, KMess::InkFormat format, const QByteArray &inkData );
    // Send an ink drawing to the current chat.
    void                sendInkMessageToCurrentChat( KMess::InkFormat format, const QByteArray &inkData );
    // Send a text message to the current chat.
    void                sendMessage( const QString& message );
    // Send a nudge.
    void                sendNudge( const QString &handle, bool privateChat );
    // Send a nudge to the current chat.
    void                sendNudgeToCurrentChat();
    // Send a wink to the chat.
    void                sendWink( const QString &handle, bool privateChat, KMess::WinkMessage& message );
    // Send a wink to the current chat.
    void                sendWinkToCurrentChat( KMess::WinkMessage& message );

  signals: // Public signals
    // Signal that this chat is about to close.
    void                closing( Chat *chat );
    // Signal that a contact joined the conversation.
    void                contactJoined( KMess::MsnContact* contact );
    // Signal that a contact has left the conversation.
    void                contactLeft( KMess::MsnContact *contact, bool isChatIdle );
    // Signal that a contact is now typing.
    void                contactTyping( KMess::MsnContact* contact );
    // Signal the presence of a new incoming chat message.
    void                gotChatMessage( KMess::TextMessage &message, Chat *chat );
    // Signal that a nudge has been received.
    void                gotNudge();
    // Signal that a typing message has been received.
    void                gotTypingMessage( Chat *chat );
    // Signal that a message is about to be sent.
    void                messageAboutToSend( QString message );
    // Signal that a message was received from a remote end.
    void                messageReceived( KMess::Message );
    // Signal that the ChatMaster should start a file transfer.
    void                requestFileTransfer (const QString &handle, const QString &filename );
    // Signal that the sendButton_ button on a ChatWindow was clicked.
    void                sendButtonClicked();
    // Signal that a message could not be sent.
    void                sendingFailed( KMess::Message message, const QString& recipient );
    // Signal that a message was sent.
    void                sendingSucceeded( KMess::Message message );
    // Signal that the user is typing.
    void                userIsTyping();

  private slots:
    // Connect the signals required for a new chat when is created.
    void                newChatCreated( const KMess::MsnChat* msnchat, const Chat* chat, const ChatWindow* chatWindow );
};

#endif
