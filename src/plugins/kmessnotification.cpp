/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright 2010 Daniel E. Moctezuma <democtezuma@gmail.com>          *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

#include "kmessnotification.h"

#include "../kmessglobal.h"

#include "../notification/chatnotification.h"
#include "../notification/contactstatusnotification.h"
#include "../notification/newemailnotification.h"
#include "../notification/statusnotification.h"

KMessNotification::KMessNotification( QObject *parent )
: QObject( parent )
{
  setObjectName( "KMessNotification" );

  // Connect through NotificationManager's signals to our signals.
  connect( NotificationManager::instance()->chatNotification(),          SIGNAL( raiseChat( Chat*,bool ) ),
           this,                                                         SIGNAL( raiseChat( Chat*,bool) ) );
  connect( NotificationManager::instance()->contactStatusNotification(), SIGNAL( startChat( QString ) ),
           this,                                                         SIGNAL( startChat( QString ) ) );

  // Connect the MsnSession's signals with our slots.
  connect( globalSession, SIGNAL( statusEvent( KMess::StatusMessageType, KMess::StatusMessage, QVariant ) ),
           this,            SLOT( notifyApplicationStatusEvent( KMess::StatusMessageType, KMess::StatusMessage, QVariant ) ) );

  connect( globalSession, SIGNAL( chatMessageReceived( KMess::Message ) ),
           this,            SLOT( notifyChat( KMess::Message ) ) );

  connect( globalSession, SIGNAL( contactChangedStatus( KMess::MsnContact*, KMess::MsnStatus ) ),
          this,             SLOT( notifyContactStatus( KMess::MsnContact* ) ) );
  connect( globalSession, SIGNAL( contactOffline( KMess::MsnContact* ) ),
           this,            SLOT( notifyContactStatus( KMess::MsnContact* ) ) );
  connect( globalSession, SIGNAL( contactOnline( KMess::MsnContact*, bool ) ),
           this,            SLOT( notifyContactStatus( KMess::MsnContact*, bool ) ) );

  connect( globalSession, SIGNAL( newEmail( QString, QString, bool, QString, QString, QString ) ),
           this,            SLOT( notifyNewEmailHasArrived( QString, QString, bool, QString, QString, QString ) ) );
}



/**
  * @brief Notify the user about this event (a status event has occurred).
  *
  * @param type       The type of message to be notified (i.e. TYPE_ERROR, TYPE_PROGRESS, etc.).
  * @param message    The status message (i.e. KMess::MESSAGE_ERROR_INTERNAL).
  * @param extraInfo  Extra info if is needed.
  */
void KMessNotification::notifyApplicationStatusEvent( KMess::StatusMessageType type, KMess::StatusMessage message, const QVariant &extraInfo )
{
  NotificationManager::instance()->statusNotification()->notify( type, message, extraInfo );
}



/**
  * @brief Notify the user about this event (a contact has contacted the user).
  *
  * @param message  The received message.
  */
void KMessNotification::notifyChat( KMess::Message message )
{
  NotificationManager::instance()->chatNotification()->notify( message );
}



/**
  * @brief Notify the user about this event (a contact has changed its status).
  *
  * @param contact          The contact that changed its status.
  * @param isInitialStatus  Whether or not is this the initial status of the contact.
  */
void KMessNotification::notifyContactStatus( KMess::MsnContact *contact, bool isInitialStatus )
{
  NotificationManager::instance()->contactStatusNotification()->notify( contact, isInitialStatus );
}



/**
  * @brief Notify the user about this event (a new email has arrived).
  *
  * @param sender   The contact that sent the email.
  * @param subject  The email subject.
  * @param inInbox  Whether or not the arrived email is in the user's inbox.
  * @param command  (unused).
  * @param folder   The Message URL.
  * @param url      The url for the MSN Hotmail/Live web site (unused).
  */
void KMessNotification::notifyNewEmailHasArrived( QString sender, QString subject, bool inInbox, QString command, QString folder, QString url )
{
  NotificationManager::instance()->newEmailNotification()->notify( sender, subject, inInbox, command, folder, url );
}



#include "kmessnotification.moc"
