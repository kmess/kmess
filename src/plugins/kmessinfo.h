/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright 2009 Sjors Gielen <sjors@kmess.org>                       *
 * (C) Copyright 2010 Daniel E. Moctezuma <democtezuma@gmail.com>          *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

#ifndef KMESSINFO_H
#define KMESSINFO_H

#include <QObject>

/**
  * This class is one of the plugin interface classes: it provides access
  * to static information about KMess, such as its version, its KDE version,
  * and its Qt version.
  *
  * @author Sjors Gielen
  * @author Daniel E. Moctezuma
  * @ingroup Plugins
  */
class KMessInfo : public QObject
{
  Q_OBJECT

  Q_PROPERTY( QString kmessVersion       READ getKmessVersion      )
  Q_PROPERTY( QString kdeVersion         READ getKdeVersion        )
  Q_PROPERTY( QString qtVersion          READ getQtVersion         )
  Q_PROPERTY( QString kdeCompileVersion  READ getKdeCompileVersion )
  Q_PROPERTY( QString qtCompileVersion   READ getQtCompileVersion  )

  public: // Public methods
    // The constructor
                       KMessInfo( QObject *parent = 0 );
    // getters
    const QString&     getKmessVersion()      const;

    const QString&     getKdeVersion()        const;
    const QString&     getQtVersion()         const;

    const QString&     getKdeCompileVersion() const;
    const QString&     getQtCompileVersion()  const;

  public slots:
    const QString      getKmessName()         const;

  private: // Private attributes
    QString            kmessName;
    QString            kmessVersion;
    QString            kdeVersion;
    QString            qtVersion;
    QString            kdeCompileVersion;
    QString            qtCompileVersion;
};

#endif
