/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright 2009 Sjors Gielen <sjors@kmess.org>                       *
 * (C) Copyright 2010 Daniel E. Moctezuma <democtezuma@gmail.com>          *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

#include "session.h"

#include "../account.h"
#include "../kmessglobal.h"
#include "../kmessapplication.h"

// The constructor
Session::Session( QObject *parent )
: QObject( parent )
{
  setObjectName( "Session" );

  // Connect through MsnSession's signals to our signals
  connect( globalSession->self(), SIGNAL( displayNameChanged()    ),
           this,                  SIGNAL( displayNameChanged()    ) );
  connect( globalSession->self(), SIGNAL( personalMessageChanged() ),
           this,                  SIGNAL( personalMessageChanged() ) );
  connect( globalSession->self(), SIGNAL( displayPictureChanged()  ),
           this,                  SIGNAL( displayPictureChanged()  ) );
  connect( globalSession, SIGNAL( changedStatus()   ),
           this,          SIGNAL( changedStatus()   ) );
  connect( globalSession, SIGNAL( changedNoEmails() ),
           this,          SIGNAL( changedNoEmails() ) );
  connect( globalSession, SIGNAL( newEmail( QString, QString, bool, QString, QString, QString ) ),
           this,          SIGNAL( newEmail( QString, QString, bool, QString, QString, QString ) ) );


  // Also connect to our private slots, because we want to update
  // our internal cache
  connect( globalSession->self(), SIGNAL(     displayNameChanged() ),
           this,                    SLOT(    updateInternalCache() ) );
  connect( globalSession->self(), SIGNAL( personalMessageChanged() ),
           this,                    SLOT(    updateInternalCache() ) );
  connect( globalSession->self(), SIGNAL(  displayPictureChanged() ),
           this,                    SLOT(    updateInternalCache() ) );

  connect( globalSession, SIGNAL(            loggedIn() ),
           this,            SLOT( updateInternalCache() ) );
  connect( globalSession, SIGNAL( loggedOut( KMess::DisconnectReason ) ),
           this,            SLOT( updateInternalCache() ) );

  // TODO: We *must* update our internal cache *before* the plugins
  // manager starts running scripts. Therefore, we may need the
  // plugins manager to update our cache, or so.

  // Start out with the right values (even though, at this point,
  // we're most probably logged out - so the initial values will be
  // incorrect, but the loggedIn() connection above will fix that)
  updateInternalCache();
}



/**
  * @brief Returns the current path to the display picture.
  */
const QString Session::getDisplayPicture() const
{
  return displayPicture_;
}



/**
   * @brief Returns the current friendly name in raw format.
   */
const QString Session::getDisplayName() const
{
  return displayName_;
}



/**
  * @brief Returns the current handle.
  */
const QString Session::getHandle() const
{
  return handle_;
}



/**
  * @brief Returns the current personal messagae in raw format.
  */
const QString Session::getPersonalMessage() const
{
  return personalMessage_;
}



/**
  * @brief Return the user's status.
  */
const QString Session::getStatus() const
{
  switch( status_ )
  {
    case KMess::OnlineStatus:     return QString( "online"    );
    case KMess::AwayStatus:       return QString( "away"      );
    case KMess::BrbStatus:        return QString( "brb"       );
    case KMess::BusyStatus:       return QString( "busy"      );
    case KMess::InvisibleStatus:  return QString( "invisible" );
    case KMess::IdleStatus:       return QString( "idle"      );
    case KMess::OfflineStatus:    return QString( "offline"   );
    case KMess::PhoneStatus:      return QString( "phone"     );
    case KMess::LunchStatus:      return QString( "lunch"     );

    default:                      return QString( "online"    );
  }
}



/**
  * @brief Return whether the notification connection is currently connected.
  */
 bool Session::isNotificationConnected() const
 {
   return globalSession->isNotificationConnected();
 }



// Setters
// These don't actually *set*, but they ask MsnSession to set, and if
// that works well that should lead to updateInternalCache() updating the
// right values in this class, above.
// We don't make this a special case by changing our own values here.
/**
  * @brief Request the service a change to the display name of the user.
  *
  * @param displayName  The display name to be set.
  */
void Session::setDisplayName( const QString displayName )
{
  globalSession->self()->setDisplayName( displayName );
}



/**
  * Special setter for the display picture. It checks if the destination
  * path is valid and and actually points to an image.
  *
  * @see KMess::MsnSession::validDisplayPicturePath()
  *
  * @param path  The path to the display picture to be set.
  */
 bool Session::setDisplayPicture( const QString path )
{
  globalSession->self()->setDisplayPicture( path );

  return true;
}



/**
  * @brief Set the personal message.
  *
  * @param personalMessage  The personal message to be set.
  */
void Session::setPersonalMessage( const QString personalMessage )
{
  globalSession->self()->setPersonalMessage( personalMessage );
}



/**
  * @brief Request the service a change to the status of the user
  *
  * @param newStatus  The status to be set.
  */
void Session::setStatus( QString newStatus )
{
  if( newStatus.toLower() == "online" )
  {
    globalSession->self()->changeStatus( KMess::OnlineStatus );
  }
  else if( newStatus.toLower() == "away" )
  {
    globalSession->self()->changeStatus( KMess::AwayStatus );
  }
  else if( newStatus.toLower() == "brb" )
  {
    globalSession->self()->changeStatus( KMess::BrbStatus );
  }
  else if( newStatus.toLower() == "busy" )
  {
    globalSession->self()->changeStatus( KMess::BusyStatus );
  }
  else if( newStatus.toLower() == "invisible" )
  {
    globalSession->self()->changeStatus( KMess::InvisibleStatus );
  }
  else if( newStatus.toLower() == "idle" )
  {
    globalSession->self()->changeStatus( KMess::IdleStatus );
  }
  else if( newStatus.toLower() == "offline" )
  {
    globalSession->self()->changeStatus( KMess::OfflineStatus );
  }
  else if( newStatus.toLower() == "phone" )
  {
    globalSession->self()->changeStatus( KMess::PhoneStatus );
  }
  else if( newStatus.toLower() == "lunch" )
  {
    globalSession->self()->changeStatus( KMess::LunchStatus );
  }
}



/**
 * @brief Update our internal cache (re-request display name, personal message,
 *        handle and display picture from the session).
 */
void Session::updateInternalCache()
{
  displayName_     = globalSession->self()->displayName();
  personalMessage_ = globalSession->self()->personalMessage();
  handle_          = globalSession->self()->handle();
  status_          = globalSession->self()->status();

  if( Account::connectedAccount )
  {
    displayPicture_  = Account::connectedAccount->getDisplayPicture();
  }
}



#include "session.moc"
