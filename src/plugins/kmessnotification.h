/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright 2010 Daniel E. Moctezuma <democtezuma@gmail.com>          *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

#ifndef KMESSNOTIFICATION_H
#define KMESSNOTIFICATION_H

#include "../chat/chat.h"
#include "../notification/notificationmanager.h"

#include <QObject>

/**
  * This class is one of the plugin interface classes. It provides access
  * to the Notification classes.
  *
  * @author Daniel E. Moctezuma
  * @ingroup Plugins
  */
class KMessNotification : public QObject
{
  Q_OBJECT

  public:
    // The Constructor
                        KMessNotification( QObject *parent = 0 );

  signals: // Public signals
    // Raise an existing chat which received a message.
    void                raiseChat( Chat *chatWindow, bool force ); // from chatnotification
    // Start a chat with a contact.
    void                startChat( const QString &contactHandle ); // from contactstatusnotification

  public slots:
    // Notify the user that a status event has occurred.
    void                notifyApplicationStatusEvent( const KMess::StatusMessageType type, const KMess::StatusMessage message, const QVariant &extraInfo );
    // Notify the user that contact has contacted the user.
    void                notifyChat( KMess::Message message );
    // Notify the user that a contact has changed its status.
    void                notifyContactStatus( KMess::MsnContact *contact, bool isInitialStatus = false );
    // Notify the user that a new email has arrived.
    void                notifyNewEmailHasArrived( QString sender, QString subject, bool inInbox, QString command, QString folder, QString url );
};

#endif
