/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright 2009 Sjors Gielen <sjors@kmess.org>                       *
 * (C) Copyright 2010, 2011 Daniel E. Moctezuma <democtezuma@gmail.com>    *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */


#include "pluginsmaster.h"

#include "../settings/pluginsmodel.h"

#include "../account.h"

#include "../kmessdebug.h"
#include "../kmessapplication.h"

#include <KAboutApplicationDialog>
#include <KAction>
#include <kdeversion.h>
#include <KDialog>
#include <KLocale>
#include <KStandardDirs>

#include <Kross/Action>
#include <Kross/ActionCollection>
#include <Kross/Manager>

#include <QFileInfo>
#include <QScriptEngine>
#include <QStringList>

// The constructor
PluginsMaster::PluginsMaster( QObject *parent )
: QObject( parent )
{
  setObjectName( "PluginsMaster" );
}



// The destructor
PluginsMaster::~PluginsMaster()
{
#ifdef KMESSDEBUG_PLUGINSMASTER
  kmDebug() << " DESTROYED.";
#endif
}



/**
  * @brief Add a plugin from its desktop file and return a pointer to it.
  *
  * @param dir          the plugin's directory.
  * @param desktopFile  the plugin's desktop file.
  */
Plugin* PluginsMaster::addPluginFromDesktopFile( const QString &dir, const QString &desktopFile )
{
  Plugin* plugin = new Plugin( this );

  if( ! plugin->loadDesktopFile( dir, desktopFile ) )
  {
    delete plugin;
    return 0;
  }

  // Delete duplicates
  if( ! plugins_.isEmpty() )
  {
    foreach( Plugin* plug, plugins_ )
    {
      if( plugin->file() == plug->file() )
      {
        delete plugin;
        return 0;
      }
    }
  }

  plugin->setPluginDirectory( dir );
  plugins_.append( plugin );
  pluginsModel_->insertRow( plugins_.count() - 1 );

  return plugin;
}



/**
  * @brief Calls updateActions with the selectedPlugins as a parameter.
  *
  * @param from  Start of selection (unused).
  * @param to    End of selection (unused).
  */
void PluginsMaster::dataChanged( const QModelIndex &from, const QModelIndex &to )
{
    Q_UNUSED( from );
    Q_UNUSED( to );

    updateActions( selectedPlugins() );
}



/**
  * @brief Return a list of the available plugins.
  */
const QList<Plugin*>& PluginsMaster::getPluginList() const
{
  return plugins_;
}



/**
  * @brief Return a list of the current running plugins.
  */
const QStringList PluginsMaster::getRunningPluginList() const
{
  if( plugins_.isEmpty() )
  {
    return QStringList();
  }

  QStringList pluginList;
  foreach( Plugin* plugin, plugins_ )
  {
    if( plugin->isRunning() )
    {
      pluginList.append( plugin->getKPluginInfo().name() );
    }
  }

  return pluginList;
}



/**
  * @brief Load the plugins available.
  */
void PluginsMaster::load()
{
  QStringList dirList = KGlobal::dirs()->findDirs( "data", KMESS_NAME "/plugins" );
  foreach( const QString &dir, dirList )
  {
    QDir d( dir );
    QStringList subdirs = d.entryList( QDir::Dirs );
    foreach( const QString &sdir, subdirs )
    {
      if ( sdir != ".." && sdir != "." )
      {
        QString absolutePath = d.absoluteFilePath( sdir );

        if( loadPluginDir( absolutePath ) )
        {
#ifdef KMESSDEBUG_PLUGINSMASTER
          kmDebug() << "plugin has configure file?: " << plugin->hasConfigure();
          kmDebug() << "has main file?: " << plugin->file();
          kmDebug() << "has a valid desktop file?: " << plugin->getKPluginInfo().isValid();
          kmDebug() << "plugins_ size: " << plugins_.size();
#endif
        }
        else
        {
          kmDebug() << "plugin IS null!";
        }
      }
    }
  }
}



/**
  * @brief Load the plugin's directory and return a pointer to it.
  */
Plugin* PluginsMaster::loadPluginDir( const QString &dir )
{
  QDir d( dir );
  QStringList desktopFiles = d.entryList( QDir::Files );
  QString dirPath = dir;
  if( ! dirPath.endsWith( "/" ) )
  {
    dirPath.append( "/" );
  }

  // Look for desktop files.
  foreach( const QString &file, desktopFiles )
  {
    if( file.endsWith( ".desktop" ) )
    {
      return addPluginFromDesktopFile( dirPath, file );
    }
  }

  return 0;
}



/**
  * @brief Calls updateActions with the selectedPlugins as a parameter.
  */
void PluginsMaster::onSelectionChanged( const QItemSelection &selected, const QItemSelection &deselected )
{
  Q_UNUSED( deselected );
  Q_UNUSED( selected );

  updateActions( selectedPlugins() );
}



/**
  * @brief Run the selected plugin in the plugin's model.
  */
void PluginsMaster::runPlugin()
{
  QModelIndexList sel = selectedPlugins();
  foreach( const QModelIndex &index, sel )
  {
     if ( ! pluginsModel_->setData( index, Qt::Checked, Qt::CheckStateRole ) )
     {
#ifdef KMESSDEBUG_PLUGINSMASTER
       kmDebug() << "setData failed";
#endif
     }
  }

  updateActions( sel );
}



/**
  * @brief Return a list of the selected plugins in the plugin's model.
  */
QModelIndexList PluginsMaster::selectedPlugins()
{
  return listView_->selectionModel()->selectedRows();
}



/**
  * @brief Set the list view for the plugins.
  */
void PluginsMaster::setListView( QListView* listView )
{
  listView_ = listView;
}



/**
  * @brief Set the plugin's model so the plugins can be displayed.
  */
void PluginsMaster::setPluginsModel( PluginsModel* model )
{
  pluginsModel_ = model;
}



/**
  * @brief Setup the run & stop actions.
  */
void PluginsMaster::setupActions()
{
  actionCollection_ = new KActionCollection( this );

  runPlugin_  = new KAction(  "run", this );
  stopPlugin_ = new KAction( "stop", this );

  actionCollection_->addAction(  "run", runPlugin_  );
  actionCollection_->addAction( "stop", stopPlugin_ );

  connect( this,        SIGNAL( enableRunPlugin( bool ) ),
           runPlugin_,    SLOT( setChecked(bool)) );
  connect( this,        SIGNAL( enableStopPlugin( bool ) ),
           stopPlugin_,   SLOT( setChecked(bool)) );

  connect( runPlugin_,  SIGNAL( triggered() ),
           this,          SLOT( runPlugin() ) );
  connect( stopPlugin_, SIGNAL( triggered() ),
           this,          SLOT( stopPlugin() ) );
}



/**
  * @brief Shows the plugin's About dialog.
  */
void PluginsMaster::showProperties( Plugin* plugin )
{
  KPluginInfo info = plugin->getKPluginInfo();

  KAboutData aboutData( info.pluginName().toUtf8()
                      , info.pluginName().toUtf8()
                      , ki18n( info.name().toUtf8() )
                      , info.version().toUtf8()
                      , ki18n( info.comment().toUtf8() )
                      , KAboutLicense::byKeyword( info.license() ).key()
                      , ki18n( QByteArray() )
                      , ki18n( QByteArray() )
                      , info.website().toLatin1() );
  aboutData.setProgramIconName( info.icon() );

  const QStringList authors = info.author().split( ',' );
  const QStringList emails = info.email().split( ',' );
  if( authors.count() == emails.count() )
  {
    int i = 0;
    foreach( const QString &author, authors )
    {
      if( ! author.isEmpty() )
      {
        aboutData.addAuthor( ki18n( author.toUtf8() ), ki18n( QByteArray() ), emails[ i ].toUtf8() );
      }

      i++;
    }
  }

  KAboutApplicationDialog aboutDialog( &aboutData, listView_ );
  aboutDialog.setPlainCaption( i18nc( "Used only for plugins"
                                    , "About %1"
                                    , aboutData.programName() ) );
  aboutDialog.exec();
}



/**
  * @brief Stop the selected plugin in the plugin's model.
  */
void PluginsMaster::stopPlugin()
{
  QModelIndexList sel = selectedPlugins();
  foreach( const QModelIndex & idx, sel )
  {
    if ( ! pluginsModel_->setData( idx, Qt::Unchecked, Qt::CheckStateRole ) )
    {
#ifdef KMESSDEBUG_PLUGINSMASTER
      kmDebug() << "setData failed" << endl;
#endif
    }
  }
  updateActions( sel );
}



/**
  * @brief Update the plugin's run & stop actions.
  */
void PluginsMaster::updateActions( const QModelIndexList &selected )
{
  int num_running = 0;
  int num_not_running = 0;
  foreach( const QModelIndex &index, selected )
  {
    Plugin* plugin = pluginsModel_->pluginForIndex( index );
    if( plugin )
    {
      if( plugin->isRunning() )
      {
        num_running++;
      }
      else
      {
        num_not_running++;
      }
    }
    else
    {
      num_not_running++;
    }
  }

  emit enableRunPlugin( selected.count() > 0 && num_not_running > 0 );
  emit enableStopPlugin( selected.count() > 0 && num_running > 0 );

  Plugin* plugin = 0;
  if ( selected.count() > 0 )
  {
    plugin = pluginsModel_->pluginForIndex( selected.front() );
  }
  emit enableProperties( selected.count() == 1 && plugin );
  emit enableConfigure( selected.count() == 1 && plugin && plugin->hasConfigure() );
}



#include "pluginsmaster.moc"
