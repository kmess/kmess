/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright 2010 Daniel E. Moctezuma <democtezuma@gmail.com>          *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

#include "kmesschat.h"

#include "../kmessdebug.h"

#include "../kmessapplication.h"
#include "../chat/chatmaster.h"
#include "../chat/chatwindow.h"

#include <KMess/MsnChat>
#include <KMess/MsnContact>

// The constructor
KMessChat::KMessChat( QObject* parent )
: QObject( parent )
{
  setObjectName( "KMessChat" );

  // Connect the signals to all the current opened chats
  QList<Chat*> chats = KMessApplication::instance()->chatMaster()->getAllChats();
  foreach( Chat* chat, chats )
  {
    newChatCreated( chat->getMsnChat(), chat, chat->getChatWindow() );
  }

  // Connect the signals when a new chat is created
  connect( KMessApplication::instance()->chatMaster(), SIGNAL( newChatCreated( const KMess::MsnChat*, const Chat*, const ChatWindow* ) ),
           this,                                         SLOT( newChatCreated( const KMess::MsnChat*, const Chat*, const ChatWindow* ) ) );
}



/**
  * @brief Clear the chat window's message editor.
  */
void KMessChat::clearMessageEditor()
{
  KTextEdit *messageEditor = getCurrentChat()->getChatWindow()->getMessageEditor();

  messageEditor->clear();
  messageEditor->setFocus();
}



/**
  * @brief Get all chats in all chat windows.
  */
QList<Chat*> KMessChat::getActiveChats()
{
  return KMessApplication::instance()->chatMaster()->getAllChats();
}



/**
  * @brief Return a list of the contacts in the chat.
  *
  * @param chat  The chat to be checked for contacts in it.
  */
ContactsList KMessChat::getContactsInChat( Chat *chat ) const
{
  return KMessApplication::instance()->chatMaster()->getChatWindow( chat )->getCurrentChat()->getMsnChat()->participants();
}



/**
  * @brief Return the current chat.
  */
Chat* KMessChat::getCurrentChat() const
{
  QList<Chat*> chats = KMessApplication::instance()->chatMaster()->getAllChats();

  foreach( Chat *chat, chats )
  {
    if( chat->isCurrentChat() )
    {
      return chat;
    }
  }

  return 0;
}



QString KMessChat::getCurrentChatText()
{
  return getCurrentChat()->getChatWindow()->getMessageEditor()->toPlainText();
}



/**
  * @brief Return the number of contacts in the current chat.
  */
int KMessChat::getNumberOfContactsInChat() const
{
  return getCurrentChat()->getMsnChat()->participants().size();
}



/**
  * @brief Return the date and time the chat has started.
  *
  * @param handle       The contact's handle.
  * @param privateChat  Whether or not is an exclusive chat with the contact.
  */
QDateTime KMessChat::getStartTime( const QString &handle, bool privateChat ) const
{
  return KMessApplication::instance()->chatMaster()->getContactsChat( handle, privateChat )->getStartTime();
}



/**
  * @brief Return the list of contacts which are typing.
  *
  * @param chat  The chat to be checked for typing contacts.
  */
const QStringList KMessChat::getTypingContacts( Chat *chat ) const
{
  return KMessApplication::instance()->chatMaster()->getChatWindow( chat )->getCurrentChat()->getTypingContacts();
}



/**
  * @brief Invite a contact to the chat.
  *
  * @param handle       The contact's handle.
  * @param contacts     List of contacts to invite.
  * @param privateChat  Whether or not is an exclusive chat with the contact.
  */
void KMessChat::inviteContacts( const QString &handle, const QStringList &contacts, bool privateChat )
{
  KMessApplication::instance()->chatMaster()->getContactsChat( handle, privateChat )->inviteContacts( contacts );
}



/**
  * @brief Return whether or not the contact is in this chat,
  *        check also if the chat is exclusive with the contact.
  *
  * @param handle  The contact's handle.
  */
bool KMessChat::isContactInChat( const QString& handle )
{
  return KMessApplication::instance()->chatMaster()->isContactInChat( handle );
}



/**
  * @brief Return whether or not all contacts left the chat.
  *
  * @param chat  The chat to be checked.
  */
bool KMessChat::isEmpty( Chat *chat ) const
{
  return KMessApplication::instance()->chatMaster()->getChatWindow( chat )->getCurrentChat()->getMsnChat()->isEmpty();
}



/**
  * @brief Return whether or not only the given contact is in the chat.
  *
  * @param handle  The contact's handle.
  * @param chat    The chat to be checked.
  */
bool KMessChat::isPrivateChatWith( const QString &handle, Chat *chat ) const
{
  return KMessApplication::instance()->chatMaster()->getChatWindow( chat )->getCurrentChat()->getMsnChat()->isPrivateChatWith( handle );
}



/**
  * @brief Connect the signals required for a new chat when is created.
  */
void KMessChat::newChatCreated( const KMess::MsnChat* msnchat, const Chat* chat, const ChatWindow* chatWindow )
{
  connect( chat,       SIGNAL( closing( Chat* ) ),
           this,       SIGNAL( closing( Chat* ) ) );
  connect( msnchat,    SIGNAL( contactJoined(KMess::MsnContact*)),
           this,       SIGNAL( contactJoined( KMess::MsnContact* ) ) );
  connect( msnchat,    SIGNAL( contactLeft( KMess::MsnContact*, bool ) ),
           this,       SIGNAL( contactLeft( KMess::MsnContact*, bool ) ) );
  connect( msnchat,    SIGNAL( contactTyping( KMess::MsnContact* ) ),
           this,       SIGNAL( contactTyping( KMess::MsnContact* ) ) );
  connect( chat,       SIGNAL( gotChatMessage( KMess::TextMessage&, Chat* ) ),
           this,       SIGNAL( gotChatMessage( KMess::TextMessage&, Chat* ) ) );
  connect( chat,       SIGNAL( gotNudge() ),
           this,       SIGNAL( gotNudge() ) );
  connect( chat,       SIGNAL( gotTypingMessage( Chat* ) ),
           this,       SIGNAL( gotTypingMessage( Chat* ) ) );
  connect( chatWindow, SIGNAL( messageAboutToSend( QString ) ),
           this,       SIGNAL( messageAboutToSend( QString ) ) );
  connect( msnchat,    SIGNAL( messageReceived( KMess::Message ) ),
           this,       SIGNAL( messageReceived( KMess::Message ) ) );
  connect( chat,       SIGNAL( requestFileTransfer( QString, QString ) ),
           this,       SIGNAL( requestFileTransfer( QString, QString ) ) );
  connect( msnchat,    SIGNAL( sendingFailed( KMess::Message, QString ) ),
           this,       SIGNAL( sendingFailed( KMess::Message, QString ) ) );
  connect( msnchat,    SIGNAL( sendingSucceeded( KMess::Message ) ),
           this,       SIGNAL( sendingSucceeded( KMess::Message ) ) );
  connect( chatWindow, SIGNAL( userIsTyping() ),
           this,       SIGNAL( userIsTyping() ) );
}



/**
  * @brief Send a text message to the chat.
  *
  * @param handle       The contact's handle.
  * @param privateChat  Whether or not is an exclusive chat with the contact.
  * @param message      The Message to be sent.
  */
void KMessChat::sendChatMessage( const QString &handle, bool privateChat, const QString &message )
{
  KMessApplication::instance()->chatMaster()->getContactsChat( handle, privateChat)->sendChatMessage( message );
}



/**
  * @brief Send a text message to the current chat.
  *
  * @param message  The Message to be sent.
  */
void KMessChat::sendChatMessageToCurrentChat( const QString &message )
{
  this->getCurrentChat()->sendChatMessage( message );
}



/**
  * @brief Send an ink drawing to the chat.
  *
  * @param handle       The contact's handle.
  * @param privateChat  Whether or not is an exclusive chat with the contact.
  * @param format       The format of the data of this InkMessage (FORMAT_ISF or FORMAT_GIF).
  * @param inkData      Raw data describing the ink, can be either a fortified-GIF or ISF drawing.
  */
void KMessChat::sendInkMessage( const QString &handle, bool privateChat, KMess::InkFormat format, const QByteArray &inkData )
{
  KMessApplication::instance()->chatMaster()->getContactsChat( handle, privateChat )->sendInkMessage( format, inkData );
}



/**
  * @brief Send an ink drawing to the current chat.
  *
  * @param format   The format of the data of this InkMessage (FORMAT_ISF or FORMAT_GIF).
  * @param inkData  Raw data describing the ink, can be either a fortified-GIF or ISF drawing.
  */
void KMessChat::sendInkMessageToCurrentChat( KMess::InkFormat format, const QByteArray& inkData )
{
  this->getCurrentChat()->sendInkMessage( format, inkData );
}



/**
  * @brief Send a text message to the current chat.
  */
void KMessChat::sendMessage( const QString& message )
{
  getCurrentChat()->getChatWindow()->getMessageEditor()->setText( message );
}



/**
  * @brief Send a nudge.
  *
  * @param handle       The contact's handle.
  * @param privateChat  Whether or not is an exclusive chat with the contact.
  */
void KMessChat::sendNudge( const QString &handle, bool privateChat )
{
  KMessApplication::instance()->chatMaster()->getContactsChat( handle, privateChat )->slotSendNudge();
}



/**
  * @brief Send a nudge to the current chat.
  */
void KMessChat::sendNudgeToCurrentChat()
{
  this->getCurrentChat()->slotSendNudge();
}



/**
  * @brief Send a wink to the chat.
  *
  * @param handle       The contact's handle.
  * @param privateChat  Whether or not is an exclusive chat with the contact.
  * @param winkMessage  Wink message.
  */
void KMessChat::sendWink( const QString &handle, bool privateChat, KMess::WinkMessage &winkMessage )
{
  KMessApplication::instance()->chatMaster()->getContactsChat( handle, privateChat )->getMsnChat()->sendWink( winkMessage );
}



/**
  * @brief Send a wink to the current chat.
  *
  * @param winkMessage  Wink message.
  */
void KMessChat::sendWinkToCurrentChat( KMess::WinkMessage& message )
{
  this->getCurrentChat()->getMsnChat()->sendWink( message );
}



#include "kmesschat.moc"
