/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright 2009 Sjors Gielen <sjors@kmess.org>                       *
 * (C) Copyright 2010 Daniel E. Moctezuma <democtezuma@gmail.com>          *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

#ifndef SESSION_H
#define SESSION_H

#include "../contact/status.h"

#include <QObject>

/**
  * @brief MsnSession Interface.
  *
  * This class is one of the plugin interface classes.
  * It provides access to the global KMess::MsnSession object.
  *
  * @author Sjors Gielen
  * @author Daniel E. Moctezuma
  * @ingroup Plugins
  */
class Session : public QObject
{
  Q_OBJECT

  // Read-write properties
  Q_PROPERTY( QString displayName_        READ  getDisplayName
                                          WRITE setDisplayName     )
  Q_PROPERTY( QString personalMessage_    READ  getPersonalMessage
                                          WRITE setPersonalMessage )
  Q_PROPERTY( QString status_             READ  getStatus
                                          WRITE setStatus          )
  // Read-only properties
  Q_PROPERTY( QString handle_             READ  getHandle          )
  Q_PROPERTY( QString displayPicture_     READ  getDisplayPicture  )

  public: // Public methods
    // The constructor
                       Session( QObject *parent = 0 );
  public slots:
    // Returns the current display name in raw format.
    const QString      getDisplayName()    const;
    // Returns the current path to the display picture.
    const QString      getDisplayPicture()  const;
    // Returns the current handle.
    const QString      getHandle()          const;
    // Returns the current personal messagae in raw format.
    const QString      getPersonalMessage() const;
    // Return the user's status.
    const QString      getStatus()          const;

    // Return whether the notification connection is currently connected.
    bool               isNotificationConnected() const;

    // Request the service a change to the display name of the user.
    void               setDisplayName( const QString friendlyName );
    // Set the display picture but does additional checking.
    bool               setDisplayPicture( const QString path );
    // Set the personal message.
    void               setPersonalMessage( const QString personalMessage );
    // Request the service a change to the status of the user.
    void               setStatus( QString newStatus );

  private: // Private attributes
    // The display name of the user.
    QString            displayName_;
    // The personal message of the user.
    QString            personalMessage_;
    // The status of the user.
    KMess::MsnStatus            status_;
    // The handle of the user.
    QString            handle_;
    // The display picture of the user.
    QString            displayPicture_;

  private slots:
    // Update our internal cache (re-request friendly name, personal message,
    // handle and display picture from the session).
    void               updateInternalCache();

  signals: // Public signals
    // Signal that the user's current media has changed.
    void               changeCurrentMedia();
    // Signal that the number of emails has changed.
    void               changedNoEmails();
    // Signal that the user's status has changed.
    void               changedStatus();
    // Signal that the user's friendly name has changed.
    void               displayNameChanged();
    // Signal that the user's display picture has changed.
    void               displayPictureChanged();
    // Signal that a new email has been received.
    void               newEmail( QString sender, QString subject, bool inInbox, QString command, QString folder, QString url );
    // Signal that the user's personal status has changed.
    void               personalMessageChanged();
};

#endif
