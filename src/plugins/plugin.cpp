/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright 2010, 2011 Daniel E. Moctezuma <democtezuma@gmail.com>    *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

#include "plugin.h"

#include "kmessaccount.h"
#include "kmesschat.h"
#include "kmesscontactlist.h"
#include "kmessinfo.h"
#include "kmessnotification.h"
#include "session.h"

#include "../kmessdebug.h"

#include <KConfigGroup>
#include <KDesktopFile>
#include <KMimeType>
#include <Kross/Manager>
#include <Kross/ActionCollection>


Plugin::Plugin( QObject* parent )
: QObject( parent )
, action_( 0 )
{

}



Plugin::Plugin( const QString& file, QObject* parent )
: QObject( parent )
, action_( 0 )
, file_( file )
{
#ifdef __GNUC__
  #warning TODO: implement this function when a user adds plugins.
#endif
}



Plugin::~Plugin()
{
#ifdef KMESSDEBUG_PLUGIN
  kmDebug() << "DESTROYED.";
#endif
}



/**
  * @brief Call the 'configure' function from the plugin file.
  */
void Plugin::configure()
{
  if( ! action_ )
  {
    return;
  }

  action_->callFunction( "configure" );
}



/**
  * @brief Return the plugin's file.
  */
QString Plugin::file() const
{
  return file_;
}



/**
  * @brief Return the plugin's KPluginInfo.
  */
KPluginInfo Plugin::getKPluginInfo() const
{
  return pluginInfo_;
}



/**
  * @brief Return the plugin's directory.
  */
QString Plugin::getPluginDirectory() const
{
  return pluginDirectory_;
}



/**
  * @brief Return whether or not the plugin's file has a 'configure' function.
  */
bool Plugin::hasConfigure() const
{
  if( ! action_ )
  {
    return false;
  }
#ifdef KMESSDEBUG_PLUGIN
  kmDebug() << "'configure' function found for plugin: " << pluginInfo_.name();
#endif
  QStringList functions = action_->functionNames();
  return functions.contains( "configure" );
}



/**
  * @brief Return whether or not the plugin has KPluginInfo info available.
  */
bool Plugin::hasKPluginInfo() const
{
  return pluginInfo_.isValid();
}



/**
  * @brief Return whether or not the plugin is running.
  */
bool Plugin::isRunning() const
{
  return pluginInfo_.isPluginEnabled();
}



/**
  * @brief Returns 'true' if the plugin's desktop file was loaded successfully, otherwise returns 'false'.
  */
bool Plugin::loadDesktopFile( const QString& dir, const QString& desktopFile )
{
  KDesktopFile df( dir + desktopFile );
  // check if everything is OK.
  if( df.readType().trimmed() != "KMessPlugin" )
  {
    return false;
  }

  pluginInfo_ = KPluginInfo( dir + "/" + desktopFile );
  KConfigGroup g = df.group( "Desktop Entry" );
  file_ = g.readEntry( "X-KMess-Plugin-File", QString() );

#ifdef KMESSDEBUG_PLUGIN
  kmDebug() << "Found '" << file_ << "' config widget for plugin: " <<pluginInfo_.name();
#endif

  if( file_.isEmpty() ) // the plugin file must exist.
  {
    return false;
  }

  file_ = dir + file_;

  return true;
}



/**
  * @brief Run the plugin and return 'true' if it was successfully executed, otherwise returns 'false'.
  */
bool Plugin::run()
{
  if( action_ )
  {
    return false;
  }

  QString interpreter = Kross::Manager::self().interpreternameForFile( file_ );
  if( interpreter.isNull() )
  {
#ifdef KMESSDEBUG_PLUGIN
    kmDebug() << "interpreter is null.";
#endif

    delete action_;
    action_ = 0;

    return false;
  }
  else
  {
    KMimeType::Ptr mimeType = KMimeType::findByPath( file_ );
    QString name = QFileInfo( file_ ).fileName();

    action_ = new Kross::Action( this, name );

    action_->setInterpreter( interpreter );

    action_->addObject( new KMessAccount(),      "Account",      Kross::ChildrenInterface::AutoConnectSignals );
    action_->addObject( new KMessChat(),         "Chat",         Kross::ChildrenInterface::AutoConnectSignals );
    action_->addObject( new KMessContactList(),  "ContactList",  Kross::ChildrenInterface::AutoConnectSignals );
    action_->addObject( new KMessInfo(),         "Info" ,        Kross::ChildrenInterface::AutoConnectSignals );
    action_->addObject( new KMessNotification(), "Notification", Kross::ChildrenInterface::AutoConnectSignals );
    action_->addObject( new Session(),           "Session",      Kross::ChildrenInterface::AutoConnectSignals );

    action_->setText( name );
    action_->setDescription( name );
    action_->setFile( file_ );
    action_->setIconName( mimeType->iconName() );

    Kross::Manager::self().actionCollection()->addAction( file_, action_ ) ;

    action_->trigger();
    pluginInfo_.setPluginEnabled( true );

#ifdef KMESSDEBUG_PLUGIN
    kmDebug() << pluginInfo_.name() << "plugin is now running!";
#endif

    return true;
  }
}



/**
  * @brief Set the plugin's directory.
  */
void Plugin::setPluginDirectory(const QString& dir)
{
  pluginDirectory_ = dir;
}



/**
  * @brief Stop the plugin.
  */
void Plugin::stop()
{
  if( ! pluginInfo_.isPluginEnabled() )
  {
    return;
  }

  // Call unload function if the plugin has one
  if( action_->functionNames().contains( "unload" ) )
  {
    QVariantList args;
    action_->callFunction( "unload", args );
  }

  pluginInfo_.setPluginEnabled( false );

  Kross::ActionCollection* col = Kross::Manager::self().actionCollection();
  col->removeAction( action_->file() );
  action_->deleteLater();
  action_ = 0;


#ifdef KMESSDEBUG_PLUGIN
    kmDebug() << pluginInfo_.name() << "plugin has been stopped!";
#endif
}



#include "plugin.moc"
