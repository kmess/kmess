/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright 2010 Daniel E. Moctezuma <democtezuma@gmail.com>          *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

#ifndef KMESSACCOUNT_H
#define KMESSACCOUNT_H

#include "../account.h"

#include <QObject>

/**
  * @brief Account Interface.
  *
  * This class is one of the plugin interface classes.
  * It provides access to the Account class.
  *
  * @author Daniel E. Moctezuma
  * @ingroup Plugins
  */
class KMessAccount : public QObject
{
  Q_OBJECT

  public:
  // The constructor.
                            KMessAccount ( QObject *parent = 0 );

  public slots:
    // Get the list of all saved accounts.
    const QList<Account *>& getAccounts() const;
    // Return the account selected for automatic login, if any.
    Account*                getAutoLoginAccount();
    // Return the path to the *current* display picture.
    const QString           getDisplayPicture() const;
    // Return the user's friendly name.
    const QString           getFriendlyName() const;
    // Return the user's personal message.
    const QString           getPersonalMessage() const;

    // Return whether the account has unsaved settings changes.
    bool                    isDirty() const;
    // Return whether the account is a guest account, not permanently saved on the computer.
    bool                    isGuestAccount() const;

    // Save account properties.
    void                    saveProperties();
    // Whether the account is a guest account, not permanently saved on the computer.
    void                    setGuestAccount( bool guestAccount );

  signals: // Public signals
    // Signal that a new account has been created.
    void                    accountAdded( Account *account );
    // Signal that an account's properties have changed.
    void                    accountChanged( Account *account, QString oldHandle, QString oldFriendlyName );
};

#endif
