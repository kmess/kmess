/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright 2009 Sjors Gielen <sjors@kmess.org>                       *
 * (C) Copyright 2010 Daniel E. Moctezuma <democtezuma@gmail.com>          *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

#include "kmessinfo.h"
#include "../config-kmess.h"

#include <kdeversion.h>

// The constructor
KMessInfo::KMessInfo( QObject *parent )
 : QObject( parent )
{
  setObjectName( "KMessInfo" );

  kmessVersion       = KMESS_VERSION;

  kdeVersion         = KDE::versionString();
  qtVersion          = qVersion();

  kdeCompileVersion  = KDE_VERSION_STRING;
  qtCompileVersion   = QT_VERSION_STR;
}



// Getters
const QString KMessInfo::getKmessName()         const { return KMESS_NAME;          }
const QString& KMessInfo::getKmessVersion()      const { return kmessVersion;       }

const QString& KMessInfo::getKdeVersion()        const { return kdeVersion;         }
const QString& KMessInfo::getQtVersion()         const { return qtVersion;          }

const QString& KMessInfo::getKdeCompileVersion() const { return kdeCompileVersion;  }
const QString& KMessInfo::getQtCompileVersion()  const { return qtCompileVersion;   }



#include "kmessinfo.moc"
