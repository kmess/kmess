/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright 2010 Daniel E. Moctezuma <democtezuma@gmail.com>          *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

#include "kmesscontactlist.h"

#include "../chat/chat.h"
#include "../chat/chatmaster.h"
#include "../chat/chatwindow.h"

#include "../contact/contact.h"
#include "../contact/contactextension.h"
#include "../contact/contactprivate.h"
#include "../contact/group.h"

#include "../kmessapplication.h"

#include <KMess/MsnContact>
#include <KMess/NetworkGlobals>
#include <KMess/MsnGroup>

// The constructor
KMessContactList::KMessContactList( QObject* parent)
: QObject( parent )
{
  setObjectName( "KMessContactList" );

  // Connect through MsnSession's signals to our signals.
  connect( globalSession->contactList(), SIGNAL( contactAdded( KMess::MsnContact* ) ),
           this,                         SIGNAL( contactAdded( KMess::MsnContact* ) ) );
  connect( globalSession->contactList(), SIGNAL( contactAddedToGroup( KMess::MsnContact*, KMess::MsnGroup* ) ),
           this,                         SIGNAL( contactAddedToGroup( KMess::MsnContact*, KMess::MsnGroup* ) ) );
  connect( globalSession, SIGNAL( contactAddedUser( KMess::MsnContact* ) ),
           this,          SIGNAL( contactAddedUser( KMess::MsnContact* ) ) );
  connect( globalSession, SIGNAL( contactOffline( KMess::MsnContact* ) ),
           this,          SIGNAL( contactOffline( KMess::MsnContact* ) ) );
  connect( globalSession, SIGNAL( contactChangedStatus( KMess::MsnContact*, KMess::MsnStatus ) ),
           this,          SIGNAL( contactChangedStatus( KMess::MsnContact*, KMess::MsnStatus ) ) );
  connect( globalSession, SIGNAL( contactOnline( KMess::MsnContact*, bool ) ),
           this,          SIGNAL( contactOnline ( KMess::MsnContact*, bool ) ) );
  connect( globalSession->contactList(), SIGNAL( groupAdded( KMess::MsnGroup* ) ),
           this,                         SIGNAL( groupAdded( KMess::MsnGroup* ) ) );
  connect( globalSession->contactList(), SIGNAL( groupRenamed( KMess::MsnGroup* ) ),
           this,                         SIGNAL( groupRenamed( KMess::MsnGroup* ) ) );



  QList<KMess::MsnContact*> contactsList = globalSession->contactList()->contacts();

  foreach( KMess::MsnContact* msnContact, contactsList )
  {
    // Connect through MsnSession's signals to our signals.
    connect( msnContact, SIGNAL( groupsChanged() ),
             this,       SIGNAL( groupChanged() ) );
    connect( msnContact, SIGNAL( displayPictureChanged() ),
             this,       SIGNAL( contactChangedDisplayPicture() ) );
    connect( msnContact, SIGNAL( personalMessageChanged() ),
             this,       SIGNAL( contactChangedPersonalMessage() ) );
    connect( msnContact, SIGNAL( mediaChanged() ),
             this,       SIGNAL( contactChangedCurrentMedia() ) );
  }

  // Connect the MsnSession's signals with our slots.
  connect( globalSession->contactList(), SIGNAL( contactAddedToGroup( KMess::MsnContact*, KMess::MsnGroup* ) ),
           this,                           SLOT( addContactToGroup( KMess::MsnContact*, KMess::MsnGroup* ) ) );
}



/**
  * @brief Request the service to add a contact to the friends list.
  *
  * It's possible to add a contact to multiple groups.
  *
  * If the contact wasn't previously known he/she will be added as a friend.
  * If the contact was previously known, he/she will no longer appear in the
  * allowed or removed contacts group, but in the friends list.
  * The contact should exist already at some list before for this to happen
  * (e.g. the block list or friends list).
  *
  * @param contact The MsnContact instance that represents the contact.
  * @param groups  A QList of MsnGroup instances, each one being a group the contact will be added to. May be empty.
  */
void KMessContactList::addContact( KMess::MsnContact *contact, const QList<KMess::MsnGroup*> &groups )
{
  globalSession->contactList()->addContact( contact, groups );
}



/**
  * @brief This is an overloaded function.
  *        Request the service to add a contact to the friends list.
  *
  * @param handle   Email address of the contact.
  * @param network  The network of the contact (Yahoo or Msn).
  * @param groups   Groups where to add the contact. May be empty.
  */
void KMessContactList::addContact( const QString &handle, const KMess::NetworkType network, const QList<KMess::MsnGroup*> &groups )
{
  globalSession->contactList()->addContact( handle, network, groups );
}



/**
  * @brief Request the service to add a contact to another group.
  *
  * @param contact  Pointer to the contact to add.
  * @param group    Pointer to the group that the user will be added to.
  */
void KMessContactList::addContactToGroup( KMess::MsnContact* contact, KMess::MsnGroup* group )
{
  globalSession->contactList()->addContactToGroup( contact, group );
}



/**
  * @brief Request the service to create a group.
  *
  * @param name  Name of the group.
  */
void KMessContactList::addGroup( const QString& name )
{
  globalSession->contactList()->addGroup( name );
}



/**
  * @brief Request the service to block a contact.
  *
  * @param handle  Contact's handle.
  */
void KMessContactList::blockContact( const QString& handle )
{
  foreach( const KMess::MsnContact* msnContact, globalSession->contactList()->contacts() )
  {
    if( msnContact->handle() == handle )
    {
      globalSession->contactList()->blockContact( msnContact );
    }
  }
}



/**
  * @brief Return a list of all known contacts.
  *
  * @return ContactsList
  */
ContactsList KMessContactList::getContacts() const
{
  return globalSession->contactList()->contacts();
}



/**
  * @brief Return a list of all known groups
  *
  * @return GroupsList
  */
GroupsList KMessContactList::getGroups() const
{
  return globalSession->contactList()->groups();
}



/**
  * @brief Return the contact alias.
  *
  * @param handle  The contact's handle.
  * @param mode    The formatting mode.
  *
  * @return QString
  */
const QString KMessContactList::getContactAlternativeName( const QString& handle, FormattingMode mode ) const
{
  Contact contact( globalSession->contactList()->contact( handle ) );

  return contact.getExtension()->getAlternativeName( mode );
}



/**
  * @brief Return the contact's current media.
  *
  * Using this method, you can retrieve the media the contact is currently
  * watching, listening to, or playing.
  *
  * @return QString (the actual current media string)
  */
const QString KMessContactList::getContactCurrentMedia( const QString& handle ) const
{
  return globalSession->contactList()->contact( handle )->media().toString();
}



/**
  * @brief Return the contact display picture path.
  *
  * @param handle  The contact's handle.
  *
  * @return QString
  */
const QString KMessContactList::getContactDisplayPicture( const QString& handle ) const
{
  Contact contact( globalSession->contactList()->contact( handle ) );

  return contact.getContactPicturePath();
}



/**
  * @brief Return the contact's display name.
  *
  * @param handle  The contact's handle.
  *
  * @return QString
  */
const QString KMessContactList::getContactDisplayName( const QString& handle ) const
{
  return globalSession->contactList()->contact( handle )->displayName();
}



/**
  * @brief Return the contacts' handle from the current chat.
  *
  * @return QStringList
  */
const QStringList KMessContactList::getContactsHandleInCurrentChat() const
{
  QList<Chat*> chats = KMessApplication::instance()->chatMaster()->getAllChats();

  foreach( Chat *chat, chats )
  {
    if( chat->isCurrentChat() )
    {
      QStringList handleList;

      foreach( KMess::MsnContact *contact, chat->getMsnChat()->participants() )
      {
        handleList.append( contact->handle() );
      }

      return handleList;
    }
  }

  return QStringList();
}



/**
  * @brief Return the list of user-defined groups where the contact can be found.
  *
  * @param handle  The contact's handle.
  *
  * @return QStringList
  */
const QStringList KMessContactList::getContactIsInGroups( const QString &handle ) const
{
  QStringList groupList;

  foreach( KMess::MsnGroup *group, globalSession->contactList()->contact( handle )->groups() )
  {
    groupList.append( group->name() );
  }

  if( groupList.count() == 0 )
  {
    return QStringList();
  }

  return groupList;
}



/**
  * @brief Return the contact's personal message.
  *
  * @param handle  The contact's handle.
  *
  * @return QString
  */
const QString KMessContactList::getContactPersonalMessage( const QString& handle ) const
{
  return globalSession->contactList()->contact( handle )->personalMessage();
}



/**
  * @brief Return the contact's current status.
  *
  * @param handle  The contact's handle.
  *
  * @return MsnStatus
  */
KMess::MsnStatus KMessContactList::getContactStatus( const QString& handle) const
{
  return globalSession->contactList()->contact( handle )->status();
}



/**
  * @brief Return the contact's true friendly name, regardless of extension.
  *
  * @param handle  The contact's handle.
  * @param mode    The formatting mode.
  *
  * @return QString
  */
const QString KMessContactList::getContactTrueFriendlyName( const QString& handle, FormattingMode mode ) const
{
  Contact contact( globalSession->contactList()->contact( handle ) );

  return contact.getTrueFriendlyName( mode );
}



/**
  * @brief Return the name of this group (will automatically return a name for special groups).
  *
  * @param msnGroup  Pointer to the MsnGroup instance group.
  *
  * @return QString
  */
const QString KMessContactList::getGroupName( KMess::MsnGroup* msnGroup ) const
{
  Group group( msnGroup );

  return group->getName();
}



/**
  * @brief  Read when the last message from this contact has been received.
  *
  * @param handle  The contact's handle.
  *
  * @return QDateTime
  */
const QDateTime &KMessContactList::getLastMessageDate( const QString &handle )
{
  Contact contact( globalSession->contactList()->contact( handle ) );

  return contact.getExtension()->getLastMessageDate();
}



/**
  * @brief Read when the contact has been seen the last time.
  *
  * @param handle  The contact's handle.
  *
  * @return QString
  */
const QDateTime &KMessContactList::getLastSeen( const QString &handle )
{
  Contact contact( globalSession->contactList()->contact( handle ) );

  return contact.getExtension()->getLastSeen();
}



/**
  * @brief Read the list of pictures which the contact has used.
  *
  * @param handle  The contact's handle.
  *
  * @return QStringList
  */
const QStringList &KMessContactList::getPictureList( const QString &handle ) const
{
  Contact contact( globalSession->contactList()->contact( handle ) );

  return contact.getExtension()->getPictureList();
}



/**
  * @brief Return whether the contact has the user in his/her contact list.
  *
  * @param handle  The contact's handle.
  *
  * @return bool
  */
bool KMessContactList::hasUser( const QString& handle ) const
{
  return globalSession->contactList()->contact( handle )->hasUser();
}



/**
  * @brief Return whether the contact was allowed to see the user's presence.
  *
  * @param handle  The contact's handle.
  *
  * @return bool
  */
bool KMessContactList::isContactAllowed( const QString& handle ) const
{
  return globalSession->contactList()->contact( handle )->isAllowed();
}



/**
  * @brief Return whether the contact was blocked.
  *
  * @param handle  The contact's handle.
  *
  * @return bool
  */
bool KMessContactList::isContactBlocked( const QString& handle ) const
{
  return globalSession->contactList()->contact( handle )->isBlocked();
}



/**
  * @brief Return whether the contact is a friend.
  *
  * @param handle  The contact's handle.
  *
  * @return bool
  */
bool KMessContactList::isContactFriend( const QString& handle ) const
{
  return globalSession->contactList()->contact( handle )->isFriend();
}



/**
  * @brief Return whether the contact is currently offline.
  *
  * @param handle  The contact's handle.
  *
  * @return bool
  */
bool KMessContactList::isContactOffline( const QString& handle ) const
{
  return globalSession->contactList()->contact( handle )->isOffline();
}



/**
  * @brief Return whether the contact is currently online.
  *
  * @param handle  The contact's handle.
  *
  * @return bool
  */
bool KMessContactList::isContactOnline( const QString& handle ) const
{
  return globalSession->contactList()->contact( handle )->isOnline();
}



/**
  * @brief Return whether the contact has not accepted our friendship request.
  *
  * @param handle  The contact's handle.
  *
  * @return bool
  */
bool KMessContactList::isContactPending( const QString& handle ) const
{
  return globalSession->contactList()->contact( handle )->isPending();
}



/**
  * @brief Return whether this contact is "unknown",
  *        (i.e. they do not exist in any list in your address book).
  *
  * @param handle  The contact's handle.
  *
  * @return bool
  */
bool KMessContactList::isContactUnknown( const QString& handle ) const
{
  return globalSession->contactList()->contact( handle )->isUnknown();
}



/**
  * @brief Return whether the group with the given ID is empty (i.e. has zero contacts in it).
  *
  * @param groupId GUID of the group
  *
  * @return True if the group has zero contacts in it, false otherwise.
  */
bool KMessContactList::isGroupEmpty( const QString& groupId ) const
{
  return globalSession->contactList()->group( groupId )->isEmpty();
}



/**
  * @brief Return true if this is a special group.
  *
  * @param groupId  GUID of the group.
  *
  * @return bool
  */
bool KMessContactList::isSpecialGroup( const QString& groupId ) const
{
  return globalSession->contactList()->group( groupId )->isSpecialGroup();
}



/**
  * @brief Request the service to move a contact between two groups.
  *
  * @param contact   The contact that is being moved.
  * @param oldGroup  MsnGroup for the source group.
  * @param newGroup  MsnGroup for the destination group.
  */
void KMessContactList::moveContact( const KMess::MsnContact* contact, const KMess::MsnGroup* oldGroup, const KMess::MsnGroup* newGroup )
{
  globalSession->contactList()->moveContact( contact, oldGroup, newGroup );
}



/**
 * @brief Request the service to rename a group.
 *
 * @param group    The group to be renamed.
 * @param newName  Group new name.
 */
void KMessContactList::renameGroup( KMess::MsnGroup* group, QString& newName )
{
  globalSession->contactList()->renameGroup( group, newName );
}



/**
  * @brief Change the contact alias.
  *
  * @param handle           The contact's handle.
  * @param alternativename  The new alternative name to be set.
  */
void KMessContactList::setContactAlternativeName( const QString& handle, const QString& newAlternativeName )
{
  Contact contact( globalSession->contactList()->contact( handle ) );

  contact.getExtension()->setAlternativeName( newAlternativeName );
}



/**
 * @brief Request the service to unblock a contact.
 *
 * @param handle  Contact's handle.
 */
void KMessContactList::unblockContact( const QString& handle )
{
  foreach( const KMess::MsnContact* msnContact, globalSession->contactList()->contacts() )
  {
    if( msnContact->handle() == handle )
    {
      globalSession->contactList()->unblockContact( msnContact );
    }
  }
}



#include "kmesscontactlist.moc"
