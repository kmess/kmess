/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright 2010 Daniel E. Moctezuma <democtezuma@gmail.com>          *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

#include "kmessaccount.h"

#include "../kmessglobal.h"
#include "../accountsmanager.h"

KMessAccount::KMessAccount( QObject *parent )
: QObject( parent )
{
  setObjectName( "KMessAccount" );

  // Connect through AccountsManager's signals to our signals.
  connect( AccountsManager::instance(), SIGNAL( accountAdded( Account* ) ),
           this,                        SIGNAL( accountAdded( Account* ) ) );
  connect( AccountsManager::instance(), SIGNAL( accountChanged( Account*, QString, QString ) ),
           this,                        SIGNAL( accountChanged( Account*, QString, QString ) ) );
}



/**
  * @brief Get the list of all saved accounts.
  */
const QList<Account *>& KMessAccount::getAccounts() const
{
  return AccountsManager::instance()->getAccounts();
}



/**
  * @brief Return the account selected for automatic login, if any.
  */
Account *KMessAccount::getAutoLoginAccount()
{
  return AccountsManager::instance()->getAutoLoginAccount();
}



/**
  * @brief Return the path to the *current* display picture.
  */
const QString KMessAccount::getDisplayPicture() const
{
  return AccountsManager::instance()->getAccount( globalSession->self()->handle() )->getDisplayPicture();
}



/**
  * @brief Return the user's friendly name.
  */
const QString KMessAccount::getFriendlyName() const
{
  return AccountsManager::instance()->getAccount( globalSession->self()->handle() )->getFriendlyName( STRING_CLEANED );
}



/**
  * @brief Return the user's personal message.
  */
const QString KMessAccount::getPersonalMessage() const
{
  return AccountsManager::instance()->getAccount( globalSession->self()->handle() )->getPersonalMessage( STRING_CLEANED );
}



/**
  * @brief Return whether the account has unsaved settings changes.
  */
bool KMessAccount::isDirty() const
{
  return AccountsManager::instance()->getAccount( globalSession->self()->handle() )->isDirty();
}



/**
  * @brief Return whether the account is a guest account, not permanently saved on the computer.
  */
bool KMessAccount::isGuestAccount() const
{
  return AccountsManager::instance()->getAccount( globalSession->self()->handle() )->isGuestAccount();
}



/**
  * @brief Save account properties.
  */
void KMessAccount::saveProperties()
{
  AccountsManager::instance()->getAccount( globalSession->self()->handle() )->saveProperties();
}



/**
  * @brief Whether the account is a guest account, not permanently saved on the computer.
  *
  * @param guestAccount  Whether the account is a guest account.
  */
void KMessAccount::setGuestAccount( bool guestAccount )
{
  AccountsManager::instance()->getAccount( globalSession->self()->handle() )->setGuestAccount( guestAccount );
}



#include "kmessaccount.moc"
