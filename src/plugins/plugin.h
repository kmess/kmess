/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright 2010, 2011 Daniel E. Moctezuma <democtezuma@gmail.com>    *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

#ifndef PLUGIN_H
#define PLUGIN_H

#include <Kross/Action>
#include <KPluginInfo>

#include <QObject>

/**
  * @brief Class for plugin configuration
  *
  * @author Daniel E. Moctezuma
  * @ingroup Plugins
  */
class Plugin : public QObject
{
  friend class PluginsMaster;

  public:
                      Plugin( QObject *parent = 0 );
                      Plugin( const QString &file, QObject *parent = 0 );
                     ~Plugin();

    // Call the 'configure' function from the plugin file.
    void              configure();
    // Return the plugin's file.
    QString           file()           const;
    // Return the plugin's KPluginInfo.
    KPluginInfo       getKPluginInfo() const;
    // Return the plugin's directory.
    QString           getPluginDirectory() const;
    // Return whether or not the plugin's file has a 'configure' function.
    bool              hasConfigure()   const;
    // Return whether or not the plugin has KPluginInfo info available.
    bool              hasKPluginInfo() const;
    // Return whether or not the plugin is running.
    bool              isRunning()      const;
    // Returns 'true' if the plugin's desktop file was loaded successfully, otherwise returns 'false'.
    bool              loadDesktopFile( const QString &dir, const QString &desktopFile );
    // Run the plugin and return 'true' if it was successfully executed, otherwise returns 'false'.
    bool              run();
    // Set the plugin's directory.
    void              setPluginDirectory( const QString &dir );
    // Stop the plugin.
    void              stop();

  private:
    Kross::Action*    action_;
    // Plugin's KPluginInfo information.
    KPluginInfo       pluginInfo_;
    // Plugin's file.
    QString           file_;
    // Plugin's firectory.
    QString           pluginDirectory_;
};

#endif
