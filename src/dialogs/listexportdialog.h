/***************************************************************************
                          listexportdialog.h -  description
                             -------------------
    begin                : Tue Sep 09 2008
    copyright            : (C) 2008 by Antonio Nastasi
    email                : sifcenter@gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef LISTEXPORTDIALOG_H
#define LISTEXPORTDIALOG_H

#include "ui_listexportdialog.h"


/**
 * A dialog to export the contact list
 *
 * @author Antonio Nastasi
 */
class ListExportDialog : public QWidget, private Ui::ListExportDialog
{
  Q_OBJECT

  public:
              ListExportDialog( QWidget *parent = 0 );
              ~ListExportDialog();

  private: // private methods
    // Returns the string formatted in csv
    QString   getCsvString( const QString &text );
    // Select or deselect the items of the contact list
    void      selectAll( bool select = true );
    // Setup the widgets
    void      setupWidgets();

  private slots:
    // The export button was pressed
    void      slotExport();
    // The deselect all button was pressed
    void      slotDeselectAll();
    // The select all button was pressed
    void      slotSelectAll();
};

#endif
