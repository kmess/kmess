/***************************************************************************
                          transferwindow.h -  description
                             -------------------
    begin                : Wed Nov 3 2004
    copyright            : (C) 2004 by Diederik van der Boor
    email                : vdboor --at-- codingdomain.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef TRANSFERMANAGER_H
#define TRANSFERMANAGER_H

#include "ui_transferwindow.h"

#include <QMap>

#include <KMainWindow>


// Forward declarations
class QVBoxLayout;
class TransferEntry;



/**
 * A file transfer manager (aka download manager) singleton.
 *
 * @author Diederik van der Boor
 * @ingroup Dialogs
 */
class TransferWindow : public KMainWindow, private Ui::TransferWindow
{
  Q_OBJECT

  private:
    // Constructor
                               TransferWindow( QWidget *parent = 0 );
    // Destructor
    virtual                   ~TransferWindow();

  public:
    // Add a new entry
    int                        addEntry( const QString filename, ulong filesize, bool incoming = false,
                                         const QImage preview = QImage() );
    // Destroy the current instance
    static void                destroy();
    // Return an instance of the transfer manager
    static TransferWindow     *getInstance();

  public slots:
    // Mark a transfer as failed
    void                       failTransfer( int transferID, const QString &message = QString() );
    // Mark a transfer as complete
    void                       finishTransfer( int transferID );
    // Set the status message of a transfer
    void                       setStatusMessage( int transferID, const QString &message );
    // Update the progress bar of a transfer
    void                       updateProgress( int transferID, ulong bytesTransferred );

  private slots:
    // The "Clean up" button was pressed.
    void                       cleanUp();
    // The "Download" button was pressed
    void                       slotDownloadButtonToggled( bool checked );
    // The "Upload" button was pressed
    void                       slotUploadButtonToggled( bool checked );

  signals:
    // Emit a request to cancel a transfer
    void                       cancelTransfer( int transferID );

  private: // Private methods
    // Show or hide selected entries
    void                       showEntries( bool incoming, bool show );

  private:
    // Counter used to generate unique transfer IDs
    static int                 currentEntryID_;
    // List of all entries
    QMap<int,TransferEntry*>   entryList_;
    // Layout
    QVBoxLayout               *layout_;
    // Instance to the singleton
    static TransferWindow     *onlyInstance_;
};

#endif
