/***************************************************************************
                          contactaddeduserdialog.cpp
                             -------------------
    begin                : Sun Apr 14 2002
    copyright            : (C) 2002 by Mike K. Bennett
    email                : mkb137b@hotmail.com
    copyright            : (C) 2010 by Timo Tambet
    email                : ttambet@gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "contactaddeduserdialog.h"
#include "contactaddeduserwidget.h"

#include "../utils/kmessconfig.h"



/**
 * Constructor
 *
 * This KDialog creates a QTabWidget which holds ContactAddedUserWidget instances. New
 * instances can be added by calling addTab().
 *
 * @param contactHandle         Reference to the handle of the contact that has added you
 * @param contactFriendlyName   The friendly name of the contact
 */
ContactAddedUserDialog::ContactAddedUserDialog( const KMess::MsnContact* contact )
: KDialog()
{
  tabWidget_ = new QTabWidget( this );
  setMainWidget( tabWidget_ );
  setObjectName( "ContactAddedUser" );

  // Let the dialog destroy itself when it's done
  setAttribute( Qt::WA_DeleteOnClose );
  //adjust or restore saved size
  adjustSize();
  setButtons( KDialog::Close );
  restoreDialogSize( KMessConfig::instance()->getGlobalConfig( "ContactAddedUserDialog" ) );

  //  Update the window properties
  setWindowTitle( i18n( "You have been added by someone" ) );
  tabWidget_->setTabsClosable( true );

  addTab( contact );

  connect( tabWidget_, SIGNAL( tabCloseRequested(int) ),
           this,       SLOT(        slotCloseTab(int) ) );
}



/**
* @brief Destructor
*
*/
ContactAddedUserDialog::~ContactAddedUserDialog()
{
  KConfigGroup group = KMessConfig::instance()->getGlobalConfig( "ContactAddedUserDialog" );
  saveDialogSize( group );
}



/**
* @brief Called when a new contact request is received
*/
void ContactAddedUserDialog::addTab( const KMess::MsnContact* contact )
{
  ContactAddedUserWidget *widget = new ContactAddedUserWidget( contact );

  connect( widget, SIGNAL( userChoice(const KMess::MsnContact*,GroupsList,int)),
             this, SIGNAL( userChoice(const KMess::MsnContact*,GroupsList,int) ) );

  tabWidget_->addTab( widget, contact->displayName() );

  connect( widget, SIGNAL(      destroyed(QObject*) ),
           this,   SLOT  ( slotTabDeleted()       ) );
}



/**
 * @brief Called when the user wants to ignore a request.
 *
 * Close the tab as if the user clicked "ignore the request" and confirmed.
 */
void ContactAddedUserDialog::slotCloseTab( int index )
{
  ContactAddedUserWidget* widget = static_cast<ContactAddedUserWidget*>( tabWidget_->widget( index ) );
  if( ! widget )
  {
    return;
  }

  widget->reject();
}



/**
 * @brief Slot to destroy the window when all tabs are gone.
 *
 * When a tab gets destroyed - for whatever reason - check if
 * any tabs are left. If none, the window has to be closed.
 */
void ContactAddedUserDialog::slotTabDeleted()
{
  // When the destroyed() signal is sent out for a tab, it's not yet gone
  // from the tab widget; so it still counts the tab, and we have to fix
  // the count ourselves
  int count = tabWidget_->count() - 1;

  if( count <= 0 )
  {
    deleteLater();
  }
}


