/***************************************************************************
                          awaymessagedialog.h  -  description
                             -------------------
    begin                : Sat Dec 14 2002
    copyright            : (C) 2002 by Mike K. Bennett
    email                : mkb137b@hotmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef AWAYMESSAGEDIALOG_H
#define AWAYMESSAGEDIALOG_H

#include "ui_awaymessagedialog.h"

#include <KDialog>



/**
 * A dialog to let the user set a message, which is then sent automatically when
 * contacts start chatting with the user.
 *
 * @author Mike K. Bennett
 * @ingroup Dialogs
 */
class AwayMessageDialog : public KDialog, private Ui::AwayMessageDialog
{
   Q_OBJECT

  public:
    // The constructor
           AwayMessageDialog( QWidget *parent = 0 );
    // The destructor
          ~AwayMessageDialog();
    // Show the dialog
    bool   useMessage( QString &message );
};

#endif
