/***************************************************************************
                          awaymessagedialog.cpp  -  description
                             -------------------
    begin                : Sat Dec 14 2002
    copyright            : (C) 2002 by Mike K. Bennett
    email                : mkb137b@hotmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "awaymessagedialog.h"

#include "../utils/kmessconfig.h"



// The constructor
AwayMessageDialog::AwayMessageDialog( QWidget *parent )
: KDialog( parent )
, Ui::AwayMessageDialog()
{
  // Set up the interface and the dialog
  setObjectName( "AwayMessage" );
  QWidget *mainWidget = new QWidget( this );
  setupUi( mainWidget );
  setMainWidget( mainWidget );

  setButtons( Ok | Cancel );
  setDefaultButton( Ok );
  setCaption( i18n("Automatic Away Message") );
  restoreDialogSize( KMessConfig::instance()->getGlobalConfig( "AwayMessageDialog" ) );

  adjustSize();
}



// The destructor
AwayMessageDialog::~AwayMessageDialog()
{
  KConfigGroup group = KMessConfig::instance()->getGlobalConfig( "AwayMessageDialog" );
  saveDialogSize( group );
}



// Show the dialog
bool AwayMessageDialog::useMessage(QString &message)
{
  // Set the given mesage name to the text edit as the default message
  messageEdit_->setPlainText( message );

  // Show the form modally
  int result = exec();

  // If OK was pressed...
  if( result != QDialog::Accepted )
  {
    return false;
  }

  // Get the group name from the text edit and store it
  message = messageEdit_->toPlainText();
  return true;
}



#include "awaymessagedialog.moc"
