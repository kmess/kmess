/***************************************************************************
                          networkwindow.h  -  description
                             -------------------
    begin                : Wed Jan 28 2005
    copyright            : (C) 2005 by Diederik van der Boor
    email                : "vdboor" --at-- "codingdomain.com"
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef NETWORKWINDOW_H
#define NETWORKWINDOW_H

#include "../kmessdebug.h"

#include "ui_networkwindow.h"

#include <KMess/Debugger>

#include <QDateTime>
#include <QHash>

#include <KDialog>
#include <KTextBrowser>


// Forward declarations
class QTimer;
class KTabWidget;



/**
 * Network log window, to filter MSN protocol information in a readable way.
 * Based on the original work of Michael Jarrett, but totally revised.
 *
 * @author Diederik van der Boor
 * @author Valerio Pilo <valerio@kmess.org>
 * @ingroup Dialogs
 */
class NetworkWindow : public KDialog, private Ui::NetworkWindow
{
  Q_OBJECT

  public:
    // Return the primary instance of the network window.
    static void            destroy();

  public:
    // Return the primary instance of the network window.
    static NetworkWindow*  instance();


  private slots:
    void                   checkButtons();
    void                   closeConnection( QWidget* item );
    void                   connectionsButtonClicked( QAbstractButton* button );
    void                   logData( const QByteArray& data, bool isReceived, const KMess::Debugger::LogDataSource source, const quint32 connectionId, const QString& name );
    void                   showConnection( QTreeWidgetItem* item, int column );
    void                   slotChatCreated( KMess::MsnChat* );
    void                   slotChatDestroyed( KMess::MsnChat* );
    void                   slotSendCommand();
    void                   slotSendRequest();
    void                   switchViews();
    void                   updateConnectionsTree();


  private:  // Private methods
    // Constructor
                           NetworkWindow( QWidget* parent = 0 );
    // Destructor
    virtual               ~NetworkWindow();
    QString                formatData( const KMess::Debugger::LogDataSource source, bool isReceived, const QByteArray& data );
    // Return the current time.
    QString                getTime() const;


  private: // Private data types
    struct Connection
    {
      QString       name;          // Connection name, provided by the library
      quint32       connectionId;  // Connection ID
      KTextBrowser* logView;       // Widget to show incoming data
      quint64       entries;       // Number of log entries for this connection
      QTime         lastEntry;     // Time of last entry added to the connection data
    };


  private: // Private properties
    // List of all connections
    QHash<KMess::Debugger::LogDataSource,Connection*> connections_;
    // The instance of the network window
    static NetworkWindow* instance_;
    // Global session debugger instance
    KMess::Debugger* debugger_;
    // Timer to style the connections tree
    QTimer* connectionsTreeTimer_;


/*
    // Add a log message to the last log entry
    void                   addLogMessage( QObject* connection, const QString& message );
    // Add an incoming message to the log.
    void                   addIncomingMessage( QObject* connection, const QByteArray& message );
    // Add an outgoing message to the log.
    void                   addOutgoingMessage( QObject* connection, const QByteArray& message );
    // Inform the connection was closed
    void                   connectionClosed( QObject* connection );
    // Set the title of the connection in the display
    void                   setConnectionTitle( QObject* connection, const QString& title );

  private:  // Private forward declarations
    struct ConnectionEntry;

  private slots: // Private slots
    // The 'save tab' button was pressed.
    void                   saveCurrentTab();
    // The 'clear tab' button was pressed.
    void                   clearCurrentTab();
    // The 'close all tabs' button was pressed.
    void                   closeAllTabs();
    // The 'close tab' button was pressed.
    void                   closeTab( QWidget* removedWidget );
    // A tab was selected.
    void                   slotUpdateWidgets( int selectedTab = -2 );
    // Send a command
    void                   slotSendCommand();

  private:  // Private methods
    // The internal function to add a message
    void                   addMessage( ConnectionEntry* entry, QString html );
    // Return a log description for a standard command.
    QString                describeCommand( const QString& command );
    // Return a log description for the direct connection preamble
    QString                describeDirectConnectionPreamble( const QByteArray& message );
    // Return a log description for the mime message
    QString                describeMimeMessage( const QString& mimeMessage );
    // Return a log description for the p2p message
    QString                describeP2PMessage( const QByteArray& message, const int headerStart );
    // Format the description to be displayed
    QString                formatDescription( const QString& description );
    // Format the message to be displayed
    QString                formatMessage( ConnectionEntry* entry, bool incoming, const QByteArray& message );
    // Format a msnp2p message to be displayed.
    QString                formatP2PMessage( bool incoming, const QByteArray& message, const int headerStart );
    // Format a utf-8 string to be displayed
    QString                formatRawData( bool incoming, const QByteArray& message,
                                          const int start, const int length, const int bytesPerCol = 4, const int bytesPerRow = 32 );
    // Format a utf-8 string to be displayed
    QString                formatString( const QString& message );
    // Find the tab for the connection, or create a new one.
    ConnectionEntry*       getConnectionEntry( QObject* connection );

  private:  // Private properties
    // List of connection types
    enum ConnectionType
    {
      TYPE_UNKNOWN = 0,
      TYPE_NS      = 1,
      TYPE_SB      = 2,
      TYPE_FTP     = 3,
      TYPE_FTP_RAW = 4,
      TYPE_DC      = 5,
      TYPE_HTTP    = 6
    };

    // The connection types
    struct ConnectionEntry
    {
      KTextBrowser  *logView;
      ConnectionType type;
      QDateTime      lastMessage;
      bool           isClosed;
    };

    // The html view for each connection
    QHash<void*,ConnectionEntry*> connectionViews_;
*/

};

#endif
