/***************************************************************************
                          contactaddeduserwidget.cpp - QWidget class with
                          QTabWidget
                             -------------------
    begin                : Friday Sept 24 2010
    copyright            : (C) 2010 by Timo Tambet
    email                : ttambet@gmail.com
    copyright            : (C) 2002 by Mike K. Bennett
    email                : mkb137b@hotmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "contactaddeduserwidget.h"
#include "../contact/contact.h"
#include "../contact/group.h"
#include "../utils/kmessconfig.h"
#include "../kmessdebug.h"

#include <KMess/MsnGroup>
#include <KMess/NetworkGlobals>

#include <KStandardGuiItem>



/**
 * Constructor
 *
 * This contains all the widgets for selecting what action to perform when a user adds you
 * to their contact list. Multiple instances of this widget are added to the QTabWidget in
 * ContactAddedUserDialog.
 *
 * @param contact Contact object of the contact who has added you
 */
ContactAddedUserWidget::ContactAddedUserWidget( const KMess::MsnContact* contact )
  : QWidget()
  , Ui::ContactAddedUserWidget()
  , contact_( contact )
{
  setupUi( this );

  buttonbox->addButton( KStandardGuiItem::ok(),      QDialogButtonBox::AcceptRole, this, SLOT( accept() ) );
  buttonbox->addButton( KStandardGuiItem::discard(), QDialogButtonBox::RejectRole, this, SLOT( reject() ) );

  if( contact->handle() == contact->displayName() )
  {
    messageLabel_->setText( QString( "%1" ).arg( contact->handle() ) );
  }
  else
  {
    messageLabel_->setText( QString( "%1 (%2)" ).arg( contact->displayName() )
                                                .arg( contact->handle() ) );
  }

   // if they're already a friend, ie in the contact list, don't allow the "add" option to
  // be selected. the only options should be to allow or block.
  Contact c( contact );
  if( c.isValid() && c.isFriend() )
  {
    addContactOption_->setEnabled( false );
  }

  // Fill up the groups list from the contact list data
  GroupsList groupList( globalSession->contactList()->groups() );
  QListWidgetItem* item;
  foreach( KMess::MsnGroup* group, groupList )
  {
    if( group->isSpecialGroup() )
    {
      continue;
    }

    item = new QListWidgetItem( group->name(), groupList_ );

    // Set the group id in the item's data, to retrieve it later
    item->setData( Qt::UserRole, group->id() );
    item->setFlags( Qt::ItemIsUserCheckable | Qt::ItemIsEnabled );
    item->setCheckState( Qt::Unchecked );
  }
}



/**
* @brief Destructor
*
*/
ContactAddedUserWidget::~ContactAddedUserWidget()
{
}



/**
* @brief Called when the user pressed the "ok" button
*
*/
void ContactAddedUserWidget::accept()
{
  GroupsList groupList;
  if( addContactOption_->isChecked() )
  {
    // Find all checked items in the list
    QListWidgetItem *item;
    for( int i = 0; i < groupList_->count(); i++ )
    {
      item = groupList_->item( i );
      if( item->checkState() != Qt::Checked )
      {
        continue;
      }

      // Retrieve the group id from the item's data
      const QString id( item->data( Qt::UserRole ).toString() );
      KMess::MsnGroup* group = globalSession->contactList()->group( id );
      if( ! group )
      {
        kmWarning() << "Group" << id << "doesn't exist anymore, cannot add new user to it.";
        continue;
      }

      groupList.append( group );
    }
    emit userChoice( contact_, groupList, ADD );
  }
  else if( allowContactOption_->isChecked() )
  {
    emit userChoice( contact_, groupList, ALLOW );
  }
  else if ( blockContactOption_->isChecked() )
  {
    emit userChoice( contact_, groupList, BLOCK );
  }

  // The widget is not necessary anymore, delete it
  deleteLater();
}



/**
* @brief Called when the user pressed the "cancel" button
*
*/
void ContactAddedUserWidget::reject()
{
  // Communicate the user choice
  emit userChoice( contact_, GroupsList(), IGNORE );

  // The widget is not necessary anymore, delete it
  deleteLater();
}



#include "contactaddeduserwidget.moc"
