/***************************************************************************
                          transferentry.h  -  description
                             -------------------
    begin                : Wed Nov 3 2004
    copyright            : (C) 2004 by Diederik van der Boor
    email                : vdboor --at-- codingdomain.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef TRANSFERENTRY_H
#define TRANSFERENTRY_H

#include "ui_transferentry.h"



/**
 * @brief One transfer entry in the transfer window.
 *
 * @author Diederik van der Boor
 * @ingroup Dialogs
 */
class TransferEntry : public QWidget, private Ui::TransferEntry
{
  Q_OBJECT

public:
    // Constructor
    TransferEntry( QWidget *parent, const QString filename, const ulong filesize, bool incoming = false,
                   const QImage preview = QImage() );
    // Destructor
    virtual ~TransferEntry();

    // Returns true if the transfer is either cancelled or completed
    bool            isDone() const;
    // Returns true if the transfer is incoming
    bool            isIncoming() const;
    // Returns the file name
    QString         getFileName() const;
    // Set the transfer ID to identify ourselves to the Transfer Window
    void            setTransferID( int transferID );

    // Format nicely the byte quantities
    static QString  toReadableBytes( ulong bytes );

public slots:
    // Mark the transfer as failed
    void            failTransfer( const QString &message = QString() );
    // Mark the transfer as complete
    void            finishTransfer();
    // Set a status message
    void            setStatusMessage( const QString &message );
    // Update the progress bar
    void            updateProgress( ulong bytesTransferred );

private slots:
    // The cancel link was pressed
    void            cancelClicked();
    // The open link was pressed
    void            openClicked();
    // Update the transfer rate and ETA
    void            updateTransferRate();

signals:
    // Emit a request to cancel the transfer
    void            cancelTransfer( int transferID );

private:
    // Indicate the transfer is done (cancelled or complete)
    bool    isDone_;
    // Indicate this is an incoming transfer (shows the open label)
    bool    incoming_;
    // The file name
    QString filename_;
    // The file size
    ulong   filesize_;
    // The image preview
    QPixmap preview_;
    // The previous percentage
    int     previousPercentage_;
    // The current trasferred bytes
    ulong   bytesTransferred_;
    // The timer for estimated the trasfer rate
    QTimer  *timer_;
     // The previous trasferred bytes
    ulong   previousTransferred_;
    // The current status text
    QString statusText_;
    // The current speed text
    QString speedText_;
    // The ID of this transfer, used by the Transfer Window
    int     transferID_;
};

#endif
