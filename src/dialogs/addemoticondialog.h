/***************************************************************************
                          addemoticondialog.h - shows a dialog to add a custom emoticon
                             -------------------
    begin                : Tue April 10 2007
    copyright            : (C) 2007 by Valerio Pilo
    email                : valerio@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef ADDEMOTICONDIALOG_H
#define ADDEMOTICONDIALOG_H

#include "ui_addemoticondialog.h"

#include <KDialog>

#include <QMovie>
#include <QObject>

class EmoticonTheme;



/**
 * A dialog to create a new emoticon
 *
 * There's no need to call exec() on it, just create an instance: it will popup automatically.
 * If the user accepts the changes, the new emoticon will be added to the current custom emoticons theme.
 *
 * @author Valerio Pilo <valerio@kmess.org>
 * @ingroup Dialogs
 */
class AddEmoticonDialog : public KDialog, private Ui::AddEmoticonDialog
{
  Q_OBJECT

  public:
    // Constructor
    explicit     AddEmoticonDialog( EmoticonTheme *theme, QWidget *parent = 0 );
    // Destructor
    virtual     ~AddEmoticonDialog();
    // Edit the custom emoticon
    void         editEmoticon( const QString &pictureName, const QString &shortcut );
    // Preselects a file name and an emoticon shortcut in the dialog
    void         preSelect( const QString &pictureName, const QString &shortcut );

  private slots:
    // Shows a File Selection dialog to choose an image for the new emoticon
    void         choosePicture();
    // Controls the OK button by checking the dialog's widgets
    void         interfaceChanged();
    // Adds the new emoticon to the theme if the Ok button has been pressed
    void         slotButtonClicked( int button );

  private:
    // Preselected shortcut text
    QString                     preselectedShortcut_;
    // Movie object for gif movie
    QMovie                      *movie_;
    // Theme instance to which we'll add the new emoticon
    EmoticonTheme              *theme_;
    // Determinate if we are editing the current emoticon
    bool                       isEditing_;

  signals:
    // Signal that we've added an emoticon
    void  addedEmoticon( const QString &shortcut );
    // Signal that we've changed an emoticon
    void  changedEmoticon( const QString &shortcut );
};

#endif
