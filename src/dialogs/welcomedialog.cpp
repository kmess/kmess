 /***************************************************************************
                   welcomedialog.cpp  -  A welcoming dialog for new accounts
                             -------------------
    begin                : Thu Jul 9 2009
    copyright            : (C) 2009 by Sjors Gielen
    email                : sjors@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "welcomedialog.h"

#include "../utils/kmessconfig.h"
#include "../utils/kmessshared.h"
#include "../account.h"
#include "../kmessdebug.h"

#include <QWizard>



// Constructor
WelcomeDialog::WelcomeDialog( QWidget *parent )
: QWizard( parent )
{
  // Set up the dialog
  setupUi( this );
  //TODO: setHelp( "using-chat-history" );

  // Let the dialog destroy itself when it's done
  setAttribute( Qt::WA_DeleteOnClose );

  // Set a nice icon
  //TODO:setWindowIcon( KMessShared::drawIconOverlay( KIconLoader::global()->loadIcon( "kmess", KIconLoader::Panel ), KIcon( "chronometer" ).pixmap( 20, 20 ) ) );
  connect( this, SIGNAL(     accepted() ),
           this, SLOT(   saveSettings() ) );
  //TODO:
  //connect( this, SIGNAL( helpRequested() ),
  //         this, SLOT(   helpRequested() ) );
}



// Destructor
WelcomeDialog::~WelcomeDialog()
{
#ifdef KMESSDEBUG_WELCOMEDIALOG
  kmDebug() << "DESTROYED";
#endif
}



/**
 * Save the chosen settings.
 */
void WelcomeDialog::saveSettings()
{
  SettingsList list;
  list[ "LoggingToFileEnabled" ] = keepExternalLogs_->isChecked();
  list[ "LoggingEnabled"       ] = keepInternalLogs_->isChecked();
  list[ "NowListeningEnabled"  ] = enableNowListening_->isChecked();
  list[ "WelcomeDialogShown"   ] = true;

#ifdef KMESSDEBUG_WELCOMEDIALOG
  kmDebug() << list;
#endif

  Account::connectedAccount->setSettings( list );
}

#include "welcomedialog.moc"
