/***************************************************************************
                          contactentry.h -  description
                             -------------------
    begin                : Wed Oct 31 2008
    copyright            : (C) 2008 by Antonio Nastasi
    email                : sifcenter@gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/


#ifndef CONTACTENTRY_H
#define CONTACTENTRY_H

#include "ui_contactentry.h"

#include <QObject>

class Contact;



class ContactEntry : public QWidget, private Ui::ContactEntry
{
  Q_OBJECT

  public:
    // Constructor
    explicit          ContactEntry( const Contact &contact, QWidget *parent = 0 );
    // Another constructor
    explicit          ContactEntry( const QString &handle, QWidget *parent = 0 );
    // Destructor
                      ~ContactEntry();
    // Return the friendly name of the contact
    const QString     getFriendlyName() const;
    // Return the cleaned friendly name (stripped of all formatting, for searching etc)
    const QString&    getCleanedFriendlyName() const;
    // Return the handle of the contact
    const QString&    getHandle() const;
    // Return if the widget is selected
    bool              isSelected() const;
    // Select or deselect this entry
    void              click( bool deselect = false );

  protected:
    // Event Filter to catch mouse click
    bool              eventFilter( QObject *obj, QEvent *event );

  private:
    // Handle
    QString           handle_;
    // Whether this entry is selected
    bool              selected_;
    // The friendly name stripped of all formatting (for searching etc)
    QString           cleanedFriendlyName_;

  signals:
    // Emitted when the widget is clicked
    void              clicked();
};


#endif
