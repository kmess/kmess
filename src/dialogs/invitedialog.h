/***************************************************************************
                          invitedialog.h -  description
                             -------------------
    begin                : Wed Oct 31 2008
    copyright            : (C) 2008 by Antonio Nastasi
    email                : sifcenter@gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef INVITEDIALOG_H
#define INVITEDIALOG_H

#include "ui_invitedialog.h"
#include <KMess/NetworkGlobals>

class ContactEntry;



class InviteDialog : public KDialog, private Ui::InviteDialog
{
  Q_OBJECT

  public:
    // Constructor
                              InviteDialog( const ContactsList &usersAlreadyInChat, QStringList &usersToInvite, QWidget *parent = 0 );
    // Destructor
                              ~InviteDialog();

  private: // private method
    // Update the interface with the contact entries
    void                      updateInterface(  const ContactsList &usersAlreadyInChat, bool onlyOnline = true );

  private: // private attributes
    // Layout for contacts
    QVBoxLayout               *contactsLayout_;
    // Layout for invited contacts
    QVBoxLayout               *invitedLayout_;
    // All users selected for the invitation
    QStringList               usersToInvite_;
    // All contact entries
    QList<ContactEntry*>      contactEntryList_;
    // All invited contact entries
    QList<ContactEntry*>      invitedContacts_;

  private slots:
    // Add contact to invite list
    void                      addContact();
    // The other contact insert button was pressed
    void                      addOtherContactEntry();
    // One contact entry was pressed
    void                      contactClicked();
    // Removed contact from invite list
    void                      removeContact();
    // The search text was changed
    void                      searchTextChanged();
};


#endif
