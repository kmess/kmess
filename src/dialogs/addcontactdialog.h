/***************************************************************************
                          addcontactdialog.h  -  description
                             -------------------
    begin                : Sun Apr 14 2002
    copyright            : (C) 2002 by Mike K. Bennett
    email                : mkb137b@hotmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef ADDCONTACTDIALOG_H
#define ADDCONTACTDIALOG_H

#include "ui_addcontactdialog.h"

#include <KDialog>
#include <KMess/NetworkGlobals>


/**
 * A dialog to add a new friend to the list
 *
 * There's no need to call exec() on it, just create an instance: it will popup automatically.
 * If the user accepts the changes, the friend is added and the dialog then auto-destroys.
 *
 * @author Mike K. Bennett
 * @author Valerio Pilo <valerio@kmess.org>
 * @ingroup Dialogs
 */
class AddContactDialog : public KDialog, private Ui::AddContactDialog
{
   Q_OBJECT

  public:
    // Constructor
                      AddContactDialog( QString &handle, GroupsList &groupsId, KMess::NetworkType &network, QWidget *parent = 0 );
    // Destructor
                      ~AddContactDialog();

  private: // private methods
    // Iterate to check the selected groups
    const GroupsList  getSelectedGroups() const;

  protected slots: // Protected slots
    // Controls the OK button by checking the dialog's widgets
    void              interfaceChanged();
};

#endif
