/***************************************************************************
                          userpicturesdialog.h  -  description
                             -------------------
    begin                : Fri Jul 7 2008
    copyright            : (C) 2008 by Antonio Nastasi
    email                : sifcenter@gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef USERPICTURESDIALOG_H
#define USERPICTURESDIALOG_H

#include "ui_userpicturesdialog.h"

#include <KDialog>



/**
 *
 * @author Antonio Nastasi
 * @ingroup Dialogs
 */
class UserPicturesDialog : public KDialog, private Ui::UserPicturesDialog
{
  Q_OBJECT

  public:
    // Constructor
              UserPicturesDialog( QWidget *parent = 0 );
    // Destructor
             ~UserPicturesDialog();
    // Update the widgets
    void      updateWidgets( const QString &handle );

  private: // Private methods
    // Clear and free memory of list widget
    void      clearList();
    // Use the selected picture.
    void      usePicture();
    // Delete the selected picture.
    void      deletePicture();

  private slots: // Private slots
    // A button has been pressed, act accordingly.
    void      slotButtonClicked( int button );
    // Current item was changed.
    void      slotItemSelectionChanged();

  private: // Private attributes
    QString   handle_;

  signals:
    void      pictureUpdate( bool resize, QString picture );
};

#endif
