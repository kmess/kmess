 /***************************************************************************
                   welcomedialog.h  -  A welcoming dialog for new accounts
                             -------------------
    begin                : Thu Jul 9 2009
    copyright            : (C) 2009 by Sjors Gielen
    email                : sjors@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef WELCOMEDIALOG_H
#define WELCOMEDIALOG_H

#include <QWizard>

#include "ui_welcomedialog.h"



class WelcomeDialog : public QWizard, private Ui::WelcomeDialog
{
  Q_OBJECT

  public:
    // Constructor
            WelcomeDialog( QWidget *parent = 0 );
    // Destructor
    virtual ~WelcomeDialog();

  private:

  private slots:
    void    saveSettings();
};

#endif
