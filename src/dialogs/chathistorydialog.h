 /***************************************************************************
                          chathistorydialog.h  -  chat logs browser
                             -------------------
    begin                : Sun Feb 22 2009
    copyright            : (C) 2009 by Dario Freddi
    email                : drf54321@gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CHATHISTORYDIALOG_H
#define CHATHISTORYDIALOG_H

#include <QDomDocument>
#include <QHash>
#include <QMap>
#include <QMutex>
#include <QThread>
#include <QPoint>

#include <KDialog>

#include "ui_chathistorydialog.h"


// Forward declarations
class QSortFilterProxyModel;
class QStandardItemModel;
class QTimer;
class Account;
class ChatMessageView;
class XmlLogParser;



class ChatHistoryDialog : public KDialog, private Ui::historyDialog
{
  Q_OBJECT

  public:
    // Constructor
                           ChatHistoryDialog( QWidget *parent = 0 );
    // Destructor
    virtual               ~ChatHistoryDialog();
    // Set the contact for whom the logs will be initially shown
    bool                   setContact( const QString &handle );

  private:
    // Load the list of contacts for which logs are available
    void                   loadContactsList();

  private slots:
    // A conversation has been parsed by the thread
    void                   addConversation( int timestamp, const QDomElement &newConversation );
    // Verify whether the chat date interval is valid or not
    bool                   checkDates( bool reloadLogs = true );
    // Caches the XML for the selected contact. Makes things a lot faster
    void                   cacheContactXml(const QModelIndex &index);
    // Reload the current chat log history
    void                   reloadLogs();
    // Set whether the dialog is loading chats or not
    void                   setLoading( bool isLoading = false );
    // Show the context menu for the chat message view
    void                   slotShowContextMenu( const QString &clickedUrl, const QPoint &point );
    // Change the account for which to show logs
    void                   reloadHandle( int index );

  private:
    // Mutex to watch over the chats addition
    QMutex                 addConversationLock_;
    // View used to show the chat logs
    ChatMessageView       *chatView_;
    // Cache to store a contact's list of available conversations
    QMap<int,QDomNode>     conversations_;
    // XML log parsing thread
    XmlLogParser          *logParser_;
    // Model for the list of contacts
    QStandardItemModel    *model_;
    // Filter for the list of contacts
    QSortFilterProxyModel *proxyModel_;
    // Flag used to avoid reloading multiple times
    bool                   reloadingLogs_;
    // Timer to reload the shown chats
    QTimer                *reloadLogsTimer_;
    // Cache to store a contact's entire chat history
    QDomDocument           xml_;
    // The account which to display logs of
    Account               *historyAccount_;
};



// Service class to make use of threads to fill in the list of available chats
class XmlLogParser : public QThread
{
  Q_OBJECT

  public:
    // Constructor
    XmlLogParser( bool showDate, bool showTime, bool showSeconds );
    // Destructor
    ~XmlLogParser() {}
    // Add another document to the queue
    void addDocument( const QString &fileName, const QByteArray &newDocument );
    // Stop the thread
    void end();

  protected:
    // Parse XML files into conversations
    void run();

  private:
    // List of documents to parse
    QHash<const QString,QByteArray> documents_;
    // An order of stopping has been received
    bool              end_;
    // Show dates in messages
    bool              showDate_;
    // Show time in generated messages
    bool              showTime_;
    // Show seconds in messages
    bool              showSeconds_;

  signals:
    // A conversation has been parsed
    void              gotConversation( int timestamp, const QDomElement &conversation );
};



#endif /* CHATHISTORYDIALOG_H */
