/***************************************************************************
                          contactaddeduserdialog.h  -  description
                             -------------------
    begin                : Sun Apr 14 2002
    copyright            : (C) 2002 by Mike K. Bennett
    email                : mkb137b@hotmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CONTACTADDEDUSERDIALOG_H
#define CONTACTADDEDUSERDIALOG_H

#include <KMess/MsnContact>
#include <KMess/MsnGroup>

#include <KDialog>

class QTabWidget;



/**
 * The dialog for when someone has added the user to his or her contact list.
 *
 * @author Mike K. Bennett
 * @author Timo Tambet
 * @ingroup Dialogs
 */
class ContactAddedUserDialog : public KDialog
{
  Q_OBJECT

  public:
    // The constructor
                 ContactAddedUserDialog( const KMess::MsnContact *contact );
    // The destructor
    virtual      ~ContactAddedUserDialog();
    void         addTab( const KMess::MsnContact* contact );

   private slots:
    void         slotCloseTab( int index );
    void         slotTabDeleted();

  signals:
    void         userChoice( const KMess::MsnContact* contact, const GroupsList groups, const int code );

  private:
    QTabWidget   *tabWidget_;
};



#endif
