/***************************************************************************
                          contactaddeduserwidget.h  -
                             -------------------
    begin                : Friday Sept 24 2010
    copyright            : (C) 2010 by Timo Tambet
    email                : ttambet@gmail.com
    copyright            : (C) 2002 - ... by Mike K. Bennett
    email                : mkb137b@hotmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CONTACTADDEDUSERWIDGET_H
#define CONTACTADDEDUSERWIDGET_H

#include <KMess/MsnContact>

#include "ui_contactaddeduserwidget.h"



/**
 * The dialog for when someone has added the user to his or her contact list.
 *
 * @author Timo Tambet, Mike K. Bennett
 * @ingroup Dialogs
 */
class ContactAddedUserWidget : public QWidget, private Ui::ContactAddedUserWidget
{
  Q_OBJECT

  public:
    // Return types for the dialog
    enum ReturnCode
    {
      ADD   = 0
    , ALLOW = 1
    , BLOCK = 2
    , IGNORE = 3
    };

  public:
    // The constructor
    ContactAddedUserWidget( const KMess::MsnContact* contact );
    // The destructor
    virtual ~ContactAddedUserWidget();

  public slots: // Protected slots
    // The user pressed the "ok" button
    void accept();
    // The user pressed the "cancel" button
    void reject();

  private: // Private properties
    // The contact object of the person who has added the user
    const KMess::MsnContact* contact_;
  signals:
    // Notify of the choice made by the user
    void userChoice( const KMess::MsnContact* contact, const GroupsList groups, int code );
};



#endif
