/***************************************************************************
                          contactpropertiesdialog.h  -  description
                             -------------------
    begin                : Sun Dec 15 2002
    copyright            : (C) 2002 by Michael Curtis
    email                : magnalium@hotmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CONTACTPROPERTIESDIALOG_H
#define CONTACTPROPERTIESDIALOG_H

#include "ui_contactpropertiesdialog.h"

#include "../contact/contact.h"

#include <KDialog>
#include <Phonon/MediaObject>

namespace KMess
{
  class MsnContact;
  class MsnGroup;
}


/**
 * The dialog for modifying contact properties (ContactExtension).
 *
 * @author Michael Curtis
 * @ingroup Dialogs
 */
class ContactPropertiesDialog : public KDialog, private Ui::ContactProperties
{
   Q_OBJECT

  public:
    enum DefaultTab
    {
      Information,
      Notes,
      Emoticons,
      Images
    };

  public:
                 ContactPropertiesDialog( QWidget *parent=0 );
    virtual      ~ContactPropertiesDialog();
    // Clear the qlistwidget if there are already object
    void         clearLists();
    // Show the dialog and obtain the contact handle.
    bool         launch( KMess::MsnContact *contact, DefaultTab defaultTab = Information );

  protected:
    bool         eventFilter(QObject *obj, QEvent *event);

  private: // Private methods
    void         applyChanges();
    void         setupWidgets();

  private slots:
    // The alt name checkbox was toggled.
    void         alternativeNameCheckBoxToggled(bool checked);
    // Choose a picture file (KFile dialog)
    void         choosePicture();
    // Choose a sound file (KFile dialog)
    void         disableNotificationsCheckBoxToggled(bool checked);
    // Restore the original contact picture
    void         restoreContactPicture();
    // The Ok or Cancel button has been pressed
    void         slotButtonClicked( int button );
    // The "Clear cache" button has been pressed
    void         slotClearCache();
    // The user has selected or deselected something in the emoticon blacklist
    void         slotEmoticonSelectionChanged();
    // The user has selected or deselected something in the pictures list
    void         slotPictureSelectionChanged();
    // The play sound button has been pressed
    void         slotPlaySound();
    // Remove a blacklisted emoticon
    void         slotRemoveEmoticon();
    // Called when media sound finished
    void         slotSoundFinished();
    // Use the selected picture as personal image
    void         slotUsePicture();
    // Check if the fileformat is supported by Phonon
    bool checkNotificationSoundFile( const KUrl &url );

  private: // Private attributes
    // Alternative picture path
    QString             alternativePicturePath_;
    // Contact
    KMess::MsnContact  *msnContact_;
    // GUI contact
    Contact             contact_;
    // Whether or not OK was pressed.
    bool                ok_;
    // Sound media object
    Phonon::MediaObject *mediaSound_;
};

#endif
