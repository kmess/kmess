/***************************************************************************
                          contactentry.cpp -  description
                             -------------------
    begin                : Wed Oct 31 2008
    copyright            : (C) 2008 by Antonio Nastasi
    email                : sifcenter@gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "contactentry.h"

#include "../contact/contact.h"
#include "../utils/richtextparser.h"
#include "../kmessdebug.h"

#include <QTextDocument>



// Constructor
ContactEntry::ContactEntry( const Contact &contact, QWidget *parent )
  : QWidget( parent )
  , Ui::ContactEntry()
  , selected_(false)
{
  // Setup UI
  setupUi( this );
  installEventFilter( this );

  // Fill labels with the informations about contact
  handle_              = contact->getHandle();
  cleanedFriendlyName_ = contact->getFriendlyName( STRING_CLEANED );
  nameLabel_  ->setText( contact->getFriendlyName( STRING_LIST_SETTING_ESCAPED ) );
  handleLabel_->setText( handle_ );
  picture_    ->setPixmap( contact->getContactPicturePath() );
}



// Another constructor
ContactEntry::ContactEntry( const QString &handle, QWidget *parent )
  : QWidget( parent )
  ,Ui::ContactEntry()
  , selected_(false)
{
  // Setup UI
  setupUi( this );
  installEventFilter( this );

  // Fill labels with the informations about contact, the picture and the name is unknow
  // because we haven't information about it
  handle_              = handle;
  cleanedFriendlyName_ = handle;
  nameLabel_  ->setText( handle );
  handleLabel_->setText( handle );
}



// Destructor
ContactEntry::~ContactEntry()
{
}



// Event Filter to catch mouse click
bool ContactEntry::eventFilter( QObject */*obj*/, QEvent *event )
{
  if( event->type() == QEvent::MouseButtonRelease )
  {
    emit clicked();
  }

  return false;
}



// Return the friendly name of the contact
const QString ContactEntry::getFriendlyName() const
{
  return nameLabel_->text();
}



// Return the cleaned friendly name (stripped of all formatting, for searching etc)
const QString& ContactEntry::getCleanedFriendlyName() const
{
  return cleanedFriendlyName_;
}



// Return the handle for the contact
const QString& ContactEntry::getHandle() const
{
  return handle_;
}



bool ContactEntry::isSelected() const
{
  return selected_;
}



// Set the color of background to simulate the selection
void ContactEntry::click( bool deselect )
{
  QPalette palette;
  QColor color;

  if( deselect )
  {
    selected_ = false;
  }
  else
  {
    selected_ = ! selected_;
  }

  if( selected_ )
  {
    color = palette.color( QPalette::Highlight );
  }
  else
  {
    color = palette.color( QPalette::Background );
  }

  palette.setColor( QPalette::Background, color );
  setPalette( palette );
}

#include "contactentry.moc"
