/***************************************************************************
                          emoticonswidget.h -  description
                             -------------------
    begin                : Mon 12 09 2005
    copyright            : (C) 2005 by Diederik van der Boor
    email                : "vdboor" --at-- "codingdomain.com"
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef EMOTICONSWIDGET_H
#define EMOTICONSWIDGET_H

#include <QListWidget>


class AddEmoticonDialog;
class Emoticon;
class EmoticonListWidget;
class EmoticonManager;

class QBoxLayout;
class QDockWidget;
class QLabel;
class QMouseEvent;
class QPoint;
class QResizeEvent;



/**
 * The widget in a chat that contains the emoticons to send to your contacts.
 *
 * @author Diederik van der Boor, Valerio Pilo
 * @ingroup Chat
 */
class EmoticonsWidget : public QWidget
{
  Q_OBJECT

  public:
    // The constructor
                       EmoticonsWidget( QWidget *parent );
    // The destructor
    virtual           ~EmoticonsWidget();
    // Initialize the widget
    void               initialize( bool useCustomTheme );
    // Connect the widget to a dock
    void               setDockWidget( QDockWidget *dockWidget, Qt::DockWidgetArea initialArea );

  protected:
    // Event filter for the preview label
    bool              eventFilter( QObject *obj, QEvent *e );

  private slots:
    // One item was clicked
    void               slotItemClicked( const QListWidgetItem *item );
    // The location of the parent dock widget has changed
    void               slotLocationChanged( Qt::DockWidgetArea area );
    // The "Click to open the custom emoticons settings" link has been clicked
    void               slotNoEmoticonsLinkClicked();
    // Preview requested
    void               slotRequestPreview( const QListWidgetItem *item );
    // The theme has changed, we need to reload it
    void               slotThemeUpdated();
    // The parent dock widget floating status has changed
    void               slotTopLevelChanged( bool isTopLevel );
    // Right mouse button was clicked
    void               showContextMenu(const QListWidgetItem *item, const QPoint &point );

  private:  // Private enumerations
    enum ButtonRole
    {
      ShortcutRole = Qt::UserRole  // Role to store the button's shortcut
    , PictureRole                  // Role to store the button's picture path
    };

  private:  // private properties
    // Emoticon dialog for edit the custom emoticons
    AddEmoticonDialog *emoticonDialog_;
    // Whether the widget has been initialized
    bool               initialized_;
    // Whether we will keep a standard or custom theme
    bool               isCustom_;
    // Horizontal Layout which will lay our list
    QBoxLayout        *layout_;
    // The list widget of emoticons
    EmoticonListWidget    *list_;
    // A warning message to tell the user he/she's got no custom emoticons
    QLabel            *noEmoticonsWarning_;
    // Preview label
    QLabel            *previewLabel_;
    // Preview movie to insert in preview label
    QMovie            *previewMovie_;

    // Reference to the emoticon manager, to update the emoticon buttons
    EmoticonManager   *manager_;

  signals: // Signals
    // Request that the emoticon's html code should be inserted in the message.
    void               insertEmoticon( const QString &html );
};



// Extend the class QListWidget to grep the mousePressEvent
// because the use of showContextMenu in main class calls also the clicked slot.
class EmoticonListWidget : public QListWidget
{
  Q_OBJECT

  public:
                      EmoticonListWidget( QWidget *parent );

  protected:
    // Reimplement the mouse press event to grep left and right click
    void              mousePressEvent( QMouseEvent *e );
    // Reimplement the event filter
    bool              eventFilter( QObject *obj, QEvent *e );

  private:
    QListWidgetItem   *previousItem_;

  signals:
    void              leftClicked ( const QListWidgetItem* );
    void              rightClicked( const QListWidgetItem*, const QPoint& );
    void              requestPreview( const QListWidgetItem* );
};


#endif
