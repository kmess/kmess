/***************************************************************************
                          chat.h  -  description
                             -------------------
    begin                : Wed Jan 15 22:41:32 CST 2003
    copyright            : (C) 2003 by Mike K. Bennett
    email                : mkb137b@hotmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CHAT_H
#define CHAT_H

#include "chatview.h"

#include <KMess/MsnChat>

#include <QHash>
#include <QStringList>
#include <QTimer>


// Forward declarations
class ChatView;
class ChatWindow;
class Contact;
class KMessTest;

namespace KMess
{
  class MsnChat;
  class TextMessage;
  class MsnObject;
};

class QDir;



/**
 * Time before a typing message expires
 *
 * Expressed in seconds, this value determines for how long the chat window's status bar messages
 * "Contact is typing..." are displayed.
 */
#define CHAT_TYPING_EXPIRATION_TIME     5



/**
 * @brief Manage an active chat
 *
 * This class manages a chat. It handles the communication with the Switchboard connection;
 * and connects the Chat View signals and calls to make user interaction possible.
 *
 * It is meant to be inserted into a ChatWindow object, not to be used alone.
 *
 * @ingroup Chat
 */
class Chat : public ChatView
{
  Q_OBJECT

  friend class KMessTest;

  public:
    // The construtor
                       Chat( QWidget* parent = 0 );
    // The destructor
                      ~Chat();
    // Perform final processing on an incoming message before showing it to the user.
    void               displayIncomingMessage( KMessMessage &message);
    // Return a list of the contacts in the chat to be used as window caption
    const QString      getCaption();
    // Return when the last message came in, if any
    QDateTime          getLastReceivedMessageTime() const;
    // Returns the associated MsnChat object
    KMess::MsnChat    *getMsnChat() const;
    // Return the chat window which currently contains this Chat
    ChatWindow        *getChatWindow();
    // Return the icon to use in the tab icon chat
    KIcon              getParticipantsTabIcon();
    // Return the list of previously sent messages
    QStringList        &getQuickRetypeList();
    // Return the date and time the chat has started
    QDateTime          getStartTime() const;
    // Return the list of contacts which are typing
    const QStringList  getTypingContacts() const;
    // Invite a contact to the chat
    void               inviteContacts( const QStringList &contacts );
    // Return whether or not the contact is in this chat. Check also if the chat is exclusive with the contact.
    bool               isContactInChat( const QString &handle, bool isExclusiveChatWithContact = false );
    // Return whether or not the chat is at its first incoming message
    bool               isChatFirstMessage();
    // Return whether or not the chat is closing
    bool               isClosing();
    // Return whether or not is the chat currently visible in the chat window.
    bool               isCurrentChat();
    // The application is exiting
    void               queryExit();
    // The chat is closing
    void               queryClose();
    // Send a text message to the current chat
    void               sendChatMessage( const QString &message );
    // Send an ink drawing to the current chat
    void               sendInkMessage( KMess::InkFormat format, const QByteArray &inkData );
    // Send an wink to the current chat
    void               sendWink( const KMess::MsnObject &message );
    // Enable or disable the parts of the window which allow user interaction
    void               setEnabled( bool isEnabled );
    // Start a chat
    void               startChat();

  public slots:

    // Change the switchboard connection used by the chat window.
    void               setMsnChat( KMess::MsnChat *newChat );
    // Send a nudge
    void               slotSendNudge();
    // Send a file to a contact.
    void               startFileTransfer( QList<QUrl> fileList = QList<QUrl>() );
    //Voice conversation
    void               startConversation();
    // Start GnomeMeeting with a contact.
    void               startMeeting();
    // Send webcam data to a contact.
    void               startWebcamTransfer();
    // Show a wink received by a contact
    void               showWink( const QString &handle, const QString &filename, const QString &animationName );
    // save the chat if needed.
    void               saveChatAutomatically( bool saveExternalFile = false );

  private: // Private methods
    // Choose the contact to start an invitation with.
    const QString      chooseContact();
    // An ink message was received from a chat
    void               receivedInkMessage( KMess::InkMessage message );
    // Notify the user of a received nudge
    void               receivedNudgeMessage( KMess::MsnContact *contact );

  private slots: // Private slots
    // A contact joined the chat
    void               contactJoined( KMess::MsnContact *contact );
    // A contact left the chat
    void               contactLeft  ( KMess::MsnContact *contact, bool isChatIdle );
    // A contact is typing
    void               contactTyping( KMess::MsnContact *contact = 0, bool forceExpiration = false );
    // A warning from the switchboard has been received, display it
    void               showWarning( KMess::ChatWarningType type, KMess::MsnContact *contact );
    // Display a message to notify of a contact status change
    void               slotContactChangedStatus();
    // A message was received from the chat (could be any kind of message - ink, nudge, wink, text, etc).
    void               slotReceivedMessage( KMess::Message message);
    // Notify that a message could not be sent by the switchboard
    void               slotSendingFailed( KMess::Message message, const QString &recipient );

  private: // Private attributes
    // Whether or not the incoming message is the first incoming message
    bool               firstMessage_;
    bool               isClosing_;
    // The date/time the last message came in
    QDateTime          lastReceivedMessage_;
    // The last time an automessage has been sent
    QTime              lastSentAutoMessage_;
    // The object that connects to the switchboard server.
    //MsnSwitchboardConnection *msnSwitchboardConnection_;
    // List of previously sent messages, to allow sending them again quickly
    QStringList        quickRetypeList_;
    // The date/time the chat started
    QDateTime          startDateTime_;
    // The list of names of contacts which are currently typing
    QHash<QString,QString> typingContactsNames_;
    // The list of times when the contacts have started typing
    QHash<QString,QTime>   typingContactsTimes_;
    // A timer to update the typing information
    QTimer             typingTimer_;
    // Associated MsnChat object.
    KMess::MsnChat    *msnChat_;

  public:
    // maximum log file size, 2MB.
    static const long   MAX_LOG_FILE_SIZE = 2*1024*1024;

  signals:
    // Signal that some chat details (like the number of active contacts) have changed
    void               chatInfoChanged();
    // Signal that this chat is about to close.
    void               closing( Chat *chat );
    // Signal the presence of a new incoming chat message
    void               gotChatMessage( KMess::TextMessage &message, Chat *chat );
    // Signal that a typing message has been received
    void               gotTypingMessage( Chat *chat );
    // Signal that a nudge has been received
    void               gotNudge();
    // Signal that the ChatMaster should start a filetransfer.
    void               requestFileTransfer(const QString &handle, const QString &filename);
    // Signal that the ChatMaster should start a newmeeting session.
    void               requestNetMeeting(const QString &handle);
    // Signal that the ChatMaster should start a webcam transfer.
    void               requestWebcamTransfer(const QString &handle);
};

#endif
