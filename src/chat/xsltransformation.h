/***************************************************************************
                          xsltransformation.h -  description
                             -------------------
    begin                : Sat Oct 8 2005
    copyright            : (C) 2005 by Diederik van der Boor
    email                : vdboor --at-- codingdomain.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef XSLTRANSFORMATION_H
#define XSLTRANSFORMATION_H

#include <libxml/globals.h>
#include <libxslt/xsltInternals.h>

#include <QMap>
#include <QString>
#include <QVariant>



/**
 * A wrapper class to call libxslt functions.
 * The XSL theme files are used to transform XML
 * chat messages in the desired HTML markup.
 *
 * @author Diederik van der Boor
 * @ingroup Chat
 */
class XslTransformation
{
  public:
    // The constructor
                               XslTransformation();
    // The destructor
    virtual                   ~XslTransformation();


    // Convert an XML string
    QString                    convertXmlString(const QString &xml) const;
    // Check whether a stylesheet is loaded successfully
    bool                       hasStylesheet() const;
    // Set an XSL parameter
    void                       setParameter( const QString &key, const QVariant &value );
    // Set the XSL parameters
    void                       setParameters( const QMap<QString,QString> xslParameters );
    // Set the XSL stylesheet.
    void                       setStylesheet(const QString &xslFileName);

  private:
    // Refresh the actual XSL parameters list
    void                       updateParametersList();
    // Update the translation strings in the XSL document.
    void                       updateXslTranslations( xmlNode *node, const xmlNs *xslNs, const xmlNs *kmessNs );

  private:
    QString                    xslFileName_;
    xsltStylesheet            *styleSheet_;
    xmlDoc                    *xslDoc_;
    char                     **xslParams_;
    int                        xslParamCount_;
    QMap<QString,QString>      parametersMap_;
};

#endif
