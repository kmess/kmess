/***************************************************************************
                         webcamframe.h - Code for the webcam frame
                             -------------------
    begin                : Wed 1 Apr 2009
    copyright            : (C) 2008 by Sjors Gielen
    email                : dazjorz@dazjorz.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef WEBCAMFRAME_H
#define WEBCAMFRAME_H

#include "ui_webcamframe.h"


class WebcamFrame : public QWidget, public Ui::WebcamFrame
{
  Q_OBJECT

  public:
    // Constructor
                        WebcamFrame( QWidget *parent = 0, QString status = QString() );

    void                setStatus( const QString& status );
    const QString&      getStatus() const;

    //void                setPainter( QPainter *painter );
    //QPainter           *getPainter() const;
    const QPainter&     getPainter();

  public slots:

  protected: // protected methods

  private: // private methods

  private slots:

  private: // private attribute
    QString            status_;
    QPixmap            pixmap_;
    QPainter          *painter_;
};

#endif
