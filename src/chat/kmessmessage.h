/***************************************************************************
    kmessmessage.h - Message containing KMess-specific data shown
                     in chat views (presence, application, nudge,
                     etc messages).
                             -------------------
    begin                : Fri 18 Dec 2009
    copyright            : 2009 by Adam Goossens
    email                : adam@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef KMESSMESSAGE_H
#define KMESSMESSAGE_H

#include <KMess/TextMessage>

namespace KMess
{
  class MsnChat;
  class MsnContact;
};

/**
 * This class has two purposes: it wraps TextMessages from the underlying library
 * and also provides a way to identify KMess-specific messages, such as presence notifications,
 * application invites, inks, etc etc.
 */
class KMessMessage : public KMess::TextMessage
{
  public: // enumerations
    /**
     * The type of kmess-specific notifications 
     */
    enum MessageType
    {
      Notification,
      Nudge,
      System,
      Wink,
      ApplicationFile,
      ApplicationWebcam,
      ApplicationAudio,
      Application,
      OfflineIncoming,
      Presence,
      Ink,
      IncomingText,
      OutgoingText,
      Unknown
    };
    
  public: // Constructors
    KMessMessage();
    KMessMessage( MessageType type, KMess::MsnChat *chat = 0, const KMess::MsnContact *contact = 0 );
    KMessMessage( const KMessMessage &other );
    KMessMessage( const Message &other, MessageType fmt = IncomingText );
    
  public: // Public members
    bool                isNormal() const;
    MessageType         specificType() const;
    void                setSpecificType( MessageType type );
    
  private: // Private variables
    MessageType         specificType_;
    
};

#endif
