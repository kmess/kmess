/***************************************************************************
                          contactframe.h  -  description
                             -------------------
    begin                : Thu Jan 16 2003
    copyright            : (C) 2003 by Mike K. Bennett
    email                : mkb137b@hotmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CONTACTFRAME_H
#define CONTACTFRAME_H

#include "ui_contactframe.h"

#include <QDateTime>
#include <QTimer>


// Forward declarations
class ContactPropertiesDialog;
class KAction;
class KActionMenu;
class KMenu;

#include "../contact/contact.h"


/**
 * The frame displaying contact information.
 *
 * @author Mike K. Bennett
 * @ingroup Chat
 */
class ContactFrame : public QWidget, private Ui::ContactFrame
{
  Q_OBJECT

  public: // Public enumerations

    // Contact frame display modes
    enum DisplayMode
    {
      ModeSingle   // Single contact chat: all the widgets but the separator are displayed
    , ModeNormal   // Common group chat: all the widgets are displayed
    , ModeSmall    // Big group chat: Smaller pictures, less details for easier viewing
    , ModeTiny     // Mega group chat: A lot less details to optimize the available space
    };



  public:
    // The constructor
                     ContactFrame(QWidget *parent=0);
    // The destructor
                    ~ContactFrame();
    // Activate the frame by giving it a contact
    void             activate( KMess::MsnContact *contact );
    // Return the handle of this frame's contact
    const QString&   getHandle() const;
    // Initialize the contact popup
    bool             initContactPopup();
    // The user received a message from this contact
    void             messageReceived();
    // Change the display mode of the frame when the contact leaves the chat or there are many active contacts
    void             setDisplayMode( DisplayMode mode );
    // Enable or disable the frame
    void             setEnabled( bool isEnabled );
    // Receive notice that the contact is typing
    void             startTyping();

  private: // Private methods
    // The personal status message received an event.
    bool             eventFilter( QObject *obj, QEvent *event );

  private slots: // Private slots
    // Allow this contact to see our MSN status
    void             allowContact();
    // Foward the changed list signal from contact
    void             contactChangedList();
    // The contact was destroyed
    void             contactDestroyed( QObject *object );
    // Copy some details of the contact to the clipboard
    void             copyText();
    // Email the contact
    void             sendEmail();
    // Update and show the contact popup menu
    void             showContactPopup( const QPoint &point );
    // Show the contact properties window
    void             showContactProperties();
    // Show the contact's profile
    void             showProfile();
    // Request to start private chat
    void             slotStartPrivateChat();
    // Disable the typing label when the timer has timed out
    void             stopTyping();
    // Change the contact's added/removed status
    void             toggleContactAdded();
    // Change the contact's blocked/unblocked status
    void             toggleContactBlocked();
    // Update the contact picture
    void             updatePicture();
    // Update the status widgets
    void             updateStatusWidgets();

  private: // Private attributes
    // The contact whom the frame represents
    KMess::MsnContact *contact_;
    // The menu of actions possible to perform on a given contact
    KMenu           *contactActionPopup_;
    // Whether or not the display picture will be shown
    bool             contactDisplayPictureEnabled_;
    // Contact properties dialog
    ContactPropertiesDialog *contactPropertiesDialog_;
    // List of actions with links to copy to the clipboard
    QList<KAction*>  copyLinkActionsList_;
    // The detailed contact information, if available
    Contact          detailedContact_;
    // The contact's handle, saved for performance and for safety
    QString          handle_;
    // Current display mode
    DisplayMode      currentMode_;
    // Whether or not the PM or music can be shown
    bool             infoLabelEnabled_;
    // Whether or not is it possible to add or block the contact
    bool             locked_;
    // Contact popup KAction to add the contact to the contact list
    KAction         *popupAddContact_;
    // Contact popup KAction to allow the contact to see the user's status
    KAction         *popupAllowContact_;
    // Contact popup KAction to block the contact
    KAction         *popupBlockContact_;
    // Contact popup menu to show the contact's properties window
    KAction         *popupContactProperties_;
    // Contact popup menu to show the friendly name/personal message/media copying options
    KActionMenu     *popupCopyMenu_;
    // Contact popup KAction to copy the contact's friendly name to clipboard
    KAction         *popupCopyFriendlyName_;
    // Contact popup KAction to copy the contact's message to clipboard
    KAction         *popupCopyPersonalMessage_;
    // Contact popup KAction to copy the contact's email address to clipboard
    KAction         *popupCopyHandle_;
    // Contact popup KAction to copy links from pm and friendly name
    KAction         *popupCopyLink_;
    // Contact popup KAction to copy contact's media message
    KAction         *popupCopyMusic_;
    // Contact popup KAction to edit notes
    KAction         *popupEditNotes_;
    // Contact popup KAction to send an email to the contact
    KAction         *popupEmailContact_;
    // Contact popup KAction to show the contact's profile
    KAction         *popupMsnProfile_;
    // Contact popup KAction to remove a contact from the contact list
    KAction         *popupRemoveContact_;
    // Contact popup KAction to start a private chat with the contact while the user is in a multi-chat
    KAction         *popupStartPrivateChat_;
    // Contact popup KAction to unblock the contact
    KAction         *popupUnblockContact_;
    // popup actions for the properties menu
    KActionMenu     *popupPropMenu_;
    KAction         *popupPropGeneral_;
    KAction         *popupPropImages_;
    KAction         *popupPropNotes_;
    KAction         *popupPropEmoticons_;
    // A timer to time the duration that the typing label is shown
    QTimer           typingTimer_;

  signals:
    // The user wants to add or remove a contact.
    void               contactAdded( QString handle, bool isAdded );
    // The user wants to allow a contact to see hi/hers online status.
    void               contactAllowed( QString handle );
    // The user wants to block or unblock a contact.
    void               contactBlocked( QString handle, bool isBlocked );
};

#endif
