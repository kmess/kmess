/***************************************************************************
                          chatview.h  -  description
                             -------------------
    begin                : Wed Jan 15 2003
    copyright            : (C) 2003 by Mike K. Bennett
    email                : mkb137b@hotmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CHATVIEW_H
#define CHATVIEW_H

#include "../account.h"
#include "chatmessageview.h"
#include "kmessmessage.h"

// Forward declarations
class QTextDocument;

class ChatMessageStyle;
class ChatMessageView;
class ContactsWidget;
class EmoticonsWidget;
class KMessTest;

namespace Isf { class Drawing; }


/**
 * The view widget of a chat with a contact with message display and entry.
 *
 * @author Mike K. Bennett
 * @ingroup Chat
 */
class ChatView : public QWidget
{
  Q_OBJECT

  friend class KMessTest;

  public:
    // The constructor
                          ChatView( QWidget *parent = 0 );
    // The destructor
    virtual              ~ChatView();
    // Copy the currently selected text
    void                  editCopy();
    // Return a pointer to this chat's Contacts Widget
    ContactsWidget       *getContactsWidget() const;
    // Return a pointer to the ink image drawn by the user in this chat
    Isf::Drawing         *getInkDrawing() const;
    // Return a pointer to the text typed by the user in this chat
    QTextDocument        *getMessageEditContents() const;
    // Get the text zoom factor
    int                   getZoomFactor();
    // Whether or not the message area is empty
    bool                  isEmpty() const;
    // Scroll the view forward or backward
    void                  scrollTo( bool forward, bool fast );
    // Scroll the view down to the last line
    void                  scrollToBottom();
    // Change the text zoom factor
    void                  setZoomFactor( int percentage );
    // Update the messages which contain custom emoticons
    void                  updateCustomEmoticon( const QString &handle, const QString &code );

  public slots:
    // Enable or disable the parts of the chat which allow user interaction
    void                  setEnabled( bool isEnabled );
    // Add the given message to the message browser.
    void                  showMessage( KMessMessage message );
    // Show a dialog to save the chat.
    void                  showSaveChatDialog();
    // The user clicked the "find text" option in the context menu
    void                  slotFindChatText();
    // Save the chat to the given file
    bool                  saveChatToFile( const QString &path, Account::ChatExportFormat format, bool overwriteContents, bool allowUserInteraction = true );
    // Clear the chat's contents
    void                  slotClearChat();

  private: // Private methods
    // Event filter to detect special actions in the message editor.
    bool                  eventFilter( QObject *obj, QEvent *event );
    // Set up the contacts widget
    bool                  initializeContactsWidget();
    // Invite a contact to the chat
    virtual void          inviteContacts( const QStringList &contacts ) = 0;
    // Send a file to a contact.
    virtual void          startFileTransfer( QList<QUrl> fileList = QList<QUrl>() ) = 0;

  private slots: // Private slots
    // Add a contact's email to the contact list
    void                  slotAddContact();
    // Open a dialog to add a new custom emoticon seen in the chat
    void                  slotAddNewEmoticon();
    // The user clicked a kmess internal link in the ChatMessageView
    void                  slotSendAppCommand();
    // The user clicked the "copy address" or "copy email" option in the context menu
    void                  slotCopyAddress();
    // Add an emoticon from the chat to the contact's emoticon blacklist
    void                  slotIgnoreEmoticon();
    // Open a new url clicked in the khtml widget
    void                  slotOpenURLRequest( const KUrl &url );
    // The user right clicked at the KHTMLPart to show a popup.
    void                  slotShowContextMenu(const QString &url, const QPoint &point);
    // The user clicked the "visit address" or "send email" option in the context menu, or clicked a link in the ChatMessageView
    void                  slotVisitAddress();

  protected: // Protected attributes
    // The contacts widget, containing the contacts present in this chat
    ContactsWidget       *contactsWidget_;

  private: // Private attributes
    // The embedded khtml part
    ChatMessageView      *chatMessageView_;
    // URL address which has been right-clicked
    KUrl                  chatViewClickedUrl_;
    // The current ink the user is drawing
    Isf::Drawing         *inkDrawing_;
    // The text typed by the user in this chat
    QTextDocument        *messageEditContents_;

  signals: // Public signals
    // Signal that the user wants to add a new custom emoticon copying it from a friend's
    void                  addEmoticon(QString handle, QString shortcut);
    // Signal that there's an application command
    void                  appCommand(QString cookie, QString contact, QString method);
    // Signal that the user wants to transfer some files
    void                  sendFiles( QList<QUrl> urls );
    // Signal for ChatMessageView to replace an application's accept/reject/cancel links with another text
    void                  updateApplicationMessage( const QString &messageId, const QString &newMessage );
};

#endif
