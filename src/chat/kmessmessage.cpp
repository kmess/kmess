/***************************************************************************
    kmessmessage.h - Message containing KMess-specific data shown
                     in chat views (presence, application, nudge,
                     etc messages).
                             -------------------
    begin                : Fri 18 Dec 2009
    copyright            : 2009 by Adam Goossens
    email                : adam@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "kmessmessage.h"

KMessMessage::KMessMessage()
: TextMessage()
, specificType_( Unknown )
{
}



/**
 * Constructor. Associate with an MsnChat and MsnContact.
 */
KMessMessage::KMessMessage( MessageType type, KMess::MsnChat *chat, const KMess::MsnContact *contact )
: TextMessage( chat, const_cast<KMess::MsnContact*>( contact ) )
, specificType_ ( type )
{
}


/**
 * Copy constructor.
 */
KMessMessage::KMessMessage( const KMessMessage &other )
: TextMessage( other )
, specificType_ ( other.specificType_ )
{
}



/**
 * Copy constructor.
 */
KMessMessage::KMessMessage( const Message &other, MessageType fmt )
: TextMessage( other )
, specificType_ ( fmt )
{
}



/**
 * Return true if this is a "normal" i.e., incoming or outgoing text message.
 *
 * For all other message types (ink, app, system, etc) this returns false.
 *
 */
bool KMessMessage::isNormal() const 
{
  return ( specificType_ == IncomingText || specificType_ == OutgoingText );
}



/**
 * Return the specific type of this message.
 */
KMessMessage::MessageType KMessMessage::specificType() const
{
  return specificType_;
}



/**
 * Set the specific type of this message.
 * @param type Specific type of the message.
 */
void KMessMessage::setSpecificType( MessageType type )
{
  specificType_ = type;
}


