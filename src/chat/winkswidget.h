/***************************************************************************
                          winkswidget.h -  description
                             -------------------
    begin                : Sat Mar 8 2009
    copyright            : (C) 2009 by Antonio Nastasi
    email                : sifcenter (at) gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/


#ifndef WINKSWIDGET_H
#define WINKSWIDGET_H

#include <QBoxLayout>
#include <QListWidget>


// Forward declaration
class QLabel;

namespace KMess
{
  class MsnObject;
}



/**
 * The widget in a chat that contains the winks to send to your contacts.
 *
 * @author Antonio Nastasi
 * @ingroup Chat
 */
class WinksWidget : public QWidget
{
  Q_OBJECT
  public: // public enum
    enum CABEXTRACTOR
    {
      SUCCESS = 0,
      FAILED,
      NOTINSTALLED,
      UNKNOW
    };

  public:
                                       WinksWidget( QWidget *parent );;
                                       ~WinksWidget();
    void                               refresh();
    static WinksWidget::CABEXTRACTOR   getHtmlFromWink( const QString &filename, QString &html );
    const KMess::MsnObject             getMsnObjectWinkSelected();

  private: // Private attributes
    QBoxLayout      *layout_;
    QListWidget     *list_;
    // A warning message to tell the user he/she's got no winks
    QLabel          *noWinksWarning_;
    QString         winksDir_;
};

#endif
