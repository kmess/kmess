/***************************************************************************
                          chatmessagestyle.h -  description
                             -------------------
    begin                : Sat Okt 29 2005
    copyright            : (C) 2005 by Diederik van der Boor
    email                : "vdboor" --at-- "codingdomain.com"
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CHATMESSAGESTYLE_H
#define CHATMESSAGESTYLE_H

#include <KMess/TextMessage>
#include "kmessmessage.h"

#include <QFont>
#include <QString>
#include <QStringList>


class XslTransformation;
namespace KMess
{
  class TextMessage;
}



/**
 * Utility class to convert chat messages to the current style/appearance.
 *
 * @author Diederik van der Boor
 * @ingroup Chat
 */
class ChatMessageStyle : public QObject
{
  Q_OBJECT

  public:  // public methods

    // The constructor
                         ChatMessageStyle();
    // The destructor
    virtual             ~ChatMessageStyle();

    // Return whether XSL conversion works with the current style
    bool                 canConvert() const;
    // Convert a chat message to HTML string
    QString              convertMessage(const KMessMessage *message);
    // Convert a chat message to XML
    QString              convertMessageToXml( const KMessMessage *message, bool isHistory = false );
    // Convert a group of chat message to HTML string
    QString              convertMessageList(const QList<KMessMessage> &messageList);
    // Convert the message root.
    QString              convertMessageRoot();
    // Convert a string with XML to HTML
    QString              convertXmlMessageList( const QString &xmlMessagesList );
    // Return the base folder of the style.
    const QString &      getBaseFolder() const;
    // Return the font used for contact messages, if forced to.
    const QFont&         getContactFont() const;
    // Return the color of the forced contact font.
    const QString&       getContactFontColor() const;
    // Return the css file attached to the stylesheet. Return null if there is none.
    QString              getCssFile() const;
    // Return the currently used emoticon style
    const QString       &getEmoticonStyle() const;
    // Return the grow follow up messages
    bool                 getGroupFollowupMessages() const;
    // Return the name of the style.
    const QString &      getName() const;
    // Return if the date is displayed in messages
    bool                 getShowMessageDate() const;
    // Return if the seconds are displayed in the message times
    bool                 getShowMessageSeconds() const;
    // Return if the time is displayed in messages
    bool                 getShowMessageTime() const;
    // Return the ID's of inserted <img> tags for the pending emoticons.
    const QStringList &  getPendingEmoticonTagIds() const;
    // Return the xsl stylesheet. Return null if there is none.
    QString              getStyleSheet() const;
    // Return whether or not to show text formatting in chat messages
    bool                 getUseChatFormatting() const;
    // Return whether or not to show contact messages in the stored font.
    bool                 getUseContactFont() const;
    // Return whether or not the emoticons are visible
    bool                 getUseEmoticons() const;
    // Return whether or not the font effects are visible
    bool                 getUseFontEffects() const;
    // Return whether or not the formatting is visible
    bool                 getUseFormatting() const;
    // Return whether or not emoticon links may be inserted
    bool                 getAllowEmoticonLinks() const;
    // Return whether an style is loaded.
    bool                 hasStyle() const;

  public slots:
    // The the contact font
    void                 setContactFont(const QFont &font);
    // The the contact font color
    void                 setContactFontColor(const QString &fontColor);
    // Set the emoticon style
    void                 setEmoticonStyle( const QString &style );
    // Set grow follow up messages
    void                 setGroupFollowupMessages( bool groupFollowupMessages );
    // Set the show time state
    void                 setShowTime(bool showTime);
    // Sets whether the date should be shown in the timestamp
    void                 setShowDate(bool showDate);
    // Sets whether the seconds should be shown in the timestamp
    void                 setShowSeconds(bool showSeconds);
    // Set the message style, return false if it failed
    bool                 setStyle(const QString &style);
    // Enable or disable contact font overrides
    void                 setUseContactFont(bool useContactFont);
    // Enable or disable emoticons
    void                 setUseEmoticons(bool useEmoticons);
    // Enable or disable font effects
    void                 setUseFontEffects(bool useFontEffects);
    // Enable or disable MSN Plus formatting
    void                 setUseFormatting( bool useFormatting );
    // Enable or disable adding emoticon links
    void                 setAllowEmoticonLinks( bool allowEmoticonLinks );


  private:  // private methods

    // Convert the message as HTML, fallback method when XML fails.
    QString              createFallbackMessage(const KMessMessage *message);
    // Return whether the given result is empty
    bool                 isEmptyResult( const QString &parsedMessage );
    // Replace the newline characters
    void                 parseBody(QString &body) const;
    // Do some effects characters (ie, bold, underline and italic specials)
    void                 parseEffects(QString &text) const;
    // Parse the font tags.
    void                 parseFont(const KMessMessage *message, QFont &font, QString &color, QString& fontBefore, QString& fontAfter) const;
    // Strip the DOCTYPE tag from the message
    QString              stripDoctype( const QString &parsedMessage );


  private:   // private properties
    // The base folder of the style
    QString              baseFolder_;
    // Whether the XSL converter is working
    bool                 canConvert_;
    // The contact font
    QFont                contactFont_;
    // The contact font color
    QString              contactFontColor_;
    // Current emoticon style
    QString              emoticonStyle_;
    // Group follow up messages
    bool                 groupFollowupMessages_;
    // The name of the style
    QString              name_;
    // A list of IDs of pending emoticons
    QStringList          pendingEmoticonTags_;
    // Whether the time should be displayed
    bool                 showTime_;
    // Whether the date should be displayed
    bool                 showDate_;
    // Whether the seconds should be displayed
    bool                 showSeconds_;
    // Whether the contact font should be used
    bool                 useContactFont_;
    // Whether emoticons should be used
    bool                 useEmoticons_;
    // Whether font effects should be used
    bool                useFontEffects_;
    // Whether MSN Plus formatting should be used
    bool                useFormatting_;
    // If false, will instruct the rich text parser to never allow emoticon links
    bool                allowEmoticonLinks_;
    // The XSL transformation
    XslTransformation   *xslTransformation_;
};


#endif
