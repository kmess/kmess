/***************************************************************************
                          chatwindow.h  -  description
                             -------------------
    begin                : Tue Apr 23 2002
    copyright            : (C) 2002 by Mike K. Bennett
    email                : mkb137b@hotmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CHATWINDOW_H
#define CHATWINDOW_H

#include "ui_chatwindow.h"
#include "chatstatusbar.h"

#include <QList>
#include <QTimer>

#include <KAction>
#include <KActionCollection>
#include <KToggleAction>
#include <KXmlGuiWindow>


// forward declarations
class Chat;
class ChatView;
class EmoticonsWidget;
class MsnSwitchboardConnection;

class QLabel;

class KActionMenu;
class KConfigGroup;
class KHelpMenu;
class KMenu;
class KTabBar;
class KToolBarButton;
class KToolBarPopupAction;

namespace KMess
{
  class TextMessage;
}


/**
 * The interface for the chat window, defining the menus.
 *
 * @author Mike K. Bennett
 * @ingroup Chat
 */
class ChatWindow : public KXmlGuiWindow, private Ui::ChatWindow
{
  Q_OBJECT

  friend class KMessTest;

  public:
    enum UIState
    {
      Disconnected,
      Connecting,
      Connected,
      Disconnecting
    };

  public: // Public methods
    // The constructor
                     ChatWindow( QWidget *parent=0 );
    // The destructor
    virtual         ~ChatWindow();
    // Add a new chat tab
    Chat            *addChatTab( Chat *newChat, bool foreground );
    // Return a reference to a chat
    Chat            *getChat( int index = -1 );
    // Return the references to chats
    QList<Chat*>     getChats();
    // Return a reference to the active chat
    Chat            *getCurrentChat();
    // Return a reference to the chat message editor.
    KTextEdit       *getMessageEditor();
    // Initialize the object
    bool             initialize();
    // The chat window is closing, called by KMainWindow
    bool             queryClose();
    // The application is exiting, called by KMainWindow
    bool             queryExit();
    // Close a chat tab without user intervention
    void             removeChatTab( Chat *chat );
    // Change the active chat
    void             setCurrentChat( Chat *chat );
    // Enable or disable the parts of the window which allow user interaction
    void             setUIState( UIState state );
    // Show a status message.
    void             showStatusMessage( ChatStatusBar::MessageType type, QString message );
    // Check if the window must be closed
    bool             checkAndCloseWindow();

  private: // Private methods
    // Set the zoom factor of the text
    void             changeZoomFactor( bool increase );
    // Filter to catch window activation and keyboard shortcut events
    bool             eventFilter( QObject *obj, QEvent *event );
    // Set up the preferences which depend on the currently logged in account
    bool             initializeCurrentAccount();
    // Create the menus.
    void             createMenus();
    // Restore the window properties (called by KMainWindow)
    void             readProperties( const KConfigGroup &config = *((const KConfigGroup *)0) );
    // Save the window properties (called by KMainWindow)
    void             saveProperties( KConfigGroup &config = *((KConfigGroup *)0) );
    // Enable or disable the parts of the window which allow user interaction
    void             setEnabled( bool isEnabled );
    // Set the window icon for this chat window
    void             setChatWindowIcon();

  public slots:
    // Show a status message temporarily in the status dialog
    void             showStatusMessage( QString message = QString(), int duration = 5 );

  private slots: // Private slots
    // All tabs must be closed
    bool             closeAllTabs();
    // A tab must be closed
    bool             closeTab();
    // Close any open widget or close the tab
    void             closeWidgetOrTab();
    // Copy the marked text/object into the clipboard
    void             editCopy();
    // Move the marked text/object from the document into the clipboard
    void             editCut();
    // Bring up a find dialog to search for text in the chat.
    void             editFind();
    // Bring up a dialog to change the message font color.
    void             editFont();
    // Bring up a dialog to change the message font color.
    void             editFontColor();
    // The color in the text box changed.
    void             editorColorChanged( const QColor &color );
    // paste the clipboard into the document
    void             editPaste();
    // Insert an emoticon in chat
    void             insertEmoticon( const QString &shortcut );
    // Save the chat according to the user's request
    void             saveChat();
    // Send a ink via server
    void             sendInk();
    // Send a message via the server
    void             sendMessage();
    // The 'Send nudge' button has been clicked
    void             sendNudge();
    // Change the caption of the chat window
    void             setWindowTitle( const QString &caption = QString() );
    // "Show menu bar" was toggled.
    void             showMenuBar();
    // Make the caption blink if the window still doesn't have focus
    void             slotBlinkCaption();
    // Change ink brush color
    void             slotChangeInkColor();
    // Change ink brush.
    void             slotChangeInkBrush();
    // Called when an emoticon dock is toggled
    void             slotEmoticonDocksToggled();
    // A message has been received
    void             slotGotChatMessage( KMess::TextMessage &message, Chat *chat );
    // A nudge has been received: shake the window.
    void             slotGotNudge();
    // Update the status label text
    void             slotGotTypingMessage( Chat *chat = 0 );
    // Invite button was pressed
    void             slotInviteContact();
    // The message text changed, the user is typing
    void             slotMessageChanged();
    // The user clicked the new line button so insert a new line in the editor
    void             slotNewLineButtonClicked();
    // Change the info for tab
    void             slotUpdateChatInfo( Chat *chat = 0 );
    // Switch the window to the tab at the left of the current one
    void             slotSwitchToLeftTab();
    // Switch the window to the tab at the right of the current one
    void             slotSwitchToRightTab();
    // The user pressed return in the message editor, so send the message
    void             slotSendButtonClicked();
    // Switch to the message editor, or another one
    void             slotSwitchEditor();
    // The active tab was changed
    void             slotTabChanged( int currentIndex );
    // Remove a chat
    bool             slotTabRemoved( Chat* chat, bool force = true );
    // Delete an existing chat tab by widget
    bool             slotTabRemoved( QWidget* chatWidget, bool force = true );
    // Update the editor's font to match the account's font
    void             updateEditorFont();
    // Start a voice conversation
    void             startConversation();
    // Start a file transfer
    void             startFileTransfer();
    // Start GnomeMeeting with a contact.
    void             startMeeting();
    // Start a webcam transfer
    void             startWebcamTransfer();
    // Called when the "use emoticons" action is called.
    void             toggleEmoticons( bool useEmoticons );
    // Called when the "show session messages" action is called.
    void             toggleSessionInfo( bool showSessionMessages );
    // Called when the "use spell checking" action is called.
    void             toggleSpellCheck( bool useSpellCheck );
    // Update the status of the tiny writing mode buttons
    void             updateModeButtons();
    // Clean up the view of the current chat tab
    void             viewClearChat();
    // enlarge the text size
    void             viewZoomIn();
    // decrease the text size
    void             viewZoomOut();

  private: // private attributes
    // ActionCollection
    KActionCollection *actionCollection_;
    // The add emoticon action
    KAction         *addEmoticonAction_;
    // A timer used to make the caption blink on new messages.
    QTimer           blinkTimer_;
    // Whether to blink to upper (true) or lower (false) case
    bool             blinkToUpper_;
    // The stored window caption
    QString          caption_;
    // Action to change own text font
    KAction         *changeFontAction_;
    // Action to change own text color
    KAction         *changeFontColorAction_;
    // Action to close the window along with all of its tabs
    KAction         *closeAllAction_;
    // Contacts list Dock Widget
    QDockWidget     *contactsDock_;
    // Custom emoticons Dock Widget
    QDockWidget     *customEmoticonsDock_;
    // Action to cut text to the clipboard
    KAction         *cutAction_;
    // Whether or not typing messages should be sent (true except the very moment a text message is sent)
    bool             doSendTypingMessages_;
    // The emoticon display toggling action
    KAction         *emoticonAction_;
    // The first item in the Chat Menu (used to add the Invite menu before it)
    QAction         *firstChatMenuItem_;
    // Index for quick-retype
    int              indexSentences_;
    // Whether or not the object was initialized
    bool             initialized_;
    // The contact invite action
    KAction         *inviteButton_;
    // Last sentence in writing
    QString          lastSentence_;
    // The last time the window was shaked
    QTime            lastShake_;
    // Action to open a meeting
    KAction         *meetingAction_;
    // Action to switch to the next tab
    KAction         *nextTabAction_;
    // Action to send a nudge
    KAction         *nudgeAction_;
    // Action menu containing the panels shortcuts
    KActionMenu     *panelsMenuAction_;
    // Action to paste the clipboard contents
    KAction         *pasteAction_;
    // Action to switch to the previous tab
    KAction         *prevTabAction_;
    // Action to start a file transfer
    KAction         *sendAction_;
    // Toggle actions for showing and hiding the menu bar
    KToggleAction   *showMenuBar_;
    // Toggle action to show/hide the join and part messages
    KToggleAction   *sessionInfoAction_;
    // Action to toggle spell checking in the message editor
    KToggleAction   *spellCheckAction_;
    // Standard emoticons Dock Widget
    QDockWidget     *standardEmoticonsDock_;
    // The status label
    ChatStatusBar   *statusLabel_;
    // A timer to time the duration that a status message is shown
    QTimer           statusTimer_;
    // A timer to time whether a "user is typing" message should be sent.
    QTimer           userTypingTimer_;
    // Action to start a webcam transfer
    KAction         *webcamAction_;
    // Stores the current level of zoom
    int              zoomLevel_;
    // Store the Gui state
    UIState          uiState_;

  signals: // Public signals
    // Signal that the window is about to close.
    void               closing( QObject *object );
    // Signal that a message is about to be sent.
    void               messageAboutToSend( QString message );
    // Reconnect
    void               reconnect();
    // Signal that the user is typing
    void               userIsTyping();
};
#endif
