/***************************************************************************
                          contactswidget.h  -  description
                             -------------------
    begin                : Thu Jan 16 2003
    copyright            : (C) 2003 by Mike K. Bennett
    email                : mkb137b@hotmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CONTACTSWIDGET_H
#define CONTACTSWIDGET_H

#include "contactframe.h"

#include "ui_contactswidget.h"

#include <QList>
#include <QScrollArea>


// Forward declarations
namespace KMess
{
  class MsnContact;
};

class ContactList;

class QBoxLayout;
class QDockWidget;



/**
 * The widget in a chat that contains the little contact information frames.
 *
 * @author Mike K. Bennett
 * @ingroup Chat
 */
class ContactsWidget : public QWidget, private Ui::ContactsWidget
{
  Q_OBJECT

  public:
    // The constructor
                               ContactsWidget( QWidget *parent = 0 );
    // The destructor
    virtual                   ~ContactsWidget();
    // A message was received from one of the contacts... notify its frame
    void                       messageReceived(const QString& handle);
    // Enable/disable the frames
    void                       setEnabled( bool isEnabled );
    // Connect the widget to a dock
    void                       setDockWidget( QDockWidget *dockWidget, Qt::DockWidgetArea initialArea );

  public slots: // Public slots
    // A contact was added to the contact list
    void                       contactAdded( KMess::MsnContact *contact );
    // A contact joined the chat
    void                       contactJoined( KMess::MsnContact *contact );
    // A contact left the chat
    void                       contactLeft  ( KMess::MsnContact *contact, bool isChatIdle );
    // A contact was removed from the contact list
    void                       contactRemoved( KMess::MsnContact *contact );
    // A contact is typing
    void                       contactTyping( KMess::MsnContact  *contact );

  private slots:
    // The location of the parent dock widget has changed
    void                       slotLocationChanged( Qt::DockWidgetArea area );
    // The parent dock widget floating status has changed
    void                       slotTopLevelChanged( bool isTopLevel );
    // Update the user's display picture
    void                       slotUpdateDisplayPicture();
    // Open the account settings
    void                       slotUserDisplayPictureClicked();

  private: // Private methods
    // Return one new contact frame
    ContactFrame*              createContactFrame();
    // Get the most suitable frame size, depending on how many contacts are currently in chat with us
    ContactFrame::DisplayMode  getBestFrameMode();
    // Find the contact frame with the given handle
    ContactFrame*              getContactFrame(const QString& handle);

  private: // Private attributes
    // The list of frames contained in the widget
    QList<ContactFrame *>      contactFrames_;
    // the layout to put contact frames into
    QBoxLayout                *layout_;
    // Whether or not the widget was visible before being moved
    bool                       wasVisible_;
};

#endif
