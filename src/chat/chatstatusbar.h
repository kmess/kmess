/***************************************************************************
                          chatstatusbar.h -  description
                             -------------------
    begin                : Wed Okt 8 2008
    copyright            : (C) 2008 by Ruben Vandamme
    email                : ruben@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/



#ifndef STATUSBAR_H
#define STATUSBAR_H

#include <QWidget>

class QPaintEvent;
class QResizeEvent;
class QPushButton;
class QTimer;



class ChatStatusBar: public QWidget
{
  Q_OBJECT

  public:
    // Types of messages
    enum MessageType
    {
      DefaultType,
      ContactTyping,
      Disconnected
    };

  public:
    // The constructor
    explicit             ChatStatusBar( QWidget *parent=0 );
    // The destructor
    virtual             ~ChatStatusBar();
    // Returns the text in the statusbar.
    const QString       &text() const;
    // Returns the type of the status message.
    MessageType          type() const;
    // Returns the minimum text height.
    int                  minimumTextHeight() const;
    // Enable or disable the button for the specified message type.
    void                 setButtonEnabled( MessageType type, bool enable );
    // Shows a message in the status label.
    void                 setMessage( MessageType type, const QString &text );
    // Set the minimum text height.
    void                 setMinimumTextHeight( int min );
    // Resets the message label properties.
    void                 reset();

  protected:
    // Paint the statusbar.
    virtual void         paintEvent ( QPaintEvent  *event);
    // Statusbar is resized, update it.
    virtual void         resizeEvent( QResizeEvent *event);

  private slots:
    // Update the background color.
    void                 timerDone();
    // Increases the height of the message label so that the given text fits into given area.
    void                 assureVisibleText();
    // Returns the available width in pixels for the text.
    int                  availableTextWidth() const;
    // Moves the close button to the upper right corner of the message label.
    void                 updateCloseButtonPosition();
    // User requested an action.
    void                 buttonClicked();

  private: // Private attributes
    // States for background animation
    enum State
    {
      DefaultState,
      Illuminate,
      Illuminated,
      Desaturate
    };

    // Why enums?
    enum { GeometryTimeout = 100 };
    enum { BorderGap = 2 };

    // Type of current message.
    MessageType          type_;
    // Current state in background animation.
    State                state_;
    // Current background illumination.
    int                  illumination_;
    // Minimal text height
    int                  minTextHeight_;
    // Timer used for background animation
    QTimer              *timer_;
    // Current text
    QString              text_;
    // Current icon
    QPixmap              pixmap_;
    // Button for user action.
    QPushButton         *button_;

  signals: // Public signals
    void                 reconnect();
};



#endif
