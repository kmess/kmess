/***************************************************************************
                          chat.cpp  -  description
                             -------------------
    begin                : Wed Jan 15 22:41:32 CST 2003
    copyright            : (C) 2003 by Mike K. Bennett
    email                : mkb137b@hotmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "chat.h"
#include "kmessmessage.h"

#include <KMess/MsnChat>
#include <KMess/MsnContact>
#include <KMess/TextMessage>
#include <KMess/InkMessage>
#include <KMess/WinkMessage>
#include <KMess/TypingMessage>
#include <KMess/NudgeMessage>
#include <KMess/MsnObject>
#include <KMess/Message>
#include <KMess/YahooMessage>
#include <KMess/NetworkGlobals>

#include <IsfQt>
#include <IsfQtDrawing>

#include "../contact/contact.h"
#include "../contact/contactextension.h"

#include "../utils/kmessconfig.h"
#include "../utils/kmessshared.h"
#include "../account.h"
#include "../kmessglobal.h"
#include "../kmessapplication.h"
#include "../kmessdebug.h"
#include "chatwindow.h"
#include "contactswidget.h"

#include <QDir>
#include <QFile>
#include <QRegExp>
#include <QBuffer>

#include <KFileDialog>
#include <KIO/NetAccess>
#include <KMessageBox>



#ifdef KMESSDEBUG_CHAT
  #define KMESSDEBUG_CHAT_GENERAL
  #define KMESSDEBUG_CHAT_CONTACTS
  #define KMESSDEBUG_CHAT_FILETRANSFER
//   #define KMESSDEBUG_CHAT_TYPING_MESSAGES
#endif



// The constructor
Chat::Chat( QWidget *parent )
: ChatView( parent )
, firstMessage_( true )
, isClosing_(false)
, msnChat_(0)
{
  hide();

  // Setup the contact typing information timer
  typingTimer_.setSingleShot( true );

  connect( &typingTimer_, SIGNAL(       timeout() ),
           this,          SLOT  ( contactTyping() ) );

  // Connect the contacts widget's signals
  connect( globalSession,     SIGNAL(      contactAdded(KMess::MsnContact*)     ),
           contactsWidget_,   SLOT  (      contactAdded(KMess::MsnContact*)     ) );

  // Save the date/time the chat started.
  // Use it to save a chatlog later.
  startDateTime_ = QDateTime::currentDateTime();

  // Update the chat window caption with the contact name
 // emit chatInfoChanged();
}



// The destructor
Chat::~Chat()
{
#ifdef KMESSDEBUG_CHAT_GENERAL
  kmDebug() << "DESTROYED.";
#endif
}



// Choose the contact to start an invitation with.
const QString Chat::chooseContact()
{
  // TODO fix properly
  // there's pendingInvitations_ in SwitchboardConnection
  // move to MsnChat and this will be possible
  if( !msnChat_->isConnected() ) return QString();

  // Choose the contact.
  const ContactsList contactsInChat( msnChat_->participants() );
  switch( contactsInChat.count() )
  {
    case 0:
      // No contacts in the chat
      if( ! msnChat_->lastContact() || msnChat_->lastContact()->isUnknown() )
      {
        // Send invitation to the first contact that should appear in the chat.
        return msnChat_->firstContact()->handle();
      }
      else
      {
        // Resume chat with last contact
        return msnChat_->lastContact()->handle();
      }

    case 1:
      // Choose the only contact available in the chat
      return contactsInChat.first()->handle();

    default:
      // Multiple contacts in the chat.
      // TODO: The official client opens a contact-choose dialog here, and starts a new chat with the selected contact.
      KMessageBox::sorry( this, i18nc( "Error dialog box text",
                                       "You cannot send invitations when there are multiple contacts in a chat. "
                                       "Please start a separate chat with the contact you wanted to send the invitation to." ) );
      return QString();
  }
}



// A contact joined the chat
void Chat::contactJoined( KMess::MsnContact *msnContact )
{
  Contact contact( msnContact );
  const QString&     friendlyName( contact->getFriendlyName( STRING_CLEANED_ESCAPED ) );

  // Don't show the message when the contact went/is offline (because in such cases, the message
  // is unreliable, and if we started the chat we're just pretending the contact joined)
  if( ! msnContact->isOffline() && ( Account::connectedAccount->getSettingBool( "ChatSessionMessagesEnabled" ) || getMsnChat()->participants().count() > 1 ) )
  {
    KMessMessage presence( KMessMessage::Presence, getMsnChat(), msnContact );
    presence.setMessage( i18n( "%1 has joined the chat.", friendlyName ) );
    showMessage( presence );
  }

  // Change the chat title
  emit chatInfoChanged();

  // Allow future changes in the nickname and status to update the chat informations
  if( msnContact != 0 )
  {
    // disconnect any old signals, just to be sure
    disconnect( msnContact, 0, this, 0 );
    disconnect( contact, 0, this, 0 );

    connect( contact, SIGNAL(           contactOffline() ),
             this,    SLOT  ( slotContactChangedStatus() ) );

    connect( contact, SIGNAL(            contactOnline() ),
             this,    SLOT  ( slotContactChangedStatus() ) );

    connect( contact, SIGNAL(            statusChanged() ),
             this,    SLOT  ( slotContactChangedStatus() ) );

    connect( contact, SIGNAL(            statusChanged() ),
             this,    SIGNAL(          chatInfoChanged() ) );

    connect( contact, SIGNAL(       displayNameChanged() ),
             this,    SIGNAL(          chatInfoChanged() ) );
  }
}



// A contact left the chat
void Chat::contactLeft( KMess::MsnContact *contact, bool isChatIdle )
{
  const QString&     handle      ( contact->handle() );
  const ContactsList participants( getMsnChat()->participants() );

#ifdef KMESSDEBUG_CHAT_GENERAL
  kmDebug() << "contact '" << handle << "' has left" << (isChatIdle ? " due to inactivity" : "") << ".";
#endif

  // A contact has left the chat.
  // Group chats: it gets disconnected completely from it
  // 1-on-1 chats: the chat UI will still show the contact, so don't disconnect its update signals
  if( participants.count() > 1 )
  {
     disconnect( Contact ( contact ), 0, this, 0 );
  }

  // Always show a message when this is a group chat,
  // but never if it would be the first message.
  if( ! firstMessage_ && ( Account::connectedAccount->getSettingBool( "ChatSessionMessagesEnabled" ) || participants.count() > 1 ) )
  {
    const QString& contactName( Contact ( contact )->getFriendlyName( STRING_CLEANED_ESCAPED ) );
    QString message;

    if( isChatIdle )
    {
      message = i18nc( "Message shown in chat, %1 is the contact's friendly name",
                       "The chat went idle, %1 has left it.",
                       contactName );
    }
    else
    {
      message = i18nc( "Message shown in chat, %1 is the contact's friendly name",
                       "%1 has left the chat.",
                       contactName );
    }

    KMessMessage presence( KMessMessage::Presence, getMsnChat(), contact );
    presence.setMessage( message );

    showMessage( presence );
  }

  // Change the caption to remove the contact who has left
  emit chatInfoChanged();
}



// A contact is typing
// If called without arguments, it will only update the list of typing contacts.
void Chat::contactTyping( KMess::MsnContact *contact, bool forceExpiration )
{
  int secsTo;
  int nextUpdate = CHAT_TYPING_EXPIRATION_TIME; // Allow a maximum number of seconds between each update
  QString handle;
  QString friendlyName;

  if( contact != 0 )
  {
    handle       = contact->handle();
    friendlyName = Contact( contact )->getFriendlyName( STRING_CLEANED );

    if( friendlyName.isEmpty() ) // If the contact has no friendlyName, use its handle as name
    {
      friendlyName = handle;
    }
  }

#ifdef KMESSDEBUG_CHAT_TYPING_MESSAGES
  kmDebug() << "Called with forceExpiration=" << forceExpiration << ", handle=" << handle << ", friendlyName=" << friendlyName;
  kmDebug() << "Current list:" << typingContactsNames_.keys();
#endif

  // Do not add anything to the list if the friendly name is empty
  if( ! handle.isEmpty() )
  {
    QTime startTime = QTime::currentTime();

    // If a contact is still typing, re-insert it in the list, so that the typing start time is updated
    if( typingContactsNames_.contains( handle ) )
    {
#ifdef KMESSDEBUG_CHAT_TYPING_MESSAGES
      kmDebug() << "Removing" << handle << "from the list.";
#endif
      typingContactsNames_.remove( handle );
      typingContactsTimes_.remove( handle );
    }

    // When we receive a message from this contact, we only remove it from the typing contacts
    if( ! forceExpiration )
    {
#ifdef KMESSDEBUG_CHAT_TYPING_MESSAGES
      kmDebug() << "Adding" << handle << "to the list.";
#endif
      // Add this contact to the list of contacts which are typing, and record when it has started writing
      startTime.start();
      typingContactsNames_.insert( handle, friendlyName );
      typingContactsTimes_.insert( handle, startTime    );
    }
#ifdef KMESSDEBUG_CHAT_TYPING_MESSAGES
    else
    {
      kmDebug() << "Removal from the list was forced.";
    }
#endif
  }

  // Update the list to remove expired typing events
  QMutableHashIterator<QString,QTime> it( typingContactsTimes_ );
  while( it.hasNext() )
  {
    it.next();
    secsTo = (int) ( it.value().elapsed() * .001 ); // Only keep the seconds

    // If the typing event has expired, stop displaying it
    if( secsTo >= CHAT_TYPING_EXPIRATION_TIME )
    {
#ifdef KMESSDEBUG_CHAT_TYPING_MESSAGES
      kmDebug() << "Removing" << handle << "from the list due to expiration.";
#endif
      typingContactsNames_.remove( it.key() );
      typingContactsTimes_.remove( it.key() );
    }

    // Find the typing event which will expire first, so we can next update when it expires
    if( secsTo && secsTo < nextUpdate )
    {
      nextUpdate = secsTo;
    }
  }

#ifdef KMESSDEBUG_CHAT_TYPING_MESSAGES
  kmDebug() << "List after cleanup and update:" << typingContactsNames_.keys();
#endif

  // The more time passes from the last update, the more we get close to the next update
  if( nextUpdate < CHAT_TYPING_EXPIRATION_TIME )
  {
    nextUpdate = CHAT_TYPING_EXPIRATION_TIME - nextUpdate;
  }

#ifdef KMESSDEBUG_CHAT_TYPING_MESSAGES
  kmDebug() << "Next update in" << nextUpdate << "seconds.";
#endif

  // Update the user interface
  if( typingContactsNames_.contains( handle ) )
  {
    contactsWidget_->contactTyping( contact );
  }
  else
  {
    contactsWidget_->messageReceived( handle );
  }

#ifdef KMESSDEBUG_CHAT_TYPING_MESSAGES
  kmDebug() << "emitting signal from:" << this;
#endif

  // Let the other window components update, too
  emit gotTypingMessage( this );

  // Activate the timer for the next update if needed
  if( ! typingContactsTimes_.isEmpty() )
  {
    typingTimer_.start( nextUpdate * 1000 );
  }
}



/**
 * Called once we have done higher-level processing on an incoming message.
 *
 * Here, update contact-extension details, show it in the ChatMessageView
 * and send the auto-away reply if needed. Reset the typing notification timer and
 * emit a signal so the user receives a notification.
 *
 * @param message The Message to display in the chat window.
 */
void Chat::displayIncomingMessage( KMessMessage &message )
{
#ifdef KMESSDEBUG_CHAT_GENERAL
  kmDebug() << "Displaying message, internal type=" << message.type();
  kmDebug() << "First message = " << firstMessage_ << ".";
#endif

  showMessage( message );

  // Update the date of the last received message. Only allowed for contacts on the contact list.
  Contact sourceContact ( globalSession->contactList()->contact( message.peer()->handle() ) );
  if( sourceContact.isValid() && sourceContact->getExtension() )
  {
    sourceContact->getExtension()->setLastMessageDate();
  }

  // Check if we should send an automatic reply to the chat message
  if( globalSession           != 0
  &&  globalSession->sessionSettingBool( "AutoReplyEnabled" )
  && ( lastSentAutoMessage_.isNull() || lastSentAutoMessage_.elapsed() > 120000 ) )
  {
    // Send an auto away message every two minutes (if the contact keeps writing)

#ifdef KMESSDEBUG_CHAT_GENERAL
    kmDebug() << "Sending auto reply to chat message.";
#endif

    QString awayMessage( i18nc( "Automatic reply message",
                                "%1 (This message was sent automatically)",
                                Account::connectedAccount->getSettingString( "AutoReplyMessage" ) ) );

    // Send the autoreply message to the contact(s)
    KMess::TextMessage msg;
    msnChat_->sendMessage( awayMessage, &msg );

    showMessage( KMessMessage( msg ) );

    // Save the time the message was sent, to display the next after some time
    lastSentAutoMessage_.start();
  }

  // Remove this contact from the list of typing messages and let the parent window to update
  // this chat's typing information
  contactTyping( sourceContact, true );

#warning XXX HACK
  // In earlier versions of KMess, custom emoticons were held by the Contact object after they
  // were received. This was incorrect, as custom emoticons are sent per-message, and should be
  // related to the next message only. In KMess Master (2.1 / 3.0) this was corrected and the
  // emoticons are now emitted by the library along with the message they belong to. However,
  // in the frontend this has not been fixed in the RichTextParser etc. yet, so we currently
  // still drop all emoticons in the relevant Contact objects. TODO: fix this properly.
  const MsnObjectMap &emoticons = message.emoticons();

  MsnObjectMapIterator i(emoticons);
  while(i.hasNext())
  {
    i.next();
    sourceContact->addEmoticonDefinition(i.key(), i.value());
  }

  // Emit the new message signal so that the user will receive a notification
  emit gotChatMessage( message, this );

  // If this was the first received message, clear the flag
  if( firstMessage_ )
  {
    firstMessage_ = false;
  }
}



// Return a list of the contacts in the chat to be used as window caption
const QString Chat::getCaption()
{
  // Grep the current participants or the last participant
  const ContactsList &participants( getMsnChat()->participants() );
  QString caption;

  // How many people are there in here?
  switch( participants.count() )
  {
    // The chat is completely empty - it is not even a chat!
    case 0:
      break;

    // One contact is connected; or the only contact of the session has left
    case 1:
      caption = Contact( participants[0] )->getFriendlyName( STRING_CLEANED );
      break;

    // Two contacts are connected
    case 2:
      caption = i18nc( "Name of a chat tab", "%1 and %2",
                       Contact( participants[0] )->getFriendlyName( STRING_CLEANED ),
                       Contact( participants[1] )->getFriendlyName( STRING_CLEANED )
                     );
      break;

    // Three or more contacts are connected
    default:
      // TODO: here as well.
      caption =  i18nc( "Name of a chat tab", "%1 et al.",
                        Contact( participants[0] )->getFriendlyName( STRING_CLEANED )
                      );
  }

  // Replace the 'return chars' with 'space chars'
  return caption.replace( "\n", " " );
}



// Return the chat window which currently contains this Chat
ChatWindow *Chat::getChatWindow()
{
  return qobject_cast<ChatWindow*>( window() );
}



/**
 * Return the associated MsnChat object.
 */
KMess::MsnChat *Chat::getMsnChat() const
{
  return msnChat_;
}



// Return the icon to use in the tab icon chat
KIcon Chat::getParticipantsTabIcon()
{
  const ContactsList &participants( getMsnChat()->participants() );

  // If there are more participants use group icon
  if(  participants.count() != 1 )
  {
    return KIcon( "system-users" );
  }

  // Else use the status icon of the current contact
  const KMess::MsnContact *contact = participants.first();

  // Find the contact, if it isn't in the contact list, use the "unknown" tab icon
  if( contact->isUnknown() )
  {
    return KIcon( "view-media-artist" );
  }

  return KIcon( Status::getIcon( contact->status() ) );
}



// Return when the last message came in, if any
QDateTime Chat::getLastReceivedMessageTime() const
{
  return lastReceivedMessage_;
}



// Return the list of previously sent messages
QStringList& Chat::getQuickRetypeList()
{
  return quickRetypeList_;
}



// Return the list of contacts which are typing
const QStringList Chat::getTypingContacts() const
{
  return typingContactsNames_.values();
}



// Return the date and time the chat has started
QDateTime Chat::getStartTime() const
{
  return startDateTime_;
}



// Invite a contact to the chat
void Chat::inviteContacts( const QStringList &contacts )
{
  // Invite each selected contact, if it's not in chat already
  foreach( const QString &handle, contacts )
  {
    if( ! isContactInChat( handle, false ) )
    {
      msnChat_->inviteContact( handle );
    }
  }
}



// Return whether or not the chat is at its first incoming message
bool Chat::isChatFirstMessage()
{
  return firstMessage_;
}


// Return whether or not the chat is closing
bool Chat::isClosing()
{
  return isClosing_;
}



// Return whether or not is the chat currently visible in the chat window
bool Chat::isCurrentChat()
{
  return this == getChatWindow()->getCurrentChat();
}



// Return whether or not the contact is in this chat.
bool Chat::isContactInChat( const QString &handle, bool isExclusiveChatWithContact )
{
  return ( isExclusiveChatWithContact ? msnChat_->isPrivateChatWith( handle ) : msnChat_->isContactInChat( handle ) );
}



// The chat is closing
void Chat::queryClose()
{
#ifdef KMESSDEBUG_CHAT_GENERAL
  kmDebug() << "Saving chat log";
#endif

  // Notify ChatMaster
  isClosing_ = true;
  emit closing( this );

  // Save the chat
  saveChatAutomatically();

  /**
   * TODO: Uncomment this part: it needs a _reliable_ method to check whether there are running file
   * transfers. The commented part uses "ApplicationList::isEmpty()" which I've removed, the new
   * method to check will probably be elsewhere. The main code of that method was this:
   * <code>
      // check if there are active applications
      if ( mimeApplications_.count() +
          p2pApplications_.count() -
          abortingApplications_.count() == 0)
        return true; else return false;
   * </code>
   */
  /*
  // If there are active applications show a warning to the user
  if( participants_.count() == 1 ) // Transfers are not possibile in multiple chats
  {
    // Check if there are active applications
    Contact contact ( participants_.first() );
    if( contact != 0 && contact->getApplicationList() != 0
        && ! contact->getApplicationList()->isEmpty() )
    {
      int choice = KMessageBox::warningContinueCancel( this, i18n("Continue closing the chat window?\nActive transfers will be aborted!"));
      if( choice == KMessageBox::Cancel )
      {
        return false;
      }
    }
  }
  */
}



// The application is exiting
void Chat::queryExit()
{
#ifdef KMESSDEBUG_CHAT_GENERAL
  kmDebug() << "Saving chat log";
#endif

  // Just save the chat before going down
  saveChatAutomatically();
}



/**
 * Received an Ink message. We need to validate it,
 * if necessary get libisf to render it and then generate the HTML
 * to display it.
 *
 * @param message The InkMessage to handle.
 */
void Chat::receivedInkMessage( KMess::InkMessage message )
{
  // Validate the incoming data
  QString inkData = message.data().toBase64().trimmed();
  kmDebug() << inkData;
  QRegExp inkChars( "([^A-Za-z0-9:+/=])" );
  if( inkChars.indexIn( inkData ) != -1 )
  {
    kmWarning() << "Received invalid ink message from" << message.peer()->handle() << ", found char 0x" << QString::number( inkChars.cap(1)[0].unicode(), 16 ) << "at position" << inkChars.pos(1);
    kmWarning() << "Ink data dump:" << inkData;

    KMessMessage msg( KMessMessage::System, getMsnChat(), 0 );
    msg.setMessage( i18nc( "Error message shown in chat, %1 is the name of the contact",
                           "You received an handwritten message from %1, "
                           "but it could not be displayed. The data could "
                           "not be read.", message.peer()->displayName() ) );

    showMessage( msg );
    return;
  }

  QString imageMimeType;
  QByteArray inkEncodedData( inkData.toLatin1() );
  inkEncodedData.replace( "base64:", "" );

  kmDebug() << inkEncodedData;
  switch( message.format() )
  {
    case KMess::FORMAT_ISF:
    {
      // ISF messages need to be base64-decoded, converted to a picture readable by KHTML,
      // then base64-encoded again.
      // Since I'd rather avoid using giflib to encode ISF data into pictures, I'll use PNG.
      imageMimeType = "image/png";

      QPixmap pixmap;

      Isf::Drawing drawing( Isf::Stream::reader( inkEncodedData, true /* fromBase64 */ ) );

      if( drawing.isNull() )
      {
        // Create a blank picture instead of sending invalid data to KHTML
        pixmap = QPixmap( 10, 10 );
        pixmap.fill( Qt::white );
      }
      else
      {
        pixmap = drawing.pixmap();
      }

      // Save it using a buffer
      QBuffer buffer( &inkEncodedData );
      buffer.open( QIODevice::WriteOnly );
      pixmap.save( &buffer, "PNG" );
      buffer.close();

      // Encode the data with Base64 to insert it inline in the HTML image tag
      inkEncodedData = inkEncodedData.toBase64();
      break;
    }

    case KMess::FORMAT_GIF:
      // Yes, this is it, just set the GIF mimetype. The data is already
      // there in the inkEncodedData variable, and KHTML can show inline
      // gif images right away
      imageMimeType = "image/gif";
      break;
  }

  QString html( "<img src=\"data:" + imageMimeType + ";base64," + Qt::escape( inkEncodedData ) + "\">" );

  KMessMessage msg( KMessMessage::Ink, getMsnChat(), message.peer() );
  msg.setMessage( html );

  displayIncomingMessage( msg );
}



// Notify the user of a received nudge
void Chat::receivedNudgeMessage( KMess::MsnContact *contact )
{
#ifdef KMESSDEBUG_CHAT_GENERAL
  kmDebug() << "Received a nudge.";
#endif

  // Get friendly name
  const QString& friendlyName( Contact( contact )->getFriendlyName( STRING_CLEANED_ESCAPED ) );

  KMessMessage message( KMessMessage::Nudge, getMsnChat(), contact );
  message.setMessage( i18n( "%1 has sent you a nudge!", friendlyName ) );

  displayIncomingMessage( message );

  // Emit the new message signal so that the user will receive a notification
  emit gotChatMessage( message, this );

  // Start the buzz effect.
  emit gotNudge();

}



/**
 * Save the chat if needed.
 *
 * The boolean argument should not be used, as it's used to recurse once when
 * the chat should also be saved to an external file.
 */
void Chat::saveChatAutomatically( bool saveExternalFile )
{
  if( Account::connectedAccount == 0 )
  {
    // Normally, in this situation, the chat should have been saved right
    // before the connectedAccount logged out, so this is no problem.
    kmWarning() << "ConnectedAccount is already gone, can't save chats.";
    return;
  }

  // Check if "save chats" is enabled
  if( ! Account::connectedAccount->getSettingBool( "LoggingEnabled" ) )
  {
#ifdef KMESSDEBUG_CHAT_GENERAL
    kmDebug() << "User has save chats disabled, not saving chat.";
#endif
    return;
  }

  // Check if the message area is empty, we won't save empty chats
  if( isEmpty() )
  {
#ifdef KMESSDEBUG_CHAT_GENERAL
    kmDebug() << "Message area is empty, not saving chat.";
#endif
    return;
  }

#ifdef KMESSDEBUG_CHAT_GENERAL
  if( ! saveExternalFile )
  {
    kmDebug() << "Automatically saving the XML log file.";
  }
  else
  {
    kmDebug() << "Automatically saving the external log file.";
  }
#endif

  QDir    dir;
  QString fileName, extension;
  Account::ChatExportFormat format;

  // Set the base file name, same for all logs
  const ContactsList &participants( getMsnChat()->participants() );
  if( participants.isEmpty() )
  {
    // Should never, never, ever happen.
    fileName = "KMess";
  }
  else
  {
    fileName = participants.first()->handle();
  }

  if( ! saveExternalFile )
  {
    extension = "xml";
    format = Account::EXPORT_XML;
    dir.setPath( KMessConfig::instance()->getAccountDirectory( Account::connectedAccount->getHandle() )
               + "/chatlogs" );
  }
  else
  {
    dir.setPath( Account::connectedAccount->getSettingString( "LoggingToFilePath" ) );
    format = (Account::ChatExportFormat) Account::connectedAccount->getSettingInt( "LoggingToFileFormat" );
    switch( format )
    {
      case Account::EXPORT_TEXT:
        extension = "txt";
        break;
      case Account::EXPORT_HTML:
        extension = "html";
        break;
      default:
        kmWarning() << "Extension is unknown for save format" << format << "! Using \"html\".";
        extension = "html";
        format = Account::EXPORT_HTML;
        break;
    }

    const QString& year ( startDateTime_.toString( "yyyy" ) );
    const QString& month( startDateTime_.toString( "MM"   ) );
    const QString& day  ( startDateTime_.toString( "dd"   ) );

    // Determine the path where to save the log file
    switch( Account::connectedAccount->getSettingInt( "LoggingToFileStructure" ) )
    {
      // Save as <logs-path>/yyyy/contactemail.ext
      case Account::BYYEAR:
        if( ! dir.exists( year  ) ) dir.mkdir( year  );
        dir.cd( year  );
        break;

      // Save as <logs-path>/yyyy/mm/contactemail.ext
      case Account::BYMONTH:
        if( ! dir.exists( year  ) ) dir.mkdir( year  );
        dir.cd( year  );
        if( ! dir.exists( month ) ) dir.mkdir( month );
        dir.cd( month );
        break;

      // Save as <logs-path>/yyyy/mm/dd/contactemail.ext
      case Account::BYDAY:
        if( ! dir.exists( year  ) ) dir.mkdir( year  );
        dir.cd( year  );
        if( ! dir.exists( month ) ) dir.mkdir( month );
        dir.cd( month );
        if( ! dir.exists( day   ) ) dir.mkdir( day   );
        dir.cd( day   );
        break;

      // Save as <logs-path>/contactemail.ext
      case Account::SINGLEDIRECTORY:
      default:
        break;
    }
  }

  // Try creating the directory if it doesn't exist
  if( ! dir.exists() )
  {
    dir.mkpath( dir.absolutePath() );
  }
  if( ! dir.exists() ) // It couldn't be created
  {
    if( ! KMessApplication::instance()->quitSelected() )
    {
      KMessageBox::sorry( getChatWindow(),
                          i18n( "<html>KMess could not save the log for this chat:<br />"
                                "The chat logs directory, &quot;%1&quot;, does not exist.</html>",
                                dir.absolutePath() ) );
    }
    // TODO: show a KDE save file dialog

    // Always warn via debug log, too
    kmWarning() << "Unable to save the chat log. The base directory" << dir.absolutePath() << "doesn't exist.";
    return;
  }

  int fileNumber = 0;
  QString fileCount;
  QString savePath( dir.absolutePath() + "/" );

  // We need to get the current (last) logging file; if it's not too big we'll append
  // to it; if it is too big or there is no current/last logging file, we'll create a new one.
  bool lastFileExists = KMessShared::selectNextFile( savePath, fileName, fileCount, extension, fileNumber );

  // this is the last file as pointed to by selectNextFile; lastFileExists should be the same as file.exists()
  QFile file( savePath + fileName + fileCount + "." + extension );

  // There are log files already for this contact: verify if we can
  // append to the last one: logs should not be too big (2MB max)
  if( lastFileExists && file.size() > Chat::MAX_LOG_FILE_SIZE )
  {
#ifdef KMESSDEBUG_CHAT_GENERAL
    kmDebug() << "Last file" << file.fileName() << "is too big, creating a new file";
#endif
    // so automatically choose the next file name.
    file.setFileName( savePath + fileName + "." + QString::number( fileNumber ) + "." + extension );
  }

  // if lastFileExists == false. then the base file is available for writing.
  // use it.

#ifdef KMESSDEBUG_CHAT_GENERAL
  kmDebug() << "Saving to chat log file:" << file.fileName();
#endif

  // Save the chat in the format requested by the user
  if( ! saveChatToFile( file.fileName(), format,
                        false, /* Don't overwrite */
                        false  /* No user interaction */ ) )
  {
    // If saving fails (due to a corrupted file for example) try to save the chat
    // with the next available file name

    // Possible race condition but we'll take it: since we can assume the
    // returned file above was indeed the last file, the next file must be
    // nonexistant, so we use that as a new file now

    // Another thing is that if we were *already* creating a new file and the
    // save failed, we will try to save to the same file yet again. However,
    // when saving to a new file already fails, whatever we do it probably
    // won't work with a different filename either.
    // TODO: make this more robust.

    file.setFileName( savePath + fileName + "." + QString::number( fileNumber ) + "." + extension );
    kmWarning() << "Save failed. Trying next file name=" << file.fileName();

    // Try to save it again, this time warning the user if it fails once more
    if( ! saveChatToFile( file.fileName(), format, false, true ) )
    {
      kmWarning() << "Second save failed too.";
    }
#ifdef KMESSDEBUG_CHAT_GENERAL
    else
    {
      kmDebug() << "Second save attempt succeeded.";
    }
#endif
  }

  if( saveExternalFile == false && Account::connectedAccount->getSettingBool( "LoggingToFileEnabled" ) )
  {
#ifdef KMESSDEBUG_CHAT_GENERAL
    kmDebug() << "Done, saving external file too...";
#endif
    saveChatAutomatically( true );
  }
}



// Send a message to the current chat
void Chat::sendChatMessage( const QString &message )
{
  // If we send the first message of the chat, clear the first message status
  if( firstMessage_ )
  {
    firstMessage_ = false;
  }

  KMess::TextMessage msg;
  msnChat_->sendMessage( message,
                         &msg,
                         QFont( Account::connectedAccount->getSettingFont( "FontUser" ) ),
                         QColor( Account::connectedAccount->getSetting( "FontUserColor" ).toString() )
                       );

  showMessage( KMessMessage( msg, KMessMessage::OutgoingText ) );
}



// Send an ink drawing to the current chat
void Chat::sendInkMessage( KMess::InkFormat format, const QByteArray &inkData )
{
  KMess::InkMessage message( msnChat_, 0 );
  message.setFormat( format );
  message.setData( inkData );

  msnChat_->sendMessage( message );
}



// Send an wink to the current chat
void Chat::sendWink( const KMess::MsnObject &msnObject )
{
  KMess::WinkMessage message( msnChat_, 0 );
  message.setWinkObjectString( msnObject.objectString() );
  msnChat_->sendMessage( message );
  QString winkHtml;

  // Only show the sent wink if they're enabled
  if( Account::connectedAccount->getSettingBool( "ChatShowWinks" ) )
  {
    WinksWidget::getHtmlFromWink( msnObject.getLocation(), winkHtml );
  }

  // If the stored wink didn't have a name, don't show it:
  QString notificationText;
  if( msnObject.getFriendly().isEmpty() )
  {
    notificationText = i18nc( "Message shown in the chat window (when the wink name is unknown)",
                              "You have sent a wink!" );
  }
  else
  {
    notificationText = i18nc( "Message shown in the chat window, %1 is the wink name",
                              "You have sent the &quot;%1&quot; wink!",
                              msnObject.getFriendly() );
  }

  KMessMessage wink( KMessMessage::Wink, getMsnChat(), globalSession->self() );
  wink.setMessage( notificationText + winkHtml );
  showMessage( wink );
}



// Enable or disable the parts of the window which allow user interaction
void Chat::setEnabled( bool isEnabled )
{
  if( ! isEnabled )
  {
    getChatWindow()->showStatusMessage( ChatStatusBar::Disconnected, i18n("The chat has been disabled because you are no longer connected to the Live Messenger server." ));
  }
  else
  {
    getChatWindow()->showStatusMessage( ChatStatusBar::DefaultType, " " );
  }


  // Do not reactivate if there is no connection (ChatMaster will recreate it for us)
  if( isEnabled && !msnChat_->isConnected() )
  {
    return;
  }

  ChatView::setEnabled( isEnabled );

  if ( isEnabled )
  {
    getChatWindow()->setUIState( ChatWindow::Connected );
  }

}



/**
 * @brief Set the MsnChat object for this Chat object.
 * @todo remove
 */
void Chat::setMsnChat( KMess::MsnChat *chat )
{
  // Disconnect the old MsnChat if any
  if( msnChat_ )
  {
    disconnect( msnChat_, 0, this, 0 );
  }

  // Set it
  msnChat_ = chat;

  // And connect new stuff
  // TODO
#ifdef __GNUC__
#warning TODO: finish wiring MsnChat ( warnings, send fail/succeed? )
#endif
  connect( msnChat_, SIGNAL( showWink( const QString&,const QString&, const QString& ) ),
           this,     SLOT  (   showWink( const QString&, const QString&, const QString& ) ) );

  connect( msnChat_, SIGNAL( contactJoined( KMess::MsnContact * ) ),
           this,     SLOT  (   contactJoined( KMess::MsnContact * ) ) );

  connect( msnChat_, SIGNAL( contactLeft( KMess::MsnContact *, bool ) ),
           this,     SLOT  (   contactLeft( KMess::MsnContact *, bool ) ) );

  connect( msnChat_, SIGNAL( messageReceived( KMess::Message ) ),
           this,     SLOT  ( slotReceivedMessage( KMess::Message ) ) );

  connect( msnChat_, SIGNAL( sendingFailed( KMess::Message,QString ) ),
           this,     SLOT  ( slotSendingFailed( KMess::Message ) ) );

  connect( msnChat_, SIGNAL( chatWarning( KMess::Message, KMess::MsnContact* ) ),
           this,     SLOT  ( showWarning( KMess::Message, KMess::MsnContact* ) ) );
}



// A warning from the switchboard has been received, display it
void Chat::showWarning( KMess::ChatWarningType type, KMess::MsnContact *contact )
{
  QString handle, friendlyName;
  Contact c( contact );
  if( ! c.isValid() )
  {
    handle       = Account::connectedAccount->getHandle();
    friendlyName = Account::connectedAccount->getFriendlyName( STRING_CLEANED_ESCAPED );
  }
  else
  {
    handle       = c->getHandle();
    friendlyName = c->getFriendlyName( STRING_CLEANED_ESCAPED );
  }

#ifdef __GNUC__
#warning TODO: Switchboard warnings
#endif
#if 0
  switch( type )
  {
    case KMess::WARNING_CONNECTION_DROP:
      showMessage( KMess::TextMessage( KMess::TextMessage::TYPE_SYSTEM,
                                KMess::TextMessage::CONTENT_SYSTEM_ERROR,
                                true,
                                i18nc( "Warning message shown in chat",
                                       "There has been a connection problem." ),
                                handle,
                                friendlyName ) );
      break;

    case KMess::WARNING_TOO_MANY_EMOTICONS:
      showMessage( KMess::TextMessage( KMess::TextMessage::TYPE_SYSTEM,
                                KMess::TextMessage::CONTENT_SYSTEM_ERROR,
                                true,
                                i18nc( "Warning message shown in chat",
                                       "There were too many different custom emoticons in your last message. Only the first 7 will be sent." ),
                                handle,
                                friendlyName ) );
      break;

    case KMess::WARNING_UNSUPPORTED_VOICECLIP:
#ifdef KMESSTEST
      KMESS_ASSERT( contact != 0 );
#endif
      showMessage( KMess::TextMessage( KMess::TextMessage::TYPE_SYSTEM,
                                KMess::TextMessage::CONTENT_SYSTEM_ERROR,
                                true,
                                i18nc( "Warning message shown in chat, %1 is the contact's friendly name",
                                       "%1 has sent you a voice clip, but KMess does not support voice clips yet.",
                                       friendlyName ),
                                handle,
                                friendlyName ) );
      break;

    case KMess::WARNING_UNSUPPORTED_ACTIONMESSAGE:
#ifdef KMESSTEST
      KMESS_ASSERT( contact != 0 );
#endif
      showMessage( KMess::TextMessage( KMess::TextMessage::TYPE_SYSTEM,
                                KMess::TextMessage::CONTENT_SYSTEM_ERROR,
                                true,
                                i18nc( "Warning message shown in chat, %1 is the contact's friendly name",
                                       "%1 has sent you an action message, but KMess does not support action messages yet.",
                                       friendlyName ),
                                handle,
                                friendlyName ) );
      break;

    case KMess::WARNING_INK_UNSUPPORTED_BY_CONTACT:
      showMessage( KMess::TextMessage( KMess::TextMessage::TYPE_SYSTEM,
                                KMess::TextMessage::CONTENT_SYSTEM_ERROR,
                                true,
                                i18nc( "Warning message shown in chat",
                                       "One or more contacts do not support the handwriting message." ),
                                handle,
                                friendlyName ) );
      break;

    case KMess::WARNING_UNSUPPORTED_UNKNOWN:
#ifdef KMESSTEST
      KMESS_ASSERT( contact != 0 );
#endif
    default:
      showMessage( KMess::TextMessage( KMess::TextMessage::TYPE_SYSTEM,
                                KMess::TextMessage::CONTENT_SYSTEM_ERROR,
                                true,
                                i18nc( "Warning message shown in chat, %1 is the contact's friendly name",
                                       "%1 has sent you a Live Messenger feature that KMess does not support yet.",
                                       friendlyName ),
                                handle,
                                friendlyName ) );
      break;
  }
#endif
}



// Show a wink received by a contact
void Chat::showWink( const QString &handle, const QString &filename, const QString &animationName )
{
#ifdef KMESSDEBUG_CHAT_GENERAL
  kmDebug() << "Processing received wink data.";
#endif

#ifdef __GNUC__
#warning TODO: Implement showing of winks once MSNObject download is ready in lib.
#endif
#if 0
  QString message;
  Contact contact( handle );
  const QString& friendlyName( contact->getFriendlyName( STRING_CLEANED_ESCAPED ) );

  // If we aren't showing winks, notify the user that we got one but don't actually display the animation.
  if( ! Account::connectedAccount->getSettingBool( "ChatShowWinks" ) )
  {
    if( animationName.isEmpty() )
    {
      message = i18nc( "Message shown in the chat window, %1 is the contact's friendly name",
                       "You received a wink from %1, but displaying "
                       "winks has been disabled. "
                       "You can re-enable it in the <a href='kmess://accountconfig'>"
                       "account settings</a>.",
                       friendlyName );
    }
    else
    {
      message = i18nc( "Message shown in the chat window, %1 is the contact's "
                       "friendly name, %2 is the wink name",
                       "You received the &quot;%2&quot; wink from %1, but displaying "
                       "winks has been disabled. "
                       "You can re-enable it in the <a href='kmess://accountconfig'>"
                       "account settings</a>.",
                       friendlyName,
                       animationName );
    }
#if 0
    showMessage( KMess::TextMessage( KMess::TextMessage::TYPE_NOTIFICATION,
                              KMess::TextMessage::CONTENT_NOTIFICATION_WINK,
                              false,
                              message,
                              handle ) );
#endif
    return;
  }

  // Test whether the file exists, before cabextract fails.
  if( ! QFile::exists( filename ) )
  {
    kmWarning() << "file not found: " << filename << "!";
    return;
  }

  QString html;
  WinksWidget::CABEXTRACTOR value = WinksWidget::getHtmlFromWink( filename, html );

  if( value == WinksWidget::SUCCESS )
  {
    if( animationName.isEmpty() )
    {
      message = i18nc( "Message shown in the chat window, %1 is the contact's friendly name",
                                        "%1 has sent you a wink!",
                                        friendlyName );
    }
    else
    {
      message = i18nc( "Message shown in the chat window, %1 is the contact's "
                                        "friendly name, %2 is the wink name",
                                        "%1 has sent you a wink: &quot;%2&quot;!",
                                        friendlyName,
                                        animationName );
    }

    // Show a notification
#if 0
    receivedMessage( KMess::TextMessage( KMess::TextMessage::TYPE_NOTIFICATION,
                                  KMess::TextMessage::CONTENT_NOTIFICATION_WINK,
                                  true,
                                  message + html,
                                  handle,
                                  friendlyName ) );
#endif
    return;
  }

  // Extraction failed
  switch( value )
  {
    case WinksWidget::NOTINSTALLED:
      if( animationName.isEmpty() )
      {
        message = i18nc( "Message shown in the chat window, %1 is the contact's friendly name",
                         "You received a wink from %1, but it "
                         "could not be displayed. Make sure you have "
                         "the &quot;cabextract&quot; program installed.",
                         friendlyName );
      }
      else
      {
        message = i18nc( "Message shown in the chat window, %1 is the contact's "
                         "friendly name, %2 is the wink name",
                         "You received the &quot;%2&quot; wink from %1, but it "
                         "could not be displayed. Make sure you have "
                         "the &quot;cabextract&quot; program installed.",
                         friendlyName,
                         animationName );
      }
      break;

    case WinksWidget::FAILED:
      if( animationName.isEmpty() )
      {
        message = i18nc( "Message shown in the chat window, %1 is the contact's friendly name",
                         "You received a wink from %1, but it "
                         "could not be displayed. Extracting the wink "
                         "package with &quot;cabextract&quot; has failed.",
                         friendlyName );
      }
      else
      {
        message = i18nc( "Message shown in the chat window, %1 is the contact's "
                         "friendly name, %2 is the wink name",
                         "You received the &quot;%2&quot; wink from %1, but it "
                         "could not be displayed. Extracting the wink "
                         "package with &quot;cabextract&quot; has failed.",
                         friendlyName,
                         animationName );
      }
      break;

    default:
      if( animationName.isEmpty() )
      {
        message = i18nc( "Message shown in the chat window, %1 is the contact's friendly name",
                         "You received a wink from %1, but it "
                         "could not be displayed. The data could not be "
                         "read.",
                         friendlyName );
      }
      else
      {
        message = i18nc( "Message shown in the chat window, %1 is the contact's "
                         "friendly name, %2 is the wink name",
                         "You received the &quot;%2&quot; wink from %1, but it "
                         "could not be displayed. The data could not be "
                         "read.",
                         friendlyName,
                         animationName );
      }
      break;
  }

#ifdef __GNUC__
#warning todo
#endif
#if 0
  showMessage( KMess::TextMessage( KMess::TextMessage::TYPE_SYSTEM,
                            KMess::TextMessage::CONTENT_SYSTEM_ERROR,
                            false,
                            message,
                            handle ) );
#endif
#endif
}



// Display a message to notify of a contact status change
void Chat::slotContactChangedStatus()
{
  Contact c( sender() );
  const KMess::MsnContact *contact = c->msnContact();

  if( KMESS_NULL( contact ) ) return;

  // Make sure this contact is in chat
  if( ! isContactInChat( contact->handle(), false ) )
  {
#ifdef KMESSDEBUG_CHAT_GENERAL
    kmDebug() << "Received a status change signal from a contact not in chat!";
#endif
    return;
  }

  QString message;

  const QString &friendlyName( c->getFriendlyName( STRING_CLEANED_ESCAPED ) );

  // Display a message when the user goes offline.
  if( contact->isOffline() )
  {
    // Show a different one depending if this is an exclusive chat (which means we send offline IMs)
    if( getMsnChat()->participants().size() == 1 )
    {
      message = i18n( "%1 has gone offline. Any messages you send will be delivered the next time he or she logs in.",
                      friendlyName );
    }
    else if( Account::connectedAccount->getSettingBool( "ChatSessionMessagesEnabled" ) )
    {
      message = i18n( "%1 has gone offline.",
                      friendlyName );
    }
    else
    {
      return;
    }
  }
  else if( Account::connectedAccount->getSettingBool( "ChatSessionMessagesEnabled" ) )
  {
    message = i18n( "%1 has changed his or her status to &quot;%2&quot;.",
                    friendlyName,
                    Status::getName( contact->status() ) );
  }
  else
  {
    return;
  }


  KMessMessage presence( KMessMessage::Presence, getMsnChat(), contact );
  presence.setMessage( message );

  showMessage( presence );
}



/**
 * Called when a message is received from the chat. We must determine what type of message it is and respond accordingly.
 *
 * From here, if it requires further processing the message is passed to one of the
 * received*Message helper methods. Then it is sent to displayReceivedMessage for final
 * processing and display to the user.
 *
 * @param message The Message received.
 */
void Chat::slotReceivedMessage( KMess::Message message )
{
  kmDebug() << "Received a message. Selecting type...";

  // extract the inner message for Yahoo messages
  if ( message.type() == KMess::YahooMessageType )
  {
    KMess::YahooMessage msg( message );
    message = msg.innerMessage();
  }

  switch( message.type() )
  {
    case KMess::TextMessageType:
    case KMess::OfflineMessageType:
    {
      KMessMessage msg( message, KMessMessage::IncomingText );
      displayIncomingMessage( msg );
      break;
    }

    case KMess::InkMessageType:
    {
      receivedInkMessage( KMess::InkMessage( message ) );
      break;
    }

    case KMess::NudgeMessageType:
      receivedNudgeMessage( message.peer() );
      break;

    case KMess::TypingMessageType:
      contactTyping( message.peer() );
      break;

    default:
      kmDebug() << "Unhandled Message type: " << message.type();
  }

  // Mark the moment of arrival
  lastReceivedMessage_ = QDateTime::currentDateTime();
}



// Signal that a message could not be sent to the switchboard
void Chat::slotSendingFailed( KMess::Message message, const QString &recipient )
{
  const QString& contentType( message.field( "Content-Type" ) );
  QString errorMessage;
  QString friendlyName;

  Contact contact( message.peer() );

#ifdef KMESSDEBUG_CHAT_GENERAL
  kmDebug() << "Showing a sending failure notice.";
#endif

  // It's possible that a message could not be delivered to any of the contacts in chat:
  // in this case, the handle is *
  if( recipient == "*" )
  {
    friendlyName = i18nc( "Phrase to be inserted in place of a contact name, when a message can't be delivered "
                          "to any of the recipients",
                          "all contacts" );
  }
  else
  {

    friendlyName = contact->getFriendlyName( STRING_CLEANED_ESCAPED );
  }

  if( contentType.startsWith( "text/x-msnmsgr-datacast" ) )
  {
    switch( message.field( "ID" ).toInt() )
    {
      case 1: // Nudge
        errorMessage = i18nc( "Error message shown in chat, %1 is the contact's friendly name",
                              "Failed to send the nudge to %1.",
                              friendlyName );
        break;
      case 2: // Wink
        errorMessage = i18nc( "Error message shown in chat, %1 is the contact's friendly name",
                              "Failed to send the wink to %1.",
                              friendlyName );
        break;
      default: // Unsupported datacast?!
        kmWarning() << "Failed sending datacast message: no sending failure chat message available!";
        return;
    }
  }
  else if( contentType.startsWith( "image/gif" ) || contentType.startsWith( "application/x-ms-ink" ) )
  {
    errorMessage = i18nc( "Error message shown in chat, %1 is the contact's friendly name", "Failed to send the handwritten message to %1.",
                          friendlyName );
  }
  else if( contentType.startsWith( "text/plain" ) && ! message.contents().isEmpty() )
  {
    QString body( Qt::escape( message.contents() ) );

    // Cut down the message a bit if needed
    if( body.length() > 16 )
    {
      body.truncate( 16 );
      body += "...";
    }

    errorMessage = i18nc( "Error message shown in chat, %1 is the sent message, %2 is the contact's friendly name",
                          "Failed to send the message to %2:<br/>%1",
                          body,
                          friendlyName );
  }
  else
  {
#ifdef KMESSDEBUG_CHAT_GENERAL
    kmDebug() << "Not displaying a sending failure notice for messages of type" << contentType;
#endif
    return;
  }

  KMessMessage system( KMessMessage::System, getMsnChat(), message.peer() );
  system.setMessage( errorMessage );
}



// Send a file to a contact.
void Chat::startFileTransfer( QList<QUrl> fileList )
{
  if( KMESS_NULL(msnChat_) ) return;

  // Ask for a contact
  const QString& handle( chooseContact() );

  // If no contact can be chosen, stop
  if( handle.isEmpty() )
  {
#ifdef KMESSDEBUG_CHAT_FILETRANSFER
    kmDebug() << "Unable to find a contact to send files to. Aborting.";
#endif
    return;
  }

#ifdef KMESSDEBUG_CHAT_FILETRANSFER
  kmDebug() << "File list contents:" << fileList;
  kmDebug() << "Chosen handle:" << handle;
#endif

  if( fileList.isEmpty() )
  {
#ifdef KMESSDEBUG_CHAT_FILETRANSFER
    kmDebug() << "Asking for a file to send";
#endif

    // If no file names were given, ask for a filename
    KUrl fileName( KFileDialog::getOpenUrl( KUrl( "kfiledialog:///:fileupload" ) ) );

    // Stop if no file was selected
    if( fileName.isEmpty() )
    {
      return;
    }

    fileList.append( fileName );
  }

  foreach( const QUrl &fileUrl, fileList )
  {
    KUrl localFileUrl( KIO::NetAccess::mostLocalUrl( fileUrl, this ) );

    // If the url can't be translated to a local file, copy it to disk
    if( ! localFileUrl.isLocalFile() )
    {
#ifdef KMESSDEBUG_CHAT_FILETRANSFER
      kmDebug() << "Not a local file, copying it locally.";
#endif
      // Download the remote file to a temporary folder
      QString localFilePath;
      if( ! KIO::NetAccess::download( fileUrl, localFilePath, this ) )
      {
#ifdef KMESSDEBUG_CHAT_FILETRANSFER
        kmDebug() << "Copy failed.";
#endif
        // Show a message to the user
        KMessMessage system( KMessMessage::System, getMsnChat(), globalSession->self() );
        system.setMessage( i18n( "The file &quot;%1&quot; could not be found on "
                                        "your computer. The download could not be completed.",
                                        localFileUrl.prettyUrl() ) );
        showMessage( system );
        continue;
      }

      // TODO Remove the temporary file after the transfer
      localFileUrl.setUrl( localFilePath );
    }

#ifdef KMESSDEBUG_CHAT_FILETRANSFER
    kmDebug() << "Original file URL:" << fileUrl << "-- Local file URL:" << localFileUrl;
#endif

    // Ask the ChatMaster to initiate the file transfer. If there are multiple contacts
    // in this chat, it needs to create a new chat session for that contact.
    emit requestFileTransfer( handle, localFileUrl.toLocalFile() );
  }
}



// Send a nudge to a contact
void Chat::slotSendNudge()
{
  if( msnChat_ == 0 )
  {
    return;
  }

  // Show a more detailed message if we're in an active chat
  QString messageText;
  const ContactsList &contactsInChat = msnChat_->participants();
  if( contactsInChat.count() == 1 && msnChat_->isConnected() )
  {
    messageText = i18nc( "Message shown in chat window, %1 is the contact's friendly name",
                         "You have sent a nudge to %1!",
                         Contact( contactsInChat.first() ).getFriendlyName( STRING_CLEANED_ESCAPED ) );
  }
  else
  {
    // Chat is either empty (should resume), or a multi-chat
    messageText = i18n( "You have sent a nudge!" );
  }

  KMessMessage msg( KMessMessage::Nudge, getMsnChat(), globalSession->self() );
  msg.setMessage( messageText );

  // Display message
  msnChat_->sendNudge();

  showMessage( msg );

  emit gotNudge();
}



// Start a chat
void Chat::startChat()
{
#ifdef KMESSDEBUG_CHAT_GENERAL
  kmDebug() << "Opening a chat of type:" << ( msnChat_->isConnected() ? "Active" : "Offline" );
#endif

  // Add the participant contacts to the chat, even if they're offline
  const ContactsList &participants( msnChat_->participants() );

#ifdef KMESSDEBUG_CHAT_GENERAL
  kmDebug() << "Adding initial chat participants to the chat:" << participants;
#endif

  foreach( KMess::MsnContact *contact, participants )
  {
    contactsWidget_->contactJoined( Contact( contact ) );
  }

  // if this is an offline chat, inform the user.
  if( getMsnChat()->participants().size() == 1 && !msnChat_->isConnected() )
  {
    KMess::MsnContact *contact = getMsnChat()->participants().first();
    if( ! contact->isUnknown() && contact->isOffline() )
    {
#ifdef KMESSDEBUG_CHAT_GENERAL
      kmDebug() << "Initial chat participant " << contact->handle() << " is offline; pretending the contact joined";
#endif
      contactJoined( contact );

      KMessMessage presence( KMessMessage::Presence, getMsnChat(), contact );
      presence.setMessage( i18n( "%1 is currently offline. Any messages you send will be delivered the next time he or she logs in.", Contact( contact ).getFriendlyName( STRING_CLEANED_ESCAPED ) ) );

      showMessage( presence );
    }
  }
}



//Start voice conversation
void Chat::startConversation()
{
  kmWarning() << "not implemented!";
  // msnChat_->startApp( MsnSwitchboardConnection::VOICECONVERSATION );
}



// Start GnomeMeeting with a contact.
void Chat::startMeeting()
{
  // Choose the contact.
  const QString& handle( chooseContact() );

  emit requestNetMeeting(handle);
}



// Send webcam data to a contact.
void Chat::startWebcamTransfer()
{
  // Choose the contact.
  const QString& handle( chooseContact() );

  emit requestWebcamTransfer(handle);
}


#include "chat.moc"
