/***************************************************************************
                          chatmaster.h  -  description
                             -------------------
    begin                : Sat Jan 18 2003
    copyright            : (C) 2003 by Mike K. Bennett
    email                : mkb137b@hotmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CHATMASTER_H
#define CHATMASTER_H

#include <KMess/TextMessage>
#include <KMess/NetworkGlobals>

#include <QList>
#include <QStringList>
#include <QTimer>


// Forward declarations
class Application;
class ApplicationList;
class ChatMessage;
class Chat;
class ChatWindow;
class Contact;
class MimeApplication;
class P2PApplication;
class P2PMessage;

namespace KMess {
  class MsnChat;
  class MsnObject;
  class MsnSwitchboardConnection;
}


/**
 * This class governs the chat windows, detecting when a chat
 * can be restarted in an open window and the like.
 *
 * @author Mike K. Bennett
 * @ingroup Chat
 */
class ChatMaster : public QObject
{
  Q_OBJECT

  friend class KMessTest;

  public: // Public methods
    // The constructor
                       ChatMaster(QObject *parent = 0);
    // The destructor
    virtual           ~ChatMaster();
    // Get all chats in all chat windows
    QList<Chat*>       getAllChats();
    // Return the chat for the given MsnChat
    Chat*              getChat( KMess::MsnChat *chat );
    // Return the window for the given chat
    ChatWindow        *getChatWindow( Chat *chat );
    // Return the chat window where we're having an conversation with the given contact.
    Chat              *getContactsChat( const QString &handle, bool privateChat );
    // Check whether the contact is in any of the existing chat windows.
    bool               isContactInChat( const QString &handle );

  public slots: // Public slots
    // The connection has been established
    void               connected();
    // The user has disconnected, so close all open switchboard connections and disable the chats
    void               disconnected();
    // KMess is establishing a connection.
    void               connecting();
    // Raise an existing chat window
    void               raiseChat( Chat *chat, bool force );
    // Verify if a new chat can be opened and requests a switchboard
    void               requestChat( const QString &handle );

  private slots: // Private slots
    // Start a chat with the information gathered from a switchboard connection
    Chat              *createChat( KMess::MsnChat *msnchat, bool requestedByUser = false );
    // Create a new chat window or retrieve an existing one if tabbed chatting is enabled
    ChatWindow        *createChatWindow( Chat *chat );
    // A chat is closing
    void               slotChatClosing( Chat *chat );
    // A chat window was destroyed
    void               slotChatWindowDestroyed(QObject *chatWindow);
    // Delete an existing switchboard
    void               slotMsnChatDelete( KMess::MsnChat *closing );
    // Update the grouping of chats/tabs.
    void               updateChatGrouping();
    // Search the appropriate window for a chat.
    ChatWindow        *findWindowForChat( Chat *chat );
    // A new chat was created by the library, add it to the list
    void               chatCreated( KMess::MsnChat *chat );
    // Periodically update events. It's based on the pings sent by MsnNotificationConnection.
    void               timedUpdate();

  private:  // private methods
    // getExclusiveChatWith
    KMess::MsnChat    *getExclusiveChatWith( const QString &handle ) const;
    // Return the chat window where we're having a conversation with the given contacts.
    Chat              *getContactsChat( const ContactsList contacts, bool privateChat );

  private: // Private attributes
    // The pointers to chat windows
    QList<ChatWindow*> chatWindows_;
    // The list of contacts whose DPs must be downloaded
    QStringList        pendingDisplayPictures_;
    // A list of requested chat with one contact ( handle )
    QStringList        requestedChats_;
    // The pointers to the switchboard server connections
    //QList<MsnSwitchboardConnection*> switchboardConnections_;
    QList<KMess::MsnChat*> msnChats_;
    // The current tabbed chat setting
    int                chatTabbingMode_;
    // Timer to call method for update commands
    QTimer             timer_;

  signals: // Public signals
    // Signal that a chat was created.
    void               newChatCreated( const KMess::MsnChat* msnChat, const Chat* chat, const ChatWindow* chatWindow );
    // Signal for ChatMessageView to replace an application's accept/reject/cancel links with another text
    void               updateApplicationMessage( const QString &messageId, const QString &newMessage );
    // Signal a reconnect request.
    void               reconnect();
};

#endif
