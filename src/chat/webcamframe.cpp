/***************************************************************************
                     webcamframe.cpp - Code for the ebcam frame
                             -------------------
    begin                : Wed May 14 2008
    copyright            : (C) 2008 by Antonio Nastasi
    email                : sifcenter@gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "webcamframe.h"

#include "../kmessdebug.h"

//WebcamFrame::WebcamFrame( QWidget *parent, QPainter *painter, QString status )
WebcamFrame::WebcamFrame( QWidget *parent, QString status )
: QWidget( parent )
, Ui::WebcamFrame()
{
  setupUi( this );
  
  // painter should not be NULL
  //setPainter( painter );

  if( ! status.isEmpty() )
  {
    setStatus( status );
  }

  // maybe set an initializing image ("The webcam session is being initialized..." with the
  // KMess bird etc ^__^) in the QPainter?

  frame_.setPixmap( painter_ );
  frame_.show();
}



void WebcamFrame::setStatus( const QString& status )
{
  _status = status;
}



const QString& WebcamFrame::getStatus() const
{
  return _status;
}


#if 0
void WebcamFrame::setPainter( QPainter *painter )
{
  if( painter == 0 )
  {
    kmError() << "Tried to set painter to a NULL value in WebcamFrame, ignoring.";
    return;
  }

  _painter = painter;
  frames_. 
}
#endif

const QPainter& WebcamFrame::getPainter()
{
  return painter_;
}


#include "webcamframe.moc"
