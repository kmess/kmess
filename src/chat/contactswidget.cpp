/***************************************************************************
                          contactswidget.cpp  -  description
                             -------------------
    begin                : Thu Jan 16 2003
    copyright            : (C) 2003 by Mike K. Bennett
    email                : mkb137b@hotmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "contactswidget.h"

#include "../contact/contact.h"
#include "../account.h"
#include "../kmessglobal.h"
#include "../kmessdebug.h"
#include "../settings/accountsettingsdialog.h"

#include <QDockWidget>

#include <KIconEffect>



// The constructor
ContactsWidget::ContactsWidget( QWidget *parent )
: QWidget( parent )
, Ui::ContactsWidget()
, wasVisible_( false )
{
  setupUi( this );

  container_->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);

  // Create the layout which will organize the contact frames
  layout_ = new QBoxLayout( QBoxLayout::TopToBottom, container_ );
  layout_->setSpacing( 2 );
  layout_->setContentsMargins( 0, 0, 0, 0 );
  //layout_->setSizeConstraint( QLayout::SetMinAndMaxSize );
  layout_->setAlignment( Qt::AlignTop | Qt::AlignLeft );

  // The scroll area background should mimetize with the rest of the app
  area_->setBackgroundRole( QPalette::Window );

  // Initialize the user picture frame
  connect( userDisplayPicture_,       SIGNAL(                    leftClicked() ),
           this,                      SLOT  (  slotUserDisplayPictureClicked() ) );
  connect( Account::connectedAccount, SIGNAL(          changedDisplayPicture() ),
           this,                      SLOT  (       slotUpdateDisplayPicture() ) );
  slotUpdateDisplayPicture();
}



// The destructor
ContactsWidget::~ContactsWidget()
{
  // Remove all contact frames
  qDeleteAll( contactFrames_ );
  contactFrames_.clear();

#ifdef KMESSDEBUG_CONTACTSWIDGET
  kmDebug() << "DESTROYED.";
#endif
}



// A contact was added to the contact list
void ContactsWidget::contactAdded( KMess::MsnContact *contact )
{
  // Update any invited contact frame
  ContactFrame *contactFrame = getContactFrame( contact->handle() );
  if ( contactFrame != 0 )
  {
    contactFrame->activate( contact );
  }
}



// A contact joined the chat
void ContactsWidget::contactJoined( KMess::MsnContact *contact )
{
  // See if the contact already has a frame
  const QString& handle( contact->handle() );
  ContactFrame *contactFrame = getContactFrame( handle );

  if( contactFrame != 0 )
  {
    // Reactivate the contact's frame
    contactFrame->setEnabled( true );
    return;
  }

#ifdef KMESSDEBUG_CONTACTSWIDGET
  kmDebug() << handle;
#endif

  ContactFrame::DisplayMode frameMode;

  // Get the first available contact frame
  contactFrame = createContactFrame();
  if( KMESS_NULL(contactFrame) ) return;

  // Activate the frame
  contactFrame->activate( contact );

  // Find the most suitable frame display mode
  frameMode = getBestFrameMode();

  // Set it to all the frames, including the new one
  foreach( ContactFrame *frame, contactFrames_ )
  {
    frame->setDisplayMode( frameMode );
  }
}



// A contact left the chat
void ContactsWidget::contactLeft( KMess::MsnContact *contact, bool /*isChatIdle*/ )
{
  const QString& handle( contact->handle() );
  ContactFrame *contactFrame = getContactFrame( handle );

  if ( contactFrame == 0 )
  {
    return;
  }

#ifdef KMESSDEBUG_CONTACTSWIDGET
  kmDebug() << handle;
#endif


  ContactFrame::DisplayMode frameMode;

  // Delete the frames whenever there is more than one; when only one is left, grey it out instead.
  if( contactFrames_.count() > 1 )
  {
    contactFrames_.removeAll( contactFrame );
    delete contactFrame;

    // Find the most suitable frame mode
    frameMode = getBestFrameMode();

    // Set it to all the frames
    foreach( ContactFrame *frame, contactFrames_ )
    {
      frame->setDisplayMode( frameMode );
    }
  }
  else
  {
    // Deactivate the contact frame
    contactFrame->setEnabled( false );
  }
}



// A contact was removed from the contact list
void ContactsWidget::contactRemoved( KMess::MsnContact *contact )
{
  // Update any invited contact frame
  ContactFrame *contactFrame = getContactFrame( contact->handle() );
  if ( contactFrame != 0 )
  {
    contactFrame->activate( 0 );
  }
}



// A contact is typing
void ContactsWidget::contactTyping( KMess::MsnContact *contact )
{
  ContactFrame *contactFrame = getContactFrame( contact->handle() );
  if ( contactFrame != 0 )
  {
    contactFrame->startTyping();
  }
}



// Get the most suitable frame size, depending on how many contacts are currently in chat with us
inline ContactFrame::DisplayMode ContactsWidget::getBestFrameMode()
{
  // Find out how many are chatting at the moment
  int count = contactFrames_.count();

  if( count > 5 )
  {
    // More than 5 contacts in chat with us: it's a big group chat and we need to be able to display as many frames as possible
    return ContactFrame::ModeTiny;
  }
  else if( count > 2 )
  {
    // A normal group chat: reduce the size of the frames to be able to see all of our contacts at once
    return ContactFrame::ModeSmall;
  }
  else if( count == 2 )
  {
    // A small group chat: Show all details, there's still a lot of space
    return ContactFrame::ModeNormal;
  }

  // A simple 1-on-1 chat: we can show as much detail as possible
  return ContactFrame::ModeSingle;
}



// Find the contact frame with the given handle
ContactFrame* ContactsWidget::getContactFrame( const QString& handle )
{
  foreach( ContactFrame *contactFrame, contactFrames_ )
  {
    if( contactFrame->getHandle() == handle )
    {
      return contactFrame;
    }
  }

  return 0;
}



// Return one new contact frame
ContactFrame* ContactsWidget::createContactFrame()
{
  ContactFrame *contactFrame = new ContactFrame( container_ );

  // Add it to the viewBox so it appears in the widget
  layout_->insertWidget( children().count() - 2, contactFrame );

  // put it in the list of frames so we can find it again.
  contactFrames_.append( contactFrame );

  return contactFrame;
}



// A message was received from one of the contacts... notify its frame
void ContactsWidget::messageReceived( const QString& handle )
{
  ContactFrame *frame = getContactFrame( handle );
  if ( frame != 0 )
  {
    frame->messageReceived();
  }
}



// Enable/disable the frames
void ContactsWidget::setEnabled( bool isEnabled )
{

  if( isEnabled )
  {
    // Loop through all frames
    foreach( ContactFrame *frame, contactFrames_ )
    {
      KMess::MsnContact *contact = globalSession->contactList()->contact( frame->getHandle() );

      // Only reactivate frames for contacts which exist in the account
      if( contact )
      {
        // Reactivate the frame
        frame->activate( contact );
      }
    }
  }
  else
  {
    // Loop through all frames
    foreach( ContactFrame *frame, contactFrames_ )
    {
      // Remove the activation status
      frame->activate( 0 );
    }
  }
}



// Connect the widget to a dock
void ContactsWidget::setDockWidget( QDockWidget *dockWidget, Qt::DockWidgetArea initialArea )
{
#ifdef KMESSTEST
  KMESS_ASSERT( dockWidget );
#endif

  // We need to change layout when the dock moves
  connect( dockWidget, SIGNAL( dockLocationChanged(Qt::DockWidgetArea) ),
           this,       SLOT  ( slotLocationChanged(Qt::DockWidgetArea) ) );
  connect( dockWidget, SIGNAL(     topLevelChanged(bool)               ),
           this,       SLOT  ( slotTopLevelChanged(bool)               ) );

  slotLocationChanged( initialArea );
}



// The location of the parent dock widget has changed
void ContactsWidget::slotLocationChanged( Qt::DockWidgetArea area )
{
#ifdef KMESSDEBUG_CONTACTSWIDGET
  kmDebug() << "Changing location to" << area;
#endif

  switch( area )
  {
    case Qt::TopDockWidgetArea:
    case Qt::BottomDockWidgetArea:

      // Loop through all frames to switch the layout mode for frames
      foreach( ContactFrame *frame, contactFrames_ )
      {
        frame->setDisplayMode( ContactFrame::ModeSmall );
      }

      layout_->setDirection( QBoxLayout::LeftToRight );
      area_->setHorizontalScrollBarPolicy( Qt::ScrollBarAsNeeded  );
      area_->setVerticalScrollBarPolicy  ( Qt::ScrollBarAlwaysOff );
      userPictureFrame_->setVisible( false );
      break;

    case Qt::LeftDockWidgetArea:
    case Qt::RightDockWidgetArea:
    default:

      // Loop through all frames to switch the layout mode for frames
      ContactFrame::DisplayMode mode = getBestFrameMode();
      foreach( ContactFrame *frame, contactFrames_ )
      {
        frame->setDisplayMode( mode );
      }

      layout_->setDirection( QBoxLayout::TopToBottom );
      area_->setHorizontalScrollBarPolicy( Qt::ScrollBarAlwaysOff );
      area_->setVerticalScrollBarPolicy  ( Qt::ScrollBarAsNeeded  );

      // Determine if the user picture needs to be shown
      slotUpdateDisplayPicture();
      break;
  }

  // Ensure that the view shows the top left corner
  area_->ensureVisible( 0, 0 );
}



// The parent dock widget floating status has changed
void ContactsWidget::slotTopLevelChanged( bool isTopLevel )
{
  // Moving has started
  if( isTopLevel )
  {
    wasVisible_ = isVisible();
    setVisible( false );
  }
  // The widget has been dropped somewhere
  else
  {
    setVisible( wasVisible_ );
  }

  adjustSize();
}



// Update the user's display picture
void ContactsWidget::slotUpdateDisplayPicture()
{
  bool isVisible = Account::connectedAccount->getSettingBool( "ChatUserDisplayPictureEnabled" );

  userPictureFrame_->setVisible( isVisible );

  // Don't update it if it's not even visible
  if( ! isVisible )
  {
#ifdef KMESSDEBUG_CONTACTSWIDGET
    kmDebug() << "Not updating hidden user picture widget";
#endif
    return;
  }

  // If the picture is unchanged, do nothing
  const QString &pictureFileName( Account::connectedAccount->getDisplayPicture() );
  if( userDisplayPicture_->getPicture() == pictureFileName )
  {
#ifdef KMESSDEBUG_CONTACTSWIDGET
    kmDebug() << "User picture unchanged, not updating widget";
#endif
    return;
  }

#ifdef KMESSDEBUG_CONTACTSWIDGET
  kmDebug() << "Updating user picture widget";
#endif

  // Change the picture
  userDisplayPicture_->setPicture( pictureFileName );
}



// Open the account settings
void ContactsWidget::slotUserDisplayPictureClicked()
{
  AccountSettingsDialog *accountSettingsDialog = AccountSettingsDialog::instance();
  accountSettingsDialog->loadSettings( Account::connectedAccount );
  accountSettingsDialog->show();
}



#include "contactswidget.moc"
