/***************************************************************************
                          chatmaster.cpp  -  description
                             -------------------
    begin                : Sat Jan 18 2003
    copyright            : (C) 2003 by Mike K. Bennett
    email                : mkb137b@hotmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "chatmaster.h"

#include <KMess/MsnObject>
#include <KMess/MsnContact>

#include "../contact/contactextension.h"

#include "../kmessglobal.h"
#include "../kmessapplication.h"
#include "../kmessdebug.h"
#include "../utils/kmessconfig.h"

#include "chat.h"
#include "chatwindow.h"

#include <KMess/TextMessage>
#include <KMess/MsnChat>

#include <QFile>
#include <QImageReader>
#include <QTextDocument>
#include <QTest>

#include <KWindowSystem>

#ifdef Q_WS_X11
# include <QX11Info>
#endif



// The constructor
ChatMaster::ChatMaster(QObject *parent)
: QObject( parent )
{
  setObjectName( "ChatMaster" );

  // Every 30s call method for update commands.
  connect( &timer_, SIGNAL( timeout() ), this, SLOT( timedUpdate() ) );
  timer_.start( 30000 );
}



// The destructor
ChatMaster::~ChatMaster()
{
  disconnected();

  // Delete all open windows
  qDeleteAll( chatWindows_ );
  chatWindows_.clear();

#ifdef KMESSDEBUG_CHATMASTER
  kmDebug() << "DESTROYED.";
#endif
}



/**
 * @brief A new chat was created, add it to the list.
 */
void ChatMaster::chatCreated( KMess::MsnChat *chat )
{
  msnChats_.append( chat );
  createChat( chat );
}



// The connection has been established
void ChatMaster::connected()
{
  // Connect the contact list
  // TODO: move ChatMaster into KMess::MsnSession, or at *least* connect to
  // MsnSession signals instead of ContactList signals

  // needs to be looked at - we should not be attaching to signals in the contact list model anymore.
  // Update chat grouping when settings are changed
  connect( Account::connectedAccount, SIGNAL(    changedChatStyleSettings() ),
           this,                      SLOT  (          updateChatGrouping() ) );

  // Get the tabbed chat settings
  chatTabbingMode_ = Account::connectedAccount->getSettingInt( "ChatTabbingMode" );

  // There are no chat windows to reactivate
  if( chatWindows_.isEmpty() )
  {
    return;
  }

#ifdef KMESSDEBUG_CHATMASTER
  kmDebug() << "Trying reactivation of" << chatWindows_.count() << "open chats.";
#endif

  QList<ChatWindow*> windowsList;

  // Go through all the chats and reactivate them
  foreach( ChatWindow *chatWindow, chatWindows_ )
  {
    QList<Chat*> chats( chatWindow->getChats() );
    foreach( Chat *chat, chats )
    {
      // Reactivate right away chats which have a connection already. Should never happen though.
      if( chat->getMsnChat()->isConnected() )
      {
        chat->setEnabled( true );

        // Mark this chat's window for reactivation
        if( ! windowsList.contains( chatWindow ) )
        {
          windowsList.append( chatWindow );
        }

        continue;
      }

      ContactsList participants( chat->getMsnChat()->participants() );

#ifdef KMESSDEBUG_CHATMASTER
      kmDebug() << "Reactivating chat:" << participants;
#endif

      // Check if the participants list is empty: if so, there's something fishy going on.
      if( participants.isEmpty() )
      {
        kmWarning() << "Tried to reactivate chat with no contacts - has a window?" << ( chatWindow != 0 );
        chatWindow->removeChatTab( chat );
        continue;
      }

      // Do not re-enable group chats, as they need a new invitation to work again
      if( participants.count() > 1 )
      {
#ifdef KMESSDEBUG_CHATMASTER
        kmDebug() << "This is a group chat, skipping it.";
#endif
       continue;
      }

      KMess::MsnContact *contact = participants.first();

      // Only reactivate the chat if this contact is in the connected account's list
      if( contact->isUnknown() )
      {
#ifdef KMESSDEBUG_CHATMASTER
        kmDebug() << "This chat is with a non-listed contact, closing it.";
#endif
        chatWindow->removeChatTab( chat );
        continue;
      }

#ifdef KMESSDEBUG_CHATMASTER
      kmDebug() << "Reactivating chat with listed contact:" << contact;
#endif

      // Create a new MsnChat object, with a hint about who to call if
      // the user wants to chat.
      KMess::MsnChat *msnchat = globalSession->createMsnChat( contact->handle() );
      // The chat object will already be added into msnChats_ at this point.
      chat->setMsnChat( msnchat );
      chat->setEnabled( true );

      // Mark this chat's window for reactivation
      if( ! windowsList.contains( chatWindow ) )
      {
        windowsList.append( chatWindow );
      }
    }
  }


  // Go through all the windows and reactivate them
  foreach( ChatWindow *window, windowsList )
  {
    window->setUIState( ChatWindow::Connected );
  }
}



// The user has disconnected, so close all open switchboard connections and disable the chats
void ChatMaster::disconnected()
{
  // Don't waste time disabling the chats when exiting.
  if( KMessApplication::instance()->quitSelected() )
  {
    return;
  }

  // Disconnect the old session's signals to avoid duplicated connections if we reconnect
  disconnect( Account::connectedAccount, 0, this, 0 );

#ifdef KMESSDEBUG_CHATMASTER
  kmDebug() << "Disabling" << chatWindows_.count() << "open chat windows.";
#endif

  // Go through all the chats
  foreach( ChatWindow *chatWindow, chatWindows_ )
  {
    chatWindow->setUIState( ChatWindow::Disconnected );
    QList<Chat*> chats( chatWindow->getChats() );

    // Go through all the chats and disable them
    foreach( Chat *chat, chats )
    {
      chat->setEnabled( false );
      // Save it now, because it can't be saved once currentAccount is gone
      chat->saveChatAutomatically();
    }
  }

  // close down all switchboards first.
  // TODO: is this our task or will it happen automatically?
#ifdef __GNUC__
#warning who will do this?
#endif
#ifdef KMESSDEBUG_CHATMASTER
  kmDebug() << "Cleaning up" << msnChats_.count() << "open MsnChat objects.";
#endif

  // give each switchboard a chance to close properly.
  foreach( KMess::MsnChat *chat, msnChats_ )
  {
    chat->destroy();
  }

  // Cleanup MsnChats.
  msnChats_.clear();
}



// KMess is establishing a connection.
void ChatMaster::connecting()
{
  // There are no chats modify.
  if( chatWindows_.isEmpty() )
  {
    return;
  }

#ifdef KMESSDEBUG_CHATMASTER
  kmDebug() << "Disabling reconnect button of"  << chatWindows_.count() << " open chat windows.";
#endif

  // Go through all the windows and disable their reconnect button.
  foreach( ChatWindow *window, chatWindows_ )
  {
    window->setUIState( ChatWindow::Connecting );
  }
}



// Get all chats in all chat windows
QList<Chat*> ChatMaster::getAllChats()
{
  if( chatWindows_.isEmpty() )
  {
    return QList<Chat*>();
  }

  QList<Chat*> chats;
  foreach( ChatWindow *chatWindow, chatWindows_ )
  {
    chats.append( chatWindow->getChats() );
  }

  return chats;
}



// Return the chat for the given MsnChat
Chat* ChatMaster::getChat( KMess::MsnChat *chat )
{
  if( chatWindows_.isEmpty() )
  {
    return 0;
  }

  foreach( ChatWindow *chatWindow, chatWindows_ )
  {
    QList<Chat*> chats( chatWindow->getChats() );

    foreach( Chat *currentChat, chats )
    {
      if( currentChat->getMsnChat() == chat )
      {
        return currentChat;
      }
    }
  }

  return 0;
}



// Return the window for the given chat
ChatWindow *ChatMaster::getChatWindow( Chat *chat )
{
  if( chatWindows_.isEmpty() )
  {
    return 0;
  }

  foreach( ChatWindow *chatWindow, chatWindows_ )
  {
    QList<Chat*> chats( chatWindow->getChats() );

    foreach( const Chat *currentChat, chats )
    {
      if( currentChat == chat )
      {
        return chatWindow;
      }
    }
  }

  return 0;
}



// Return the chat where we're having an conversation with the given contact.
Chat *ChatMaster::getContactsChat( const QString &handle, bool privateChat )
{
  Chat *result = 0;

  // Look through the chats for an exclusive one with the contact given
  QList<Chat*> chats( getAllChats() );
  foreach( Chat *chat, chats )
  {
    if( chat->isContactInChat( handle, true ) )
    {
      return chat; // best candidate found
    }
    else if( ! privateChat && chat->isContactInChat( handle ) )
    {
      result = chat;  // keep it as a second choice
    }
  }

  return result;
}



// Return the chat where we're having a conversation with the given contacts.
// The privateChat parameter is ignored.
Chat *ChatMaster::getContactsChat( const ContactsList contacts, bool privateChat )
{
  // Do nothing if there are no open chats or if the list is empty
  if( contacts.isEmpty() || chatWindows_.isEmpty() )
  {
    return 0;
  }

  // If the list only has one contact, just call the single handle version of this method.
  if( contacts.count() == 1 )
  {
    return getContactsChat( contacts.first()->handle(), privateChat );
  }

  // Look through the chats for a group chat with all the contacts of the list
  QList<Chat*> chats( getAllChats() );
  foreach( Chat *chat, chats )
  {
    bool found = true;
    // Check every contact in the list to see if they're all present in this chat
    foreach( const KMess::MsnContact *contact, contacts )
    {
      if( ! chat->isContactInChat( contact->handle() ) )
      {
        found = false;
        break;
      }
    }

    if( found )
    {
      return chat;
    }
  }

  return 0;
}



/**
 * Retrieve the chat with this contact as only contact, or 0 if there are none.
 */
KMess::MsnChat *ChatMaster::getExclusiveChatWith( const QString &handle ) const
{
  foreach( KMess::MsnChat *check, msnChats_ )
  {
    if( check->isPrivateChatWith( handle ) )
    {
      return check;
    }
  }

  return 0;
}



// Check whether the contact is in any of the existing chat windows.
bool ChatMaster::isContactInChat( const QString &handle )
{
  // Find the chat where we have an exclusive chat with the given contact
  return ( getContactsChat( handle, false ) != 0 );
}



// Verify if a new chat can be opened and requests a switchboard
void ChatMaster::requestChat( const QString &handle )
{
  // Forbid chatting with yourself
  if( handle == globalSession->sessionSettingString( "AccountHandle" ) )
  {
    kmWarning() << "Cannot chat with yourself.";
    return;
  }

  // Look through the chats for an exclusive chat with the given contact
  Chat *chat = getContactsChat( handle, true );

  // No need to open another chat, notify about the presence of the existing one.
  if( chat != 0 )
  {
    raiseChat( chat, true );
    return;
  }

#ifdef KMESSDEBUG_CHATMASTER
  kmDebug() << "Requesting a switchboard to chat with " << handle;
#endif

  // Check if a private chat is already open with this contact
  KMess::MsnChat *msnchat = getExclusiveChatWith( handle );
  if( msnchat != 0 )
  {
    // The user request chat with user that have already opened one MsnChat
    // So, re-use the MsnChat and force the raise of chat!
    createChat( msnchat, true );
    return;
  }

  // Append the requested chat with handle to the list
  // So when the chat will be created it will be raised
  requestedChats_.append( handle );
  msnchat = globalSession->createMsnChat( handle );
  msnchat->startConnecting();
}



// A chat is closing
void ChatMaster::slotChatClosing( Chat *chat )
{
#ifdef KMESSDEBUG_CHATMASTER
  kmDebug() << "Marking chat as closing.";
#endif

  // The contains check avoids deleting references to old switchboards which may be outdated and don't exist anymore.
  KMess::MsnChat *msnchat = chat->getMsnChat();
  if( msnchat && msnChats_.contains( msnchat ) )
  {
    msnchat->destroy();
    msnChats_.removeAll( msnchat );
  }
  else
  {
    kmWarning() << "Chat object had no or an unknown MsnChat object associated"
               << "with it!";
  }
}



// A chat window was destroyed
void ChatMaster::slotChatWindowDestroyed( QObject *chatWindow )
{
#ifdef KMESSDEBUG_CHATMASTER
  kmDebug() << "Removing chat window from list.";
#endif

  // Remove from the list
  ChatWindow *window = static_cast<ChatWindow*>( chatWindow );

  chatWindows_.removeAll( window );
}



// Raise an existing chat window. Forcing a raise is reasonable only when the user requests to, since it makes the window
// to grab keyboard focus and be raised on top of other windows.
void ChatMaster::raiseChat( Chat *chat, bool force )
{
  // Check if the pointer is valid
  if( chat == 0 )
  {
#ifdef KMESSDEBUG_CHATMASTER
    kmDebug() << "Invalid raising request received; chat is probably already closed.";
#endif
    return;
  }

  ChatWindow *chatWindow = getChatWindow( chat );
  if( chatWindow == 0  )
  {
#ifdef KMESSDEBUG_CHATMASTER
    kmDebug() << "Invalid raising request received; chat is probably already closed.";
#endif
    return;
  }

  if( chatWindow->isVisible() && ! force )
  {
#ifdef KMESSDEBUG_CHATMASTER
    kmDebug() << "Window has focus already.";
#endif
    return;
  }

  // Move the window on the current desktop if it wasn't shown before or if we're forcing its raise
  // TODO: make this optional, maybe the user doesn't want the windows to span over desktops
  if( ! chatWindow->isVisible() || force )
  {
    KWindowSystem::setOnDesktop( chatWindow->winId(), KWindowSystem::currentDesktop() );
  }

  // Making windows raise and get focus interrupts the user... it's better to make them appear minimized,
  // and let them just flash in the taskbar.
  if( ! force )
  {
#ifdef KMESSDEBUG_CHATMASTER
    kmDebug() << "Showing minimized window.";
#endif
    // Required for new chats
    chatWindow->showMinimized();
    return;
  }

  // Activate the selected tab
  chatWindow->setCurrentChat( chat );

  // Notify about the new window and force it to show and receive focus. Only use upon user request.

#ifdef KMESSDEBUG_CHATMASTER
  kmDebug() << "Forcing window raise.";
#endif

  // Show the chat's window
  if( chatWindow->isMinimized() )
  {
#ifdef KMESSDEBUG_CHATMASTER
    kmDebug() << "Window was minimized, showing it.";
#endif
    chatWindow->showNormal();
  }
  else
  {
#ifdef KMESSDEBUG_CHATMASTER
    kmDebug() << "Window was not minimized, showing it again.";
#endif
    chatWindow->show();
  }

#ifdef Q_WS_X11
  // Bypass focus stealing prevention (code from Quassel)
  QX11Info::setAppUserTime( QX11Info::appTime() );
#endif

  // Workaround a bug in KWin: force the window to get focus
  if( chatWindow->windowState() & Qt::WindowMinimized) {
    chatWindow->setWindowState( (chatWindow->windowState() & ~Qt::WindowMinimized) | Qt::WindowActive );
    chatWindow->show();
  }

  // Set the keyboard focus to the chat window
  chatWindow->raise();
  KWindowSystem::activateWindow( chatWindow->winId() );
  KApplication::setActiveWindow( chatWindow );
}



// Delete an existing MsnChat
void ChatMaster::slotMsnChatDelete( KMess::MsnChat *closing )
{
  // Remove from collection
  msnChats_.removeAll( closing );

  // Disconnect
  disconnect( closing, 0, this, 0 );
}



// Start a chat with the associated MsnChat object
Chat *ChatMaster::createChat( KMess::MsnChat *msnChat, bool requestedByUser )
{
  Chat       *newChat;
  ChatWindow *newChatWindow;

  // If the new chat is a group chat, find existing group chat windows; else find private chats
  const ContactsList participants( msnChat->participants() );
  newChat = getContactsChat( participants, true );

  // If the chat was requested by a contact, check if we have a chat request
  // pending for that contact and remove it
  if( ! requestedByUser && participants.count() < 2 )
  {
    const KMess::MsnContact *first = participants.first();
    // Also change the request source to the user if we asked a chat with this contact first
    requestedByUser = requestedChats_.contains( first->handle() );
    if( requestedByUser )
    {
      // Remove the request
      requestedChats_.removeAll( first->handle() );
    }
  }

  if( newChat != 0 )
  {
#ifdef KMESSDEBUG_CHATMASTER
    kmDebug() << "A chat with contacts already exists, raising it.";
#endif

    // The chat may have been disabled, reenable it just in case.
    // Seen happening with group chats where you get reinvited to after a reconnection.
    newChat->setEnabled( true );

    // Delete the previous MsnChat object (if it's different from the current one) and attach the new
    KMess::MsnChat *oldMsnChat = newChat->getMsnChat();
    if( oldMsnChat != msnChat )
    {
      newChat->setMsnChat( msnChat );
      slotMsnChatDelete( oldMsnChat );
#ifdef __GNUC__
#warning todo
#endif
      // TODO: oldMsnChat won't be deleted. In fact, this whole routine
      // should not be needed since MsnChat objects should be able to last.
    }

    // If we've requested a chat window, raise it forcing it open over other KMess' windows;
    // if some contact wants to chat with us, the chat window will open minimized.
    raiseChat( newChat, requestedByUser );
    return newChat;
  }

  // Create the chat widget
  newChat = new Chat();

  if( newChat == 0 )
  {
    kmWarning() << "Couldn't create a new chat tab.";
    return 0;
  }

  newChat->setMsnChat( msnChat );

  // Select the chat window where to open the new tab, or create a new one
  newChatWindow = createChatWindow( newChat );

  // Create the first tab in the chat
  // TODO: msnChat->getUserStartedChat() can't do this, making it always true
#ifdef __GNUC__
#warning getUserStartedChat()
#endif
  newChat = newChatWindow->addChatTab( newChat, true );
  if( newChat == 0 )
  {
    kmWarning() << "Couldn't add the initial chat.";
    return 0;
  }

  // Connect the chat's request signals
  connect( newChat, SIGNAL(                  closing(Chat*)                   ),
           this,    SLOT  (          slotChatClosing(Chat*)                   ) );
  connect( this,    SIGNAL( updateApplicationMessage(QString,QString)         ),
           newChat, SIGNAL( updateApplicationMessage(QString,QString)         ) );

  // Add the new chat to our list
  //chats_.append( newChat );

  // Finally, start the new chat
  newChat->startChat();

  // If we've requested a chat window, raise it forcing it open over other KMess' windows;
  // if some contact wants to chat with us, the chat window will open minimized.
  raiseChat( newChat, requestedByUser );

  return newChat;
}



// Create a new chat window or retrieve an existing one if tabbed chatting is enabled
ChatWindow *ChatMaster::createChatWindow( Chat *chat )
{
  // Search a window for the chat according to the users grouping settings
  ChatWindow *window = findWindowForChat( chat );

  // Found a window to use, return that
  if( window != 0 )
  {
    return window;
  }

  // Create the new chat window
#ifdef KMESSDEBUG_CHATMASTER
  kmDebug() << "No existing chat window, creating new.";
#endif
  // Create the window: it should have no parent, so it will be displayed in the taskbar for Windows.
  window = new ChatWindow();

  // Bail out if the window could not be initialized
  if( ! window->initialize() )
  {
    kmWarning() << "Couldn't initialize the new chat window!";
    return 0;
  }

  // Connect its signals
  connect( window, SIGNAL(               destroyed(QObject*) ),
           this,   SLOT  ( slotChatWindowDestroyed(QObject*) ) );
  connect( window, SIGNAL(               reconnect()         ),
           this,   SIGNAL(               reconnect()         ) );

  // Add the new chat window to our list
  chatWindows_.append( window );

  return window;
}



/**
 * Periodically called method for update commands.
 *
 * This method is synchronized with the ping timer of MsnNotificationConnection
 * to avoid multiple timers which reduces power consumption (see www.linuxpowertop.org).
 *
 * Currently this is used to download display pictures in the background,
 * but it can also be used for other events.
 */
void ChatMaster::timedUpdate()
{
  // Handle the queue of display pictures to download, but only if we're not hidden
  if( pendingDisplayPictures_.count() == 0 || globalSession->self()->status() ==  KMess::InvisibleStatus )
  {
    return;
  }

#ifdef KMESSDEBUG_CHATMASTER
  kmDebug() << "handling timed events.";
#endif

  // See if there are pictures to download.
  // Get 4 pictures at once.
  int requestCount = pendingDisplayPictures_.count();
  requestCount = requestCount > 4 ? 4 : requestCount;
  while( requestCount > 0 && ! pendingDisplayPictures_.isEmpty() )
  {
    // Unshift the first handle of the list.
    QString handle( pendingDisplayPictures_.first() );
    pendingDisplayPictures_.removeAll( handle );

#ifdef KMESSDEBUG_CHATMASTER
    kmDebug() << "see if the display picture of '" << handle << "' should  be downloaded.";
#endif

    // See if the picture was already downloaded somehow (e.g. user opened a chat)

    // First get the contact.
    // Using getContactList()->.. because it skips the InvitedContact objects returned by globalSession->getContact()
    KMess::MsnContact *contact = globalSession->contactList()->contact( handle );
    if( contact == 0 )
    {
#ifdef KMESSDEBUG_CHATMASTER
      kmDebug() << "contact not found, was it removed?";
#endif
      continue;
    }

    // See if the contact is still online.
    if( contact->isOffline() )
    {
#ifdef KMESSDEBUG_CHATMASTER
      kmDebug() << "contact is no longer online.";
#endif
      continue;
    }

    // See if the contact is still blocked.
    // Avoid starting a chat which "invites" the contact to chat back.
    if( contact->isBlocked() )
    {
#ifdef KMESSDEBUG_CHATMASTER
      kmDebug() << "contact is blocked.";
#endif
      continue;
    }

    // Get the msn object, can become null now.
    const KMess::MsnObject msnObject ( contact->msnObject() );

#ifdef __GNUC__
    #warning TODO: Picture downloading. Look at this whole area. Commented out for sanity.
#endif
#if 0
    // See if the object already exists in the cache
    QString objectFileName( KMessConfig::instance()->getMsnObjectFileName( *msnObject ) );
    if( QFile::exists( objectFileName )
    &&  ! QImageReader::imageFormat( objectFileName ).isEmpty()
    &&  msnObject->verifyFile( objectFileName ) )
    {
#ifdef KMESSDEBUG_CHATMASTER
      kmDebug() << "Picture is already downloaded.";
#endif
      continue;
    }

    // All tests passed.
    // Download the picture.

#ifdef KMESSDEBUG_CHATMASTER
    kmDebug() << "Picture is not available, starting switchboard connection to download it.";
#endif

    // Start a chat to download the picture.
    // The download will be initiated up by ChatMaster::slotContactJoinedChat().
    KMess::MsnChat *msnChat = globalSession->createMsnChat( handle );
    msnChat->startConnecting();
    requestCount--;
#endif
  }
}



// Update the grouping of chats/tabs.
void ChatMaster::updateChatGrouping()
{
  int currentChatTabbingMode = Account::connectedAccount->getSettingInt( "ChatTabbingMode" );
  if( currentChatTabbingMode == chatTabbingMode_ )
  {
#ifdef KMESSDEBUG_CHATMASTER
    kmDebug() << "Chat grouping mode not changed, nothing to update";
#endif
    return;
  }

#ifdef KMESSDEBUG_CHATMASTER
  kmDebug() << "Updating chat grouping";
#endif

  chatTabbingMode_ = currentChatTabbingMode;

  if( chatWindows_.empty() )
  {
#ifdef KMESSDEBUG_CHATMASTER
    kmDebug() << "No ChatWindows, nothing to update";
#endif
    return;
  }

  ChatWindow *window;

  foreach( ChatWindow *chatWindow, chatWindows_ )
  {
    QList<Chat*> chats( chatWindow->getChats() );
    foreach( Chat *chat, chats )
    {
      window = findWindowForChat( chat );

      if( window == 0 )
      {
#ifdef KMESSDEBUG_CHATMASTER
        kmDebug() << "No suited window found for chat" << chat;
#endif

        ChatWindow* newChatWindow = createChatWindow( chat );
        newChatWindow->addChatTab( chat, false );
        chatWindow->removeChatTab( chat );
        chatWindow->checkAndCloseWindow();

        raiseChat( chat, false );
      }
      else if( window != chatWindow )
      {
#ifdef KMESSDEBUG_CHATMASTER
        kmDebug() << "Moving between windows chat" << chat << ": from" << chatWindow << "to" << window;
#endif

        window->addChatTab( chat, false );
        chatWindow->removeChatTab( chat );
        chatWindow->checkAndCloseWindow();

        raiseChat( chat, false );
      }
   }
  }
}



// Search the appropriate window for a chat.
ChatWindow *ChatMaster::findWindowForChat( Chat *chat )
{
  ChatWindow *window = 0;
  ChatWindow *otherGroupWindow = 0;

  if( ! chatWindows_.empty() )
  {
#ifdef KMESSDEBUG_CHATMASTER
    kmDebug() << "At least one chat window is already open: checking the tabbed chatting settings.";
#endif

    if( Account::connectedAccount->getSetting( "ChatTabbingMode" ) == 0 ) // Always group in tabs
    {
      // Pick the last open chat window
      window =  chatWindows_.last();

#ifdef KMESSDEBUG_CHATMASTER
      kmDebug() << "Always group in tabs. Picking the last created window.";
#endif
    }
    else if( Account::connectedAccount->getSetting( "ChatTabbingMode" ) == 1 ) // Group tabs by initial contact's group
    {
#ifdef KMESSDEBUG_CHATMASTER
      kmDebug() << "Group in tabs by contact group. Choosing a window.";
#endif
      const KMess::MsnContact *contact = chat->getMsnChat()->firstContact();

      if( contact && ! contact->groups().isEmpty() )
      {
        KMess::MsnGroup *group = contact->groups().first();
#ifdef KMESSDEBUG_CHATMASTER
        kmDebug() << "Searching chats for one whose first contact is in the same group as the new one's.";
#endif
        foreach( ChatWindow *chatWindow, chatWindows_ )
        {
          QList<Chat*> chats( chatWindow->getChats() );
          foreach( Chat *chatItem, chats )
          {
            const KMess::MsnContact *contact = chatItem->getMsnChat()->firstContact();

            if( contact && ! contact->groups().isEmpty()
            && contact->groups().first() == group )
            {
#ifdef KMESSDEBUG_CHATMASTER
              kmDebug() << "Match found!";
#endif
             window = chatWindow;

              // We found a possibly suited window.
             // Check if it already displays tabs of another group.
             // If so, we can't use it.
             if( otherGroupWindow == window )
             {
               window = 0;
             }

              break;
           }
           else
            {
             // Keep track of the window used by another group.
             otherGroupWindow = chatWindow;
           }
          }
        }
      }
    }
  }

  return window;
}



#include "chatmaster.moc"
