/***************************************************************************
                          account.h  -  description
                             -------------------
    begin                : Sun Jan 5 2003
    copyright            : (C) 2003 by Mike K. Bennett
    email                : mkb137b@hotmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef ACCOUNT_H
#define ACCOUNT_H

#include "contact/status.h"
#include "utils/richtextparser.h"

#include <QFont>
#include <QVariant>

// Forward declarations
class KMessTest;
class Account;


// Custom types declarations
#if ! defined( ACCOUNT_SETTINGS_LIST )
  #define ACCOUNT_SETTINGS_LIST
  typedef QHash        <QString,QVariant> SettingsList;
  typedef QHashIterator<QString,QVariant> SettingsListIterator;
#endif



/**
 * @brief Data class for user account settings.
 *
 * This class manages the data specific to every user account. It holds all data that belongs to
 * this particular Account. All volatile (session) data is stored in KMess::MsnSession.
 *
 * Previously, when logging in, CurrentAccount would copy all account data from Account, and at
 * logout, the account data would be copied back and saved to disk. This is not the case anymore;
 * now, Account is the place where all account data should be kept. The currently logged-in account
 * can be retrieved using Account::connectedAccount, which is zero if nobody is logged in.
 *
 * Some properties, the most important ones, are handled with dedicated get/set methods; all the
 * others however are accessible by using the getSetting()/setSetting()/setSettings() methods.
 *
 * Each setting is accessible by name, like "ChatStyle", and is stored as a QVariant. There are
 * some quick-access methods to avoid converting manually the most popular data types (ie
 * getSettingInt()) but for other types you must use getSetting("name") and cast using the QVariant
 * methods.
 * There is a static list of default settings, filled up in the ctor, which is used to minimize the
 * amount of data written (only settings different than the default are saved) and improve the
 * memory footprint of the settings system.
 * To add new settings, just add a new entry with its default value in the constructor.
 *
 * You cannot use names not present in the default settings list: KMess will terminate. This is
 * to avoid spelling mistakes from going unnoticed.
 *
 * @author Mike K. Bennett
 * @author Valerio Pilo
 * @ingroup Root
 * @todo Update all Account/CurrentAccount/KMess::MsnSession documentation to reflect the current situation
 */
class Account : public QObject
{
  Q_OBJECT

  friend class KMessTest;

  public:  // Public enumerations
  static Account *connectedAccount;
    /// Directory structure constants
    enum ChatDirectoryStructure
    {
      SINGLEDIRECTORY = 0 /// Put all log files in the same dir
    , BYYEAR          = 1 /// Divide the log files by year directories
    , BYMONTH         = 2 /// Divide the log files by year and then month directories
    , BYDAY           = 3 /// Divide the log files by year, month and then day directories
    };
    /// Available chat exporting formats
    enum ChatExportFormat
    {
      EXPORT_XML    /// Export the chat to XML
    , EXPORT_HTML   /// Export the chat to HTML
    , EXPORT_TEXT   /// Export the chat to text
    };
    /// Contact List view organization constants
    enum ContactListDisplayMode
    {
      VIEW_BYGROUP    /// Show the user-defined groups
    , VIEW_BYSTATUS   /// Divide contacts between online and offline ones
    , VIEW_MIXED      /// Show the user-defined groups and group offline contacts together
    };

  public:  // Public methods
    // The constructor
                                 Account();
    // The destructor
    virtual                     ~Account();
    // Return the user's friendlyName
    QString                      getFriendlyName( FormattingMode mode = STRING_CLEANED ) const;
    // Return the user's handle
    const QString                &getHandle() const;
    // Return the user's password
    const QString                &getPassword() const;
    // Return the personal message
    QString                      getPersonalMessage( FormattingMode mode = STRING_CLEANED ) const;
    // Retrieve a setting's value
    const QVariant               getSetting( const QString &name ) const;
    // Return the path to the *current* display picture
    const QString                getDisplayPicture() const;
    // Whether the account has unsaved settings changes
    bool                         isDirty() const;
    // Whether the account is a guest account, not permanently saved on the computer
    bool                         isGuestAccount() const;
    // Read in account properties
    void                         readProperties( const QString &handle );
    // Revert a setting to its default value
    void                         revertSetting( const QString &name );
    // Save account properties
    void                         saveProperties();
    // Whether the account is a guest account, not permanently saved on the computer
    void                         setGuestAccount( bool guestAccount );
    // Change the current handle (read Doxygen comments!)
    void                         setHandle( const QString &handle );
    // Set the account's login password
    void                         setPassword( const QString &password );
    // Change a single setting
    void                         setSetting( const QString &name, const QVariant &value );
    // Change a list of settings
    void                         setSettings( const SettingsList& changes );

  public:  // Public inline methods
    // Retrieve a setting's value in different forms
    inline bool                  getSettingBool  ( const QString &name ) const { return getSetting( name ).toBool();   };
    inline QFont                 getSettingFont  ( const QString &name ) const { QFont f; f.fromString( getSetting( name ).toString() ); return f; };
    inline int                   getSettingInt   ( const QString &name ) const { return getSetting( name ).toInt();    };
    inline QString               getSettingString( const QString &name ) const { return getSetting( name ).toString(); };

  public slots:
    // The friendly name was changed in the global session, update it here
    void                         friendlyNameChanged();
    // The personal message was changed in the global session, update it here
    void                         personalMessageChanged();

  public: // Static public members
    // Returns whether a setting exists (see also ensureSettingExists)
    static bool                  settingExists( const QString &name );

  private: // Private members
    // Ensure a setting exists, or crash otherwise
    void                         ensureSettingExists( const char *name ) const;

  private: // Private attributes

    // Whether the account settings have been changed or not
    bool                         dirty_;
    // The user's friendly name, i.e "Mike"
    // It is kept in sync with QString KMess::MsnSession::friendlyName_.
    FormattedString              friendlyName_;
    // Whether the account is a "guest account", not permanently saved on the computer
    bool                         guestAccount_;
    // The user's account handle, i.e. kmess@test.com
    QString                      handle_;
    // The user's password. Empty when it's not being saved
    QString                      password_;
    // the user's personal message
    FormattedString              personalMessage_;
    // The name-to-value hash of account settings
    SettingsList                 settings_;

  private: // Private static attributes
    // The name-to-value hash of default values for account settings
    static SettingsList          settingsDefaults_;

  signals: // Public signals
    // Signal that the chat style settings have been changed
    void                         changedChatStyleSettings();
    // Signal that the contact list settings have been changed
    void                         changedContactListOptions();
    // Signal that group/contact display settings have been changed
    void                         changedDisplaySettings();
    // Signal that the display picture has been changed
    void                         changedDisplayPicture();
    // Signal that the user changed email display settings
    void                         changedEmailDisplaySettings();
    // Signal that the emoticon settings changed
    void                         changedEmoticonSettings();
    // Signal that font settings have changed
    void                         changedFontSettings();
    // Signal that chat logging settings have changed
    void                         changedLoggingSettings();
    // Signal that the now listening settings have changed
    void                         changedNowListeningSettings();
    // Signal that timer settings were changed
    void                         changedTimerSettings();
    // Signal that the view mode has changed
    void                         changedViewMode();
};

#endif
