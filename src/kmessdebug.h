/***************************************************************************
                          kmessdebug.h  -  description
                             -------------------
    begin                : Sun Jan 5 2003
    copyright            : (C) 2003 by Mike K. Bennett
    email                : mkb137b@hotmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef KMESSDEBUG_H
#define KMESSDEBUG_H

// #warning make the compiler to shut the fuck up about duplicate definitions (between here and libkmessdebug.h)
#define LIBKMESSDEBUG_H

#include "config-kmess.h"

#include <KDebug>


extern int debugArea();

#define kmDebug()    kDebug( debugArea() )
#define kmWarning()  kWarning( debugArea() )
#define kmError()    kError( debugArea() )


#if ( KMESS_DEBUG == 1 )
  #define KMESSTEST

  #define KMESSDEBUG_MAINWINDOW
  #define KMESSDEBUG_LIKEBACK
  #define KMESSDEBUG_KMESSAPPLICATION
  #define KMESSDEBUG_KMESSINTERFACE
  #define KMESSDEBUG_KMESSVIEW
  #define KMESSDEBUG_INITIALVIEW
  #define KMESSDEBUG_ACCOUNTSETTINGSDIALOG
  #define KMESSDEBUG_SETTINGSDIALOG
  #define KMESSDEBUG_CONTACTPROPERTIES
  #define KMESSDEBUG_TRANSFERENTRY
  #define KMESSDEBUG_TRANSFERWINDOW
  #define KMESSDEBUG_CHATHISTORYDIALOG
  #define KMESSDEBUG_WELCOMEDIALOG
  #define KMESSDEBUG_SCRIPTCONSOLEDIALOG

  #define KMESSDEBUG_ACCOUNT
  #define KMESSDEBUG_ACCOUNTSMANAGER
  #define KMESSDEBUG_CONTACT
  #define KMESSDEBUG_CONTACTBASE
  #define KMESSDEBUG_CONTACTEXTENSION
  #define KMESSDEBUG_CONTACTLIST
//   #define KMESSDEBUG_CONTACTLISTMODELTEST
//  #define KMESSDEBUG_CONTACTLISTMODEL
  #define KMESSDEBUG_GROUP

  #define KMESSDEBUG_CONTACTLISTWIDGET
  #define KMESSDEBUG_DISPLAYPICTUREWIDGET
  #define KMESSDEBUG_DYNAMICTOOLTIP

  #define KMESSDEBUG_CHATMASTER
  #define KMESSDEBUG_CHAT
  #define KMESSDEBUG_CHATWINDOW
  #define KMESSDEBUG_CHATVIEW
  #define KMESSDEBUG_CHATMESSAGEVIEW
  #define KMESSDEBUG_CONTACTSWIDGET
  #define KMESSDEBUG_EMOTICONSWIDGET
  #define KMESSDEBUG_WINKSWIDGET
  #define KMESSDEBUG_CONTACTFRAME
  #define KMESSDEBUG_INVITEDCONTACT
  #define KMESSDEBUG_INKEDIT

  #define KMESSDEBUG_EMOTICONS
//   #define KMESSDEBUG_RICHTEXTPARSER
//   #define KMESSDEBUG_IDLETIMER
  #define KMESSDEBUG_AUTOLOCK
  #define KMESSDEBUG_NOWLISTENINGCLIENT
  #define KMESSDEBUG_XSLTRANSFORMATION
  #define KMESSDEBUG_THUMBNAILPROVIDER
  #define KMESSDEBUG_SYSTEMTRAY
  #define KMESSDEBUG_CRASHHANDLER
  #define KMESSDEBUG_SHAREDMETHODS
  #define KMESSDEBUG_KMESSDBUS
  #define KMESSDEBUG_KMESSSENDPLUGIN
  #define KMESSDEBUG_APPSTYLES

  #define KMESSDEBUG_UPNP

  #define KMESSDEBUG_PASSIVEPOPUP
  #define KMESSDEBUG_NOTIFICATIONMANAGER
  #define KMESSDEBUG_CONTACTSTATUSNOTIFICATION
  #define KMESSDEBUG_CHATNOTIFICATION
  #define KMESSDEBUG_NEWEMAILNOTIFICATION
  #define KMESSDEBUG_STATUSNOTIFICATION
#endif


// k_funcinfo in c-string style.
#ifdef __GNUC__
#define c_funcinfo __PRETTY_FUNCTION__
#else
#define c_funcinfo "<unknown>"
#endif


// Debug macro to warn for null pointers
#ifdef KMESSTEST
  // Warns that a pointer was null, full debug version
  bool _kmessWarnNull(bool isNull, const char *var, const char *funcinfo, const char *file, int line);
  // Warn if a pre- or post- condition fails
  bool _kmessAssert(const char *condition, const char *file, int line);

  // Wrapper macro for warn-if-null methods.
  // This idea was taken from Q_ASSERT (qglobal.h).
  // Using KDE_ISUNLIKELY so the compiler optimizes branching.
  // Silent version:  #define KMESS_NULL(x) (x != 0)
  #define KMESS_NULL(x) ( KDE_ISUNLIKELY((x) == 0) && _kmessWarnNull((x) == 0, #x, c_funcinfo, __FILE__, __LINE__) )

  // Warn if a pre- or post- condition fails
  // Idea taken from Q_ASSERT, too, but without terminating the application;
  // this is often useful with P2P code, some details may change and using KMESS_ASSERT
  // KMess would die a lot.
  //#define KMESS_ASSERT(cond) ( KDE_ISUNLIKELY((cond) == 0) && _kmessAssert( #cond, __FILE__, __LINE__) )

  // adam (14Aug10): we can replace the above statement once master stablises.
  #define KMESS_ASSERT( cond ) Q_ASSERT( cond )
#else
  // Warns that a pointer was null
  bool _kmessWarnNull(bool isNull, const char *var, const char *funcinfo);

  #define KMESS_NULL(x) ( KDE_ISUNLIKELY((x) == 0) && _kmessWarnNull((x) == 0, #x, c_funcinfo) )

  // Avoid asserts in release versions
  #define KMESS_ASSERT(cond)
#endif


// A Qt debug message handler to avoid unwanted exit(1) or abort() calls.
void  kmessDebugPrinter( QtMsgType type, const char *msg );


#endif  // KMESSDEBUG_H
