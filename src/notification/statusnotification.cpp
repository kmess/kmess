/***************************************************************************
                          statusnotification.cpp - notifies about application status changes
                             -------------------
    begin                : Wed Jul 29 2009
    copyright            : (C) 2009 by Valerio Pilo <valerio@kmess.org>
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "statusnotification.h"

#include "../utils/kmessshared.h"
#include "../kmessdebug.h"
#include "notificationmanager.h"

#include <QTextDocument>

#include <KDialog>
#include <KLocale>
#include <KMessageBox>
#include <KNotification>



// Class constructor
StatusNotification::StatusNotification( NotificationManager* manager )
: manager_(manager)
{
  // Connect the activation signal to process notification actions
  connect( manager_, SIGNAL( eventActivated(NotificationManager::EventSettings,NotificationManager::Buttons) ),
           this,     SLOT  (       activate(NotificationManager::EventSettings,NotificationManager::Buttons) ) );
}



// Execute the action triggered in a notification
void StatusNotification::activate( NotificationManager::EventSettings settings, NotificationManager::Buttons button )
{
  // The signal wasn't meant for us
  if( settings.sender != this )
  {
    return;
  }

  KDialog *dialog;
  QString developerInfo;
  const QMap<QString,QVariant> &info = settings.data.toMap();

  switch( button )
  {
    case NotificationManager::BUTTON_MORE_DETAILS:
#ifdef KMESSDEBUG_STATUSNOTIFICATION
      kmDebug() << "Showing error details";
#endif

#ifdef KMESSDEBUG_STATUSNOTIFICATION
      // Add more details about the error in developer builds.
      developerInfo = i18nc( "Developer details placed on the network error dialog box",
                             "<p><b>Developer info:</b><br/>"
                             "<i>Error number:</i> %1<br/>"
                             "<i>Error string:</i> %2"
                             "</p>",
                             info["type"   ].toString(),
                             info["message"].toString() );
#endif

      // We need to create a non-modal informative message box: a modal one would block
      // execution here, and if the user dismisses the box after the notification has
      // already been deleted by KDE, kmess would crash.
      dialog = new KDialog( 0 );
      dialog->setAttribute( Qt::WA_DeleteOnClose );
      dialog->setButtons( KDialog::Ok );
      KMessageBox::createKMessageBox( dialog,
                                      QMessageBox::Information,
                                      info["description"].toString() + developerInfo,
                                      QStringList(),
                                      QString(),
                                      0,
                                      KMessageBox::Notify | KMessageBox::AllowLink | KMessageBox::NoExec,
                                      QString() );
      dialog->show();
      break;

    case NotificationManager::BUTTON_HIDE:
#ifdef KMESSDEBUG_STATUSNOTIFICATION
      kmDebug() << "Hiding status notification";
#endif
      // Do nothing
      break;

    default:
#ifdef KMESSDEBUG_STATUSNOTIFICATION
      kmDebug() << "Invalid button" << button;
#endif
      return;
  }
}



// Notify the user about this event (a status event has occurred)
void StatusNotification::notify( const KMess::StatusMessageType type, const KMess::StatusMessage message, const QVariant &extraInfo )
{
  // Save the error details in the event settings
  QMap<QString,QVariant> details;

  QString messageText;
  QString detailedText;

  // protocol level messages are internal errors.
  if ( type == KMess::ProtocolMessage )
  {
    messageText = i18nc( "Status event message",
                         "An internal error has occurred in KMess. Error code: %1" ).arg( extraInfo.toMap()[ "ErrorNumber" ].toString() );
  }
  else
  {
    // non-protocol messages
    switch( message )
    {
      /*
      * Warning messages
      */
      case KMess::ErrorOfflineIMServiceUnavailable:
        messageText = i18nc( "Status event message",
                            "The Offline Messages web service is currently not available." );
        break;
      case KMess::ErrorAddressBookServiceUnavailable:
        messageText = i18nc( "Status event message",
                            "The Live Messenger web service is experiencing problems." );
        break;

      /*
      * Error messages
      */
      case KMess::ErrorUnknown:
      case KMess::ErrorConnectionDropped:
      case KMess::ErrorSocketDataInvalid:
        messageText = i18nc( "Status event message", "Network connection lost." );
        break;
      case KMess::ErrorConnectingToSocket:
        messageText = i18nc( "Status event message", "KMess could not connect to the Live servers." );
        break;
      case KMess::ErrorConnectingToGateway:
        messageText  = i18nc( "Status event message",
                              "KMess could not connect to the Live server.<br/>"
                              "Maybe you need to authenticate before you can access the network?" );
        detailedText = i18nc( "Status event detailed message",
                              "KMess could not connect to the Live servers.<br/>"
                              "There may be a problem with your Internet connection, "
                              "or the Live servers may be temporarily unavailable.<br/>"
                              "It is also possible that an authentication to a web page or proxy "
                              "may be required to access the network.</p>"
                              "<p><a href='%1'>Click here</a> to visit the Messenger service "
                              "status page.",
                              "http://status.messenger.msn.com/Status.aspx" );
        break;
      case KMess::ErrorConnectionTimedOut:
        messageText = i18nc( "Status event message", "Connection not successful within time limit." );
        break;
      case KMess::ErrorConnectedFromElsewhere:
        messageText = i18nc( "Status event message", "Connected from another location." );
        break;
      case KMess::ErrorInvalidUserCredentials:
        messageText  = i18nc( "Status event message", "Authentication failed. Wrong login credentials." );
        detailedText = i18nc( "Status event detailed message",
                              "Authentication has failed, please verify "
                              "your account email and password." );
        break;
      case KMess::ErrorComputeHashFailed:
        messageText  = i18nc( "Status event message",
                              "Unable to perform authentication!<br/>"
                              "The Live network may be unavailable, or there might be a problem with your"
                              "internet connection or firewall." );
        detailedText = i18nc( "Status event detailed message",
                              "KMess could not connect to the Live servers.<br/>"
                              "Please check your internet connection and firewall!<br/>"
                              "It is also possible that the Live network is unavailable at "
                              "the moment." );
        break;
      case KMess::ErrorInternal:
      case KMess::ErrorSoapInternal:
        messageText  = i18nc( "Status event message",
                              "An internal error has occurred in KMess. Network connection lost." );
        break;

      case KMess::ErrorInvalidUser:
      case KMess::ErrorInvalidContactNetwork:
        messageText = i18nc( "Status event message", "The email address you have tried to add is not a Live Messenger account." );
        break;
      case KMess::ErrorContactListFull:
        messageText = i18nc( "Status event message", "Your contact list is full." );
        break;
      case KMess::ErrorUserNotOnline:
        messageText = i18nc( "Status event message", "This contact is not online." );
        break;
      case KMess::ErrorTooManyGroups:
        messageText = i18nc( "Status event message", "Your contact list has too many groups; you are allowed to only have at most 30." );
        break;
      case KMess::ErrorGroupNameTooLong:
        messageText = i18nc( "Status event message", "The group name is too long; it cannot be longer than 61 bytes." );
        break;
      case KMess::ErrorNotLoggedIn:
        messageText = i18nc( "Status event message", "This user has not logged in." );
        break;
      case KMess::ErrorInvalidAccountPermissions:
        messageText = i18nc( "Status event message", "This account was denied access to the Live service." );
        break;
      case KMess::ErrorServiceUnavailable:
        messageText = i18nc( "Status event message", "The Live Messenger service is temporarily unavailable. Try again later." );
        break;
      case KMess::ErrorAccountBanned:
        messageText = i18nc( "Status event message", "Your account was banned from the Live service!" );
        break;
      case KMess::ErrorServerGoingDown:
        messageText = i18nc( "Status event message", "The server is going down for maintenance. Network connection lost." );
        break;
      case KMess::WarningServerGoingDownSoon:
        messageText = i18nc( "Status event message", "The server will soon go down for maintenance..." );
        break;
      case KMess::ErrorTooManyChatSessions:
        messageText = i18nc( "Status event message", "You have too many open chats." );
        break;
      case KMess::ErrorKidsPassportNoParentalConsent:
        messageText = i18nc( "Status event message", "You have a Kids Passport account, you need parental consent to chat online." );
        break;
      case KMess::ErrorPassportNotVerified:
        messageText = i18nc( "Status event message", "Your Passport account needs to be verified first." );
        break;

      /*
      * Fatal messages
      */
      case KMess::FatalNoSupportedProtocols:
        messageText  = i18nc( "Status event message",
                              "Unable to connect!<br/>"
                              "The server does not support any of the protocols known by KMess!" );
        detailedText = i18nc( "Status event detailed message",
                              "KMess could not access the Live servers: Microsoft has "
                              "changed the MSN protocols in such a way that KMess can not "
                              "communicate with the server anymore! Please, contact the "
                              "KMess developers!" );
        break;

      default:
        // Do nothing
        return;
    }
  }

  // TODO: Format the messages in a better way
  details[ "type"        ] = (int) type;
  details[ "message"     ] = "<html><p>" + messageText  + "</p></html>";
  details[ "description" ] = "<html><p>" + detailedText + "</p></html>";

  NotificationManager::EventSettings settings;
  settings.sender  = this;
  settings.contact = 0;
  settings.widget  = 0;
  settings.buttons = NotificationManager::BUTTON_HIDE;
  settings.data    = details;

  // Add the More Details button if there is a detailed description of the error.
  if( ! detailedText.isEmpty() )
  {
    settings.buttons |= NotificationManager::BUTTON_MORE_DETAILS;
  }

  // Notify the user about the network error
  NotificationManager::instance()->notify( "status", messageText, settings );
}



#include "statusnotification.moc"
