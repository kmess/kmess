/***************************************************************************
                          newsystemtraywidget.h  -  description
                             -------------------
    begin                : Mon Aug 31 2009
    copyright            : (C) 2009 by Valerio Pilo
    email                : valerio@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef NEWSYSTEMTRAYWIDGET_H
#define NEWSYSTEMTRAYWIDGET_H

#include <kdeversion.h>

#if KDE_IS_VERSION(4,3,70)
  #include <KStatusNotifierItem>
#else
  #include <knotificationitem.h>
  using namespace Experimental; // KNotificationItem is in the Experimental namespace until 4.4
#define KStatusNotifierItem KNotificationItem
#endif

// Forward declarations
class KMenu;



/**
 * @brief System tray event handling.
 *
 * The user's status is displayed in the system tray.
 * It also gives access to some settings.
 * This class handles the events fired by the KDE systemtray.
 *
 * @author Valerio Pilo
 * @ingroup Root
 */
class SystemTrayWidget : public KStatusNotifierItem
{
  Q_OBJECT

  public:
    // The constructor
                     SystemTrayWidget( QWidget *parent=0 );
    // The destructor
    virtual         ~SystemTrayWidget();
    // Display a customized close window with a screenshot of the app's tray icon
    void             displayCloseMessage( QString fileMenu = QString() );
    // Enable the system tray icon
    void             enable();
    // Return the context menu
    KMenu           *menu() const;

  public slots:
    // Change the icon when the user's status changes
    void             statusChanged();
};

#endif
