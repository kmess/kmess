/***************************************************************************
                    macnotification.h - Notifications on Mac OS X
                             -------------------
    begin                : Thursday June 3 2010
    copyright            : (C) 2010 by Sjors Gielen
    email                : sjors@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef MACNOTIFICATION_H
#define MACNOTIFICATION_H

#include <QHash>
#include <QVariant>

#include "notificationmanager.h"

class QSystemTrayIcon;
class QDomNode;

/**
 * @brief Support for Mac-style notifications on OS X
 *
 * This file is compiled along if we are compiling a Mac binary. It adds
 * support for Growl notifications and the 'jumping' of the icon.
 *
 * Because of the nature of KNotify, it is very unlikely that KNotify will
 * ever implement this. Therefore, we implement it ourselves.
 *
 * @author Sjors Gielen <sjors@kmess.org>
 * @ingroup Notification
 */
class MacNotification : public QObject
{
  Q_OBJECT

  public:
    //MacNotification();
    //~MacNotification();
    static bool growlSupported();

  public slots:
    static void showGrowlBalloon( const QString &title, const QString &text );
    static void playSound( const QString &filename );
    static void jumpIcon();
    static void notify( const QString &event, const QString &text,
                        NotificationManager::EventSettings settings );

    static QString stripHtml( const QString& );

  private:
    static QSystemTrayIcon *icon_;

};



#endif
