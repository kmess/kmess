/***************************************************************************
                          newemailnotification.cpp - notifies when a
                            new email has arrived
                             -------------------
    begin                : Tuesday April 10 2007
    copyright            : (C) 2007 by Valerio Pilo
    email                : valerio@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "newemailnotification.h"

#include "../utils/kmessshared.h"
#include "../kmessdebug.h"
#include "notificationmanager.h"

#include <QTextDocument>

#include <KLocale>
#include <KNotification>



// Class constructor
NewEmailNotification::NewEmailNotification( NotificationManager* manager )
: manager_(manager)
{
  // Connect the activation signal to process notification actions
  connect( manager_, SIGNAL( eventActivated(NotificationManager::EventSettings,NotificationManager::Buttons) ),
           this,     SLOT  (       activate(NotificationManager::EventSettings,NotificationManager::Buttons) ) );
}



// Execute the action triggered in a notification
void NewEmailNotification::activate( NotificationManager::EventSettings settings, NotificationManager::Buttons button )
{
  // The signal wasn't meant for us
  if( settings.sender != this )
  {
    return;
  }

  switch( button )
  {
    case NotificationManager::BUTTON_OPEN_MAILBOX:
#ifdef KMESSDEBUG_NEWEMAILNOTIFICATION
      kmDebug() << "Opening mailbox on folder" << settings.data.toString();
#endif
      KMessShared::openEmailClient( QString(), settings.data.toString() );
      break;

    case NotificationManager::BUTTON_HIDE:
#ifdef KMESSDEBUG_NEWEMAILNOTIFICATION
      kmDebug() << "Hiding email notification";
#endif
      // Do nothing
      break;

    default:
#ifdef KMESSDEBUG_NEWEMAILNOTIFICATION
      kmDebug() << "Invalid button" << button;
#endif
      return;
  }
}



// Notify the user about this event (a new email has arrived)
void NewEmailNotification::notify( QString sender, QString subject, bool inInbox, QString command, QString folder, QString url )
{
  // Command and url are not used at the moment (jun 2008)
  Q_UNUSED( url );
  Q_UNUSED( command );

#ifdef KMESSDEBUG_NEWEMAILNOTIFICATION
  kmDebug() << "New email from" << sender << "- Is in inbox?" << inInbox;
#else
  Q_UNUSED( inInbox );
#endif


  QString text( i18nc("%1 is the subject of the mail, %2 is the sender of the mail", "<html><b>New email:</b><br/>'%1'<br/>by '%2'</html>",
                      Qt::escape( subject ),
                      Qt::escape( sender  ) ) );

  NotificationManager::EventSettings settings;
  settings.sender  = this;
  settings.widget  = 0;
  settings.contact = 0;
  settings.data    = folder;
  settings.buttons = NotificationManager::BUTTON_HIDE | NotificationManager::BUTTON_OPEN_MAILBOX;

  manager_->notify( "new email", text, settings );
}



#include "newemailnotification.moc"
