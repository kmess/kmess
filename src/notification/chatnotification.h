/***************************************************************************
                          notificationcontactonline.h -  notifies when a
                            contact goes online
                             -------------------
    begin                : Tuesday April 10 2007
    copyright            : (C) 2007 by Valerio Pilo
    email                : valerio@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CHATNOTIFICATION_H
#define CHATNOTIFICATION_H

#include "notificationmanager.h"

#include <QObject>


// Forward declarations
class Chat;


namespace KMess
{
  class Message;
}

/**
 * @brief Notifies the user when a contact has sent him/her a message.
 *
 * Notification is made via a popup balloon and a sound (which can be customized)
 *
 * @author Valerio Pilo <valerio@kmess.org>
 * @ingroup Notification
 */
class ChatNotification : public QObject
{
  Q_OBJECT

  public:  // Public methods
    // Class constructor
                          ChatNotification( NotificationManager* manager );

  signals:
    // Raise an existing chat which received a message
    void                  raiseChat( Chat *chatWindow, bool force );

  public slots:
    // Notify the user about this event (a contact has contacted the user)
    void                  notify( KMess::Message message );

  private slots:
    // Execute the action triggered in a notification
    void                  activate( NotificationManager::EventSettings settings, NotificationManager::Buttons button );

  private:  // Private properties
    // The notification manager instance
    NotificationManager  *manager_;

};


#endif
