/***************************************************************************
                          chatnotification.cpp -  notifies when a
                            contact communicates with the user
                             -------------------
    begin                : Tuesday April 10 2007
    copyright            : (C) 2007 by Valerio Pilo
    email                : valerio@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "chatnotification.h"

#include "../chat/chat.h"
#include "../chat/chatmaster.h"
#include "../chat/chatwindow.h"
#include "../contact/contactextension.h"
#include "../kmessdebug.h"
#include "../kmessglobal.h"
#include "../kmessapplication.h"
#include "notificationmanager.h"

#include <KMess/Message>
#include <KMess/InkMessage>
#include <KMess/NudgeMessage>
#include <KMess/OfflineMessage>
#include <KMess/TextMessage>
#include <KMess/TypingMessage>
#include <KMess/VoiceMessage>
#include <KMess/WinkMessage>
#include <KMess/MsnContact>
#include <KMess/NetworkGlobals>

#include <QPixmap>

#include <KNotification>
#include <KWindowSystem>


// How long a message can be without being cropped away
#define MAX_BALLOON_MESSAGE_SIZE      64



// Class constructor
ChatNotification::ChatNotification( NotificationManager* manager )
: manager_(manager)
{
  // Connect the activation signal to process notification actions
  connect( manager_, SIGNAL( eventActivated(NotificationManager::EventSettings,NotificationManager::Buttons) ),
           this,     SLOT  (       activate(NotificationManager::EventSettings,NotificationManager::Buttons) ) );
}



// Execute the action triggered in a notification
void ChatNotification::activate( NotificationManager::EventSettings settings, NotificationManager::Buttons button )
{
  // The signal wasn't meant for us
  if( settings.sender != this )
  {
    return;
  }

  switch( button )
  {
    case NotificationManager::BUTTON_VIEW_MESSAGE:
#ifdef KMESSDEBUG_CHATNOTIFICATION
      kmDebug() << "Raising chat with" << settings.contact->handle();
#endif
      emit raiseChat( static_cast<Chat*>( settings.widget ), true );
      break;

    case NotificationManager::BUTTON_HIDE:
#ifdef KMESSDEBUG_CHATNOTIFICATION
      kmDebug() << "Hiding chat notification";
#endif
      // Do nothing
      break;

    default:
#ifdef KMESSDEBUG_CHATNOTIFICATION
      kmDebug() << "Invalid button" << button;
#endif
      return;
  }
}



// Notify the user about this event (a contact has contacted the user)
void ChatNotification::notify( KMess::Message message )
{
  // End if the event is coming from us instead of from a contact
  if( ! message.isIncoming() )
  {
#ifdef KMESSDEBUG_CHATNOTIFICATION
    kmDebug() << "Not displaying a popup for an outgoing message.";
#endif
    return;
  }

  KMess::MsnChat* msnChat = message.chat();
  Chat* chat = KMessApplication::instance()->chatMaster()->getChat( msnChat );

  // Check the chat pointer, we need it
  if( ! chat )
  {
#ifdef KMESSDEBUG_CHATNOTIFICATION
    kmDebug() << "Invalid pointer to chat! MsnChat:" << ((void*)msnChat) << "Chat:" << ((void*)chat);
#endif
    return;
  }

  ChatWindow *window = chat->getChatWindow();

  // Verify if the chat is active, and if so don't notify anything
  if( KWindowSystem::activeWindow() == window->winId()
  &&  chat == window->getCurrentChat() )
  {
#ifdef KMESSDEBUG_CHATNOTIFICATION
    kmDebug() << "Window is active, skipping.";
#endif
    return;
  }

  Contact contact( message.peer() );
  if( contact.isValid() && contact->getExtension() )
  {
    if( contact.getExtension()->getDisableNotifications() )
    {
      kmDebug() << "Contact" << message.peer()->handle() << "has notification disabled, skip it.";
      return;
    }
  }

  QString text;
  KMessMessage newMessage( message, KMessMessage::IncomingText );


  // KNotify kills formatting by cropping strings in the middle of HTML codes. Remove it.
  const QString& friendlyName( contact.getFriendlyName( STRING_CLEANED_ESCAPED ) );

  switch( message.type() )
  {
    case KMess::InkMessageType:
    {
      text = i18n( "<html><i><b>%1</b> has sent you an handwritten message!</i></html>", friendlyName );
      break;
    }

    case KMess::OfflineMessageType:
    {
      KMess::OfflineMessage offlineMessage( message );
      text = i18n( "<html><i><b>%1</b> has sent you an offline message:</i><br/>%2</html>", friendlyName, offlineMessage.message() );
      break;
    }

    case KMess::TextMessageType:
    {
      // Truncate the message so it doesn't make the balloon too big
      text = newMessage.message();
      if( text.length() > MAX_BALLOON_MESSAGE_SIZE )
      {
        text.truncate( MAX_BALLOON_MESSAGE_SIZE );
        text += "...";
      }

      // Parse the emoticons in the message
      text = RichTextParser::instance()->parseMsnString( text,
             ( RichTextParser::FormattingOptions )
               ( RichTextParser::PARSE_EMOTICONS
               | RichTextParser::EMOTICONS_SMALL ) );

      text = i18n( "<html><i><b>%1</b> said:</i><br/>%2</html>", friendlyName, text );
      break;
    }

    case KMess::NudgeMessageType:
    {
      text = i18n( "<html><b>%1</b> has sent you a nudge!</html>", friendlyName );
      break;
    }

    case KMess::VoiceMessageType:
    {
      text = i18n( "<html><b>%1</b> has sent you a voice message!</html>", friendlyName );
      break;
    }

    case KMess::WinkMessageType:
    {
      text = i18n( "<html><b>%1</b> has sent you a wink!</html>", friendlyName );
      break;
    }

    default:
      // Other messages may be KMess specific messages: don't notify anything
#ifdef KMESSDEBUG_CHATNOTIFICATION
      kmDebug() << "Not a displayable message, skipping.";
#endif
      return;
  }

#ifdef __GNUC__
#warning TODO: Add back all notification messages
#endif
#if 0
  switch( messageType )
  {
    case ChatMessage::TYPE_PRESENCE:
    case ChatMessage::TYPE_NOTIFICATION:
      text = i18n( "<html>In <b>%1</b>'s chat:<br/>%2</html>", friendlyName, body );
      break;

    case ChatMessage::TYPE_INCOMING:
      text = i18n( "<html><b>%1</b> says:<br/>'%2'</html>", friendlyName, body );
      break;

    case ChatMessage::TYPE_OFFLINE_INCOMING:
      text = i18n( "<html><b>%1</b> has sent you an offline message:<br/>'%2'</html>", friendlyName, body );
      break;

    case ChatMessage::TYPE_APPLICATION:
    case ChatMessage::TYPE_APPLICATION_WEBCAM:
    case ChatMessage::TYPE_APPLICATION_FILE:
    case ChatMessage::TYPE_APPLICATION_AUDIO:
      text = i18n( "<html>%1's chat requests attention!</html>", friendlyName );
      break;

    case ChatMessage::TYPE_SYSTEM:
    default:
      // Not localized, as there is no text in it
      text = QString( "<html><b>%1</b>:<br/>%2</html>" ).arg( friendlyName )
                                                        .arg( body );
      break;
  }

  switch( message.getContentsClass() )
  {
    case ChatMessage::CONTENT_MESSAGE_INK:
      text = i18n( "<html><b>%1</b> has sent you a handwritten message!</html>", friendlyName );
      break;

    case ChatMessage::CONTENT_NOTIFICATION_NUDGE:
      text = i18n( "<html><b>%1</b> has sent you a nudge!</html>", friendlyName );
      break;

    case ChatMessage::CONTENT_NOTIFICATION_WINK:
      text = i18n( "<html><b>%1</b> has sent you a wink!</html>", friendlyName );
      break;

    case ChatMessage::CONTENT_APP_INVITE:
      switch( messageType )
      {
        case ChatMessage::TYPE_APPLICATION_WEBCAM: text = i18n( "<html><b>%1</b> wants to use the webcam!</html>", friendlyName ); break;
        case ChatMessage::TYPE_APPLICATION_FILE: text = i18n( "<html><b>%1</b> is sending you a file!</html>", friendlyName ); break;
        default: text = i18n( "<html><b>%1</b> has sent you an invitation!</html>", friendlyName ); break;
      }
      break;

    case ChatMessage::CONTENT_APP_CANCELED:
      switch( messageType )
      {
        case ChatMessage::TYPE_APPLICATION_WEBCAM: text = i18n( "<html><b>%1</b> has canceled the webcam session!</html>", friendlyName ); break;
        case ChatMessage::TYPE_APPLICATION_FILE: text = i18n( "<html><b>%1</b> has canceled the file transfer!</html>", friendlyName ); break;
        default: text = i18n( "<html><b>%1</b>'s activity has been canceled!</html>", friendlyName ); break;
      }
      break;

    case ChatMessage::CONTENT_APP_STARTED:
      switch( messageType )
      {
        case ChatMessage::TYPE_APPLICATION_WEBCAM: text = i18n( "<html><b>%1</b> has accepted to use the webcam!</html>", friendlyName ); break;
        case ChatMessage::TYPE_APPLICATION_FILE: text = i18n( "<html><b>%1</b> has accepted the file transfer!</html>", friendlyName ); break;
        default: text = i18n( "<html><b>%1</b> has accepted your invitation!</html>", friendlyName ); break;
      }
      break;

    case ChatMessage::CONTENT_APP_ENDED:
      switch( messageType )
      {
        case ChatMessage::TYPE_APPLICATION_WEBCAM: text = i18n( "<html><b>%1</b> has ended the webcam session!</html>", friendlyName ); break;
        case ChatMessage::TYPE_APPLICATION_FILE: text = i18n( "<html>The file transfer with <b>%1</b> is done!</html>", friendlyName ); break;
        default: text = i18n( "<html><b>%1</b>'s activity has ended!</html>", friendlyName ); break;
      }
      break;

    case ChatMessage::CONTENT_APP_FAILED:
      switch( messageType )
      {
        case ChatMessage::TYPE_APPLICATION_WEBCAM: text = i18n( "<html><b>%1</b>'s webcam session has failed!</html>", friendlyName ); break;
        case ChatMessage::TYPE_APPLICATION_FILE: text = i18n( "<html>The file transfer with <b>%1</b> has failed!</html>", friendlyName ); break;
        default: text = i18n( "<html><b>%1</b>'s activity has ended with an error!</html>", friendlyName ); break;
      }
      break;

    default:
      break;
  }
#endif

  QString eventName;
  if( newMessage.specificType() == KMessMessage::Nudge )
  {
    eventName = "nudge";
  }
  else if( chat->isChatFirstMessage()
        && (  newMessage.specificType() == KMessMessage::IncomingText
           || newMessage.specificType() == KMessMessage::OfflineIncoming ) )
  {
    eventName = "chat start";
  }
  else
  {
    eventName = "new message";
  }

  NotificationManager::EventSettings settings;
  settings.sender  = this;
  settings.widget  = chat;
  settings.contact = message.peer();
  settings.buttons = NotificationManager::BUTTON_HIDE | NotificationManager::BUTTON_VIEW_MESSAGE;

  manager_->notify( eventName, text, settings );
}



#include "chatnotification.moc"
