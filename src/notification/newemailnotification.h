/***************************************************************************
                          newemailnotification.h - notifies when a
                            new email has arrived
                             -------------------
    begin                : Tuesday April 10 2007
    copyright            : (C) 2007 by Valerio Pilo
    email                : valerio@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef NEWEMAILNOTIFICATION_H
#define NEWEMAILNOTIFICATION_H

#include "notificationmanager.h"

#include <QObject>


// Forward declarations
class NotificationManager;
class PassivePopup;



/**
 * @brief Notifies the user when a new email is in the mailbox
 *
 * Notification is made via a popup balloon and a sound (which can be customized)
 *
 * @author Valerio Pilo <valerio@kmess.org>
 * @ingroup Notification
 */
class NewEmailNotification : public QObject
{
  Q_OBJECT

  public:  // Public methods
    // Class constructor
                          NewEmailNotification( NotificationManager* manager );

  public slots:
    // Notify the user about this event (a new email has arrived)
    void                  notify( QString sender, QString subject, bool inInbox, QString command, QString folder, QString url );

  private slots:
    // Execute the action triggered in a notification
    void                  activate( NotificationManager::EventSettings settings, NotificationManager::Buttons button );

  private:  // Private properties
    // The notification manager instance
    NotificationManager  *manager_;

};


#endif
