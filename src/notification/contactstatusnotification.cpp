/***************************************************************************
                          contactstatusnotification.cpp - notifies when a
                            contact changes their msn status
                             -------------------
    begin                : Saturday July 28 2007
    copyright            : (C) 2007 by Valerio Pilo
    email                : valerio@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "contactstatusnotification.h"

#include "../contact/contact.h"
#include "../contact/contactextension.h"
#include "../mainwindow.h"
#include "../kmessdebug.h"
#include "kmessapplication.h"
#include "../chat/chatmaster.h"
#include "notificationmanager.h"

#include <KMess/MsnContact>

#include <QFile>
#include <QTextDocument>
#include <QTimer>

#include <KApplication>
#include <KLocale>
#include <KNotification>
#include <Phonon/MediaObject>



// Class constructor
ContactStatusNotification::ContactStatusNotification( NotificationManager* manager )
: enableNotifications_( false )
, manager_( manager )
{
  // Connect the activation signal to process notification actions
  connect( manager_, SIGNAL( eventActivated(NotificationManager::EventSettings,NotificationManager::Buttons) ),
           this,     SLOT  (       activate(NotificationManager::EventSettings,NotificationManager::Buttons) ) );
}



// Execute the action triggered in a notification
void ContactStatusNotification::activate( NotificationManager::EventSettings settings, NotificationManager::Buttons button )
{
  // The signal wasn't meant for us
  if( settings.sender != this )
  {
    return;
  }

  switch( button )
  {
    case NotificationManager::BUTTON_OPEN_CHAT:
    case NotificationManager::BUTTON_OPEN_OFFLINE_CHAT:
#ifdef KMESSDEBUG_CONTACTSTATUSNOTIFICATION
      kmDebug() << "Opening a chat with" << settings.contact->handle();
#endif
      KMessApplication::instance()->chatMaster()->requestChat( settings.contact->handle() );
      break;

    case NotificationManager::BUTTON_HIDE:
#ifdef KMESSDEBUG_CONTACTSTATUSNOTIFICATION
      kmDebug() << "Hiding status notification";
#endif
      // Do nothing
      break;

    default:
#ifdef KMESSDEBUG_CONTACTSTATUSNOTIFICATION
      kmDebug() << "Invalid button" << button;
#endif
      return;
  }
}



// Obtains a status string like "contact has come online" for a MSN status code
const QString ContactStatusNotification::getStatusString( const KMess::MsnStatus status, const QString& friendlyName ) const
{
  switch( status )
  {
    case KMess::OnlineStatus:        return i18n( "<html><b>%1</b><br/>is now online</html>",        friendlyName );
    case KMess::AwayStatus:          return i18n( "<html><b>%1</b><br/>has gone away</html>",        friendlyName );
    case KMess::BrbStatus:           return i18n( "<html><b>%1</b><br/>will be right back</html>",   friendlyName );
    case KMess::BusyStatus:          return i18n( "<html><b>%1</b><br/>is now busy</html>",          friendlyName );
    case KMess::InvisibleStatus:     return i18n( "<html><b>%1</b><br/>has become invisible</html>", friendlyName ); // Is this one even possible?!
    case KMess::IdleStatus:          return i18n( "<html><b>%1</b><br/>has gone idle</html>",        friendlyName );
    case KMess::OfflineStatus:       return i18n( "<html><b>%1</b><br/>has logged out</html>",       friendlyName );
    case KMess::PhoneStatus:         return i18n( "<html><b>%1</b><br/>is on the phone</html>",      friendlyName );
    case KMess::LunchStatus:         return i18n( "<html><b>%1</b><br/>is out for lunch</html>",     friendlyName );
    default:
      kmWarning() << "Invalid status" << status << "!";
      return getStatusString( KMess::OnlineStatus, friendlyName );
  }
}


/**
 * @brief Lock notifications for the first seconds of connections
 *
 * The first moments KMess is connected notifications are blocked, to avoid receiving popups before
 * the user could even look at the contact list, possibly distracting him/her or covering screen space.
 */
void ContactStatusNotification::lockNotifications()
{
#ifdef KMESSDEBUG_CONTACTSTATUSNOTIFICATION
  kmDebug() << "Notifications locked";
#endif

  enableNotifications_ = true;
}



// Notify the user about this event (a contact has changed its status)
void ContactStatusNotification::notify( KMess::MsnContact *c, bool isInitialStatus )
{
  Contact contact( c );
  if( ! contact.isValid() )
  {
    kmWarning() << "Contact is not valid!";
    return;
  }

  // If notification for this event is disabled
  // or the user decided to don't show the notification about this contact
  // don't show a balloon.
  if( ! enableNotifications_ || isInitialStatus || contact->getExtension()->getDisableNotifications() )
  {
#ifdef KMESSDEBUG_CONTACTSTATUSNOTIFICATION
    kmDebug() << "Not needed, skipping.";
#endif
    return;
  }


  // KNotify kills formatting by cropping strings in the middle of HTML codes. Remove it.
  const QString& friendlyName( contact->getFriendlyName( STRING_CLEANED_ESCAPED ) );

  QString text;
  KMess::MsnStatus  status = contact->msnContact()->status();
  KMess::MsnStatus  lastStatus = contact->getLastStatus();

#ifdef KMESSDEBUG_CONTACTSTATUSNOTIFICATION
  kmDebug() << "Contact:" << contact->getHandle()
           << "Status:"  << Status::getCode( lastStatus ) << "->" << Status::getCode( status );
#endif

  // When contacts go online, whichever may be their status, just show an 'is now online'.
  if( lastStatus == KMess::OfflineStatus && status != KMess::OfflineStatus )
  {
    text = getStatusString( KMess::OnlineStatus, friendlyName );
  }
  else
  {
    text = getStatusString( status, friendlyName );
  }

// TODO Fix custom contact sound, possibly using contexts and setComponentData() from KNotification
/*
  // Play a sound
  QString soundPath( contact->getExtension()->getContactSoundPath() );
  QFile   soundFile( soundPath );

  // If a custom audio file exists, play it
  if ( soundFile.exists() )
  {
#ifdef KMESSDEBUG_CONTACTSTATUSNOTIFICATION
    kmDebug() << "Play sound " << soundPath;
#endif

    // Play the custom sound instead of the generic sound
    Phonon::MediaObject* media = Phonon::createPlayer( Phonon::NotificationCategory );
    media->setCurrentSource( soundPath );
    media->play();
  }
*/

  QString eventName;
  if( lastStatus == KMess::OfflineStatus )
  {
    eventName = "contact online";
  }
  else if( status == KMess::OfflineStatus )
  {
    eventName = "contact offline";
  }
  else
  {
    eventName = "contact status";
  }

  // Associate the list events with the contact list window
  QWidget *mainWindow = MainWindow::instance()->window();

  NotificationManager::EventSettings settings;
  settings.sender  = this;
  settings.widget  = mainWindow;
  settings.contact = contact;
  settings.buttons = NotificationManager::BUTTON_HIDE;

  if( status == KMess::OfflineStatus )
  {
    settings.buttons |= NotificationManager::BUTTON_OPEN_OFFLINE_CHAT;
  }
  else
  {
    settings.buttons |= NotificationManager::BUTTON_OPEN_CHAT;
  }

  manager_->notify( eventName, text, settings );
}


/**
 * @brief Unlock notifications after the first seconds of connections
 *
 * The first moments KMess is connected notifications are blocked, to avoid receiving popups before
 * the user could even look at the contact list, possibly distracting him/her or covering screen space.
 */
void ContactStatusNotification::unlockNotifications()
{
  if( QString( sender()->metaObject()->className() ) != "QSingleShotTimer" )
  {
#ifdef KMESSDEBUG_CONTACTSTATUSNOTIFICATION
    kmDebug() << "Notifications will be unlocked in 3 seconds...";
#endif
    // Start the timer to unlock the notification after 3 seconds
    QTimer::singleShot( 3000, this, SLOT( unlockNotifications() ) );
    return;
  }

#ifdef KMESSDEBUG_CONTACTSTATUSNOTIFICATION
  kmDebug() << "Notifications unlocked";
#endif

  enableNotifications_ = true;
}



#include "contactstatusnotification.moc"
