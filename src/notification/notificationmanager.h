/***************************************************************************
                          notificationmanager.h - manage the notifications
                            stack and updates them as needed
                             -------------------
    begin                : Thursday April 19 2007
    copyright            : (C) 2007 by Valerio Pilo
    email                : valerio@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef NOTIFICATIONMANAGER_H
#define NOTIFICATIONMANAGER_H

#include <QHash>
#include <QVariant>


// Forward declarations
#ifndef Q_WS_MAC
class KNotification;
#endif

class ChatNotification;
class Contact;
class ContactStatusNotification;
class NewEmailNotification;
class StatusNotification;
class XAutoLock;


namespace KMess
{
  class MsnContact;
};

/**
 * @brief Manages the notifications stack
 *
 * Notification balloons are handled as a vertical stack to avoid overlapping.
 *
 * @author Valerio Pilo <valerio@kmess.org>
 * @ingroup Notification
 */
class NotificationManager : public QObject
{
  Q_OBJECT

  public: // Public enumerations

    /// Types of buttons which can be shown in popups
    enum Buttons
    {
      BUTTON_INVALID             = 0       /// No button pressed (invalid choice)
    , BUTTON_OPEN_CHAT           = 1       /// Button to open a chat
    , BUTTON_OPEN_OFFLINE_CHAT   = 2       /// Button to open an offline chat
    , BUTTON_VIEW_MESSAGE        = 4       /// Button to raise an existing chat
    , BUTTON_OPEN_MAILBOX        = 8       /// Button to open the mail box
    , BUTTON_MORE_DETAILS        = 16      /// Button to show more details about an event
    , BUTTON_HIDE                = 16384   /// Button to hide the popup
    };

  public: // Public data structures

    struct EventSettings
    {
      public:
        QObject             *sender;
        QWidget             *widget;
        KMess::MsnContact   *contact;
        QVariant             data;
        int                  buttons;
    };


  public: // Public methods
    // Class destructor
    virtual                       ~NotificationManager();

    // Return the notification object for when a contact sends us a message
    ChatNotification              *chatNotification()          const;
    // Return the notification object for when a contact changes its status
    ContactStatusNotification     *contactStatusNotification() const;
    // Return the notification object for when a new email is received
    NewEmailNotification          *newEmailNotification()      const;
    // Return the notification object for when an application status event occur
    StatusNotification            *statusNotification()        const;

    // Insert a new popup in the stack if needed, or change an existing one
    void                           notify( const QString &event, const QString &text, EventSettings settings );

  public: // Public static methods
    // Return a singleton instance of the current account
    static NotificationManager    *instance();

  private:  // Private methods
    // Class constructor
                                   NotificationManager();
    // Associate a KNotification action with an item of the Buttons bitfield
    Buttons                        getButtonFromAction( int buttons, int action );
    // Associate a Buttons bitfield with button labels
    QStringList                    getButtonsLabels( int buttons );

  private slots:
    // Forward an activation action to the class which will manage it
    void                           relayActivation( unsigned int action );
    // Delete an expired notification
    void                           remove( QObject *object );

  private: // Private properties
    // Autolock instance, used to check for screensaver/locker presence when showing popups
    XAutoLock                     *autoLock_;
    // The notification object for when a contact sends us a message
    ChatNotification              *chatNotification_;
    // The notification object for when a contact changes its status
    ContactStatusNotification     *contactStatusNotification_;
    #ifndef Q_WS_MAC
    // List of notifications currently active
    QHash<KNotification*,KMess::MsnContact*> events_;
    // Additional data stored for each notification
    QHash<KNotification*,EventSettings> eventSettings_;
    #endif
    // The notification object for when a new email is received
    NewEmailNotification          *newEmailNotification_;
    // The notification object for when an application status event occurs
    StatusNotification            *statusNotification_;
    // The instance of the singleton manager
    static NotificationManager    *instance_;

  signals:
    // A notification button has been pressed
    void                           eventActivated( NotificationManager::EventSettings settings,
                                                   NotificationManager::Buttons clickedButton );
};



#endif
