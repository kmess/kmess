/***************************************************************************
                          notificationmanager.cpp - manage the notifications
                            stack and updates them as needed
                             -------------------
    begin                : Thursday April 19 2007
    copyright            : (C) 2007 by Valerio Pilo
    email                : valerio@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "notificationmanager.h"

#include "../contact/contact.h"
#include "../utils/xautolock.h"
#include "../account.h"
#include "../kmessglobal.h"
#include "../kmessdebug.h"
#include "kmessapplication.h"
#include "../chat/chatmaster.h"
#include "chatnotification.h"
#include "contactstatusnotification.h"
#include "newemailnotification.h"
#include "statusnotification.h"

#include <QWidget>

#include <KLocale>
#ifndef Q_WS_MAC
#include <KNotification>
#else
#include "macnotification.h"
#endif
#include <KWindowSystem>

#ifdef Q_WS_WIN
#include <windows.h>
#endif


// Initialize the instance to zero
NotificationManager* NotificationManager::instance_(0);



// Class constructor
NotificationManager::NotificationManager()
{
#ifdef KMESSDEBUG_NOTIFICATIONMANAGER
  kmDebug() << "Initializing notifications manager";
#endif

  // Initialize the interfaces to screen saver and fullscreen apps checks
  autoLock_ = new XAutoLock();

  // Create the various notification types
  chatNotification_          = new ChatNotification         ( this );
  newEmailNotification_      = new NewEmailNotification     ( this );
  contactStatusNotification_ = new ContactStatusNotification( this );
  statusNotification_        = new StatusNotification       ( this );

#ifdef KMESSTEST
  KMESS_ASSERT( chatNotification_          != 0 );
  KMESS_ASSERT( contactStatusNotification_ != 0 );
  KMESS_ASSERT( newEmailNotification_      != 0 );
  KMESS_ASSERT( statusNotification_        != 0 );
#endif

  // Connect the chat messages notification signals
  connect( globalSession,     SIGNAL( chatMessageReceived(KMess::Message) ),
           chatNotification_, SLOT  (              notify(KMess::Message) ) );

  // TODO: why is there a "raiseChat" on globalSession?
  connect( chatNotification_,                          SIGNAL(      raiseChat(Chat*,bool)        ),
           KMessApplication::instance()->chatMaster(), SLOT  (      raiseChat(Chat*,bool)        ) );

  // Connect the email notification signals
  connect( globalSession,         SIGNAL( newEmail(QString,QString,bool,QString,QString,QString) ),
           newEmailNotification_, SLOT  (   notify(QString,QString,bool,QString,QString,QString) ) );

  // Connect the status change notification signals
  connect( globalSession,              SIGNAL(        contactOnline(KMess::MsnContact*,bool)  ),
           contactStatusNotification_, SLOT  (               notify(KMess::MsnContact*,bool)  ) );
  connect( globalSession,              SIGNAL(       contactOffline(KMess::MsnContact*)  ),
           contactStatusNotification_, SLOT  (               notify(KMess::MsnContact*)  ) );
  connect( globalSession,              SIGNAL( contactChangedStatus(KMess::MsnContact*, KMess::MsnStatus)  ),
           contactStatusNotification_, SLOT  (               notify(KMess::MsnContact*)  ) );

  // Connect the notification unlocking mechanism ( see ContactStatusNotification::lockNotifications() )
  connect( globalSession,              SIGNAL(            loggedIn() ),
           contactStatusNotification_, SLOT  ( unlockNotifications() ) );
  connect( globalSession,              SIGNAL(           loggedOut(KMess::DisconnectReason) ),
           contactStatusNotification_, SLOT  (   lockNotifications() ) );

  // Connect the application status event signals
  connect( globalSession,       SIGNAL( statusEvent(KMess::StatusMessageType,KMess::StatusMessage,QVariant) ),
           statusNotification_, SLOT  (      notify(KMess::StatusMessageType,KMess::StatusMessage,QVariant) ) );
}



// Class destructor
NotificationManager::~NotificationManager()
{
#ifndef Q_WS_MAC
  foreach( KNotification *notification, events_.keys() )
  {
    notification->close();
  }

  events_.clear();
  eventSettings_.clear();
#endif

  delete autoLock_;
  delete chatNotification_;
  delete contactStatusNotification_;
  delete newEmailNotification_;
  delete statusNotification_;
}



// Return the notification object for when a contact sends us a message
ChatNotification* NotificationManager::chatNotification() const
{
  return chatNotification_;
}



// Return the notification object for when a contact changes its status
ContactStatusNotification* NotificationManager::contactStatusNotification() const
{
  return contactStatusNotification_;
}



// Associate a KNotification action with an item of the Buttons bitfield
NotificationManager::Buttons NotificationManager::getButtonFromAction( int buttons, int action )
{
  int number = 1;

  // It's important to keep the synchronization and the same order for the buttons,
  // both here and in getButtonsLabels(): KNotification just uses the index for each action
  if( ( buttons & BUTTON_OPEN_CHAT ) && number++ == action )
  {
    return BUTTON_OPEN_CHAT;
  }
  if( ( buttons & BUTTON_OPEN_OFFLINE_CHAT ) && number++ == action )
  {
    return BUTTON_OPEN_OFFLINE_CHAT;
  }
  if( ( buttons & BUTTON_VIEW_MESSAGE ) && number++ == action )
  {
    return BUTTON_VIEW_MESSAGE;
  }
  if( ( buttons & BUTTON_MORE_DETAILS ) && number++ == action )
  {
    return BUTTON_MORE_DETAILS;
  }
  if( ( buttons & BUTTON_OPEN_MAILBOX ) && number++ == action )
  {
    return BUTTON_OPEN_MAILBOX;
  }
  if( ( buttons & BUTTON_HIDE ) && number++ == action )
  {
    return BUTTON_HIDE;
  }

  return BUTTON_INVALID;
}



// Associate a Buttons bitfield with button actions
QStringList NotificationManager::getButtonsLabels( int buttons )
{
  QStringList labels;

  // It's important to keep the synchronization and the same order for the buttons,
  // both here and in getButtonFromAction(): KNotification just uses the index for each action
  if( buttons & BUTTON_OPEN_CHAT )
  {
    labels << i18nc( "Button text for KDE notification boxes", "Start Chatting" );
  }
  if( buttons & BUTTON_OPEN_OFFLINE_CHAT )
  {
    labels << i18nc( "Button text for KDE notification boxes", "Leave a Message" );
  }
  if( buttons & BUTTON_VIEW_MESSAGE )
  {
    labels << i18nc( "Button text for KDE notification boxes", "View Message" );
  }
  if( buttons & BUTTON_MORE_DETAILS )
  {
    labels << i18nc( "Button text for KDE notification boxes", "Details" );
  }
  if( buttons & BUTTON_OPEN_MAILBOX )
  {
    labels << i18nc( "Button text for KDE notification boxes", "Read Email" );
  }
  if( buttons & BUTTON_HIDE )
  {
    labels << i18nc( "Button text for KDE notification boxes", "Hide" );
  }

  return labels;
}



// Return the notification object for when a new email is received
NewEmailNotification* NotificationManager::newEmailNotification() const
{
  return newEmailNotification_;
}



// Insert a new notification in the stack if needed, or update an existing one
void NotificationManager::notify( const QString &event, const QString &text, EventSettings settings )
{
  // Check if the screen saver is running
  if( autoLock_->isScreenSaverActive() )
  {
#ifdef KMESSDEBUG_NOTIFICATIONMANAGER
    kmDebug() << "Screen saver/locker is active, not showing notifications.";
#endif
    return;
  }

  // Do nothing if, when busy, the user doesn't want to be disturbed
  if( globalSession->self()->status() == KMess::BusyStatus
  &&  Account::connectedAccount->getSettingBool( "NotificationsHideWhenBusy" ) )
  {
#ifdef KMESSDEBUG_NOTIFICATIONMANAGER
    kmDebug() << "User is busy, not showing notifications.";
#endif
    return;
  }

  // On Mac, notification emitting goes via the MacNotifications class
#ifdef Q_WS_MAC
  MacNotification::notify( event, text, settings );
#else
  bool isFullscreen = false;

 #ifdef Q_WS_X11
  // Check if a full screen application is running by querying the window manager and asking
  // the state of the currently active window
  KWindowInfo info = KWindowSystem::windowInfo( KWindowSystem::activeWindow(), NET::WMState | NET::FullScreen );
  isFullscreen = ( info.valid() && info.hasState( NET::FullScreen ) );
 #else
  #ifdef Q_WS_WIN
  // Windows version
  HWND hwnd = GetForegroundWindow();
  RECT rcWindow;
  GetWindowRect( hwnd, &rcWindow );
  int screenWidth  = GetSystemMetrics( SM_CXSCREEN );
  int screenHeight = GetSystemMetrics( SM_CYSCREEN );

  int windowWidth  = ( rcWindow.right  - rcWindow.left );
  int windowHeight = ( rcWindow.bottom - rcWindow.top );

  isFullscreen = ( screenWidth == windowWidth && screenHeight == windowHeight );
  #else
#ifdef __GNUC__
    #warning Full screen application detection is not implemented for this platform yet.
#endif
  #endif
 #endif

  if( isFullscreen )
  {
    kmDebug() << "Active window is full screen, not showing notifications.";
    return;
  }

 #ifdef KMESSDEBUG_NOTIFICATIONMANAGER
  #ifdef Q_WS_X11
  kmDebug() << "Active window was valid?" << info.valid() <<". If so, it is not fullscreen, showing notifications.";
  #endif
 #endif

  KNotification *notification;
  Contact guiContact( settings.contact );

  // An event already exists for this contact, update it
  notification = events_.key( settings.contact, (KNotification*)0 );
  if( notification )
  {
 #ifdef KMESSDEBUG_NOTIFICATIONMANAGER
    kmDebug() << "Updating existing notification" << notification;
 #endif
    notification->setText   ( text );
    notification->setActions( getButtonsLabels( settings.buttons ) );
    // We can't update the event ID, crap.

    if( settings.contact != 0 )
    {
      notification->setPixmap( QPixmap( guiContact->getContactPicturePath() ).scaled( 96, 96 ) );
    }

    eventSettings_[ notification ] = settings;

    notification->update();
    return;
  }

  // We have to send a new event

  // Create the notification
  notification = new KNotification( event );

 #ifdef KMESSDEBUG_NOTIFICATIONMANAGER
  kmDebug() << "Showing notification" << notification << "for event" << event;
 #endif

  // Add it to our lists to be able to find it later
  events_       .insert( notification, settings.contact );
  eventSettings_.insert( notification, settings );

  // Set it up
  notification->setWidget( settings.widget );
  notification->setText   ( text );
  notification->setActions( getButtonsLabels( settings.buttons ) );
  notification->setFlags  ( KNotification::CloseOnTimeout
                          | KNotification::CloseWhenWidgetActivated );

  if( settings.contact != 0 )
  {
    notification->setPixmap( QPixmap( guiContact->getContactPicturePath() ).scaled( 96, 96 ) );
  }

  // The slots will call the appropriate *Notification class
  connect( notification, SIGNAL(       activated(unsigned int) ),
           this,         SLOT  ( relayActivation(unsigned int) ) );
  connect( notification, SIGNAL(       destroyed(QObject*)     ),
           this,         SLOT  (          remove(QObject*)     ) );

  notification->sendEvent();
#endif /* !Q_WS_MAC */
}



// Return a singleton instance of the current account
NotificationManager* NotificationManager::instance()
{
  // If the instance is null, create a new manager and return that.
  if ( instance_ == 0 )
  {
    instance_ = new NotificationManager();
  }
  return instance_;
}



// Forward an activation action to the class which will manage it
void NotificationManager::relayActivation( unsigned int action )
{
#ifdef Q_WS_MAC
#ifdef __GNUC__
  #warning TODO!
#endif
#else
  KNotification *notification = static_cast<KNotification*>( sender() );
  if( ! notification || ! eventSettings_.contains( notification ) )
  {
    kmWarning() << "Cannot relay notification activation, pointer" << notification << "not found!";
    return;
  }

 #ifdef KMESSDEBUG_NOTIFICATIONMANAGER
  kmDebug() << "Relaying activation for notification" << notification;
 #endif

  EventSettings settings = eventSettings_[ notification ];
  Buttons clickedButton = getButtonFromAction( settings.buttons, action );

  emit eventActivated( settings, clickedButton );
#endif /* !Q_WS_MAC */
}



// Delete an expired notification
void NotificationManager::remove( QObject *object )
{
#ifdef Q_WS_MAC
  // This doesn't need to happen on Mac
#else
  KNotification *notification = static_cast<KNotification*>( object );

  if( notification == 0 )
  {
 #ifdef KMESSDEBUG_NOTIFICATIONMANAGER
    kmDebug() << "Cannot remove notification, pointer" << notification << "not found!";
 #endif
    return;
  }

  // Don't delete the notification, it will do it automatically
  events_.remove( notification );
  eventSettings_.remove( notification );

 #ifdef KMESSDEBUG_NOTIFICATIONMANAGER
  kmDebug() << "Deleted notification" << notification << "from the stack";
 #endif
#endif /* !Q_WS_MAC */
}



// Return the notification object for when an application status event occur
StatusNotification* NotificationManager::statusNotification() const
{
  return statusNotification_;
}



#include "notificationmanager.moc"
