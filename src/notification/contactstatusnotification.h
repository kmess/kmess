/***************************************************************************
                          contactstatusnotification.h - notifies when a
                            contact changes their msn status
                             -------------------
    begin                : Saturday July 28 2007
    copyright            : (C) 2007 by Valerio Pilo
    email                : valerio@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CONTACTSTATUSNOTIFICATION_H
#define CONTACTSTATUSNOTIFICATION_H

#include "../contact/status.h"
#include "notificationmanager.h"

#include <QObject>
#include <KMess/NetworkGlobals>


// Forward declarations
class Contact;



/**
 * @brief Notifies the user when a contact has changed its MSN status
 *
 * Notification is made via a popup balloon and a sound (which can be customized)
 *
 * @author Valerio Pilo <valerio@kmess.org>
 * @ingroup Notification
 */
class ContactStatusNotification : public QObject
{
  Q_OBJECT

  public:  // Public methods
    // Class constructor
                          ContactStatusNotification( NotificationManager* manager );

  private:
    // Obtains a status string like "contact has come online" for a MSN status code
    const QString         getStatusString( const KMess::MsnStatus status, const QString& friendlyName ) const;

  signals:
    // Start a chat with a contact
    void                  startChat( const QString &contactHandle );

  public slots:
    void                  notify( KMess::MsnContact *contact, bool isInitialStatus = false );
    // Lock the notifications
    void                  lockNotifications();
    // Unlock the notifications
    void                  unlockNotifications();

  private slots:
    // Execute the action triggered in a notification
    void                  activate( NotificationManager::EventSettings settings, NotificationManager::Buttons button );

  private:  // Private properties
    // Whether to enable notifications or not
    bool                 enableNotifications_;
    // The notification manager instance
    NotificationManager  *manager_;


};


#endif
