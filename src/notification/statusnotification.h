/***************************************************************************
                          statusnotification.h - notifies about application status changes
                             -------------------
    begin                : Wed Jul 29 2009
    copyright            : (C) 2009 by Valerio Pilo <valerio@kmess.org>
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef STATUSNOTIFICATION_H
#define STATUSNOTIFICATION_H

#include "notificationmanager.h"

#include <KMess/NetworkGlobals>

#include <QObject>


// Forward declarations
class NotificationManager;
class PassivePopup;



/**
 * @brief Notifies the user when application status events occur
 *
 * Notification is made via a popup balloon and a sound (which can be customized)
 *
 * @author Valerio Pilo <valerio@kmess.org>
 * @ingroup Notification
 */
class StatusNotification : public QObject
{
  Q_OBJECT

  public:  // Public methods
    // Class constructor
                          StatusNotification( NotificationManager* manager );

  public slots:
    // Notify the user about this event (a status event has occurred)
    void                  notify( const KMess::StatusMessageType type, const KMess::StatusMessage message, const QVariant &extraInfo );

  private slots:
    // Execute the action triggered in a notification
    void                  activate( NotificationManager::EventSettings settings, NotificationManager::Buttons button );

  private:  // Private properties
    // The notification manager instance
    NotificationManager  *manager_;

};


#endif
