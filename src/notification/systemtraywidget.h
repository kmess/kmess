/***************************************************************************
                          systemtraywidget.h  -  description
                             -------------------
    begin                : Sun Dec 29 2002
    copyright            : (C) 2002 by Mike K. Bennett
    email                : mkb137b@hotmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SYSTEMTRAYWIDGET_H
#define SYSTEMTRAYWIDGET_H

#include <KSystemTrayIcon>


// Forward declarations
class KMenu;



/**
 * @brief System tray event handling.
 *
 * The user's status is displayed in the system tray.
 * It also gives access to some settings.
 * This class handles the events fired by the KDE systemtray.
 *
 * @author Mike K. Bennett
 * @ingroup Root
 */
class SystemTrayWidget : public KSystemTrayIcon
{
  Q_OBJECT

  public:
    // The constructor
                     SystemTrayWidget( QWidget *parent=0 );
    // The destructor
    virtual         ~SystemTrayWidget();
    // Display a customized close window with a screenshot of the app's tray icon
    void             displayCloseMessage( QString fileMenu = QString() );
    // Enable the system tray icon
    void             enable();
    // Return the context menu
    QMenu           *menu() const;

  public slots:
    // Change the icon when the user's status changes
    void             statusChanged();
};

#endif
