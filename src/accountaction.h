/***************************************************************************
                          accountaction.h  -  description
                             -------------------
    begin                : Tue Oct 22 2002
    copyright            : (C) 2002 by Mike K. Bennett
    email                : mkb137b@hotmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef ACCOUNTACTION_H
#define ACCOUNTACTION_H

#include <KAction>


// Forward declarations
class Account;



/**
 * An action menu for an account that gives an account object when
 * activated and can update its text when the account changes.
 *
 * @author Mike K. Bennett
 * @ingroup Root
 */
class AccountAction : public KAction
{
   Q_OBJECT

  public:
    // The constructor
    explicit AccountAction( Account *account, QWidget *parent = 0 );
    // The destructor
   ~AccountAction();

  public slots:
    // Update the action's text based on the account.
    void           updateText();

  protected slots:
    //Inherit from KAction...
    void           slotActivated();

  private: // Private attributes
    // The stored user account
    Account       *account_;

  signals:
    void           activated( Account *account );
};

#endif
