/***************************************************************************
                          initialview.h  -  description
                             -------------------
    begin                : Sun Jan 12 2003
    copyright            : (C) 2003 by Mike K. Bennett
    email                : mkb137b@hotmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef INITIALVIEW_H
#define INITIALVIEW_H

#include "ui_initialview.h"
#include "contact/status.h"

#include <QHash>
#include <QPainter>
#include <QTime>
#include <QTimer>

#include <Solid/Networking>


// Forward declarations
class Account;

namespace KMess
{
  enum StatusMessage;
  enum StatusMessageType;
  enum MsnStatus;
}


/**
 * @brief The login widget.
 *
 * Users can login by selecting an existing account, or entering their username/password directly.
 * When the login starts, the AutoLoginView widget is displayed in the main window.
 * This class implements the events defined in the inherited user interface class.
 *
 * @author Mike K. Bennett
 * @ingroup Root
 */
class InitialView : public QWidget, private Ui::InitialView
{
  Q_OBJECT

  public:
    // The constructor
                         InitialView( QWidget *parent );
    // The destructor
    virtual             ~InitialView();
    // PaintEvent used for theming
    void                 paintEvent( QPaintEvent * );
    // Reset the view to its initial state
    void                 reset();
    // Change the selected initial status
    void                 setSelectedStatus( KMess::MsnStatus status );
    // Start connecting with a specified account
    bool                 startConnecting( const QString handle, bool emitConnectionSignal = true );

  private: // Private methods
    // The users picture received an event.
    bool                 eventFilter( QObject *obj, QEvent *ev );

  public slots: // Public slots
    // Automatically reconnect with a specified account
    void                 reconnect( QString handle = QString(), bool connectImmediately = false );
    // Change the connection status text when interesting messages arrive
    void                 statusEvent( const KMess::StatusMessageType type, const KMess::StatusMessage message, const QVariant &extraInfo );
    // Show a message on the status label with a variable timeout
    void                 statusMessage( const QString &message = QString(), int timeout = 0 );

  private slots:
    // Add an account to the list of displayed accounts from which the user can choose
    void                 addAccount( Account *account );
    //The "auto connect" checkbox changed its state
    void                 autoLoginStateChanged( int state );
    // Modify an account in the list of displayed accounts from which the user can choose
    void                 changedAccount( Account *account, QString oldHandle, QString oldFriendlyName );
    // The account was deleted
    void                 deleteAccount( Account *account );
    // Get the currently selected handle
    const QString        getSelectedHandle() const;
    // The "remember account" checkbox changed its state
    void                 rememberAccountStateChanged( int state );
    // The "remember password" checkbox changed its state
    void                 rememberPasswordStateChanged( int state );
    // Update the reconnection timer data
    void                 slotReconnectTimerEvent();
    // Enable or disable the widgets when we're connecting or waiting
    void                 setEnabled( bool isEnabled );
    // The connect/disconnect button has been clicked
    void                 slotConnectClicked();
    // Detect changes in the status of the internet connection
    void                 slotConnectionStatusChanged( Solid::Networking::Status newStatus );
    // Execute the browser for a clicked UI link
    void                 slotClickedUrl( const QString &url );
    // A profile was selected from the drop-down list, or written manually.
    void                 updateView();

  private: // Private attributes
    // The application's configuration file
    KConfigGroup         config_;
    // Whether the UI is set up for connection or waiting for the login data
    bool                 isConnectingUI_;
    // The default handle to display
    QString              lastUsedHandle_;
    // Loader for icons
    KIconLoader         *loader_;
    // The current status of the network connection
    Solid::Networking::Status networkStatus_;
    // Account to reconnect with
    QString              reconnectionHandle_;
    // Number of seconds left before reconnection
    quint16              reconnectionRemainingSeconds_;
    // Timer to schedule reconnection
    QTimer               reconnectionTimer_;
    // Timer to blank the status message
    QTimer               statusMessageTimer_;
    // Whether KMess was trying to connect.
    bool                 triedConnecting_;

  signals: // Public signals
    // Connect with the given account
    void                 connectWithAccount( QString handle, QString password, bool rememberAccount, bool rememberPassword, bool autologin, KMess::MsnStatus initialStatus );
    // Show the settings of the current account.
    void                 showSettings( Account *account );
};

#endif
