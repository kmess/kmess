/***************************************************************************
                          kmessapplication.cpp -  description
                             -------------------
    begin                : Wed 7 13 2005
    copyright            : (C) 2005 by Diederik van der Boor
    email                : vdboor --at-- codingdomain.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "kmessapplication.h"

#include "dialogs/transferwindow.h"
#include "notification/notificationmanager.h"
#include "plugins/pluginsmaster.h"
#include "chat/chatmaster.h"
#include "utils/crashhandler.h"
#include "utils/idletimer.h"
#include "utils/kmessconfig.h"
#include "utils/kmessdbus.h"
#include "utils/nowlisteningclient.h"
#include "account.h"
#include "accountsmanager.h"
#include "emoticonmanager.h"
#include "mainwindow.h"
#include "kmessdebug.h"
#include "kmessglobal.h"
//#include "kmesstest.h"

#include <KMess/Utils>
#include <KMess/MPOPEndpoint>

#include "config-kmess.h"

#include <QDir>
#include <QNetworkProxy>

#include <KCmdLineArgs>
#include <KIconLoader>
#include <KMessageBox>
#include <KProtocolManager>
#include <KStandardDirs>



/**
 * The application constructor
 */
KMessApplication::KMessApplication()
: KUniqueApplication()
, chatMaster_( 0 )
, dbus_( 0 )
, idleTimer_( 0 )
, notificationManager_( 0 )
, nowListeningClient_( 0 )
, pluginsMaster_( 0 )
, quitSelected_( false )
{
  // KMessApplication is created from main.cpp.
  // It continues the initialisation of the application.

  // Install a message handler, so KMESS_ASSERT won't do a exit(1) or abort()
  // It makes debugging output on Windows disappear, so don't use it there
#ifndef Q_OS_WIN
  qInstallMsgHandler( kmessDebugPrinter );
#endif

#ifdef KMESSTEST
  kmDebug() << "Starting KMess " KMESS_VERSION " on" << QDateTime::currentDateTime().toString( Qt::ISODate );
  kmDebug() << "Compiled with KDE" KDE_VERSION_STRING ", Qt" QT_VERSION_STR;
  kmDebug() << "Running on KDE" << KDE::versionString() << ", Qt" << qVersion();
#endif

  // Install our crash handler.
  CrashHandler::activate();


  KCmdLineArgs *args = KCmdLineArgs::parsedArgs();

  // yes... we have it enough of bug reports about trunk.
  if( ! args->isSet( "iKnowThatTrunkIsBroken" ) )
  {
    kmDebug() << "The user is using trunk inadvertently: showing a warning.";
    KMessageBox::sorry( 0,
                        "<p><strong>Warning:</strong>This version of KMess is "
                        "<b>barely working</b> at the moment.<br/>"
                        "If you use a packaged binary, you should use KMess 2.0.4.</p>"
                        "<p>If you instead use SVN or Git, switch your checkout to "
                        "one from the 2.0.x branch, which works and also gets new features.<br/>"
                        "You can find the simple instructions on how to do it here: "
                        "<a href='http://bit.ly/FVtmo'>http://bit.ly/FVtmo</a></p>"
                        "<p><b>KMess will stay running, but you have been warned.</b></p>",
                        "Application error",
                        KMessageBox::Notify |
                        KMessageBox::AllowLink |
                        KMessageBox::PlainCaption );
  }
  else
  {
    kmWarning() << "It seems the user knows what he/she's doing.";
  }

#if ( KMESS_DEBUG == 1 )
  // The debug build allows to use an alternative server
  testServerAddress_ = args->getOption( "server" );
#endif

  // Start remaining initialisation
  initializePaths();
  initializeServices();
  initializeProxy();
  initializeMainWindow();
}



/**
 * Destructor
 */
KMessApplication::~KMessApplication()
{
  // Delete all chat windows
  delete chatMaster_;

  // unset logged in user
  Account::connectedAccount = 0L;

  // while globalSession still uses classes like the AccountsManager and
  // KMessConfig, we have to destroy it first. connectedAccount must be unset
  // before, so everything knows no user is logged in anymore.
  delete globalSession;
  globalSession = 0;

  AccountsManager::destroy();
  TransferWindow::destroy();
  KMessConfig::destroy();
  EmoticonManager::destroy();

  delete dbus_;
  delete idleTimer_;
  delete notificationManager_;
  delete nowListeningClient_;
  delete pluginsMaster_;

#ifdef KMESSDEBUG_KMESSAPPLICATION
  kmDebug() << "DESTROYED.";
#endif
}



/**
 * Returns the chat master
 */
ChatMaster *KMessApplication::chatMaster() const
{
  return chatMaster_;
}



/**
 * Return the server used in test server mode
 */
const QString &KMessApplication::getTestServer() const
{
  return testServerAddress_;
}



/**
 * Return whether to use an alternative server as the Live Service server
 */
bool KMessApplication::getUseTestServer() const
{
  return ( ! testServerAddress_.isEmpty() );
}



/**
 * Initialisation of the main window.
 */
void KMessApplication::initializeMainWindow()
{
  // Fetch the command line arguments
  KCmdLineArgs *args = KCmdLineArgs::parsedArgs();
  const QString &autologinHandle( args->getOption( "autologin" ) );

  // Enable KDE session restore.
  // We should use KMainWindow's RESTORE macro, or the kRestoreMainWindows
  // macro (see kmainwindow.h).
  // But they create new objects and also restore all windows: we have no
  // point in restoring chat windows. Our initialization process is also
  // different.
  // So we just manually restore the KMess main window, which is the
  // contact list window.
  int restoredWindow = -1;
  if( kapp->isSessionRestored() )
  {
    int n = 0;
    while( KMainWindow::canBeRestored( ++n ) )
    {
#ifdef KMESSDEBUG_KMESSAPPLICATION
      kmDebug() << "Checking if window" << n << "named" << KMainWindow::classNameOfToplevel( n ) << "can be restored...";
#endif
      if( KMainWindow::classNameOfToplevel( n ) != QLatin1String( "KMess" ) )
      {
        continue;
      }

#ifdef KMESSDEBUG_KMESSAPPLICATION
      kmDebug() << "Contact List window found!";
#endif
      restoredWindow = n;
      break;
    }
  }

  // Create the main window and initialize it
  MainWindow *mainWindow = MainWindow::instance();

  // Show the screen where to insert login details to log in
  mainWindow->switchViewToInitialScreen();

  // Initialize KApplication
  setTopWidget( mainWindow );

  // Connect the reconnection event coming from chat windows
  connect( chatMaster_, SIGNAL( reconnect() ), mainWindow, SLOT( reconnect() ) );

  // Apply our own quit policy.
  // Only quit when the user wanted to, or KDE is logging out.
  connect( this, SIGNAL(lastWindowClosed()), this, SLOT(slotLastWindowClosed()) );
  connect( this, SIGNAL(aboutToQuit()),      this, SLOT(slotAboutToQuit()) );

#ifdef __GNUC__
#warning The --runtest option is currently disabled
#endif
#if 0
  // The debug build allows to run tests from the command line.
  const QString &testName( args->getOption( "runtest" ) );
  if( ! testName.isEmpty() )
  {
    // KMessTest is deleted when the app closes
    KMessTest *kmessTest = new KMessTest( mainWindow );
    kmessTest->runTest( testName );
    kmessTest->endTest();
    return;
  }
#endif

  // We found session data for the Contact List, to restore it
  if( kapp->isSessionRestored() && restoredWindow != -1 )
  {
    // Wait for the user passwords
    AccountsManager::instance()->readPasswords( true );

    mainWindow->restore( restoredWindow, false );
  }
  else
  {
    // Start auto login if needed, then show the login screen
    mainWindow->checkAutologin( autologinHandle );

    const KConfigGroup& group( KMessConfig::instance()->getGlobalConfig( "General" ) );

    bool hidden = group.readEntry( "hiddenStart", false );

    if( ! ( args->isSet( "hidden" ) || hidden ) )
    {
      mainWindow->show();
    }
  }
}



/**
 * Initialize additional application paths and create the data dirs if not present
 */
void KMessApplication::initializePaths()
{
  // Add compile time paths as fallback
  KGlobal::dirs()      ->addPrefix( KMESS_PREFIX );
  KIconLoader::global()->addAppDir( KMESS_PREFIX "/share" );

  // Test whether the prefix is correct.
  if( KGlobal::dirs()->findResource( "appdata", "pics/kmesspic.png" ).isNull() )
  {
    kmWarning() << "KMess could not find resources in the search paths: "
               << KGlobal::dirs()->findDirs( "appdata", QString() ).join(", ") << endl;
  }

  QDir appsDir, kmessDir;
  const QString &localKdeDir( KGlobal::dirs()->localkdedir() );

#ifdef KMESSDEBUG_KMESSAPPLICATION
  kmDebug() << "Local KDE directory:" << localKdeDir;
#endif

  appsDir.setPath( localKdeDir + "/share/apps" );

  if( appsDir.exists() )
  {
    kmessDir.setPath( appsDir.absolutePath() + "/kmess" );

#ifdef KMESSDEBUG_KMESSAPPLICATION
    kmDebug() << "Local KMess directory:" << kmessDir.absolutePath();
#endif

    if( ! kmessDir.exists() )
    {
#ifdef KMESSDEBUG_KMESSAPPLICATION
      kmDebug() << "Local KMess directory did not exist, creating it.";
#endif
      appsDir.mkdir( kmessDir.absolutePath() );
    }
  }
#ifdef KMESSDEBUG_KMESSAPPLICATION
  else
  {
    kmWarning() << "Local KMess directory:" << kmessDir.absolutePath() << "does not exist!";
  }
#endif

#ifdef KMESSTEST
  KMESS_ASSERT( kmessDir.exists() );
#endif


  // Check whether the 'eventsrc' file can be found.
  // The warning is a bit obtruisive, but should help to user to fix the problem.
  if( KGlobal::dirs()->findResource( "data", "kmess/kmess.notifyrc" ).isNull() )
  {
    QString dirs( KGlobal::dirs()->findDirs( "data", QString() ).join("/kmess<br>") );
    if( ! dirs.isEmpty() )
    {
      dirs = "<br>" + dirs + "/kmess";
      dirs = dirs.replace( "//kmess", "/kmess" );  // possibly added by /kmess suffix.
      dirs = i18nc( "Paragraph to be added to the text of a message dialog box, but only when KDE gives a list of "
                    "folders where to search for an application file",
                    "<p>KMess has searched for it in the following folders:<br>%1</p>",
                    dirs );
    }

    // Always show warning at the console.
    kmWarning() << "Sounds and notifications will not be available, the file 'kmess/kmess.notifyrc' could not be found.";

    // Show the message in the GUI as well.
    // Allow to user to choose "don't show this message again", in case he/she doesn't know how to fix it.
    KMessageBox::information( 0,
                              i18nc( "Text for a message dialog box; %1 is an explanation about the list of folders "
                                     "where the file was searched for, which is only shown if any folders are found",
                                     "<html><p>KMess will not be able to play sounds nor show notifications.</p>"
                                     "<p>The required file 'kmess.notifyrc' could not be found in any application folder.</p>"
                                     "%1"
                                     "<p>Please verify your installation.</p>"
                                     "</html>",
                                     dirs ),
                              i18nc( "Message box title", "Error With Notifications" ),
                              "eventsrcNotFound" );
  }
}



// Initialize the proxy support
void KMessApplication::initializeProxy()
{
  // NOTE: Due to the fact that KDE proxy settings do not have a signal to notify proxy
  // config changes, KMess will need to be restarted if the user changes them.

  // No proxy, done already!
  if( ! KProtocolManager::useProxy() )
  {
#ifdef KMESSDEBUG_KMESSAPPLICATION
    kmDebug() << "No proxy support is needed (kde's proxy is turned off).";
#endif
    return;
  }

  QString proxyAddress( KProtocolManager::proxyForUrl( KUrl( "https://www.kmess.org/" ) ) );

  // No need to use the proxy for HTTPS traffic
  if( proxyAddress.isEmpty() || proxyAddress == "DIRECT" )
  {
#ifdef KMESSDEBUG_KMESSAPPLICATION
    kmDebug() << "No proxy support is needed (no proxy is needed for HTTPS traffic).";
#endif
    return;
  }

#ifdef KMESSDEBUG_KMESSAPPLICATION
  kmDebug() << "Proxy URL:" << proxyAddress;
#endif

  KUrl proxyUrl( proxyAddress );
  QNetworkProxy::ProxyType proxyType = QNetworkProxy::NoProxy;

  // Configure the proxy
  if( proxyUrl.protocol() == "socks" )
  {
    proxyType = QNetworkProxy::Socks5Proxy;
  }
  else
  {
    proxyType = QNetworkProxy::HttpProxy;
  }

  // Set the default proxy for the whole application
  QNetworkProxy appProxy( proxyType,
                          proxyUrl.host(),
                          (quint16) proxyUrl.port(),
                          proxyUrl.user(),
                          proxyUrl.pass() );
  QNetworkProxy::setApplicationProxy( appProxy );

  return;
}



/**
 * Initialize the program service classes and helpers
 */
void KMessApplication::initializeServices()
{
#ifdef KMESSDEBUG_KMESSAPPLICATION
  kmDebug() << "Starting service classes";
#endif

  // Set up the DBus interface
  dbus_ = new KMessDBus( globalSession );

  // Create the current session instance
  globalSession = new KMess::MsnSession();

  // set local endpoint name.

  globalSession->self()->localEndpoint()->setName( KMESS_VERSION );

  // set our application capabilities and name/version.
  KMess::Utils::setClientCapabilities(
      KMess::ClientCapabilities::CF_SUPPORTS_WINKS
    | KMess::ClientCapabilities::CF_SUPPORTS_GIF_INK
    | KMess::ClientCapabilities::CF_SUPPORTS_ISF_INK );
  KMess::Utils::setClientName( "KMess", KMESS_VERSION );

  // Create the ChatMaster
  chatMaster_ = new ChatMaster( this );

  // create the ContactsManager
  contactsManager = new ContactsManager();

  // Get the machine GUID of this KMess installation
  KConfigGroup group( KMessConfig::instance()->getGlobalConfig( "General" ) );
  QString machineGuid( group.readEntry( "machineGuid", QString() ) );

  // Create a new machine GUID if none was present
  // NOTE In reality it's a kde-installation GUID, but I'd refrain
  // from implementing a more system-wide solution.
  if( machineGuid.isEmpty() )
  {
    machineGuid = KMess::Utils::generateGUID();
    group.writeEntry( "machineGuid", machineGuid );
  }

  // Assign it to the MsnSession
  globalSession->self()->localEndpoint()->setGuid( machineGuid );
  globalSession->self()->localEndpoint()->setName( "KMess3.0" );

  // And set some more configuration
  globalSession->setSessionSetting( "P2PTransferLowestPort", group.readEntry( "lowestTransferPort", 6891 ) );
  globalSession->setSessionSetting( "P2PTransferHighestPort", group.readEntry( "highestTransferPort", 6900 ) );
  if( getUseTestServer() )
  {
    globalSession->setSessionSetting( "SoapRedirectionServer", getTestServer() );
    globalSession->setSessionSetting( "NotificationServerOverride", getTestServer() );
  }

  // Initialize the idle time checker
  idleTimer_ = new IdleTimer();

  // Initialise the Plugins Master, so plugins can be executed when kmess logs in
  pluginsMaster_ = new PluginsMaster( this );

  // Create the notification manager
  notificationManager_ = NotificationManager::instance();

  // Create the Now Listening client
  nowListeningClient_ = new NowListeningClient();

  connect( globalSession, SIGNAL(   connecting()         ),
           chatMaster_,   SLOT(     connecting()         ) );
  connect( globalSession, SIGNAL(     loggedIn()         ),
           chatMaster_,   SLOT(      connected()         ) );
  connect( globalSession, SIGNAL(    loggedOut(KMess::DisconnectReason)         ),
           chatMaster_,   SLOT(   disconnected()         ) );
  connect( globalSession, SIGNAL(  chatCreated(KMess::MsnChat*) ),
           chatMaster_,   SLOT(    chatCreated(KMess::MsnChat*) ) );

}



/**
 * Get the current instance.
 *
 * This is a convenience method so it's easier to access KMessApplication through kapp.
 */
KMessApplication *KMessApplication::instance()
{
  return static_cast<KMessApplication*>( kapp );
}



/**
 * Return the idle timer object
 */
IdleTimer *KMessApplication::idleTimer() const
{
  return idleTimer_;
}



/**
 * Return the now listening object
 */
NowListeningClient *KMessApplication::nowListeningClient() const
{
  return nowListeningClient_;
}



/**
 * Return the plugins master object
 */
PluginsMaster *KMessApplication::pluginsMaster() const
{
  return pluginsMaster_;
}


/**
 * Get the notification that the app is quitting
 */
void KMessApplication::slotAboutToQuit()
{
  // No user-interaction is possible here anymore..!

#ifdef KMESSDEBUG_KMESSAPPLICATION
  kmDebug() << "KMess is about to quit...";
#endif
}



/**
 * Quit the application.
 *
 * Called when the last window is closed.
 *
 * @see KMessInterface::queryClose()
 */
void KMessApplication::slotLastWindowClosed()
{
  if( quitSelected_ || sessionSaving() )
  {
#ifdef KMESSDEBUG_KMESSAPPLICATION
    kmDebug() << "Last window closed, user really wants to quit now. Initiate quit() call.";
#endif

    quit();
  }
  else
  {
#ifdef KMESSDEBUG_KMESSAPPLICATION
    kmDebug() << "User closed last window, but keep KMess running with all windows closed.";
#endif
  }
}



/**
 * Return true if quit was selected
 */
bool KMessApplication::quitSelected() const
{
  return quitSelected_;
}



/**
 * Tell the application that quit was selected
 */
void KMessApplication::setQuitSelected(bool quitSelected)
{
  quitSelected_ = quitSelected;
}



#include "kmessapplication.moc"
