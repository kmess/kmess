/***************************************************************************
                          contactsmanager.cpp  -  description
                             -------------------
    begin                : Tue 03 Nov 2009
    copyright            : (C) 2009 by Adam Goossens
    email                : adam@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "contactsmanager.h"

#include "../utils/kmessconfig.h"
#include "../kmessdebug.h"

#include "kmessglobal.h"

#include "contactprivate.h"
#include "groupprivate.h"

#include <KMess/MsnContact>
#include <KMess/MsnGroup>

#include "account.h"


/**
 * The Constructor
 */
ContactsManager::ContactsManager()
    : selfPrivate_(0)
{
}



/**
 * Destroys this ContactsManager instance and deletes all ContactData held within.
 */
ContactsManager::~ContactsManager()
{
  clear();
}




/**
 * Clear all of the private data, ready to receive fresh information.
 *
 * This should be called by KMess when it wants to completely reset the session to a blank
 * slate - ie, on a user-intiated disconnect.
 *
 * In the case of /unintentional disconnect/ (where we want to attempt a reconnect)
 * although you can clear this info without any side-effects (it'll just get reloaded from disk)
 * you should persist it - private data is indexed by contact and group ID so it will get reused
 * when the connection is re-established.
 *
 */
void ContactsManager::clear()
{
  // delete all ContactData and GroupData instances.
  qDeleteAll( contactStore_.values() );
  qDeleteAll( groupStore_.values() );

  contactStore_.clear();
  groupStore_.clear();

  if ( selfPrivate_ )
  {
    delete selfPrivate_;
    selfPrivate_ = 0;
  }

  kmDebug() << "All contact and group information cleared.";
}



/**
 * Return private ContactData by providing an MsnContact instance.
 */
ContactPrivate *ContactsManager::contactData( const KMess::MsnContact *msnContact )
{
  ContactPrivate *data = 0;

  if ( msnContact == 0 ) return 0;

  QString id( msnContact->handle() );

  if ( ! contactStore_.contains( id ) )
  {
    // data object doesn't exist, so create it once.
    // the object will automatically load config.
    data = new ContactPrivate( msnContact );

    if ( Account::connectedAccount != 0 )
    {
      data->readProperties( contactsConfigGroup_ );
    }

    contactStore_.insert( id, data );
  }
  else
  {
    data = contactStore_[ id ];
  }

  return data;
}



/**
 * Return private ContactData by providing a contact handle.
 */
ContactPrivate *ContactsManager::contactData( const QString &handle )
{
  return contactData( globalSession->contactList()->contact( handle ) );
}



/**
 * Return private Group data by providing an MsnGroup instance.
 */
GroupPrivate *ContactsManager::groupData( const KMess::MsnGroup *msnGroup )
{
  GroupPrivate *data = 0;

  if ( msnGroup == 0 ) return 0;

  QString id( msnGroup->id() );
  if ( ! groupStore_.contains( id ) )
  {
    // data object doesn't exist, so create it once.
    data = new GroupPrivate( msnGroup );

    // read config from disk
    data->readProperties( groupsConfigGroup_ );

    // and cache in group store.
    groupStore_.insert( id, data );
  }
  else
  {
    data = groupStore_[ id ];
  }

  return data;
}



/**
 * Return private Group data by providing a group ID.
 */
GroupPrivate *ContactsManager::groupData( const QString &groupId )
{
  return groupData( globalSession->contactList()->group( groupId ) );
}



/**
 * Save the contact and group data to disk "as-is"
 */
void ContactsManager::saveProperties()
{
  {
    KConfigGroup cfg = KMessConfig::instance()->getContactListConfig( Account::connectedAccount->getHandle(), "Contacts" );
    foreach( ContactPrivate *data, contactStore_ )
    {
      if ( data ) data->saveProperties( cfg );
    }

    cfg.sync();
  }

  {
    KConfigGroup cfg = KMessConfig::instance()->getContactListConfig( Account::connectedAccount->getHandle(), "Groups" );
    foreach( GroupPrivate *data, groupStore_ )
    {
      if ( data ) data->saveProperties( cfg );
    }

    cfg.sync();
  }
}



/**
 * Pre-loads KConfigGroup objects for Contacts and Groups. Saves us
 * re-creating them multiple times for each contact.
 * @param account The account to read configuration information from
 */
void ContactsManager::loadConfig( Account *account )
{
  contactsConfigGroup_ = KConfigGroup( KMessConfig::instance()->getContactListConfig( account->getHandle(), "Contacts" ) );
  groupsConfigGroup_   = KConfigGroup( KMessConfig::instance()->getContactListConfig( account->getHandle(), "Groups" ) );
}



/**
 * Save the config for a given contact to disk.
 *
 * Will not save if the account is invalid or is a guest account.
 *
 * @param contact The MsnContact whose KMess data you wish to save.
 */
void ContactsManager::saveContact( const KMess::MsnContact *contact )
{
  if ( contact == 0 )
  {
    kmWarning() << "Not saving information for an invalid MsnContact.";
    return;
  }

  Account *account = Account::connectedAccount;

  // Do not save account properties when it's a temporary account
  if( account == 0 || account->isGuestAccount() )
  {
    kmWarning() << "Not saving ContactList properties for unknown or guest account.";
    return;
  }

  KConfigGroup config( KMessConfig::instance()->getContactListConfig( account->getHandle(), "Contacts" ) );

  // Save the contact
  ContactPrivate *data = contactStore_[ contact->handle() ];
  if ( data )
  {
    data->saveProperties( config );
  }

  // Write the data now!
  config.sync();
}




/**
 * Save the contact list to disk. Does not save if this is a guest or temporary account.
 */
void ContactsManager::saveContactList()
{
#ifdef KMESSDEBUG_CONTACTLIST
  kmDebug() << "Saving properties for the contact list";
#endif

  Account *account = Account::connectedAccount;

  // Do not save account properties when it's a temporary account
  // TODO remove all the isGuestAccount to another class. In this class we should have only reference to the connected Account
  // TODO 2: remove all these saveProperties anyway, and change them to send all state to the frontend for it to save it itself
  //         (they don't belong in the backend library)
  if( account == 0 || account->isGuestAccount() )
  {
    kmWarning() << "Not saving ContactList properties for unknown or guest account.";
    return;
  }

#ifdef __GNUC__
#warning TODO: Rewrite this entire chunk - contact list saving.
#endif
#if 0
  // There always are at least some special groups to be saved: we can never skip saving
  // like it's done with contacts below
  KConfigGroup config( KMessConfig::instance()->getContactListConfig( account->getHandle(), "Groups" ) );

  // Save all groups
  foreach( Group *group, contactList->getGroupList() )
  {
    group->saveProperties( config );
  }

  // Write the data now!
  config.sync();

  QHash<QString, Contact*> contacts_ = contactList->getContactList();

  // Skip saving contacts if there aren't any, as above
  if( ! contacts_.isEmpty() )
  {
    config = KMessConfig::instance()->getContactListConfig( account->getHandle(), "Contacts" );

    // We want to delete unused contact info; so here we get a list of all contacts and remove
    // the used ones. Then we remove what's left in this list
    QStringList pendingRemoval( config.groupList() );

    // Save all contact extensions
    foreach( KMess::MsnContact *c, contactList->getContactsList() )
    {
      pendingRemoval.removeAll( contact->handle() );
      contact->saveProperties( config );
    }

    // Delete the remaining contacts info, which at this point are only old contacts not in list anymore
    QFile picture;
    KConfigGroup contactConfig;
    foreach( const QString &unusedContact, pendingRemoval )
    {
  #ifdef KMESSDEBUG_CONTACTLIST
      kmDebug() << "Deleting unused contact info for " << unusedContact;
  #endif

      // Remove the custom picture associated with this contact, if there is any
      contactConfig = config.group( unusedContact );
      picture.setFileName( contactConfig.readEntry( "pictureFile", "" ) );
      if( picture.exists() )
      {
        picture.remove();
      }
      config.deleteGroup( unusedContact );
    }

    // Write the data now!
    config.sync();
  }
#ifdef KMESSDEBUG_CONTACTLIST
  else
  {
  kmDebug() << "No contacts to save";
  }
#endif
#endif
}




/**
 * Save a group's KMess-specific data to disk.
 *
 * Will not save if the account is invalid or is a guest account.
 *
 * @param group The MsnGroup whose KMess-specific data you want to save.
 */
void ContactsManager::saveGroup( const KMess::MsnGroup *group )
{
  if ( group == 0 )
  {
    kmWarning() << "Not saving information for invalid MsnGroup";
    return;
  }

  Account *account = Account::connectedAccount;

  // Do not save account properties when it's a temporary account
  if( account == 0 || account->isGuestAccount() )
  {
    kmWarning() << "Not saving ContactList properties for unknown or guest account.";
    return;
  }

  KConfigGroup config( KMessConfig::instance()->getContactListConfig( account->getHandle(), "Groups" ) );

  // Save the contact
  GroupPrivate *data = groupStore_[ group->id() ];
  if ( data )
  {
    data->saveProperties( config );
  }
  // Write the data now!
  config.sync();
}
