/***************************************************************************
                          contact.cpp  -  description
                             -------------------
    begin                : Thu Jan 16 2003
    copyright            : (C) 2003 by Mike K. Bennett
                           (C) 2009 by Adam Goossens
    email                : mkb137b@hotmail.com
                           adam@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "contact.h"
#include "contactprivate.h"
#include "../kmessdebug.h"
#include "contactextension.h"
#include "account.h"

#include <KMess/Utils>
#include <KMess/MsnObject>
#include <KMess/MsnContact>

#include <QFile>
#include <QImage>

#include <KConfigGroup>
#include <KLocale>
#include <KStandardDirs>



// create based on contact handle
Contact::Contact( const QString &handle )
  : d( contactsManager->contactData( handle ) )
{
}



// copy constructor - just copy the d-pointer.
Contact::Contact( const Contact &other )
{
  d = other.d;
}



// Sneaky constructor that will automagically
// get the appropriate private data by testing the type of sender.
//
// You can do the following: Contact c(sender())
Contact::Contact( const QObject *sender )
{
  const ContactPrivate *p = qobject_cast<const ContactPrivate*>( sender );
  const KMess::MsnContact *c = qobject_cast<const KMess::MsnContact*>( sender );
  const ContactExtension *e = qobject_cast<const ContactExtension*>( sender );

  if ( p )
  {
    d = const_cast<ContactPrivate*>(p);
  }
  else if ( e )
  {
    d = contactsManager->contactData( e->getContact() );
  }
  else
  {
    d = contactsManager->contactData( c );
  }
}



// The destructor
Contact::~Contact()
{
  // don't delete d-pointer; that's done when ContactsManager is deleted.
}



// Add a custom emoticon definition
void Contact::addEmoticonDefinition(const QString &emoticonCode, const QString &msnObjectDataHash )
{
#ifdef KMESSDEBUG_CONTACTBASE
  kmDebug() << "adding '" << emoticonCode << "' to pending replacements";
#endif

  // Escape the code so the HTML parser won't be fooled.
  QString shortcut( emoticonCode );
  KMess::Utils::htmlEscape( shortcut );

  // Add emoticon to pending list, until Msn object is received.
  d->pendingEmoticons_.insert( msnObjectDataHash, shortcut );

  // Regenerate the pending emoticon regexp
  regeneratePendingEmoticonPattern();
}




/**
 * A contact is "valid" if its d-pointer doesn't point to null.
 */
bool Contact::isValid() const
{
  return d != 0;
}



// Add a custom emoticon file
void Contact::addEmoticonFile(const QString &msnObjectDataHash, const QString &filename)
{
  // Check whether the mapping is available.
  if( ! d->pendingEmoticons_.contains(msnObjectDataHash) )
  {
    kmWarning() << "Received an custom emoticon, but no emoticon code found!";
    return;
  }

  // Get the emoticon code from pending list, remove it
  QString emoticonCode( d->pendingEmoticons_[msnObjectDataHash] );    // no QString& because record gets removed.
  d->pendingEmoticons_.remove(msnObjectDataHash);

#ifdef KMESSDEBUG_CONTACTBASE
  kmDebug() << "linking '" << emoticonCode << "' to received file.";
#endif

  // Check if filename exists, attempt to read it.
  if( ! QFile::exists(filename) )
  {
    kmWarning() << "Could not read emoticon file: file not found!";
    return;
  }
  QImage emoticonData(filename);
  if( emoticonData.isNull() )
  {
    kmWarning() << "Could not read emoticon file!";
    return;
  }

#ifdef KMESSDEBUG_CONTACTBASE
  kmDebug() << "Setting image for '" << emoticonCode << " to " << filename;
#endif

  // Escape the code to insert it safely into a regular expression
  QString escapedCode( QRegExp::escape( emoticonCode ) );

  // Update regexp pattern
  if( d->emoticonRegExp_.isEmpty() )
  {
    d->emoticonRegExp_ = QRegExp( escapedCode );
  }
  else
  {
    d->emoticonRegExp_ = QRegExp( d->emoticonRegExp_.pattern() + "|" + escapedCode );
  }

  // Regenerate the pending emoticon pattern
  regeneratePendingEmoticonPattern();

#ifdef KMESSTEST
  KMESS_ASSERT( d->emoticonRegExp_.isValid() );
#endif


  // Get the image dimensions from the file
  int originalWidth = emoticonData.width();
  int originalHeight = emoticonData.height();

  // Resize the displayed image to match KMess' maximum emoticon size
  int width  = originalWidth; // qMin( EMOTICONS_MAX_SIZE, originalWidth ); // Require only a max height
  int height = qMin( EMOTICONS_MAX_SIZE, originalHeight );

  if( originalWidth > originalHeight )
  {
      height = ( width * originalHeight ) / originalWidth;
  }
  else
  {
      width = ( height * originalWidth ) / originalHeight;
  }

  // Add replacement.
  QString altCode ( emoticonCode );
  altCode = KMess::Utils::htmlUnescape( altCode ).replace("'", "&#39;");
  QString replacement( "<img src='" + filename
                     + "' alt='" + altCode
                     + "' width='" + QString::number( width )
                     + "' height='" + QString::number( height )
                     + "' valign='middle'"
                     +  " class='customEmoticon' />" );

  d->customEmoticons_.insert( emoticonCode, replacement );
  d->customHashes_.insert( emoticonCode, msnObjectDataHash );
}



// Register the contact as participant in the chat session
void Contact::addSwitchboardConnection( const KMess::MsnSwitchboardConnection *connection )
{
#ifdef KMESSDEBUG_CONTACTBASE
  kmDebug() << "adding connection to list.";
#endif

  if( d->chatSessions_.contains(connection) )
  {
    kmWarning() << "chat session is already added.";
    return;
  }

  d->chatSessions_.append( connection );
}



// Set the application name based on the capabilities
void Contact::detectClientName( const QString capabilities )
{
  // Note there are two ways to detect the client name.
  // - in the notification connection, we can read the 'capabilities' and filename from the 'msnobject' (which some thirdparty clients tag with their name).
  // - in the switchboard connection, a x-clientcaps message is sent by most thirdparty clients with exact version information.

  KMess::ClientCapabilities caps;

  if( capabilities.isEmpty() )
  {
    caps = d->msnContact_->capabilities();
  }
  else
  {
    caps = KMess::ClientCapabilities( capabilities );
  }

  // Third party clients also include their name in the MsnObject,
  // since that's already available at the notification connection.
  QString newClientName;
  QString pictureClientName;
  const KMess::MsnObject msnObject( d->msnContact_->msnObject() );
  KMess::ClientCapabilities::Version msnVersion = caps.getClientVersion();

  if( msnObject.isValid() )
  {
    const QString pictureLocation( msnObject.getLocation() );

    if( ! pictureLocation.isEmpty()
    &&    pictureLocation != "0"                // WLM 8.1
    &&  ! pictureLocation.startsWith( "TFR" ) ) // MSN / WLM
    {
      // Known variations:
      if( pictureLocation == "KMess.tmp" )
      {
        pictureClientName = "KMess";
      }
      else if( pictureLocation == "Mercury.tmp" )
      {
        pictureClientName = "Mercury";
      }
      else if( pictureLocation == "kopete.tmp" or pictureLocation == "1.tmp" )
      {
        pictureClientName = "Kopete";
      }
      else if( pictureLocation == "amsn.tmp" )
      {
        pictureClientName = "aMSN";
      }
      else if( pictureLocation == "EncartaIcon.png" )
      {
        // Encarta Instant Answers.
        // ignore, no special string for this one.
      }
      else
      {
#ifdef KMESSDEBUG_CONTACTBASE
        kmDebug() << "NOTICE: Unknown client name in MsnObject location:" << pictureLocation;
#endif
      }
    }
  }

  if( ! pictureClientName.isNull() )
  {
    newClientName = pictureClientName;
  }
  // Check capabilities for special software
  else if( caps.isOnlineViaMobileDevice() )
  {
    newClientName = i18n( "Windows Mobile" );
  }
  else if( caps.isOnlineViaTheWeb() )
  {
    newClientName = i18n( "Web Messenger" );
  }
  else if( caps.caps() & KMess::ClientCapabilities::CF_ONLINE_VIA_FEDERATED_GATEWAY )
  {
    newClientName = i18n( "Yahoo Messenger or Office Communicator" );
  }
  else if( caps.caps() & KMess::ClientCapabilities::CF_IS_MEDIA_CENTER_EDITION_USER )
  {
    newClientName = i18n( "Windows Media Center" );
  }
  else if( caps.caps() & KMess::ClientCapabilities::CF_IS_BOT )
  {
    newClientName = i18n( "Messenger Bot" );
  }
  else
  {
    if( msnVersion.major == 0 )
    {
      // Pre-P2P clients. MSN 4.x is locked out after the switch to SSL logins.
      newClientName = i18n( "MSN Messenger %1 compatible", QString( "5.0" ) );
    }
    else if( msnVersion.major < 8 )
    {
      // pre wlm client
      newClientName = i18n( "MSN Messenger %1 compatible", QString::number( msnVersion.major ) );
    }
    else
    {
      // WLM client.
      // See if it has all capabilities of WLM 8.1 / 2009, or it's a third party client which is just faking...
      // We don't check for the CF_SUPPORTS_ISF_INK flag, as it is optional and requires Windows Journal to be installed.
      const KMess::ClientCapabilities::CapabilitiesFlags wlm80Mask =
          KMess::ClientCapabilities::CF_SUPPORTS_GIF_INK
        | KMess::ClientCapabilities::CF_SUPPORTS_MESSAGE_CHUNKING
        | KMess::ClientCapabilities::CF_SUPPORTS_DIRECTIM
        | KMess::ClientCapabilities::CF_SUPPORTS_WINKS
        | KMess::ClientCapabilities::CF_SUPPORTS_SHARED_SEARCH
        | KMess::ClientCapabilities::CF_SUPPORTS_VOICEIM
        | KMess::ClientCapabilities::CF_P2P_SUPPORTS_SECURECHANNEL
        | KMess::ClientCapabilities::CF_SUPPORTS_SIP_INVITE
        | KMess::ClientCapabilities::CF_SUPPORTS_SHARED_DRIVE; // FIXME: TURN / UUN seen in 8.1, also in 8.0?

      const KMess::ClientCapabilities::CapabilitiesFlags wlm2009Mask =
          KMess::ClientCapabilities::CF_SUPPORTS_GIF_INK
        | KMess::ClientCapabilities::CF_SUPPORTS_MESSAGE_CHUNKING
        | KMess::ClientCapabilities::CF_SUPPORTS_DIRECTIM
        | KMess::ClientCapabilities::CF_SUPPORTS_WINKS
        | KMess::ClientCapabilities::CF_SUPPORTS_SHARED_SEARCH
        | KMess::ClientCapabilities::CF_SUPPORTS_VOICEIM
        | KMess::ClientCapabilities::CF_P2P_SUPPORTS_SECURECHANNEL
        | KMess::ClientCapabilities::CF_SUPPORTS_SIP_INVITE
        | KMess::ClientCapabilities::CF_SUPPORTS_SHARED_DRIVE
        | KMess::ClientCapabilities::CF_SUPPORTS_MULTIPARTY_MEDIA
        | KMess::ClientCapabilities::CF_P2P_SUPPORTS_TURN
        | KMess::ClientCapabilities::CF_P2P_SUPPORTS_BOOTSTRAPPING_VIA_UUN;

      bool isRealWlm = ( ( msnVersion.major < 9 && ( caps.caps() & wlm80Mask ) == wlm80Mask )
                      || ( caps.caps() & wlm2009Mask ) == wlm2009Mask );

      if( isRealWlm )
      {
        if( msnVersion.major )
        {
          // Supports all default caps of 8.1 or 2009, remove the "compatible" string. This truly is WLM.
          newClientName = i18n( "Windows Live Messenger %1", msnVersion.toString() );
        }
        else
        {
          // Beyond our current finger print database, can't show version number yet.
          newClientName = i18n( "Windows Live Messenger" );
        }
      }
      else
      {
        if( msnVersion.major )
        {
          newClientName = i18n( "Windows Live Messenger %1 compatible", msnVersion.toString() );
        }
        else
        {
          // Kopete seems to use 0xc0000000 as capabilities.. 2 versions beyond WLM2009
          // (this is fixed in a later version, turns out they added all supported MSN versions instead of just sending the newest one)
          // Its caps are 0xc0148028; ink-isf, multi-packet, winks, voice-clips, sip
          newClientName = i18n( "Windows Live Messenger compatible" );
        }
      }
    }
  }

  // If the base client name has changed, the full name has probably changed too
  if( newClientName != d->clientName_ )
  {
    d->clientName_ = newClientName;
    d->clientFullName_.clear();
  }

#ifdef KMESSDEBUG_CONTACTBASE
  if( isOnline() )
  {
    QStringList capsList;
    if( caps.caps() & KMess::ClientCapabilities::CF_ONLINE_VIA_MOBILE )
       capsList << "online-via-mobile"; // 0x00000001
    if( caps.caps() & KMess::ClientCapabilities::CF_ONLINE_VIA_MSN_EXPLORER )
       capsList << "online-via-msn-explorer"; // 0x00000002
    if( caps.caps() & KMess::ClientCapabilities::CF_SUPPORTS_GIF_INK )
       capsList << "supports-gif-ink"; // 0x00000004
    if( caps.caps() & KMess::ClientCapabilities::CF_SUPPORTS_ISF_INK )
       capsList << "supports-isf-ink"; // 0x00000008
    if( caps.caps() & KMess::ClientCapabilities::CF_WEBCAM_DETECTED )
       capsList << "webcam-detected"; // 0x00000010
    if( caps.caps() & KMess::ClientCapabilities::CF_SUPPORTS_MESSAGE_CHUNKING )
       capsList << "supports-message-chunking"; // 0x00000020
    if( caps.caps() & KMess::ClientCapabilities::CF_IS_MOBILE_ENABLED )
       capsList << "is-mobile-enabled"; // 0x00000040
    if( caps.caps() & KMess::ClientCapabilities::CF_IS_MSN_DIRECT_DEVICE_ENABLED )
       capsList << "is-msn-direct-device-enabled"; // 0x00000080
    if( caps.caps() & KMess::ClientCapabilities::CF_MOBILE_MESSAGING_DISABLED )
       capsList << "mobile-messaging-disabled"; // 0x00000100
    if( caps.caps() & KMess::ClientCapabilities::CF_ONLINE_VIA_WEBIM )
       capsList << "online-via-webim"; // 0x00000200
    if( caps.caps() & KMess::ClientCapabilities::CF_MOBILE_DEVICE )
       capsList << "mobile-device"; // 0x00000400
    if( caps.caps() & KMess::ClientCapabilities::CF_ONLINE_VIA_FEDERATED_GATEWAY )
       capsList << "online-via-federated-gateway"; // 0x00000800
    if( caps.caps() & KMess::ClientCapabilities::CF_HAS_MSN_SPACE )
       capsList << "has-msn-space"; // 0x00001000
    if( caps.caps() & KMess::ClientCapabilities::CF_IS_MEDIA_CENTER_EDITION_USER )
       capsList << "is-media-center-edition-user"; // 0x00002000
    if( caps.caps() & KMess::ClientCapabilities::CF_SUPPORTS_DIRECTIM )
       capsList << "supports-directim"; // 0x00004000
    if( caps.caps() & KMess::ClientCapabilities::CF_SUPPORTS_WINKS )
       capsList << "supports-winks"; // 0x00008000
    if( caps.caps() & KMess::ClientCapabilities::CF_SUPPORTS_SHARED_SEARCH )
       capsList << "supports-shared-search"; // 0x00010000
    if( caps.caps() & KMess::ClientCapabilities::CF_IS_BOT )
       capsList << "is-bot"; // 0x00020000
    if( caps.caps() & KMess::ClientCapabilities::CF_SUPPORTS_VOICEIM )
       capsList << "supports-voiceim"; // 0x00040000
    if( caps.caps() & KMess::ClientCapabilities::CF_P2P_SUPPORTS_SECURECHANNEL )
       capsList << "p2p-supports-securechannel"; // 0x00080000
    if( caps.caps() & KMess::ClientCapabilities::CF_SUPPORTS_SIP_INVITE )
       capsList << "supports-sip-invite"; // 0x00100000
    if( caps.caps() & KMess::ClientCapabilities::CF_SUPPORTS_MULTIPARTY_MEDIA )
       capsList << "supports-multiparty-media"; // 0x00200000
    if( caps.caps() & KMess::ClientCapabilities::CF_SUPPORTS_SHARED_DRIVE )
       capsList << "supports-shared-drive"; // 0x00400000
    if( caps.caps() & KMess::ClientCapabilities::CF_SUPPORTS_PAGE_MODE_MESSAGING )
       capsList << "supports-page-mode-messaging"; // 0x00800000
    if( caps.caps() & KMess::ClientCapabilities::CF_HAS_ONECARE )
       capsList << "has-onecare"; // 0x01000000
    if( caps.caps() & KMess::ClientCapabilities::CF_P2P_SUPPORTS_TURN )
       capsList << "p2p-supports-turn"; // 0x02000000
    if( caps.caps() & KMess::ClientCapabilities::CF_P2P_SUPPORTS_BOOTSTRAPPING_VIA_UUN )
       capsList << "p2p-supports-bootstrapping-via-uun"; // 0x04000000
    if( caps.caps() & KMess::ClientCapabilities::CF_USING_ALIAS )
       capsList << "using-alias"; // 0x08000000

    kmDebug() << "Contact" << getHandle() << "uses client:" << d->clientName_ << "msn version:" << msnVersion.toString();
    kmDebug() << "Contact capabilities:" << capabilities << "which is:" << capsList;
  }
#endif
}



// Return a user readable client name string.
const QString & Contact::getClientName( bool getFullNameIfAvailable ) const
{
  if( getFullNameIfAvailable && ! d->clientFullName_.isEmpty() )
  {
    return d->clientFullName_;
  }

  return d->clientName_;
}



// Return the third party client name field
const QString & Contact::getClientFullName() const
{
  return d->clientFullName_;
}



// Return the contact display picture path
const QString Contact::getContactPicturePath() const
{
  // Check before if there is some alternative contact's picture
  QString picturePath( d->extension_->getContactAlternativePicturePath() );

  if( ! picturePath.isEmpty() )
  {
    return picturePath;
  }

  // Check for contact picture ( sended by msn object )
  picturePath = d->extension_->getContactPicturePath();

  if( ! picturePath.isEmpty() )
  {
    return picturePath;
  }

  // else return the default contact picture path
  return getContactDefaultPicturePath();
}



// Return the default contact picture path
QString Contact::getContactDefaultPicturePath() const
{
  KStandardDirs *dirs   = KGlobal::dirs();
  return dirs->findResource( "data", "kmess/pics/unknown.png" );
}



// Return the list of custom emoticons to ignore.
const QStringList &Contact::getEmoticonBlackList() const
{
  return d->emoticonBlackList_;
}



// Return the custom emoticon code for an msn object
QString Contact::getEmoticonCode(const QString &msnObjectDataHash) const
{
  return d->pendingEmoticons_[ msnObjectDataHash ];
}



// Return the custom emoticon search pattern.
const QRegExp & Contact::getEmoticonPattern() const
{
  return d->emoticonRegExp_;
}



// Return the custom emoticon replacements.
const QHash<QString,QString>& Contact::getEmoticonReplacements() const
{
  return d->customEmoticons_;
}



// Return the custom emoticon hashes.
const QHash<QString,QString>& Contact::getEmoticonHashes() const
{
  return d->customHashes_;
}



// Return the ContactExtension object
ContactExtension *Contact::getExtension() const
{
  return d->extension_;
}



// Return the contact's friendly name
const QString& Contact::getFriendlyName( FormattingMode mode ) const
{
  if ( d->extension_->getUseAlternativeName() )
  {
    return d->extension_->getAlternativeName( mode );
  }
  else
  {
    return d->friendlyName_.getString( mode );
  }

}



// Return the contact's handle
const QString& Contact::getHandle() const
{
  return d->msnContact_->handle();
}



// Return the contact's previous status
KMess::MsnStatus Contact::getLastStatus() const
{
  return d->lastStatus_;
}



const QString& Contact::getPersonalMessage( FormattingMode mode ) const
{
  return d->personalMessage_.getString( mode );
}



// return the scaled display picture for this contact.
// this is used for display in the contact list.
QPixmap & Contact::getScaledDisplayPicture()
{
  int dpSize = Account::connectedAccount->getSettingInt( "ContactListPictureSize" );

  if ( dpSize == d->scaledDPSize_ && ! d->scaledDisplayPicture_.isNull() )
  {
    return d->scaledDisplayPicture_;
  }

  // here either the scaling size has changed or we have no cache.
  d->scaledDisplayPicture_.load( getContactPicturePath() );
  d->scaledDisplayPicture_ = d->scaledDisplayPicture_.scaled( dpSize, dpSize, Qt::KeepAspectRatio, Qt::SmoothTransformation );

  d->scaledDPSize_ = dpSize;

  return d->scaledDisplayPicture_;
}



// return the Msn contact.
const KMess::MsnContact *Contact::msnContact() const
{
  return d->msnContact_;
}



// Return the search pattern for pending custom emoticons.
const QRegExp & Contact::getPendingEmoticonPattern() const
{
  return d->pendingRegExp_;
}



// Return the contact's true friendly name, regardless of extension
const QString Contact::getTrueFriendlyName( FormattingMode mode ) const
{
  return d->friendlyName_.getString( mode );
}



// Regenerate the pending emoticon regexp, because it was changed
void Contact::regeneratePendingEmoticonPattern()
{
  QString pattern;
  foreach( const QString& shortcut, d->pendingEmoticons_ )
  {
    // Escape the code to insert it safely into a regular expression
    QString escapedCode( QRegExp::escape( shortcut ) );
    if( pattern.isEmpty() )
    {
      pattern = escapedCode;
    }
    else
    {
      pattern.append( "|" + escapedCode );
    }
  }

  d->pendingRegExp_ = QRegExp( pattern );

#ifdef KMESSTEST
  KMESS_ASSERT( d->pendingRegExp_.isValid() );
#endif
}



// Return the list of switchboard connections
const QList<const KMess::MsnSwitchboardConnection*> Contact::getSwitchboardConnections() const
{
  return d->chatSessions_;
}



// Add or remove a custom emoticon from the black list
bool Contact::manageEmoticonBlackList( bool add, const QString &emoticonCode )
{
  // Add the emoticon
  if( add )
  {
    // Don't duplicate emoticons in the list
    if( d->emoticonBlackList_.contains( emoticonCode ) )
    {
      return false;
    }

    d->emoticonBlackList_.append( emoticonCode );
    return true;
  }
  // Remove the emoticon
  else
  {
    return ( d->emoticonBlackList_.removeAll( emoticonCode ) > 0 );
  }
}



/**
 * Load properties for this contact. Also load extension properties too.
 */
void Contact::readProperties( const KConfigGroup &config )
{
  d->readProperties( config );
}




// Unregister the contact as participant, contact has left the chat.
void Contact::removeSwitchboardConnection( const KMess::MsnSwitchboardConnection *connection, bool userInitiated )
{
#ifdef KMESSDEBUG_CONTACTBASE
  kmDebug() << "removing connection, userInitiated=" << userInitiated;
#endif

  bool found = d->chatSessions_.removeAll( connection ) > 0;
  if( ! found )
  {
    kmWarning() << "chat session not found.";
  }

  if( d->chatSessions_.isEmpty() )
  {
#ifdef KMESSDEBUG_CONTACTBASE
    kmDebug() << getHandle() << " is no longer in any other chat conversation, cleaning up.";
#endif

    // This will be used by MsnSession to clean up temporary InvitedContact objects.
    // TODO: this should be in the library surely?
    //emit leftAllChats( this );
  }
}




/**
 * Save contact properties to config. Also save extension properties.
 */
void Contact::saveProperties( KConfigGroup &config )
{
  d->saveProperties( config );
}




// Save the simple name of the client used by the contact
void Contact::setClientName( const QString &clientName )
{
  d->clientName_ = clientName;
}



// Set the full name of the client used by the contact
void Contact::setClientFullName( const QString& clientFullName )
{
  d->clientFullName_ = clientFullName;

  // Make the name more readable
  d->clientFullName_ = d->clientFullName_.replace( '/', ' ' );

  // 'Purple 2.3.1' means the user actually has Pidgin or Adium (because they use libpurple).
  if( d->clientFullName_.startsWith( "Purple " ) )
  {
    d->clientFullName_ = "Pidgin/Adium (" + d->clientFullName_ + ")";
  }
}
