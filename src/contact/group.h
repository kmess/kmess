/***************************************************************************
                          kmessgroup.h  -  description
                             -------------------
    begin                : Tue 03 Nov 2009
    copyright            : (C) 2009 by Adam Goossens
    email                : adam@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef GROUP_H
#define GROUP_H

namespace KMess
{
  class MsnGroup;
}

#include "kmessglobal.h"
#include "groupprivate.h"

/**
 * Contains all KMess-specific data about an MsnGroup instance.
 */
class Group
{
  public:
    Group();
    Group( const QString &id );
    Group( const KMess::MsnGroup *msnGroup );
    ~Group();

    // Return the sort value
    int                  sortPosition() const;
    // Return the gropu ID
    const QString        id() const;
    // Return true if this group is expanded.
    bool                 isExpanded() const;
    // Return true if this is a special group
    bool                 isSpecialGroup() const;
    // Return true if this is a valid group (points to a valid d-pointer)
    bool                 isValid() const;
    // Return the name of this group (will automatically return a name for special groups)
    const QString        getName() const;
    // Return the sort position of the group
    int                  getSortPosition() const;
    // Return the MsnGroup
    const KMess::MsnGroup *msnGroup() const;
    // Set the group name
    void                 setName( const QString &name );
    // Set sort position
    void                 setSortPosition( int position );
    // Set expanded state
    void                 setExpanded( bool expanded );
    // Save this group.
    void                 saveProperties( KConfigGroup &config );
    // Load this groups properties.
    void                 readProperties( KConfigGroup &config );
  // operator overloads
  public:
    Group        *operator->() const { return const_cast<Group*>(this); }
    friend bool   operator==( const Group &c1, const Group &c2 );
    friend bool   operator!=( const Group &c1, const Group &c2);
    Group        &operator=( const Group &other )
    {
      d = other.d;
      return *this;
    }

    Group        &operator=( const KMess::MsnGroup *other )
    {
      d = contactsManager->groupData( other );
      return *this;
    }

    // by doing this we can pass a Contact instance to QObject::(dis)connect()
    // and it will transparently attach the signals to the underlying
    // private data instance.
    operator KMess::MsnGroup *()
    {
      return const_cast<KMess::MsnGroup *>( d->msnGroup_ );
    }

  private:
    GroupPrivate *d;
};

#endif
