/***************************************************************************
                          groupdata.cpp  -  description
                             -------------------
    begin                : Tue 03 Nov 2009
    copyright            : (C) 2009 by Adam Goossens
    email                : adam@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "groupprivate.h"
#include "kmessdebug.h"

#include <KConfigGroup>

#include <KMess/MsnGroup>

GroupPrivate::GroupPrivate( const KMess::MsnGroup *group )
  : msnGroup_ ( group )
{
  // if this is a special group we show at the very bottom the 
  // contact list.
  if ( group->isSpecialGroup() )
  {
    sortPosition_ = 9999;
  }
}



/**
 * Read properties from the given KConfigGRoup.
 */
void GroupPrivate::readProperties( const KConfigGroup &config )
{
  // Get the configuration group where this group's data is stored
  const KConfigGroup& groupConfig( config.group( msnGroup_->id() ) );

  // Obtain the sort positiong of this group. Special groups have no sorting.
  if ( ! msnGroup_->isSpecialGroup() )
  {
    sortPosition_ = groupConfig.readEntry( "sortPosition", sortPosition_ );
  }

  kmDebug() << "Reading private data for group " << msnGroup_->name();

  // Is the group expanded? that is, can the user see the group's contacts?
  isExpanded_ = groupConfig.readEntry( "isExpanded", true );
}



/**
 * Save contact properties to config. Also save extension properties.
 */
void GroupPrivate::saveProperties( KConfigGroup &config )
{
  // Get the configuration group where this group's data is stored
  KConfigGroup groupConfig( config.group( msnGroup_->id() ) );

  // Save the sort positiong of this group. Special groups have no sorting.
  if( ! msnGroup_->isSpecialGroup() )
  {
    groupConfig.writeEntry( "sortPosition", sortPosition_ );
  }

  groupConfig.writeEntry( "isExpanded", isExpanded_ );
}
