/***************************************************************************
                          contactprivate.cpp  -  description
                             -------------------
    begin                : Sat 07 Nov 09
    copyright            : (C) 2009 by Adam Goossens
    email                : adam@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "contactprivate.h"
#include "contactextension.h"
#include "kmessdebug.h"
#include "kmessglobal.h"

#include <KMess/MsnContact>

/**
 * Creates a new ContactPrivate instance bound to the given MsnContact.
 * @param contact The MsnContact instance associated with this object.
 */
ContactPrivate::ContactPrivate( const KMess::MsnContact *contact )
  : msnContact_ ( contact )
  , lastStatus_ (KMess::OfflineStatus)
  , scaledDPSize_ (-1)
{
  extension_ = new ContactExtension( contact );
  friendlyName_.setString( contact->displayName() ) ;

  // forward a relevant signal from the extension.
  CHAIN_SIGNAL( extension_, this, displayPictureChanged() );
  CHAIN_SIGNAL( extension_, this, displayNameChanged() );
  
  // now forward all the signals from the MsnContact instance.
  // makes it easier for the GUI to connect to signals - it can connect to these
  CHAIN_SIGNAL( msnContact_, this, groupsChanged() );
  CHAIN_SIGNAL( msnContact_, this, msnObjectChanged() );
  CHAIN_SIGNAL( msnContact_, this, statusChanged() );
  CHAIN_SIGNAL( msnContact_, this, personalMessageChanged() );
  CHAIN_SIGNAL( msnContact_, this, contactOffline() );
  CHAIN_SIGNAL( msnContact_, this, contactOnline() );

  // we need this so we can reparse the MSN+ formatting
  // we'll also emit changedFriendlyName here.
  connect( contact, SIGNAL( displayNameChanged() ),
           this,      SLOT( slotChangedFriendlyName() ) );

  // Whenever the contacts change status, re-check their client name
  connect( contact, SIGNAL(     statusChanged() ),
           this,      SLOT( slotChangedStatus() ) );
}


/**
 * Deletes this instance and also deletes the ContactExtension object.
 */
ContactPrivate::~ContactPrivate()
{
  delete extension_;
}



/**
 * Read properties from the given KConfigGRoup.
 * @param config The KConfigGroup instance to read the data from.
 */
void ContactPrivate::readProperties( const KConfigGroup &config )
{
  // don't bother reading for unknown contacts - there shouldn't be anything for them.
  if ( msnContact_->isUnknown() ) return;

  // Get the configuration group where this contact's data is stored
  KConfigGroup contactConfig = config.group( msnContact_->handle() );

  // Restore cached state of this class
  //capabilities_             = contactConfig.readEntry( "capabilities", 0 );
  clientName_               = contactConfig.readEntry( "clientName", QString() );
  clientFullName_           = contactConfig.readEntry( "clientFullName", QString() );
  personalMessage_.setString( contactConfig.readEntry( "lastPersonalMessage", "" ) );
  emoticonBlackList_        = contactConfig.readEntry( "emoticonBlackList", QStringList() );

  // TODO: restore more state variables here, to implement contact list caching.

  kmDebug() << "Reading private data for contact " << msnContact_->handle();

  // Restore extension
  extension_->readProperties( contactConfig );
}



/**
 * Save contact properties to config. Also save extension properties.
 */
void ContactPrivate::saveProperties( KConfigGroup &config )
{
  // don't save unknown contacts.
  if ( msnContact_->isUnknown() ) return;

  // Get the configuration group where this contact's data is stored
  KConfigGroup contactConfig = config.group( msnContact_->handle() );

  // Save state of this class
  // TODO: check "capabilities" and "personal status"
  contactConfig.writeEntry( "capabilities",         msnContact_->capabilities().toString() );
  contactConfig.writeEntry( "clientName",           clientName_ );
  contactConfig.writeEntry( "clientFullName",       clientFullName_ );
  contactConfig.writeEntry( "lastPersonalMessage",  msnContact_->personalMessage() );
  contactConfig.writeEntry( "emoticonBlackList",    emoticonBlackList_ );

  // store extension
  extension_->saveProperties( contactConfig );
}



/**
 * When the friendly name for our base MsnContact changes, we need to ask the
 * rich text parser to reparse it.
 */
void ContactPrivate::slotChangedFriendlyName()
{
  KMess::MsnContact *contact = qobject_cast<KMess::MsnContact*>( sender() );

  if ( contact->displayName() != friendlyName_.getOriginal() )
  {
    friendlyName_.setString( contact->displayName() );
    emit displayNameChanged();
  }
}



/**
 * Re-checks the contact's client name when the contact changes status.
 *
 * @param contact The contact who changed status
 */
void ContactPrivate::slotChangedStatus()
{
  Contact kmessContact( sender() );

  kmessContact.detectClientName();
}
