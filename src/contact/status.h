/***************************************************************************
                          status.h
                             -------------------
    begin                : Saturday March 8th 2008
    copyright            : (C) 2008 by Richard Conway
    email                : richardconway1984 hotmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef STATUS_H
#define STATUS_H

#include <QPixmap>
#include <KMess/NetworkGlobals>

#undef Status

// Forward declarations
class KMenu;



/**
 * Contact status management class
 *
 * This class contains everything we need to manage the MSN states.
 * The main enumeration which contains the MSN states can be used
 * along the conversion methods, which translate the enum values to
 * string (the three letter character codes like NLN for Online) and
 * to localized strings (like "Out to lunch"). There's also a method
 * to translate the status values to the relative status icon.
 *
 * @author Valerio Pilo <valerio@kmess.org>
 * @author Richard Conway <richardconway1984 at hotmail.com>
 * @ingroup Contact
 */
class Status
{
  public: // Public static methods
    ~Status();

    // Returns an icon for a status
    static QPixmap           getIcon( const  KMess::MsnStatus status, const KMess::Flags flags = KMess::FlagNone );
    // Returns the icon name for the specified status
    static QString           getIconName( const KMess::MsnStatus status );
    // Returns a three-letter code (like BSY) for a status
    static QString           getCode( const KMess::MsnStatus status );
    // Returns the localized name for a status
    static QString           getName( const KMess::MsnStatus status );
    // Create and return a menu with all the states
    static KMenu            *getStatusMenu();
    // Convert a three-letter code to a status
    static KMess::MsnStatus  codeToStatus( const QString &status );


  private: // Private static properties

    // The status menu
    static KMenu   *statusMenu_;
};


#endif
