/***************************************************************************
                          contactdata.h  -  description
                             -------------------
    begin                : Tue 03 Nov 2009
    copyright            : (C) 2009 by Adam Goossens
    email                : adam@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CONTACTDATA_H
#define CONTACTDATA_H

#include <KConfigGroup>

#include <KMess/MsnContact>

#include <QPixmap>
#include <QObject>

namespace KMess
{
  class MsnSwitchboardConnection;
}

class ContactExtension;


#include "../utils/richtextparser.h"

/**
 * Stores the shared private data for the Contact object.
 *
 * This is accessible via the dptr in the Contact class.
 */
class ContactPrivate : public QObject
{
  Q_OBJECT

  public:
    ContactPrivate( const KMess::MsnContact *contact = 0 );
    ~ContactPrivate();

    void    readProperties( const KConfigGroup &config );
    void    saveProperties( KConfigGroup &config );

  private slots:
    // handles the reparsing of the friendly name by the rich text parser
    void    slotChangedFriendlyName();
    // Re-checks the contact's client name
    void    slotChangedStatus();

  public: // naughty!
    // The MsnContact associated with this contact.
    const KMess::MsnContact   *msnContact_;
   // The name of the client application
    QString              clientName_;
    // The third party client name field
    QString              clientFullName_;
    // The custom emoticons for the contact.
    QHash<QString,QString> customEmoticons_;
    // The custom emoticon mappings from shortcut to hash.
    QHash<QString,QString> customHashes_;
    // The list of custom emoticons to ignore for this contact
    QStringList          emoticonBlackList_;
    // The emoticon regexp
    QRegExp              emoticonRegExp_;
    // The contact's friendly name
    FormattedString      friendlyName_;
    // The contact's last status
    KMess::MsnStatus        lastStatus_;
    // The pending custom emoticons for the contact.
    QHash<QString,QString> pendingEmoticons_;
    // The regexp for pending custom emoticons
    QRegExp              pendingRegExp_;
    // The personal message
    FormattedString      personalMessage_;
    // the contact's scaled display picture.
    QPixmap              scaledDisplayPicture_;
    // the scaled size of the dp (used for caching)
    int                  scaledDPSize_;
    // the contactextension object.
    ContactExtension    *extension_;
    // The list of chat sessions the contact participates in.
    QList<const KMess::MsnSwitchboardConnection*> chatSessions_;

  signals:
    // a lot of these are simply forwards from the MsnContact instance
    // that is beneath us.
    // Signal that the friendly name of this contact has changed
    void                 displayNameChanged();
    // Signal that the contact may have moved to a different group
    void                 groupsChanged();
    // Signal that the contact's msnobject has changed
    void                 msnObjectChanged();
    // Signal that the contact's status has changed.
    void                 statusChanged();
    // Signal that the display picture has changed (either we set a custom one or they changed theirs)
    void                 displayPictureChanged();
    // Signal that the contact may have changed it's personal message
    void                 personalMessageChanged();
    // Signal if the contact goes offline
    void                 contactOffline();
    // Signal if the contact goes online
    void                 contactOnline();

};

#endif
