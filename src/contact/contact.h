/***************************************************************************
                          contact.h  -  description
                             -------------------
    begin                : Sun Jan 5 2003
    copyright            : (C) 2003 by Mike K. Bennett
    email                : mkb137b@hotmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CONTACT_H
#define CONTACT_H

#include <QStringList>
#include <QObject>

#include "contactprivate.h"

#include "kmessglobal.h"

#include <KMess/MsnContact>

// Forward declarations
class ContactExtension;

class KConfigGroup;

namespace KMess
{
  class MsnSwitchboardConnection;
  class MsnObject;
}

/**
 * Contains all KMess-specific information about a MsnContact instance.
 */
class Contact
{

  public: // Public constants

  // operator overloads
  public:


  public: // Public methods
    // The constructor
                        Contact( const Contact &other );
                        Contact( const QString &handle );
                        explicit Contact( const QObject *msnContact );
    // The destructor
                        ~Contact();
    // Add a custom emoticon definition
    void                 addEmoticonDefinition(const QString &emoticonCode, const QString &msnObjectDataHash);
    // Add a custom emoticon definition
    void                 addEmoticonFile(const QString &msnObjectDataHash, const QString &filename);
    // Register the contact as participant in the chat session
    void                 addSwitchboardConnection( const KMess::MsnSwitchboardConnection *connection );
    // Set the application name based on the capabilities
    void                 detectClientName( const QString capabilities = QString() );
    // Return the name of the client used by the contact
    const QString &      getClientName( bool getFullNameIfAvailable = true ) const;
    // Return the full name of the client used by the contact
    const QString &      getClientFullName() const;
    // Return the default contact picture path
    QString              getContactDefaultPicturePath() const;
    // Return the path to the contact's picture
    const QString        getContactPicturePath() const;
    // Return the list of custom emoticons to ignore.
    const QStringList   &getEmoticonBlackList() const;
    // Return the custom emoticon code for an msn object
    QString              getEmoticonCode(const QString &msnObjectDataHash) const;
    // Return the custom emoticon search pattern.
    const QRegExp &      getEmoticonPattern() const;
    // Return the custom emoticon hashes.
    const QHash<QString,QString> & getEmoticonHashes() const;
    // Return the custom emoticon replacements.
    const QHash<QString,QString> & getEmoticonReplacements() const;
    // Return the contact's friendly name
    const QString&       getFriendlyName( FormattingMode mode = STRING_CLEANED ) const;
    // Return the contact's handle
    const QString&       getHandle() const;
    // Return the contacts' personal message
    const QString&       getPersonalMessage( FormattingMode mode = STRING_CLEANED) const;
    // Return true if the Contact is valid (ie, points to a valid d-pointer).
    bool                 isValid() const;
    // Get the parent MsnContact
    const KMess::MsnContact   *msnContact() const;
    // Return the search pattern for pending custom emoticons.
    const QRegExp &      getPendingEmoticonPattern() const;
    // Return the list of switchboard connections
    const QList<const KMess::MsnSwitchboardConnection*> getSwitchboardConnections() const;
    // Add or remove a custom emoticon from the black list
    bool                 manageEmoticonBlackList( bool add, const QString &emoticonCode );
    // Unregister the contact as participant, contact has left the chat.
    void                 removeSwitchboardConnection( const KMess::MsnSwitchboardConnection *connection, bool userInitiated = false );
    // Set the full name of the client used by the contact
    void                 setClientFullName( const QString &clientFullName );
    // Return the contact's display picture, scaled to the right size for the contact list.
    QPixmap &            getScaledDisplayPicture();
    // Return a pointer to the extension class
    ContactExtension    *getExtension() const;
    // Return the contact's information
    const QVariant       getInformation( const QString &information ) const;
    // Return the contact's previous status
    KMess::MsnStatus     getLastStatus() const;
    // Return the contact's true friendly name, regardless of extension
    const QString        getTrueFriendlyName( FormattingMode mode = STRING_CLEANED ) const;
    // Load cached state for a contact
    void                 readProperties( const KConfigGroup &config );
    // Set the name of ths client
    void                 setClientName( const QString &clientName );
    // Save cached state for for a contact
    void                 saveProperties( KConfigGroup &config );
    // Set the extra information retrieved by soap response
    void                 setInformations( const QHash<QString, QVariant> &informations );

  public: // Public inline methods
    /**
    * Return whether the contact has the user in his/her contact list.
    * @return bool
    */
    inline bool        hasUser() const { return d->msnContact_->hasUser(); }
    /**
    * Return whether the contact was allowed to see the user's presence.
    * @return bool
    */
    inline bool        isAllowed() const { return d->msnContact_->isAllowed(); }
    /**
    * Return whether the contact was blocked.
    * @return bool
    */
    inline bool        isBlocked() const { return d->msnContact_->isBlocked(); }
    /**
    * Return whether the contact is a friend.
    * @return bool
    */
    inline bool        isFriend() const { return d->msnContact_->isFriend(); }
    /**
    * Return whether the contact has not accepted our friendship request.
    * @return bool
    */
    inline bool        isPending() const { return d->msnContact_->isPending(); }
    /**
    * Return whether the contact is currently offline.
    * @return bool
    */
    bool               isOffline() const { return d->msnContact_->isOffline(); }
    /**
    * Return whether the contact is currently online.
    * @return bool
    */
    bool               isOnline() const { return d->msnContact_->isOnline(); }

  // operator overloads
  public:
    Contact        *operator->() const { return const_cast<Contact*>(this); }
    friend bool     operator==( const Contact &c1, const Contact &c2 );
    friend bool     operator!=( const Contact &c1, const Contact &c2);

    /**
     * Reseat this Contact so that it has the same dptr as other.
     */
    Contact        &operator=( const Contact &other )
    {
      d = other.d;
      return *this;
    }

     /**
     * Constructs a new Contact that is backed by the given MsnContact.
     */
    Contact        &operator=( const KMess::MsnContact *other )
    {
      d = contactsManager->contactData( other );
      return *this;
    }

    /**
     * Returns the MsnContact * that this class represents.
     */
    operator KMess::MsnContact *()
    {
      return const_cast<KMess::MsnContact *>( d->msnContact_ );
    }

    /**
     * Returns the dptr for this instance as a QObject *. This allows
     * us to quickly and easily attach to its signals. So the following is fine:
     *
     * connect( guiContact, SIGNAL( changedFriendlyName( MsnContact * ) ), this, SLOT( doFoo() ) );
     */
    operator QObject *()
    {
      return (QObject *)( d );
    }

  private:  // private methods
    // Regenerate the pending emoticon regexp, because it was changed
    void                 regeneratePendingEmoticonPattern();

  private: // Private attributes
    ContactPrivate *d;

};

/**
 * Compare two Contact instances. Two Contacts are equal if they point to the same shared data.
 *
 * @param c1 First Contact instance
 * @param c2 Second contact instance
 * @return True if the two contacts point to the same shared data, false otherwise.
 */
bool inline operator==( const Contact &c1, const Contact &c2 )
{
  return c1.d == c2.d;
}

/**
 * Compare two Contact instances. Two Contacts are not equal if they point different sets of shared data.
 *
 * @param c1 First Contact instance
 * @param c2 Second contact instance
 * @return True if the two contacts point to different shared data, false otherwise.
 */
bool inline operator!= ( const Contact &c1, const Contact &c2 )
{
  return ! ( c1 == c2 );
}

#endif
