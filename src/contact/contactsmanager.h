/***************************************************************************
                          contactsmanager.h  -  description
                             -------------------
    begin                : Tue 03 Nov 2009
    copyright            : (C) 2009 by Adam Goossens
    email                : adam@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CONTACTSMANAGER_H
#define CONTACTSMANAGER_H

#include <KConfigGroup>

namespace KMess
{
  class MsnContact;
  class MsnGroup;
}

class Account;
class ContactPrivate;
class GroupPrivate;

#include <QHash>

/**
 * Holds copies of the data for all ContactPrivate and GroupPrivate instances.
 *
 * Through the use of this class we can quickly and cheaply copy around
 * ContactPrivate and GroupPrivate instances. It is also made possible to 
 * quickly retrieve the KMess-specific data associated with an MsnContact:
 *
 * KMess::MsnContact msnContact = ..... ;
 * ContactPrivate guiContact( msnContact );
 *
 * Because the data is shared, guiContact can be copied freely without incurring
 * deep copy costs.
 *
 * Copy on write is NOT SUPPORTED. If you change the data via one Contact, it
 * will be replicated to ALL Contact instances that reference the same shared data.
 */
class ContactsManager : public QObject
{

  Q_OBJECT

  public:
                         ContactsManager();
                        ~ContactsManager();

    void                 clear();

    ContactPrivate      *contactData( const KMess::MsnContact *msnContact );
    ContactPrivate      *contactData( const QString &handle );

    GroupPrivate        *groupData( const KMess::MsnGroup *msnGroup );
    GroupPrivate        *groupData( const QString &groupId );

    void                 saveContact( const KMess::MsnContact *contact );
    void                 saveGroup( const KMess::MsnGroup *group );

    void                 loadConfig( Account *account );

  public slots:
    void                 saveContactList();
    void                 saveProperties();

  private:
    /// Our private data associated with our personal MsnContact instance.
    ContactPrivate                     *selfPrivate_;
    QHash<QString, ContactPrivate*>   contactStore_;
    QHash<QString, GroupPrivate*>       groupStore_;
    KConfigGroup                        contactsConfigGroup_, groupsConfigGroup_;
};
#endif
