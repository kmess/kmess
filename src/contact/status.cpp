/***************************************************************************
                          status.cpp
                             -------------------
    begin                : Sunday March 9th 2008
    copyright            : (C) 2008 by Richard Conway
    email                : richardconway1984 hotmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "status.h"

#include "../kmessdebug.h"

#include <KAction>
#include <KIconEffect>
#include <KIconLoader>
#include <KLocale>
#include <KMenu>

#include <KMess/NetworkGlobals>

// Initialize the status menu
KMenu* Status::statusMenu_( 0 );

Status::~Status()
{
  delete statusMenu_;
}



/**
 * Returns an icon for the specified status
 *
 * Additionally, you can specify a flag you want to overlay on the status icon.
 *
 * @param status KMess::MsnStatus for which to create the picture
 * @param flags  Optional flags to add overlays, @see KMess::Flags enum
 * @return The status icon
 */
QPixmap Status::getIcon( const KMess::MsnStatus status, const KMess::Flags flags )
{
  // initially set the overlay icon to an empty string as we do not know
  // at this point if we want one.
  QString iconOverlay;

  // Set the base icon depending on the status
  QString iconBase = getIconName( status );

  // Choose the overlay for the states, if they need one
  switch( status )
  {
    case KMess::BrbStatus:   iconOverlay = "contact_away_overlay";      break;
    case KMess::AwayStatus:  iconOverlay = "contact_invisible_overlay"; break;
    case KMess::PhoneStatus: iconOverlay = "contact_phone_overlay";     break;
    case KMess::LunchStatus: iconOverlay = "contact_food_overlay";      break;
    default:
      break;
  }

  // Choose the picture's overlay, if any.
  // FIXME: there is no 'blocked' image or overlay image in the oxygen icon set
  // One may be released in time, for now use the busy overlay as it looks the part
  // in the CURRENT oxygen theme! this may change
  switch( flags )
  {
    case KMess::FlagNone:          break;
    case KMess::FlagBlocked:       iconOverlay = "contact_busy_overlay"; break;
    case KMess::FlagWebMessenger:  break;
    case KMess::FlagMobile:        break;
  }

  // Load the icon
  QPixmap icon = KIconLoader::global()->loadIcon( iconBase, KIconLoader::Small );

  // Add the overlay if needed
  if( ! iconOverlay.isEmpty() )
  {
    // Add the overlay image to our icon
    QPixmap overlayIcon  = KIconLoader::global()->loadIcon( iconOverlay, KIconLoader::Small );
    QImage  iconImage    = icon.toImage();
    QImage  overlayImage = overlayIcon.toImage();

    KIconEffect::overlay( iconImage, overlayImage );
    icon = QPixmap::fromImage( iconImage );
  }

  return icon;
}



/**
 * Returns the icon name for the specified status
 *
 * Only the base icon name can be returned.
 *
 * @param status        KMess::MsnStatus for which to create the picture
 * @return The status icon
 */
QString Status::getIconName( const KMess::MsnStatus status )
{
  // Set the base icon depending on the status
  switch( status )
  {
    case KMess::OnlineStatus:        return "user-online";
    case KMess::BusyStatus:          return "user-busy";
    case KMess::BrbStatus:
#ifdef __GNUC__
    #warning TODO: away w/autoreply.
#endif
    //case KMess::AwayStatus_AUTOREPLY: 
    case KMess::AwayStatus:          return "user-away";
    case KMess::PhoneStatus:
    case KMess::LunchStatus:
    case KMess::IdleStatus:          return "user-away-extended";
    case KMess::InvisibleStatus:     return "user-invisible";
    case KMess::OfflineStatus:       return "user-offline";

    default:
      kmWarning() << "Invalid status" << status << "!";
  }

  return "user-online";
}



/**
 * Returns a three-letter code (like BSY for Busy) for a status
 *
 * @param status The status to convert
 * @return Three-letter status code
 */
QString Status::getCode( const KMess::MsnStatus status )
{
  // Just return the code
  switch( status )
  {
    case KMess::OnlineStatus:         return QString( "NLN" );
    case KMess::AwayStatus:           return QString( "AWY" );
    //case KMess::AwayStatus_AUTOREPLY: return QString( "AWY" );
    case KMess::BrbStatus:           return QString( "BRB" );
    case KMess::BusyStatus:           return QString( "BSY" );
    case KMess::InvisibleStatus:      return QString( "HDN" );
    case KMess::IdleStatus:           return QString( "IDL" );
    case KMess::OfflineStatus:        return QString( "FLN" );
    case KMess::PhoneStatus:          return QString( "PHN" );
    case KMess::LunchStatus:          return QString( "LUN" );
    default:
      kmWarning() << "Invalid status" << status << "!";
      return QString( "NLN" );
  }
}



/**
 * Returns the localized name for a status
 *
 * @param status The status to convert
 * @return Localized status name
 */
QString Status::getName( const KMess::MsnStatus status )
{
  // Localize and return the status name
  switch( status )
  {
    case KMess::OnlineStatus:         return i18n( "Online"               );
    case KMess::BusyStatus:           return i18n( "Busy"                 );
    case KMess::AwayStatus:           return i18n( "Away"                 );
#ifdef __GNUC__
    # warning away w/auto reply
#endif
    //case KMess::AwayStatus_AUTOREPLY: return i18n( "Away with Auto-Reply" );
    case KMess::IdleStatus:           return i18n( "Idle"                 );
    case KMess::BrbStatus:            return i18n( "Be Right Back"        );
    case KMess::PhoneStatus:          return i18n( "On the Phone"         );
    case KMess::LunchStatus:          return i18n( "Out to Lunch"         );
    case KMess::InvisibleStatus:      return i18n( "Invisible"            );
    case KMess::OfflineStatus:        return i18n( "Offline"              );

    default:
      kmWarning() << "Invalid status" << status << "!";
      return i18n( "Online" );
  }
}



/**
 * Create and return a menu with all the states
 *
 * It also has a Disconnect option (instead of Offline)
 * Be sure to actually attach the returned menu to some toolbar or the like,
 * or it won't be properly deleted.
 *
 * @return KMenu with a QAction for each available KMess::MsnStatus
 */
KMenu *Status::getStatusMenu()
{
  if( statusMenu_ != 0 )
  {
    return statusMenu_;
  }

  statusMenu_ = new KMenu();
  statusMenu_->addTitle( KIcon( "go-jump" ), i18n( "&My Status" ) );
  statusMenu_->setIcon( KIcon( "go-jump" ) );
  statusMenu_->setTitle( i18n( "&My Status" ) );
  KMess::MsnStatus status;
  KAction *action;

  // This loop assumes that a) STATES_NUMBER is the last item of the KMess::MsnStatus enum, and
  // b) KMess::OfflineStatus is the second last. Fun stuff happens if it is not the case.
  for( int index = 0; index < KMess::MSNSTATUS_NUMBER; ++index )
  {
    // Setting the action's data allows us to later retrieve the
    // KMess::MsnStatus from a triggered QAction
    status = (KMess::MsnStatus) index;
    action = new KAction( KIcon( getIcon( status ) ), getName( status ), statusMenu_ );
    action->setData( status );

    // The disconnection action is a bit special :)
    if( status == KMess::OfflineStatus )
    {
      // Divide the normal states from the disconnection action
      statusMenu_->addSeparator();

      // Change its name: "Offline" is misleading.
      action->setText( i18nc( "Menu action name", "Disconnect" ) );
    }

    statusMenu_->addAction( action );
  }

  /* TODO: After the Account/CurrentAccount/MsnSession rewrite, move the Status menu's signal connection
           from class KMessInterface::initialize() to here, with something like:
             connect( Status,        SIGNAL(    triggered(QAction*) ),
                     globalSession,   SLOT  ( changeStatus(QAction*) ) );
           To avoid adding such a stupid slot on CurrentAccount, it'd be better to add it
           somewhere else (here?) to get the KMess::MsnStatus out of the QAction.
  */

  return statusMenu_;
}



/**
 * Convert a three-letter code to a status
 *
 * @param status The status to convert
 * @return MSN KMess::MsnStatus enum value
 */
KMess::MsnStatus Status::codeToStatus( const QString &status )
{
       if( status == "AWY" ) return KMess::AwayStatus;
  else if( status == "BRB" ) return KMess::BrbStatus;
  else if( status == "BSY" ) return KMess::BusyStatus;
  else if( status == "FLN" ) return KMess::OfflineStatus;
  else if( status == "HDN" ) return KMess::InvisibleStatus;
  else if( status == "IDL" ) return KMess::IdleStatus;
  else if( status == "LUN" ) return KMess::LunchStatus;
  else if( status == "NLN" ) return KMess::OnlineStatus;
  else if( status == "PHN" ) return KMess::PhoneStatus;
  else
  {
    kmWarning() << "Invalid status" << status << "!";
    return KMess::OnlineStatus;
  }
}


