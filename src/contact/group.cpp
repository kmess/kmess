/***************************************************************************
                          group.cpp  -  description
                             -------------------
    begin                : Sun Jan 5 2003
    copyright            : (C) 2003 by Mike K. Bennett
                           (C) 2009 by Adam Goossens
    email                : mkb137b@hotmail.com
                           adam@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "group.h"
#include "groupprivate.h"

#include "kmessglobal.h"

#include <KMess/MsnGroup>
#include <KConfigGroup>

#include <KLocale>

Group::Group()
  : d ( 0 )
{
}



Group::Group( const QString &id ) 
  : d( contactsManager->groupData( id ) )
{
}


// The constructor
Group::Group( const KMess::MsnGroup *group )
  : d( contactsManager->groupData( group ) )
{

}



// The destructor
Group::~Group()
{
#ifdef KMESSDEBUG_GROUP
  kmDebug() << "DESTROYED. [group =" << d->name_ << ", id=" << d->id_ << "]";
#endif
}



/**
 * A group is valid if its d-pointer is non-null.
 */
bool Group::isValid() const
{
  return d != 0;
}




const QString Group::getName() const
{
  if ( ! isSpecialGroup() ) 
  {
    return d->msnGroup_->name();
  }
  else
  {
    // return a translated name for the special groups from the lib.
    if ( id() == KMess::SpecialGroups::OFFLINE )
    {
      return i18n( "Offline" );
    }
    else if ( id() == KMess::SpecialGroups::ONLINE )
    {
      return i18n( "Online" );
    }
    else if ( id() == KMess::SpecialGroups::INDIVIDUALS )
    {
      return i18n( "Individuals" );
    }
    else if ( id() == KMess::SpecialGroups::ALLOWED )
    {
      return i18n( "Allowed" );
    }
    else if ( id() == KMess::SpecialGroups::FAVORITES )
    {
      return i18n( "Favorites" );
    }
    else if ( id() == KMess::SpecialGroups::REMOVED )
    {
      return i18n( "Removed" );
    }
    else
    {
      // no idea what this one is.
      return d->msnGroup_->name();
    }
  }
}


/**
 * Return the sort position of the group.
 */
int Group::getSortPosition() const
{
  return d->sortPosition_;
}



/**
 * Return the KMess::MsnGroup instance associated with this Group.
 */
const KMess::MsnGroup *Group::msnGroup() const
{
  return d->msnGroup_;
}



/**
 * Return the sort position in the contact list.
 */
int Group::sortPosition() const
{
  return d->sortPosition_;
}



/**
 * Return the ID of this grop
 */
const QString Group::id() const
{
  return d->msnGroup_->id();
}



/**
 * Return true if this group is expanded, false otherwise.
 */
bool Group::isExpanded() const
{
  return d->isExpanded_;
}



/**
 * Return True if this is a special group (ONLINE, OFFLINE, ALLOWED, REMOVED, INDIVIDUALS)
 * or False otherwise.
 */
bool Group::isSpecialGroup() const
{
  return d->msnGroup_->isSpecialGroup();
}



/**
 * Read group properties from disk config.
 */
void Group::readProperties( KConfigGroup &config )
{
  d->readProperties( config );
}



/**
 * Save group properties to disk config.
 */
void Group::saveProperties( KConfigGroup &config )
{
  d->saveProperties( config );
}





/**
 * Change the sort position of this group.
 */
void Group::setSortPosition( int sortPosition )
{
  d->sortPosition_    = sortPosition;
}



/**
 * Change the expanded state of this group
 */
void Group::setExpanded(bool expanded)
{
  d->isExpanded_ = expanded;
}

