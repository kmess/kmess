/***************************************************************************
                          groupdata.h  -  description
                             -------------------
    begin                : Tue 03 Nov 2009
    copyright            : (C) 2009 by Adam Goossens
    email                : adam@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef GROUPPRIVATE_H
#define GROUPPRIVATE_H

#include <KConfigGroup>

namespace KMess
{
  class MsnGroup;
};

/**
 * Holds all of the kmess-specific data for a group.
 *
 * Only created once; managed centrally by ContactsManager.
 *
 * A user can then do this: 
 *
 * GroupData guiGroup( someMsnGroupPointer );
 *
 * And the data will be shared as a d-pointer. No large copies.
 */
class GroupPrivate
{
  public:
    GroupPrivate( const KMess::MsnGroup *group = 0 );

    void    readProperties( const KConfigGroup &config );
    void    saveProperties( KConfigGroup &config );

  public: // naughty!
    // The MsnGroup associated with this Group.
    const KMess::MsnGroup *msnGroup_;
    // The name of the group
    QString              name_;
    // The group's expanded state
    bool                 isExpanded_;
    // The group's sort value
    int                  sortPosition_;
    
};

#endif
