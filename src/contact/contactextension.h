/***************************************************************************
                          contactextension.h  -  description
                             -------------------
    begin                : Thu 12 Dec 2002
    copyright            : (C) 2003 by Michael Curtis
    email                : mdcurtis@users.sourceforge.net
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CONTACTEXTENSION_H
#define CONTACTEXTENSION_H

#include "../utils/richtextparser.h"

#include "contact.h"

#include <QObject>
#include <QDateTime>


// forward declarations
namespace KMess
{
  class MsnContact;
}

class Contact;
class KConfigGroup;



/**
 * @brief Additional KMess-specific properties of a contact.
 *
 * The class stores "extended" info for a contact, such as an alternative friendlyName.
 * This information is not received from the MSN Messenger servers,
 * the Contact class is reserved for that information.
 * Most of these properties are set by the user from the ContactPropertiesDialog.
 *
 * @author Michael Curtis
 * @ingroup Contact
 */
class ContactExtension : public QObject
{
  Q_OBJECT

  public:
    // The constructor with specified contact handle
                           ContactExtension( const KMess::MsnContact *contact );
    // The destructor
                          ~ContactExtension();

    // Clear the cache of picture files
    void                   clearCache();
    // Read the contact alias
    const QString &        getAlternativeName( FormattingMode mode = STRING_CLEANED ) const;
    // Return the MsnContact 
    const KMess::MsnContact *getContact() const;
    // Read the custom contact picture path
    const QString &        getContactAlternativePicturePath() const;
    // Read the original contact picture path
    const QString          getContactPicturePath() const;
    // Read the custom contact sound path
    const QString          getContactSoundPath() const;
    // Read whether KMess should notify when this contact does an action
    bool                   getDisableNotifications() const;
    // Read when the contact has been seen the last time
    const QDateTime&       getLastSeen();
    // Read when the last message from this contact has been received
    const QDateTime&       getLastMessageDate();
    // Get the notes about this contact
    const QString&         getNotes() const;
    // Read whether to use the alias or not
    bool                   getUseAlternativeName() const;
    // Read the list of pictures which the contact has used
    const QStringList&     getPictureList() const;
     // Load extension settings for a contact
    void                   readProperties( const KConfigGroup &config );
    // Save extension settings for a contact
    void                   saveProperties( KConfigGroup &config );
    // Change the contact alias
    void                   setAlternativeName( const QString& newAlternativeName );
    // Change the custom contact picture path
    void                   setContactAlternativePicturePath( const QString& _newVal );
    // Change the original contact picture path
    void                   setContactPicturePath( const QString& _newVal );
    // Change the custom contact sound path
    void                   setContactSoundPath( const QString& _newVal );
    // Write whether KMess should notify when this contact change status or do any actions
    void                   setDisableNotifications( bool _newVal );
    // Set the time of the last received message from this contact
    void                   setLastMessageDate();
    // Write the notes about this contact
    void                   setNotes( const QString& _newVal );
    // Set whether to use the alias or not
    void                   setUseAlternativeName( bool _newVal );


  private: // Private attributes
    // The alternative name
    FormattedString        alternativeName_;
    // The alternative picture
    QString                alternativePictureFile_;
    // The current contact
    KMess::MsnContact     *contact_;
    // Whether or not to notify actions
    bool                   disableNotifications_;
    // The contact's handle (ie email)
    QString                handle_;
    // The last date the contact has been seen
    QDateTime              lastSeen_;
    // The date the last message has been received
    QDateTime              lastMessageDate_;
    // The contact's notes string
    QString                notes_;
    // A picture used for the "contact is online" balloons
    QString                pictureFile_;
    // A list of all pictures used by the contact
    QStringList            pictureFileList_;
    // A sound played when the contact is online
    QString                soundFile_;
    // Whether or not we should use the alternative name stored in the class
    bool                   useAlternativeName_;

  signals: // Signals
    // Notifiy any watchers that this contact's information has been changed.
    void                   displayNameChanged();
    // Signal that the contact's picture path has changed
    void                   displayPictureChanged();

  private slots:
    // Set when the contact has been seen the last time
    void                   setLastSeen();

};
#endif
