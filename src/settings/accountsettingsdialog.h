/***************************************************************************
                          accountsettingsdialog.h  -  description
                             -------------------
    begin                : Sat Jan 11 2003
    copyright            : (C) 2003 by Mike K. Bennett
    email                : mkb137b@hotmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef ACCOUNTSETTINGSDIALOG_H
#define ACCOUNTSETTINGSDIALOG_H

//#include <QWidget>

#include <KPageDialog>


// Forward declarations
class Account;
class ContactListPage;
class AccountPage;
class AppStylePage;
class ChatLoggingPage;
class ChatMessage;
class ChatMessageStyle;
class ChatMessageView;
class ChatStylePage;
class EmoticonsPage;
class PluginsPage;



/**
 * The account settings dialog.
 *
 * @author Mike K. Bennett
 * @ingroup Settings
 */
class AccountSettingsDialog : public KPageDialog
{
  Q_OBJECT

  public: // Public enumerations
    // A list of all the settings tabs, used to switch the dialog to a determined page and tab when needed
    enum Page
    {
      PageNone                    = -1 /// Used to stop switching pages
    , PageAccount                 =  0
    , PageAccountStatus
    , PageAppStyle
    , PageAppStyleTabText
    , PageContactList
    , PageEmoticons
    , PageEmoticonsTabCustom
    , PageChatStyle
    , PageChatStyleTabText
    , PageChatLogging
    , PagePlugins
    };

  public:
    // The constructor
                                  AccountSettingsDialog( QWidget *parent = 0 );
    // The destructor
                                 ~AccountSettingsDialog();
    // Show the settings dialog for the given account
    void                          loadSettings( Account *account, Page initialPage = PageAccount  );
    // Return a singleton instance of the settings dialog window
    static AccountSettingsDialog *instance();

  private: // Private methods
    // Save all widget settings
    bool                          saveAccountSettings();
    // Select the Account page when showing the settings dialog
    void                          showEvent( QShowEvent *event );

  private slots: // Private slots
    // Save the window options before closing.
    void                          closeEvent( QCloseEvent *event );
    // A button has been pressed, act accordingly
    void                          slotButtonClicked( int button );

  private: // Private attributes
    // The account being edited
    Account                      *account_;
    // The configuration group from which window size is loaded from and saved to
    KConfigGroup                  config_;
    // The widget for entering profile data (handle, name, password).
    AccountPage                  *accountPage_;
    // Page widgets of the settings, saved to switch to a determined tab
    QList<KPageWidgetItem*>       pageWidgets_;
    // The widget for the contact list display options
    ContactListPage              *contactListPage_;
    // The widget for selecting the user's application style.
    AppStylePage                 *appStylePage_;
    // The widget for selecting the user's font.
    ChatStylePage                *chattingPage_;
    // A widget for chat logging settings
    ChatLoggingPage              *chatLoggingPage_;
    // The widget for selecting emoticon styles
    EmoticonsPage                *emoticonPage_;
    // The page where to open the settings dialog at
    Page                          initialPage_;
    // The widget for selecting plugins.
    PluginsPage                  *pluginsPage_;
    // The instance of the singleton settings dialog window
    static AccountSettingsDialog *instance_;

  signals:
    // The settings have been changed
    void                          changedSettings( Account *account, QString oldHandle, QString oldFriendlyName );
    // Request that the given account be deleted
    void                          deleteAccount( Account *account );
};

#endif
