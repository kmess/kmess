/***************************************************************************
                          accountsettingsdialog.cpp  -  description
                             -------------------
    begin                : Sat Jan 11 2003
    copyright            : (C) 2003 by Mike K. Bennett
    email                : mkb137b@hotmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "accountsettingsdialog.h"

#include "../utils/kmessconfig.h"
#include "../utils/kmessshared.h"
#include "../accountsmanager.h"
#include "../account.h"
#include "../emoticonmanager.h"
#include "../kmessglobal.h"
#include "../mainwindow.h"
#include "../kmessdebug.h"
#include "accountpage.h"
#include "appstylepage.h"
#include "contactlistpage.h"
#include "chatloggingpage.h"
#include "chatstylepage.h"
#include "emoticonspage.h"
#include "pluginspage.h"

#include <QCloseEvent>
#include <QShowEvent>

#include <KMessageBox>



// Initialize the instance to zero
AccountSettingsDialog* AccountSettingsDialog::instance_(0);



// The constructor
AccountSettingsDialog::AccountSettingsDialog( QWidget *parent )
: KPageDialog( parent )
, account_(0)
, initialPage_( PageAccount )
{
  // Open the config group we'll use throughout the class
  config_ = KMessConfig::instance()->getGlobalConfig( "AccountSettingsDialog" );

  // Set up the dialog box
  setObjectName( "AccountSettings" );
  setButtons( Help | Ok | Apply | Cancel | User1 );
  setHelp( "settings-account" );
  setButtonText( User1, i18n( "&Delete" ) );
  setDefaultButton( Ok );
  setCaption( i18n("Settings") );
  restoreDialogSize( config_ );

  // Create the widgets that belong in the pages
  accountPage_     = new AccountPage();
  appStylePage_    = new AppStylePage();
  contactListPage_ = new ContactListPage();
  chattingPage_    = new ChatStylePage();
  chatLoggingPage_ = new ChatLoggingPage();
  emoticonPage_    = new EmoticonsPage();
  pluginsPage_     = new PluginsPage();

  // Connect the account page signal to update the chatting page preview
  connect( accountPage_,  SIGNAL(      settingsUpdated(QString,QString,QString) ),
           chattingPage_,   SLOT( updatePreviewDetails(QString,QString,QString) ) );

  // Connect signals for Accounts Manager
  AccountsManager *accountsManager_ = AccountsManager::instance();

  connect( this,             SIGNAL(   deleteAccount(Account*)                 ),
           accountsManager_, SLOT  (   deleteAccount(Account*)                 ) );
  connect( this,             SIGNAL( changedSettings(Account*,QString,QString) ),
           accountsManager_, SLOT  (   changeAccount(Account*,QString,QString) ) );

  // Add the pages to the dialog and set them up
  KPageWidgetItem *page;

  page = addPage( accountPage_, i18n("Account") );
  page->setHeader( i18n( "My Account" ) );
  page->setIcon( KIcon( "user-identity" ) );
  pageWidgets_.insert( PageAccount, page );
  pageWidgets_.insert( PageAccountStatus, page );

  page = addPage( appStylePage_, i18n("Application Styles") );
  page->setHeader( i18n( "Choose an Application Style" ) );
  page->setIcon( KIcon( "preferences-desktop-theme" ) );
  pageWidgets_.insert( PageAppStyle, page );
  pageWidgets_.insert( PageAppStyleTabText, page );


  page = addPage( contactListPage_, i18n("Contact List") );
  page->setHeader( i18n( "Contact List" ) );
  page->setIcon( KIcon( "view-list-details" ) );
  pageWidgets_.insert( PageContactList, page );

  page = addPage( emoticonPage_, i18n("Emoticons") );
  page->setHeader( i18n( "Emoticons" ) );
  page->setIcon( KIcon( "emoticons" ) );
  pageWidgets_.insert( PageEmoticons, page );
  pageWidgets_.insert( PageEmoticonsTabCustom, page );

  page = addPage( chattingPage_, i18n("Chatting") );
  page->setHeader( i18n( "Chatting" ) );
  page->setIcon( KIcon( "format-stroke-color" ) );  // preferences-desktop-color, insert-text?
  pageWidgets_.insert( PageChatStyle, page );
  pageWidgets_.insert( PageChatStyleTabText, page );

  page = addPage( chatLoggingPage_, i18n("Chat Logging") );
  page->setHeader( i18n( "Chat Logging" ) );
  page->setIcon( KIcon( "document-save-as" ) );
  pageWidgets_.insert( PageChatLogging, page );

  page = addPage( pluginsPage_, i18n( "Plugins" ) );
  page->setHeader( i18n( "Plugins" ) );
  page->setIcon( KIcon( "preferences-plugin" ) );
  pageWidgets_.insert( PagePlugins, page );
}



// The destructor
AccountSettingsDialog::~AccountSettingsDialog()
{
  // Save the window size before deleting it
  saveDialogSize( config_ );

  pageWidgets_.clear();

  instance_ = 0;

#ifdef KMESSDEBUG_ACCOUNTSETTINGSDIALOG
  kmDebug() << "DESTROYED.";
#endif
}



// Load the settings for the given account
void AccountSettingsDialog::loadSettings( Account *account, Page initialPage )
{
  bool isNewAccount = false;

  // No account has been specified, create one
  if( ! account )
  {
#ifdef KMESSDEBUG_ACCOUNTSETTINGSDIALOG
    kmDebug() << "Opening settings for a new account";
#endif

    // Create an account to store the profile information
    account = new Account();

    // Assume the user *does* want to save it's settings,
    // otherwise he/she would have used the main login dialog.
    account->setGuestAccount( false );

    // Remember to disable delete button for new account
    isNewAccount = true;
  }
#ifdef KMESSDEBUG_ACCOUNTSETTINGSDIALOG
  else
  {
    kmDebug() << "Opening settings for " << account->getHandle();
  }
#endif

  account_ = account;

  // Load account information into the widgets
  accountPage_    ->loadSettings( account );
  appStylePage_   ->loadSettings( account );
  contactListPage_->loadSettings( account );
  chattingPage_   ->loadSettings( account );
  chatLoggingPage_->loadSettings( account );
  emoticonPage_   ->loadSettings( account );

  // Disable the "remove account" button for guest accounts and for
  // the current account
  if( account->isGuestAccount() ||  account == Account::connectedAccount ||  isNewAccount )
  {
    enableButton( KDialog::User1, false );
    setButtonToolTip( KDialog::User1,
                      i18nc( "Button tooltip text",
                             "Click here to delete this account from the "
                             "list of registered accounts.\nYou cannot delete "
                             "the currently connected account or a guest account, "
                             "which will be deleted when you disconnect." ) );
  }

  // Save the initial page for later, when the dialog is actually shown
  initialPage_ = initialPage;
}



// Save options before closing.
void AccountSettingsDialog::closeEvent( QCloseEvent *event )
{
  saveDialogSize( config_ );
  event->accept();
}



// Return a singleton instance of the settings dialog window
AccountSettingsDialog* AccountSettingsDialog::instance()
{
  // If the instance is null, create a new dialog and return that.
  if ( instance_ == 0 )
  {
    instance_ = new AccountSettingsDialog();
  }

  return instance_;
}



// Save all widget settings
bool AccountSettingsDialog::saveAccountSettings()
{
  if( account_ == 0 )
  {
    return false;
  }

  // Save the old handle and name to check if they were changed later
  QString oldHandle       ( account_->getHandle() );
  QString newHandle       ( accountPage_->getEnteredHandle() );
  QString oldFriendlyName ( account_->getFriendlyName( STRING_ORIGINAL ) );

  // Make sure the account handle was changed off the default
  if( newHandle.isEmpty() || ! KMessShared::validateEmail( newHandle )
  ||  newHandle == i18n( "you@hotmail.com" ) )
  {
    KMessageBox::error( 0, i18n( "The email address you have entered is not valid, and cannot be used as an account: '%1'", newHandle ) );
    return false;
  }

  // Also verify that the account name is not already listed
  if( oldHandle != newHandle
  && AccountsManager::instance()->getAccount( newHandle ) != 0 )
  {
    KMessageBox::error( 0, i18n( "The email address you have entered is already in use: '%1'", newHandle ) );
    return false;
  }

  if ( accountPage_->getEnteredFriendlyName().trimmed().isEmpty() )
  {
    KMessageBox::error( 0, i18n( "Please enter a friendly name for this account." ) );
    setCurrentPage( pageWidgets_.at( PageAccount ) );
    return false;
  }

  // Save information from the widgets
  accountPage_    ->saveSettings( account_ );
  appStylePage_   ->saveSettings( account_ );
  contactListPage_->saveSettings( account_ );
  chattingPage_   ->saveSettings( account_ );
  chatLoggingPage_->saveSettings( account_ );
  emoticonPage_   ->saveSettings( account_ );

  // Let the Accounts Manager know the settings have changed. It will also make sure
  // that the updated settings get synced to disk.
  // TODO FIXME: Shouldn't this be account_.setSettings() or whatever?
  emit changedSettings( account_, oldHandle, oldFriendlyName );

  return true;
}



// Select the Account page when showing the settings dialog
void AccountSettingsDialog::showEvent( QShowEvent */*event*/ )
{
  // After the first switch (happening when the window is first shown)
  // we'll keep whichever page the user has currently selected
  if( initialPage_ == PageNone )
  {
    return;
  }

  // Switch the dialog to the specified page
  setCurrentPage( pageWidgets_.at( initialPage_ ) );

  // Switch to the tabs other than the first if needed
  if( initialPage_ == PageEmoticonsTabCustom )
  {
    emoticonPage_->switchToTab( 1 );
  }
  else if( initialPage_ == PageChatStyleTabText )
  {
    chattingPage_->switchToTab( 1 );
  }
  else if ( initialPage_ == PageAccountStatus )
  {
    accountPage_->switchToTab( 1 );
  }

  initialPage_ = PageNone;
}



// A button has been pressed, act accordingly
void AccountSettingsDialog::slotButtonClicked( int button )
{
  switch( button )
  {
    case KDialog::Ok:
      if( ! saveAccountSettings() )
      {
        return;
      }

      accept();
      break;

    case KDialog::Apply:
      saveAccountSettings();

      // Do not delete the dialog, we still need it
      return;
      break;

    case KDialog::Cancel:
    case KDialog::Close:
    {
      // reset the live preview, if any.
      appStylePage_->reset();
      reject();
      break;
    }

    case KDialog::User1:
      // Delete the account, but first ask the user for confirmation
      if( KMessageBox::warningYesNo( this, i18n("Are you sure you want to delete this account?") ) == KMessageBox::Yes )
      {
        emit deleteAccount( account_ );
        reject();
      }
      else
      {
        // Do not delete the dialog, we still need it
        return;
      }
      break;

    default:
      KDialog::slotButtonClicked( button );
      break;
  }

  // Schedule the dialog for deletion, to save memory
  // if we've been hidden then "Ok" or "Close" has been chosen
  // in which case we can delete ourselves.
  if ( ! isVisible() )
  {
    deleteLater();
  }
}



#include "accountsettingsdialog.moc"
