#ifndef FILETRANSFERSETTINGSPAGE_H
#define FILETRANSFERSETTINGSPAGE_H

/**
 * @brief Allows to modify the file transfer settings
 *
 * @author Francesco Nwokeka
 * @ingroup Settings
 */

#include "ui_filetransfersettingspage.h"

#include <QWidget>

class FileTransferSettingsPage : public QWidget, private Ui::FileTransferSettingsPage
{
    Q_OBJECT

public:
    FileTransferSettingsPage( QWidget *parent = 0 );
    void loadSettings( const KConfigGroup &group );     /** Load users settings */
    bool saveSettings( KConfigGroup &group );           /** Save users settings */

private slots:
    void checkPortsInterval();                          /** Ensure that the specified ports interval is valid */
    void slotChooseDirectory();                         /** Allow the user to navigate the filesystem and choose a directory */
};


#endif  //FILETRANSFERSETTINGSPAGE
