/***************************************************************************
                          emoticonspage.h -  description
                             -------------------
    begin                : Sun Dev 11 2005
    copyright            : (C) 2005 by Diederik van der Boor
    email                : "vdboor" --at-- "codingdomain.com"
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef EMOTICONSPAGE_H
#define EMOTICONSPAGE_H

#include "ui_emoticonspage.h"


// Forward declarations
class Account;
class EmoticonManager;
class EmoticonTheme;

class QListWidgetItem;



/**
 * @brief Allows to choose a standard emoticon theme, and to organize custom emoticons
 *
 * This class creates a temporary custom emoticons theme to avoid making changes which may be refused
 * by the user. If then the user saves the changes by clicking Ok or Apply, then the temporary theme
 * will overwrite the current set; otherwise it'll get destroyed.
 *
 * @author Diederik Van Der Boor, Valerio Pilo
 * @ingroup Settings
 */
class EmoticonsPage : public QWidget, private Ui::EmoticonsPage
{
  Q_OBJECT

  public:  // public methods
    // The constructor
                       EmoticonsPage( QWidget* parent = 0 );
    // The destructor
    virtual           ~EmoticonsPage();

    // Load the settings of the dialog
    void               loadSettings( const Account *account );
    // Save the settings to the account object
    void               saveSettings( Account *account );
    // Force the page onto a specific tab
    void               switchToTab( int tabIndex );

  private:  // private methods
    // Initialize the standard emoticon theme chooser
    void               updateThemesList();

  private slots:
    // Update the Remove Button status when emoticons get selected
    void               customEmoticonSelected();
    // Delete the selected custom emoticon from the theme and the view
    void               removeCustomEmoticon();
    // Starts in-place renaming of the selected custom emoticon
    void               renameCustomEmoticon();
    // Show the Add New Emoticon dialog to create a new custom emoticon
    void               showAddEmoticonDialog();
    // Fill the widget which contains all the user's custom emoticons
    void               updateCustomEmoticonView();
    //Request preview of the emoticon
    void               requestPreview(QListWidgetItem *item );

  private:  // private properties
    // Handle of the account we're changing
    QString            accountHandle_;
    // The temporary emoticon theme
    EmoticonTheme     *emoticonTheme_;
    // Selected emoticon style name
    QString            emoticonStyle_;
    QMovie             *previewMovie_;
};

#endif

