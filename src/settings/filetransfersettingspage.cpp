#include "filetransfersettingspage.h"
#include "../kmessglobal.h"

#include <KFileDialog>
#include <KMessageBox>
#include <KUrlCompletion>
#include <QDir>

FileTransferSettingsPage::FileTransferSettingsPage( QWidget* parent )
  : QWidget( parent )
{
  // setup ui
  setupUi( this );

  connect( lowestTransferPort_, SIGNAL( valueChanged( int ) ), this, SLOT( checkPortsInterval() ) );
  connect( highestTransferPort_, SIGNAL( valueChanged( int ) ), this, SLOT( checkPortsInterval() ) );

  // Prepare and connect the directory selection button
  chooseDirButton_->setIcon( KIcon( "folder-open" ) );
  connect( chooseDirButton_, SIGNAL( clicked() ), this, SLOT( slotChooseDirectory() ) );

  // Make the directory selection friendlier
  KUrlCompletion *dirCompletion = new KUrlCompletion( KUrlCompletion::DirCompletion );
  dirCompletion->setParent( this );
  receivedFilesDirEdit_->setCompletionObject( dirCompletion );
  receivedFilesDirEdit_->setClearButtonShown( true );
}



void FileTransferSettingsPage::loadSettings( const KConfigGroup& group )
{
  receivedFilesDirEdit_->setText( group.readEntry( "receivedFilesDir", QDir::homePath() ) );
  useReceivedFilesDir_->setChecked( group.readEntry( "useReceivedFilesDir", false ) );
  lowestTransferPort_->setValue( group.readEntry( "lowestTransferPort", 6891 ) );
  highestTransferPort_->setValue( group.readEntry( "highestTransferPort", 6900 ) );
}



bool FileTransferSettingsPage::saveSettings( KConfigGroup& group )
{
  // Make sure there's a selected directory if the Save files in a directory option is set
  if( useReceivedFilesDir_->isChecked() && receivedFilesDirEdit_->text().isEmpty() ) {
    KMessageBox::error( 0, i18n( "You have to select a directory for the received files!" ) );
    return false;
  }

  // Start saving settings
  group.writeEntry( "receivedFilesDir", receivedFilesDirEdit_->text() );
  group.writeEntry( "useReceivedFilesDir", useReceivedFilesDir_ ->isChecked() );
  group.writeEntry( "lowestTransferPort", lowestTransferPort_  ->value() );
  group.writeEntry( "highestTransferPort", highestTransferPort_ ->value() );

  // Update the MsnSession
  globalSession->setSessionSetting( "P2PTransferLowestPort",  lowestTransferPort_ ->value() );
  globalSession->setSessionSetting( "P2PTransferHighestPort", highestTransferPort_->value() );

  return true;
}



void FileTransferSettingsPage::checkPortsInterval()
{
  int lowerPortValue = lowestTransferPort_->value();

  // Allow a minimum of two ports in the interval
  if( lowerPortValue >= highestTransferPort_->value() )
  {
    highestTransferPort_->setValue( lowerPortValue + 1 );
  }
}



void FileTransferSettingsPage::slotChooseDirectory()
{
  const KUrl url( KFileDialog::getExistingDirectoryUrl(
                    receivedFilesDirEdit_->text(),
                    this, i18n( "Select Directory" ) )
                );

  if( ! url.isEmpty() )
  {
    receivedFilesDirEdit_->setText( url.path() );
  }
}
