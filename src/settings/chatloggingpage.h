/***************************************************************************
                          chatloggingpage.h -  description
                             -------------------
    begin                : Thu Jan 30 2008
    copyright            : (C) 2008 by Diederik van der Boor
    email                : "vdboor" --at-- "codingdomain.com"
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CHATLOGGINGPAGE_H
#define CHATLOGGINGPAGE_H

#include "ui_chatloggingpage.h"

#include <QWidget>


// Forward class declarations
class Account;



/**
 * Setttings page for the alerts settings page.
 *
 * @ingroup Settings
 */
class ChatLoggingPage : public QWidget, private Ui::ChatLoggingPage
{
  Q_OBJECT

  public:
    // Constructor
           ChatLoggingPage( QWidget* parent = 0 );

  public:  // Public methods
    // Load the widget state from an account
    void   loadSettings( const Account *account );
    // Save an account's information according to the widgets
    void   saveSettings( Account *account );

  private slots:
    // Get a directory from the user
    void   chooseDirectory();
};


#endif
