/***************************************************************************
 *   Copyright (C) 2008 by Joris Guisson and Ivan Vasic                    *
 *   joris.guisson@gmail.com                                               *
 *   ivasic@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.          *
 ***************************************************************************/

/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright 2010 Daniel E. Moctezuma <democtezuma@gmail.com>          *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

#ifndef PLUGINSMODEL_H
#define PLUGINSMODEL_H

#include <QAbstractListModel>
#include <QWidget>

class KArchive;
class KArchiveDirectory;
class Plugin;
class PluginsMaster;

/**
  * @brief Handle all the 'is enabled/disabled' stuff to be shown in the PluginsPage
  *
  * @author Joris Guisson
  * @author Ivan Vasic
  * @author Daniel E. Moctezuma
  * @ingroup Settings
  */
class PluginsModel : public QAbstractListModel
{
  Q_OBJECT

  public:
                          PluginsModel( QWidget* parent );
                 virtual ~PluginsModel();

    enum Role
    {
      CommentRole = Qt::UserRole,
      ConfigurableRole,
      ConfigureRole,
      AboutRole
    };

    Plugin*               pluginForIndex( const QModelIndex &index ) const;
    void                  runPlugins( const QStringList &r );
    void                  setPluginList( QList<Plugin*> list );

    virtual QVariant      data( const QModelIndex &index, int role ) const;
    virtual Qt::ItemFlags flags( const QModelIndex &index ) const;
    virtual bool          insertRows( int row, int count, const QModelIndex &parent );
    virtual bool          removeRows( int row, int count, const QModelIndex &parent );
    virtual int           rowCount( const QModelIndex &parent ) const;
    virtual bool          setData( const QModelIndex &index, const QVariant &value, int role );

  signals:
    void                  showPropertiesDialog( Plugin* plugin );

  private:
    PluginsMaster*        pluginsMaster_;
    QList<Plugin*>        plugins_;
};

#endif
