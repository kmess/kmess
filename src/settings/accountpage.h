/***************************************************************************
                          accountpage.h  -  description
                             -------------------
    begin                : 03-08-2004
    copyright            : (C) 2004 by Madelman
                           (C) 2006 by Diederik van der Boor
    email                : mkb137b@hotmail.com
                           "vdboor" --at-- "codingdomain.com"
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef ACCOUNTPAGE_H
#define ACCOUNTPAGE_H

#include "../account.h"
#include "ui_accountpage.h"

#include <QPixmap>
#include <QString>

class UserPicturesDialog;

/**
 * The main account settings page.
 *
 * @author Mike K. Bennet, Diederik van der Boor
 * @ingroup Settings
 */
class AccountPage : public QWidget, private Ui::AccountPage
{
  Q_OBJECT

  public:
    // Constructor
                       AccountPage( QWidget* parent = 0 );
    // Destructor
    virtual           ~AccountPage();
    // Return the handle which is currently entered.
    const QString      getEnteredHandle() const;
    // Return the friendly name currently entered
    const QString      getEnteredFriendlyName() const;
    // Request to load the settings
    void               loadSettings( const Account *account );
    // Request to save the settings
    void               saveSettings( Account *account );
    // Force the page onto a specific tab
    void               switchToTab( int tabIndex );

  public slots:
    // Show the normal browse dialog to change display picture
    void               pictureBrowseSimple();
    // Show the browse dialog to change display picture and then allow the user to crop it
    void               pictureBrowseResize();
    // Show the browse dialog to view the used user images
    void               pictureSetPrevious();
    // The user toggled the remember me option
    void               rememberMeToggled(bool noGuest);
    // The user pressed the browse picture button
    void               selectPicture( bool resize, QString picturePath = QString() );
    // The user togged the show picture checkbox
    void               showPictureToggled(bool noPicture);
    // The user pressed the "Create new account" button
    void               showRegisterPassport();
    // The user pressed the "Send verification email" button
    void               showVerifyPassport();

  private:
    // The handle of the account
    QString            myHandle_;
    // The picture file of the account
    QString            pictureFile_;
    // The location where display pictures are saved
    QString            pictureDir_;
    // The location of the downloaded display picture
    QString            tempPictureFile_;
    // The file format of the downloaded display picture
    QString            tempPictureFormat_;
    // Do we have a custom display picture or not?
    bool               hasTempPicture_;
    // User pictures dialog
    UserPicturesDialog  *userPicturesDialog_;

  private slots:
    // Send a signal to the Chat Style page with the new account settings
    void               forwardSettingsUpdate();

  signals:
    // Send to the Chat Style page the new account details, to update the preview
    void               settingsUpdated( const QString &handle, const QString &name, const QString &picture );
};

#endif // ACCOUNTSWIDGET_H
