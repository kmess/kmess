/***************************************************************************
                          miscellaneouspage.h -  description
                             -------------------
    begin                : Sat May 3 2008
    copyright            : (C) 2008 by Antonio Nastasi
    email                : sifcenter@gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef MISCELLANEOUSPAGE_H
#define MISCELLANEOUSPAGE_H

#include "ui_miscellaneouspage.h"

#include <QWidget>



/**
 * @brief Allows to choose the miscellaneous settings
 *
 * @author Antonio Nastasi
 * @ingroup Settings
 */
class MiscellaneousPage : public QWidget, private Ui::MiscellaneousPage
{
  Q_OBJECT


  public:  // public methods

    // The constructor
                        MiscellaneousPage( QWidget *parent = 0 );
    // The destructor
    virtual            ~MiscellaneousPage();
    // Load the widget state
    void                loadSettings( const KConfigGroup &group );
    // Save account information from the misc options widget
    bool                saveSettings( KConfigGroup &group );

  private: // Private properties

    // A list of browser executables, mapped by browser name
    QMap<QString,QString> browsersList_;
};

#endif

