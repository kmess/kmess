/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright 2010 Daniel E. Moctezuma <democtezuma@gmail.com>          *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

#include "pluginspage.h"

#include "pluginsdelegate.h"
#include "pluginsmodel.h"
#include "../plugins/pluginsmaster.h"
#include "../kmessapplication.h"

#include "../kmessdebug.h"

#include <QVBoxLayout>

PluginsPage::PluginsPage( QWidget* parent )
: QWidget( parent )
, pluginsMaster_( KMessApplication::instance()->pluginsMaster() )
{
  QVBoxLayout* layout = new QVBoxLayout( this );
  layout->setSpacing( 0 );
  layout->setMargin( 0 );

  // Create the plugin's model that will handle all the plugin items to be displayed
  pluginsModel_ = new PluginsModel( this );

#ifdef KMESSDEBUG_PLUGINSPAGE
  kmDebug() << "PluginsModel object created.";

  if( pluginsMaster_ == NULL )
    kmDebug() << "PluginsMaster IS NULL!";
#endif

  pluginsMaster_->setPluginsModel( pluginsModel_ );

  connect( pluginsModel_, SIGNAL( showPropertiesDialog( Plugin* ) ),
           pluginsMaster_,  SLOT( showProperties( Plugin* ) ) );

  // Find all the plugins available and load them
  pluginsMaster_->load();

  // Setup run/stop KAction instantes for the plugins
  pluginsMaster_->setupActions();

  // Finish setup of the plugins' page
  view_ = new QListView( this );
  view_->setItemDelegate( new PluginsDelegate( view_ ) );
  view_->setAlternatingRowColors( true) ;

  view_->setModel( pluginsModel_ );
  layout->addWidget( view_ );

  view_->setContextMenuPolicy( Qt::CustomContextMenu );
  view_->setSelectionMode( QAbstractItemView::SingleSelection );
  view_->setSelectionBehavior( QAbstractItemView::SelectItems );

  connect( view_->selectionModel(), SIGNAL( selectionChanged( const QItemSelection &, const QItemSelection ) ),
           pluginsMaster_,            SLOT( onSelectionChanged( const QItemSelection &, const QItemSelection ) ) ) ;
  connect( pluginsModel_,           SIGNAL( dataChanged( const QModelIndex&, const QModelIndex& ) ),
           pluginsMaster_,            SLOT( dataChanged( const QModelIndex&, const QModelIndex& ) ) );

  pluginsMaster_->setListView( view_ );
  pluginsModel_->setPluginList( pluginsMaster_->getPluginList() );

  // If there are any running plugins, then mark them as "enabled"
  pluginsModel_->runPlugins( pluginsMaster_->getRunningPluginList() );
#ifdef KMESSDEBUG_PLUGINSPAGE
  kmDebug() << pluginsMaster_->getRunningPluginList().count() << "running plugins marked as 'enabled'!";
  kmDebug() << "Done!";
#endif
}



PluginsPage::~PluginsPage()
{
#ifdef KMESSDEBUG_PLUGINSPAGE
  kmDebug() << "DESTROYED.";
#endif
}



/**
  * @brief Load the settings of the dialog
  *
  * @param account  Account instance which settings will be loaded
  */
void PluginsPage::loadSettings( Account *account )
{
#ifdef __GNUC__
  #warning TODO: implement this function.
#endif
}



/**
 * @brief Save the settings to the account object
 *
 * @param account  Account instance which settings will be saved
 */
void PluginsPage::saveSettings( Account *account )
{
#ifdef __GNUC__
  #warning TODO: implement this function.
#endif
}



#include "pluginspage.moc"
