/***************************************************************************
                          globalsettingsdialog.cpp  -  description
                             -------------------
    begin                : Sat Apr 3 2008
    copyright            : (C) 2008 by Valerio Pilo
    email                : valerio@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "globalsettingsdialog.h"

#include "../utils/kmessconfig.h"
#include "../kmessdebug.h"
#include "accountsmanagerpage.h"
#include "filetransfersettingspage.h"
#include "miscellaneouspage.h"

#include <QCloseEvent>
#include <QShowEvent>



// Initialize the instance to zero
GlobalSettingsDialog* GlobalSettingsDialog::instance_(0);



// The constructor
GlobalSettingsDialog::GlobalSettingsDialog( QWidget *parent )
: KPageDialog( parent )
{
  // Open the config group we'll use throughout the class
  config_ = KMessConfig::instance()->getGlobalConfig( "GlobalSettingsDialog" );

  // Set up the dialog box
  setObjectName( "GlobalSettings" );
  setButtons( Help | Ok | Apply | Cancel );
  setHelp( "settings-kmess" );
  setDefaultButton( Ok );
  setCaption( i18n("KMess Settings") );
  restoreDialogSize( config_ );

  // Create the widgets that belong in the pages
  accountsManagerPage_ = new AccountsManagerPage();
  fileTransferSettingsPage_ = new FileTransferSettingsPage();
  miscellaneousPage_ = new MiscellaneousPage();
  notificationPage_ = new KNotifyConfigWidget( this );
  notificationPage_->setApplication( "kmess" );
  // Force a minimum size which allows the user to actually see the events
  notificationPage_->setMinimumSize( QSize( 300, 300 ) );

  // Add the pages to the dialog and set them up
  KPageWidgetItem *page;

  page = addPage( accountsManagerPage_, i18n("Accounts") );
  page->setHeader( i18n( "Saved Accounts" ) );
  page->setIcon( KIcon( "system-users" ) );

  page = addPage( notificationPage_, i18n( "Notifications" ) );
  page->setIcon( KIcon( "text-speak" ) );
  page->setHeader( i18n( "Notifications" ) );

  page = addPage( miscellaneousPage_, i18n("Settings") );
  page->setHeader( i18n( "Settings" ) );
  page->setIcon( KIcon( "configure" ) );

  page = addPage( fileTransferSettingsPage_, i18n( "FileTransfers" ) );
  page->setIcon( KIcon( "folder-remote" ) );
  page->setHeader( i18n( "Transfer Settings" ) );

  // Let all the tabs load their settings
  loadSettings();
}



// The destructor
GlobalSettingsDialog::~GlobalSettingsDialog()
{
  // Save the window size before deleting it
  saveDialogSize( config_ );

  instance_ = 0;

#ifdef KMESSDEBUG_SETTINGSDIALOG
  kmDebug() << "DESTROYED.";
#endif
}



// Save options before closing.
void GlobalSettingsDialog::closeEvent( QCloseEvent *event )
{
  saveDialogSize( config_ );
  event->accept();
}



// Return a singleton instance of the settings dialog window
GlobalSettingsDialog* GlobalSettingsDialog::instance( QWidget *parent )
{
  // If the instance is null, create a new dialog and return that.
  if ( instance_ == 0 )
  {
    instance_ = new GlobalSettingsDialog( parent );
  }

  return instance_;
}



// Load the settings to all tabs
void GlobalSettingsDialog::loadSettings()
{
  const KConfigGroup& group( KMessConfig::instance()->getGlobalConfig( "General" ) );

  accountsManagerPage_->loadSettings();
  miscellaneousPage_->loadSettings( group );
  fileTransferSettingsPage_->loadSettings( group );
}



// Save the settings from all tabs
bool GlobalSettingsDialog::saveSettings()
{
  KConfigGroup group( KMessConfig::instance()->getGlobalConfig( "General" ) );

  accountsManagerPage_->saveSettings();
  if( ! miscellaneousPage_->saveSettings( group ) )
  {
    return false;
  }

  if( !fileTransferSettingsPage_->saveSettings( group ) )
  {
      return false;
  }

  notificationPage_ ->save();

  // Force the saving to be sure the KMessShared Class gets the correct options
  group.sync();

  return true;
}



// A button has been pressed, act accordingly
void GlobalSettingsDialog::slotButtonClicked( int button )
{
  switch( button )
  {
    case KDialog::Ok:
      if( ! saveSettings() )
      {
        return;
      }

      accept();
      break;

    case KDialog::Apply:
      saveSettings();

      // Do not delete the dialog, we still need it
      return;
      break;

    case KDialog::Cancel:
    case KDialog::Close:
      reject();
      break;

    default:
      KDialog::slotButtonClicked( button );
      break;
  }

  // Schedule the dialog for deletion, to save memory
  // if we've been hidden then "Ok" or "Close" has been chosen
  // in which case we can delete ourselves.
  if ( ! isVisible() )
  {
    deleteLater();
  }
}


#include "globalsettingsdialog.moc"
