/***************************************************************************
                          accountpage.cpp  -  description
                             -------------------
    copyright            : (C) 2004 by Madelman
                           (C) 2006 by Diederik van der Boor
    email                : mkb137b@hotmail.com
                           "vdboor" --at-- "codingdomain.com"
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "accountpage.h"

#include "../dialogs/userpicturesdialog.h"
#include "../utils/kmessconfig.h"
#include "../utils/kmessshared.h"
#include "../account.h"
#include "../accountsmanager.h"
#include "../kmessglobal.h"
#include "../kmessdebug.h"
#include <config-kmess.h>

#include <KMess/Utils>

#include <QFileInfo>

#include <KAction>

#include <KFileDialog>
#include <KIO/NetAccess>
#include <KLocale>
#include <KMenu>
#include <KMessageBox>
#include <QMovie>
#include <KPixmapRegionSelectorDialog>
#include <KStandardDirs>


/**
 * Fixed square size of the Messenger pictures
 *
 * At the moment, the maximum size allowed for display pictures is 96x96.
 */
#define DP_SIZE     96



/**
 * Constructor
 */
AccountPage::AccountPage( QWidget* parent )
: QWidget(parent)
, Ui::AccountPage()
, hasTempPicture_(false)
, userPicturesDialog_(0)
{
  // First setup the user interface
  setupUi( this );

#ifndef HAVE_XSCREENSAVER
  useIdleTimerCheckBox_ ->setEnabled( false );
  idleTimeSpinBox_      ->setEnabled( false );
  idleLabel1_           ->setEnabled( false );
  idleLabel2_           ->setEnabled( false );
#else
  needXScreensaverLabel_->setVisible( false );
#endif

  // Connect the UI signals
  connect( friendlyNameEdit_,  SIGNAL(   textChanged(QString)),        this, SLOT(forwardSettingsUpdate()     ) );
  connect( handleEdit_,        SIGNAL(   textChanged(QString)),        this, SLOT(forwardSettingsUpdate()     ) );
  connect( noPictureCheckbox_, SIGNAL(       toggled(bool)          ), this, SLOT(   showPictureToggled(bool) ) );
  connect( registerButton_,    SIGNAL(leftClickedUrl()              ), this, SLOT( showRegisterPassport()     ) );
  connect( verifyButton_,      SIGNAL(leftClickedUrl()              ), this, SLOT(   showVerifyPassport()     ) );
  connect( rememberCheckbox_,  SIGNAL(       toggled(bool)          ), this, SLOT(    rememberMeToggled(bool) ) );
  connect( pictureLabel_,      SIGNAL(   leftClicked()              ), this, SLOT(  pictureBrowseResize()     ) );
  connect( pictureLabel_,      SIGNAL(  rightClicked()              ), this, SLOT(   pictureSetPrevious()     ) );

  // Set the default display picture
  pictureLabel_->setDefaultPicture( KGlobal::dirs()->findResource( "data", "kmess/pics/kmesspic.png" ) );

  // Create the "Change..." button actions
  KMenu   *browsePopup   = new KMenu( this );
  KAction *browseSimple  = new KAction( KIcon("folder-open"), i18n("Browse..."),                  this );
  KAction *browseResize  = new KAction( KIcon("edit-cut"),    i18n("Browse and Crop Picture..."), this );
  KAction *setPrevious   = new KAction( KIcon("edit-redo"),   i18n("Set Previous Image..."),      this );
  connect( browseSimple, SIGNAL(activated()), this, SLOT(pictureBrowseSimple() ) );
  connect( browseResize, SIGNAL(activated()), this, SLOT(pictureBrowseResize() ) );
  connect( setPrevious,  SIGNAL(activated()), this, SLOT( pictureSetPrevious() ) );

  // Plug the items into the browse menu button
  browsePopup->addAction( browseSimple );
  browsePopup->addAction( browseResize );
  browsePopup->addAction( setPrevious  );

  // Assign the new popup to the button and detach old connection
  browseButton_->setMenu( browsePopup );
  disconnect( browseButton_, SIGNAL( clicked() ) );

  // Add the items to the initial status combo box
  QStringList states;
  states << i18n("Online")
         << i18n("Away")
         << i18n("Be Right Back")
         << i18n("Busy")
         << i18n("Out to Lunch")
         << i18n("On the Phone")
         << i18n("Invisible");
  initialStatus_->addItems( states );
}



/**
 * Destructor
 */
AccountPage::~AccountPage()
{
  delete userPicturesDialog_;

  // Clean up the temp file.
  if( hasTempPicture_ )
  {
    QFile::remove( tempPictureFile_ );
  }

  // no need to delete child widgets, Qt does it all for us
}



// Send a signal to the Chat Style page with the new account settings
void AccountPage::forwardSettingsUpdate()
{
  emit settingsUpdated( handleEdit_->text(), friendlyNameEdit_->text(), tempPictureFile_ );
}



// Return the currently entered handle.
const QString AccountPage::getEnteredHandle() const
{
  return handleEdit_->text();
}



// return the entered friendly name.
const QString AccountPage::getEnteredFriendlyName() const
{
  return friendlyNameEdit_->text();
}



/**
 * Load the dialog settings.
 */
void AccountPage::loadSettings( const Account *account )
{
  bool showPicture;

  bool isCurrentAccount = Account::connectedAccount == account;

  bool isVerified = isCurrentAccount
                      ? globalSession->self()->passportVerified()
                      : true;

  // Read settings
  myHandle_        = account->getHandle();
  pictureFile_     = account->getDisplayPicture();
  pictureDir_      = KMessConfig::instance()->getAccountDirectory( myHandle_ ) + "/displaypics/";
  tempPictureFile_ = pictureDir_ + "temporary-picture.dat"; // No file format assumed
  showPicture      = account->getSettingBool( "DisplayPictureEnabled" );

  // Load default properties
  friendlyNameEdit_ ->setText( account->getFriendlyName( STRING_ORIGINAL ) );
  handleEdit_       ->setText( myHandle_ );

  const QString &password( account->getPassword() );
  rememberPasswordCheckBox_->setChecked( ! password.isEmpty() );
  passwordEdit_     ->setText( password );

  useIdleTimerCheckBox_   ->setChecked( account->getSettingBool( "AutoIdleEnabled" ) );
  idleTimeSpinBox_        ->setValue( account->getSettingInt( "AutoIdleTime" ) );
  hideNotificationsWhenBusyCheckBox_->setChecked( account->getSettingBool( "NotificationsHideWhenBusy" ) );

  // Make sure the drop down list matches the user's initial status
  int item;
  switch( account->getSettingInt( "StatusInitial" ) )
  {
    case KMess::AwayStatus:          item = 1; break;
    case KMess::BrbStatus:           item = 2; break;
    case KMess::BusyStatus:          item = 3; break;
    case KMess::InvisibleStatus:     item = 6; break;
    case KMess::PhoneStatus:         item = 5; break;
    case KMess::LunchStatus:         item = 4; break;
    case KMess::OnlineStatus:
    default:                   item = 0; break;
  }
  initialStatus_->setCurrentIndex( item );

  // Show or hide the help links
  if( isCurrentAccount )
  {
    registerLabel_   ->hide();
    registerButton_  ->hide();
    friendlyNameEdit_->setEnabled( isVerified );
    handleEdit_      ->setEnabled( false );

    // We can only know whether the account is verified when we're logged in
    verifyLabel_     ->setVisible( ! isVerified );
    verifyButton_    ->setVisible( ! isVerified );
  }
  else
  {
    verifyLabel_     ->hide();
    verifyButton_    ->hide();
    friendlyNameEdit_->setEnabled( false );
    handleEdit_      ->setEnabled( true );

    // Only show the Register link when creating new accounts
    bool hasInvalidHandle = ( account->getHandle() == i18n("you@hotmail.com") );
    registerLabel_   ->setVisible( hasInvalidHandle );
    registerButton_  ->setVisible( hasInvalidHandle );
  }

  // When the account is a guest account, offer a method to upgrade to a real account.
  if( account->isGuestAccount() )
  {
    // Disable settings that require a real account which is saved in the config file.
    rememberMeToggled(false);
  }
  else
  {
    // Hide the upgrade function
    rememberCheckbox_->hide();
  }

  // Load the display picture, or the default one if it is not valid
  pictureLabel_->setPicture( pictureFile_ );

  // Set the "Show Picture" checkbox
  // Run the event manually this time to update the GUI.
  noPictureCheckbox_->setChecked( ! showPicture );
  showPictureToggled( ! showPicture );

  // Remove any left over temporary picture
  QFile::remove( tempPictureFile_ );

  // Force update of the preview in the Chat Style settings page
  forwardSettingsUpdate();
}



// Show the normal browse dialog to change display picture
void AccountPage::pictureBrowseSimple()
{
  selectPicture( false );
}



// Show the browse dialog to change display picture and then allow the user to crop it
void AccountPage::pictureBrowseResize()
{
  selectPicture( true );
}



// Show the browse dialog to view the used user images
void AccountPage::pictureSetPrevious()
{
  if( userPicturesDialog_ == 0 )
  {
    userPicturesDialog_ = new UserPicturesDialog();
    connect( userPicturesDialog_,   SIGNAL( pictureUpdate( bool, QString ) ),
             this,                    SLOT( selectPicture( bool, QString ) ) );
  }

  userPicturesDialog_->updateWidgets( myHandle_ );
  userPicturesDialog_->show();
}



/**
 * Save the settings in the account object.
 */
void AccountPage::saveSettings( Account *account )
{
  QString password;
  QString pictureName;
  KMess::MsnStatus  initialStatus;
  QDir    pictureFolder;
  bool    showPicture;

  // Read settings
  showPicture = ! noPictureCheckbox_->isChecked();

  // Update the picture if we have one. This is done before changing the handle, so the new picture is saved at the old account dir
  // and can be moved to the new one easily
  if( hasTempPicture_ )
  {
    if( ! showPicture )
    {
      // The temporary file is no longer needed.
      QFile::remove( tempPictureFile_ );
      hasTempPicture_ = false;

      // Leaving variable 'pictureName' empty, which means no image is selected
    }
    else
    {
      // Retrieve the picture's hash
      QString msnObjectHash( KMess::Utils::generateFileHash( tempPictureFile_ ).toBase64() );
      const QString safeMsnObjectHash( msnObjectHash.replace( QRegExp("[^a-zA-Z0-9+=]"), "_" ) );

      // Save the new display picture with its hash as name, to ensure correct caching
      pictureName = msnObjectHash + "." + tempPictureFormat_;
      pictureFolder = QDir( pictureDir_ );

#ifdef KMESSDEBUG_ACCOUNTSETTINGSDIALOG
      kmDebug() << "Saving temp picture" << tempPictureFile_ << "to final path" << (pictureDir_+pictureName);
#endif

      // Do not overwrite identical pictures
      if( ! pictureFolder.exists( pictureName ) )
      {
        if( pictureFolder.rename( tempPictureFile_, pictureName ) )
        {
          // The temp picture has now its final name, nothing is left to be removed
          hasTempPicture_ = false;
        }
        else
        {
          kmWarning() << "The display picture file could not be renamed from" << ( tempPictureFile_ )
                     << "to" << pictureName << ".";
        }
      }
    }

    // Set the new picture
    account->setSetting( "DisplayPicturePath", pictureDir_ + pictureName );
  }

  // When the account is a guest account, see if the user wanted to "upgrade" it.

  if( account->isGuestAccount() )
  {
    if( rememberCheckbox_->isChecked() )
    {
      account->setGuestAccount(false);
    }
  }

  // Update the initial status
  switch( initialStatus_->currentIndex() )
  {
    case 1:  initialStatus = KMess::AwayStatus;          break;
    case 2:  initialStatus = KMess::BrbStatus;           break;
    case 3:  initialStatus = KMess::BusyStatus;          break;
    case 6:  initialStatus = KMess::InvisibleStatus;     break;
    case 5:  initialStatus = KMess::PhoneStatus;         break;
    case 4:  initialStatus = KMess::LunchStatus;         break;
    case 0:
    default: initialStatus = KMess::OnlineStatus;        break;
  }

  // Update the settings
  // Any handle change will be given to Account, and potentially used from
  // next login onwards; the current friendly name will be given to the
  // global session if connected, and the password will also be given to Account,
  // and when saving KMess' settings it will be read from Account by the
  // AccountsManager.
  //
  // TODO: Some notes here:
  // - If we make it possible to change the handle while logged in, that may
  // cause problems and inconsitencies in the interface;
  // - If we change the friendly name while not logged in, the remote value
  // will overwrite anything entered here, so we make it impossible while the
  // interface says otherwise;
  // - Do we check if the password is incorrect? Shouldn't this be more like
  // a "forget password" option instead of actually making it possible to
  // change it without a succesful login?
  //
  //account->setLoginInformation( handleEdit_->text(), friendlyNameEdit_->text(), passwordEdit_->text() );
  account->setHandle( handleEdit_->text() );

  if( account == Account::connectedAccount )
  {
    globalSession->self()->setDisplayName( friendlyNameEdit_->text() );
  }
  else
  {
    kmWarning() << "Can't change friendly name, not logged in.";
  }

  account->setPassword( passwordEdit_->text() );

  SettingsList settings;
  settings[ "StatusInitial"             ] = initialStatus;
  settings[ "AutoIdleEnabled"           ] = useIdleTimerCheckBox_->isChecked();
  settings[ "AutoIdleTime"              ] = idleTimeSpinBox_->value();
  settings[ "NotificationsHideWhenBusy" ] = hideNotificationsWhenBusyCheckBox_->isChecked();
  settings[ "DisplayPictureEnabled"     ] = showPicture;
  account->setSettings( settings );
}



/**
 * The user toggled the remember me option
 */
void AccountPage::rememberMeToggled(bool noGuest)
{
  initialStatus_->setEnabled( noGuest );
  rememberPasswordCheckBox_->setEnabled( noGuest );
}



/*
 * Allow the user to select a picture, and convert it to 96x96, then place it in a temp file.
 */
void AccountPage::selectPicture( bool resize, QString picturePath )
{
  // This code is partially borrowed from Kopete.

  bool         isRemoteFile = false;
  QDir         dir;
  QFileInfo    fileInfo;
  KUrl         filePath;
  QImageReader imageInfo;
  QPixmap      pictureData;
  QString      localFilePath;

  // Show the open file dialog
  if( picturePath.isEmpty() )
  {
    QString startDir;

    if( ! pictureFile_.isEmpty() )
    {
      // Directory where the current display picture is.
      QFileInfo info( pictureFile_ );
      startDir = info.canonicalPath();
    }
    else
    {
      // Search a resourcedir to use as default.

      // all pictures:  dirs->findAllResources("data", "kdm/pics/users/*.png")
      // default dir: dirs->findResource("data", "kdm/pics/users/");

      // Get the resourcedir which contains the largest collection of pictures.
      // This fixes a problem with SuSE, where /etc/opt/kde/share is returned instead of /opt/kde3/share

      KStandardDirs *dirs = KGlobal::dirs();
      const QStringList& kdmDirs( dirs->findDirs( "data", "kdm/pics/users/" ) );
      QDir kdmDir;
      int fileCount = 0;

      foreach( const QString& kdmPath, kdmDirs )
      {
        kdmDir.setPath( kdmPath );
        const QStringList& pngFiles( kdmDir.entryList( QStringList( "*.png" ) ) );

        if( pngFiles.size() > fileCount )
        {
          fileCount = pngFiles.size();
          startDir = kdmPath;
        }
      }
    }

    filePath = KFileDialog::getImageOpenUrl( startDir, this, i18n( "Display Picture" ) );
  }
  else
  {
    filePath = picturePath;
  }

  // The user has canceled the file open dialog
  if( filePath.isEmpty() )
  {
    return;
  }

  // Read the path.
  if( filePath.isLocalFile() )
  {
    localFilePath = filePath.path();
  }
  else
  {
    // File is remote, download it to a local folder
    // first because QPixmap doesn't have KIO magic.
    // KIO::NetAccess::download fills the localFilePath field.
    if( ! KIO::NetAccess::download( filePath, localFilePath, this ) )
    {
#ifdef KMESSDEBUG_ACCOUNTSETTINGSDIALOG
      kmDebug() << "Unable to download the remote file:" << filePath;
#endif

      // At the end of this method
      goto pictureChangingFailure;
    }

    isRemoteFile = true;
  }

  // Create any missing directories
  fileInfo.setFile( tempPictureFile_ );
  if( ! dir.mkpath( fileInfo.absolutePath() ) )
  {
    kmWarning() << "Making the account folder" << fileInfo.absolutePath() << "has failed!";

    // At the end of this method
    goto pictureChangingFailure;
  }

  // Save the picture locally.
  // We use a temporary picture first, then replace the old one if the changes
  // are confirmed
  QFile::remove( tempPictureFile_ );
  if( ! QFile::copy( localFilePath, tempPictureFile_ ) )
  {
#ifdef KMESSDEBUG_ACCOUNTSETTINGSDIALOG
    kmDebug() << "Copying" << localFilePath << "to" << tempPictureFile_ << "has failed!";
#endif
    // At the end of this method
    goto pictureChangingFailure;
  }

  // We'll need the image pixels too
  pictureData = QPixmap( tempPictureFile_ );

  // Remove any temporary files
  if( isRemoteFile )
  {
    KIO::NetAccess::removeTempFile( localFilePath );
  }

  // Allow the user to select a region of the picture
  if( resize )
  {
    QImage resizedImage( KPixmapRegionSelectorDialog::getSelectedImage( pictureData,
                                                                        DP_SIZE,
                                                                        DP_SIZE,
                                                                        this ) );

    // Abort if cancel was clicked
    if( resizedImage.isNull() )
    {
      return;
    }

    // Use the original picture if the image wasn't cropped
    if( ! resizedImage.isNull() && resizedImage.size() != pictureData.size() )
    {
      pictureData = QPixmap::fromImage( resizedImage );
    }

    // Abort if the image was not valid
    if( pictureData.isNull() )
    {
#ifdef KMESSDEBUG_ACCOUNTSETTINGSDIALOG
      kmDebug() << "Resizing the picture has failed";
#endif
      // At the end of this method
      goto pictureChangingFailure;
    }
  }

  // Retrieve the picture format for later, when saving the settings
  imageInfo.setFileName( localFilePath );
  tempPictureFormat_ = imageInfo.format().toLower();

  // If the picture is animated, do not resize it
  if( ! QMovie::supportedFormats().contains( tempPictureFormat_.toLatin1() ) )
  {
    // This makes the smallest edge 96 pixels, preserving aspect ratio
    pictureData = pictureData.scaled( DP_SIZE, DP_SIZE, Qt::KeepAspectRatioByExpanding, Qt::SmoothTransformation );

    if( pictureData.width() != DP_SIZE || pictureData.height() != DP_SIZE )
    {
      // Crop the picture to its the middle part
      pictureData = pictureData.copy( ( pictureData.width () - DP_SIZE ) / 2, // X
                                      ( pictureData.height() - DP_SIZE ) / 2, // Y
                                      DP_SIZE,
                                      DP_SIZE );
    }

    /**
     * Save static images as JPG:
     * - to maximize the compatibility with WLM, in case the original picture is in
     *   some exotic format supported by Qt but not by windows
     * - to keep storage size down
     * - to minimize bandwidth and data transfer times
     * The quality loss of JPEG is not really much at 100% quality.
     */
    tempPictureFormat_ = "jpg";
    if( ! pictureData.save( tempPictureFile_, tempPictureFormat_.toLatin1(), 100 ) )
    {
#ifdef KMESSDEBUG_ACCOUNTSETTINGSDIALOG
      kmDebug() << "Saving the picture has failed";
#endif
      // At the end of this method
      goto pictureChangingFailure;
    }
  }

  // Sucess! Update the preview picture
  pictureLabel_->setPicture( tempPictureFile_ );
  hasTempPicture_ = true;

#ifdef KMESSDEBUG_ACCOUNTSETTINGSDIALOG
  kmDebug() << "Temporary picture changed to" << tempPictureFile_;
#endif

  // Signal the update to the settings
  forwardSettingsUpdate();

  return;

  // Three code paths lead to this same error.
  // I chose to avoid cluttering the code with the same error over and over again.
  // Note to readers: "goto" is not evil if used with moderation, say once per
  // big method should be ok :)
  // For more information and criticism, e-mail me at noreply@kmess.org
pictureChangingFailure:

  KMessageBox::sorry( this,
                      i18n( "An error occurred while trying to change the display picture.\n"
                            "Make sure that you have selected an existing image file." ),
                      i18n( "KMess" ) );
}



// Slot running when the "Show picture" checkbox was toggled.
void AccountPage::showPictureToggled( bool noPicture )
{
  browseButton_->setEnabled( ! noPicture );
  pictureLabel_->setEnabled( ! noPicture );
}



// The user pressed the "Create new account" button
void AccountPage::showRegisterPassport()
{
  // Launch the browser for the given URL
  KMessShared::openBrowser( KUrl( "https://accountservices.passport.net/reg.srf" ) );
}



// The user pressed the "Send verification email" button
void AccountPage::showVerifyPassport()
{
  // Launch the browser for the given URL
  KMessShared::openBrowser( KUrl( "https://login.live.com/emailval.srf" ) );
}



// Force the page onto a specific tab
void AccountPage::switchToTab( int tabIndex )
{
  if( ( tabIndex < 0 ) || tabIndex > tabWidget_->count() )
  {
    kmWarning() << "Invalid tab index" << tabIndex << "selected!";
    return;
  }

  tabWidget_->setCurrentIndex( tabIndex );
}


#include "accountpage.moc"
