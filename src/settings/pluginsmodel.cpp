/***************************************************************************
 *   Copyright (C) 2008 by Joris Guisson and Ivan Vasic                    *
 *   joris.guisson@gmail.com                                               *
 *   ivasic@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.          *
 ***************************************************************************/

/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright 2010 Daniel E. Moctezuma <democtezuma@gmail.com>          *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

#include "pluginsmodel.h"

#include "../plugins/plugin.h"
#include "../plugins/pluginsmaster.h"

#include "../kmessdebug.h"

#include <KIcon>
#include <KLocale>
#include <KMimeType>

PluginsModel::PluginsModel( QWidget* parent )
: QAbstractListModel( parent )
{
}



PluginsModel::~PluginsModel()
{
#ifdef KMESSDEBUG_PLUGINSMODEL
  kmDebug() << "DESTROYED.";
#endif
}



QVariant PluginsModel::data( const QModelIndex &index, int role ) const
{
  Plugin* plugin = pluginForIndex( index );

  if ( ! plugin )
  {
    return QVariant();
  }

  switch ( role )
  {
    case Qt::DisplayRole:
      return plugin->getKPluginInfo().name();
    case Qt::DecorationRole:
      return plugin->getKPluginInfo().icon();
    case Qt::CheckStateRole:
      return plugin->isRunning();
    case Qt::ToolTipRole:
      return i18n( "<b>%1</b><br/><br/>%2", plugin->getKPluginInfo().name(), plugin->getKPluginInfo().comment() );
    case CommentRole:
      return plugin->getKPluginInfo().comment();
    case ConfigurableRole:
      return plugin->isRunning() && plugin->hasConfigure();
    default:
      return QVariant();
  }
}



Qt::ItemFlags PluginsModel::flags( const QModelIndex &index ) const
{
  if ( ! index.isValid() )
  {
    return QAbstractItemModel::flags( index );
  }
  else
  {
    return QAbstractItemModel::flags( index ) | Qt::ItemIsUserCheckable;
  }
}



bool PluginsModel::insertRows( int row, int count, const QModelIndex &parent )
{
  Q_UNUSED( parent );

  beginInsertRows( QModelIndex(), row, row + count - 1 );
  endInsertRows();
  return true;
}



Plugin* PluginsModel::pluginForIndex( const QModelIndex &index ) const
{
  if ( ! index.isValid() )
  {
    return 0;
  }

  if ( index.row() < 0 || index.row() >= plugins_.count() )
  {
    return 0;
  }

  return plugins_[ index.row() ];
}



bool PluginsModel::removeRows( int row, int count, const QModelIndex &parent )
{
  Q_UNUSED( parent );

  beginRemoveRows( QModelIndex(), row, row + count - 1 );
  endRemoveRows();
  return true;
}



int PluginsModel::rowCount( const QModelIndex &parent ) const
{
  return parent.isValid() ? 0 : plugins_.count();
}



void PluginsModel::runPlugins( const QStringList &r )
{
  int idx = 0;
  foreach( Plugin* plugin, plugins_ )
  {
#ifdef KMESSDEBUG_PLUGINSMODEL
    kmDebug() << " plugin is running?: " << plugin->isRunning();
#endif

    if ( r.contains( plugin->file() ) && ! plugin->isRunning() )
    {
      plugin->run();
      QModelIndex i = index( idx, 0 );
      emit dataChanged( i, i );
    }
    idx++;
  }
}



bool PluginsModel::setData( const QModelIndex &index, const QVariant &value, int role )
{
  if ( ! index.isValid() )
  {
    return false;
  }

  Plugin* plugin = pluginForIndex( index );
  if ( ! plugin )
  {
    return false;
  }

  if ( role == Qt::CheckStateRole )
  {
    if ( value.toBool() )
      plugin->run();
    else
      plugin->stop();

    emit dataChanged( index, index );
    return true;
  }
  else if ( role == ConfigureRole )
  {
    plugin->configure();
    return true;
  }
  else if ( role == AboutRole )
  {
    emit showPropertiesDialog( plugin );
    return true;
  }

  return false;
}



void PluginsModel::setPluginList( QList< Plugin* > list )
{
  plugins_ = list;
}



#include "pluginsmodel.moc"
