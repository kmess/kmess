/***************************************************************************
                          emoticonspage.cpp -  description
                             -------------------
    begin                : Sat May 3 2008
    copyright            : (C) 2008 by Valerio Pilo
    email                : valerio@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "accountsmanagerpage.h"

#include "accountsettingsdialog.h"
#include "../utils/kmessconfig.h"
#include "../account.h"
#include "../accountsmanager.h"
#include "../mainwindow.h"
#include "../kmessdebug.h"
#include "../kmessglobal.h"

#include <QListWidget>

#include <KMessageBox>




/**
 * Constructor
 *
 * @param parent  Parent widget
 */
AccountsManagerPage::AccountsManagerPage( QWidget *parent )
: QWidget(parent)
, Ui::AccountsManagerPage()
{
  // First setup the user interface
  setupUi( this );

  accountsManager_ = AccountsManager::instance();

  addAccountButton_->setIcon      ( KIcon( "list-add-user"    ) );
  // This icon seems to be black while the others are white (22x22 only!)
  configureAccountButton_->setIcon( KIcon( "user-properties"  ) );
  removeAccountButton_->setIcon   ( KIcon( "list-remove-user" ) );

  // Connect the signals
  connect( addAccountButton_,       SIGNAL(              clicked()                         ),
           this,                    SLOT  ( showAddAccountDialog()                         ) );
  connect( configureAccountButton_, SIGNAL(              clicked()                         ),
           this,                    SLOT  (     configureAccount()                         ) );
  connect( removeAccountButton_,    SIGNAL(              clicked()                         ),
           this,                    SLOT  (        deleteAccount()                         ) );
  connect( accountsList_,           SIGNAL(        doubleClicked(QListWidgetItem*,QPoint)  ),
           this,                    SLOT  (     configureAccount()                         ) );
  connect( accountsList_,           SIGNAL( itemSelectionChanged()                         ),
           this,                    SLOT  (      accountSelected()                         ) );
  connect( accountsManager_,        SIGNAL(         accountAdded(Account*)                 ),
           this,                    SLOT  (         loadSettings()                         ) );
  connect( accountsManager_,        SIGNAL(       accountChanged(Account*,QString,QString) ),
           this,                    SLOT  (         loadSettings()                         ) );
  connect( accountsManager_,        SIGNAL(       accountDeleted(Account*)                 ),
           this,                    SLOT  (         loadSettings()                         ) );
}



/**
 * Destructor
 */
AccountsManagerPage::~AccountsManagerPage()
{
}



/**
 * Update the buttons status when accounts get selected
 */
void AccountsManagerPage::accountSelected()
{
  bool hasSelection = ( ! accountsList_->selectedItems().isEmpty() );

  configureAccountButton_->setEnabled( hasSelection );
  removeAccountButton_   ->setEnabled( hasSelection );

  // Disallow removal of the currently connected account.
  if( hasSelection )
  {
    const QListWidgetItem *selection = accountsList_->selectedItems().first();
    const QString &handle( selection->data( Qt::UserRole ).toString() );

    if( globalSession->isLoggedIn()
    &&  Account::connectedAccount->getHandle() == handle )
    {
      removeAccountButton_->setEnabled( false );
    }
  }
}



/**
 * Open the currently selected account's settings
 */
void AccountsManagerPage::configureAccount()
{
  if( accountsList_->selectedItems().isEmpty() )
  {
    return;
  }

  const QListWidgetItem * selection = accountsList_->selectedItems().first();
  const QString& accountHandle( selection->data( Qt::UserRole ).toString() );

#ifdef KMESSDEBUG_SETTINGSDIALOG
  kmDebug() << "Opening Account Settings for account" << accountHandle << ".";
#endif


  Account *account = accountsManager_->getAccount( accountHandle );

  // Show the settings dialog
  AccountSettingsDialog *accountSettingsDialog = AccountSettingsDialog::instance();
  accountSettingsDialog->loadSettings( account );
  accountSettingsDialog->show();
}



/**
 * Delete the currently selected account
 */
void AccountsManagerPage::deleteAccount()
{
  if( accountsList_->selectedItems().isEmpty() )
  {
    kmWarning() << "No account was selected for deletion!";
    return;
  }

  const QListWidgetItem *selection = accountsList_->selectedItems().first();
  const QString &handle( selection->data( Qt::UserRole ).toString() );

  // Prevent deletion of the currently connected account
  if( globalSession->isLoggedIn()
  &&  Account::connectedAccount->getHandle() == handle )
  {
    kmWarning() << "Cannot delete the current account!";
    return;
  }

  Account *account = accountsManager_->getAccount( handle );
  if( ! account )
  {
    return;
  }

  int result = KMessageBox::warningYesNo(
                                 this,
                                 i18n("<html>Are you sure you want to delete the account '%1' ?<br/>"
                                      "All settings of this account will be lost.</html>",
                                      handle ) );
  if( result != KMessageBox::Yes )
  {
    return;
  }

  accountsManager_->deleteAccount( account );

  // Refresh the accounts list
  loadSettings();
}



/**
 * Load the settings of the dialog
 */
void AccountsManagerPage::loadSettings()
{
  accountsList_->clear();
  autoLoginComboBox_->clear();
  const QList<Account*>  &accountsList = AccountsManager::instance()->getAccounts();
  const Account *autoLoginAccount = AccountsManager::instance()->getAutoLoginAccount();

  foreach( const Account *account, accountsList )
  {
    const QString text( account->getFriendlyName() + "\n" + account->getHandle() );
    QListWidgetItem *item = new QListWidgetItem( QIcon( account->getDisplayPicture() ), text, accountsList_ );
    item->setData( Qt::UserRole, account->getHandle() );

    if( ! account->getPassword().isEmpty() )
    {
      autoLoginComboBox_->addItem( account->getHandle(), account->getHandle() );
    }

    // Select the current autologin account
    if( account == autoLoginAccount )
    {
      autoLoginComboBox_->setCurrentIndex( autoLoginComboBox_->count() - 1 );
    }
  }

  if( autoLoginComboBox_->count() > 0 )
  {
    autoLoginComboBox_->setEnabled( true );
    autoLoginCheckBox_->setEnabled( true );
    autoLoginCheckBox_->setChecked( autoLoginAccount != 0 );
  }
  else
  {
    autoLoginComboBox_->addItem( i18n( "No accounts have a saved password" ) );
  }

  // Update the viewport and the buttons
  accountSelected();
}



// Save the settings of the dialog
void AccountsManagerPage::saveSettings()
{
  Account *autoLoginAccount = 0;

  if( autoLoginCheckBox_->isEnabled() && autoLoginCheckBox_->isChecked() )
  {
    const QString &autoLoginHandle( autoLoginComboBox_->itemData( autoLoginComboBox_->currentIndex() ).toString() );
    autoLoginAccount = AccountsManager::instance()->getAccount( autoLoginHandle );
  }

  AccountsManager::instance()->setAutoLoginAccount( autoLoginAccount );
}



/**
 * Show the Account Settings dialog for a new account
 */
void AccountsManagerPage::showAddAccountDialog()
{
  // Show the settings dialog
  AccountSettingsDialog *accountSettingsDialog = AccountSettingsDialog::instance();
  accountSettingsDialog->loadSettings( 0 );
  accountSettingsDialog->show();
}



#include "accountsmanagerpage.moc"
