/***************************************************************************
                          chatstylepage.h -  description
                             -------------------
    begin                : Thu Jan 30 2008
    copyright            : (C) 2008 by Diederik van der Boor
    email                : "vdboor" --at-- "codingdomain.com"
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CHATSTYLEPAGE_H
#define CHATSTYLEPAGE_H

#include "ui_chatstylepage.h"

#include <QColor>
#include <QFont>
#include <QWidget>

#include <KMess/MsnContact>

// Forward class declarations
class Account;
class ChatMessageStyle;
class ChatMessageView;


/**
 * Represents a "fake" contact designed to attach to the chat history view
 * in the settings page.
 */
class PlaceholderContact : public KMess::MsnContact
{
  public:
    PlaceholderContact( const QString &id, const QString &friendlyName );
};



/**
 * Setttings page for the alerts settings page.
 *
 * @ingroup Settings
 */
class ChatStylePage : public QWidget, private Ui::ChatStylePage
{
  Q_OBJECT

  public:
    // Constructor
                       ChatStylePage( QWidget* parent = 0 );
    // Destructor
                      ~ChatStylePage();

  public:  // Public methods
    // Load the widget state from an account
    void               loadSettings( const Account *account );
    // Save an account's information according to the widgets
    void               saveSettings( Account *account );
    // Force the page onto a specific tab
    void               switchToTab( int tabIndex );

  private:  // Private methods
    // Get a font and cleaned-up font family from a dialog
    bool               getFont( QFont &font ) const;

  public slots:
    // Update the data shown in the message preview
    void               updatePreviewDetails( const QString &handle, const QString &name, const QString &picture );

  private slots:
    // The contact font button was pressed.  Show a font dialog to get a new font.
    void               contactFontClicked();
    // The font button was pressed.  Show a font dialog to get a new font.
    void               fontClicked();
    // The chat style was changed
    void               slotChatStyleChanged( const QString &style );
    // Update the contacts font color
    void               slotContactFontColorChanged( const QColor &color );
    // Update the user font color
    void               slotUserFontColorChanged( const QColor &color );
    // Update the chat style preview
    void               updatePreview();
    // Enable or disable the contact font and color selectors
    void               useContactFontToggled( bool checked );
    // Open a dialog for KDE New Stuff Chat Themes
    void               getNewThemes();
    // Update the style list
    void               loadStyleList();

  private:  // Private properties
    // The chat message preview
    ChatMessageView   *chatMessageView_;
    // The XSL transformation handler
    ChatMessageStyle  *chatStyleTransform_;
    // Color used for contact messages
    QColor             contactColor_;
    // Font used for contact messages
    QFont              contactFont_;
    // User handle shown in the chat preview
    QString            previewHandle_;
    // User picture shown in the chat preview
    QString            previewPicture_;
    // Username shown in the chat preview
    QString            previewUsername_;
    // Color used for user messages
    QColor             userColor_;
    // Font used for user messages
    QFont              userFont_;
};


#endif
