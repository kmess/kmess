/***************************************************************************
                          contactlistpage.h -  description
                             -------------------
    begin                : Sun Mar 1 2009
    copyright            : (C) 2009 by Andrea Decorte
    email                : "adecorte" --at-- "gmail.com"
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CONTACTLISTPAGE_H
#define CONTACTLISTPAGE_H

#include "ui_contactlistpage.h"

#include <QWidget>


// Forward class declarations
class Account;



/**
 * Setttings page for the contact list settings.
 *
 * @ingroup Settings
 * @author Andrea Decorte
 */
class ContactListPage : public QWidget, private Ui::ContactListPage
{
  Q_OBJECT

  public:
    // Constructor
           ContactListPage( QWidget* parent = 0 );

  public:  // Public methods
    // Load the widget state from an account
    void   loadSettings( const Account *account );
    // Save an account's information according to the widgets
    void   saveSettings( Account *account );
};


#endif
