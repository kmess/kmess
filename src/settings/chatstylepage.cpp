/***************************************************************************
                          chatstylepage.cpp -  description
                             -------------------
    begin                : Thu Jan 30 2008
    copyright            : (C) 2008 by Diederik van der Boor
    email                : "vdboor" --at-- "codingdomain.com"
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "chatstylepage.h"

#include "../chat/chatmessagestyle.h"
#include "../chat/chatmessageview.h"
#include "../account.h"
#include "../kmessdebug.h"

#include "../kmessglobal.h"

#include <KMess/TextMessage>
#include <KMess/MsnContact>
#include <KMess/Message>

#include <QDir>

#include <KFontDialog>
#include <KHTMLView>
#include <KStandardDirs>
#include <knewstuff2/engine.h>


/**
 * Construct a new PlaceholderContact which is designed for place holding in the chat history view.
 * 
 * @param id The ID (handle) of the contact.
 * @param friendlyName The friendly name to display for the contact.
 */
PlaceholderContact::PlaceholderContact( const QString &id, const QString &friendlyName )
  : MsnContact( id )
{
  _setDisplayName( friendlyName );
}



/**
 * The constructor
 */
ChatStylePage::ChatStylePage( QWidget *parent )
: QWidget( parent )
, Ui::ChatStylePage()
{
  setupUi( this );

  newStylesButton_->setIcon( KIcon( "get-hot-new-stuff" ) );

  // Connect our signals
  connect( contactFontColorButton_,       SIGNAL(                     changed(QColor)  ),
           this,                          SLOT  (               updatePreview()        ) );
  connect( fontColorButton_,              SIGNAL(                     changed(QColor)  ),
           this,                          SLOT  (               updatePreview()        ) );
  connect( contactFontColorButton_,       SIGNAL(                     changed(QColor)  ),
           this,                          SLOT  ( slotContactFontColorChanged(QColor)  ) );
  connect( fontColorButton_,              SIGNAL(                     changed(QColor)  ),
           this,                          SLOT  (    slotUserFontColorChanged(QColor)  ) );
  connect( useContactFontCheckBox_,       SIGNAL(                     toggled(bool)    ),
           this,                          SLOT  (       useContactFontToggled(bool)    ) );
  connect( fontButton_,                   SIGNAL(                     clicked()        ),
           this,                          SLOT  (                 fontClicked()        ) );
  connect( contactFontButton_,            SIGNAL(                     clicked()        ),
           this,                          SLOT  (          contactFontClicked()        ) );
  connect( chatStyle_,                    SIGNAL(                   activated(QString) ),
           this,                          SLOT  (        slotChatStyleChanged(QString) ) );
  connect( useEmoticonsCheckBox_,         SIGNAL(                     toggled(bool)    ),
           this,                          SLOT  (               updatePreview()        ) );
  connect( useFontEffectsCheckBox_       ,SIGNAL(                     toggled(bool)    ),
           this,                          SLOT  (               updatePreview()        ) );
  connect( showTimeCheckbox_,             SIGNAL(                     toggled(bool)    ),
           this,                          SLOT  (               updatePreview()        ) );
  connect( showDateCheckbox_,             SIGNAL(                     toggled(bool)    ),
           this,                          SLOT  (               updatePreview()        ) );
  connect( showSecondsCheckbox_,          SIGNAL(                     toggled(bool)    ),
           this,                          SLOT  (               updatePreview()        ) );
  connect( groupFollowupCheckbox_,        SIGNAL(                     toggled(bool)    ),
           this,                          SLOT  (               updatePreview()        ) );
  connect( enableChatFormattingCheckBox_, SIGNAL(              toggled(bool)           ),
           this,                          SLOT  (               updatePreview()        ) );
  connect( newStylesButton_,              SIGNAL(                     clicked()        ),
           this,                          SLOT  (                getNewThemes()        ) );


  // Configure the preview form of the "chatting" widget

  // Insert a KHTMLPart in the placeholder
  chatMessageView_ = new ChatMessageView( styleTab_, this );

  // Create a layout to maximize the KHTMLPart
  QBoxLayout *layout = new QHBoxLayout( khtmlPlaceholder_ );
  layout->addWidget( chatMessageView_->view() );

  loadStyleList();
}



// Destructor
ChatStylePage::~ChatStylePage()
{
}



// Update the style list
void ChatStylePage::loadStyleList()
{
  // Save the current choice to apply it again, after the list has been reloaded
  const QString& text( chatStyle_->currentText() );

  // Remove any previous items, in case they're in
  chatStyle_->clear();

  // Get all available chat styles for the "chatting" widget, avoiding duplicate entries
  const QStringList& stylesDirectories( KGlobal::dirs()->findDirs( "data", "kmess/styles" ) );

  foreach( const QString &dir, stylesDirectories )
  {
    const QDir styleDir( dir );
    const QStringList& styles( styleDir.entryList( QDir::Dirs, QDir::Name | QDir::IgnoreCase ) );

    foreach( const QString &styleName, styles )
    {
      // e.g. share/apps/kmess/styles/Demo/Demo.xsl
      const QString xslFile( styleDir.path() + '/' + styleName + '/' + styleName + ".xsl" );
      if( QFile::exists( xslFile ) && ( chatStyle_->findText( styleName ) == -1 ) )
      {
        chatStyle_->addItem( styleName );
      }
    }
  }

  // Prepare the XSL transformation
  chatStyleTransform_ = chatMessageView_->getStyle();

  if( ! text.isEmpty() )
  {
    chatStyle_->setCurrentIndex( chatStyle_->findText( text, Qt::MatchExactly ) );
  }
}



// The contact font button was pressed.  Show a font dialog to get a new font.
void ChatStylePage::contactFontClicked()
{
  // Get a new font and font family from the user: if they get
  // changed, set the new font to the button
  if( ! getFont( contactFont_ ) )
  {
    return;
  }

  contactFontButton_->setText( contactFont_.family().replace( '&', "&&" ) );
  contactFontButton_->setFont( contactFont_ );
  contactFontButton_->setShortcut( QKeySequence() );

  // Update the preview
  chatStyleTransform_->setContactFont( contactFont_ );
  updatePreview();
}



// The font button was pressed.  Show a font dialog to get a new font.
void ChatStylePage::fontClicked()
{
  // Get a new font and font family from the user: if they get
  // changed, set the new font to the button
  if( ! getFont( userFont_ ) )
  {
    return;
  }

  fontButton_->setText( userFont_.family().replace( '&', "&&" ) );
  fontButton_->setFont( userFont_ );
  fontButton_->setShortcut( QKeySequence() );

  // Update the preview
  // We don't set chatStyleTransform like in contactFontClicked(),
  // because user messages are automatically generated with the currently
  // selected font
  updatePreview();
}



// Get a font and cleaned-up font family from a dialog
bool ChatStylePage::getFont( QFont &font ) const
{
  bool  accepted;
  int   result;
  QFont selection = font;

  result = KFontDialog::getFont( selection );
  accepted = ( result == KFontDialog::Accepted );

  if ( accepted )
  {
    font = selection;
  }

  return accepted;
}



/**
 * @brief Load the widget state from an account
 *
 * Reads an Account's info and sets our internal widgets accordingly
 *
 * @param account The account where to pick settings from
 */
void ChatStylePage::loadSettings( const Account *account )
{
  QPixmap        pixmap;
  const QString& emoticonDir ( account->getSettingString( "EmoticonsTheme" ) );
  KStandardDirs *dirs        = KGlobal::dirs();

  // Set up the emoticons which explain what the graphics elements in chat are
  pixmap.load( dirs->findResource( "emoticons", emoticonDir + "/teeth.png" ) );
  pixmapLabel1_->setPixmap( pixmap );
  pixmap.load( dirs->findResource( "emoticons", emoticonDir + "/rainbow.png" ) );
  pixmapLabel2_->setPixmap( pixmap );
  pixmap.load( dirs->findResource( "emoticons", emoticonDir + "/dog.png" ) );
  pixmapLabel3_->setPixmap( pixmap );
  
  userFont_    = account->getSettingFont( "FontUser" );
  contactFont_ = account->getSettingFont( "FontContact" );
  userColor_   .setNamedColor( account->getSettingString( "FontUserColor" )        );
  contactColor_.setNamedColor( account->getSettingString( "FontContactColor" ) );

  fontButton_->setText( userFont_.family().replace( '&', "&&" ) );
  fontButton_->setFont( userFont_ );
  fontButton_->setShortcut( QKeySequence() );
  fontColorButton_->setColor( userColor_ );

  contactFontButton_->setText( contactFont_.family().replace( '&', "&&" ) );
  contactFontButton_->setFont( contactFont_ );
  contactFontButton_->setShortcut( QKeySequence() );
  contactFontColorButton_->setColor( contactColor_ );

  useContactFontToggled( account->getSettingBool( "FontContactForce" ) );
  tabbedChatMode_->setCurrentIndex( account->getSettingInt( "ChatTabbingMode" ) );
  useContactFontCheckBox_        ->setChecked( account->getSettingBool( "FontContactForce"                 ) );
  useEmoticonsCheckBox_          ->setChecked( account->getSettingBool( "EmoticonsEnabled"                 ) );
  showWinksCheckBox_             ->setChecked( account->getSettingBool( "ChatShowWinks"                    ) );
  showTimeCheckbox_              ->setChecked( account->getSettingBool( "ChatTimestampEnabled"             ) );
  showDateCheckbox_              ->setChecked( account->getSettingBool( "ChatTimestampDateEnabled"         ) );
  showSecondsCheckbox_           ->setChecked( account->getSettingBool( "ChatTimestampEnabled"             ) );
  useFontEffectsCheckBox_        ->setChecked( account->getSettingBool( "FontEffectsEnabled"               ) );
  groupFollowupCheckbox_         ->setChecked( account->getSettingBool( "ChatGroupFollowUpMessagesEnabled" ) );
  shakeNudgeCheckBox_            ->setChecked( account->getSettingBool( "ChatShakeNudge"                   ) );
  enableChatFormattingCheckBox_  ->setChecked( account->getSettingBool( "ChatTextFormattingEnabled"        ) );
  displayChatUserPictureCheckBox_->setChecked( account->getSettingBool( "ChatUserDisplayPictureEnabled"    ) );

  // Select the correct chat style,
  int item = chatStyle_->findText( account->getSettingString( "ChatStyle" ), Qt::MatchExactly | Qt::MatchCaseSensitive );
  if( item == -1 )
  {
    kmWarning() << "Current chat style was not found, attempting to revert to the default setting.";
    item = chatStyle_->findText( "Default", Qt::MatchExactly | Qt::MatchCaseSensitive );
  }

  if( item != -1 )
  {
    chatStyle_->setCurrentIndex( item );
    slotChatStyleChanged( chatStyle_->currentText() );
  }

  // Also load contact font settings into the chatStyle object,
  // other settings are set in updatePreview()
  chatStyleTransform_->setContactFont( contactFont_ );
  chatStyleTransform_->setContactFontColor( contactColor_.name() );
}



/**
 * @brief Save account information from the chat style widget
 *
 * Calls the set-up methods from Account to change its properties, based on how
 * the user has set the widgets of this page.
 */
void ChatStylePage::saveSettings( Account *account )
{
  SettingsList settings;
  settings[ "FontContactForce"                 ] = useContactFontCheckBox_->isChecked();
  settings[ "EmoticonsEnabled"                 ] = useEmoticonsCheckBox_->isChecked();
  settings[ "ChatShowWinks"                    ] = showWinksCheckBox_->isChecked();
  settings[ "FontEffectsEnabled"               ] = useFontEffectsCheckBox_->isChecked();
  settings[ "ChatTextFormattingEnabled"        ] = enableChatFormattingCheckBox_->isChecked();
  settings[ "ChatShakeNudge"                   ] = shakeNudgeCheckBox_->isChecked();
  settings[ "ChatTimestampEnabled"             ] = showTimeCheckbox_->isChecked();
  settings[ "ChatTimestampDateEnabled"         ] = showDateCheckbox_->isChecked();
  settings[ "ChatTimestampSecondsEnabled"      ] = showSecondsCheckbox_->isChecked();
  settings[ "ChatGroupFollowUpMessagesEnabled" ] = groupFollowupCheckbox_->isChecked();
  settings[ "ChatTabbingMode"                  ] = tabbedChatMode_->currentIndex();
  settings[ "ChatStyle"                        ] = chatStyle_->currentText();
  settings[ "ChatUserDisplayPictureEnabled"    ] = displayChatUserPictureCheckBox_->isChecked();
  settings[ "FontUser"                         ] = userFont_;
  settings[ "FontUserColor"                    ] = userColor_.name();
  settings[ "FontContact"                      ] = contactFont_;
  settings[ "FontContactColor"                 ] = contactColor_.name();
  account->setSettings( settings );
}



// The chat style was changed.
void ChatStylePage::slotChatStyleChanged(const QString &style)
{
  // Update the chat style object
  chatStyleTransform_->setStyle( style );

  // Update the preview
  updatePreview();
}



// Update the contacts font color
void ChatStylePage::slotContactFontColorChanged( const QColor &color )
{
  contactColor_ = color;
}



// Update the user font color
void ChatStylePage::slotUserFontColorChanged( const QColor &color )
{
  userColor_ = color;
}



// Force the page onto a specific tab
void ChatStylePage::switchToTab( int tabIndex )
{
  if( ( tabIndex < 0 ) || tabIndex > tabWidget_->count() )
  {
    kmWarning() << "Invalid tab index" << tabIndex << "selected!";
    return;
  }

  tabWidget_->setCurrentIndex( tabIndex );
}



// Update the chat style preview
void ChatStylePage::updatePreview()
{
  if( chatStyleTransform_ == 0 || ! chatStyleTransform_->hasStyle() )
  {
    // Don't bother updating the preview for every event fired by the load...Settings() methods.
    return;
  }

  // Update the boolean settings (other settings are updated in their slots)
  chatStyleTransform_->setContactFont     ( contactFont_                               );
  chatStyleTransform_->setContactFontColor( contactColor_.name()                       );
  chatStyleTransform_->setShowTime        ( showTimeCheckbox_->isChecked()             );
  chatStyleTransform_->setShowDate        ( showDateCheckbox_->isChecked()             );
  chatStyleTransform_->setShowSeconds     ( showSecondsCheckbox_->isChecked()          );
  chatStyleTransform_->setUseContactFont  ( useContactFontCheckBox_->isChecked()       );
  chatStyleTransform_->setUseEmoticons    ( useEmoticonsCheckBox_->isChecked()         );
  chatStyleTransform_->setUseFontEffects  ( useFontEffectsCheckBox_->isChecked()       );
  chatStyleTransform_->setUseFormatting   ( enableChatFormattingCheckBox_->isChecked() );
  chatStyleTransform_->setGroupFollowupMessages( groupFollowupCheckbox_->isChecked()   );

  // Create three fake messages
#if 0
  ChatMessage fake1( ChatMessage::TYPE_INCOMING, ChatMessage::CONTENT_MESSAGE, true, i18n("Hi, how are you doing? :)"), "test@kmess.org",
                     i18n("Stacy") );
  ChatMessage fake2( ChatMessage::TYPE_OUTGOING, ChatMessage::CONTENT_MESSAGE, false, i18n("Great!"), previewHandle_,
                     previewUsername_, previewPicture_, userFont_, userColor_.name() );
  ChatMessage fake3( ChatMessage::TYPE_OUTGOING, ChatMessage::CONTENT_MESSAGE, false, i18n("I /just/ got back from my vacation in Italy!"), previewHandle_,
                     previewUsername_, previewPicture_, userFont_, userColor_.name() );
#endif

  PlaceholderContact *stacy = new PlaceholderContact( "stacy@stacy.com", i18n( "Stacy" ) );
  PlaceholderContact *fakeMe = new PlaceholderContact( previewHandle_, previewUsername_ );

  KMessMessage fake1( KMessMessage::IncomingText, 0, stacy );

  KMessMessage fake2( KMessMessage::OutgoingText, 0, globalSession->self() );
  KMessMessage fake3( KMessMessage::OutgoingText, 0, globalSession->self() );

  // need a fake peer if we're not logged in (since self() is NULL).
  if ( ! globalSession->isLoggedIn() )
  {
    fake2.setPeer( fakeMe );
    fake3.setPeer( fakeMe );
  }
  
  fake1.setMessage( "Hi, how are you doing? :)" );
  fake1.setDirection( KMess::DirectionIncoming );
  
  fake2.setMessage( "Great!" );
  fake3.setMessage( "I /just/ got back from my vacation in Italy!" );

  // Remove any old chat samples
  chatMessageView_->clearView( true );

  // Display the sample chat
  chatMessageView_->showMessage( fake1 );
  chatMessageView_->showMessage( fake2 );
  chatMessageView_->showMessage( fake3 );

  // bye bye stacy.
  delete stacy;
  delete fakeMe;
}



// Update the data shown in the message preview
void ChatStylePage::updatePreviewDetails( const QString &handle, const QString &name, const QString &picture )
{
  previewHandle_   = handle;
  previewPicture_  = picture;

  previewUsername_ = ( name.isEmpty() ? handle : name );
}



// Enable or disable the contact font and color selectors
void ChatStylePage::useContactFontToggled( bool checked )
{
  // Enable/disable buttons
  contactFontButton_->setEnabled( checked );
  contactFontColorButton_->setEnabled( checked );

  // Update preview
  chatStyleTransform_->setUseContactFont( checked );
  updatePreview();
}



// Open a dialog for KDE New Stuff Chat Themes
void ChatStylePage::getNewThemes()
{
  KNS::Engine engine( this );
  engine.init( "kmesschatstyles.knsrc" );
  KNS::Entry::List entries = engine.downloadDialogModal( this );

  loadStyleList();
}



#include "chatstylepage.moc"
