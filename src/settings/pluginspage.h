/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright 2010 Daniel E. Moctezuma <democtezuma@gmail.com>          *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

#ifndef PLUGINSPAGE_H
#define PLUGINSPAGE_H

#include <QListView>
#include <QWidget>

class Account;
class PluginsMaster;
class PluginsModel;

/**
  * @brief Allows to choose between any plugins available.
  *
  * @author Daniel E. Moctezuma
  * @ingroup Settings
  */
class PluginsPage : public QWidget
{
  Q_OBJECT

  public:
    // The constructor
                      PluginsPage( QWidget* parent = 0 );
    // The destructor
                     ~PluginsPage();

    // Load the settings of the dialog
    void              loadSettings( Account *account );
    // Save the settings to the account object
    void              saveSettings( Account *account );

  private:
    PluginsMaster*    pluginsMaster_;
    PluginsModel*     pluginsModel_;
    QListView*        view_;
};

#endif
