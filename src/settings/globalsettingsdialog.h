/***************************************************************************
                          globalsettingsdialog.h  -  description
                             -------------------
    begin                : Sat Apr 3 2008
    copyright            : (C) 2008 by Valerio Pilo
    email                : valerio@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef GLOBALSETTINGSDIALOG_H
#define GLOBALSETTINGSDIALOG_H

#include <KNotifyConfigWidget>
#include <KPageDialog>


// Forward declarations
class AccountsManagerPage;
class FileTransferSettingsPage;
class MiscellaneousPage;

class KConfigGroup;



/**
 * @author Valerio Pilo <valerio@kmess.org>
 * @ingroup Settings
 */
class GlobalSettingsDialog : public KPageDialog
{
  Q_OBJECT

  public:
    // The constructor
                                 GlobalSettingsDialog( QWidget *parent = 0 );
    // The destructor
                                ~GlobalSettingsDialog();
    // Return a singleton instance of the settings dialog window
    static GlobalSettingsDialog *instance( QWidget *parent = 0 );

  private slots: // Private slots
    // Save the window options before closing.
    void                         closeEvent( QCloseEvent *event );
    // Load the settings to all tabs
    void                         loadSettings();
    // Save the settings from all tabs
    bool                         saveSettings();
    // A button has been pressed, act accordingly
    void                         slotButtonClicked( int button );

  private: // Private attributes
    // The configuration group from which window size is loaded from and saved to
    KConfigGroup                 config_;
    // The page where to manage the saved MSN accounts
    AccountsManagerPage         *accountsManagerPage_;
    // The page where to manage file transfer settings
    FileTransferSettingsPage    *fileTransferSettingsPage_;
    // The page where to manage the miscellaneous settings
    MiscellaneousPage           *miscellaneousPage_;
    // The page where to manage the notification settings
    KNotifyConfigWidget         *notificationPage_;
    // The instance of the singleton settings dialog window
    static GlobalSettingsDialog *instance_;

  signals:
    // The settings have been changed
    void                         changedGlobalSettings();

};

#endif
