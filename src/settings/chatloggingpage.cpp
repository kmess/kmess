/***************************************************************************
                          chatloggingpage.cpp -  description
                             -------------------
    begin                : Thu Jan 30 2008
    copyright            : (C) 2008 by Diederik van der Boor
    email                : "vdboor" --at-- "codingdomain.com"
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "chatloggingpage.h"

#include "../account.h"

#include <KFileDialog>



/**
 * The constructor
 */
ChatLoggingPage::ChatLoggingPage( QWidget *parent )
: QWidget( parent )
, Ui::ChatLoggingPage()
{
  setupUi( this );

  // Connect our signals
  connect( chatSavePathButton_, SIGNAL(         clicked()     ),
           this,                SLOT  ( chooseDirectory()     ) );

  // Disallow to directly edit the chat logs saving path
  chatSavePathEdit_->setReadOnly( true );
}



/**
 * @brief Get a directory from the user
 *
 * Asks the user for a directory and sets the path widget with the chosen folder
 */
void ChatLoggingPage::chooseDirectory()
{
  // Let the user choose a folder, starting from the previously set path (if any)
  const QString& dir( KFileDialog::getExistingDirectory( chatSavePathEdit_->text() ) );

  if( ! dir.isEmpty() )
  {
    chatSavePathEdit_->setText( dir );
  }
}



/**
 * @brief Load the widget state from an account
 *
 * Reads an Account's info and sets our internal widgets accordingly
 *
 * @param account The account where to pick settings from
 */
void ChatLoggingPage::loadSettings( const Account *account )
{
  logChatsCheckBox_       ->setChecked( account->getSettingBool( "LoggingEnabled" )       );
  saveChatsToFileCheckBox_->setChecked( account->getSettingBool( "LoggingToFileEnabled" ) );

  switch( account->getSettingInt( "LoggingToFileFormat" ) )
  {
    case Account::EXPORT_TEXT: fileFormatComboBox_->setCurrentIndex( 1 ); break;
    case Account::EXPORT_HTML:
    default:                   fileFormatComboBox_->setCurrentIndex( 0 ); break;
  }

  chatSavePathEdit_->setText( account->getSettingString( "LoggingToFilePath" ) );

  switch( account->getSettingInt( "LoggingToFileStructure" ) )
  {
    case Account::SINGLEDIRECTORY: singleDirectoryRadioButton_->setChecked( true ); break;
    case Account::BYYEAR:          yearRadioButton_           ->setChecked( true ); break;
    case Account::BYMONTH:         monthRadioButton_          ->setChecked( true ); break;
    case Account::BYDAY:           dayRadioButton_            ->setChecked( true ); break;
  }
}



/**
 * @brief Save account information from the chat logging widget
 *
 * Calls the set-up methods from Account to change its properties, based on how
 * the user has set the widgets of this page.
 */
void ChatLoggingPage::saveSettings( Account *account )
{
  int directoryStructure = Account::BYMONTH;

  if( singleDirectoryRadioButton_->isChecked() )
  {
    directoryStructure = Account::SINGLEDIRECTORY;
  }
  else if( yearRadioButton_->isChecked() )
  {
    directoryStructure = Account::BYYEAR;
  }
  else if( monthRadioButton_->isChecked() )
  {
    directoryStructure = Account::BYMONTH;
  }
  else if( dayRadioButton_->isChecked() )
  {
    directoryStructure = Account::BYDAY;
  }

  Account::ChatExportFormat format;
  switch( fileFormatComboBox_->currentIndex() )
  {
    case 1:  format = Account::EXPORT_TEXT; break;
    case 0:
    default: format = Account::EXPORT_HTML; break;
  }

  SettingsList settings;
  settings[ "LoggingEnabled"         ] = logChatsCheckBox_->isChecked();
  settings[ "LoggingToFileEnabled"   ] = saveChatsToFileCheckBox_->isChecked();
  settings[ "LoggingToFileFormat"    ] = format;
  settings[ "LoggingToFilePath"      ] = chatSavePathEdit_->text();
  settings[ "LoggingToFileStructure" ] = directoryStructure;
  account->setSettings( settings );
}


#include "chatloggingpage.moc"
