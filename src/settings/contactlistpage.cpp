/***************************************************************************
                          contactlistpage.cpp -  description
                             -------------------
    begin                : Sun Mar 1 2009
    copyright            : (C) 2009 by Andrea Decorte
    email                : "adecorte" --at-- "gmail.com"
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "contactlistpage.h"

#include "../account.h"
#include "../kmessdebug.h"



/**
 * The constructor
 */
ContactListPage::ContactListPage( QWidget *parent )
: QWidget( parent )
, Ui::ContactListPage()
{
  setupUi( this );
}



/**
 * @brief Load the widget state from an account
 *
 * Reads an Account's info and sets our internal widgets accordingly
 *
 * @param account The account where to pick settings from
 */
void ContactListPage::loadSettings( const Account *account )
{
  nowListeningCheckBox_        ->setChecked( account->getSettingBool( "NowListeningEnabled"              ) );
  showEmailInfoCheckBox_       ->setChecked( account->getSettingBool( "ContactListEmailCountShow"        ) );
  enableListFormattingCheckBox_->setChecked( account->getSettingBool( "ContactListTextFormattingEnabled" ) );
  showContactEmailCheckBox_    ->setChecked( account->getSettingBool( "ContactListEmailAsNamesShow"      ) );
  showBirdCheckBox_            ->setChecked( account->getSettingBool( "ContactListBackgroundEnabled"     ) );
}



/**
 * @brief Save account information from the alerts widget
 *
 * Calls the set-up methods from Account to change its properties, based on how
 * the user has set the widgets of this page.
 */
void ContactListPage::saveSettings( Account *account )
{
  SettingsList settings;
  settings[ "NowListeningEnabled"              ] = nowListeningCheckBox_->isChecked();
  settings[ "ContactListEmailAsNamesShow"      ] = showContactEmailCheckBox_->isChecked();
  settings[ "ContactListTextFormattingEnabled" ] = enableListFormattingCheckBox_->isChecked();
  settings[ "ContactListEmailCountShow"        ] = showEmailInfoCheckBox_->isChecked();
  settings[ "ContactListBackgroundEnabled"     ] = showBirdCheckBox_->isChecked();
  account->setSettings( settings );
}



#include "contactlistpage.moc"
