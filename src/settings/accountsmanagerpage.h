/***************************************************************************
                          accountsmanagerpage.h -  description
                             -------------------
    begin                : Sat May 3 2008
    copyright            : (C) 2008 by Valerio Pilo
    email                : valerio@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef ACCOUNTSMANAGERPAGE_H
#define ACCOUNTSMANAGERPAGE_H

#include "ui_accountsmanagerpage.h"

#include <QWidget>


// Forward declarations
class AccountsManager;



/**
 * Settings page for the various accounts.
 *
 * @author Valerio Pilo
 * @ingroup Settings
 */
class AccountsManagerPage : public QWidget, private Ui::AccountsManagerPage
{
  Q_OBJECT


  public:  // public methods

    // The constructor
                       AccountsManagerPage( QWidget *parent = 0 );
    // The destructor
    virtual           ~AccountsManagerPage();
    // Save the settings of the dialog
    void               saveSettings();



  public slots:

    // Open the currently selected account's settings
    void               configureAccount();
    // Delete the given account
    void               deleteAccount();
    // Load the settings of the dialog
    void               loadSettings();
    // Show the Account Settings dialog for a new account
    void               showAddAccountDialog();


  private slots:

    // Update the buttons status when accounts get selected
    void               accountSelected();


  private:  // private properties

    AccountsManager   *accountsManager_;



};

#endif

