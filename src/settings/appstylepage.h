/***************************************************************************
                           appstylepage.h -  description
                             -------------------
    begin                : Sat Aug 08 2009
    copyright            : (C) 2009 by Ruben Vandamme
    email                : vandammeru gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef APPSTYLEPAGE_H
#define APPSTYLEPAGE_H

#include "ui_appstylepage.h"

#include <QColor>
#include <QFont>
#include <QWidget>



// Forward class declarations
class Account;



/**
 * Settings page for the application styles.
 *
 * @ingroup Settings
 */
class AppStylePage : public QWidget, private Ui::AppStylePage
{
  Q_OBJECT

  public:
    // Constructor
                       AppStylePage( QWidget* parent = 0 );
    // Destructor
                      ~AppStylePage();
    // Load settings from an account.
    void               loadSettings( const Account *account );
    // Save settings to an account.
    void               saveSettings( Account *account );
    // Reset the live preview
    void               reset();
    
  private:
    // Update the style list.
    void               loadStyleList();

  private slots:
    // Update preview.
    void               updatePreview( QListWidgetItem * item );
    // Set/Unset previews.
    void               setPreviewEnabled( );
    
  private:
    // needed so we can reset the live preview.
    QString            currentStyle_;

};


#endif
