/***************************************************************************
                           appstylepage.cpp -  description
                             -------------------
    begin                : Sat Aug 08 2009
    copyright            : (C) 2009 by Ruben Vandamme
    email                : vandammeru gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "appstylepage.h"

#include "../account.h"
#include "../mainwindow.h"

#include <KDebug>
#include <KIcon>
#include <KStandardDirs>
#include <QDir>



#ifdef KMESSDEBUG_APPSTYLES
  #define KMESSDEBUG_APPSTYLES_SETTINGS
#endif



// Constructor
AppStylePage::AppStylePage( QWidget *parent )
: QWidget( parent )
, Ui::AppStylePage()
{
  setupUi( this );

  connect( appStylesList_,    SIGNAL(  itemClicked      ( QListWidgetItem * ) ),
           this,              SLOT  (  updatePreview    ( QListWidgetItem * ) ) );
  connect( livePreviewCheck_, SIGNAL(  stateChanged     ( int               ) ),
           this,              SLOT  (  setPreviewEnabled(                   ) ) );

  loadStyleList();
}



// Destructor
AppStylePage::~AppStylePage()
{
}



// Load settings from an account.
void AppStylePage::loadSettings( const Account *account )
{

  QString styleName = account->getSettingString( "ApplicationStyle" );

  currentStyle_ = styleName;
  
  // Highlight the selected style
  QListWidgetItem* item = 0;
  QList<QListWidgetItem*> items( appStylesList_->findItems( styleName, Qt::MatchExactly | Qt::MatchCaseSensitive ) );

  if( items.isEmpty() )
  {
    kmWarning() << "Current app style was not found, attempting to revert to the default setting.";
    items = appStylesList_->findItems( "KMess-Default", Qt::MatchExactly | Qt::MatchCaseSensitive );
  }
  else
  {
    item = items.first();
  }

  // If there is an item, select it
  if( item != 0 )
  {
    appStylesList_->setCurrentItem( item );
  }
  else
  {
    // select the first item otherwise
    appStylesList_->setCurrentRow( 0 );
  }
}



// Save settings to an account.
void AppStylePage::saveSettings( Account *account )
{
  QListWidgetItem* item = appStylesList_->currentItem();
  QString styleName = "KMess-Default";

  if( ! KMESS_NULL( item ) )
  {
     styleName = item->text();
  }

  account->setSetting( "ApplicationStyle", styleName );
  
  currentStyle_ = styleName;

  MainWindow::instance()->setApplicationStyle( currentStyle_ );
}



// reset the style back to normal.
void AppStylePage::reset()
{
  MainWindow::instance()->setApplicationStyle( currentStyle_ );
}



// Update the style list.
void AppStylePage::loadStyleList()
{
  appStylesList_->clear();

  // Browse all appstyle dirs, to get all appstyles
  const QStringList& themesDirs( KGlobal::dirs()->findDirs( "data", "kmess/appstyles" ) );

#ifdef KMESSDEBUG_APPSTYLES_SETTINGS
  kmDebug() << "App style dirs: " << themesDirs;
#endif

  QDir themesDir;
  foreach( const QString& themesPath, themesDirs )
  {
    // Browse for all themes in the emoticon dir
    themesDir.setPath( themesPath );

    // Grep all themes in current dir
    const QStringList& themes( themesDir.entryList( QDir::Files | QDir::NoDotAndDotDot, QDir::Name | QDir::IgnoreCase ) );

    foreach( const QString& themePath, themes )
    {
#ifdef KMESSDEBUG_APPSTYLES_SETTINGS
      kmDebug() << "Folder: " << themesPath << " - Theme: " << themePath;
#endif
      new QListWidgetItem( KIcon( "preferences-desktop-theme" ), themePath.split('.')[0], appStylesList_ );
    }

  }
}



// Update preview
void AppStylePage::updatePreview( QListWidgetItem * item )
{
  if( livePreviewCheck_->checkState() != Qt::Checked )
  {
    MainWindow::instance()->setApplicationStyle( currentStyle_ );
    return;
  }

  MainWindow::instance()->setApplicationStyle( item->text() );
}



// Set/Unset previews
void AppStylePage::setPreviewEnabled()
{
  updatePreview( appStylesList_->currentItem() );
}
#include "appstylepage.moc"
