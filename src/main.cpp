/***************************************************************************
                          main.cpp  -  description
                             -------------------
    begin                : Sun Jan  5 15:18:36 CST 2003
    copyright            : (C) 2003 by Mike K. Bennett
    email                : mkb137b@hotmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "utils/crashhandler.h"
#include "config-kmess.h"
#include "kmessapplication.h"
#include "kmessdebug.h"

#include <KAboutData>
#include <KCmdLineArgs>
#include <KLocalizedString>



/**
 * @brief Main starting point of KMess.
 * @param  argc  Number of arguments entered at the command line.
 * @param  argv  Array of command line arguments.
 * @returns  Exit code of the application.
 * @ingroup Root
 */
int main(int argc, char *argv[])
{
  Q_INIT_RESOURCE(isfqtresources);
  // Init about dialog.
  // Tab 1: General
  KAboutData aboutData( KMESS_NAME,                                // internal name
                        0,                                         // catalog name
                        ki18n("KMess"),                            // program name
                        KMESS_VERSION,                             // app version from config-kmess.h
                        ki18n("A Live Messenger client for KDE"),  // short description
                        KAboutData::License_GPL,                   // license
                        ki18n("(c) 2002-2010, Mike K. Bennett\n"   // copyright
                              "(c) 2005-2010, Diederik van der Boor\n"
                              "(c) 2007-2010, Valerio Pilo\n"
                              "(c) 2008-2010, Antonio Nastasi\n"
                              "(c) 2008-2010, Ruben Vandamme\n"
                              "(c) 2009-2010, Sjors Gielen\n"
                              "(c) 2009-2010, Adam Goossens\n"),
                        KLocalizedString(),
                        "http://www.kmess.org/",                   // home page
                        "bugs" "@" "kmess" "." "org"               // address for bugs
                      );

  // Note all email addresses are written in an anti-spam style.

  // Tab 2: Authors
  // TODO: escape the special characters to some Unicode format. (how?)
  aboutData.addAuthor( ki18n("Mike K. Bennett"),       ki18n("Developer and project founder"), "mkb137"     "@" "users.sourceforge" "." "net" );
  aboutData.addAuthor( ki18n("Michael Curtis"),        ki18n("Developer"),                     "mdcurtis"   "@" "users.sourceforge" "." "net" );
  aboutData.addAuthor( ki18n("Jan Tönjes"),            ki18n("Project support"),               "jan"        "@" "kmess" "." "org" );
  aboutData.addAuthor( ki18n("Diederik van der Boor"), ki18n("Current developer"),             "diederik"   "@" "kmess" "." "org" );
  aboutData.addAuthor( ki18n("Valerio Pilo"),          ki18n("Current developer"),             "valerio"    "@" "kmess" "." "org" );
  aboutData.addAuthor( ki18n("Antonio Nastasi"),       ki18n("Current developer"),             "sifcenter"  "@" "gmail" "." "com" );
  aboutData.addAuthor( ki18n("Ruben Vandamme"),        ki18n("Current developer"),             "vandammeru" "@" "gmail" "." "com" );
  aboutData.addAuthor( ki18n("Sjors Gielen"),          ki18n("Current developer"),             "sjors"      "@" "kmess" "." "com" );
  aboutData.addAuthor( ki18n("Adam Goossens"),         ki18n("Current developer"),             "fontknocker""@" "gmail" "." "com" );

  // Tab 3: Credits
  aboutData.addCredit( ki18n("Jan Tönjes"),                ki18n("German translation, testing, documentation, web master, project management, etc..."), "jan" "." "toenjes" "@" "web" "." "de");
  aboutData.addCredit( ki18n("Dane Harnett"),              ki18n("Web design"),                     "dynamitedane" "@" "hotmail" "." "com");
  aboutData.addCredit( ki18n("David Vignoni"),             ki18n("Main and yellow/blue/violet emoticon sets, Italian translation"), "dvgn" "@" "libero" "." "it");
  aboutData.addCredit( ki18n("Julien Joubin"),             ki18n("Cartoon emoticons"),               "jujubinche" "@" "netscape" "." "net");
  aboutData.addCredit( ki18n("Christian Müller"),          ki18n("Default sound theme"),             "cmue81" "@" "gmx" "." "de");
  aboutData.addCredit( ki18n("Michael Anderton"),          ki18n("KMess icon in Oxygen style"),      "mike" "." "s" "." "anderton" "@" "gmail" "." "com");

  // Translations
  // Sorted by Alphabetic order of language.
  aboutData.addCredit( ki18n("Panagiotis Papadopoulos"),   ki18n("Translations Maintainer"), "pano_90" "@" "gmx" "." "net");

  aboutData.addCredit( ki18n("Mohamed Aser"),              ki18n("Arabic translation, internationalization of file saving fix" "." ""), "mohasr" "@" "link" "." "net");
  aboutData.addCredit( ki18n("Youssef Chahibi"),           ki18n("More Arabic translation"),         "chahibi" "@" "gmail" "." "com");

  aboutData.addCredit( ki18n("Mauricio Rother"),           ki18n("Brazilian Portuguese translation"), "mauricio" "@" "digicomm" "." "com.br");
  aboutData.addCredit( ki18n("Leonel Freire"),             ki18n("More Brazilian Portuguese translation"), "leonelfreire" "@" "gmail" "." "com");
  aboutData.addCredit( ki18n("Sergio Rafael Lemke"),       ki18n("More Brazilian Portuguese translation"), "bedi" "." "com" "@" "gmail" "." "com");
  aboutData.addCredit( ki18n("Morris Arozi Moraes"),     ki18n("More Brazilian Portuguese translation"));

  aboutData.addCredit( ki18n("Jaume Cornadó"),             ki18n("Catalan translation"),              "jaumec" "@" "lleida" "." "net");
  aboutData.addCredit( ki18n("Adrià Arrufat"),             ki18n("More Catalan translation"),              "swiftscythe" "@" "gmail" "." "com");

  aboutData.addCredit( ki18n("Lin Haoxiang"),              ki18n("Simplified Chinese translation, file send bug fix, proxy connect code"), "linhaoxiang" "@" "hotmail" "." "com");
  aboutData.addCredit( ki18n("Liu Sizhuang"),              ki18n("More Simplified Chinese translation"),  "chinatslsz" "@" "hotmail.com");
  aboutData.addCredit( ki18n("Cheng Yang"),                ki18n("More Simplified Chinese translation"),  "yangzju" "@" "gmail.com");
  aboutData.addCredit( ki18n("Yen-chou Chen"),             ki18n("Traditional Chinese translation"),  "yenchou" "." "mse90" "@" "nctu" "." "edu" "." "tw");
  aboutData.addCredit( ki18n("Tryneeds-Chinese"),          ki18n("More Traditional Chinese translation"), "tryneeds@gmail.com");

  aboutData.addCredit( ki18n("Lars Sommer"),               ki18n("Danish translation"),               "admin" "@" "lasg" "." "dk");
  aboutData.addCredit( ki18n("Pascal d'Hermilly"),         ki18n("More Danish translation"),          "pascal" "@" "tipisoft" "." "dk");

  aboutData.addCredit( ki18n("Arend van Beelen Jr."),      ki18n("Dutch translation"),               "arend" "@" "auton" "." "nl");
  aboutData.addCredit( ki18n("Diederik van der Boor"),     ki18n("More Dutch translation"),          "diederik" "@" "kmess" "." "org");
  aboutData.addCredit( ki18n("Jaap Woldringh"),            ki18n("More Dutch translation"),          "jjh" "." "woldringh" "@" "planet" "." "nl");
  aboutData.addCredit( ki18n("Elve"),                      ki18n("More Dutch translation"),          "elve" "@" "savage-elve" "." "net");
  aboutData.addCredit( ki18n("Sander Pientka"),            ki18n("More Dutch translation"),          "cumulus0007" "@" "gmail" "." "com");
  aboutData.addCredit( ki18n("Heimen Stoffels"),           ki18n("More Dutch translation"),          "djmusic121" "@" "gmail" "." "com");

  aboutData.addCredit( ki18n("Panagiotis Papadopoulos"),   ki18n("More German translation, Greek translation"), "pano_90" "@" "gmx" "." "net");
  aboutData.addCredit( ki18n("Dimitrios Glentadakis"),     ki18n("More Greek translation"),    "dglent" "@" "gmail" "." "com");

  aboutData.addCredit( ki18n("Jyri Toomessoo"),            ki18n("Estonian translation"),             "nuubik" "@" "hotmail" "." "com");
  aboutData.addCredit( ki18n("Markus Vuori"),              ki18n("Finnish translation"),              "markus" "@" "vuoret" "." "net");
  aboutData.addCredit( ki18n("Joonas Niilola"),            ki18n("More Finnish translation"),         "juippis" "@" "roskakori" "." "org");
  aboutData.addCredit( ki18n("Jussi Timperi"),             ki18n("More Finnish translation"),         "jussi.timperi" "@" "gmail" "." "com");
  aboutData.addCredit( ki18n("Antony Hussi"),              ki18n("More Finnish translation"),         "antony.hussi" "@" "tranciend" "." "com");

  aboutData.addCredit( ki18n("Choplair"),                  ki18n("French translation"),              "pachilor" "@" "yahoo" "." "co" "." "jp");
  aboutData.addCredit( ki18n("Vincent Fretin"),            ki18n("More French translation, MSN6 emoticon definitions"), "fretinvincent" "@" "hotmail" "." "com");
  aboutData.addCredit( ki18n("Andrea Blankenstijn"),       ki18n("More French translation"),         "darkan9el" "@" "gmail" "." "com" ); // or "andrea" "@" "zenephiris" "." "ch"
  aboutData.addCredit( ki18n("Barthe Guillaume"),          ki18n("More French translation"),         "gu_barthe" "@" "yahoo" "." "fr" );
  aboutData.addCredit( ki18n("Scias"),                     ki18n("More French translation"),         "shining" "." "scias" "@" "gmail" "." "com" );
  aboutData.addCredit( ki18n("Émeric Dupont"),             ki18n("More French translation"),         "emeric" "." "dupont" "@" "agol" "." "org" );

  aboutData.addCredit( ki18n("Páder Rezső"),               ki18n("Hungarian translation"),            "rezso" "@" "rezso" "." "net");
  aboutData.addCredit( ki18n("Pauli Henrik"),              ki18n("More Hungarian translation"),      "henrik.pauli" "@" "drangolin" "." "net");
  aboutData.addCredit( ki18n("Valerio Pilo"),              ki18n("More Italian translation"),        "valerio" "@" "kmess" "." "org");
  aboutData.addCredit( ki18n("Vincenzo Reale"),            ki18n("More Italian translation"),        "smart2128" "@" "baslug" "." "org");
  aboutData.addCredit( ki18n("Andrea Decorte"),            ki18n("More Italian translation, Group selection in 'contact added user' dialog"), "adecorte" "@" "gmail" "." "com");
  aboutData.addCredit( ki18n("Daniel E. Moctezuma"),       ki18n("Japanese translation"),            "dmoctezuma" "@" "kmess" "." "org");
  aboutData.addCredit( ki18n("Park Dong Cheon"),           ki18n("Korean translation"),              "pdc" "@" "kaist" "." "ac.kr");
  aboutData.addCredit( ki18n("Øyvind Sæther"),             ki18n("Norsk Bokmål translation"),         "oyvind" "@" "sather" "." "tk");

  aboutData.addCredit( ki18n("Zoran Milovanović"),         ki18n("Serbian translation"),            "provalisam" "@" "gmail" "." "com");

  aboutData.addCredit( ki18n("Rastislav Krupanský"),       ki18n("Slovak translation"),              "ra100" "@" "atlas" "." "sk");
  aboutData.addCredit( ki18n("Matjaž Kaše"),               ki18n("Slovenian translation"),          "matjaz" "." "kase" "@" "g-kabel" "." "si");

  aboutData.addCredit( ki18n("Johanna Gersch"),            ki18n("Spanish translation"));
  aboutData.addCredit( ki18n("J.C.A. Javi"),               ki18n("More Spanish translation"),        "yovoya30ks" "@" "hotmail" "." "com");
  aboutData.addCredit( ki18n("Alejandro Araiza Alvarado"), ki18n("More Spanish translation"),        "mebrelith" "@" "gmail" "." "com");
  aboutData.addCredit( ki18n("Jaume Corbí"),               ki18n("More Spanish translation"),        "jaume4" "@" "gmail" "." "com");
  aboutData.addCredit( ki18n("Christian Kaiser"),          ki18n("More Spanish translation"),        "k39" "@" "users" "." "sourceforge" "." "net");
  aboutData.addCredit( ki18n("Juan Pablo González Tognarelli"), ki18n("More Spanish translation"),   "jotapesan" "@" "gmail" "." "com" );
  aboutData.addCredit( ki18n("Alexis Daniel Medina Medina"), ki18n("More Spanish translation"),        "alexismedina" "@" "gmail" "." "com");
  aboutData.addCredit( ki18n("Manuel Ramírez"),            ki18n("More Spanish translation"),        "elpreto" "@" "kde" "." "org" "." "ar");
  aboutData.addCredit( ki18n("Mauricio Muñoz Lucero"),     ki18n("More Spanish translation"),        "real" "." "mml" "@" "gmail" "." "com");

  aboutData.addCredit( ki18n("Christian Lundgren"),        ki18n("Swedish translation"),            "zeflunk" "@" "gmail" "." "com");
  aboutData.addCredit( ki18n("Mattias Newzella"),          ki18n("More Swedish translation"),       "newzella" "@" "gmail" "." "com");

  aboutData.addCredit( ki18n("Rachan Hongpairote"),        ki18n("Thai translation"),                 "rachanh" "@" "yahoo" "." "com");
  aboutData.addCredit( ki18n("Gorkem Cetin"),              ki18n("Turkish translation"),            "gorkem" "@" "gelecek" "." "com" "." "tr");
  aboutData.addCredit( ki18n("Barbaros Ulutas"),           ki18n("More Turkish translation"),        "ulutas" "@" "gmail" "." "com");
  aboutData.addCredit( ki18n("Uğur Çetin"),                ki18n("More Turkish translation"),        "ugur" "." "jnmbk" "@" "gmail" "." "com");

  // Other contributors
  aboutData.addCredit( ki18n("Richard Conway"),            ki18n("MSNP12 support, various patches"), "richardconway" "@" "users" "." "sourceforge.net");
  aboutData.addCredit( ki18n("Guido Solinas"),             ki18n("Pictures in contact list code, contact client info, chat font zoom"), "whereismwhite" "@" "gmail" "." "com");
  aboutData.addCredit( ki18n("Pedro Ferreira"),            ki18n("File transfer thumbnails"),        "pedro." "ferreira" "@" "fe" "." "up.pt");
  aboutData.addCredit( ki18n("Liu Sizhuang"),              ki18n("P4-Context field support"),        "chinatslsz" "@" "hotmail.com");
  aboutData.addCredit( ki18n("Scott Morgan"),              ki18n("Xinerama fixes"),                  "blumf" "@" "blumf" "." "freeserve" "." "co" "." "uk");
  aboutData.addCredit( ki18n("Laurence Anderson"),         ki18n("Original file receive code"),      "l.d" "." "anderson" "@" "warwick" ".ac.uk");
  aboutData.addCredit( ki18n("Matteo Nardi"),              ki18n("KWallet support"),                 "91.matteo" "@" "gmail" "." "com");
  aboutData.addCredit( ki18n("Adam Goossens"),             ki18n("Notifications blocking option, winks disabling option, last message date feature"), "fontknocker" "@" "gmail" "." "com");
  aboutData.addCredit( ki18n("Sjors Gielen"),              ki18n("IRC-like commands in the chat window"), "dazjorz" "@" "dazjorz" "." "com");
  aboutData.addCredit( ki18n("Dario Freddi"),              ki18n("Chat history dialog"),             "drf_av" "@" "users" "." "sourceforge" "." "net");
  // Lin Haoxiang (file send bug fix, proxy connect code) already above
  // Mohamed Aser (internationalization of file saving fix) already above
  aboutData.addCredit( ki18n("Alexandre Peixoto Ferreira"), ki18n("Various internationalization fixes"), "alexandref" "@" "o2" ".net" ".br");
  aboutData.addCredit( ki18n("Choe Hwanjin"),              ki18n("Various internationalization fixes" "." ""), "hjchoe" "@" "hancom" "." "com");
  // Vincent Fretin (MSN6 emoticon definitions) already above
  aboutData.addCredit( ki18n("Damien Sandras"),            ki18n("GnomeMeeting developer"),          "dsandras" "@" "seconix" "." "com");
  aboutData.addCredit( ki18n("Tobias Tönjes"),             ki18n("Guy with a bag over his head"),    "");
  aboutData.addCredit( ki18n("Camille Begue"),             ki18n("Chat History functionality when disconnected, autologin checkbox on login screen"),    "prsieux" "@" "@gmail" "." "com");
  aboutData.addCredit( ki18n("David López"),               ki18n("Nudge button in chat"),            "grannost" "@" "gmail" "." "com" );
  aboutData.addCredit( ki18n("Pieterjan Camerlynck"),      ki18n("Roaming Service support"),         "pieterjan" "." "camerlynck" "@" "gmail" "." "com" );
  aboutData.addCredit( ki18n("Anastasios Bourazanis"),     ki18n("Emoticon preview in settings page"), "");
  aboutData.addCredit( ki18n("Timo Tambet"),               ki18n("contact added user dialog tabbing"), "ttambet" "@" "gmail" "." "com" );

  // Other apps
  aboutData.addCredit( ki18n("KMerlin (kmerlin.olsd.de)"), ki18n("Inspiration and assorted code"));
  aboutData.addCredit( ki18n("Kopete (kopete.kde.org)"),   ki18n("Old popup balloons code, initial p2p code, MSN challenge handler, Yahoo and MSN protocol icons"));
  aboutData.addCredit( ki18n("KScreensaver"),              ki18n("Idle timer code"));
  aboutData.addCredit( ki18n("BasKet"),                    ki18n("Close-to-tray icon screenshot code"));
  aboutData.addCredit( ki18n("Amarok"),                    ki18n("Custom crash handler implementation, system tray icon overlay implementation"));
  aboutData.addCredit( ki18n("Quassel"),                   ki18n("KNotify not giving focus bug fix and KWin focus stealing prevention workaround"));

  // Nice community detail (very subtle at the bottom..)
  aboutData.addCredit( ki18n("Your name here?"), ki18n("You are welcome to send bugfixes and patches to the KMess help forum!\nIf you feel your name is missing here, please contact us too!"), "you@kmess.org");

  // Add the translation names from the .po file:
  aboutData.setTranslator( ki18nc("NAME OF TRANSLATORS", "Your names"),
                           ki18nc("EMAIL OF TRANSLATORS", "Your email addresses") );


  // Configure command line options
  // Those are handled in the KMessApplication class.
  KCmdLineOptions options;
  options.add( "hidden",
               ki18n("Do not show the contact list window initially") );
  options.add( "unique",
               ki18n("Do not allow multiple KMess instances to run at the same time") );
  options.add( "autologin <email>",
               ki18n("Autologin with the given email address") );
  options.add( "iKnowThatTrunkIsBroken",
               ki18n("If you are conscious that trunk DOES NOT WORK and you wish to use it anyways,\n"
                     "use this option, completely at your own risk. Be also aware that you may lose\n"
                     "your settings.") );

  // For the developer build,
  // make sure we can run tests fast.
#if ( KMESS_DEBUG == 1 )
#ifdef __GNUC__
#warning --runtest is disabled for now (KMessTest does not work)
#endif
  //options.add( "runtest <test>",
  //             ki18n("Run a debug test (developer build only)") );
  options.add( "server <address>",
               ki18n("Connect to the specified server instead of the official Live server.\n"
                     "Use \"localhost\" or \"127.0.0.1\" to connect to a local KMess Test Server.") );
#endif


  // Initialize the crash handler first.
  if( argc > 0 )
  {
    CrashHandler::setAppName( QLatin1String( argv[0] ) );
  }

  // Parse command line
  // Also pass about data for the arguments help dialog.
  KCmdLineArgs::init( argc, argv, &aboutData );
  KCmdLineArgs::addCmdLineOptions( options );
  KUniqueApplication::addCmdLineOptions();

  // Only allow one running instance if requested
  KUniqueApplication::StartFlag startupFlag;
  KCmdLineArgs *args = KCmdLineArgs::parsedArgs();

  startupFlag = ( ! args->isSet( "unique" ) )
                  ? KUniqueApplication::NonUniqueInstance
                  : KUniqueApplication::StartFlag( 0 );

  if( ! KUniqueApplication::start( startupFlag ) )
  {
    fprintf( stderr, ki18n("KMess is already running!\n").toString().toUtf8().constData(), 0 );
    return 0;
  }

  // Create the KUniqueApplication object
  // and start the event loop
  KMessApplication kmessApp;

  return kmessApp.exec();
}
