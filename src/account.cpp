/***************************************************************************
                          account.cpp  -  description
                             -------------------
    begin                : Sun Jan 5 2003
    copyright            : (C) 2003 by Mike K. Bennett
    email                : mkb137b@hotmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "account.h"

#include "utils/kmessconfig.h"
#include "accountsmanager.h"
#include "kmessglobal.h"
#include "kmessdebug.h"

#include <QDir>
#include <QRegExp>

#include <KConfigGroup>

#include <KGlobal>
#include <KLocale>
#include <KStandardDirs>


#ifdef KMESSDEBUG_ACCOUNT
  #define KMESSDEBUG_ACCOUNT_GENERAL
//   #define KMESSDEBUG_ACCOUNT_DIRTY
//   #define KMESSDEBUG_ACCOUNT_SETTINGS
#endif



// Initialize the default settings list
SettingsList Account::settingsDefaults_;

Account *Account::connectedAccount(0);

// The constructor
Account::Account()
:
  dirty_(false),

  friendlyName_( i18n("Unknown name"), false ), // Do not parse this name, connected account is not ready when initializing
  // TODO: Maybe we should make this "" (empty), and check for that when
  // retrieving the friendly name, returning the handle if it is empty.
  guestAccount_(true),
  handle_( "unknown_handle" )
{
  QFont defaultChatFont;
  defaultChatFont.setFamily( "Sans Serif" );
  defaultChatFont.setBold( false );
  defaultChatFont.setItalic( false );
  defaultChatFont.setUnderline( false );
  defaultChatFont.setPointSize( 12 );

  connect( globalSession->self(), SIGNAL(    displayNameChanged() ),
           this,          SLOT(      friendlyNameChanged() ) );
  connect( globalSession->self(), SIGNAL( personalMessageChanged() ),
           this,          SLOT(   personalMessageChanged() ) );

  // Initialize the default settings
  if( settingsDefaults_.isEmpty() )
  {
    // The application style
    settingsDefaults_[ "ApplicationStyle"                  ] = "KMess-Default";
    // Whether or not the user wants to go idle after a certain time
    settingsDefaults_[ "AutoIdleEnabled"                   ] = true;
    // The number of minutes of inactivity before the user status is changed to idle
    settingsDefaults_[ "AutoIdleTime"                      ] = 5;
    // The message used to automatically reply to messages when the user is away
    settingsDefaults_[ "AutoReplyMessage"                  ] = i18n( "I am away from my computer" );
    // Whether or not to enable MSN Plus formatting in chat
    settingsDefaults_[ "ChatTextFormattingEnabled"         ] = true;
    // Whether follow-up messages from the contact should be grouped
    settingsDefaults_[ "ChatGroupFollowUpMessagesEnabled"  ] = true;
    // Whether or not to show join/part messages in chat
    settingsDefaults_[ "ChatSessionMessagesEnabled"        ] = false;
    // Whether the chat window shakes for nudges
    settingsDefaults_[ "ChatShakeNudge"                    ] = true;
    // whether to show winks
    settingsDefaults_[ "ChatShowWinks"                     ] = true;
    // The chat style
    settingsDefaults_[ "ChatStyle"                         ] = "Fresh";
    // Whether or not chats should be tabbed in one window
    settingsDefaults_[ "ChatTabbingMode"                   ] = 0;
    // Whether or not time information is shown for chat messages
    settingsDefaults_[ "ChatTimestampEnabled"              ] = true;
    // Whether to show date in message timestamps
    settingsDefaults_[ "ChatTimestampDateEnabled"          ] = false;
    // Whether to show seconds in message timestamps
    settingsDefaults_[ "ChatTimestampSecondsEnabled"       ] = false;
    // Whether or not to show our display picture in chat
    settingsDefaults_[ "ChatUserDisplayPictureEnabled"     ] = false;
    // Whether or not allowed contacts should be visible
    settingsDefaults_[ "ContactListAllowedGroupShow"       ] = false;
    // Whether to show the contact list background image
    settingsDefaults_[ "ContactListBackgroundEnabled"      ] = true;
    // Whether contacts should be shown by group, by online/offline status, or mixed
    settingsDefaults_[ "ContactListDisplayMode"            ] = VIEW_MIXED;
    // Whether or not to show the email of contacts in the contact list instead of their friendly name
    settingsDefaults_[ "ContactListEmailAsNamesShow"       ] = false;
    // Whether or not to show any email notifications (the user may not have a hotmail account)
    settingsDefaults_[ "ContactListEmailCountShow"         ] = true;
    // Whether or not to display empty groups in the contact list
    settingsDefaults_[ "ContactListEmptyGroupsShow"        ] = true;
    // Whether or not to enable MSN Plus formatting in the contact list
    settingsDefaults_[ "ContactListTextFormattingEnabled"  ] = true;
    // Whether or not offline contacts are shown
    settingsDefaults_[ "ContactListOfflineGroupShow"       ] = true;
    // Dimensions of the display pictures in contact list
    settingsDefaults_[ "ContactListPictureSize"            ] = 32;
    // Whether or not removed (or reverse) contacts should be visible
    settingsDefaults_[ "ContactListRemovedGroupShow"       ] = false;
    // Whether or not to force alphabetical sorting.
    settingsDefaults_[ "ContactListSortAlphabetical"       ] = false;
    // Whether or not to send our display picture
    settingsDefaults_[ "DisplayPictureEnabled"             ] = true;
    // The path of the current picture
    settingsDefaults_[ "DisplayPicturePath"                ] = QString();
    // Whether or not emoticons should be shown
    settingsDefaults_[ "EmoticonsEnabled"                  ] = true;
    // The theme currently used for emoticons
    settingsDefaults_[ "EmoticonsTheme"                    ] = "KMess-new";
    // The font used to overridden the contact messages' font
    settingsDefaults_[ "FontContact"                       ] = defaultChatFont;
    // The font color used to overridden the contact messages' font
    settingsDefaults_[ "FontContactColor"                  ] = "#000000";
    // Whether or not the fonts used by the contacts should be ignored and overridden
    settingsDefaults_[ "FontContactForce"                  ] = false;
    // Whether or not to use bold/italic/underline effects in chats
    settingsDefaults_[ "FontEffectsEnabled"                ] = true;
    // The user's message font
    settingsDefaults_[ "FontUser"                          ] = defaultChatFont;
    // The user's message font color
    settingsDefaults_[ "FontUserColor"                     ] = "#000000";
    // Whether or not to save chat logs
    settingsDefaults_[ "LoggingEnabled"                    ] = false;
    // The format to use to save external chat log files
    settingsDefaults_[ "LoggingToFileFormat"               ] = EXPORT_HTML;
    // Whether saved chat logs are organized by year, month, day, or all in a single directory
    settingsDefaults_[ "LoggingToFileStructure"            ] = BYMONTH;
    // Whether or not to save chat files to external files
    settingsDefaults_[ "LoggingToFileEnabled"              ] = false;
    // The path where external chat log files are automatically saved
    settingsDefaults_[ "LoggingToFilePath"                 ] = QDir::home().absolutePath();
    // How long in seconds a notification balloon is shown
    settingsDefaults_[ "NotificationsDuration"             ] = 10;
    // Whether to hide popup notifications when the status is 'Busy'
    settingsDefaults_[ "NotificationsHideWhenBusy"         ] = true;
    // Whether the now listening feature should be retrieved and shown or not
    settingsDefaults_[ "NowListeningEnabled"               ] = true;
    // The status to set upon connection
    settingsDefaults_[ "StatusInitial"                     ] = Status::getCode( KMess::OnlineStatus );
    // Whether or not to show the history box
    settingsDefaults_[ "UIHistoryBoxShow"                  ] = false;
    // Whether or not to show the search bar
    settingsDefaults_[ "UISearchBarShow"                   ] = true;
    // Whether the welcome dialog has been shown (or finished) for this account yet
    settingsDefaults_[ "WelcomeDialogShown"                ] = false;
  }
}



// The destructor
Account::~Account()
{
  if( Account::connectedAccount == this )
  {
    kmWarning() << "About to destroy account " << handle_ << " but it is also set as the connectedAccount! KMess will most likely crash later because of this!";
  }

#ifdef KMESSDEBUG_ACCOUNT_GENERAL
  kmDebug() << "DESTROYED account" << handle_;
#endif
}



/**
 * Ensure a setting exists, or crash otherwise.
 *
 * If it doesn't, this method crashes. After calling
 * this method, it's safe to use the setting name.
 *
 * This is a drastic measure to avoid spelling errors in setting names going unnoticed.
 *
 * This method takes a const char* explicitly, because any crashes will make
 * the value of 'name' show up in gdb backtraces, along with what method
 * actually made the request.
 *
 * @param name  Setting name to verify
 */
void Account::ensureSettingExists( const char *name ) const
{
  // There's something wrong if an invalid setting is asked for - quit
  if( ! settingsDefaults_.contains( name ) )
  {
    kBacktrace();
    kmWarning() << "Tried to access an incorrect setting name:" << name;
    abort();
  }
}



// Return the user's friendlyName
QString Account::getFriendlyName( FormattingMode mode ) const
{
  return friendlyName_.getString( mode );
}



// Return the user's handle
const QString & Account::getHandle() const
{
  return handle_;
}



// Return the user's password
const QString & Account::getPassword() const
{
  return password_;
}



// Return the personal message
QString Account::getPersonalMessage( FormattingMode mode ) const
{
  return personalMessage_.getString( mode );
}



// Return the path to the current display picture
const QString Account::getDisplayPicture() const
{
  // Return the picture path if the user has selected to show it; else return an empty string
  if( ! getSettingBool( "DisplayPictureEnabled" ) )
  {
    return QString();
  }

  // if the user has selected a custom picture, it will be located in
  // ~/.kde/share/apps/kmess/<account@handle>/displaypics/<pictureHash>.png

  // If the file can't be found, fallback to the default picture
  const QString &displayPicturePath( getSettingString( "DisplayPicturePath" ) );
  if( displayPicturePath.isEmpty() || ! QFile::exists( displayPicturePath ) )
  {
    return KGlobal::dirs()->findResource( "data", "kmess/pics/kmesspic.png" );
  }

  return displayPicturePath;
}



/**
 * Retrieve a setting's value.
 *
 * @param name   Name of the setting to get
 */
const QVariant Account::getSetting( const QString &name ) const
{
  ensureSettingExists( name.toAscii() );

  // If a setting wasn't changed, get it from the defaults
  if( ! settings_.contains( name ) )
  {
#ifdef KMESSDEBUG_ACCOUNT_SETTINGS
    kmDebug() << "Retrieving from setting" << name << "the default value:" << settingsDefaults_[ name ];
#endif
    return settingsDefaults_[ name ];
  }

#ifdef KMESSDEBUG_ACCOUNT_SETTINGS
    kmDebug() << "Retrieving from setting" << name << "the value:" << settingsDefaults_[ name ];
#endif
  return settings_[ name ];
}



// Whether the account has unsaved settings changes
bool Account::isDirty() const
{
  return dirty_;
}



// Whether the account is a guest account, not permanently saved on the computer
bool Account::isGuestAccount() const
{
  return guestAccount_;
}



// Read in account properties
void Account::readProperties( const QString &handle )
{
  handle_ = handle;

  // Choose the main group
  KConfigGroup profileGroup = KMessConfig::instance()->getAccountConfig( handle_, "Account" );

  // Retrieve settings from configuration groups
  // Friendly name is really cache, to display a nice label with the last
  // friendly name when logging in, personal message on the other hand is
  // stored locally and not sent back to us.
  // Friendly name will be refreshed later, if inaccurate now.
  friendlyName_.setString        ( profileGroup.readEntry( "FriendlyName",               handle_   ) );
  personalMessage_.setString     ( profileGroup.readEntry( "PersonalMessage",            QString() ) );

  // Passwords are read by AccountsManager for safety

  // Load the settings (and emit the required update signals)
  SettingsList settings;
  SettingsListIterator it( settingsDefaults_ );
  while( it.hasNext() )
  {
    it.next();

    const QString  &name  = it.key();
    const QVariant &value = it.value();

    if( profileGroup.hasKey( name ) )
    {
      settings[ name ] = profileGroup.readEntry( name, value );
#ifdef KMESSDEBUG_ACCOUNT_SETTINGS
      kmDebug() << "Reading from disk setting" << name << "with value:" << settings[ name ];
#endif
    }
  }
  setSettings( settings );

  // After reading options from file, there is nothing to save
  guestAccount_ = false;
  dirty_        = false;

#ifdef KMESSDEBUG_ACCOUNT_DIRTY
  kmDebug() << "Resetting 'dirty' to false.";
#endif

  // If a chat style is removed, fall back to the default
  // This default is "Fresh" as of 1.5 final, to improve the first impression a user gets
  // The previous "Default" theme has been renamed to "Classic"
  // This code also makes the migration from 1.5-pre2 to 1.5 (and beyond) easier

  if( KGlobal::dirs()->findResource( "data",
                                     "kmess/styles/" +
                                     getSettingString( "ChatStyle" ) +
                                     '/' +
                                     getSettingString( "ChatStyle" ) +
                                     ".xsl" ).isEmpty() )
  {
#ifdef KMESSDEBUG_ACCOUNT_DIRTY
    kmDebug() << "Auto-corrected chat & setting, setting 'dirty' to true.";
#endif

    // Fall back to our default chat style
    revertSetting( "ChatStyle" );
    revertSetting( "ChatGroupFollowUpMessagesEnabled" );
    dirty_ = true;
  }
}



/**
 * Revert a setting to its default value.
 *
 * @param name The setting name
 */
void Account::revertSetting( const QString &name )
{
#ifdef KMESSDEBUG_ACCOUNT_SETTINGS
  kmDebug() << "Reverting setting" << name << "to default";
#endif

  ensureSettingExists( name.toAscii() );

  settings_.remove( name );
}



// Save account properties
void Account::saveProperties()
{
  // Protect against unwanted usage
  if( guestAccount_ )
  {
    kmWarning() << "Application attempted to save a guest account!";
    return;
  }

  if( ! dirty_ )
  {
#ifdef KMESSDEBUG_ACCOUNT_DIRTY
    kmDebug() << "Account" << handle_ << "is not 'dirty', skip saving properties.";
#endif
    return;
  }

  // Choose the main group
  KConfigGroup profileGroup = KMessConfig::instance()->getAccountConfig( handle_, "Account" );

  // Reset it to remove unnecessary options
  profileGroup.deleteGroup();

  // Save the settings hash
  SettingsListIterator it( settings_ );
  while( it.hasNext() )
  {
    it.next();

    const QString  &name  = it.key();
    const QVariant &value = it.value();

#ifdef KMESSDEBUG_ACCOUNT_SETTINGS
    kmDebug() << "Saving to disk setting" << name << "with value:" << value;
#endif

    profileGroup.writeEntry( name, value );
  }

  // Save the friendly name cache, and the personal message
  profileGroup.writeEntry( "FriendlyName",       friendlyName_.getOriginal()    );
  profileGroup.writeEntry( "PersonalMessage",    personalMessage_.getOriginal() );

  // Passwords are stored by AccountsManager for safety

  // Save to disk the changes now, to avoid losing data
  profileGroup.config()->sync();
}



// Change the friendly name
void Account::friendlyNameChanged()
{
  const QString &newName( globalSession->self()->displayName() );

  if( newName == friendlyName_.getOriginal() )
  {
    return;
  }

  if( newName.isEmpty() )
  {
    kmWarning() << "New name is empty.";
    return;
  }

  friendlyName_.setString( newName );

  dirty_ = true;

#ifdef KMESSDEBUG_ACCOUNT_DIRTY
  kmDebug() << "Setting 'dirty' to true.";
#endif
}



// Whether the account is a guest account, not permanently saved on the computer
void Account::setGuestAccount( bool guestAccount )
{
  guestAccount_ = guestAccount;
}



/**
 * @brief Set the current handle object.
 *
 * Use this method with extreme care! It is only kept around because AccountPage
 * currently allows changing accounts' handles in-place. It may probably be used
 * by people who wish to change handles but keep all their settings.
 *
 * This may work, because from the moment the setting changed, KMess will save
 * all the special Account data to the *new* handle. This is very bug-prone
 * and this dangerous nonfeature should be removed, inconveniencing users who
 * may wish to risk losing their data. Maybe we can create a "copy account"
 * functionality, but not now.
 *
 * Another use case for this method is in KMess, when initialising a new
 * Account. My proposal would be to give a handle to the Account constructor.
 *
 * @see getHandle()
 */
void Account::setHandle( const QString& handle )
{
  handle_ = handle;
}



// Set basic account data
/* void Account::setLoginInformation( QString handle, QString friendlyName, QString password )
{
#ifdef KMESSTEST
  KMESS_ASSERT(   handle.contains("@") );
  KMESS_ASSERT( ! handle.isEmpty()     );
#endif

  handle = handle.toLower();

  if( handle_                     == handle
  &&  friendlyName_.getOriginal() == friendlyName
  &&  password                    == password_ )
  {
    return;
  }

  handle_ = handle;

  // why would we need this method *and* setFriendlyName() ?
  friendlyName_.setString( friendlyName );
  emit changedFriendlyName();

  if( ! password.isEmpty() )
  {
    password_ = password;
  }

  dirty_ = true;

#ifdef KMESSDEBUG_ACCOUNT_DIRTY
  kmDebug() << "Setting 'dirty' to true.";
#endif
} */



// Set the account's login password
void Account::setPassword( const QString &password )
{
  password_ = password;
}



// Set the personal message
void Account::personalMessageChanged()
{
  const QString &newMessage( globalSession->self()->personalMessage() );

  if( newMessage == personalMessage_.getOriginal() )
  {
    return;
  }

  personalMessage_.setString( newMessage );
  dirty_ = true;

#ifdef KMESSDEBUG_ACCOUNT_DIRTY
  kmDebug() << "Setting 'dirty' to true.";
#endif
}



/**
 * Change a single setting.
 *
 * Do not use if you need to change more than one setting.
 * This is just a wrapper around setSettings(), which also emits the required update signals.
 *
 * @param name  Name of the setting to alter
 * @param value New value of the setting
 * @see setSettings()
 */
void Account::setSetting( const QString &name, const QVariant &value )
{
  SettingsList list;
  list[ name ] = value;

  setSettings( list );
}



/**
 * Change a list of settings.
 *
 * This method allows to alter a list of settings of an account.
 *
 * Only use setSetting() when it is necessary to change ONE SINGLE setting (like in the case of an
 * UI checkable menu option)) within, say, one second.
 * If there are more (for example, saving changes to settings dialogs with many options) ONLY use
 * setSettings(), or you'll send multiple times the Account update signals.
 *
 * @param changes List with the name->value settings to alter
 */
void Account::setSettings( const SettingsList& changes )
{
  bool emitChatStyleSignal      = false;
  bool emitIdleTimerSignal      = false;
  bool emitContactListSignal    = false;
  bool emitDisplaySignal        = false;
  bool emitViewModeSignal       = false;
  bool emitDisplayPictureSignal = false;
  bool emitEmoticonsSignal      = false;
  bool emitFontSignal           = false;
  bool emitEmailDisplaySignal   = false;
  bool emitNowListeningSignal   = false;
  bool emitLoggingSignal        = false;
  
  SettingsListIterator it( changes );
  while( it.hasNext() )
  {
    it.next();

    const QString  &name  = it.key();
    const QVariant &value = it.value();

    ensureSettingExists( name.toAscii() );

     // If the setting's new value is the default, the default setting will be used
    if( settingsDefaults_.value( name ) == value )
    {
#ifdef KMESSDEBUG_ACCOUNT_SETTINGS
      kmDebug() << "Setting" << name << "set to default, removing setting.";
#endif
      settings_.remove( name );
    }

    // The setting didn't change value, skip it
    // Not using the [] operator because it creates an empty item if the key is not found
    if( settings_.value( name ) == value )
    {
#ifdef KMESSDEBUG_ACCOUNT_SETTINGS
      kmDebug() << "Setting" << name << "value" << value << "unchanged, skipping setting";
#endif
      continue;
    }
    else
    {
#ifdef KMESSDEBUG_ACCOUNT_SETTINGS
      kmDebug() << "Changing setting" << name << "to" << value;
#endif
      settings_[ name ] = value;
    }

    // Check which signals need to be emitted

    if( name == "EmoticonsEnabled"
    or  name == "FontContactForce"
    or  name == "EmoticonsEnabled"
    or  name == "FontEffectsEnabled"
    or  name == "ChatTextFormattingEnabled"
    or  name == "ChatShakeNudge"
    or  name == "ChatTimestampEnabled"
    or  name == "ChatShowWinks"
    or  name == "ChatTimestampDateEnabled"
    or  name == "ChatTimestampSecondsEnabled"
    or  name == "ChatGroupFollowUpMessagesEnabled"
    or  name == "ChatTabbingMode"
    or  name == "ChatStyle" )
    {
      emitChatStyleSignal = true;
    }
    else
    if( name == "NotificationsHideWhenBusy"
    or  name == "AutoIdleEnabled"
    or  name == "AutoIdleTime" )
    {
      emitIdleTimerSignal = true;
    }
    else
    if( name == "ContactListBackgroundEnabled"
    or  name == "ContactListTextFormattingEnabled"
    or  name == "ContactListEmailAsNamesShow" )
    {
      emitContactListSignal = true;
    }
    else
    if( name == "ContactListAllowedGroupShow"
    or  name == "ContactListRemovedGroupShow"
    or  name == "ContactListOfflineGroupShow"
    or  name == "ContactListPictureSize"
    or  name == "ContactListSortAlphabetical" )
    {
      emitDisplaySignal = true;
    }
    else
    if( name == "ContactListEmptyGroupsShow"
    or  name == "ContactListDisplayMode" )
    {
      emitViewModeSignal = true;
    }
    else
    if( name == "ChatUserDisplayPictureEnabled"
    or  name == "DisplayPictureEnabled"
    or  name == "DisplayPicturePath"     )
    {
      emitDisplayPictureSignal = true;
    }
    else
    if( name == "EmoticonsTheme" )
    {
      emitEmoticonsSignal = true;
    }
    else
    if( name == "FontContact"
    or  name == "FontContactColor"
    or  name == "FontUser"
    or  name == "FontUserColor" )
    {
      emitFontSignal = true;
    }
    else
    if( name.startsWith( "Logging" ) )
    {
      emitLoggingSignal = true;
    }
    else
    if( name == "ContactListEmailCountShow" )
    {
      emitEmailDisplaySignal = true;
    }
    else
    if( name == "NowListeningEnabled" )
    {
      emitNowListeningSignal = true;
    }
  }

  // Send the signals for the updated settings
  if( emitChatStyleSignal      ) emit changedChatStyleSettings();
  if( emitIdleTimerSignal      ) emit changedTimerSettings();
  if( emitContactListSignal    ) emit changedContactListOptions();
  if( emitDisplaySignal        ) emit changedDisplaySettings();
  if( emitViewModeSignal       ) emit changedViewMode();
  if( emitDisplayPictureSignal ) emit changedDisplayPicture();
  if( emitEmoticonsSignal      ) emit changedEmoticonSettings();
  if( emitFontSignal           ) emit changedFontSettings();
  if( emitEmailDisplaySignal   ) emit changedEmailDisplaySettings();
  if( emitNowListeningSignal   ) emit changedNowListeningSettings();
  if( emitLoggingSignal        ) emit changedLoggingSettings();

  // Mark the settings as dirty, so they will get saved
  dirty_ = true;
}



/**
 * This method returns whether the given setting name exists. Do *not* use this
 * when you want to read a static setting in KMess, do *not* use it from inside
 * Account if you just received a setting name, etc etc. Use
 * ensureSettingExists for that.
 *
 * @param name the setting to check the existance of.
 * @returns whether 'name' is a valid setting.
 */
bool Account::settingExists( const QString &name )
{
  return settingsDefaults_.contains( name );
}



#include "account.moc"
