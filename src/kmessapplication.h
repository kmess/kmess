/***************************************************************************
                          kmessapplication.h -  description
                             -------------------
    begin                : Wed 7 13 2005
    copyright            : (C) 2005 by Diederik van der Boor
    email                : vdboor --at-- codingdomain.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef KMESSAPPLICATION_H
#define KMESSAPPLICATION_H

#include <KUniqueApplication>


// Forward declarations
class KMessDBus;
class IdleTimer;
class NotificationManager;
class NowListeningClient;
class PluginsMaster;
class ChatMaster;



/**
 * @brief KApplication subclass to handle quit requests.
 *
 * This class stores the data to close or quit KMess correctly.
 * When quitSelected() is false, the main window should only close.
 * When it's true, or sessionSaving() returns true, the application is exiting.
 * For compatibility with KDE 3.1.0, the sessionSaving() is implemented in an <code>\#if</code> block,
 * so users can still compile KMess at their old systems.
 *
 * @author Mike K. Bennett (original work), Diederik van der Boor (new class)
 * @ingroup Root
 */
class KMessApplication : public KUniqueApplication
{
  Q_OBJECT

  friend class KMessTest;

  public: // Public methods
    // Constructor
                             KMessApplication();
    // Destructor
    virtual                 ~KMessApplication();
    // Return the server used in test server mode
    const QString           &getTestServer() const;
    // Return whether to use an alternative server as the Live Service server
    bool                     getUseTestServer() const;
    // Return the idle timer object
    IdleTimer               *idleTimer() const;
    // Return the now listening object
    NowListeningClient      *nowListeningClient() const;
    // Returns the plugins master
    PluginsMaster           *pluginsMaster() const;
    // Returns the chat master
    ChatMaster              *chatMaster() const;
    // Return true if quit was selected
    bool                     quitSelected() const;
    // Tell the application that quit was selected
    void                     setQuitSelected( bool quitSelected );

  public: // Public static methods
    // Get the instance
    static KMessApplication *instance();

  private slots:
    // Get the notification that the app is quitting
    void                     slotAboutToQuit();
    // Quit the application
    void                     slotLastWindowClosed();

  private: // private methods
    void                     initializeMainWindow();
    // Initialize additional application paths and create the data dirs if not present
    void                     initializePaths();
    // Initialize the proxy support
    void                     initializeProxy();
    // Initialize the program service classes and helpers
    void                     initializeServices();

  private: // private properties
    // The chat master
    ChatMaster              *chatMaster_;
    // The DBus class
    KMessDBus               *dbus_;
    // The idle timer
    IdleTimer               *idleTimer_;
    // The notifications manager instance
    NotificationManager     *notificationManager_;
    // The now listening client
    NowListeningClient      *nowListeningClient_;
    // The plugins manager
    PluginsMaster           *pluginsMaster_;
    // True when quit was selected
    bool                     quitSelected_;
    // Address of the testing server, if any
    QString                  testServerAddress_;
};



#endif
