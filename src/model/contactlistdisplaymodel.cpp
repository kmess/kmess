/***************************************************************************
                          contactlistmodelfilter.cpp  -  description
                             -------------------
    begin                : Thu Jul 10 2008
    copyright            : (C) 2008 by Valerio Pilo
    email                : valerio@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "contactlistdisplaymodel.h"

#include "../account.h"
#include "../kmessglobal.h"
#include "../kmessdebug.h"

#include "../contact/group.h"
#include "../contact/contact.h"

#include <KMess/ContactListModelItem>
#include <KMess/MsnContact>
#include <KMess/MsnGroup>

#include <QMimeData>


#ifdef KMESSDEBUG_CONTACTLISTMODEL
//   #define KMESSDEBUG_CONTACTLISTMODELFILTER_VISIBILITY
#endif


// The constructor
ContactListDisplayModel::ContactListDisplayModel( QObject *parent )
: QSortFilterProxyModel( parent )
, showAllowed_   ( true )
, showEmpty_     ( false )
, showGroupsMode_( Account::VIEW_MIXED )
, showOffline_   ( true )
, showRemoved_   ( true )
{
  // Set filtering options
  setDynamicSortFilter( true );
  setFilterKeyColumn( 0 );
  setFilterRole( KMess::ContactListModelItem::SearchRole );
  setFilterCaseSensitivity( Qt::CaseInsensitive );
}



/**
 * This is how we determine the display order of items in the contact list.
 *
 * Groups are ordered based on their sort position.
 *
 * Contacts are ordered by online status and then alphabetically by their friendlyname.
 * It should support contacts with MSN+ formatting directly since Contact::getFriendlyName()
 * by default will clean the string.
 *
 * This is the *right* way to do sorting on a model! :)
 */
bool ContactListDisplayModel::lessThan( const QModelIndex &leftItem, const QModelIndex &rightItem ) const
{
  KMess::ContactListModelItem *left  = static_cast<KMess::ContactListModelItem*> (leftItem.internalPointer());
  KMess::ContactListModelItem *right = static_cast<KMess::ContactListModelItem*> (rightItem.internalPointer());
  
  if ( left->type() != right->type() )
  {
    // no idea what this is....
    // TODO: handle better.
#ifdef KMESSDEBUG_CONTACTLISTMODEL
    kmWarning() << "Left and right contact list items are two different types.";
#endif
    return true;
  }

  if ( left->type() == KMess::ContactListModelItem::ItemGroup )
  {
    Group leftGroup ( static_cast<const KMess::MsnGroup*>( left->object() ) );
    Group rightGroup ( static_cast<const KMess::MsnGroup*>( right->object() ) );

    const GroupsList groups = globalSession->contactList()->groups();

    // set a sort position if this is a new or unknown group.
    return leftGroup->getSortPosition() < rightGroup->getSortPosition();
  }
  else if ( left->type() == KMess::ContactListModelItem::ItemContact )
  {
    const KMess::MsnContact *leftContact = static_cast<const KMess::MsnContact*>( left->object() );
    const KMess::MsnContact *rightContact = static_cast<const KMess::MsnContact*>( right->object() );

    if ( leftContact->status() == rightContact->status() || sortAlphabetical_ )
    {
      // two statuses are equal. Sort fully alphabetically by name.
      // uppercase both because we have no idea if localeAwareCompare does a case-insensitive compare
      // or not.

      // TODO: needs to be a "clean" comparison!
      int comparison = Contact( leftContact )->getFriendlyName( STRING_CLEANED ).toUpper().localeAwareCompare( Contact( rightContact )->getFriendlyName( STRING_CLEANED ).toUpper() );
      return ( comparison < 0 );
    }
    else
    {
      // note: this relies on the values in the Status enum being in order of 
      // importance! Online first, Busy, Away, Idle, Offline, etc.
      return ( leftContact->status() < rightContact->status() );
    }
  }
  else
  {
#ifdef KMESSDEBUG_CONTACTLISTMODEL
    kmDebug() << "KMess::ContactListModelItems housed unknown objects! Type:" << left->type();
#endif
  }

  // pick something. something is fubar here anyway.
  return true;
}



/**
 * Return the flags which characterize the index
 *
 * @param proxyIndex  Points to the data to get flags of
 * @return The index's flags
 */
Qt::ItemFlags ContactListDisplayModel::flags( const QModelIndex &proxyIndex ) const
{
  QModelIndex index ( mapToSource( proxyIndex ) );

  if( ! index.isValid() )
  {
    return 0;
  }

  KMess::ContactListModelItem *item = static_cast<KMess::ContactListModelItem*>( index.internalPointer() );

  if( item == 0 )
  {
#ifdef KMESSDEBUG_CONTACTLISTMODEL
    kmDebug() << "Invalid target item!";
#endif
    return 0;
  }

  bool canBeDragged = false;
  bool canBeDropped = false;
  Qt::ItemFlags indexFlags = Qt::ItemIsEnabled | Qt::ItemIsSelectable;

  // Enable d&d only for certain kinds of items
  switch( item->type() )
  {
    // You can d&d contacts in normal groups
    case KMess::ContactListModelItem::ItemContact:
    {
      const KMess::MsnGroup *group = static_cast<const KMess::MsnGroup*>( item->parent()->object() );

      // Dragging check: can be dragged only contacts in normal groups and in the individuals group
      if( group->isSpecialGroup() == false || group->id() == KMess::SpecialGroups::INDIVIDUALS )
      {
        canBeDragged = true;
      }

      // Dropping check: only contacts in normal groups can be dropped on
      if( group->isSpecialGroup() == false )
      {
        canBeDragged = true;
      }
      break;
    }
    
    case KMess::ContactListModelItem::ItemGroup:
    {
      const KMess::MsnGroup *group = static_cast<const KMess::MsnGroup*>( item->object() );

      // Dragging and dropping are disabled for all special groups
      if( group->isSpecialGroup() == false )
      {
        canBeDragged = true;
        canBeDropped = true;
      }
      break;
    }

    default:
      break;
  }

  // Add the d&d flags if necessary
  if( canBeDragged )
  {
    indexFlags |= Qt::ItemIsDragEnabled;
  }
  if( canBeDropped )
  {
    indexFlags |= Qt::ItemIsDropEnabled;
  }

  return indexFlags;
}


/**
 * Dumps the contact list contents to the debug output
 *
 * This method is temporary and for debugging purposes exclusively.
 * This method is recursive. Do not call with any arguments, they are for internal use only.
 *
 * @param start Item in the model where to start from
 * @param depth Current search depth
 */
void ContactListDisplayModel::dump( QModelIndex start, int depth ) const
{
#ifdef KMESSDEBUG_CONTACTLISTMODEL
  bool started = false;

  if( ! start.isValid() )
  {
    start = QModelIndex();
    kmDebug() << "Contact List Filter model stats";
    kmDebug() << "--------------------------------------";
    kmDebug() << "Columns:"            << columnCount();
    kmDebug() << "Rows:"               << rowCount();
    kmDebug();
    kmDebug() << "Contact List Filter model dump";
    kmDebug() << "--------------------------------------";
    kmDebug() << "Root @" << start;
    started = true;
  }

  QString spacer( QString().fill( '-', 2 * depth ) + ">" );

  // Print the current node's contents, and manage children recursively
  for( int i = 0; i < rowCount( start ); i++ )
  {
    QModelIndex itemIndex( index( i, 0, start ) );
    ModelDataList itemData( itemIndex.data().toMap() );

    switch( itemData["type"].toInt() )
    {
      case KMess::ContactListModelItem::ItemContact:
        kmDebug() << spacer << (i+1) << ") Contact:"
                           << itemData["handle"].toString()
                           << "@" << itemIndex;
        break;

      case KMess::ContactListModelItem::ItemGroup:
        kmDebug() << spacer << (i+1) << ") Group:"
                           << itemData["id"].toString()
                           << "@" << itemIndex;
        dump( itemIndex, depth + 1 );
        break;

      default:
        kmDebug() << spacer << "Unknown type of item: " + itemData["type"].toInt() << "@" << itemIndex;
        break;
    }
  }

  if( started )
  {
    kmDebug() << "--------------------------------------";
    kmDebug() << "Persistent indices:" << endl << persistentIndexList();
    kmDebug() << "--------------------------------------";
  }
#else
  Q_UNUSED( start );
  Q_UNUSED( depth );
#endif
}



/**
 * @brief Determine if a row is visible or not
 *
 * Uses the internal data from the given index to determine if, according to the current
 * account options, a row should be hid or shown.
 *
 * @param sourceRow     Where the item to check is located inside the sourceParent index
 * @param sourceParent  The index containing the item to check
 */
bool ContactListDisplayModel::filterAcceptsRow( int sourceRow, const QModelIndex& sourceParent ) const
{
  bool isVisible = true;

  // Retrieve the item data from the source model
  const QModelIndex index( sourceModel()->index( sourceRow, 0, sourceParent ) );

  if( ! index.isValid() || ! index.data().isValid() || ! index.data().canConvert( QVariant::Map ) )
  {
    return false;
  }

  const KMess::ModelDataList& itemData( index.data().toMap() );

  if( itemData.isEmpty() )
  {
#ifdef KMESSDEBUG_CONTACTLISTMODEL
    kmDebug() << "Item" << index << "does not contain any data!";
#endif
    return false;
  }


  /**
   * Contact searches HOWTO: Most groups should be kept visible - the view will
   * autonomously hide any group without results (we can't do it here, because
   * the list is recursively iterated by the filter, so the group is filtered
   * before its children are).
   * The groups of the current view mode should be kept visible: we must however
   * make sure that, regardless of the current view mode, all possible contacts
   * are included in the search.
   */


  switch( itemData[ "type" ].toInt() )
  {
    case KMess::ContactListModelItem::ItemContact:
    {
      // Manage contact searches: the list view sets the filter expression, and we use it here;
      // however only contacts within their original group are shown
      if( ! filterRegExp().isEmpty() )
      {
        isVisible = (  itemData[ "group" ] != KMess::SpecialGroups::ONLINE
                    && itemData[ "group" ] != KMess::SpecialGroups::OFFLINE );
        isVisible = isVisible && ( QSortFilterProxyModel::filterAcceptsRow( sourceRow, sourceParent ) );
        break;
      }

      KMess::MsnStatus contactStatus = (KMess::MsnStatus) itemData[ "status" ].toInt();

      // Contacts in special groups are always visible. Their group may be not, though.
      // Also apply filtering to contacts in the Individuals group
      if( ! itemData[ "isInSpecialGroup" ].toBool()
      || itemData[ "group" ] == KMess::SpecialGroups::INDIVIDUALS )
      {
        switch( showGroupsMode_ )
        {
          /**
           * When showing the list by status, the user-defined groups are not visible,
           * therefore the contacts within aren't, either, regardless if the contacts
           * are visible or not.
           */
          case Account::VIEW_BYSTATUS:
            // Keep the item visible anyways.
            isVisible = true;
            break;

          /**
           * When showing by group, a contact is visible depending on the 'Show Offline
           * Contacts' option:
           *
           * -  If it is enabled: an offline contact should be visible within its group,
           *    but not within the Offline group (which will be hidden)
           * -  If it is disabled: an offline contact should always be hidden *except when searching*.
           */
          case Account::VIEW_BYGROUP:
            isVisible = ( showOffline_ || contactStatus != KMess::OfflineStatus || ! filterRegExp().isEmpty() );
            break;

          /**
           * On other view modes, a contact is visible if it's not offline.
           */
          default:
            isVisible = ( ( (KMess::MsnStatus) itemData[ "status" ].toInt() ) != KMess::OfflineStatus );
            break;
        }
      }
    }
    break;


    case KMess::ContactListModelItem::ItemGroup:
    {
      QString id( itemData[ "id" ].toString() );

      /**
       * The Allowed and Removed groups are visible only depending on their list view
       * options, not the group mode. In the next switch, these two groups will be
       * ignored because they're always handled here.
       * When searching, these two groups need to always be included in the search,
       * and therefore need to be kept visible.
       */
      if( id == KMess::SpecialGroups::ALLOWED )
      {
        isVisible = ( showAllowed_ || ! filterRegExp().isEmpty() );
        break;
      }
      else if( id == KMess::SpecialGroups::REMOVED )
      {
        isVisible = ( showRemoved_ || ! filterRegExp().isEmpty() );
        break;
      }

      // if we are searching in a group which is not special, and contains at least 1 contact (total),
      // show it: its data must be available in the model for searching.
      if( ! filterRegExp().isEmpty()
      &&  ( itemData[ "isSpecialGroup" ].toBool() == false || id == KMess::SpecialGroups::INDIVIDUALS )
      && itemData[ "totalContacts" ].toInt() > 0 )
      {
        isVisible = true;
        break;
      }

      switch( showGroupsMode_ )
      {
        /**
         * When showing the list by status, the user-defined groups are not visible;
         * the Online special group is visible; the Offline group is visible if the
         * "Show Offline Contacts" option is enabled; and the Individuals group is
         * not visible.
         * When searching, the Offline group cannot be hidden, or searching would
         * miss offline contacts.
         */
        case Account::VIEW_BYSTATUS:
          if( id == KMess::SpecialGroups::ONLINE )
          {
            isVisible = true;
          }
          else if( id == KMess::SpecialGroups::OFFLINE )
          {
            isVisible = ( showOffline_ || ! filterRegExp().isEmpty() );
          }
          else
          {
            isVisible = false;
          }
          break;

        /**
         * When showing by group, the user-defined and the Individuals groups are
         * visible (if they're not empty and the "Show Empty Groups" option is on),
         * but the Online and Offline ones are not.
         * Searching does not affect this mode.
         */
        case Account::VIEW_BYGROUP:
          if( itemData[ "isSpecialGroup" ].toBool() == false )
          {
            // TODO: Groups with no contacts at all (like new groups) are always
            // shown, because they would disappear from the view and that would
            // be confusing for users. Maybe there's a better way?
            if( ! showEmpty_ && itemData[ "totalContacts" ].toInt() > 0 )
            {
              isVisible = ( itemData[ "onlineContacts" ].toInt() > 0 );
            }
          }
          else if( id == KMess::SpecialGroups::INDIVIDUALS )
          {
            isVisible = ( itemData[ "totalContacts" ].toInt() > 0 );
            break;
          }
          else
          {
            isVisible = false;
          }
          break;

        /**
         * On the Mixed view mode, the Online group is hidden; the Offline group is shown
         * if the user enables the "Show Offline Contacts" option; the Individuals
         * group and the user-defined ones are hidden if they're empty (and if the
         * "Show Empty Groups" option is off).
         * Here, too, searching requires the Offline group to be shown.
         */
        case Account::VIEW_MIXED:
          if( id == KMess::SpecialGroups::ONLINE )
          {
            isVisible = false;
          }
          else if( id == KMess::SpecialGroups::OFFLINE )
          {
            isVisible = ( showOffline_ || ! filterRegExp().isEmpty() );
          }
          else if( id == KMess::SpecialGroups::INDIVIDUALS )
          {
            isVisible = ( itemData[ "totalContacts" ].toInt() > 0 );
            break;
          }
          else // User-defined groups
          {
            // TODO: Groups with no contacts at all (like new groups) are always
            // shown, because they would disappear from the view and that would
            // be confusing for users. Maybe there's a better way?
            if( ! showEmpty_ && itemData[ "totalContacts" ].toInt() > 0 )
            {
              isVisible = ( itemData[ "onlineContacts" ].toInt() > 0 );
            }
          }
          break;
      }
    }
    break;

    // Invalid items
    default:
      kmWarning() << "Unknown contents for index:" << index;
      kmWarning() << "-- Item data:" << itemData;
      break;

  }

#ifdef KMESSDEBUG_CONTACTLISTMODELFILTER_VISIBILITY
  switch( itemData[ "type" ].toInt() )
  {
    case KMess::ContactListModelItem::ItemContact:
      kmDebug() << "Visibility check: contact" << itemData[ "handle" ].toString() << "is"
               << ( isVisible ? "visible" : "hidden" );
      break;
    case KMess::ContactListModelItem::ItemGroup:
      kmDebug() << "Visibility check: group" << itemData[ "name" ].toString() << "is"
               << ( isVisible ? "visible" : "hidden" );
      break;
    default:
      break;
  }
#endif

  return isVisible;
}



/**
 * @brief Sets the given sourceModel to be processed by the proxy model.
 *
 * Overridden from QSortFilterProxyModel to instantly refresh the proxy
 * with the current view settings.
 *
 * @param sourceModel   Model to filter
 */
void ContactListDisplayModel::setSourceModel( QAbstractItemModel *sourceModel )
{
  QSortFilterProxyModel::setSourceModel( sourceModel );

  // Update the account-dependant options and re-sort the list
  invalidate();
}



/**
 * Support for drag/drop contacts and groups in the contact list.
 */
bool ContactListDisplayModel::dropMimeData( const QMimeData *data, Qt::DropAction action, int row, int column, const QModelIndex &proxyParent )
{
  Q_UNUSED( row );

  if( action == Qt::IgnoreAction )
  {
#ifdef KMESSDEBUG_CONTACTLISTMODEL
    kmDebug() << "Ignore action";
#endif
    return true;
  }

  // Only accept our special mimetype
  if( ! data->hasFormat( "application/kmess.list.item" ) )
  {
#ifdef KMESSDEBUG_CONTACTLISTMODEL
    kmDebug() << "Invalid format:" << data->formats();
#endif
    return false;
  }

  if ( column > 0 )
  {
#ifdef KMESSDEBUG_CONTACTLISTMODEL
    kmDebug() << "Invalid column:" << column;
#endif
    return false;
  }

  // convert to an index in the source model.
  const QModelIndex parent( mapToSource( proxyParent ) );

#ifdef KMESSDEBUG_CONTACTLISTMODEL
  kmDebug() << "Drop info - parent:" << parent << "Data:" << parent.data();
#endif

  QByteArray encodedData( data->data( "application/kmess.list.item" ) );
  QDataStream stream( &encodedData, QIODevice::ReadOnly );

  while( ! stream.atEnd() )
  {
    quint64 itemObject; // It will contain a pointer
    KMess::ContactListModelItem *sourceItem, *targetItem;

    stream >> itemObject;
    sourceItem = (KMess::ContactListModelItem*)itemObject;

    if( sourceItem == 0 )
    {
#ifdef KMESSDEBUG_CONTACTLISTMODEL
      kmDebug() << "Invalid item extracted from mime data!";
#endif
      continue;
    }

    targetItem = static_cast<KMess::ContactListModelItem*>( parent.internalPointer() );

    if( targetItem == 0 )
    {
#ifdef KMESSDEBUG_CONTACTLISTMODEL
      kmDebug() << "Invalid target item!";
#endif
      continue;
    }

#ifdef KMESSDEBUG_CONTACTLISTMODEL
    kmDebug() << "Extracted item:" << sourceItem;
#endif

    // Take action, depending on the type of dropped item
    switch( sourceItem->type() )
    {
      case KMess::ContactListModelItem::ItemContact:
      {
        /**
         * A Contact dropped to...
         * - root: no action
         * - another group: will be moved there
         * - its own group: no action
         * - a contact in another group: will be moved to that contact's group
         * - a contact in its own group: no action
         */
        const KMess::MsnContact *contact = static_cast<const KMess::MsnContact*>( sourceItem->object() );

        const KMess::MsnGroup *sourceGroup = static_cast<const KMess::MsnGroup*>( sourceItem->parent()->object() );
        if( sourceGroup->isSpecialGroup() )
        {
#ifdef KMESSDEBUG_CONTACTLISTMODEL
          kmDebug() << "Cannot move a contact from within a special group.";
#endif
          continue;
        }

        kmDebug() << targetItem->type();
        kmDebug() << "going to drop" << contact->handle();
        switch( targetItem->type() )
        {
          // Contact dropped to root
          case KMess::ContactListModelItem::ItemRoot:
#ifdef KMESSDEBUG_CONTACTLISTMODEL
            kmDebug() << "Cannot move a contact to root.";
#endif
            continue;

          // Contact dropped to a group
          case KMess::ContactListModelItem::ItemGroup:
          {
            if( targetItem == sourceItem->parent() )
            {
#ifdef KMESSDEBUG_CONTACTLISTMODEL
              kmDebug() << "Cannot move a contact to its own group.";
#endif
              continue;
            }

            const KMess::MsnGroup *targetGroup = static_cast<const KMess::MsnGroup*>( targetItem->object() );

            // Avoid moving the contact where it already is
            if( contact->groups().contains( const_cast<KMess::MsnGroup*>( targetGroup ) ) )
            {
#ifdef KMESSDEBUG_CONTACTLISTMODEL
              kmDebug() << "Cannot move a contact where it already is.";
#endif
              continue;
            }

            // Avoid moving contacts to special groups - which have no counterpart on the server
            if( targetGroup->isSpecialGroup() )
            {
#ifdef KMESSDEBUG_CONTACTLISTMODEL
              kmDebug() << "Cannot move a contact to a special group.";
#endif
              continue;
            }

#ifdef KMESSDEBUG_CONTACTLIST
            kmDebug() << "Moving contact from group" << sourceGroup << "to" << targetGroup;
#endif
            globalSession->contactList()->moveContact( contact, sourceGroup, targetGroup );
            break;
          }

          // Contact dropped to another contact
          case KMess::ContactListModelItem::ItemContact:
          {
            if( targetItem == sourceItem )
            {
#ifdef KMESSDEBUG_CONTACTLISTMODEL
              kmDebug() << "Cannot move a contact to itself.";
#endif
              continue;
            }

            const KMess::MsnGroup *targetGroup = static_cast<const KMess::MsnGroup*>( targetItem->parent()->object() );

            // Avoid moving the contact where it already is
            if( contact->groups().contains( const_cast<KMess::MsnGroup*>( targetGroup ) ) )
            {
#ifdef KMESSDEBUG_CONTACTLISTMODEL
              kmDebug() << "Cannot move a contact where it already is.";
#endif
              continue;
            }

            // Avoid moving contacts to special groups - which have no counterpart on the server
            if( targetGroup->isSpecialGroup() )
            {
#ifdef KMESSDEBUG_CONTACTLISTMODEL
              kmDebug() << "Cannot move a contact to a special group.";
#endif
              continue;
            }

#ifdef KMESSDEBUG_CONTACTLIST
            kmDebug() << "Moving contact from group" << sourceGroup << "to" << targetGroup;
#endif
            globalSession->contactList()->moveContact( contact, sourceGroup, targetGroup );
            break;
          }
        }

        break;
      }
      // End of handling of dropped contacts

      case KMess::ContactListModelItem::ItemGroup:
      {
        /**
         * A Group dropped to...
         * - root: no action
         * - another group: will be moved above it
         * - a contact within another group: will be moved above it
         * - itself: no action
         * - a contact within itself: no action
         */
        const KMess::MsnGroup *sourceGroup = static_cast<const KMess::MsnGroup*>( sourceItem->object() );

        if( sourceGroup->isSpecialGroup() )
        {
#ifdef KMESSDEBUG_CONTACTLISTMODEL
          kmDebug() << "Cannot move a special group.";
#endif
          continue;
        }

        switch( targetItem->type() )
        {
          // Group dropped to the root
          case KMess::ContactListModelItem::ItemRoot:
#ifdef KMESSDEBUG_CONTACTLISTMODEL
            kmDebug() << "Cannot move a group to root.";
#endif
            continue;

          // Group dropped to another group
          case KMess::ContactListModelItem::ItemGroup:
          {
            if( targetItem == sourceItem )
            {
#ifdef KMESSDEBUG_CONTACTLISTMODEL
              kmDebug() << "Cannot move a group to itself.";
#endif
              continue;
            }

            const KMess::MsnGroup *targetGroup = static_cast<const KMess::MsnGroup*>( targetItem->object() );

            // Avoid moving contacts to special groups - which have no counterpart on the server
            if( targetGroup->isSpecialGroup() )
            {
#ifdef KMESSDEBUG_CONTACTLISTMODEL
              kmDebug() << "Cannot move to a special group.";
#endif
              continue;
            }

#ifdef KMESSDEBUG_CONTACTLIST
            kmDebug() << "Swapping groups" << sourceGroup->name() << "and" << targetGroup->name();
#endif
            // Get the group sorting position for both groups
            Group target( targetGroup );
            Group source( sourceGroup );
            int oldGroupPos = target->getSortPosition();
            int newGroupPos = source->getSortPosition();

            // Swap them
            target->setSortPosition( newGroupPos );
            source->setSortPosition( oldGroupPos );

            invalidate();

            break;
          }

          // Group dropped to a contact
          case KMess::ContactListModelItem::ItemContact:
          {
            const KMess::MsnGroup *targetGroup = static_cast<const KMess::MsnGroup*>( targetItem->parent()->object() );

            if( targetItem->parent() == sourceItem )
            {
#ifdef KMESSDEBUG_CONTACTLISTMODEL
              kmDebug() << "Cannot move a group to itself.";
#endif
              continue;
            }

            if( targetGroup->isSpecialGroup() )
            {
#ifdef KMESSDEBUG_CONTACTLISTMODEL
              kmDebug() << "Cannot move to a special group.";
#endif
              continue;
            }

#ifdef KMESSDEBUG_CONTACTLIST
            kmDebug() << "Swapping groups" << sourceGroup->name() << "and" << targetGroup->name();
#endif

            // Get the group sorting position for both groups
            Group target( targetGroup );
            Group source( sourceGroup );
            int oldGroupPos = target->getSortPosition();
            int newGroupPos = source->getSortPosition();

            // Swap them
            target->setSortPosition( newGroupPos );
            source->setSortPosition( oldGroupPos );

            // Save the changed setting
            //saveProperties( targetGroup );
            //saveProperties( sourceGroup );

            break;
          }
        }

        break;
      }
      // End of handling of dropped groups

      case KMess::ContactListModelItem::ItemRoot:
      {
        /**
         * Root can't be moved, let alone dropped anywhere :)
         */
        break;
      }
    }
  }

  return true;
}



// Provide a MIME representation of some of the model's indexes
QMimeData *ContactListDisplayModel::mimeData( const QModelIndexList &indexes ) const
{
  QStringList  addresses;
  QByteArray   encodedData;
  QMimeData   *mimeData = new QMimeData();

  QDataStream stream( &encodedData, QIODevice::WriteOnly );

  // Encode into a byte array the details about dragged items
  foreach( const QModelIndex &proxyIndex, indexes )
  {
    QModelIndex index( mapToSource( proxyIndex ) );

    if( ! index.isValid() )
    {
      continue;
    }

    const KMess::ContactListModelItem *item = static_cast<KMess::ContactListModelItem*>( index.internalPointer() );

    if( item == 0 )
    {
#ifdef KMESSDEBUG_CONTACTLISTMODEL
      kmDebug() << "Invalid item!";
#endif
      continue;
    }

    KMess::ContactListModelItem::ItemType itemType = item->type();

    if( itemType != KMess::ContactListModelItem::ItemContact
    &&  itemType != KMess::ContactListModelItem::ItemGroup )
    {
#ifdef KMESSDEBUG_CONTACTLISTMODEL
      kmDebug() << "Non draggable item!";
#endif
      continue;
    }

#ifdef KMESSDEBUG_CONTACTLISTMODEL
    kmDebug() << "Preparing drag data for item" << item;
#endif

    // Save the pointer as a 64-bit unsigned integer
    stream << (quint64) item;

    if( itemType == KMess::ContactListModelItem::ItemContact )
    {
      const KMess::MsnContact *contact = static_cast<const KMess::MsnContact*>( item->object() );
      if( contact )
      {
        addresses.append( contact->id() );
      }
    }

    continue;
  }

  mimeData->setData( "application/kmess.list.item", encodedData );

  // External apps may want these. A better format should be used, if needed
  if( addresses.count() > 0 )
  {
    mimeData->setText( addresses.join( "; " ) );
  }

  return mimeData;
}



// Return the supported drag&drop mimetypes
QStringList ContactListDisplayModel::mimeTypes() const
{
  return QStringList() << "application/kmess.list.item";
}



// re-reads the display information (show allowed/offline/etc and the view mode) then
// invalidates the current sorting and filtering.
void ContactListDisplayModel::invalidate()
{
#ifdef KMESSDEBUG_CONTACTLISTMODEL
  kmDebug() << "Updating account options from" << sender() << "( this:" << this << ", source model:" << sourceModel() << ")";
#endif

  // Read the settings
  showAllowed_    = Account::connectedAccount->getSettingBool( "ContactListAllowedGroupShow" );
  showRemoved_    = Account::connectedAccount->getSettingBool( "ContactListRemovedGroupShow" );
  showOffline_    = Account::connectedAccount->getSettingBool( "ContactListOfflineGroupShow" );
  showEmpty_      = Account::connectedAccount->getSettingBool( "ContactListEmptyGroupsShow"  );
  showGroupsMode_ = (Account::ContactListDisplayMode) Account::connectedAccount->getSettingInt( "ContactListDisplayMode" );
  sortAlphabetical_ = Account::connectedAccount->getSettingBool( "ContactListSortAlphabetical" );

  QSortFilterProxyModel::invalidate();
  sort(0, Qt::AscendingOrder);
}



#include "contactlistdisplaymodel.moc"
