/***************************************************************************
                          contactlistmodelfilter.h  -  description
                             -------------------
    begin                : Thu Jul 10 2008
    copyright            : (C) 2008 by Valerio Pilo
    email                : valerio@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CONTACTLISTDISPLAYMODEL_H
#define CONTACTLISTDISPLAYMODEL_H

#include <QSortFilterProxyModel>
#include "../account.h"


/**
 * @brief Filtering class for the contact list model.
 *
 * This class serves as a filter for the contact list model: it reads the original list data,
 * sorts and filters out the list nodes according to the current visual options, and exposes a
 * different model with the changes applied.
 *
 * @author Valerio Pilo
 * @author Adam Goossens
 * @ingroup Root
 */
class ContactListDisplayModel : public QSortFilterProxyModel
{
  Q_OBJECT


  public: // Public methods
    // The constructor
         ContactListDisplayModel( QObject* parent = 0 );
    // Sets the given sourceModel to be processed by the proxy model.
    void                setSourceModel( QAbstractItemModel *sourceModel );
    // handles a drag/drop on the contact list.
    virtual bool        dropMimeData( const QMimeData *data, Qt::DropAction action, int row, int column, const QModelIndex &parent );
    // the flags which characterise the index.
    Qt::ItemFlags       flags( const QModelIndex &index ) const;
    // Provide a MIME representation of some of the model's indexes
    QMimeData           *mimeData( const QModelIndexList &indexes ) const;
    // Return the supported drag&drop mimetypes
    QStringList          mimeTypes() const;

  protected: // Protected methods
    // Determine if a row is visible or not
    bool         filterAcceptsRow( int sourceRow, const QModelIndex& sourceParent ) const;
    // position of two rows.
    virtual bool lessThan( const QModelIndex &left, const QModelIndex &right) const;

  public slots: // Public slots
    // Dumps the model contents to the debug output
    void dump( QModelIndex start = QModelIndex(), int depth = 1 ) const;
    // overridden. updates group options and then re-filters everything.
    void invalidate();

  private: // Private properties
    // Whether or not to show the Allowed special group
    bool            showAllowed_;
    // Whether or not to show empty groups
    bool            showEmpty_;
    // Whether or not to show the contacts grouped by online status or group
    Account::ContactListDisplayMode showGroupsMode_;
    // Whether or not to show the offline contacts in groups
    bool            showOffline_;
    // Whether or not to show the Removed special group
    bool            showRemoved_;
    // Whether or not to force sorting alphabetically.
    bool            sortAlphabetical_;


};



#endif
