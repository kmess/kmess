/***************************************************************************
                          emoticontheme.h - holds a collection of emoticons
                             -------------------
    begin                : Tue April 10 2007
    copyright            : (C) 2007 by Valerio Pilo
    email                : valerio@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef EMOTICONTHEME_H
#define EMOTICONTHEME_H

#include "emoticon.h"

#include <QHash>



/**
 * @brief Data class for a group of emoticons
 *
 * This class represents an emoticon theme, which may be a default emoticon set, or
 * a custom emoticon set. It can load, update, and save an emoticon theme.
 * Themes are loaded from, and saved to, XML definition files - compatible with other
 * clients like Kopete, even non-MSN ones.
 *
 * @author Michael Curtis, Diederik van der Boor, Valerio Pilo
 * @ingroup Root
 */
class EmoticonTheme : public QObject
{
  Q_OBJECT

  public:    // Public methods
    // Constructor
                                  EmoticonTheme();
    // Copy constructor
                                  EmoticonTheme( const EmoticonTheme &other );
    // Destructor
    virtual                      ~EmoticonTheme();

    // Create a new emoticon and add it to the theme
    void                          addEmoticon( const QString& pictureFile, const QStringList& shortcuts );
    // Check if the theme contains a certain emoticon
    bool                          contains( const QString &shortcut );
    // Returns true if a custom emoticon has already been added
    bool                          emoticonIsAdded( const QString &dataHash );
    // Obtain an emoticon by its shortcut
    const Emoticon               *getEmoticon( const QString &shortcut ) const;
    // Return the full list of emoticons
    const QList<Emoticon*> &      getEmoticons() const;
    // Return the picture file names of all emoticons, mapped by their first shortcut code
    const QHash<QString,QString>  &getFileNames() const;
    // Return a QHash to map shortcut to data hash
    const QHash<QString,QString>  &getHashes() const;
    // Return the search pattern to find emoticons in an HTML text
    const QRegExp                 &getHtmlPattern() const;
    // Return the HTML replacement codes for all emoticons
    const QHash<QString,QString>  &getHtmlReplacements( bool isSmall = false ) const;
    // Return a QStringList of emoticons
    const QStringList             &getList() const;
    // Return the search pattern to find emoticons in a text
    const QRegExp                 &getPattern() const;
    // Return one replacement code for the given emoticon
    QString                       getReplacement( const QString &code, bool isSmall = false ) const;
    // Return the replacement codes for all emoticons
    const QHash<QString,QString>  &getReplacements( bool isSmall = false ) const;
    // Return where the picture files for this theme are located
    const QString                 &getThemePath();
    // Load a theme, by creating it anew or by refreshing the current one
    bool                          loadTheme( const QString &themeName, bool isCustomTheme );
    // Delete a custom emoticon from the theme
    bool                          removeEmoticon( const QString &shortcut );
    // Change the shortcut and the tooltip of a custom emoticon
    bool                          renameEmoticon( const QString &oldShortcut, const QString &newShortcut );
    // Save the current theme to an XML theme definition file
    bool                          saveTheme();
    // Change the theme name
    void                          setThemeName( const QString &newThemeName );

  public:  // Public static methods
    // Return the full path of the first emoticon of the specified theme
    static QString                getThemeIcon( QString themeDir );

  private:     // Private methods
    // Create the theme from nothing
    bool                          createTheme( const QString& themeDir );
    // Rebuild the search&replace caches
    void                          updateCache();
    // Update a standard theme with names for the standard MSN emoticons
    void                          updateTitles();
    // Update the currently loaded theme with new images
    bool                          updateTheme( const QString& themeDir );

  private:    // Private properties
    QList<Emoticon*>              emoticons_;
    // Are we loading a theme?
    bool                          loadingTheme_;
    // Is this a custom theme?
    bool                          isCustomTheme_;
    // The HTML replace codes for emoticons
    QHash<QString,QString>        largeHtmlReplacements_;
    // The replace codes for emoticons
    QHash<QString,QString>        largeReplacements_;
    // The list of emoticon codes
    QStringList                   emoticonList_;
    // The search pattern for html emoticon codes
    QRegExp                       patternHtml_;
    // The search pattern for text emoticon codes
    QRegExp                       patternText_;
    // The HTML replace codes for emoticons
    QHash<QString,QString>        smallHtmlReplacements_;
    // The replace codes for emoticons
    QHash<QString,QString>        smallReplacements_;
    // The hashes for all emoticons
    QHash<QString,QString>        hashes_;
    // The full path to the currently loaded theme
    QString                       themePath_;
    // A mapping from code to emoticon picture filename
    QHash<QString,QString>        themeFileNames_;

  signals:
    // Signal that we've been changed and the UI needs to be refreshed
    void                          updated();
};



#endif
