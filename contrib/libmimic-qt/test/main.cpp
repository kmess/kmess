#include "main.h"
#include "../src/mimicdecode.h"
#include <KAboutData>
#include <KCmdLineArgs>
#include <KApplication>
#include <KDebug>
#include <QWaitCondition>
#include <unistd.h>
#include <QTimer>

TestApp::TestApp() : KApplication() {
  kDebug() << "Starting TestApp...";

  label_.resize(320,240);
  label_.show();

  kDebug() << "Starting to read frames...";

  dump_.setFileName("framedump.mimic");
  if( ! dump_.open( QIODevice::ReadOnly ) ) {
    kError() << "Couldn't open framedump.mimic";
    return;
  }

  i_ = 0;
  initialized_ = false;

  newFrame();
}

void TestApp::newFrame()
{
  QTimer::singleShot( 50, this, SLOT(decodeFrame()) );
}

void TestApp::decodeFrame()
{
    i_++;
    QByteArray header = dump_.read(24);
    if( header.size() == 0 )
    {
      kDebug() << "Done. Read" << (i_ - 1) << "frames.";
      quit();
      return;
    }
    else if( header.size() != 24 )
    {
      kWarning() << "Frame" << i_ << "is invalid (size " << header.size() << "). Quitting.";
      return;
    }

    QByteArray rawsize  = header.mid(8, 4);
    quint32 payloadSize = (uchar)rawsize[0] + ((uchar)rawsize[1]<<8) + ((uchar)rawsize[2]<<16) + ((uchar)rawsize[3]<<24);
    
    // Read the rest of the frame
    QByteArray frame = dump_.read( payloadSize );

    // Feed the frame through Mimic
    if( !initialized_ ) {
      kDebug() << "Feeding key frame!";
      if( ! mimic_.initDecoder( frame ) ) {
        kWarning() << "Failed to init decoder for frame " << i_;
        newFrame();
        return;
      }
    }
    unsigned int width, height, length;
    width  = mimic_.getWidth();
    height = mimic_.getHeight();
    length = mimic_.getBufferSize();

    if( !initialized_ )
    {
      initialized_ = true;
      kDebug() << "This video has a resolution of " << width << "x" << height << " and each frame has " << length << " decoded bytes.";
    }

    QByteArray decoded = mimic_.decodeFrame( frame );
    if( decoded.isEmpty() )
    {
      kWarning() << "Failed to decode frame " << i_;
      decodeFrame();
      return;
    }

    // Draw the image on the Pixmap
    QImage image( (uchar*)decoded.constData(), width, height, QImage::Format_RGB888);
    pixmap_ = QPixmap::fromImage(image);

    label_.setPixmap(pixmap_);
    label_.show();

    //free(decoded);
    kDebug() << "Decoded frame " << i_;

    newFrame();
}

TestApp::~TestApp() {
  kDebug() << "Quitting TestApp...";
}

int main( int argc, char *argv[])
{
  KAboutData aboutData( "testapp", 0, ki18n("TestApp"), "0.1", ki18n("A test app"),
      KAboutData::License_GPL, ki18n("(c) 2009, Sjors Gielen\n"), KLocalizedString(),
      "http://example.org/", "bugs" "@" "kmess" "." "org" );
  KCmdLineArgs::init( argc, argv, &aboutData );
  TestApp testapp;
  return testapp.exec();
}
