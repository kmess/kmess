/* Copyright (C) 2005  Ole Andr� Vadla Ravn�s <oleavr@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "mimicdecode.h"
#include <QDebug>
#include <QtEndian>

extern uchar _col_zag[64];

MimicDecode::MimicDecode()
 : Mimic(),
   painter_(0)
{
}

MimicDecode::MimicDecode( QPainter *painter )
 : Mimic()
{
  setPainter( painter );
}

MimicDecode::~MimicDecode()
{
}

void MimicDecode::setPainter( QPainter *painter )
{
  painter_ = painter;
}

int MimicDecode::getBufferSize()
{
  return frame_width_ * frame_height_ * 3;
}

bool MimicDecode::initDecoder( const QByteArray& frame )
{
  return initDecoder( (uchar*) frame.constData() );
}

/**
 * Initialize the mimic decoder. The frame passed in frame_buffer
 * is used to determine the resolution so that the internal state
 * can be prepared and resources allocated accordingly. Note that
 * the frame passed has to be a keyframe.
 *
 * After initializing use #mimic_get_property to determine required
 * buffer-size, resolution, quality, etc.
 *
 * Note that once a given context has been initialized
 * for either encoding or decoding it is not possible
 * to initialize it again.
 *
 * @param frame_buffer buffer containing the first frame to decode
 * @returns true on success
 */
bool MimicDecode::initDecoder( const uchar *frame_buffer )
{
  int width, height;
  bool is_keyframe;

  // Check if we've been initialized before and that frame_buffer is not NULL.
  if ( initialized_ || frame_buffer == NULL )
  {
    qWarning() << "Already initialized or frame_buffer is NULL";
    return false;
  }

  // Check resolution.
  width  = qFromLittleEndian(*((quint16 *) (frame_buffer + 4)));
  height = qFromLittleEndian(*((quint16 *) (frame_buffer + 6)));

  if (!(width == 160 && height == 120) && !(width == 320 && height == 240))
  {
    qWarning() << "Width and height are invalid "
        "(width=" << width << " height=" << height <<")";
    return false;
  }

  // Check that we're initialized with a keyframe.
  is_keyframe = (qFromLittleEndian(*((quint32 *) (frame_buffer + 12))) == 0);
    
  if (!is_keyframe)
  {
    qWarning() << "Not a keyframe - you need to initialise the decoder with a keyframe.";
    return false;
  }

  // Get quality setting (in case we get queried for it before decoding).
  quality_ = qFromLittleEndian(*((quint16 *) (frame_buffer + 2)));

  // Initialize!
  mimic_init(width, height);

  initialized_ = true;
  return true;
}

void MimicDecode::updateDisplay( const QByteArray& input )
{
    QByteArray output = decodeFrame( input );
    if( output.isEmpty() )
    {
        qWarning() << "Failed to decode frame.";
        return;
    }

    if( painter_ != 0 )
    {
        QImage image( (uchar*)output.constData(), frame_width_, frame_height_, QImage::Format_RGB888);
        painter_->drawImage( 0, 0, image );
    }
}

QByteArray MimicDecode::decodeFrame( const QByteArray& input )
{
    uchar *input_buffer, *output_buffer;
    QByteArray bytes;
    bool result;

    // Copy the QByteArray over to a charpointer
    input_buffer = (uchar *) malloc( input.size() );
    if( input_buffer == NULL )
    {
      qWarning() << "Out of memory!";
      return "";
    }
    memcpy( input_buffer, input.constData(), input.size() );

    // Prepare an output buffer
    output_buffer = (uchar *) malloc( getBufferSize() );

    // Decode the frame
    result = decodeFrame( input_buffer, output_buffer );
    if( !result )
    {
      qWarning() << "decodeFrame failed";
      return "";
    }

    bytes = (char*) output_buffer;
    free( output_buffer );
    return bytes;
}

/**
 * Decode a MIMIC-encoded frame into RGB data.
 *
 * @param input_buffer buffer containing the MIMIC-encoded frame to decode
 * @param output_buffer buffer that will receive the decoded frame in RGB 24-bpp packed pixel top-down format (use getBufferSize() to determine the required buffer size)
 * @returns true on success
 */
bool MimicDecode::decodeFrame( const uchar *input_buffer,
                               uchar *output_buffer )
{
    bool result, is_pframe;
    uchar *input_y, *input_cr, *input_cb;
    int width, height;
    
    // Some sanity checks.
    if ( input_buffer == NULL || output_buffer == NULL )
    {
        qWarning() << "Input or output buffer is NULL!";
        return false;
    }

    if ( ! initialized_ )
    {
        qWarning() << "Decoder is uninitialized!";
        return false;
    }
    
    // Get frame dimensions.
    width  = qFromLittleEndian(*((quint16 *) (input_buffer + 4)));
    height = qFromLittleEndian(*((quint16 *) (input_buffer + 6)));

    // Resolution changing is not supported.
    if (width  != frame_width_ || height != frame_height_)
    {
        qWarning() << "Frame width or height changed ("
          << "width="   << width  << " shouldbe=" << frame_width_
          << " height=" << height << " shouldbe=" << frame_height_ << ")";
        return false;
    }
    
    // Increment frame counter.
    frame_num_++;

    // Initialize state.
    quality_ = qFromLittleEndian(*((quint16 *) (input_buffer + 2)));
    is_pframe = qFromLittleEndian(*((quint32 *) (input_buffer + 12)));
    num_coeffs_ = input_buffer[16];
    
    data_buffer_ = (char *) (input_buffer + 20);
    data_index_ = 0;
    cur_chunk_len_ = 16;
    read_odd_ = false;
    
    // Decode frame.
    if ( prev_frame_buf_ != NULL )
    {
        result = startDecode( is_pframe );
    }
    else
    {
        qWarning() << "Previous frame buffer is NULL";
        result = false;
    }

    // Perform YUV 420 to RGB conversion.
    input_y  = cur_frame_buf_;
    input_cr = cur_frame_buf_ + y_size_;
    input_cb = cur_frame_buf_ + y_size_ + crcb_size_;

    _yuv_to_rgb(input_y,
                input_cb,
                input_cr,
                output_buffer,
                frame_width_,
                frame_height_);

    return result;
}

/*
 * decode_main
 *
 * Main decoding loop.
 */
bool MimicDecode::startDecode( bool is_pframe )
{
    unsigned int i;
    int y, x, j, chrom_ch, *bptr, base_offset, offset;
    int dct_block[64];
    uchar *src, *dst, *p;
    quint32 bit;
    
    /*
     * Clear Cr and Cb planes.
     */
    p = cur_frame_buf_ + y_size_;
    memset(p, 128, 2 * crcb_size_);

    /*
     * Decode Y plane.
     */
    for (y = 0; y < num_vblocks_y_; y++)
    {
        base_offset = y_stride_ * 8 * y;

        src = prev_frame_buf_ + base_offset;
        dst = cur_frame_buf_  + base_offset;

        for (x = 0; x < num_hblocks_y_; x++)
        {
            /* Check for a change condition in the current block. */

            if (is_pframe)
                bit = _read_bits( 1 );
            else
                bit = 0;

            if (bit == 0) {

                /* Yes: Is the new content the same as it was in one of
                 * the 15 last frames preceding the previous? */
                
                if (is_pframe)
                    bit = _read_bits( 1 );

                if (bit == 0) {
                    /* No: decode it. */
                    
                    if (_vlc_decode_block( dct_block, num_coeffs_ ) == false )
                    {
                        /* Corruped frame, return. */
                        return false;
                    }

                    _idct_dequant_block( dct_block, 0 );

                    bptr = dct_block;
                    for (i = 0; i < 8; i++)
                    {
                        offset = y_stride_ * i;

                        for (j = 0; j < 8; j++)
                        {
                            uint v;
                            
                            if (bptr[j] <= 255)
                                v = (bptr[j] >= 0) ? bptr[j] : 0;
                            else
                                v = 255;
                            
                            *(dst + offset + j) = v;
                        }

                        bptr += 8;
                    }
                } else {
                    quint32 backref;
                    
                    /* Yes: read the backreference (4 bits) and copy. */

                    backref = _read_bits( 4 );

                    p = buf_ptrs_[(ptr_index_ + backref) % 16];
                    p += base_offset + (x * 8);

                    for (i = 0; i < 8; i++)
                    {
                        offset = y_stride_ * i;

                        memcpy(dst + offset, p + offset, 8);
                    }
                }
            } else {
                /* No change no worries: just copy from the previous frame. */

                for (i = 0; i < 8; i++) {
                    offset = y_stride_ * i;

                    memcpy(dst + offset, src + offset, 8);
                }
            }

            src += 8;
            dst += 8;
        }
    }

    /*
     * Decode Cr and Cb planes.
     */
    for (chrom_ch = 0; chrom_ch < 2; chrom_ch++) {
        base_offset = y_size_ + (crcb_size_ * chrom_ch);

        for (y = 0; y < num_vblocks_cbcr_; y++) {
            uint num_rows = 8;
            
            /* The last row of blocks in chrominance for 160x120 resolution
             * is half the normal height and must be accounted for. */
            if (y + 1 == num_vblocks_cbcr_ && frame_height_ % 16 != 0)
                num_rows = 4;

            offset = base_offset + (crcb_stride_ * 8 * y);

            src = prev_frame_buf_ + offset;
            dst = cur_frame_buf_  + offset;
            
            for (x = 0; x < num_hblocks_cbcr_; x++) {
                /* Check for a change condition in the current block. */
                
                if (is_pframe)
                    bit = _read_bits( 1 );
                else
                    bit = 1;

                if (bit == 1) {
                    
                    /* Yes: decode it. */

                    if (_vlc_decode_block( dct_block, num_coeffs_) == false) {

                        /* Corrupted frame: clear Cr and Cb planes and return. */
                        p = cur_frame_buf_ + y_size_;
                        memset(p, 128, crcb_size_ * 2);

                        return false;
                    }

                    _idct_dequant_block( dct_block, 1);

                    for (i = 0; i < num_rows; i++) {
                        p = dst + (crcb_stride_ * i);

                        for (j = 0; j < 8; j++)
                            p[j] = dct_block[(i * 8) + j];
                    }

                } else {

                    /* No change no worries: just copy from the previous frame. */
                    
                    for (i = 0; i < num_rows; i++) {
                        offset = crcb_stride_ * i;
                        
                        memcpy(dst + offset, src + offset, 8);
                    }
                }

                src += 8;
                dst += 8;
            }
        }
    }

    /*
     * Make a copy of the current frame and store in
     * the circular pointer list of 16 entries.
     */
    prev_frame_buf_ = buf_ptrs_[ptr_index_];
    memcpy(prev_frame_buf_, cur_frame_buf_,
           y_size_ + (crcb_size_ * 2));
    
    if (--ptr_index_ < 0)
        ptr_index_ = 15;
    
    /*
     * Perform deblocking on all planes.
     */
    _deblock(cur_frame_buf_,
             y_stride_, y_row_count_);
    
    _deblock(cur_frame_buf_ + y_size_,
             crcb_stride_, crcb_row_count_);
    
    _deblock(cur_frame_buf_ + y_size_ + crcb_size_,
             crcb_stride_, crcb_row_count_);

    return true;
}


/*
 * _vlc_decode_block
 *
 * De-serialize (reconstruct) a variable length coded 8x8 block.
 */
bool MimicDecode::_vlc_decode_block( int *block, int num_coeffs )
{
    int pos;

    memset(block, 0, 64 * sizeof(int));

    /* The DC-value is read in as is. */
    block[0] = _read_bits( 8 );

    for (pos = 1; pos < num_coeffs; pos++) {
        
        uint prev_data_index, prev_cur_chunk_len, prev_chunk;
        uint value, num_bits;
        bool prev_read_odd, found_magic;
        
        /* Save context. */
        prev_data_index = data_index_;
        prev_cur_chunk_len = cur_chunk_len_;
        prev_chunk = cur_chunk_;
        prev_read_odd = read_odd_;

        /* Grab 16 bits. */
        value = _read_bits( 16 ) << 16;

        /* Restore context. */
        data_index_ = prev_data_index;
        cur_chunk_len_ = prev_cur_chunk_len;
        cur_chunk_ = prev_chunk;
        read_odd_ = prev_read_odd;

        /* Analyze and determine number of bits to read initially. */
        num_bits = 3;
        if ((value >> 30) == 0 || (value >> 30) == 1) {
            num_bits = 2;
        } else if ((value & 0xE0000000) != 0x80000000) {
            uint nibble = value >> 28;

            if (nibble == 11 || nibble == 12) {
                num_bits = 4;
            } else if (nibble == 10) {
                _read_bits( 4 );

                return true;
            } else {
                if (((value << 2) & 0x8000000) == 0)
                    num_bits = 2;

                num_bits += 2;
            }
        }

        /* Read that number of bits. */
        value = _read_bits( num_bits );

        /*
         * Look up the current value against the magic ones,
         * and continue extending it bit by bit from the input
         * stream until the magic value is found or we have
         * read 32 bits (in which case we give up).
         */
        found_magic = false;
        while (!found_magic) {
            VlcMagic *magic;

            if (num_bits > 32)
                return false;

            magic = _find_magic(value);

            if (magic != NULL) {
                pos += magic->pos_add;
                num_bits = magic->num_bits;

                found_magic = true;
            } else {
                value <<= 1;
                value |= _read_bits( 1 );

                num_bits++;
            }
        }

        /* Read the number of bits given by magic value entry. */
        value = _read_bits( num_bits );
        
        /* Gotcha! :-) */
        block[_col_zag[pos]] = vlcdec_lookup_[(num_bits * 255) + value];
    }

    return true;
}

