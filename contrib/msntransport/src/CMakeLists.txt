
SET(msntransport_SOURCES
     msnslp/message/contenttype.cpp
     msnslp/message/realtimeaddress.cpp
     msnslp/message/requestresponseline.cpp
     msnslp/message/sessiondescription.cpp
     msnslp/message/signalingheader.cpp
     msnslp/message/signalingmessage.cpp
     msnslp/message/signalingrequest.cpp
     msnslp/message/signalingresponse.cpp
     msnslp/message/signalinguri.cpp
     msnslp/dialog.cpp
     msnslp/proxysignalingsession.cpp
     msnslp/rtcsignalingendpoint.cpp
     msnslp/slppeertopeerendpoint.cpp
     msnslp/transaction.cpp
     transport/bridges/directtransportbridge.cpp
     transport/bridges/switchboardbridge.cpp
     transport/bridges/transportbridge.cpp
     transport/packet.cpp
     transport/packetscheduler.cpp
     transport/packetserializer.cpp
     transport/transportimpl.cpp
     contentdescription.cpp
     helper.cpp
     platform.cpp
)

SET(msntransport_HEADERS
     ../include/Ptp/TransportImpl
     ../include/Ptp/Platform
     ../include/Ptp/RtcSignalingEndpoint
     ../include/Ptp/SlpPeerToPeerEndpoint
     ../include/Ptp/SwitchboardBridge
     ../include/Ptp/Helper
)

set(msntransport_MOC_HDRS
     platform.h
     msnslp/dialog.h
     msnslp/proxysignalingsession.h
     msnslp/rtcsignalingendpoint.h
     msnslp/slppeertopeerendpoint.h
     msnslp/transaction.h
     transport/bridges/directtransportbridge.h
     transport/bridges/switchboardbridge.h
     transport/bridges/transportbridge.h
     transport/packetscheduler.h
     transport/transportimpl.h
)

# Run Qt's moc on the headers of QObject-derived classed
QT4_WRAP_CPP(msntransport_MOC_SOURCES ${msntransport_MOC_HDRS})

INCLUDE_DIRECTORIES( ${QT_INCLUDES} ${CMAKE_CURRENT_BINARY_DIR} )

# Do not install the library if we're bundling it in KMess
IF( NOT USE_BUNDLED_LIBRARIES MATCHES "MSNTRANSPORT" )
  ADD_LIBRARY( msntransport SHARED ${msntransport_SOURCES} ${msntransport_MOC_SOURCES} )
ELSE()
  ADD_LIBRARY( msntransport STATIC ${msntransport_SOURCES} ${msntransport_MOC_SOURCES} )
ENDIF()

TARGET_LINK_LIBRARIES( msntransport ${QT_QTCORE_LIBRARY} ${QT_QTNETWORK_LIBRARY} ${QT_QTXML_LIBRARY} ${QT_QTDBUS_LIBRARY} )
SET( LIB_INSTALL_DIR "${CMAKE_INSTALL_PREFIX}/lib" )
SET( HEADERS_INSTALL_DIR "${CMAKE_INSTALL_PREFIX}/include/Ptp" )

SET_TARGET_PROPERTIES( msntransport PROPERTIES
                                  VERSION   ${MSNTRANSPORT_VERSION}
                                  SOVERSION ${MSNTRANSPORT_VERSION}
                                  INSTALL_NAME_DIR ${LIB_INSTALL_DIR}
)


#### Installation ####

# Do not install the library if we're bundling it in another project
IF( NOT USE_BUNDLED_LIBRARIES MATCHES "MSNTRANSPORT" )
  INSTALL( TARGETS msntransport LIBRARY DESTINATION ${LIB_INSTALL_DIR} )
  INSTALL( FILES ${msntransport_HEADERS} DESTINATION ${HEADERS_INSTALL_DIR} )
ENDIF()
