//
// ContentDescription class
//
// Summary: Repesents the description of media content in a MIME
//    message
//
// Authors:
//    Gregg Edghill (gregg.edghill at gmail.com)
//
// Copyright (C) 2009 - 2010, Gregg Edghill
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of this software.
//
// THIS LIBRARY IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR
// MODIFY IT UNDER THE TERMS OF THE GNU LESSER GENERAL PUBLIC
// LICENSE AS PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER
// VERSION 2 OF THE LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.
//

#include "contentdescription.h"
#include <QtCore/QChar>

namespace PtpCore
{

ContentDescription::ContentDescription(const ContentType& contentType, const QByteArray& body)
{
  body_ = body;
  contentType_ = contentType;
}

ContentDescription::~ContentDescription()
{
}

const ContentType & ContentDescription::getContentType() const
{
  return contentType_;
}

const QByteArray & ContentDescription::getBody() const
{
  return body_;
}

void ContentDescription::setBody(const QByteArray& body)
{
  body_ = body;
}

const QString ContentDescription::toString() const
{
  QString contentAsString;
  if (contentType_.getCharSet().toLower() == QLatin1String("utf-8"))
  {
    contentAsString = QString::fromUtf8(body_);
  }

  return contentAsString;
}

}
