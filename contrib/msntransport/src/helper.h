//
// Helper class
//
// Authors:
//    Gregg Edghill (gregg.edghill at gmail.com)
//
// Copyright (C) 2006 - 2010, Gregg Edghill
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of this software.
//
// THIS LIBRARY IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR
// MODIFY IT UNDER THE TERMS OF THE GNU LESSER GENERAL PUBLIC
// LICENSE AS PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER
// VERSION 2 OF THE LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.
//

#ifndef CLASS_P2P__HELPER_H
#define CLASS_P2P__HELPER_H

#include "msnslp/message/sessiondescription.h"
#include <QtGlobal>
#include <QtCore/QUuid>
namespace PtpSignaling { class SignalingMessage; }
namespace PtpTransport { class Packet; typedef QList<Packet*> PacketQueue; }


namespace PtpInternal
{

/**
 * @brief Provides utility helper methods.
 *
 * @author Gregg Edghill
 */
class Q_DECL_EXPORT Helper
{
  public:
    /** @brief Asynchronously deletes the supplied QObject. */
    static void            asyncDeleteQObject(QObject *object);
    /** @brief Converts the specified QUuid to a byte array. */
    static QByteArray      convertQUuidToQByteArray(const QUuid& uuid);
    /** @brief Converts the specified byte array to a QUuid. */
    static QUuid           convertQByteArrayToQUuid(const QByteArray& hash);
    /** @brief Deletes the specified packet from the supplied packet queue. */
    static void            deletePacketFromQueue(PtpTransport::PacketQueue *queue, PtpTransport::Packet* packet);
    /** @brief Returns the next chunk (fragment) of the supplied packet's payload. */
    static PtpTransport::Packet * getNextPacketChunk(PtpTransport::Packet *packet, const quint32 chunkSize);
    /** @brief Reads the data in the specified packet. */
    static QByteArray      readDataFromPacket(PtpTransport::Packet *packet);
    /** @brief Reads the QUuid in the supplied packet. */
    static QUuid           readQUuidFromPacket(PtpTransport::Packet *packet);
    /** @brief Gets a cryptographically hashed version of the supplied uuid.*/
    static QUuid           sha1HashQUuid(const QUuid& uuid);
    /** @brief Parses the supplied byte array into data fields representing the content of a session description. */
    static PtpSignaling::SessionDescription parseSessionOffer(const QString& offerDescription);
    /** @brief Parses a signaling message from the supplied byte array. */
    static PtpSignaling::SignalingMessage * parseSignalingMessage(const QByteArray& signalingmessage);
    /** @brief Write the specified QUuid in the supplied packet. */
    static void            writeQUuidToPacket(PtpTransport::Packet *packet, const QUuid& key);
  private:
    /** @brief Creates an instance of the Helper class. */
                           Helper();

}; //Helper
}

#endif

