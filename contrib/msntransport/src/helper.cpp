//
// Helper
//
// Summary: Provides utility helper methods.
//
// Authors:
//    Gregg Edghill (gregg.edghill at gmail.com)
//
// Copyright (C) 2006 - 2010, Gregg Edghill
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of this software.
//
// THIS LIBRARY IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR
// MODIFY IT UNDER THE TERMS OF THE GNU LESSER GENERAL PUBLIC
// LICENSE AS PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER
// VERSION 2 OF THE LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.
//

#include "helper.h"
#include "msnslp/message/signalingrequest.h"
#include "msnslp/message/signalingresponse.h"
#include "transport/packet.h"
#include <QtCore/QCryptographicHash>
#include <QtCore/QDataStream>
#include <QtCore/qendian.h>
#include <QtCore/QHash>
#include <QtCore/QIODevice>
#include <QtCore/QRegExp>
#include <QtDebug>
using namespace PtpSignaling;
using namespace PtpTransport;

namespace PtpInternal
{

void Helper::asyncDeleteQObject(QObject *object)
{
  // Check whether the object is not null
  if (object)
  {
    // If so, schedule the object for disposal
    object->deleteLater();
    object = 0l;
  }
}

QUuid Helper::convertQByteArrayToQUuid(const QByteArray& bytearray)
{
  QUuid uuid;
  if (bytearray.size() >= 16)
  {
    // Get the Uuid byte parameters from the byte array.
    const quint32 l = (quint32(bytearray[3]) << 24) + (quint32(bytearray[2]) << 16) +
      (quint32(bytearray[1]) << 8) + quint32(bytearray[0]);
    const quint16 w1 = (quint16(bytearray[5]) << 8) + (quint16(bytearray[4]));
    const quint16 w2 = (quint16(bytearray[7]) << 8) + (quint16(bytearray[6]));
    const quint8 b1= bytearray[8], b2= bytearray[9], b3= bytearray[10], b4= bytearray[11];
    const quint8 b5= bytearray[12], b6= bytearray[13], b7= bytearray[14], b8= bytearray[15];

    uuid = QUuid(l, w1, w2, b1, b2, b3, b4, b5, b6, b7, b8);
  }

  return uuid;
}

QByteArray Helper::convertQUuidToQByteArray(const QUuid& uuid)
{
  // Get the 32 character hex string.
  const QString hexstring = uuid.toString().remove(QRegExp("[\\{\\}-]"));

  QByteArray bytearray(16, 0x00);
  QDataStream stream(&bytearray, QIODevice::WriteOnly);
  stream << qToBigEndian<quint32>(hexstring.mid(0, 8).toUInt(0, 16));
  stream << qToBigEndian<quint16>(hexstring.mid(8, 4).toUShort(0, 16));
  stream << qToBigEndian<quint16>(hexstring.mid(12, 4).toUShort(0, 16));
  stream << hexstring.mid(16, 8).toUInt(0, 16);
  stream << hexstring.mid(24, 8).toUInt(0, 16);

  return bytearray;
}

void Helper::deletePacketFromQueue(PacketQueue *queue, Packet* packet)
{
  if (queue && packet)
  {
    // Remove the packet from the queue
    queue->removeOne(packet);
    // Delete the packet.
    delete packet;
    packet = 0l;
  }
}

Packet * Helper::getNextPacketChunk(Packet *packet, const quint32 chunkSize)
{
  Packet *chunk = 0l;

  // Check whether the packet is valid
  if (!packet)
  {
    // If not, bail out.
    return chunk;
  }

  // Get the original packet's header.
  Packet::Header & packetheader0 = packet->getHeader();
  // Gets the size, in bytes, of the data to be fragmented.
  const quint32 count = packetheader0.blobSize - packetheader0.offset;
  // Calculate the payload size of the packet chunk.
  const quint32 length = qMin(chunkSize, count);

  chunk = new Packet();
  // Get the packet header.
  Packet::Header & packetheader = chunk->getHeader();
  // Copy the packet header field to the packet chunk.
  packetheader.destination = packetheader0.destination;
  packetheader.identifier = packetheader0.identifier;
  packetheader.offset = packetheader0.offset;
  packetheader.blobSize = packetheader0.blobSize;
  packetheader.payloadSize = length;
  packetheader.type = packetheader0.type;
  packetheader.lprcvd = packetheader0.lprcvd;
  packetheader.lpsent = packetheader0.lpsent;
  packetheader.lpsize = packetheader0.lpsize;

  // Copy 'length' bytes of the packet payload
  // to the packet chunk.

  QIODevice *payload = packet->getPayload();
  if (payload)
  {
    // Seek to the specified offset in the payload.
    payload->seek(packetheader.offset);

    QByteArray bytes(length, 0x00);
    // Read the packet payload bytes into the byte array.
    payload->read(bytes.data(), bytes.size());
    // Write the byte array to the chunk's payload.
    chunk->getPayload()->write(bytes);
    // Update the data offset.
    packetheader0.offset += length;
  }

  return chunk;
}

QUuid Helper::sha1HashQUuid(const QUuid& uuid)
{
  // Gets the bytes from the uuid.
  QByteArray uuidbytes = convertQUuidToQByteArray(uuid);
  // Hash the bytes of the uuid
  const QByteArray hash = QCryptographicHash::hash(uuidbytes, QCryptographicHash::Sha1);

  // Get the Uuid byte parameters from the hashed byte array.
  const quint32 l = (quint32(hash[3]&0x000000ff) << 24) + (quint32(hash[2]&0x000000ff) << 16) +
  (quint32(hash[1]&0x000000ff) << 8) + quint32(hash[0]&0x000000ff);
  const quint16 w1 = (quint16(hash[5]&0x00ff) << 8) + (quint16(hash[4]&0x00ff));
  const quint16 w2 = (quint16(hash[7]&0x00ff) << 8) + (quint16(hash[6]&0x00ff));
  const quint8 b1= hash[8], b2= hash[9], b3= hash[10], b4= hash[11];
  const quint8 b5= hash[12], b6= hash[13], b7= hash[14], b8= hash[15];

  return QUuid(l, w1, w2, b1, b2, b3, b4, b5, b6, b7, b8);
}

SessionDescription Helper::parseSessionOffer(const QString& offerDescription)
{
  SessionDescription sd;

  QHash<QString, QVariant> fields;
  bool parseError = false;
  QRegExp regex("EUF-GUID: \\{([0-9a-f]{8}\\-(?:[0-9a-f]{4}\\-){3}[0-9a-f]{12})\\}", Qt::CaseInsensitive);
  // Check if the euf guid field is valid.
  if (regex.indexIn(offerDescription) == -1)
  {
    qDebug() << Q_FUNC_INFO << "Got an invalid or no EUF-GUID field" << endl;
    parseError = true;
  }
  else
  {
    fields.insert("EUF-GUID", regex.cap(1));
  }

  regex = QRegExp("SessionID: (\\d+)");
  // Check if the session id field is valid.
  if (regex.indexIn(offerDescription) == -1)
  {
    qDebug() << Q_FUNC_INFO << "Got an invalid or no SessionID field" << endl;
    parseError = true;
  }
  else
  {
    fields.insert("SessionID", regex.cap(1));
  }

  regex = QRegExp("AppID: (\\d+)");
  // Check if the app id field is valid.
  if (regex.indexIn(offerDescription) == -1)
  {
    qDebug() << Q_FUNC_INFO << "Got an invalid or no AppID field" << endl;
    parseError = true;
  }
  else
  {
    fields.insert("AppID", regex.cap(1));
  }

  regex = QRegExp("Context: ([0-9a-zA-Z+/=]*)");
  // Check if the context field is valid.
  if (regex.indexIn(offerDescription) == -1)
  {
    qDebug() << Q_FUNC_INFO << "Got an invalid or no Context field" << endl;
    parseError = true;
  }
  else
  {
    // Convert the base64 string to a byte array.
    QByteArray context = QByteArray::fromBase64(regex.cap(1).toUtf8());
    if (context.isNull())
    {
      // If we cannot convert the context from
      // a base64 string to bytes, return true.
      qDebug() << Q_FUNC_INFO << "Error converting Base64 encoded Context field" << endl;
      parseError = true;
    }
    else
    {
      fields.insert("Context", context);
    }
  }

  if (!parseError)
  {
    QHash<QString, QVariant>::ConstIterator item;
    for (item=fields.begin(); item != fields.end(); ++item)
    {
      sd.addField(item.key(), item.value());
    }
  }

  return sd;
}

SignalingMessage * Helper::parseSignalingMessage(const QByteArray& signalingmessage)
{
  SignalingMessage *message = 0l;

  const QString messagedata = QString::fromUtf8(signalingmessage);

  // Try to parse the signaling layer protocol (slp)
  // message from the raw data received.
  const QString requestResponseLine = messagedata.section("\r\n", 0, 0);
  if (requestResponseLine.startsWith("INVITE") || requestResponseLine.startsWith("BYE") ||
      requestResponseLine.startsWith("ACK"))
  {
    // If the start line pattern matchs that of a request,
    // try to parse the request from the raw data if the
    // method is supported.
    QRegExp regex("^(\\w+) (MSNMSGR:[\\w@.;\\-\\{\\}]*) MSNSLP/(\\d\\.\\d)");
    if (regex.indexIn(requestResponseLine) != -1)
    {
      // Get the request method.
      const QString method = regex.cap(1);
      // Get the request uri.
      const QString requestUri = regex.cap(2);
      // Get the request message version.
      const QString version = regex.cap(3);
      // Create the request from the supplied method and request uri.
      message = new SignalingRequest(method, requestUri, version);
    }
  }
  else
  if (requestResponseLine.startsWith("MSNSLP"))
  {
    // If the start line pattern matchs that of a response,
    // try to parse the response from the raw data.
    QRegExp regex("^MSNSLP/(\\d\\.\\d) ([0-9]{3}) ([A-Za-z ]*)");
    if (regex.indexIn(requestResponseLine) != -1)
    {
      // Get the response message version.
      const QString version = regex.cap(1);
      // Get the response code.
      const quint32 responseCode = regex.cap(2).toUInt();
      // Get the response text
      const QString responseText = regex.cap(3);
      // Create the response from the supplied status code and description.
      message = new SignalingResponse(responseCode, responseText, version);
    }
  }

  // Check whether the message is valid
  if (message)
  {
    // If so, parse the raw message data for signaling headers.
    qint32 i = 0;
    QRegExp regex("([^\r\n:]*):\\s([^\r\n]*)");
    const QString headerdata = messagedata.section("\r\n\r\n", 0, 0).remove(requestResponseLine+"\r\n");
    while((i = regex.indexIn(headerdata, i)) != -1)
    {
      // Add the parsed signaling header to
      // the message's header collection.
      message->addSignalingHeader(SignalingHeader(regex.cap(1), regex.cap(2)));
      i += regex.matchedLength();
    }

    // Check whether the message has
    // a content description body
    if (message->getContentLength() > 0)
    {
      // If so, parse the content description body
      // from the raw array of bytes.
      const QString messageBody = messagedata.section("\r\n\r\n", 1, 1, QString::SectionIncludeTrailingSep);
      // Set the message body content.
      message->setMessageBody(messageBody.toUtf8());
    }
  }

  return message;
}

QByteArray Helper::readDataFromPacket(Packet *packet)
{
  QByteArray rawdata;
  // Check whether the packet is valid
  if (!packet)
  {
    // If not, return an empty array of bytes
    return rawdata;
  }

  // Otherwise, read the data from
  // the packet's payload.
  rawdata = packet->getPayload()->readAll();

  return rawdata;
}

QUuid Helper::readQUuidFromPacket(Packet *packet)
{
  QUuid key;

  if (packet)
  {
    const Packet::Header & packetheader = packet->getHeader();

    // Get the byte parameters from the last three fields of
    // the packet header.
    const quint32 l = packetheader.lprcvd;
    const quint16 w1 = packetheader.lpsent & 0xFFFF;
    const quint16 w2 = (packetheader.lpsent >> 16) & 0xFFFF;
    const quint8 b1 = packetheader.lpsize & 0xFF;
    const quint8 b2 = (packetheader.lpsize >> 8) & 0xFF;
    const quint8 b3 = (packetheader.lpsize >> 16) & 0xFF;
    const quint8 b4 = (packetheader.lpsize >> 24) & 0xFF;
    const quint8 b5 = (packetheader.lpsize >> 32) & 0xFF;
    const quint8 b6 = (packetheader.lpsize >> 40) & 0xFF;
    const quint8 b7 = (packetheader.lpsize >> 48) & 0xFF;
    const quint8 b8 = (packetheader.lpsize >> 56) & 0xFF;

    // Create the comparison nonce from the byte parameters.
    key = QUuid(l, w1, w2, b1, b2, b3, b4, b5, b6, b7, b8);
  }

  return key;
}

void Helper::writeQUuidToPacket(Packet *packet, const QUuid &key)
{
  // Check whether the packet is valid.
  if (!packet)
  {
    // If not, return.
    return;
  }

  // Get the 32 character hex string.
  const QString hexstring = key.toString().remove(QRegExp("[\\-\\{\\}]")).toUpper();
  // Convert the hex string to byte parameters.
  const quint32 l  = hexstring.mid(0, 8).toUInt(0, 16);
  const quint16 w1 = hexstring.mid(12, 4).toUShort(0, 16);
  const quint16 w2 = hexstring.mid(8, 4).toUShort(0, 16);
  const quint32 b1_4 = hexstring.mid(24, 8).toUInt(0, 16);
  const quint32 b5_8 = hexstring.mid(16, 8).toUInt(0, 16);

  const quint32 lprcvd = l;
  const quint32 lpsent = (quint32(w1) << 16) + w2;
  const quint64 lpsize = (quint64(qToBigEndian(b1_4)) << 32) + qToBigEndian(b5_8);

  // Get the header field of the packet.
  Packet::Header & h = packet->getHeader();
  // Set the correlation info of the packet.
  h.lprcvd = lprcvd;
  h.lpsent = lpsent;
  h.lpsize = lpsize;
}

}

