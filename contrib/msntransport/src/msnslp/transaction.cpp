//
// Transaction
//
// Summary:  Represents a series of signaling messages exchanged
//    between a caller and a callee.
//
// Authors:
//    Gregg Edghill (gregg.edghill at gmail.com)
//
// Copyright (C) 2006 - 2010, Gregg Edghill
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of this software.
//
// THIS LIBRARY IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR
// MODIFY IT UNDER THE TERMS OF THE GNU LESSER GENERAL PUBLIC
// LICENSE AS PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER
// VERSION 2 OF THE LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.
//

#include "transaction.h"

namespace PtpSignaling
{

Transaction::Transaction(QObject *pobject) : QObject(pobject)
{
}

Transaction::Transaction(const SignalingRequest& initiatingRequest, bool remotelyInitiated, QObject *pobject) : QObject(pobject)
{
  initiatingRequest_ = initiatingRequest;
  remotelyInitiated_ = remotelyInitiated;
}

Transaction::~Transaction()
{
}

bool Transaction::isRemotelyInitiated() const
{
  return remotelyInitiated_;
}

Transaction::SLP_TRANSACTION_STATE Transaction::getState() const
{
  return state_;
}

const SignalingRequest & Transaction::getInitiatingRequest() const
{
  return initiatingRequest_;
}

}
