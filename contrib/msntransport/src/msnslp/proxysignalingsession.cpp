//
// ProxySignalingSession
//
// Summary: Represents an INVITE based two-party session used for
//    media negotiation and signaling control.
//
// Authors:
//    Gregg Edghill (gregg.edghill at gmail.com)
//
// Copyright (C) 2006 - 2010, Gregg Edghill
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of this software.
//
// THIS LIBRARY IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR
// MODIFY IT UNDER THE TERMS OF THE GNU LESSER GENERAL PUBLIC
// LICENSE AS PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER
// VERSION 2 OF THE LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.
//

#include "proxysignalingsession.h"
#include "msntransportconfig.h"
#include "rtcsignalingendpoint.h"
#include "contentdescription.h"
#include "message/sessiondescription.h"
#include <QtCore/QTextStream>
#include <QtDebug>
using namespace PtpCollaboration;
using namespace PtpCore;

#ifdef MSNTRANSPORT_DEBUG
  #define P2PDEBUG_SLP_SIGNALINGSESSION
#endif

namespace PtpSignaling
{

ProxySignalingSession::ProxySignalingSession(const quint32 sessionId, RtcSignalingEndpoint *rtcendpoint) : QObject(rtcendpoint), signalingState_(SLPSS_INCOMING)
{
  rtcendpoint_ = rtcendpoint;
  sendByeMessage_ = true;
  sessionId_ = sessionId;
}

ProxySignalingSession::ProxySignalingSession(const quint32 sessionId, const QUuid& callId, RtcSignalingEndpoint *rtcendpoint) : QObject(rtcendpoint), signalingState_(SLPSS_IDLE)
{
  rtcendpoint_ = rtcendpoint;
  sendByeMessage_ = true;
  sessionId_ = sessionId;
}

ProxySignalingSession::~ProxySignalingSession()
{}

quint32 ProxySignalingSession::getSessionId() const
{
  return sessionId_;
}

bool ProxySignalingSession::isTerminatingOrTerminated() const
{
  return ((SLPSS_TERMINATING == signalingState_) ||
          (SLPSS_TERMINATED  == signalingState_));
}

void ProxySignalingSession::accept()
{
  // Set the state to establishing.
  signalingState_ = ProxySignalingSession::SLPSS_ESTABLISHING;

  SessionDescription answer;
  answer.setField(QLatin1String("SessionID"), sessionId_);

  // Accept the session with the specified answer
  rtcendpoint_->acceptSessionWithAnswer(this, answer);
}

void ProxySignalingSession::setMediaFeature(PtpCollaboration::MediaFeature *mediafeature)
{
  mediafeature_ = mediafeature;
}

void ProxySignalingSession::decline()
{
  // Set the state to terminating
  signalingState_ = ProxySignalingSession::SLPSS_TERMINATING;

  SessionDescription answer;
  answer.setField(QLatin1String("SessionID"), sessionId_);

  // Terminate the session with the specified answer
  rtcendpoint_->terminateSessionWithDecline(this, answer);
}

void ProxySignalingSession::participate()
{
  // Check whether the signaling session is
  // idle (just created) or is in progress.
  if (SLPSS_IDLE == signalingState_)
  {
    // If just created, try to establish
    // the session.
    establish();
  }
  else
  if (SLPSS_INCOMING == signalingState_)
  {
    // If in progress, accept the signaling
    // session.
    accept();
  }
}

void ProxySignalingSession::establish()
{
  // Set the state to establishing.
  signalingState_ = ProxySignalingSession::SLPSS_ESTABLISHING;

  // Retrieve the session decription offer parameters.
  const QByteArray inviteContextData;// = mediafeature_->getInviteContextData();
  const quint32 appId = 0;// = mediafeature_->getApplicationId();
  const QUuid eufguid;// = mediafeature_->getEufGuid();

  // Check whether the context data and euf guid fields are valid
  if (inviteContextData.isNull() || eufguid.isNull())
  {
    // If not, set the state to faulted and
    // indicate that an error has occurred.
#ifdef P2PDEBUG_SLP_SIGNALINGSESSION
    qDebug("%s: invalid EUF context data or guid", Q_FUNC_INFO);
#endif

    // Set the state to faulted.
    signalingState_ = ProxySignalingSession::SLPSS_FAULTED;

    // Signal that an error has occurred
//    emit faulted();

    return;
  }

  // Create the offer for the signaling session.
  SessionDescription offer;
  offer.setField(QLatin1String("EUF-GUID"), eufguid.toString().toUpper());
  offer.setField(QLatin1String("SessionID"), sessionId_);
  offer.setField(QLatin1String("AppID"), appId);
  offer.setField(QLatin1String("Context"), inviteContextData.toBase64());

  // Try to establish the session with the specified offer.
  rtcendpoint_->establishSessionWithOffer(this, offer);
}

void ProxySignalingSession::terminate()
{
  // Check whether the signaling session
  // is already established or is in
  // progress
  if (SLPSS_ESTABLISHED == signalingState_)
  {
    // If alrady established, terminate
    // the signaling session with a BYE
    // request.
    terminate(sendByeMessage_);
  }
  else
  if (SLPSS_INCOMING == signalingState_)
  {
    // If in progress, terminate the
    // signaling session with a
    // DECLINE response.
    decline();
  }
}

void ProxySignalingSession::terminate(bool sendByeMessage)
{
  // Set the state to terminating
  signalingState_ = ProxySignalingSession::SLPSS_TERMINATING;

  // Check whether the signaling session should
  // send a BYE signaling request.
  if (sendByeMessage)
  {
    // If so, set the session description
    SessionDescription close;
    close.setField(QLatin1String("SessionID"), sessionId_);
    // Retrieve the session decription parameters.
    const QByteArray closeContextData;// = mediafeature_->getCloseContextData();
    // Check whether the context data is valid
    if (!closeContextData.isNull())
    {
      // If so, add it to the description
      close.setField(QLatin1String("Context"), closeContextData.toBase64());
    }

    // Terminate the signaling session
    rtcendpoint_->terminateSession(this, close);
  }
}

}
