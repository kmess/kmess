//
// Transaction class
//
// Authors:
//    Gregg Edghill (gregg.edghill at gmail.com)
//
// Copyright (C) 2006 - 2010, Gregg Edghill
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of this software.
//
// THIS LIBRARY IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR
// MODIFY IT UNDER THE TERMS OF THE GNU LESSER GENERAL PUBLIC
// LICENSE AS PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER
// VERSION 2 OF THE LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.
//

#ifndef CLASS_P2P__SLPTRANSACTION_H
#define CLASS_P2P__SLPTRANSACTION_H

#include "message/signalingrequest.h"
#include <QtCore/QObject>
#include <QtCore/QUuid>

namespace PtpSignaling
{

/**
 * @brief Represents a series of signaling messages exchanged between a caller and a callee.
 *
 * A transaction consists of one signaling request sent by the caller and any responses
 * to that request sent from the callee back to the caller.
 *
 * @author Gregg Edghill
 */
class Transaction : public QObject
{
  Q_OBJECT

  public:
    /** @brief Defines the states of a transaction during its lifecycle. */
    enum SLP_TRANSACTION_STATE { SLPTS_CALLING=0, SLPTS_CONFIRMED=2, SLPTS_COMPLETED=4, SLPTS_TERMINATED=8};

  public:
    /** @brief Creates a new instance of the class. */
                           Transaction(QObject *pobject);
    /** @brief Creates a new instance of the class. */
                           Transaction(const SignalingRequest& initiatingRequest, bool remotelyInitiated, QObject *pobject);
    /** @brief Frees resources and performs other cleanup operations. */
                          ~Transaction();

  public:
    /** @brief Gets the request that was used to initiate the transaction. */
    const SignalingRequest & getInitiatingRequest() const;
    /** @brief Gets the state of the transaction. */
    SLP_TRANSACTION_STATE  getState() const;
    /** @brief Gets a value indicating whether the transaction was initiated remotely. */
    bool                   isRemotelyInitiated() const;

  private:
    bool                   remotelyInitiated_;
    SLP_TRANSACTION_STATE  state_;
    SignalingRequest       initiatingRequest_;

  Q_DISABLE_COPY(Transaction)

}; // Transaction
}

#endif
