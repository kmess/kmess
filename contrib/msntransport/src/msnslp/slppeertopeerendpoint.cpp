//
// SlpPeerToPeerEndpoint
//
// Summary: Represents a peer to peer endpoint used to communication
//    with other endpoints.
//
// Authors:
//    Gregg Edghill (gregg.edghill at gmail.com)
//
// Copyright (C) 2006 - 2010, Gregg Edghill
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of this software.
//
// THIS LIBRARY IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR
// MODIFY IT UNDER THE TERMS OF THE GNU LESSER GENERAL PUBLIC
// LICENSE AS PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER
// VERSION 2 OF THE LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.
//

#include "slppeertopeerendpoint.h"
#include "msntransportconfig.h"
#include "proxysignalingsession.h"
#include "platform.h"
using namespace PtpCollaboration;

#ifdef MSNTRANSPORT_DEBUG
#define P2PDEBUG_SLP
#define P2PDEBUG_SLP_VERBOSE
#endif

namespace PtpSignaling
{

SlpPeerToPeerEndpoint::SlpPeerToPeerEndpoint(const RealTimeAddress& localTarget, const RealTimeAddress& sessionTarget, QObject *pobject) : RtcSignalingEndpoint(localTarget, sessionTarget, pobject)
{
}

SlpPeerToPeerEndpoint::~SlpPeerToPeerEndpoint()
{}

bool SlpPeerToPeerEndpoint::isShuttingDown() const
{
  return false;
}

void SlpPeerToPeerEndpoint::onSessionReceived(const SessionDescription& sd, ProxySignalingSession *session) const
{
  // Try to select the appropriate handler for the session
  // invitation based on the euf guid.
  EufInviteHandler *eufhandler = Platform::instance()->getEufInviteHandler(sd.getEufGuid());
  if (eufhandler)
  {
    // If there is a registered invite handler that can handle
    // the received session description (offer), pass the offer
    // to the handler.

    // Handle the incoming EUF invite offer.
//    eufhandler->handleEufInvite(sessionTarget().getAddress(), sd, session);
  }
  else
  {
#ifdef P2PDEBUG_SLP
    qDebug("%s: EUF invite handler not found", Q_FUNC_INFO);
#endif
    // Otherwise, decline the session request.
    session->terminate();
  }
}

void SlpPeerToPeerEndpoint::shutdown()
{
#ifdef P2PDEBUG_SLP
  qDebug("%s: target=%s", Q_FUNC_INFO, qPrintable(getSessionTarget().getAddress()));
#endif

  RtcSignalingEndpoint::shutdown();
}

}
