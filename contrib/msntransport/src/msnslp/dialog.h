//
// Dialog class
//
// Authors:
//    Gregg Edghill (gregg.edghill at gmail.com)
//
// Copyright (C) 2006 - 2010, Gregg Edghill
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of this software.
//
// THIS LIBRARY IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR
// MODIFY IT UNDER THE TERMS OF THE GNU LESSER GENERAL PUBLIC
// LICENSE AS PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER
// VERSION 2 OF THE LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.
//

#ifndef CLASS_P2P__SLPDIALOG_H
#define CLASS_P2P__SLPDIALOG_H

#include "message/signalinguri.h"
#include <QtCore/QObject>
#include <QtCore/QList>
#include <QtCore/QUuid>

namespace PtpSignaling
{

class Transaction;

/**
 * @brief Represents a peer-to-peer relationship between two participants that persists for some time.
 *
 * A dialog enables the sequencing of messages between clients and the proper routing of requests
 * between both of them.  A dialog is identified at each client with a dialog ID which consists
 * of a Call ID value, a local tag and a remote tag.
 *
 * A dialog contains certain pieces of state needed for further message transmissions within the
 * dialog.  This state consists of the dialog ID, a local sequence number (used to order requests
 * from the client to its peer), a remote sequence number (used to order requests from its peer to
 * the client), a local URI and a target (remote) URI.
 *
 *  Caller                                            Callee
 *    |                                                  |                    --,
 *    |                       INVITE                     | --,                  |
 *    |------------------------------------------------->|   |                  |
 *    |                                                  |   | transaction 0    |
 *    |                       200 OK                     |   |                  |
 *    |<-------------------------------------------------|   |                  |
 *    |                                                  | --'                  |
 *    |                                                  |                      |
 *    |               reINVITE (renegotiate)             | --,                  |
 *    |------------------------------------------------->|   |                  |
 *    |                                                  |   | transaction 1    |
 *    |                       200 OK                     |   |                  |
 *    |<-------------------------------------------------|   |                  | dialog
 *    |                                                  | --'                  |
 *    |                 MEDIA / DATA STREAM              |                      |
 *    |<================================================>|                      |
 *    |                                                  |                      |
 *    |                       BYE                        | --,                  |
 *    |<-------------------------------------------------|   | transaction n    |
 *    |                                                  |   |                  |
 *    |                                                  | --'                  |
 *    |                                                  |                    --'
 *
 * @author Gregg Edghill
 */
class Dialog : public QObject
{
  Q_OBJECT

  public:
    /** @brief Enumerates the states of a dialog during its lifecycle. */
    enum SLP_DIALOG_STATE { SLPDS_PENDING=0, SLPDS_ESTABLISHED=2, SLPDS_TERMINATING=4, SLPDS_TERMINATED=8 };

  public:
    /** @brief Creates a new instance of the class. */
    Dialog(const QUuid& callId, QObject *pobject);
    /** @brief Creates a new instance of the class. */
    Dialog(Transaction *initialTransaction, QObject *pobject);
    /** @brief Frees resources and performs other cleanup operations. */
    ~Dialog();

  public:
    /** @brief Gets the UUID used to uniquely identify the dialog. */
    const QUuid &          getCallId() const;
    /** @brief Gets the current identifier of the transaction in progress. */
    quint32                getCurrentTransactionId() const;
    /** @brief Gets the FROM tag used in the initiating request. */
    const SignalingUri &   getFromUri() const;
    /** @brief Gets the initial transaction of the dialog. */
    Transaction *          getInitialTransaction() const;
    /** @brief Gets the identifier of the session associated with the dialog. */
    quint32                getSessionKey() const;
    /** @brief Gets the state of the dialog. */
    SLP_DIALOG_STATE       getState() const;
    /** @brief Gets the To tag used in the initiating request. */
    const SignalingUri &   getToUri() const;
    /** @brief Gets the list of in-dialog transactions. */
    const QList<Transaction*> & getTransactions() const;
    /** @brief Gets a value indicating whether the dialog is terminating or has already terminated. */
    bool                   isTerminatingOrTerminated() const;
    /** @brief Adds the transaction to the dialog. */
    void                   addTransaction(Transaction * transaction);
    /** @brief Sets the identifier of the current transaction in progress. */
    void                   setCurrentTransactionId(quint32 transactionId);
    /** @brief Sets the initial transaction of the dialog. */
    void                   setInitialTransaction(Transaction * transaction);
    /** @brief Sets the identifier of the session associated with the dialog. */
    void                   setSessionKey(quint32 sessionId);
    /** @brief Sets the state of the dialog. */
    void                   setState(SLP_DIALOG_STATE state);
    /** @brief Called when the INVITE process is complete. */
    void                   establish();
    /** @brief Terminates the dialog within the specified timespan. */
    void                   terminate(quint32 timeSpan=0);

  Q_SIGNALS:
    /** @brief Indicates that the INVITE process is complete. */
    void                   established();
    /** @brief Indicates that the dialog has terminated. */
    void                   terminated();

  private Q_SLOTS:
    /** @brief Called when the timer timespan expires. */
    void                   timer_OnDialogTerminateTimespanExpired();

  private:
    QUuid                  callId_;
    quint32                currentTransactionId_;
    SignalingUri           fromUri_;
    bool                   initiatedRemotely_;
    quint32                sessionId_;
    SLP_DIALOG_STATE       state_;
    QList<Transaction*>    transactions_;
    SignalingUri           toUri_;

  Q_DISABLE_COPY(Dialog)

}; //Dialog
}

#endif
