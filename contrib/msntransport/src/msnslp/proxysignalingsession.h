//
// SignalingSession class
//
// Authors:
//    Gregg Edghill (gregg.edghill at gmail.com)
//
// Copyright (C) 2006 - 2010, Gregg Edghill
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of this software.
//
// THIS LIBRARY IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR
// MODIFY IT UNDER THE TERMS OF THE GNU LESSER GENERAL PUBLIC
// LICENSE AS PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER
// VERSION 2 OF THE LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.
//

#ifndef CLASS_P2P__SLPPROXYSIGNALINGSESSION_H
#define CLASS_P2P__SLPPROXYSIGNALINGSESSION_H

#include <QtCore/QObject>
#include <QtCore/QUuid>
namespace PtpCollaboration { class MediaFeature; }

namespace PtpSignaling
{

class RtcSignalingEndpoint;
class SignalingSessionClient;

/**
 * @brief Represents an INVITE based two-party session used for media negotiation and signaling control.
 *
 * @author Gregg Edghill
 */
class Q_DECL_EXPORT ProxySignalingSession : public QObject
{
  Q_OBJECT

  public:
    /** @brief Enumerates the states of a session during its lifecycle. */
    enum SLP_SESSION_STATE
    {
      SLPSS_IDLE        = 0,    // the session is idle
      SLPSS_INCOMING    = 2,    // the session has just been received
      SLPSS_ESTABLISHING= 4,    // the session establishment is in progress
                                // after accepting or initiating the session
      SLPSS_ESTABLISHED = 8,    // the session is extablished
      SLPSS_TERMINATING = 0x10, // the session is terminating
      SLPSS_TERMINATED  = 0x20, // the session has terminated
      SLPSS_FAULTED     = 0x80  // the session has encountered an error
    };

  public:
    /** @brief Creates a new instance of the class. */
                          ProxySignalingSession(const quint32 sessionId, RtcSignalingEndpoint *rtcendpoint);
    /** @brief Creates a new instance of the class. */
                          ProxySignalingSession(const quint32 sessionId, const QUuid& callId, RtcSignalingEndpoint *rtcendpoint);
    /** @brief Frees resources and performs other cleanup operations. */
                         ~ProxySignalingSession();

  public:
    /** @brief Gets an id that uniquely identifies the signaling session. */
    quint32               getSessionId() const;
    /** @brief Gets a value indicating whether the session is terminating or has already terminated. */
    bool                  isTerminatingOrTerminated() const;
    /** @brief Bind the user-specified media feature to the session. */
    void                  setMediaFeature(PtpCollaboration::MediaFeature *mediafeature);
    /** @brief Participates in a session. */
    void                  participate();
    /** @brief Terminate the session, after which it is no longer usable. */
    void                  terminate();

  private:
    /** @brief Accepts an incoming request to establish a session. */
    void                  accept();
    /** @brief Declines an incoming request to establish a session. */
    void                  decline();
    /** @brief Starts the signaling operation to establish the session. */
    void                  establish();
    /** @brief Terminates the session and cleans up resources. */
    void                  terminate(bool sendByeMessage);

  private:
    PtpCollaboration::MediaFeature *mediafeature_;
    SignalingSessionClient *rtcendpoint_;
    quint32               sessionId_;
    SLP_SESSION_STATE     signalingState_;
    bool                  sendByeMessage_;

  Q_DISABLE_COPY(ProxySignalingSession)

}; //ProxySignalingSession
}

#endif
