//
// Dialog
//
// Summary: Represents a peer-to-peer relationship between two
//    participants that persists for some time.
//
// Authors:
//    Gregg Edghill (gregg.edghill at gmail.com)
//
// Copyright (C) 2006 - 2010, Gregg Edghill
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of this software.
//
// THIS LIBRARY IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR
// MODIFY IT UNDER THE TERMS OF THE GNU LESSER GENERAL PUBLIC
// LICENSE AS PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER
// VERSION 2 OF THE LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.
//

#include "dialog.h"
#include "transaction.h"
#include <QtCore/QRegExp>
#include <QtCore/QTimer>

namespace PtpSignaling
{

Dialog::Dialog(const QUuid &callId, QObject *pobject) : QObject(pobject), state_(SLPDS_PENDING)
{
  currentTransactionId_ = 0;
  callId_ = callId;
  initiatedRemotely_ = false;
}

Dialog::Dialog(Transaction *initialTransaction, QObject *pobject) : QObject(pobject), state_(SLPDS_PENDING)
{
  currentTransactionId_ = 0;
  initiatedRemotely_ = true;
  // Get the signaling request that initiated the dialog
  const SignalingRequest & initiatingRequest = initialTransaction->getInitiatingRequest();
  const QRegExp escaperegex("[<>]");
  fromUri_ = SignalingUri(initiatingRequest.getFromHeader().getValue().toString().remove(escaperegex));
  toUri_ = SignalingUri(initiatingRequest.getToHeader().getValue().toString().remove(escaperegex));
  callId_ = QUuid(initiatingRequest.getCallId());
  transactions_.prepend(initialTransaction);
}

Dialog::~Dialog()
{
  // Delete all the transactions.
  qDeleteAll(transactions_);
  // Clear the transactions list.
  transactions_.clear();
}

const QUuid & Dialog::getCallId() const
{
  return callId_;
}

quint32 Dialog::getCurrentTransactionId() const
{
  return currentTransactionId_;
}

const SignalingUri & Dialog::getFromUri() const
{
  return !initiatedRemotely_ ? fromUri_ : toUri_;
}

Transaction * Dialog::getInitialTransaction() const
{
  return transactions_[0];
}

quint32 Dialog::getSessionKey() const
{
  return sessionId_;
}

Dialog::SLP_DIALOG_STATE Dialog::getState() const
{
  return state_;
}

const SignalingUri & Dialog::getToUri() const
{
  return !initiatedRemotely_ ? toUri_ : fromUri_;
}

const QList<Transaction*> & Dialog::getTransactions() const
{
  return transactions_;
}

bool Dialog::isTerminatingOrTerminated() const
{
  return (Dialog::SLPDS_TERMINATING == state_ || Dialog::SLPDS_TERMINATED == state_);
}

void Dialog::setCurrentTransactionId(quint32 transactionId)
{
  currentTransactionId_ = transactionId;
}

void Dialog::addTransaction(Transaction *transaction)
{
  // Add the transaction to the list.
  transactions_.append(transaction);
}

void Dialog::setInitialTransaction(Transaction * transaction)
{
  if (0 == transactions_.count())
  {
    // Add the transaction to the list.
    transactions_.prepend(transaction);
    const QRegExp escaperegex("[<>]");
    // Get the signaling request that initiated the dialog
    const SignalingRequest & initiatingRequest = transaction->getInitiatingRequest();
    fromUri_ = SignalingUri(initiatingRequest.getFromHeader().getValue().toString().remove(escaperegex));
    toUri_ = SignalingUri(initiatingRequest.getToHeader().getValue().toString().remove(escaperegex));
  }
}

void Dialog::setSessionKey(quint32 sessionId)
{
  sessionId_ = sessionId;
}

void Dialog::setState(SLP_DIALOG_STATE state)
{
  state_ = state;
}

void Dialog::establish()
{
  // Set the dialog state to established
  state_ = Dialog::SLPDS_ESTABLISHED;
  // Signal that the dialog has been established.
  emit established();
}

void Dialog::terminate(quint32 timeSpan)
{
  // Check whether the timespan is
  // greater than zero (0)
  if (timeSpan != 0)
  {
    // if so, set the dialog state to terminating and wait an
    // interval 'timeSpan' before terminating the dialog.
    state_ = Dialog::SLPDS_TERMINATING;
    // Schedule a one time timer with the given time span.
    QTimer::singleShot(timeSpan, this, SLOT(timer_OnDialogTerminateTimespanExpired()));
  }
  else
  {
    // Otherwise, set the dialog state to terminated.
    state_ = Dialog::SLPDS_TERMINATED;
    // Signal that the dialog has terminated.
    emit terminated();
  }
}

//BEGIN Timer Event Handling Functions

void Dialog::timer_OnDialogTerminateTimespanExpired()
{
  // Set the dialog state to terminated.
  state_ = Dialog::SLPDS_TERMINATED;
  // Signal that the dialog has terminated.
  emit terminated();
}

//END

}
