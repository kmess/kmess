//
// SignalingSessionClient class
//
// Authors:
//    Gregg Edghill (gregg.edghill at gmail.com)
//
// Copyright (C) 2009 - 2010, Gregg Edghill
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of this software.
//
// THIS LIBRARY IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR
// MODIFY IT UNDER THE TERMS OF THE GNU LESSER GENERAL PUBLIC
// LICENSE AS PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER
// VERSION 2 OF THE LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.
//

#ifndef CLASS_P2P__SIGNALINGSESSIONCLIENT_H
#define CLASS_P2P__SIGNALINGSESSIONCLIENT_H

#include "message/sessiondescription.h"

namespace PtpSignaling
{

class ProxySignalingSession;

/**
* @brief Represents the interface of a signaling session client.
*
* @author Gregg Edghill
*/
class SignalingSessionClient
{
  public:
    /** @brief Create a new instance of the SignalingSessionClient class. */
                      SignalingSessionClient();
    /** @brief Frees resources and performs other cleanup operations. */
    virtual          ~SignalingSessionClient();

  protected:
    /** @brief When overriden in a derived class, accepts the supplied signaling session with the specified answer. */
    virtual void      acceptSessionWithAnswer(ProxySignalingSession *pss, const SessionDescription& answerbody) =0;
    /** @brief When overriden in a derived class, establishs the supplied signaling session with the specified offer. */
    virtual void      establishSessionWithOffer(ProxySignalingSession *pss, const SessionDescription& offerbody) =0;
    /** @brief When overriden in a derived class, terminates the supplied signaling session with the specified close description. */
    virtual void      terminateSession(ProxySignalingSession *pss, const SessionDescription& closebody) =0;
    /** @brief When overriden in a derived class, terminates the supplied signaling session with the specifiec answer. */
    virtual void      terminateSessionWithDecline(ProxySignalingSession *pss, const SessionDescription& answerbody) =0;

  friend class ProxySignalingSession;

}; // SignalingSessionClient

inline SignalingSessionClient::SignalingSessionClient()
{
}

inline SignalingSessionClient::~SignalingSessionClient()
{
}
}

#endif
