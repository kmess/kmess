//
// SignalingUri
//
// Summary:  Represents a unique address used to specify a signaling
//    target.
//
// Authors:
//    Gregg Edghill (gregg.edghill at gmail.com)
//
// Copyright (C) 2009 - 2010, Gregg Edghill
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of this software.
//
// THIS LIBRARY IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR
// MODIFY IT UNDER THE TERMS OF THE GNU LESSER GENERAL PUBLIC
// LICENSE AS PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER
// VERSION 2 OF THE LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.
//

#include "signalinguri.h"
#include <QtCore/QRegExp>

namespace PtpSignaling
{

SignalingUri::SignalingUri()
{
}

SignalingUri::SignalingUri(const QString& value)
{
  //
  // General form of a signaling uri is as follows:
  //
  // msnmsgr:user@host.com;{0a901719-40cc-8993-b0bc-ea5534466d09}
  //
  // To support multiple points of presence (MPOP), an endpoint GUID
  // is added as a URI parameter - however, this parameter does not
  // follow the RFC 3261 for uri parameter construction; that is,
  // URI parameters take the form of name=value pairs separated by
  // a semi-colon.
  //
  // On clients that don't support MPOP, the endpoint id plus
  // the semi-colon is omitted - i.e. msnmsgr:user@host.com
  //

  // Create the regular expression to parse the URI string value.
  QRegExp regex("^([^:]+):([\\w@.]+);?(\\{([0-9a-f]{8}\\-(?:[0-9a-f]{4}\\-){3}[0-9a-f]{12})\\})?",
                Qt::CaseInsensitive);
  if (regex.indexIn(value) != -1)
  {
    // Gets the scheme name part of the uri
    scheme_ = regex.cap(1);
    // Gets the user address part of the uri
    address_  = regex.cap(2);
    // Gets the endpoint id part of the uri.
    endpointId_ = regex.cap(3);
  }
}

SignalingUri::SignalingUri(const QString& scheme, const QString& address, const QString& endpointId)
{
  address_ = address;
  endpointId_ = endpointId;
  scheme_ = scheme;
}

SignalingUri::~SignalingUri()
{}

bool SignalingUri::operator ==(const SignalingUri& other) const
{
  bool isEqual = false;
  if (address_ == other.address_ &&
      scheme_ == other.scheme_ &&
      endpointId_ == other.endpointId_)
  {
    isEqual = true;
  }

  return isEqual;
}

bool SignalingUri::operator !=(const SignalingUri& other) const
{
  return !operator==(other);
}

QString SignalingUri::getEndpointId() const
{
  return endpointId_;
}

QString SignalingUri::getScheme() const
{
  return scheme_;
}

QString SignalingUri::getUser() const
{
  return address_.section(QLatin1Char('@'), 0, 0);
}

QString SignalingUri::getUserAddress() const
{
  return address_;
}

void SignalingUri::setEndpointId(const QString &endpointId)
{
  endpointId_ = endpointId;
}

void SignalingUri::setScheme(const QString &scheme)
{
  scheme_ = scheme;
}

void SignalingUri::setUserAddress(const QString &address)
{
  address_ = address;
}

QString SignalingUri::toString() const
{
  QString uriparameters;
  // Check whether an endpoint id was specified.
  if (!endpointId_.isEmpty())
  {
    // If so, add it to the uri parameters string.
    uriparameters = QLatin1String(";") + endpointId_;
  }
  return QString("%1:%2%3").arg(scheme_, address_, uriparameters);
}

}

