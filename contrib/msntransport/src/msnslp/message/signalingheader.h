//
// SignalingHeader class
//
// Authors:
//    Gregg Edghill (gregg.edghill at gmail.com)
//
// Copyright (C) 2009 - 2010, Gregg Edghill
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of this software.
//
// THIS LIBRARY IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR
// MODIFY IT UNDER THE TERMS OF THE GNU LESSER GENERAL PUBLIC
// LICENSE AS PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER
// VERSION 2 OF THE LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.
//

#ifndef CLASS_P2P__SLPSIGNALINGHEADER_H
#define CLASS_P2P__SLPSIGNALINGHEADER_H

#include <QtCore/QString>
#include <QtCore/QVariant>
typedef QHash<QString, QVariant> NameValueCollection;

namespace PtpSignaling
{

/**
 * @brief Represents a signaling header that can be attached to an outgoing message.
 *
 * @author Gregg Edghill
 */
class Q_DECL_EXPORT SignalingHeader
{
  public:
    /** @brief Creates a new instance of the class. */
                           SignalingHeader();
    /** @brief Creates a new instance of the class. */
                           SignalingHeader(const SignalingHeader& header);
    /** @brief Creates a new instance of the class. */
                           SignalingHeader(const QString& name, const QVariant& value);
    /** @brief Frees resources and performs other cleanup operations. */
                          ~SignalingHeader();

  public:
    /** @brief Gets a string containing the name of the header. */
    const QString &        getName() const;
    /** @brief Gets the parameter with the specified name. */
    const QVariant         getParameter(const QString& name) const;
    /** @brief Gets the parameters. */
    NameValueCollection    getParameters() const;
    /** @brief Gets the value of the header. */
    const QVariant &       getValue() const;
    /** @brief Gets the value of the header with no parameters. */
    const QVariant         getValueNoParameters() const;
    /** @brief Gets a value indicating whether the header is valid. */
    bool                   isNull() const;
    /** @brief Add a parameter with the specified name and value to the header. */
    void                   addParameter(const QString& name, const QVariant& value);
    /** @brief Sets the name of the header. */
    void                   setName(const QString& name);
    /** @brief Sets the value of the header. */
    void                   setValue(const QVariant& value);

  private:
    /** @brief Parses the parameters contained in the specified header value. */
    void                   parseParametersFrom(const QString& value);

  private:
    QString                name_;
    NameValueCollection    headerparameters_;
    QVariant               value_;

}; // SignalingHeader
}

#endif
