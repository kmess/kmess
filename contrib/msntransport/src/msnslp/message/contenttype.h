//
// ContentType class
//
// Authors:
//    Gregg Edghill (gregg.edghill at gmail.com)
//
// Copyright (C) 2009 - 2010, Gregg Edghill
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of this software.
//
// THIS LIBRARY IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR
// MODIFY IT UNDER THE TERMS OF THE GNU LESSER GENERAL PUBLIC
// LICENSE AS PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER
// VERSION 2 OF THE LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.
//

#ifndef CLASS_P2P__CONTENTTYPE_H
#define CLASS_P2P__CONTENTTYPE_H

#include <QtCore/QHash>
#include <QtCore/QString>
typedef QHash<QString, QString> StringDictionary;

namespace PtpSignaling
{

/**
 * @brief Represents the information from a MIME Content-Type header.
 *
 * @author Gregg Edghill
 */
class Q_DECL_EXPORT ContentType
{
  public:
    /** @brief Creates a new instance of the class. */
    ContentType(const QString& contentType=QString());
    /** @brief Frees resources and performs other cleanup operations. */
    ~ContentType();

  public:
    /** @brief Gets the value of the charset parameter. */
    const QString         getCharSet() const;
    /** @brief Gets the media type value. */
    const QString         getMediaType() const;
    /** @brief Gets the dictionary of parameters included in the content type. */
    const StringDictionary & getParameters() const;
    /** @brief Gets the string representation of this ContentType object. */
    const QString &       toString() const;

  private:
    QString               contentType_;
    StringDictionary      parameters_;

}; // ContentType
}

#endif
