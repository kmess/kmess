//
// RequestResponseLine class
//
// Summary: Represents the request or response start-line of a
//    signaling message
//
// Authors:
//    Gregg Edghill (gregg.edghill at gmail.com)
//
// Copyright (C) 2009 - 2010, Gregg Edghill
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of this software.
//
// THIS LIBRARY IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR
// MODIFY IT UNDER THE TERMS OF THE GNU LESSER GENERAL PUBLIC
// LICENSE AS PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER
// VERSION 2 OF THE LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.
//

#include "requestresponseline.h"

namespace PtpSignaling
{

RequestResponseLine::RequestResponseLine()
{
  type_ = 0;
}

RequestResponseLine::RequestResponseLine(const RequestResponseLine& rrLine)
{
  *this = rrLine;
}

RequestResponseLine::RequestResponseLine(quint32 responseCode, const QString& responseText)
{
  responseCode_ = responseCode;
  responseText_ = responseText;
  type_ = 1;
}

RequestResponseLine::RequestResponseLine(const QString& requestMethod, const QString& requestUri)
{
  requestMethod_ = requestMethod;
  requestUri_    = requestUri;
  type_ = 2;
}

bool RequestResponseLine::isValid() const
{
  bool valid = true;
  if (type_ == 0)
  {
    valid = false;
  }
  if ((type_ == 1) && responseCode_.isNull() && (slpVersion_.isEmpty() || slpVersion_.isEmpty()))
  {
    valid = false;
  }
  else
  if ((type_ == 2) && requestMethod_.isEmpty() && (requestUri_.isEmpty() ||
      slpVersion_.isEmpty()))
  {
    valid = false;
  }

  return valid;
}

const QString & RequestResponseLine::getRequestMethodType() const
{
  return requestMethod_;
}

const QString & RequestResponseLine::getRequestUri() const
{
  return requestUri_;
}

quint32 RequestResponseLine::getResponseCode() const
{
  return responseCode_.value<quint32>();
}

const QString & RequestResponseLine::getResponseText() const
{
  return responseText_;
}

const QString & RequestResponseLine::getVersionString() const
{
  return slpVersion_;
}

void RequestResponseLine::setVersionString(const QString& slpVersion)
{
  slpVersion_ = slpVersion;
}

const QString RequestResponseLine::toString() const
{
  if (!isValid())
  {
    return QLatin1String("");
  }

  if (!responseCode_.isNull())
  {
    return QString("MSNSLP/%1 %2 %3").arg(slpVersion_)
             .arg(responseCode_.toString())
             .arg(responseText_);
  }

  return QString("%1 %2 MSNSLP/%3").arg(requestMethod_)
             .arg(requestUri_)
             .arg(slpVersion_);
}

}
