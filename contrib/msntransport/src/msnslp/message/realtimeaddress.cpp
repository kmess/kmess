//
// RealTimeAddress
//
// Summary:  Represents a realtime address used to identify a
//    signaling target.
//
// Authors:
//    Gregg Edghill (gregg.edghill at gmail.com)
//
// Copyright (C) 2009 - 2010, Gregg Edghill
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of this software.
//
// THIS LIBRARY IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR
// MODIFY IT UNDER THE TERMS OF THE GNU LESSER GENERAL PUBLIC
// LICENSE AS PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER
// VERSION 2 OF THE LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.
//

#include "realtimeaddress.h"
#include <QtCore/QRegExp>

namespace PtpSignaling
{

RealTimeAddress::RealTimeAddress(const QString& value)
{
  //
  // General form of a realtime address as a string is as follows:
  //
  // user@host.com;{0a901719-40cc-8993-b0bc-ea5534466d09}
  //
  // On clients that don't support MPOP, the endpoint id plus
  // the semi-colon are omitted - i.e.
  //
  // user@host.com
  //

  // Create the regular expression to parse the URI string value.
  QRegExp regex("^([\\w@.]+);?(\\{([0-9a-f]{8}\\-(?:[0-9a-f]{4}\\-){3}[0-9a-f]{12})\\})?",
                Qt::CaseInsensitive);

  if (regex.indexIn(value) != -1)
  {
    // Gets the rtc address
    address_  = regex.cap(1);
    // Gets the endpoint id associated with
    // the epid.
    endpointId_ = regex.cap(2);
  }
}

RealTimeAddress::RealTimeAddress(const QString& address, const QString& endpointId)
{
  address_ = address;
  endpointId_ = endpointId;
}

RealTimeAddress::~RealTimeAddress()
{}

bool RealTimeAddress::operator ==(const RealTimeAddress& other)
{
  return (address_ == other.address_ && endpointId_ == other.endpointId_);
}

bool RealTimeAddress::operator !=(const RealTimeAddress& other)
{
  return !operator==(other);
}

QString RealTimeAddress::getAddress() const
{
  return address_;
}

QString RealTimeAddress::getEndpointId() const
{
  return endpointId_;
}

void RealTimeAddress::setAddress(const QString& address)
{
  address_ = address;
}

void RealTimeAddress::setEndpointId(const QString& endpointId)
{
  endpointId_ = endpointId;
}

bool RealTimeAddress::isMpopSupported() const
{
  return !endpointId_.isEmpty();
}

}


