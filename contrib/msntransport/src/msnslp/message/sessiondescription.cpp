//
// SessionDescription
//
// Summary:  .
//
// Authors:
//    Gregg Edghill (gregg.edghill at gmail.com)
//
// Copyright (C) 2009 - 2010, Gregg Edghill
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of this software.
//
// THIS LIBRARY IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR
// MODIFY IT UNDER THE TERMS OF THE GNU LESSER GENERAL PUBLIC
// LICENSE AS PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER
// VERSION 2 OF THE LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.
//

#include "sessiondescription.h"
#include <QtCore/QTextStream>

namespace PtpSignaling
{

SessionDescription::SessionDescription()
{
}

SessionDescription::~SessionDescription()
{}

quint32 SessionDescription::getApplicationId() const
{
  quint32 appid = 0;
  if (fields_.contains(QLatin1String("AppID")))
  {
    appid = fields_[QLatin1String("AppID")].toUInt();
  }
  return appid;
}

QByteArray SessionDescription::getBytes() const
{
  QByteArray bytes;
  QTextStream textwriter(&bytes, QIODevice::WriteOnly);
  const QString CrLf = QLatin1String("\r\n");

  QHash<QString, QVariant>::ConstIterator item;
  for (item=fields_.begin(); item != fields_.end(); ++item)
  {
    textwriter << item.key() << ": " << item.value().toString() << CrLf;
  }
  textwriter << CrLf;

  return bytes;
}

QByteArray SessionDescription::getContext() const
{
  QByteArray context;
  if (fields_.contains(QLatin1String("Context")))
  {
    context = fields_[QLatin1String("Context")].toByteArray();
  }
  return context;
}

QUuid SessionDescription::getEufGuid() const
{
  QUuid eufguid;
  if (fields_.contains(QLatin1String("EUF-GUID")))
  {
    eufguid = QUuid(fields_[QLatin1String("EUF-GUID")].toString());
  }
  return eufguid;
}

QVariant SessionDescription::getField(const QString &name)
{
  return fields_[name];
}

void SessionDescription::addField(const QString &name, const QVariant &value)
{
  fields_[name] = value;
}

void SessionDescription::setField(const QString &name, const QVariant &value)
{
  if (fields_.contains(name))
  {
    fields_[name] = value;
  }
}

quint32 SessionDescription::getSessionId() const
{
  quint32 sid = 0;
  if (fields_.contains(QLatin1String("SessionID")))
  {
    sid = fields_[QLatin1String("SessionID")].toUInt();
  }
  return sid;
}

bool SessionDescription::isNull() const
{
  return fields_.isEmpty();
}

QString SessionDescription::toString()
{
  return QString::fromUtf8(getBytes());
}

}
