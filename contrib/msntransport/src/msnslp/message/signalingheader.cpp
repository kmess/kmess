//
// SignalingHeader class
//
// Summary: Represents a signaling header that can be attached
//    to an outgoing message.
//
// Authors:
//    Gregg Edghill (gregg.edghill at gmail.com)
//
// Copyright (C) 2009 - 2010, Gregg Edghill
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of this software.
//
// THIS LIBRARY IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR
// MODIFY IT UNDER THE TERMS OF THE GNU LESSER GENERAL PUBLIC
// LICENSE AS PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER
// VERSION 2 OF THE LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.
//

#include "signalingheader.h"
#include <QtCore/QChar>
#include <QtCore/QRegExp>

namespace PtpSignaling
{

SignalingHeader::SignalingHeader()
{
}

SignalingHeader::SignalingHeader(const SignalingHeader& header)
{
  name_ = header.name_;
  value_ = header.value_;
  headerparameters_ = header.headerparameters_;
}

SignalingHeader::SignalingHeader(const QString& name, const QVariant& value)
{
  name_ = name;
  value_= value;
  parseParametersFrom(value_.toString());
}

SignalingHeader::~SignalingHeader()
{
}

const QString & SignalingHeader::getName() const
{
  return name_;
}

const QVariant SignalingHeader::getParameter(const QString& name) const
{
  QVariant parameter;
  if (headerparameters_.contains(name.toLower()))
  {
    parameter = headerparameters_[name.toLower()];
  }
  return parameter;
}

QHash<QString, QVariant> SignalingHeader::getParameters() const
{
  return headerparameters_;
}

const QVariant & SignalingHeader::getValue() const
{
  return value_;
}

const QVariant SignalingHeader::getValueNoParameters() const
{
  return QVariant(value_.toString().section(QChar(';'), 0, 0).trimmed());
}

bool SignalingHeader::isNull() const
{
  return (name_.isNull() && value_.isNull());
}

void SignalingHeader::addParameter(const QString& name, const QVariant& value)
{
  // Add the parameter to the name value collection.
  headerparameters_.insert(name.toLower(), value);
  // Set the signaling header value.
  NameValueCollection::ConstIterator item;
  QString newvalue = value_.toString() + QLatin1String(" ");
  for(item=headerparameters_.begin(); item != headerparameters_.end(); ++item)
  {
    newvalue += QString(";%1=%2").arg(item.key(), item.value().toString());
  }

  value_ = newvalue;
}

void SignalingHeader::setName(const QString& name)
{
  name_ = name;
}

void SignalingHeader::setValue(const QVariant& value)
{
  value_ = value;
  parseParametersFrom(value_.toString());
}

void SignalingHeader::parseParametersFrom(const QString& value)
{
  const QString parameterstring = value.section(QChar(';') , 1).trimmed();
  qint32 i = 0;
  QRegExp regex("([^\\s;=]+)=([^;]+)");
  while((i = regex.indexIn(parameterstring, i)) != -1)
  {
    headerparameters_.insert(regex.cap(1).toLower(), regex.cap(2));
    i += regex.matchedLength();
  }
}

}
