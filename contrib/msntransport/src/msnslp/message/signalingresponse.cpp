//
// SignalingResponse
//
// Summary: Captures information contained in a signaling response
//    message.
//
// Authors:
//    Gregg Edghill (gregg.edghill at gmail.com)
//
// Copyright (C) 2006 - 2010, Gregg Edghill
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of this software.
//
// THIS LIBRARY IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR
// MODIFY IT UNDER THE TERMS OF THE GNU LESSER GENERAL PUBLIC
// LICENSE AS PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER
// VERSION 2 OF THE LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.
//

#include "signalingresponse.h"

namespace PtpSignaling
{

SignalingResponse::SignalingResponse(quint32 responseCode, const QString& responsePhrase, const QString& version) : SignalingMessage()
{
  rrLine_ = RequestResponseLine(responseCode, responsePhrase);
  rrLine_.setVersionString(version);
}

SignalingResponse::~SignalingResponse()
{}

quint32 SignalingResponse::getResponseCode() const
{
  return getRequestResponseLine().getResponseCode();
}

const QString & SignalingResponse::getResponsePhrase() const
{
  return getRequestResponseLine().getResponseText();
}

const RequestResponseLine & SignalingResponse::getRequestResponseLine() const
{
  return rrLine_;
}

}
