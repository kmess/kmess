//
// SignalingRequest class
//
// Authors:
//    Gregg Edghill (gregg.edghill at gmail.com)
//
// Copyright (C) 2006 - 2010, Gregg Edghill
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of this software.
//
// THIS LIBRARY IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR
// MODIFY IT UNDER THE TERMS OF THE GNU LESSER GENERAL PUBLIC
// LICENSE AS PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER
// VERSION 2 OF THE LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.
//

#ifndef CLASS_P2P__SLPSIGNALINGREQUEST_H
#define CLASS_P2P__SLPSIGNALINGREQUEST_H

#include "signalingmessage.h"
#include <QtCore/QString>

namespace PtpSignaling
{

/**
 * @brief Captures information contained in a signaling request message.
 *
 * @author Gregg Edghill
 */
class Q_DECL_EXPORT SignalingRequest : public SignalingMessage
{
  public:
    /** @brief Creates a new instance of the class. */
                           SignalingRequest();
    /** @brief Creates a new instance of the class. */
                           SignalingRequest(const QString& method, const QString& uri, const QString& version);
    /** @brief Frees resources and performs other cleanup operations. */
                          ~SignalingRequest();

  protected:
    /** @brief Gets the request or response start-line. */
    const RequestResponseLine & getRequestResponseLine() const;

  public:
    /** @brief Gets a string containing the method type of the request. */
    const QString &        getMethodType() const;
    /** @brief Gets a string containing the request uri. */
    const QString &        getUri() const;

  private:
    RequestResponseLine    rrLine_;

}; // SignalingRequest
}

#endif
