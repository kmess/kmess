//
// SignalingMessage
//
// Summary: Captures the information that is common to a Msnslp
//    request or response message.
//
// Authors:
//    Gregg Edghill (gregg.edghill at gmail.com)
//
// Copyright (C) 2006 - 2010, Gregg Edghill
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of this software.
//
// THIS LIBRARY IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR
// MODIFY IT UNDER THE TERMS OF THE GNU LESSER GENERAL PUBLIC
// LICENSE AS PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER
// VERSION 2 OF THE LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.
//

#include "signalingmessage.h"
#include <QtCore/QTextStream>

namespace PtpSignaling
{

SignalingMessage::SignalingMessage()
{
  generateDefaultSignalingHeaders();
}

SignalingMessage::~SignalingMessage()
{}

const QString SignalingMessage::getCallId() const
{
  if (isHeaderPresent(QLatin1String("Call-ID")))
  {
    return getSignalingHeader(QLatin1String("Call-ID")).getValue().toString();
  }

  return QLatin1String("");
}

qint32 SignalingMessage::getContentLength() const
{
  if (isHeaderPresent(QLatin1String("Content-Length")))
  {
    return getSignalingHeader(QLatin1String("Content-Length")).getValue().toInt();
  }

  return 0;
}

const ContentType SignalingMessage::getContentType() const
{
  if (isHeaderPresent(QLatin1String("Content-Type")))
  {
    return ContentType(getSignalingHeader(QLatin1String("Content-Type")).getValue().toString());
  }

  return ContentType(QLatin1String(""));
}

quint32 SignalingMessage::getCorrelationId() const
{
  return correlationId_;
}

qint32 SignalingMessage::getCSeq() const
{
  if (isHeaderPresent(QLatin1String("CSeq")))
  {
    return getSignalingHeader(QLatin1String("CSeq")).getValue().toInt();
  }

  return 0;
}

const SignalingHeader SignalingMessage::getFromHeader() const
{
  return getSignalingHeader(QLatin1String("From"));
}

quint32 SignalingMessage::getId() const
{
  return id_;
}

SignalingHeader SignalingMessage::getSignalingHeader(const QString& name) const
{
  SignalingHeader header;
  for (qint32 i=0; i < headers_.size(); ++i)
  {
    if (name == headers_[i].getName())
    {
      header = headers_[i];
      break;
    }
  }

  return header;
}

const QList<SignalingHeader> & SignalingMessage::getSignalingHeaders() const
{
  return headers_;
}

QList<SignalingHeader> & SignalingMessage::getSignalingHeaders()
{
  return headers_;
}

const QByteArray SignalingMessage::getMessageBody() const
{
  return messageBody_;
}

const QByteArray SignalingMessage::getRawMessageByteArray() const
{
  QByteArray rawMessage;
  QTextStream stream(&rawMessage, QIODevice::WriteOnly);

  const QString CrLf = QLatin1String("\r\n");
  // Write the request/response line to the stream
  stream << getRequestResponseLine().toString() << CrLf;

  foreach(SignalingHeader header, headers_)
  {
    // Write the message headers to the stream.
    stream << header.getName() << ": " << header.getValue().toString() << CrLf;
  }

  stream << CrLf;

  if (getContentLength() > 0)
  {
    // If the message has a body, write the body content to the stream.
    // Append a null character; switchboard error 282 is sent if not
    // present because without null character there will be a mismatch
    // between the content length and the message data length.
    stream << messageBody_ << '\0';
  }

  return rawMessage;
}

const SignalingHeader SignalingMessage::getToHeader() const
{
  return getSignalingHeader(QLatin1String("To"));
}

const QString SignalingMessage::getVersion() const
{
  return getRequestResponseLine().getVersionString();
}

const SignalingHeader SignalingMessage::getViaHeader() const
{
  return getSignalingHeader(QLatin1String("Via"));
}

void SignalingMessage::setCorrelationId(const quint32 id)
{
  correlationId_ = id;
}

void SignalingMessage::setId(const quint32 id)
{
  id_ = id;
}

void SignalingMessage::setMessageBody(const QByteArray& body)
{
  // Set the message body.
  messageBody_ = body;
  // Get the message body size.
  qint32 contentLength = messageBody_.length();
  // If the content length is greater than zero,
  // increase the content length value by one
  // for the null character that is appended.
  contentLength = (contentLength != 0) ? contentLength + 1 : contentLength;
  // Set the content length header
  setSignalingHeader("Content-Length", contentLength);
}

void SignalingMessage::addSignalingHeader(const SignalingHeader& header)
{
  // Determine whether a signaling header with the same
  // name is already present.
  if (!isHeaderPresent(header.getName()))
  {
    // If not, append the new signaling header
    headers_.append(header);
  }
  else
  {
    // Otherwise, set the value of the existing signaling header
    setSignalingHeader(header.getName(), header.getValue());
  }
}

bool SignalingMessage::isHeaderPresent(const QString& headerName) const
{
  bool headerfound = false;
  for (qint32 i=0; i < headers_.size(); ++i)
  {
    if (headerName == headers_[i].getName())
    {
      headerfound = true;
      break;
    }
  }

  return headerfound;
}

void SignalingMessage::setSignalingHeader(const QString& name, const QVariant& value)
{
  for (qint32 i=0; i < headers_.size(); ++i)
  {
    if (name == headers_[i].getName())
    {
      headers_[i] = SignalingHeader(name, value);
      break;
    }
  }
}

void SignalingMessage::generateDefaultSignalingHeaders()
{
  // Add the default signaling headers that are present
  // for an offer, answer or negotiation message.

  // NOTE Default signaling header values will be overriden

  addSignalingHeader(SignalingHeader(QLatin1String("To"), QLatin1String("<msnmsgr:%s>")));
  addSignalingHeader(SignalingHeader(QLatin1String("From"), QLatin1String("<msnmsgr:%s>")));
  addSignalingHeader(SignalingHeader(QLatin1String("Via"), QLatin1String("MSNSLP/1.0/TLP ;branch=%s")));
  addSignalingHeader(SignalingHeader(QLatin1String("CSeq"), 0));
  addSignalingHeader(SignalingHeader(QLatin1String("Call-ID"), QLatin1String("%s")));
  addSignalingHeader(SignalingHeader(QLatin1String("Max-Forwards"), 0));
  addSignalingHeader(SignalingHeader(QLatin1String("Content-Type"), QLatin1String("%s")));
  addSignalingHeader(SignalingHeader(QLatin1String("Content-Length"), 0));
}

}
