//
// SignalingResponse class
//
// Authors:
//    Gregg Edghill (gregg.edghill at gmail.com)
//
// Copyright (C) 2006 - 2010, Gregg Edghill
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of this software.
//
// THIS LIBRARY IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR
// MODIFY IT UNDER THE TERMS OF THE GNU LESSER GENERAL PUBLIC
// LICENSE AS PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER
// VERSION 2 OF THE LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.
//

#ifndef CLASS_P2P__SLPSIGNALINGRESPONSE_H
#define CLASS_P2P__SLPSIGNALINGRESPONSE_H

#include "signalingmessage.h"
#include <QtCore/QString>

namespace PtpSignaling
{

/**
 * @brief Captures information contained in a signaling response message.
 *
 * @author Gregg Edghill
 */
class Q_DECL_EXPORT SignalingResponse : public SignalingMessage
{
  public:
    /** @brief Defines the possible response codes used when send a response. */
    enum SLP_RESPONSE_CODE
    {
      SLPRC_OK                    = 200,
      SLPRC_NOT_FOUND             = 404,
      SLPRC_NO_SUCH_CALL          = 481,
      SLPRC_INTERNAL_ERROR        = 500,
      SLPRC_VERSION_NOT_SUPPORTED = 505,
      SLPRC_DECLINE               = 603,
      SLPRC_UNACCEPTABLE          = 606
    };

  public:
    /** @brief Creates a new instance of the class. */
                           SignalingResponse(quint32 responseCode, const QString& responsePhrase, const QString& version);
    /** @brief Frees resources and performs other cleanup operations. */
                          ~SignalingResponse();

  protected:
    /** @brief Gets the request or response start-line. */
    const RequestResponseLine & getRequestResponseLine() const;

  public:
    /** @brief Gets the response code. */
    quint32                getResponseCode() const;
    /** @brief Gets a string containing the response phrase. */
    const QString &        getResponsePhrase() const;

  private:
    RequestResponseLine    rrLine_;

}; // SignalingResponse
}

#endif
