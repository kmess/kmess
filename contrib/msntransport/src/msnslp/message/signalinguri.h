//
// SignalingUri class
//
// Authors:
//    Gregg Edghill (gregg.edghill at gmail.com)
//
// Copyright (C) 2009 - 2010, Gregg Edghill
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of this software.
//
// THIS LIBRARY IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR
// MODIFY IT UNDER THE TERMS OF THE GNU LESSER GENERAL PUBLIC
// LICENSE AS PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER
// VERSION 2 OF THE LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.
//

#ifndef CLASS_P2P__SIGNALINGURI_H
#define CLASS_P2P__SIGNALINGURI_H

#include <QtCore/QString>

namespace PtpSignaling
{

/**
 * @brief Represents a unqiue address used to specify a signaling target.
 *
 * @author Gregg Edghill
 */
class SignalingUri
{
  public:
    /** @brief Creates a new instance of the class. */
                        SignalingUri();
    /** @brief Creates a new instance of the class. */
                        SignalingUri(const QString& value);
    /** @brief Creates a new instance of the class. */
                        SignalingUri(const QString& scheme, const QString& address, const QString& endpointId=QString());
    /** @brief Frees resources and performs other cleanup operations. */
                       ~SignalingUri();
  public:
    /** @brief Gets the endpoint id used to identify the endpoint of the signaling target. */
    QString             getEndpointId() const;
    /** @brief Gets a string containing the uri scheme name. */
    QString             getScheme() const;
    /** @brief Gets a string containing the uri user. */
    QString             getUser() const;
    /** @brief Gets a string containing the uri user address. */
    QString             getUserAddress() const;
    /** @brief Sets the id of the endpoint of the signaling target. */
    void                setEndpointId(const QString& endpointId);
    /** @brief Sets the uri scheme name. */
    void                setScheme(const QString& scheme);
    /** @brief Sets the user address part of the uri. */
    void                setUserAddress(const QString& address);
    /** @brief Gets a string representation of the signaling address. */
    QString             toString() const;

  public:
    /** @brief Overloads the == operator. */
    bool                operator==(const SignalingUri& other) const;
    /** @brief Overloads the != operator. */
    bool                operator!=(const SignalingUri& other) const;

  private:
    QString             address_;
    QString             endpointId_;
    QString             scheme_;

}; // SignalingUri
}

#endif
