//
// RequestResponseLine class
//
// Authors:
//    Gregg Edghill (gregg.edghill at gmail.com)
//
// Copyright (C) 2009 - 2010, Gregg Edghill
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of this software.
//
// THIS LIBRARY IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR
// MODIFY IT UNDER THE TERMS OF THE GNU LESSER GENERAL PUBLIC
// LICENSE AS PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER
// VERSION 2 OF THE LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.
//

#ifndef CLASS_P2P__SLPREQUESTRESPONSE_H
#define CLASS_P2P__SLPREQUESTRESPONSE_H

#include <QtCore/QString>
#include <QtCore/QVariant>

namespace PtpSignaling
{

/**
 * @brief Represents the request or response start-line of a signaling message.
 *
 * @author Gregg Edghill
 */
class RequestResponseLine
{
  public:
    /** @brief Creates a new instance of the class. */
                           RequestResponseLine();
    /** @brief Creates a new instance of the class. */
                           RequestResponseLine(const RequestResponseLine& rrLine);
    /** @brief Creates a new instance of the class. */
                           RequestResponseLine(quint32 responseCode, const QString& responseText);
    /** @brief Creates a new instance of the class. */
                           RequestResponseLine(const QString& requestMethod, const QString& requestUri);

  public:
    /** @brief Gets a value indicating whether the request/response start-line is valid. */
    bool                   isValid() const;
    /** @brief Gets the method type of a request start-line. */
    const QString &        getRequestMethodType() const;
    /** @brief Gets the uri of a request start-line. */
    const QString &        getRequestUri() const;
    /** @brief Gets the response code of a response start-line. */
    quint32                getResponseCode() const;
    /** @brief Gets the response phrase of the response start-line. */
    const QString &        getResponseText() const;
    /** @brief Gets the SLP version. */
    const QString &        getVersionString() const;
    /** @brief Sets the SLP version. */
    void                   setVersionString(const QString& slpVersion);
    /** @brief Gets a string representation of the start-line. */
    const QString          toString() const;

  private:
    QString                requestMethod_;
    QString                requestUri_;
    QVariant               responseCode_;
    QString                responseText_;
    QString                slpVersion_;
    quint8                 type_;

}; // RequestResponseLine
}

#endif
