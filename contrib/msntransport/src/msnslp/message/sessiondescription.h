//
// SessionDescription class
//
// Authors:
//    Gregg Edghill (gregg.edghill at gmail.com)
//
// Copyright (C) 2009 - 2010, Gregg Edghill
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of this software.
//
// THIS LIBRARY IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR
// MODIFY IT UNDER THE TERMS OF THE GNU LESSER GENERAL PUBLIC
// LICENSE AS PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER
// VERSION 2 OF THE LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.
//

#ifndef CLASS_P2P__SESSIONDESCRIPTION_H
#define CLASS_P2P__SESSIONDESCRIPTION_H

#include <QtCore/QByteArray>
#include <QtCore/QHash>
#include <QtCore/QString>
#include <QtCore/QUuid>
#include <QtCore/QVariant>

namespace PtpSignaling
{

/**
 * @brief Represents a data structure which contained the content of a signaling session description.
 *
 * @author Gregg Edghill
 */
class Q_DECL_EXPORT SessionDescription
{
  public:
    /** @brief Creates a new instance of the class. */
                        SessionDescription();
    /** @brief Frees resources and performs other cleanup operations. */
                       ~SessionDescription();
  public:
    /** @brief Added the description field with the specified name and value. */
//    void                add(const DescriptionField& field);
    /** @brief Added the description field with the specified name and value. */
    void                addField(const QString& name, const QVariant& value);
    /** @brief Gets the application id. */
    quint32             getApplicationId() const;
    /** @brief Gets an array of bytes containing the session description. */
    QByteArray          getBytes() const;
    /** @brief Gets the application context information. */
    QByteArray          getContext() const;
    /** @brief Gets the euf guid. */
    QUuid               getEufGuid() const;
    /** @brief Gets the description field with the specified name. */
    QVariant            getField(const QString& name);
    /** @brief Gets the session id. */
    quint32             getSessionId() const;
    /** @brief Gets a value indicating whether the description is valid. */
    bool                isNull() const;
    /** @bried Sets the description field with the specified name and value. */
    void                setField(const QString& name, const QVariant& value);
    /** @brief Gets a string containing the session description. */
    QString             toString();

  private:
    QHash<QString, QVariant> fields_;
//    NameValueCollection fields_;
};
}

#endif
