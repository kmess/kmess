//
// RealTimeAddress class
//
// Authors:
//    Gregg Edghill (gregg.edghill at gmail.com)
//
// Copyright (C) 2009 - 2010, Gregg Edghill
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of this software.
//
// THIS LIBRARY IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR
// MODIFY IT UNDER THE TERMS OF THE GNU LESSER GENERAL PUBLIC
// LICENSE AS PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER
// VERSION 2 OF THE LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.
//

#ifndef CLASS_P2P__REALTIMEADDRESS_H
#define CLASS_P2P__REALTIMEADDRESS_H

#include <QtCore/QString>

namespace PtpSignaling
{

/**
 * @brief Represents a realtime address used to identify a signaling target.
 *
 * @author Gregg Edghill
 */
class Q_DECL_EXPORT RealTimeAddress
{
  public:
    /** @brief Creates a new instance of the class. */
                        RealTimeAddress(const QString& value=QString());
    /** @brief Creates a new instance of the class. */
                        RealTimeAddress(const QString& address, const QString& endpointId);
    /** @brief Frees resources and performs other cleanup operations. */
                       ~RealTimeAddress();
  public:
    /** @brief Gets a string containing the address. */
    QString             getAddress() const;
    /** @brief Gets the endpoint id used to identify the endpoint of the signaling target. */
    QString             getEndpointId() const;
    /** @brief Sets the address. */
    void                setAddress(const QString& address);
    /** @brief Sets the id of the endpoint of the signaling target. */
    void                setEndpointId(const QString& endpointId);
    /** @brief Gets a value indicating whether the address supports MPOP. */
    bool                isMpopSupported() const;
    /** @brief Overrides the == operator .*/
    bool                operator==(const RealTimeAddress& other);
    /** @brief Overrides the != operator .*/
    bool                operator!=(const RealTimeAddress& other);

  private:
    QString             address_;
    QString             endpointId_;

}; // RealTimeAddress
}

#endif
