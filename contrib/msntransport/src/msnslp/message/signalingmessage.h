//
// SignalingMessage class
//
// Authors:
//    Gregg Edghill (gregg.edghill at gmail.com)
//
// Copyright (C) 2006 - 2010, Gregg Edghill
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of this software.
//
// THIS LIBRARY IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR
// MODIFY IT UNDER THE TERMS OF THE GNU LESSER GENERAL PUBLIC
// LICENSE AS PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER
// VERSION 2 OF THE LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.
//

#ifndef CLASS_P2P__SLPSIGNALINGMESSAGE_H
#define CLASS_P2P__SLPSIGNALINGMESSAGE_H

#include "contenttype.h"
#include "signalingheader.h"
#include "requestresponseline.h"
#include <QtCore/QString>
#include <QtCore/QVariant>

namespace PtpSignaling
{

/**
 * @brief Captures information that is common in a signaling request or response message.
 *
 * SignalingMessage captures information similar to that in a session initiation protocol
 * (SIP) signaling message. Each signaling message starts with a request/response string which
 * indicates whether the message is a request or a response followed by signaling headers and
 * a content description body (MIME collection), if present.
 *
 * Slp requests have the request/response string format, [method] MSNMSGR:[uri] MSNSLP/1.0
 * and slp response have the request/response string format, MSNSLP/1.0 [response code] [response text]
 * Currently, the supported (observed) Slp methods for slp requests are the INVITE, ACK and BYE
 * methods.
 *
 * @code
 *
 * INVITE MSNMSGR:peer@maildomain.com MSNSLP/1.0
 * To: <msnmsgr:peer@maildomain.com>
 * From: <msnmsgr:localuser@maildomain.com>
 * Via: MSNSLP/1.0/TLP ;branch={4967EE9D-0649-42CC-B989-317487E96182}
 * CSeq: 0
 * Call-ID: {8DF95352-F0B0-4402-A355-7551F691870C}
 * Max-Forwards: 0
 * Content-Type: application/x-msnmsgr-sessionreqbody
 * Content-Length: 335
 *
 * EUF-GUID: {A4268EEC-FEC5-49E5-95C3-F126696BDBF6}
 * SessionID: 35589
 * AppID: 12
 * Context: [base64 encoded string]
 * Capabilities-Flags: 1
 * RequestFlags: 18
 *
 * @endcode
 *
 * @code
 *
 * MSNSLP/1.0 200 OK
 * To: <msnmsgr:localuser@maildomain.com>
 * From: <msnmsgr:peer@maildomain.com>
 * Via: MSNSLP/1.0/TLP ;branch={4967EE9D-0649-42CC-B989-317487E96182}
 * CSeq: 1
 * Call-ID: {8DF95352-F0B0-4402-A355-7551F691870C}
 * Max-Forwards: 0
 * Content-Type: application/x-msnmsgr-sessionrespbody
 * Content-Length: 22
 *
 * SessionID: 35589
 *
 * @endcode
 *
 * @code
 *
 * BYE MSNMSGR:peer@maildomain.com MSNSLP/1.0
 * To: <msnmsgr:peer@maildomain.com>
 * From: <msnmsgr:localuser@maildomain.com>
 * Via: MSNSLP/1.0/TLP ;branch={DB187185-BFB2-4925-AD4C-05A7C0579AA6}
 * CSeq: 0
 * Call-ID: {8DF95352-F0B0-4402-A355-7551F691870C}
 * Max-Forwards: 0
 * Content-Type: application/x-msnmsgr-sessionclosebody
 * Content-Length: 21
 *
 * SessionID: 35589
 *
 * @endcode
 *
 * @code
 *
 * MSNSLP/1.0 606 Unacceptable
 * To: <msnmsgr:peer@maildomain.com>
 * From: <msnmsgr:localuser@maildomain.com>
 * Via: MSNSLP/1.0/TLP ;branch={13800F6B-6614-4B4C-B32D-5B6FD4DE4C6C}
 * CSeq: 1
 * Call-ID: {ea81cb1f-6fbe-4a70-9747-aeb305f8d995}
 * Max-Forwards: 0
 * Content-Type: application/x-msnmsgr-session-failure-respbody
 * Content-Length: 62
 *
 * SessionID: 16152
 * SChannelState: 0
 * Capabilities-Flags: 1
 *
 * @endcode
 *
 * Each Slp message contains a <tt>To</tt> header, a <tt>From</tt> header,
 * a <tt>Via</tt> header, a <tt>CSeq</tt> header, a <tt>Call-ID</tt> header,
 * a <tt>Max-Forwards</tt> header, a <tt>Content-Type</tt> header and a
 * <tt>Content-Length</tt> header.
 *
 * @author Gregg Edghill
 */
class Q_DECL_EXPORT SignalingMessage
{
  public:
    /** @brief Creates a new instance of the class. */
                           SignalingMessage();
    /** @brief Frees resources and performs other cleanup operations. */
    virtual               ~SignalingMessage();

  protected:
    /** @brief Gets the request or response start-line. */
    virtual const RequestResponseLine & getRequestResponseLine() const = 0;

  public:
    /** @brief Gets the call ID associated with the message. */
    const QString          getCallId() const;
    /** @brief Gets a value indicating the length in bytes of the message body or zero if none was specified. */
    qint32                 getContentLength() const;
    /** @brief Gets the content type of the message body. */
    const ContentType      getContentType() const;
    /** @brief Gets the id of a message this signaling message is associated with. */
    quint32                getCorrelationId() const;
    /** @brief Get a value indicating the sequence number of the message. */
    qint32                 getCSeq() const;
    /** @brief Gets the "From" header of the message. */
    const SignalingHeader  getFromHeader() const;
    /** @brief Gets a value used to uniquely identify the signaling message. */
    quint32                getId() const;
    /** @brief Gets the signaling header with the specified name. */
    SignalingHeader        getSignalingHeader(const QString& name) const;
    /** @brief Gets the signaling headers of the message. */
    const QList<SignalingHeader> & getSignalingHeaders() const;
    /** @brief Gets the signaling headers of the message. */
    QList<SignalingHeader> & getSignalingHeaders();
    /** @brief Gets the message body as a byte array. */
    const QByteArray       getMessageBody() const;
    /** @brief Gets an array of bytes containing the signaling message. */
    const QByteArray       getRawMessageByteArray() const;
    /** @brief Gets the "To" header of the message. */
    const SignalingHeader  getToHeader() const;
    /** @brief Gets a string containing the protocol version. */
    const QString          getVersion() const;
    /** @brief Gets the "Via" header of the message. */
    const SignalingHeader  getViaHeader() const;
    /** @brief Add the supplied signaling header to the header list. */
    void                   addSignalingHeader(const SignalingHeader& header);
    /** @brief Gets a value indicating whether the signaling header with the specified name exists. */
    bool                   isHeaderPresent(const QString& headerName) const;
    /** @brief Sets the id of a message this signaling message is associated with. */
    void                   setCorrelationId(const quint32 id);
    /** @brief Sets the id used to uniquely identify the signaling message. */
    void                   setId(const quint32 id);
    /** @brief Sets the message body. */
    void                   setMessageBody(const QByteArray& body);
    /** @brief Sets the signaling header with the specified name and value. */
    void                   setSignalingHeader(const QString& name, const QVariant& value);

  private:
    /** @brief Creates the default list of signaling headers. */
    void                   generateDefaultSignalingHeaders();

  private:
    quint32                correlationId_;
    QList<SignalingHeader> headers_;
    quint32                id_;
    QByteArray             messageBody_;

}; // SignalingMessage

}

#endif
