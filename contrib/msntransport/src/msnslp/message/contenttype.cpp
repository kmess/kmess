//
// ContentType class
//
// Summary: Represents the information from a MIME Content-Type header
//
// Authors:
//    Gregg Edghill (gregg.edghill at gmail.com)
//
// Copyright (C) 2009 - 2010, Gregg Edghill
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of this software.
//
// THIS LIBRARY IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR
// MODIFY IT UNDER THE TERMS OF THE GNU LESSER GENERAL PUBLIC
// LICENSE AS PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER
// VERSION 2 OF THE LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.
//

#include "contenttype.h"
#include <QtCore/QChar>
#include <QtCore/QRegExp>

namespace PtpSignaling
{

ContentType::ContentType(const QString& contentType)
{
  contentType_ = contentType;
  const QString parameterstring = contentType_.section(QChar(';') , 1).trimmed();
  qint32 i = 0;
  QRegExp regex("([^\\s;=]+)=([^;]+)");
  while((i = regex.indexIn(parameterstring, i)) != -1)
  {
    parameters_.insert(regex.cap(1), regex.cap(2));
    i += regex.matchedLength();
  }
}

ContentType::~ContentType()
{
}

const QString ContentType::getCharSet() const
{
  return getParameters().value(QLatin1String("charset"));
}

const QString ContentType::getMediaType() const
{
  return contentType_.section(QChar(';'), 0, 0).trimmed();
}

const StringDictionary & ContentType::getParameters() const
{
  return parameters_;
}

const QString & ContentType::toString() const
{
  return contentType_;
}

}
