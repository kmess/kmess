//
// SignalingRequest
//
// Summary: Captures information contained in a signaling request
//    message.
//
// Authors:
//    Gregg Edghill (gregg.edghill at gmail.com)
//
// Copyright (C) 2006 - 2010, Gregg Edghill
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of this software.
//
// THIS LIBRARY IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR
// MODIFY IT UNDER THE TERMS OF THE GNU LESSER GENERAL PUBLIC
// LICENSE AS PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER
// VERSION 2 OF THE LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.
//

#include "signalingrequest.h"

namespace PtpSignaling
{

SignalingRequest::SignalingRequest(): SignalingMessage()
{
}

SignalingRequest::SignalingRequest(const QString& method, const QString& uri, const QString& version) : SignalingMessage()
{
  rrLine_ = RequestResponseLine(method, uri);
  rrLine_.setVersionString(version);
}

SignalingRequest::~SignalingRequest()
{}

const QString & SignalingRequest::getMethodType() const
{
  return getRequestResponseLine().getRequestMethodType();
}

const RequestResponseLine & SignalingRequest::getRequestResponseLine() const
{
  return rrLine_;
}

const QString & SignalingRequest::getUri() const
{
  return getRequestResponseLine().getRequestUri();
}

}
