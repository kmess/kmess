//
// SlpPeerToPeerEndpoint class
//
// Authors:
//    Gregg Edghill (gregg.edghill at gmail.com)
//
// Copyright (C) 2006 - 2010, Gregg Edghill
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of this software.
//
// THIS LIBRARY IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR
// MODIFY IT UNDER THE TERMS OF THE GNU LESSER GENERAL PUBLIC
// LICENSE AS PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER
// VERSION 2 OF THE LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.
//

#ifndef CLASS_P2P__SLPPEERTOPEERENDPOINT_H
#define CLASS_P2P__SLPPEERTOPEERENDPOINT_H

#include "rtcsignalingendpoint.h"

namespace PtpSignaling
{

/**
 * @brief Represents a peer to peer endpoint used for communication with other endpoints.
 *
 * The peer to peer endpoint is divided into two sub-layers, the application layer
 * which hosts End User Features (EUFs) used to send and receive data and the signaling
 * layer used to initiate and terminate signaling sessions.  The peer to peer endpoint
 * also provides a secure layer (SChannel) used to securely send or receive data (directly)
 * to another endpoint without the use of the underlying transport layer.  The signaling
 * layer is used to negotiate and establish the secure channel.
 *
 * @code
 *                                                   +---+
 * +------------------------+   logical connection   |   |      +------------------------+
 * | EUF/Applications Layer | <--------------------> |   | <--> | EUF/Applications Layer |
 * |     +------------------+                        | n |      +------------------+     |
 * |     | +----------------+    direct connection   | e |      +----------------+ |     |  --,
 * |     | |  Secure Layer  | <====================> | t | <==> |  Secure Layer  | |     |    |
 * +-----+ +----------------+                        | w |      +----------------+ +-----+    | session
 * +------------------------+   logical connection   | o |      +------------------------+    | client
 * |     Signaling Layer    | <--------------------> | r | <--> |     Signaling Layer    |    |
 * +------------------------+                        | k |      +------------------------+  --'
 * +------------------------+   logical connection   |   |      +------------------------+
 * |    Transport  Layer    | <--------------------> |   | <--> |    Transport  Layer    |
 * +------------------------+                        |   |      +------------------------+
 *                                                   +---+
 * @endcode
 *
 * The signaling layer uses a subset of the session initialization protocol (SIP) to
 * establish sessions between participants.
 *
 * @author Gregg Edghill
 */
class Q_DECL_EXPORT SlpPeerToPeerEndpoint : public RtcSignalingEndpoint
{
  Q_OBJECT

  public:
    /** @brief Creates a new instance of the class. */
                      SlpPeerToPeerEndpoint(const RealTimeAddress& localTarget, const RealTimeAddress& sessionTarget, QObject *pobject);
    /** @brief Frees resources and performs other cleanup operations. */
                     ~SlpPeerToPeerEndpoint();
  public:
    /** @brief Gets a value indicating whether the endpoint is shutting down. */
    bool              isShuttingDown() const;

  protected:
    /** @brief Called when a signaling session is received. */
    virtual void      onSessionReceived(const SessionDescription& sd, ProxySignalingSession *session) const;

  public:
    /** @brief Terminates active sessions and cleans up resources.  The endpoint is no longer usable. */
    virtual void      shutdown();

  private:

  Q_DISABLE_COPY(SlpPeerToPeerEndpoint)

}; //SlpPeerToPeerEndpoint
}

#endif
