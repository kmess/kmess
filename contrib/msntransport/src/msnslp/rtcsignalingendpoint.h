//
// RtcSignalingEndpoint class
//
// Authors:
//    Gregg Edghill (gregg.edghill at gmail.com)
//
// Copyright (C) 2009 - 2010, Gregg Edghill
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of this software.
//
// THIS LIBRARY IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR
// MODIFY IT UNDER THE TERMS OF THE GNU LESSER GENERAL PUBLIC
// LICENSE AS PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER
// VERSION 2 OF THE LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.
//

#ifndef CLASS_P2P__RTCSIGNALINGENDPOINT_H
#define CLASS_P2P__RTCSIGNALINGENDPOINT_H

#include <QtCore/QObject>
#include "contentdescription.h"
#include "signalingsessionclient.h"
#include "message/sessiondescription.h"
#include "message/realtimeaddress.h"
#include "message/signalingrequest.h"
#include "message/signalingresponse.h"
#include "message/signalinguri.h"
#include "../transport/transportimpl.h"
using namespace PtpCore;
using namespace PtpTransport;

namespace PtpSignaling
{

class Dialog;
class ProxySignalingSession;
typedef QHash<quint32, ProxySignalingSession*> SignalingSessionDictionary;
class Transaction;

/**
* @brief Represents a real time communications signaling endpoint.
*
* @author Gregg Edghill
*/
class RtcSignalingEndpoint : public QObject, public SignalingSessionClient//, public SignalingSessionManager
{
  Q_OBJECT

  public:
    /** @brief Create a new instance of the RtcSignalingEndpoint class. */
                      RtcSignalingEndpoint(const RealTimeAddress& localTarget, const RealTimeAddress& sessionTarget, QObject *pobject);
    /** @brief Frees resources and performs other cleanup operations. */
    virtual          ~RtcSignalingEndpoint();

  protected:
    /** @brief Created a new and unique session id. */
    quint32           generateNewSessionId();
    /** @brief Adds the supplied dialog to the call map. */
    void              addDialogToCallMap(Dialog *dialog);
    /** @brief Clean up all resources created for the session. */
    void              cleanupSignalingSessionResources(const quint32 sessionkey);
    /** @brief Creates a request message. */
    SignalingRequest  buildRequest(const QString& method, const QString& callId);
    /** @brief Creates a response message. */
    SignalingResponse buildResponse(const SignalingRequest& request, const quint16 responseCode);
    /** @brief Returns a dialog with the specified call id. */
    Dialog *          getDialogByCallId(const QUuid& callId);
    /** @brief Returns a dialog associated with the specified session key. */
    Dialog *          getDialogBySessionKey(const quint32 sessionkey);
    /** @brief Gets the response text phrase associated with the specified response code. */
    const QString     mapResponseCodeToResponseText(const quint16 responseCode);
    /** @brief Called when a new signaling session is received. */
    virtual void      onSessionReceived(const SessionDescription& sd, ProxySignalingSession *session) const=0;
    /** @brief Rejects an incoming signaling request with the specified error code.*/
    void              rejectWithErrorCode(const SignalingRequest& request, const quint16 responseCode);
    /** @brief Rejects an incoming signaling request with the specified error code.*/
    void              rejectWithErrorCode(const SignalingRequest& request, const quint16 responseCode, const ContentDescription& contentDescription);
    /** @brief Removes the dialog with the specified call id. */
    void              removeDialogFromCallMap(const QUuid& callId);
    /** @brief Sends the specified signaling message. */
    void              sendSignalingMessage(SignalingMessage& message, bool priority=false);
    /** @brief Sends a signaling request. */
    void              sendSignalingRequest(const QString& method, const ContentDescription& contentDescription, const QString& callId);
    /** @brief Sends a signaling response. */
    void              sendSignalingResponse(const quint16 responseCode, const ContentDescription& contentDescription, const SignalingRequest& request);
    /** @brief Terminates a session by rejecting the incoming INVITE request . */
    void              terminateWithRejection(const quint16 responseCode, const ContentDescription& contentDescription, const SignalingRequest& request);
    /** @brief Accepts the supplied signaling session with the specified answer. */
    virtual void      acceptSessionWithAnswer(ProxySignalingSession *pssession, const SessionDescription& answerbody);
    /** @brief Establishs the supplied signaling session with the specified offer. */
    virtual void      establishSessionWithOffer(ProxySignalingSession *pssession, const SessionDescription& offerbody);
    /** @brief Terminates the supplied signaling session with the specified close description. */
    virtual void      terminateSession(ProxySignalingSession *pssession, const SessionDescription& closebody);
    /** @brief Terminates the supplied signaling session with the specified answer. */
    virtual void      terminateSessionWithDecline(ProxySignalingSession *pssession, const SessionDescription& answerbody);

  public:
    /** @brief When overriden in a derived class, gets a value indicating whether the endpoint is shutting down. */
    virtual bool      isShuttingDown() const = 0;
    /** @brief Gets the address of the remote endpoint/user. */
    RealTimeAddress   getSessionTarget() const;
    /** @brief Gets the underlying transport of the endpoint. */
    TransportImpl *   getTransport();
    /** @brief Creates a new signaling session. */
//    ProxySignalingSession *createSession(bool incoming=false);

    /** @brief Terminates active sessions and cleans up resources.  The endpoint is no longer usable. */
    virtual void      shutdown();

  private Q_SLOTS:
    /** @brief Called when a dialog is established. */
    void              dialog_OnEstablish();
    /** @brief Called when a dialog terminates. */
    void              dialog_OnTerminate();
    /** @brief Called when the underlying transport establishes a connection to the remote endpoint for the first time. */
    void              transport_OnInitialConnectionEstablished();
    /** @brief Called when the underlying transport loses its connection to the remote endpoint. */
    void              transport_OnDisconnected();
    /** @brief Called when the underlying transport loses its connection to the remote endpoint. */
    void              transport_OnError(const quint32 destination);
    /** @brief Called when raw message data is received from the underlying transport. */
    void              transport_OnMessageDataReceived(const QByteArray& messageData, const qint32 id, const qint32 correlationId);

  private:
    /** @brief Gets a value indicating whether the supplied signaling request should be honoured. */
    bool              shouldHonourDirectConnectionOfferRequest(const SignalingRequest& request);

  private:
    /** @brief Called when an ACK request is received. */
    void              onAcknowledge(const SignalingRequest& request);
    /** @brief Called when a direct connection offer request in INVITE response is received. */
    void              onDirectConnectionOfferInInviteResponse(const SignalingRequest& request);
    /** @brief Called when a direct connection negotiation reINVITE request is received. */
    void              onDirectConnectionOfferInReInvite(const SignalingRequest& request);
    /** @brief Called when a session offer INVITE request is received. */
    void              onInitialSessionOffer(const SignalingRequest& request);
    /** @brief Called when a request message is received. */
    void              onRequestMessage(const SignalingRequest& request);
    /** @brief Called when a terminate (BYE) request is received. */
    void              onTerminateSession(const SignalingRequest& request);
    /** @brief Called when a direct connection offer response message is received. */
    void              onDirectConnectionOfferAccepted(const SignalingResponse& response);
    /** @brief Called when an internal error response message is received. */
    void              onInternalError(const SignalingResponse& response);
    /** @brief Called when the remote endpoint indicates that the call ID specified in a received request is not valid. */
    void              onNoSuchCall(const SignalingResponse& response);
    /** @brief Called when a session target not found response message is received. */
    void              onRecipientUriNotFound(const SignalingResponse& response);
    /** @brief Called when a response message is received. */
    void              onResponseMessage(const SignalingResponse& response);
    /** @brief Called when a session offer accept response message is received. */
    void              onSessionOfferAccepted(const SignalingResponse& response);
    /** @brief Called when a session offer decline response message is received. */
    void              onSessionOfferDeclined(const SignalingResponse& response);
    /** @brief Called when a direct connection offer decline response message is received. */
    void              onDirectConnectionOfferDeclined(const SignalingResponse& response);

  Q_SIGNALS:
    /** @brief Indicates that the endpoint is connected. */
    void              connected();
    /** @brief Indicates that the endpoint is connecting. */
    void              connecting();
    /** @brief Indicates that the ednpoint is disconnected. */
    void              disconnected();
    /** @brief Indicates that the endpoint is disconnecting. */
    void              disconnecting();

  private:
    SignalingSessionDictionary sessions_;

    QHash<QUuid, Dialog*> dialogs_;
    RealTimeAddress   localTarget_;
    QList<Transaction*> pendingTransactions_;
    QString           supportedversion_;
    quint32           sessionId_;
    RealTimeAddress   sessionTarget_;
    TransportImpl *   transport_;

  Q_DISABLE_COPY(RtcSignalingEndpoint)

}; // RtcSignalingEndpoint
}

#endif
