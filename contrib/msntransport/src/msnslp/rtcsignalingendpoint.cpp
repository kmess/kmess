//
// RtcSignalingEndpoint class
//
// Summary: Represents a real time communications signaling endpoint.
//
// Authors:
//    Gregg Edghill (gregg.edghill at gmail.com)
//
// Copyright (C) 2009 - 2010, Gregg Edghill
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of this software.
//
// THIS LIBRARY IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR
// MODIFY IT UNDER THE TERMS OF THE GNU LESSER GENERAL PUBLIC
// LICENSE AS PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER
// VERSION 2 OF THE LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.
//

#include "rtcsignalingendpoint.h"
#include "dialog.h"
#include "msntransportconfig.h"
#include "contentdescription.h"
#include "proxysignalingsession.h"
#include "transaction.h"
#include "helper.h"
#include "message/signalinguri.h"
#include <QtCore/QHash>
#include <QtCore/QVariant>
#include <QtCore/QRegExp>
#include <QtGlobal>
#include <QtDebug>
using namespace PtpInternal;

#ifdef MSNTRANSPORT_DEBUG
  #define P2PDEBUG_SLP
  #define P2PDEBUG_SLP_VERBOSE
#endif

namespace PtpSignaling
{

RtcSignalingEndpoint::RtcSignalingEndpoint(const RealTimeAddress& localTarget, const RealTimeAddress& sessionTarget, QObject *pobject) : QObject(pobject)
{
  localTarget_ = localTarget;
  supportedversion_ = QLatin1String("1.0");
  sessionTarget_ = sessionTarget;
  sessionId_ = 0;

  transport_ = new TransportImpl(this);
  // Connect the signals/slots
  QObject::connect(transport_, SIGNAL(connected()), this, SLOT(transport_OnInitialConnectionEstablished()));
  QObject::connect(transport_, SIGNAL(disconnected()), this, SLOT(transport_OnDisconnected()));
  QObject::connect(transport_, SIGNAL(error(const quint32)), this, SLOT(transport_OnError(const quint32)));

  // TODO Remove once notifier is coded.
  QObject::connect(transport_, SIGNAL(message(const QByteArray&, const qint32, const qint32)), this,
                   SLOT(transport_OnMessageDataReceived(QByteArray,const qint32, const qint32)));
}

RtcSignalingEndpoint::~RtcSignalingEndpoint()
{
  shutdown();
}

quint32 RtcSignalingEndpoint::generateNewSessionId()
{
  if (0 == sessionId_)
  {
    // Generate the initial session id.
    sessionId_ = (quint32)(((double)(qrand() * 1.0/0x7FFFFFFF)) *
                 (0xA235 - 0x2DCC)) + 0x2DCC;
  }
  return ++sessionId_;
}

RealTimeAddress RtcSignalingEndpoint::getSessionTarget() const
{
  return sessionTarget_;
}

TransportImpl * RtcSignalingEndpoint::getTransport()
{
  return transport_;
}

void RtcSignalingEndpoint::cleanupSignalingSessionResources(const quint32 sessionkey)
{
}

//BEGIN Signaling Helper Functions

const QString RtcSignalingEndpoint::mapResponseCodeToResponseText(const quint16 responseCode)
{
  QString responseText;
  switch(responseCode)
  {
    case SignalingResponse::SLPRC_OK:
      responseText = "OK";
      break;
    case SignalingResponse::SLPRC_NOT_FOUND:
      responseText = "Not Found";
      break;
    case SignalingResponse::SLPRC_NO_SUCH_CALL:
      responseText = "No Such Call";
      break;
    case SignalingResponse::SLPRC_INTERNAL_ERROR:
      responseText = "Internal Error";
      break;
    case SignalingResponse::SLPRC_VERSION_NOT_SUPPORTED:
      responseText = "Not Supported";
      break;
    case SignalingResponse::SLPRC_DECLINE:
      responseText = "Decline";
      break;
    case SignalingResponse::SLPRC_UNACCEPTABLE:
      responseText = "Unacceptable";
      break;
  }

  return responseText;
}

SignalingRequest RtcSignalingEndpoint::buildRequest(const QString& method, const QString& callId)
{
  // Create the signaling uris associated with the request
  SignalingUri toUri("msnmsgr", sessionTarget_.getAddress(), sessionTarget_.getEndpointId());
  SignalingUri fromUri("msnmsgr", localTarget_.getAddress(), localTarget_.getEndpointId());

  // Get the signaling request uri string
  const QString uri = toUri.toString().replace("msnmsgr", "MSNMSGR", Qt::CaseSensitive);
  // Create the signaling request
  SignalingRequest request(method, uri, supportedversion_);
  // Create the via signaling header.
  SignalingHeader viaHeader(QLatin1String("Via"), QString("MSNSLP/%1/TLP").arg(supportedversion_));
  // Generate a transaction branch uuid and
  // add it as a parameter to the via header.
  viaHeader.addParameter(QLatin1String("branch"), QUuid::createUuid().toString().toUpper());

  // Per RFC 3261, the From and To headers contain a URI; If the URI
  // contains a comma, question mark or semicolon, the URI MUST be
  // enclosed in angle brackets (< and >).  Any URI parameters are
  // contained within these brackets.  If the URI is not enclosed in angle
  // brackets, any semicolon-delimited parameters are header-parameters,
  // not URI parameters.

  // Set the request headers
  request.setSignalingHeader(QLatin1String("To"), QString("<%1>").arg(toUri.toString()));
  request.setSignalingHeader(QLatin1String("From"), QString("<%1>").arg(fromUri.toString()));
  request.addSignalingHeader(viaHeader);
  request.setSignalingHeader(QLatin1String("Call-ID"), callId);
  request.setSignalingHeader(QLatin1String("CSeq"), 0);

  //Set the request context information.
  request.setId(0);
  request.setCorrelationId(qrand() % 0xCF54);

  return request;
}

SignalingResponse RtcSignalingEndpoint::buildResponse(const SignalingRequest& request, const quint16 responseCode)
{
  // Create the signaling response.
  SignalingResponse response(responseCode, mapResponseCodeToResponseText(responseCode), supportedversion_);
  // Set the response headers
  response.setSignalingHeader(QLatin1String("To"), request.getSignalingHeader("From").getValue());
  response.setSignalingHeader(QLatin1String("From"), request.getSignalingHeader("To").getValue());
  response.setSignalingHeader(QLatin1String("Via"), request.getSignalingHeader("Via").getValue());
  response.setSignalingHeader(QLatin1String("Call-ID"), request.getSignalingHeader("Call-ID").getValue());
  response.setSignalingHeader(QLatin1String("CSeq"), request.getCSeq() + 1);

  // Set the response context information.
  response.setId(0);
  response.setCorrelationId(request.getId());

  return response;
}

void RtcSignalingEndpoint::acceptSessionWithAnswer(ProxySignalingSession *pssession, const SessionDescription& answerbody)
{
  // Get the dialog associated with
  // the supplied signaling session
  Dialog *dialog = getDialogBySessionKey(pssession->getSessionId());
  // Check whether the dialog object is null
  if (!dialog)
  {
#ifdef P2PDEBUG_SLP
    qWarning("%s: dialog not found (session=%d)", Q_FUNC_INFO,
             pssession->getSessionId());
#endif
    // If so, bail out.
    return;
  }

  // Get the initiating INVITE signaling request
  SignalingRequest initiatingRequest = dialog->getInitialTransaction()->getInitiatingRequest();
  // Get the session offer answer.
  const QByteArray answer = answerbody.getBytes();
  // Create the MIME content description
  ContentDescription cd(ContentType("application/x-msnmsgr-sessionreqbody"), answer);
  // Send the 200 OK response.
  sendSignalingResponse(SignalingResponse::SLPRC_OK, cd, initiatingRequest);
}

void RtcSignalingEndpoint::establishSessionWithOffer(ProxySignalingSession *pssession, const SessionDescription& offerbody)
{
  // Create a new call id for the pending dialog
  QUuid callId = QUuid::createUuid();
  // Create a new pending dialog.
  Dialog *dialog = new Dialog(callId, this);
  // Set the session key associated with the dialog
  dialog->setSessionKey(pssession->getSessionId());
  // Get the session offer
  const QByteArray offer = offerbody.getBytes();
  // Create the MIME content description
  ContentDescription cd(ContentType("application/x-msnmsgr-sessionreqbody"), offer);

  // Add the pending dialog to the call map.
  addDialogToCallMap(dialog);

  // Generate the signaling request
  SignalingRequest initiatingRequest = buildRequest(QLatin1String("INVITE"), callId.toString());
  initiatingRequest.setSignalingHeader(QLatin1String("Content-Type"), cd.getContentType().toString());
  initiatingRequest.setMessageBody(cd.getBody());

  // Create a new transaction for the request.
  Transaction *transaction = new Transaction(initiatingRequest, true, this);
  // Add the transaction to the dialog
  dialog->setInitialTransaction(transaction);

  // Check whether the underlying transport is connected
  if (transport_->isConnected())
  {
    // If so, begin the transaction.
//    beginTransaction(transaction);
    // Send the request to initiate the dialod.
    sendSignalingMessage(initiatingRequest);
  }
  else
  {
    // Otherwise, queue the transaction until it is.
    pendingTransactions_.append(transaction);
  }
}

void RtcSignalingEndpoint::rejectWithErrorCode(const SignalingRequest &request, quint16 responseCode)
{
  ContentDescription none(ContentType("null"), QByteArray());
  // Send the error signaling response.
  rejectWithErrorCode(request, responseCode, none);
}

void RtcSignalingEndpoint::rejectWithErrorCode(const SignalingRequest &request, quint16 responseCode, const ContentDescription& contentDescription)
{
  // Send a error response message to the received request.
  sendSignalingResponse(responseCode, contentDescription, request);
}

void RtcSignalingEndpoint::sendSignalingResponse(const quint16 responseCode, const ContentDescription& contentDescription, const SignalingRequest &request)
{
  // Generate the response
  SignalingResponse response = buildResponse(request, responseCode);
  response.setSignalingHeader(QLatin1String("Content-Type"), contentDescription.getContentType().toString());
  response.setMessageBody(contentDescription.getBody());

  // Send the response.
  sendSignalingMessage(response);
}

void RtcSignalingEndpoint::sendSignalingRequest(const QString& method, const ContentDescription& contentDescription, const QString& callId)
{
  // Generate the request
  SignalingRequest request = buildRequest(method, callId);
  request.setSignalingHeader(QLatin1String("Content-Type"), contentDescription.getContentType().toString());
  request.setMessageBody(contentDescription.getBody());

  // Get the dialog with the supplied call id
  Dialog *dialog = getDialogByCallId(callId);

  // Create a new transaction for the request.
  Transaction *transaction = new Transaction(request, true, this);
  // Add the transaction to the dialog.
  dialog->addTransaction(transaction);
  // Begin the transaction.
//  beginTransaction(transaction);

  // Send the request.
  sendSignalingMessage(request);
}

void RtcSignalingEndpoint::terminateWithRejection(const quint16 responseCode, const ContentDescription& contentDescription, const SignalingRequest& request)
{
  // Send the signaling response.
  sendSignalingResponse(responseCode, contentDescription, request);
}

void RtcSignalingEndpoint::terminateSession(ProxySignalingSession *pssession, const SessionDescription& closebody)
{
  // Get the dialog associated with
  // the supplied signaling session
  Dialog *dialog = getDialogBySessionKey(pssession->getSessionId());
  // Check whether the dialog object is null
  if (!dialog)
  {
#ifdef P2PDEBUG_SLP
    qWarning("%s: dialog not found (session=%d)", Q_FUNC_INFO,
             pssession->getSessionId());
#endif
    // If so, bail out.
    return;
  }

  // Set the dialog's state to terminating.
  dialog->setState(Dialog::SLPDS_TERMINATING);

  // Get the signaling session close body
  const QByteArray close = closebody.getBytes();
  // Create the MIME content description
  ContentDescription cd(ContentType("application/x-msnmsgr-sessionclosebody"), close);
  // Send the BYE request
  sendSignalingRequest(QLatin1String("BYE"), cd, dialog->getCallId());
}

void RtcSignalingEndpoint::terminateSessionWithDecline(ProxySignalingSession *pssession, const SessionDescription& answerbody)
{
  // Get the dialog associated with
  // the supplied signaling session
  Dialog *dialog = getDialogBySessionKey(pssession->getSessionId());
  // Check whether the dialog object is null
  if (!dialog)
  {
#ifdef P2PDEBUG_SLP
    qWarning("%s: dialog not found (session=%d)", Q_FUNC_INFO,
             pssession->getSessionId());
#endif
    // If so, bail out.
    return;
  }

  // Get the initiating INVITE signaling request
  SignalingRequest initiatingRequest = dialog->getInitialTransaction()->getInitiatingRequest();
  // Get the session offer answer.
  const QByteArray answer = answerbody.getBytes();
  // Create the MIME content description
  ContentDescription cd(ContentType("application/x-msnmsgr-sessionreqbody"), answer);
  // Send the 603 DECLINE response.
  terminateWithRejection(SignalingResponse::SLPRC_DECLINE, cd, initiatingRequest);

  // Wait 5 seconds for an acknowledge before terminating the dialog.
  // If a timeout occurs, the dialog will terminate anyway.
  dialog->terminate(5000);
}

void RtcSignalingEndpoint::sendSignalingMessage(SignalingMessage& message, bool priority)
{
#ifdef P2PDEBUG_SLP_VERBOSE
  qDebug("%s: enter", Q_FUNC_INFO);
#endif
  // Get the raw message byte array.
  const QByteArray messageData = message.getRawMessageByteArray();
#ifdef P2PDEBUG_SLP
  const QString messageText = QString::fromUtf8(messageData);
  qDebug("%s: %s", Q_FUNC_INFO, qPrintable(messageText));
#endif
  // Send the message data via the transport layer.
  const quint32 id = transport_->send(messageData, 0, message.getCorrelationId());
  // Set the signaling message id
  message.setId(id);
#ifdef P2PDEBUG_SLP
   qDebug("%s: trid (%d), priority (%d)", Q_FUNC_INFO, id, priority);
#endif
  // Get the dialog associated with this signaling message.
  Dialog *dialog = getDialogByCallId(message.getCallId());
  // Set the transaction id
  dialog->setCurrentTransactionId(id);

#ifdef P2PDEBUG_SLP_VERBOSE
  qDebug("%s: leave", Q_FUNC_INFO);
#endif
}

//END

//BEGIN Signaling Request Handling Functions

void RtcSignalingEndpoint::onAcknowledge(const SignalingRequest& request)
{
}

void RtcSignalingEndpoint::onDirectConnectionOfferInInviteResponse(const SignalingRequest& request)
{
}

void RtcSignalingEndpoint::onDirectConnectionOfferInReInvite(const SignalingRequest& request)
{
}

bool RtcSignalingEndpoint::shouldHonourDirectConnectionOfferRequest(const SignalingRequest &request)
{
  return false;
}

//////////////////////////////////////////////////////////////////////
// Handles signaling layer protocol requests received
// SLP only supports a subset of the SIP methods - INVITE, ACK and BYE.
//////////////////////////////////////////////////////////////////////
void RtcSignalingEndpoint::onRequestMessage(const SignalingRequest& request)
{
  const SignalingUri toUri(request.getToHeader().getValue().toString().remove(QRegExp("[<>]")));
  const SignalingUri myUri("msnmsgr", localTarget_.getAddress(), localTarget_.getEndpointId());

#ifdef P2PDEBUG_SLP
  qDebug("%s: Got request to \'%s\' (local user=%s)", Q_FUNC_INFO,
         qPrintable(toUri.getUserAddress()),
         qPrintable(myUri.getUserAddress()));
#endif
  // Check if the message target is this endpoint.
  if (toUri.getUserAddress() != myUri.getUserAddress())
  {
    // If not, send a not found error.
#ifdef P2PDEBUG_SLP
    qDebug("%s: Got request which is not for me -- responding with 404", Q_FUNC_INFO);
#endif

    // Send a 404 Not Found status response message.
    rejectWithErrorCode(request, SignalingResponse::SLPRC_NOT_FOUND);
    return;
  }

  // Check whether the protocol version is supported.
  if (request.getVersion() != supportedversion_)
  {
#ifdef P2PDEBUG_SLP
    qDebug("%s: Got request whose PROTOCOL VERSION is not supported", Q_FUNC_INFO);
#endif

    // Send a 505 Not Supported status response message.
    rejectWithErrorCode(request, SignalingResponse::SLPRC_VERSION_NOT_SUPPORTED);
    return;
  }

  if (request.getMethodType() == QLatin1String("INVITE"))
  {
    // Get the content type of the request.
    const QString contentType = request.getContentType().getMediaType();
    // Determine what type of content is in the request.
    if (contentType == QLatin1String("application/x-msnmsgr-sessionreqbody"))
    {
      // Handle the initial dialog creating invite request.
      onInitialSessionOffer(request);
    }
    else
    if (contentType == QLatin1String("application/x-msnmsgr-transreqbody"))
    {
      // Handle the direct connection negotiation reINVITE request.
      onDirectConnectionOfferInReInvite(request);
    }
    else
    if (contentType == QLatin1String("application/x-msnmsgr-transrespbody"))
    {
      // Handle the direct connection offer request in INVITE response.
      onDirectConnectionOfferInInviteResponse(request);
    }
    else
    {
#ifdef P2PDEBUG_SLP
      qDebug("%s: unexpected INVITE media type -- sending error", Q_FUNC_INFO);
#endif
      // Send a 500 Internal Error status response message.
      rejectWithErrorCode(request, SignalingResponse::SLPRC_INTERNAL_ERROR);
    }
  }
  else
  if (request.getMethodType() == QLatin1String("ACK"))
  {
    // Handle the ack request
    onAcknowledge(request);
  }
  else
  if (request.getMethodType() == QLatin1String("BYE"))
  {
    // Handle the bye request.
    onTerminateSession(request);
  }
  else
  {
#ifdef P2PDEBUG_SLP
    qDebug("%s: Got unknown request method %s -- dropping request", Q_FUNC_INFO,
           qPrintable(request.getMethodType()));
#endif
  }
}

void RtcSignalingEndpoint::onInitialSessionOffer(const SignalingRequest& request)
{
#ifdef P2PDEBUG_SLP_VERBOSE
  qDebug("%s: enter", Q_FUNC_INFO);
#endif

  // Try to get an existing dialog with the supplied call id
  Dialog *dialog = getDialogByCallId(request.getCallId());
  // Check whether the dialog exists
  if (dialog)
  {
#ifdef P2PDEBUG_SLP
    qDebug("%s: got duplicate INVITE offer for established dialog -- ignoring", Q_FUNC_INFO);
#endif
    return;
  }

  // Create the incoming invite transaction.
  Transaction *incomingTransaction = new Transaction(request, false, this);
  // Create a new dialog associated with the transaction
  dialog = new Dialog(incomingTransaction, this);
  // Add the pending dialog to the call map.
  addDialogToCallMap(dialog);

  // Try to parse the session description
  SessionDescription sd = Helper::parseSessionOffer(QString::fromUtf8(request.getMessageBody()));
  if (sd.isNull())
  {
#ifdef P2PDEBUG_SLP
    qDebug("%s: invalid session description (offer) -- sending error", Q_FUNC_INFO);
#endif
    // Send a 500 internal error response.
    rejectWithErrorCode(request, SignalingResponse::SLPRC_INTERNAL_ERROR);
    // Wait 5 seconds for an acknowledge before terminating the dialog.
    // If a timeout occurs, the dialog will terminate anyway.
    dialog->terminate(5000);
    return;
  }

  // Gets the id of the incoming session
  const quint32 sessionId = sd.getSessionId();
  // Set the dialog session key for later lookup
  dialog->setSessionKey(sessionId);
  // Create a new signaling session.
  ProxySignalingSession *session = new ProxySignalingSession(sessionId, this);
  // Add the signaling session to the list.
  sessions_.insert(sessionId, session);

  // Handle the incoming session.
  onSessionReceived(sd, session);

#ifdef P2PDEBUG_SLP_VERBOSE
  qDebug("%s: leave", Q_FUNC_INFO);
#endif
}

void RtcSignalingEndpoint::onTerminateSession(const SignalingRequest& request)
{
}

//END

//BEGIN Signaling Response Handling Functions

void RtcSignalingEndpoint::onDirectConnectionOfferAccepted(const SignalingResponse &response)
{
}

void RtcSignalingEndpoint::onDirectConnectionOfferDeclined(const SignalingResponse &response)
{
}

void RtcSignalingEndpoint::onInternalError(const SignalingResponse &response)
{
}

void RtcSignalingEndpoint::onNoSuchCall(const SignalingResponse &response)
{
}

void RtcSignalingEndpoint::onRecipientUriNotFound(const SignalingResponse &response)
{
}

//////////////////////////////////////////////////////////////////////
// Handles signaling layer protocol responses received
//////////////////////////////////////////////////////////////////////
void RtcSignalingEndpoint::onResponseMessage(const SignalingResponse& response)
{
  // Check whether the message version is supported.
  if (response.getVersion() != supportedversion_)
  {
#ifdef P2PDEBUG_SLP
    qDebug("%s: Got response whose protocol version is not supported -- ignoring", Q_FUNC_INFO);
#endif
    return;
  }

  // Get the status code of the response.
  const SignalingResponse::SLP_RESPONSE_CODE responseCode
      = (SignalingResponse::SLP_RESPONSE_CODE)response.getResponseCode();
  if (responseCode == SignalingResponse::SLPRC_OK)
  {
    // NOTE MSNSLP/1.0 200 OK

    // Get the content type of the response.
    const QString contentType = response.getContentType().getMediaType();
    if (contentType == QLatin1String("application/x-msnmsgr-sessionreqbody"))
    {
      onSessionOfferAccepted(response);
    }
    else
    if (contentType == QLatin1String("application/x-msnmsgr-transrespbody"))
    {
      onDirectConnectionOfferAccepted(response);
    }
    else
    {
#ifdef P2PDEBUG_SLP
      qDebug("%s: unexpected 200 OK media type -- ignoring", Q_FUNC_INFO);
#endif
    }
  }
  else
  if (responseCode == SignalingResponse::SLPRC_NOT_FOUND)
  {
    // NOTE MSNSLP/1.0 404 Not Found
    onRecipientUriNotFound(response);
  }
  else
  if (responseCode == SignalingResponse::SLPRC_NO_SUCH_CALL)
  {
    // NOTE MSNSLP/1.0 481 No Such Call
    onNoSuchCall(response);
  }
  else
  if (responseCode == SignalingResponse::SLPRC_INTERNAL_ERROR)
  {
    // NOTE MSNSLP/1.0 500 Internal Error
    onInternalError(response);
  }
  else
  if (responseCode == SignalingResponse::SLPRC_VERSION_NOT_SUPPORTED)
  {
    // NOTE MSNSLP/1.0 505 Not Supported
    // TODO Implementation
  }
  else
  if (responseCode == SignalingResponse::SLPRC_DECLINE)
  {
    // NOTE MSNSLP/1.0 603 Decline

    // Get the content type of the response.
    const QString contentType = response.getContentType().getMediaType();
    if (contentType == QLatin1String("application/x-msnmsgr-sessionreqbody"))
    {
      onSessionOfferDeclined(response);
    }
    else
    if (contentType == QLatin1String("application/x-msnmsgr-transrespbody"))
    {
      onDirectConnectionOfferDeclined(response);
    }
    else
    {
#ifdef P2PDEBUG_SLP
      qDebug("%s: unexpected media type -- ignoring", Q_FUNC_INFO);
#endif
    }
  }
  else
  if (responseCode == SignalingResponse::SLPRC_UNACCEPTABLE)
  {
    // NOTE MSNSLP/1.0 606 Unacceptable
    // TODO Implementation
  }
  else
  {
#ifdef P2PDEBUG_SLP
    qDebug("%s: Got unknown response code %d -- dropping response", Q_FUNC_INFO, responseCode);
#endif
  }
}

void RtcSignalingEndpoint::onSessionOfferAccepted(const SignalingResponse &response)
{
  // TODO Implementation
}

void RtcSignalingEndpoint::onSessionOfferDeclined(const SignalingResponse &response)
{
  // TODO Implementation
}

//END

//BEGIN SendReceiveDataNotifier Event Handling Functions

void RtcSignalingEndpoint::transport_OnMessageDataReceived(const QByteArray& messageData, const qint32 id, const qint32 correlationId)
{
#ifdef P2PDEBUG_SLP_VERBOSE
  qDebug("%s: enter", Q_FUNC_INFO);
#endif

#ifdef P2PDEBUG_SLP
  const QString messageText = QString::fromUtf8(messageData);
  qDebug("%s: %s", Q_FUNC_INFO, qPrintable(messageText));
#endif

  // Try to parse the signaling layer protocol (slp)
  // message from the raw data received.
  SignalingMessage *message = Helper::parseSignalingMessage(messageData);
  // Check whether the message is valid.
  if (!message)
  {
    // If not, return.
#ifdef P2PDEBUG_SLP
    qDebug("%s: Unrecognized message, id=%d -- ignoring", Q_FUNC_INFO, id);
#endif
    return;
  }

  // Set the message context information.
  message->setId(id);
  message->setCorrelationId(correlationId);
  // Try to unbox (i.e. cast) the message as a signaling request object.
  SignalingRequest *request = dynamic_cast<SignalingRequest*>(message);
  // Check whether the message is a signaling request
  if (request)
  {
    // If so, handle the received signaling request.
    onRequestMessage(*request);
  }
  // try to unbox the message as a signaling response object
  SignalingResponse *response = dynamic_cast<SignalingResponse*>(message);
  // Check whether the message is a signaling response
  if (response)
  {
    // If so, handle the received signaling response.
    onResponseMessage(*response);
  }

  // Dispose of the message.
  delete message;

#ifdef P2PDEBUG_SLP_VERBOSE
  qDebug("%s: leave", Q_FUNC_INFO);
#endif
}

//END

//BEGIN Dialog Event Handling Functions

void RtcSignalingEndpoint::dialog_OnEstablish()
{
  // Handles the establishment of a dialog after the
  // acceptance of a locally or remotely initiated
  // session request.

  Dialog *dialog = qobject_cast<Dialog*>(sender());
  // Get the call id of the dialog
  const QUuid callId = dialog->getCallId();
#ifdef P2PDEBUG_SLP
  qDebug("%s:  dialog ESTABLISHED, call id = %s", Q_FUNC_INFO,
         qPrintable(callId.toString()));
#endif

  // Get the session key associated with the dialog
  const quint32 sessionkey = dialog->getSessionKey();
  // Get the signaling session
  ProxySignalingSession *pssession = sessions_[sessionkey];

  // Check whether the signaling session object is not null
  if (pssession)
  {
    // TODO Add the notifier to the transport layer

    // If so, establish the session.
  }
}

void RtcSignalingEndpoint::dialog_OnTerminate()
{
  // Handles the termination of a dialog after receiving a
  // BYE request or an error has occurred.

  Dialog *dialog = qobject_cast<Dialog*>(sender());
  // Get the call id of the dialog
  const QUuid callId = dialog->getCallId();
#ifdef P2PDEBUG_SLP
  qDebug("%s:  dialog TERMINATED, call id = %s", Q_FUNC_INFO,
         qPrintable(callId.toString()));
#endif
  // Disconnect from the event signal/slot
  QObject::disconnect(dialog, 0, this, 0);

  // Get the session key associated with the dialog
  const quint32 sessionkey = dialog->getSessionKey();
  // Get the signaling session
  ProxySignalingSession *pssession = sessions_[sessionkey];

  // Check whether the signaling session object is not null
  if (pssession)
  {
    // TODO Remove the notifier from the transport layer

    // If so, terminate the session if not already terminated.

    // Disconnect from the event signal/slot
    QObject::disconnect(pssession, 0, this, 0);
    // Remove the session from the list of sessions.
    sessions_.remove(sessionkey);

    // Delete the session as its lifetime has ended.
    Helper::asyncDeleteQObject(pssession);
  }

  // Get a readonly list iterator
  QListIterator<Transaction*> list(dialog->getTransactions());
  Transaction *transaction;
  // Terminate all pending transactions
  while (list.hasNext() && list.peekNext())
  {
    transaction = list.next();
    if (transaction->isRemotelyInitiated() && Transaction::SLPTS_TERMINATED != transaction->getState())
    {
      // Terminate the transaction
//      endTransaction(transaction);
    }
  }

  // Clean up all resources created for the session
  cleanupSignalingSessionResources(sessionkey);

  // Remove the dialog from the call map.
  removeDialogFromCallMap(dialog->getCallId());
  // Delete the dialog since its lifetime has expired
  Helper::asyncDeleteQObject(dialog);
}

//END

//BEGIN Dialog Helper Functions

void RtcSignalingEndpoint::addDialogToCallMap(Dialog *dialog)
{
  // Check whether a dialog with the
  // specified call id is not registered
  if (dialog && !dialogs_.contains(dialog->getCallId()))
  {
    // Get the call id of the dialog
    const QUuid callId = dialog->getCallId();
#ifdef P2PDEBUG_SLP
    qDebug("%s:  call id = %s", Q_FUNC_INFO, qPrintable(callId.toString()));
#endif
    // Connect the signal/slot.
    QObject::connect(dialog, SIGNAL(established()), this, SLOT(dialog_OnEstablish()));
    QObject::connect(dialog, SIGNAL(terminated()), this, SLOT(dialog_OnTerminate()));
    // If so, add the new dialog to the call map.
    dialogs_.insert(callId, dialog);
  }
}

Dialog * RtcSignalingEndpoint::getDialogByCallId(const QUuid& callId)
{
  Dialog *dialog = 0l;
  // Check whether a dialog with the
  // specified call id is registered
  if (dialogs_.contains(callId))
  {
    // If so, return the dialog
    dialog = dialogs_[callId];
  }
  return dialog;
}

Dialog * RtcSignalingEndpoint::getDialogBySessionKey(const quint32 sessionkey)
{
  Dialog *dialog = 0l;
  // Get the list of all dialogs.
  QList<Dialog*> dialogs = dialogs_.values();
  for(qint32 i=0; i < dialogs.count(); ++i)
  {
    // Check whether the session associated with the current
    // dialog has a session id which matchs that of supplied
    // session key
    if (dialogs[i] && dialogs[i]->getSessionKey() == sessionkey)
    {
      // If so, the dialog has been found.
      dialog = dialogs[i];
    }
  }

  return dialog;
}

void RtcSignalingEndpoint::removeDialogFromCallMap(const QUuid& callId)
{
  // Check whether a dialog with the
  // specified call id is registered
  if (dialogs_.contains(callId))
  {
#ifdef P2PDEBUG_SLP
    qDebug("%s:  call id = %s", Q_FUNC_INFO, qPrintable(callId.toString()));
#endif
    // If so, remove the dialog from the call map.
    dialogs_.remove(callId);
  }
}

//END

//BEGIN Transport Event Handling Functions

void RtcSignalingEndpoint::transport_OnInitialConnectionEstablished()
{
  // TODO Implementation
}

void RtcSignalingEndpoint::transport_OnDisconnected()
{
  // TODO Implementation
}

void RtcSignalingEndpoint::transport_OnError(const quint32 destination)
{
  // TODO Implementation
}

//END

void RtcSignalingEndpoint::shutdown()
{
#ifdef P2PDEBUG_SLP
  qDebug("%s: terminating (%d) dialog(s)", Q_FUNC_INFO,
         dialogs_.count());
#endif
  for(qint32 i=0; i < dialogs_.count(); ++i)
  {
    // Terminate the dialog.
    dialogs_.values()[i]->terminate();
  }

  // Check whether there are any registered dialogs.
  if (dialogs_.count() > 0)
  {
    // If so, dispose of all the dialogs.
    qDeleteAll(dialogs_);
    // Clear the dialog list.
    dialogs_.clear();
  }
}

}
