//
// Platform class
//
// Summary: Represents the global peer to peer (ptp) platform which
//    provides methods for initiating and participating in peer to peer
//    activities.
//
// Authors:
//    Gregg Edghill (gregg.edghill at gmail.com)
//
// Copyright (C) 2009 - 2010, Gregg Edghill
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of this software.
//
// THIS LIBRARY IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR
// MODIFY IT UNDER THE TERMS OF THE GNU LESSER GENERAL PUBLIC
// LICENSE AS PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER
// VERSION 2 OF THE LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.
//

#include "platform.h"
#include "msntransportconfig.h"
#include "msnslp/slppeertopeerendpoint.h"
#include <QtCore/QDir>
#include <QtNetwork/QTcpServer>
#include <QtNetwork/QTcpSocket>
#include <QtDebug>
using namespace PtpSignaling;
using namespace PtpTransport;

#ifdef MSNTRANSPORT_DEBUG
  #define P2PDEBUG_PLATFORM
#endif

namespace PtpCollaboration
{

Platform * Platform::s_platform = 0l;

Platform::Platform(QObject *pobject) : QObject(pobject), initialized_(false)
{
  // Create the tcp listener which will listen
  // for incoming connections.
  tcplistener_ = new QTcpServer(this);
  // Connect to signal/slot
  QObject::connect(tcplistener_, SIGNAL(newConnection()), this, SLOT(tcplistener_OnAcceptConnection()));

  settings_["allowNatTraversal"]     = true;
  settings_["objectStoreDirectory"]  = QLatin1String("");
  settings_["maxMediaPortValue"]     = 44061;
  settings_["minMediaPortValue"]     = 44031;
  settings_["connectionType"]        = QLatin1String("Unknown-Connection");
  settings_["isBehindFirewall"]      = false;
  settings_["localUserAddress"]      = QLatin1String("");
  settings_["internalIpAddress"]     = QLatin1String("");
  settings_["externalIpAddress"]     = QLatin1String("");
  settings_["localEndpointId"]       = QLatin1String("");
  settings_["maxListeningPortValue"] = 35060;
  settings_["minListeningPortValue"] = 35050;
  settings_["listeningPort"]         = 0;
}

Platform::~Platform()
{
  // Ensure all resources are cleaned up
  // and the platform shuts down.
  shutdown();
}

void Platform::dispose()
{
  if (s_platform)
  {
    // Delete the platform object.
    delete s_platform;
    s_platform = 0l;
#ifdef P2PDEBUG_PLATFORM
  qDebug("%s: destroyed platform", Q_FUNC_INFO);
#endif
  }
}

void Platform::startup(const QHash<QString, QVariant>& settings)
{
  if (initialized_)
  {
#ifdef P2PDEBUG_PLATFORM
    qDebug("%s: already initialized platform", Q_FUNC_INFO);
#endif
    return;
  }

#ifdef P2PDEBUG_PLATFORM
  qDebug("%s: initializing platform", Q_FUNC_INFO);
#endif

  QHash<QString, QVariant>::ConstIterator item = settings.begin();
  for (item = settings.begin(); item != settings.end(); ++item)
  {
    settings_[item.key()] = item.value();
  }

  // Set the default object store location if not set
  if (getSetting("objectStoreDirectory").toString().isEmpty())
  {
    settings_["objectStoreDirectory"] = QDir::tempPath() + QLatin1String("/msgrcache/");
  }

  //
  // Start up the TCP listener
  //

  // Get the port range for incoming connections
  const quint16 minListeningPort = getSetting(QLatin1String("minListeningPortValue")).toUInt();
  const quint16 maxListeningPort = getSetting(QLatin1String("maxListeningPortValue")).toUInt();

  for (quint16 port=minListeningPort; port <= maxListeningPort; ++port)
  {
    // Start listening for incoming TCP connections
    // on the selected port.
    tcplistener_->listen(QHostAddress::Any, port);
    // Check whether the connection listener is listening
    // on the selected port
    if (tcplistener_->isListening())
    {
      // If so, set the listening port to
      // the listening port of the server.
      settings_["listeningPort"] = tcplistener_->serverPort();
#ifdef P2PDEBUG_PLATFORM
      qDebug("%s: socket(%d) listening on port %d", Q_FUNC_INFO,
             tcplistener_->socketDescriptor(),
             getListeningPort());
#endif
      // Check whether NAT traversal is allowed.
      if (settings["allowNatTraversal"].toBool())
      {
        // If so, forward the listening port
        // so external clients can connect to
        // this endpoint.

        // TODO Add back in NAT forwarding
      }

      break;
    }
  }

#ifdef P2PDEBUG_PLATFORM
  if (0 == getListeningPort())
  {
    qDebug("%s: failed to listen on any ports", Q_FUNC_INFO);
  }
#endif

  // Indicate that the platform has been initialized
  initialized_ = true;
}

quint16 Platform::getListeningPort() const
{
  return settings_["listeningPort"].toUInt();
}

const QVariant Platform::getSetting(const QString& name) const
{
  return settings_.value(name);
}

const QHash<QString, QVariant> & Platform::getSettings() const
{
  return settings_;
}

EufInviteHandler * Platform::getEufInviteHandler(const QUuid& eufguid)
{
  EufInviteHandler * eufhandler = 0l;
  // Check whether an INVITE handler is registered
  // for the specified euf GUID.
  if (eufInviteHandlers_.contains(eufguid))
  {
    // If not, add the specified INVITE handler and
    // associated euf GUID.
    eufhandler = eufInviteHandlers_.value(eufguid);
  }

  return eufhandler;
}

RtcSignalingEndpoint * Platform::getOrCreateEndpoint(const RealTimeAddress& sessionTarget)
{
  RtcSignalingEndpoint *endpoint;
  // Create a unique endpoint id (key) to look up the endpoint later.
  QString endpointid = sessionTarget.getAddress();
  const bool isMpopSupported = sessionTarget.isMpopSupported();
  // Check whether the session target support MPOP
  if (isMpopSupported)
  {
    // If so, add the endpoint id string.
    endpointid += QLatin1String(";") + sessionTarget.getEndpointId();
  }

  // Check whether a signaling endpoint already
  // exists for the supplied peer at the given
  // endpoint.
  if(!endpoints_.contains(endpointid))
  {
#ifdef P2PDEBUG_PLATFORM
    qDebug("%s: creating ENDPOINT (target=%s)", Q_FUNC_INFO, qPrintable(endpointid));
#endif
    // If not, create a new rtc signaling endpoint.
    QString localUser = getSetting("localUserAddress").toString();
    const QString machineUuid =
        isMpopSupported ? getSetting("localEndpointId").toString()
                        : QLatin1String("");
    RealTimeAddress localTarget(localUser, machineUuid);
    endpoint = new SlpPeerToPeerEndpoint(localTarget, sessionTarget, this);
    // Add the endpoint to the list.
    endpoints_[endpointid] = endpoint;
  }
  else
  {
#ifdef P2PDEBUG_PLATFORM
    qDebug("%s: ENDPOINT (target=%s)", Q_FUNC_INFO, qPrintable(endpointid));
#endif
    // Otherwise, get the signaling
    // endpoint from the list.
    endpoint = endpoints_[endpointid];
  }

  return endpoint;
}

bool Platform::isBehindFirewall() const
{
  return settings_.value(QLatin1String("isBehindFirewall")).toBool();
}

Platform * Platform::instance()
{
  if (!s_platform)
  {
    // Create the peer to peer platform used to host
    // all signaling and peer to peer endpoints.
    s_platform = new Platform(0l);
  }

  return s_platform;
}

void Platform::registerEufInviteHandler(const QUuid& eufguid, EufInviteHandler *eufhandler)
{
  // Check whether an INVITE handler is already registered
  // with the specified euf GUID.
  if (!eufInviteHandlers_.contains(eufguid))
  {
#ifdef P2PDEBUG_PLATFORM
    qDebug("%s: registering invite handler for eufguid=%s", Q_FUNC_INFO,
           qPrintable(eufguid.toString().toUpper()));
#endif
    // If not, add the specified INVITE handler and
    // associated euf GUID.
    eufInviteHandlers_.insert(eufguid, eufhandler);
  }
#ifdef P2PDEBUG_PLATFORM
  else
  {
    qDebug("%s: invite handler for eufguid=%s ALREADY registered", Q_FUNC_INFO,
           qPrintable(eufguid.toString().toUpper()));
  }
#endif
}

void Platform::shutdown()
{
  // Check whether the tcp listener is active.
  if (tcplistener_->isListening())
  {
#ifdef P2PDEBUG_PLATFORM
    qDebug("%s: socket(%d) listening=%s", Q_FUNC_INFO,
           tcplistener_->socketDescriptor(),
           qPrintable(QVariant(tcplistener_->isListening()).toString()));
#endif
    // If so, shutdown the tcp listener.
    tcplistener_->close();
  }

#ifdef P2PDEBUG_PLATFORM
  qDebug("%s: terminating (%d) endpoint(s)", Q_FUNC_INFO,
         endpoints_.count());
#endif

  for(qint32 i=0; i < endpoints_.count(); ++i)
  {
    // Shutdown the signaling endpoint.
    endpoints_.values()[i]->shutdown();
  }

  // Check whether there are any registered signaling
  // endpoints attached to the peer to peeer platform.
  if (endpoints_.count() > 0)
  {
    // If so, dispose of all the ptp endpoints.
    qDeleteAll(endpoints_);
    // Clear the endpoint list.
    endpoints_.clear();
  }
}

void Platform::waitForConnection(SocketWaitHandle *waitHandle, const QHostAddress &address)
{
  // Check whether there is a listener already registered
  // for the specified address.
  if (waitHandles_.contains(address))
  {
    // If so, ignore the duplicate.
#ifdef P2PDEBUG_PLATFORM
    qDebug("%s: already registered handle for address:%s", Q_FUNC_INFO,
           qPrintable(address.toString()));
#endif
    return;
  }

  // Add the wait handle to the list.
  waitHandles_.insert(address, waitHandle);
}

void Platform::contact_WentOffline(const QString &contact, const QString& endpointId)
{
  // TODO Implementation
}

void Platform::tcplistener_OnAcceptConnection()
{
  // Gets the incoming TCP socket connection.
  QTcpSocket *connectedSocket = tcplistener_->nextPendingConnection();
  // Get the remote address of the incoming socket connection
  const QHostAddress address = connectedSocket->peerAddress();

#ifdef P2PDEBUG_PLATFORM
  qDebug("%s: accepted socket(%d) connection from address:%s", Q_FUNC_INFO,
           connectedSocket->socketDescriptor(),
           qPrintable(address.toString()));
#endif

  // Check whether there is a registered listener for
  // the incoming socket connection.
  if (waitHandles_.contains(address))
  {
    // If so, get the wait handle for the
    // incoming socket connection.
    SocketWaitHandle * waitHandle = waitHandles_[address];
    // Attach the connected socket to the wait handle
//    waitHandle->attachSocket(connectedSocket);
  }
  else
  {
#ifdef P2PDEBUG_PLATFORM
    qDebug("%s: could not find listener for address:%s -- closing socket", Q_FUNC_INFO,
           qPrintable(address.toString()));
#endif
    // Otherwise, just close the accepted socket.
    connectedSocket->close();
  }
}

}
