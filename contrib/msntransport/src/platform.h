//
// Platform class
//
// Authors:
//    Gregg Edghill (gregg.edghill at gmail.com)
//
// Copyright (C) 2006 - 2010, Gregg Edghill
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of this software.
//
// THIS LIBRARY IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR
// MODIFY IT UNDER THE TERMS OF THE GNU LESSER GENERAL PUBLIC
// LICENSE AS PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER
// VERSION 2 OF THE LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.
//

#ifndef CLASS_P2P__COLLABORATIONPLATFORM_H
#define CLASS_P2P__COLLABORATIONPLATFORM_H

#include <QtCore/QObject>
#include <QtCore/QHash>
#include <QtNetwork/QHostAddress>
#include <QtCore/QString>
#include <QtCore/QUuid>
#include <QtCore/QVariant>
#include "msnslp/message/realtimeaddress.h"

class QTcpServer;
namespace PtpSignaling { class RtcSignalingEndpoint; }
namespace PtpTransport { class SocketWaitHandle; }

namespace PtpCollaboration
{

class EufInviteHandler;
typedef QHash<QUuid, EufInviteHandler*> EufInviteHandlerDictionary;
typedef QHash<QString, PtpSignaling::RtcSignalingEndpoint*> EndpointList;

/**
 * @brief Represents the global peer to peer (ptp) platform which provides methods for initiating
 *   and participating in peer to peer activities.
 *
 * @author Gregg Edghill
 */
class Q_DECL_EXPORT Platform : public QObject
{
  Q_OBJECT

  public:
    /** @brief Frees resources and performs other cleanup operations. */
                         ~Platform();

    /** @brief Disposes of the static platform object. */
    static void           dispose();
    /** @brief Initializes the platform object. */
    void                  startup(const QHash<QString, QVariant>& settings);
    /** @brief Gets the EUF handler associated with the specified uuid. */
    EufInviteHandler *    getEufInviteHandler(const QUuid& eufguid);
    /** @brief Gets the IP address on which to listen for incoming connections. */
//    const QHostAddress    getListeningIpAddress() const;
    /** @brief Gets the port on which to listen for incoming connections. */
    quint16               getListeningPort() const;
    /** @brief Gets the setting with the specified name. */
    const QVariant        getSetting(const QString& name) const;
    /** @brief Gets the settings for the platform. */
    const QHash<QString, QVariant> & getSettings() const;
    /** @brief Gets the signaling endpoint for the specified peer. */
    PtpSignaling::RtcSignalingEndpoint *getOrCreateEndpoint(const PtpSignaling::RealTimeAddress& target);
    /** @brief Gets a value indicating whether the computer hosting the platform is behind a firewall. */
    bool                  isBehindFirewall() const;
    /** @brief Gets the static instance of the platform. */
    static Platform *     instance();
    /** @brief Registers the handler that will handle invites received for a specific EUF. */
    void                  registerEufInviteHandler(const QUuid& eufguid, EufInviteHandler *eufhandler);
    /** @brief Shuts down the platform object and all endpoints. */
    void                  shutdown();
    /** @brief Waits for a connection to arrive from the specified client. */
    void                  waitForConnection(PtpTransport::SocketWaitHandle *listener, const QHostAddress& address);

  public Q_SLOTS:
    /** @brief Called when a contact's presence changes to offline. */
    void                  contact_WentOffline(const QString& contact, const QString& endpointId);
    /** @brief Called when the computer's ip address has changed. */
//    void                  network_OnIpAddressChanged(const QString& ipaddress);
    /** @brief Called when an incoming connection is. */
    void                  tcplistener_OnAcceptConnection();

  private:
    /** @brief Creates a new instance of the class. */
                          Platform(QObject *pobject=0l);

  private:
    QHash<QHostAddress, PtpTransport::SocketWaitHandle*> waitHandles_;
    EndpointList          endpoints_;
    EufInviteHandlerDictionary eufInviteHandlers_;
    bool                  initialized_;
    static Platform *     s_platform;
    QHash<QString, QVariant> settings_;
    QTcpServer *          tcplistener_;

  Q_DISABLE_COPY(Platform)

}; //Platform
}

#endif
