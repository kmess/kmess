//
// ContentDescription class
//
// Authors:
//    Gregg Edghill (gregg.edghill at gmail.com)
//
// Copyright (C) 2009 - 2010, Gregg Edghill
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of this software.
//
// THIS LIBRARY IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR
// MODIFY IT UNDER THE TERMS OF THE GNU LESSER GENERAL PUBLIC
// LICENSE AS PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER
// VERSION 2 OF THE LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.
//

#ifndef CLASS_P2P__CONTENTDESCRIPTION_H
#define CLASS_P2P__CONTENTDESCRIPTION_H

#include <QtCore/QByteArray>
#include <QtCore/QString>
#include "msnslp/message/contenttype.h"

using namespace PtpSignaling;

namespace PtpCore
{

/**
 * @brief Repesents the description of media content in a MIME message.
 *
 * @author Gregg Edghill
 */
class ContentDescription
{
  public:
    /** @brief Creates a new instance of the class. */
    ContentDescription(const ContentType& contentType, const QByteArray& body);
    /** @brief Frees resources and performs other cleanup operations. */
    ~ContentDescription();

  public:
    /** @brief Gets the content type for the body content. */
    const ContentType &   	getContentType() const;
    /** @brief Represents the body content in bytes or null if there is none. */
    const QByteArray & 			getBody() const;
    /** @brief Set the body content. */
    void									  setBody(const QByteArray& body);
    /** @brief Gets the string representation of the content description. */
    const QString           toString() const;

  private:
    QByteArray            	body_;
    ContentType          		contentType_;

}; // ContentDescription
}

#endif
