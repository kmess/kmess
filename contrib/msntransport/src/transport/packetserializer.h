//
// PacketSerializer class
//
// Authors:
//    Gregg Edghill (gregg.edghill at gmail.com)
//
// Copyright (C) 2006, Gregg Edghill
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of this software.
//
// THIS LIBRARY IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR
// MODIFY IT UNDER THE TERMS OF THE GNU LESSER GENERAL PUBLIC
// LICENSE AS PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER
// VERSION 2 OF THE LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.
//

#ifndef CLASS_P2P__TLPPACKETSERIALIZER_H
#define CLASS_P2P__TLPPACKETSERIALIZER_H

#include <QtCore/QDataStream>
#include <QtGlobal>

namespace PtpTransport
{

class Packet;

/**
 * @brief Provides methods to serialize and deserialize a packet in binary format.
 *
 * @author Gregg Edghill
 */
class PacketSerializer
{
  public:
    /** @brief Deserializes the specified stream into a packet. */
    static Packet * readPacket(QDataStream *stream);
    /** @brief Serializes the packet to the given stream. */
    static void     writePacket(QDataStream *stream, Packet *packet);

  private:
    inline          PacketSerializer(){}

}; // PacketSerializer
}

#endif

