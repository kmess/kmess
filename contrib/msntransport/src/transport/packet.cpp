//
// Packet
//
// Summary: Represents the unit of communication between transports.
//
// Authors:
//    Gregg Edghill (gregg.edghill at gmail.com)
//
// Copyright (C) 2006 - 2010, Gregg Edghill
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of this software.
//
// THIS LIBRARY IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR
// MODIFY IT UNDER THE TERMS OF THE GNU LESSER GENERAL PUBLIC
// LICENSE AS PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER
// VERSION 2 OF THE LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.
//

#include "packet.h"
#include "msntransportconfig.h"
#include <QtCore/QBuffer>

#ifdef MSNTRANSPORT_DEBUG
#include <QtCore/QLatin1String>
#include <QtCore/QString>
#include <QtCore/QTextStream>
#include <QtCore/QVariant>
#include <QtDebug>
#endif

namespace PtpTransport
{

void Packet::dumpPacketInfo(const Packet::Header& header, bool inbound)
{
  QString info = QLatin1String("\r\n");
  const QString direction = inbound ? QLatin1String("inbound") : QLatin1String("outbound");

  QTextStream writer(&info, QIODevice::WriteOnly);
  writer << ">>> " << direction << " TLP packet BEGIN\r\n";

  unsigned char *data = (unsigned char*)(&header);
  QString hex = QLatin1String("");
  for (quint32 i=0, index=1; i < sizeof(Packet::Header); ++i, ++index)
  {
    unsigned char ch = data[i];
    hex += QString("%1 ").arg(quint32(ch), 2, 16, QLatin1Char('0'));

    if (16 == index)
    {
      // Write the hex string to the stream.
      writer << hex;
      hex = QLatin1String("");

      if (15 == i)
      {
          writer << "    Destination: " << header.destination
                 << ", Seq: "           << header.identifier
                 << ", Offset: "        << header.offset
                 << "\r\n";
      }

      if (31 == i)
      {
          writer << "    Chunk: "       << header.offset
                 << ".."                << header.offset+header.payloadSize
                 << " of "              << header.blobSize
                 << ", Flag: "          << QString("0x%1").arg(header.type, 8, 16, QLatin1Char('0'))
                 << " ("                << header.type
                 << ")\r\n";
      }

      if (47 == i)
      {
          writer << "    Last Rcvd: "   << header.lprcvd
                 << ", Last Sent: "     << header.lpsent
                 << ", Last Len: "      << header.lpsize;
      }

      index = 0;
    }
  }

  writer << "\r\n<<< " << direction << " TLP packet END\r\n";
#ifdef MSNTRANSPORT_DEBUG
  qDebug() << Q_FUNC_INFO << info << endl;
#endif
}



Packet::Packet() : ownsPayload_(true)
{
  // Create an empty byte array buffer which will
  // be used to store the packet's payload data,
  // if any.
  payload_            = new QBuffer();
  // Set the buffer's access mode to read/write.
  payload_->open(QIODevice::ReadWrite);

   // Set the destination of the packet.
  header_.destination = 0;
  // Set the sequence number of the packet.
  header_.identifier  = 0;
  // Set the datagram size offset of the packet.
  header_.offset      = 0l;
  // Set the datagram size of the packet.
  header_.blobSize    = 0l;
  // Set the payload size of the packet.
  header_.payloadSize = 0;
  // Set the type of the packet.
  header_.type        = 0;
  // Set the correlation info of the packet.
  header_.lprcvd      = 0;
  header_.lpsent      = 0;
  header_.lpsize      = 0l;
}



Packet::~Packet()
{
  // Check whether the byte array buffer
  // is the internal buffer and therefore,
  // not an external buffer (e.g. file)
  if (ownsPayload_)
  {
    // If so, delete the payload (data).
    delete payload_;
  }
}



const Packet::Header & Packet::getHeader() const
{
  return header_;
}



Packet::Header & Packet::getHeader()
{
  return header_;
}



QIODevice * Packet::getPayload() const
{
  return payload_;
}



void Packet::setPayload(QIODevice *payload)
{
  if (!payload)
  {
    return;
  }

  // Check whether the byte array buffer
  // is the internal buffer and therefore,
  // not an external buffer (e.g. file)
  if (ownsPayload_)
  {
    // If so, dispose of the payload (data).
    delete payload_;
    // Indicate what we no longer own
    // the byte array buffer that stores
    // the payload data, if any
    ownsPayload_ = false;
  }

  // Set the buffer to the specified external
  // data source buffer.
  payload_ = payload;
}



quint32 Packet::getSize() const
{
  return (sizeof(Packet::Header) + payload_->size());
}

}

