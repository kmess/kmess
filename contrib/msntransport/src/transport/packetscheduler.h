//
// PacketScheduler class
//
// Authors:
//    Gregg Edghill (gregg.edghill at gmail.com)
//
// Copyright (C) 2006 - 2010, Gregg Edghill
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of this software.
//
// THIS LIBRARY IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR
// MODIFY IT UNDER THE TERMS OF THE GNU LESSER GENERAL PUBLIC
// LICENSE AS PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER
// VERSION 2 OF THE LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.
//

#ifndef CLASS_P2P__TLPPACKETSCHEDULER_H
#define CLASS_P2P__TLPPACKETSCHEDULER_H

#include <QtCore/QObject>
#include <QtCore/QHash>
#include <QtCore/QList>

class QTimer;

namespace PtpTransport
{

class Packet;
typedef QHash<quint8, QList<quint32> > PacketClassList;
typedef QList<Packet*> PacketQueue;
class TransportImpl;

/**
 * @brief Represents a transport layer packet scheduler which manages the scheduling of packets
 *
 * and controls overall packet transmission based on priority and packet type (packet shaping).
 * The packet scheduler uses a priority scheduling model whereby, it gives priority to packets
 * of a certain class and continues scheduling these packets until the list of packets becomes
 * empty. Priority is given to control and message packets which are scheduled in a FIFO manner,
 * and DATA packets are scheduled in a random manner.
 *
 * @code
 *
 *                       +-----+       +---+
 *                       |ctrl0|  ==>  |   |
 *                       +-----+       |   |      nth                             first
 *                                     | s |      packet                          packet
 *               +----+   +----+       | c |        |                               |
 *               |msg1|   |msg0|  ==>  | h |        v                               V
 *               +----+   +----+       | e |     +------+ +------+ +----+ +----+ +-----+
 *                                     | d | ==> |datay0| |datax0| |msg1| |msg0| |ctrl0|
 *        ,--  +------+ +------+       | u |     +------+ +------+ +----+ +----+ +-----+
 *  flow  |    |datax1| |datax0|  ==>  | l |
 *        '--  +------+ +------+       | e |
 *                                     | r |
 *             +------+ +------+       |   |
 *             |datay1| |datay0|  ==>  |   |
 *             +------+ +------+       +---+
 *
 * @endcode
 *
 * The scheduling of transport layer packets therefore follows a similar scheme to Class Based
 * Weighted Fair Queuing (CBWFQ) whereby all outgoing packets are classified to determine the
 * network flow to which the packets belong.  Once it is known what packets belong to which
 * network flows, the WFQ can provide all flows with equal access to the underlying transport
 * bridge the packets will be sent on.
 *
 * @author Gregg Edghill
 */
class PacketScheduler : public QObject
{
  Q_OBJECT

  public:
    /** @brief Creates a new instance of the class PacketScheduler. */
                          PacketScheduler(TransportImpl *transport);
    /** @brief Frees resources and performs other cleanup operations. */
                         ~PacketScheduler();

  public:
    /** @brief Indicates whether the scheduler is running. */
    bool                  isRunning() const;
    /** @brief Starts the scheduling of packets. */
    void                  start();
    /** @brief Stops the scheduling of packets. */
    void                  stop();

  private Q_SLOTS:
    /** @brief Called when the packet scheduling timespan as expired. */
    void                  timer_OnSchedulePacketsToSend();

  private:
    /** @brief Creates the packet classes used for scheduling queued packets. */
    void                  createPacketClasses();
    /** @brief Gets the next packet that is ready to be sent. */
    Packet *              getNextPacket(PacketQueue *queue);
    /** @brief Gets a list of packets from the supplied packet queue that match the specified packet class. */
    PacketQueue           selectPacketsBy(const QList<quint32> & packetclass, PacketQueue* queue);

  private:
    bool                  isRunning_;
    PacketClassList       packetclasses_;
    TransportImpl *       transport_;
    QTimer *              schedulingtimer_;

  Q_DISABLE_COPY(PacketScheduler)

}; // PacketScheduler
}

#endif

