//
// TransportImpl class
//
// Authors:
//    Gregg Edghill (gregg.edghill at gmail.com)
//
// Copyright (C) 2006 - 2010, Gregg Edghill
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of this software.
//
// THIS LIBRARY IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR
// MODIFY IT UNDER THE TERMS OF THE GNU LESSER GENERAL PUBLIC
// LICENSE AS PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER
// VERSION 2 OF THE LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.
//

#ifndef CLASS_P2P__TLPTRANSPORTIMPL_H
#define CLASS_P2P__TLPTRANSPORTIMPL_H

#include <QtCore/QObject>
#include <QtCore/QHash>
#include <QtCore/QList>
#include <QtCore/QUuid>
#include <QtCore/QVariant>
#include "packet.h"
class QBuffer;
class QIODevice;

namespace PtpTransport
{

class DirectTransportBridge;
class PacketScheduler;
class SwitchboardBridge;
class TransportBridge;

typedef QHash<quint32, QBuffer*> DatagramBufferList;
typedef QList<Packet*> PacketQueue;
typedef QHash<quint32, PacketQueue *> PacketQueueList;
typedef QHash<quint32, QHash<QString, QVariant> > TransportBridgeInfo;
typedef QHash<quint32, TransportBridge *> TransportBridgeList;

/**
 * @brief Represents a transport layer implementation that manages and controls
 * end to end packet transmission.
 *
 * The transport layer provides guaranteed (reliable) end-to-end packet transfer of upper layer
 * protocol data independent of the underlying transport bridge, along with error control, fragmentation,
 * reconstruction, error correction and flow control. The TLP can be characterized as record-oriented, whereby it
 * transports data in terms of messages so that a group of bytes (message) sent in one transmission
 * operation (record) is read exactly as that group at the receiver application. The TLP assigns a
 * sequence number to each message sent in a stream. This allows independent ordering of messages in
 * different streams. In addition, it utilises a packet scheduler to control packet
 * transmission based on priority and packet type (packet shaping).
 *
 * The transport layer implementation is divided into three objects, the transport layer, its
 * underlying transport bridge(s) and its packet scheduler.
 *
 * @code
 *
 *  +------------------------+    logical connection    +------------------------+
 *  |      Upper Layers      | <----------------------> |      Upper Layers      |
 *  +------------------------+                          +------------------------+
 *  +------------------------+                          +------------------------+  --,
 *  |  Transport Layer with  |    logical connection    |  Transport Layer with  |    |
 *  |    Packet Scheduler    | <----------------------> |    Packet Scheduler    |    | transport
 *  +------------------------+                          +------------------------+    | implementation
 *  +------------------------+    logical connection    +------------------------+    |
 *  |    Transport Bridge    | <----------------------> |    Transport Bridge    |    |
 *  +------------------------+                          +------------------------+  --'
 *  +--------------+   +-----+                          +--------------+   +-----+
 *  | TRUDP | TURN |   | SWB |                          | TRUDP | TURN |   | SWB |
 *  |------------------------|                          |------------------------|
 *  |     UDP      |   TCP   |                          |     UDP      |   TCP   |
 *  +------------------------+                          +------------------------+
 *  +------------------------+                          +------------------------+
 *  |         IPv4/6         | <======================> |         IPv4/6         |
 *  +------------------------+                          +------------------------+
 * @endcode
 *
 * The transport layer uses only one transport bridge at any given time to send or receive packets.
 * If multiple transport bridges are connected, the transport layer will try to select the best
 * bridge to use.  Preference is given to direct transport bridges over indirect bridges.
 *
 * Outlined below is the typical behavior of the transport layer when certain packet types
 * are sent/received between endpoints.
 *
 *  Endpoint A sends:     Endpoint B responds:    Comment:
 *  -----------------     --------------------    ----------------------------------------------
 *  DATA                   ACKNOWLEDGE (ACK)      Endpoint B only sends ACK when the entire
 *                                                data unit has been received. That is, the
 *                                                packet fragments have been reconstructed
 *                                                if necessary into the original PDU.
 *
 *  ACKNOWLEDGE (ACK)      (nothing)              Endpoint B should remove its unACKed packet
 *                                                that corresponds to the ACK packet and
 *                                                stop its unACKed packet timer item. Endpoint B
 *                                                should also notify the upper layer session
 *                                                that the data was ACKed (received).
 *
 *  NEGATIVE ACK (NAK)     (nothing)              Endpoint A has indicated that data from data
 *                                                transfer is out-of-order. Endpoint B should
 *                                                stop sending data transfer and start re-sending
 *                                                data transfer at the data offset indicated.
 *
 *  REQUEST ACK (RAK)      ACKNOWLEDGE (ACK)      Endpoint B should send an ACK if possible
 *                                                that corresponds to the packet indicated by
 *                                                the RAK packet received.
 *
 *  CANCEL RECEIVE (CAN)   (nothing)              Endpoint B should stop receiving the data
 *                                                transfer and notify the upper layer session
 *                                                of the data CANcelation.
 *
 *  CANCEL SEND (ERR)      (nothing)              Endpoint B should stop sending the data
 *                                                transfer and notify the upper layer session
 *                                                of the data transfer ERRor.
 *
 *  TRANSPORT RESET (RST)  (nothing)              Endpoint B should cancels all active
 *                                                data transfers (both sending and receiving)
 *                                                and reset the transport layer to a known
 *                                                state.
 *
 * @author Gregg Edghill
 */
class Q_DECL_EXPORT TransportImpl : public QObject
{
  Q_OBJECT

  public:
    /** @brief Creates a new instance of the class TransportImpl. */
                          TransportImpl(QObject *pobject);
    /** @brief Frees resources and performs other cleanup operations. */
                         ~TransportImpl();

  public:
    /** @brief Gets or creates an switchboard (indirect) bridge. */
    SwitchboardBridge *   getOrCreateSbBridge();
    /** @brief Gets a value indicating whether the transport's underlying bridge is connected. */
    bool                  isConnected() const;
    /** @brief Sends the supplied array of bytes to the specified destination. */
    quint32               send(const QByteArray& datagram, const quint32 dest, const quint32 correlationId, bool isulpdata=false);
    /** @brief Sends the supplied data stream to the specified destination. */
    void                  send(QIODevice *stream, const quint32 dest);

  private Q_SLOTS:
    /** @brief Called when an underlying transport bridge connects to its remote endpoint. */
    void                  bridge_OnConnected();
    /** @brief Called when an underlying transport bridge is connecting to its remote endpoint. */
    void                  bridge_OnConnecting();
    /** @brief Called when raw data is received from the current transport bridge. */
    void                  bridge_OnDataReceived(const QByteArray& datachunk);
    /** @brief Called when raw data has been sent on the current transport bridge. */
    void                  bridge_OnDataSent(const quint32 packetid);
    /** @brief Called when raw data cannot be sent on the current transport bridge. */
    void                  bridge_OnDataSendFailed(const quint32 packetid);
    /** @brief Called when an underlying transport bridge disconnects from its remote endpoint. */
    void                  bridge_OnDisconnected();
    /** @brief Called when an underlying transport bridge is disconnecting from its remote endpoint. */
    void                  bridge_OnDisconnecting();
    /** @brief Called when an underlying transport bridge encounters an error. */
    void                  bridge_OnFault();
    /** @brief Called when the packet acknowledge timespan has expired. */
    void                  waititem_OnAcknowledgeTimespanExpired();

  Q_SIGNALS:
    /** @brief Indicates that the transport layer's underlying bridge to the remote endpoint is connected. */
    void                  connected();
    /** @brief Indicates that the transport layer's underlying bridge to the remote endpoint is disconnected. */
    void                  disconnected();
    /** @brief Indicated that the transport layer encountered an error. */
    void                  error(const quint32 destination);

    void                  message(const QByteArray&, const qint32, const qint32);

  private:
    /** @brief Adds a packet to the unacknowledged packet list. */
    void                  addPacketToUnacknowledgedList(Packet *packet);
    /** @brief Removes a packet from the unacknowledged packet list. */
    void                  removePacketFromUnacknowledgedList(Packet *packet);
    /** @brief Adds the specified transport bridge to the list. */
    void                  addTransportBridge(TransportBridge *bridge);
    /** @brief Removes the transport bridge with the specified id. */
    void                  removeTransportBridge(const quint32 bridgeid);
    /** @brief Gets the ID of the transport bridge to use for sending packets. */
    quint32               selectTransportBridgeToUse();
    /** @brief Gets the next unique id to assigned to created transport bridge. */
    quint32               nextBridgeId();
    /** @brief Gets the next sequence number to assigned to an outgoing packet. */
    quint32               nextPacketSequenceNumber();
    /** @brief Gets the datagram buffer with the specified id. */
    QBuffer *             getDatagramBufferBy(const quint32 id);
    /** @brief Reassembles the entire datagram (blob) received from the supplied packet. */
    QByteArray            reconstructEntireDatagramFrom(Packet *packet);
    /** @brief Sends an authentication key (nonce) to validate the bridge with specified bridge id. */
    void                  sendBridgeAuthenticationKey(const QUuid& key, const quint32 bridgeid);
    /** @brief Creates a packet that can be used for data or control transmission. */
    Packet *              createGenericPacket();
    /** @brief Sends the supplied packet via the bridge with the specified id. */
    void                  sendPacket(Packet* packet, const quint32 bridgeId);
    /** @brief Queues the supplied packet for transmission. */
    void                  queuePacket(Packet *packet, const quint32 bridgeid, bool prepend=false);
    /** @brief Sends an ACK control packet. */
    void                  sendAcknowledge(const quint32 dest, const quint32 lprcvd, const quint32 lpsent, const quint64 lpsize);
    /** @brief Sends a CAN (cancel) control packet. */
    void                  sendCancelTransfer(const quint32 dest, const quint32 lprcvd, const quint32 lpsent, const quint64 lpsize);
    /** @brief Sends a control packet with the specified type. */
    void                  sendControlPacket(const Packet::TLP_PACKET_TYPE type, const quint32 dest, const quint32 lprcvd, const quint32 lpsent, const quint64 lpsize);
    /** @brief Sends an ERR control packet. */
    void                  sendError(const quint32 dest, const quint32 lprcvd, const quint32 lpsent, const quint64 lpsize);
    /** @brief Sends a NAK control packet. */
    void                  sendNonAcknowledge(const quint32 dest, const quint32 lprcvd, const quint32 lpsent, const quint64 lpsize);
    /** @brief Sends a RAK control packet. */
    void                  sendRequestForAcknowledge(const quint32 dest, const quint32 lprcvd, const quint32 lpsent, const quint64 lpsize);
    /** @brief Sends a transport layer RESET control packet. */
    void                  sendReset(const quint32 dest, const quint32 lprcvd, const quint32 lpsent, const quint64 lpsize);
    /** @brief Gets the list of bridges created. */
    TransportBridgeList & getTransportBridges();
    /** @brief Gets the list of packet queues for outgoing packets. */
    PacketQueueList &     getOutgoingPacketQueues();
    /** @brief Gets the list of sent packets for a transport bridge with the specified id. */
    PacketQueue *         getSentPacketQueueBy(const quint32 bridgeId ) const;
    /** @brief Moves all packets from one queue to another. */
    void                  movePacketsBetweenBridgeQueues(const quint32 oldqueueid, const quint32 newqueueid);
    /** @brief Stops the sending of data to specified destination. */
    void                  stopSendingDataTo(const quint32 destination);
    /** @brief Called when an authentication key for a transport bridge is received. */
    void                  onBridgeAuthenticationKeyReceived(const QUuid& key, const quint32 bridgeid);
    /** @brief Called when a non-ACK control packet is received. */
    void                  onNonAcknowledgeControlPacketReceived(Packet *packet);

  private:
    DatagramBufferList    databuffers_;
    quint32               currentBridgeId_;
    quint32               nextBridgeId_;
    quint32               nextPacketId_;
    TransportBridgeList   transportBridges_;
    TransportBridgeInfo   transportInfo_;
    PacketQueueList       outgoingPackets_;
    PacketScheduler *     packetScheduler_;
    PacketQueueList       sentPackets_;

  Q_DISABLE_COPY(TransportImpl)

  friend class PacketScheduler;

}; // TransportImpl
}

#endif

