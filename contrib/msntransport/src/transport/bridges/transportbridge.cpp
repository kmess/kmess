//
// TransportBridge
//
// Summary: Represents a transport layer bridge used to send and
//   received data between transport layers.
//
// Authors:
//    Gregg Edghill (gregg.edghill at gmail.com)
//
// Copyright (C) 2006 - 2010, Gregg Edghill
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of this software.
//
// THIS LIBRARY IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR
// MODIFY IT UNDER THE TERMS OF THE GNU LESSER GENERAL PUBLIC
// LICENSE AS PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER
// VERSION 2 OF THE LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.
//

#include "transportbridge.h"

namespace PtpTransport
{

TransportBridge::TransportBridge(QObject *pobject) : QObject(pobject), state_(TransportBridge::TLPTBS_CREATED)
{}

TransportBridge::~TransportBridge()
{}

void TransportBridge::connect()
{
  if (state_ == TLPTBS_FAULTED || state_ == TLPTBS_DISCONNECTING)
  {
    return;
  }

  if (state_ != TLPTBS_CONNECTED || state_ != TLPTBS_CONNECTING)
  {
    // Set the state to connecting
    state_ = TLPTBS_CONNECTING;

    onConnect();
  }
}

void TransportBridge::disconnect()
{
  if (state_ != TLPTBS_DISCONNECTED || state_ != TLPTBS_DISCONNECTING)
  {
    // Set the state to disconnecting
    state_ = TLPTBS_DISCONNECTING;
    // Disconnect the underlying network connection
    onDisconnect();
  }
}

void TransportBridge::fault()
{
  if (state_ != TLPTBS_FAULTED)
  {
    // Set the state to faulted
    state_ = TLPTBS_FAULTED;
    // Handle the error
    onFault();

    // Signal that an error has occurred.
    emit error();
  }
}

TransportBridge::TLP_TRANSPORT_BRIDGE_STATE TransportBridge::getState() const
{
  return state_;
}

void TransportBridge::setState(TLP_TRANSPORT_BRIDGE_STATE state)
{
  state_ = state;
}

void TransportBridge::onFault()
{}

}

