//
// DirectTransportBridge class
//
// Authors:
//    Gregg Edghill (gregg.edghill at gmail.com)
//
// Copyright (C) 2006 - 2010, Gregg Edghill
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of this software.
//
// THIS LIBRARY IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR
// MODIFY IT UNDER THE TERMS OF THE GNU LESSER GENERAL PUBLIC
// LICENSE AS PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER
// VERSION 2 OF THE LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.
//

#ifndef CLASS_P2P__TLPDIRECTTRANSPORTBRIDGE_H
#define CLASS_P2P__TLPDIRECTTRANSPORTBRIDGE_H

#include "transportbridge.h"
#include <QtCore/QList>
#include <QtNetwork/QHostAddress>

namespace PtpTransport
{

/**
 * @brief Represents a transport layer bridge which can establish a connection to the remote
 * endpoint directly using specific transport protocols.
 *
 * @author Gregg Edghill
 */
class DirectTransportBridge : public TransportBridge
{
  Q_OBJECT

  public :
    /** @brief Creates a new instance of the class DirectTransportBridge. */
                           DirectTransportBridge(const QList<QHostAddress> &ipAddresses, const quint16 port, QObject *pobject);
    /** @brief Frees resources and performs other cleanup operations. */
    virtual               ~DirectTransportBridge();

  public:
    /** @brief Gets a value indicating whether the direct transport bridge is connecting to a remote endpoint. */
    virtual bool           isConnector() const = 0;

  protected:
    /** @brief Gets the list of ip addresses that the transport bridge will attempt to connect to. */
    const QList<QHostAddress> & getIpAddresses() const;
    /** @brief Gets the list of ip addresses that the transport bridge will attempt to connect to. */
    QList<QHostAddress> &  getIpAddresses();
    /** @brief Gets the port that the transport bridge will connect to. */
    quint16                getPort() const;

  private:
    QList<QHostAddress>    ipAddresses_;
    quint16                port_;

  Q_DISABLE_COPY(DirectTransportBridge)

}; // DirectTransportBridge
}

#endif
