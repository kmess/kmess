//
// SwitchboardBridge
//
// Summary: Represents a transport layer bridge used to send and
//   received data between transport layers.  The switchboard bridge
//   connects the transport layers via the switchboard network.
//
// Authors:
//    Gregg Edghill (gregg.edghill at gmail.com)
//
// Copyright (C) 2006 - 2010, Gregg Edghill
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of this software.
//
// THIS LIBRARY IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR
// MODIFY IT UNDER THE TERMS OF THE GNU LESSER GENERAL PUBLIC
// LICENSE AS PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER
// VERSION 2 OF THE LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.
//

#include "switchboardbridge.h"
#include "msntransportconfig.h"
#include "transport/packet.h"
#include <QtDebug>

#ifdef MSNTRANSPORT_DEBUG
  #define P2PDEBUG_TLP_SBBRIDGE
#endif

namespace PtpTransport
{

SwitchboardBridge::SwitchboardBridge(const quint32 bridgeId, QObject *pobject) : TransportBridge(pobject)
{
  properties_.insert(QLatin1String("bridgeId"), bridgeId);
  properties_.insert(QLatin1String("sendTimeoutTimespan"), 30000);
  properties_.insert(QLatin1String("maxDataChunkSize"), 1202);
  properties_.insert(QLatin1String("maxPendingChunks"), 3);
}

SwitchboardBridge::~SwitchboardBridge()
{
}

const QHash<QString, QVariant> & SwitchboardBridge::getProperties() const
{
  return properties_;
}

quint32 SwitchboardBridge::getId() const
{
  return properties_.value(QLatin1String("bridgeId")).toUInt();
}

void SwitchboardBridge::send(const QByteArray& datachunk, const quint32 packetid)
{
#ifdef P2PDEBUG_TLP_SBBRIDGE
  qDebug("%s: About to send datachunk of size %d bytes", Q_FUNC_INFO,
         datachunk.size());
#endif
  if (TransportBridge::getState() == TransportBridge::TLPTBS_CONNECTED)
  {
    // If the bridge is connected, try to send the specified byte array.
    qint32 cookie = -1;
    // Signal that we want to send data.
    emit sendDataChunk(datachunk, &cookie);

    if (cookie > 0)
    {
#ifdef P2PDEBUG_TLP_SBBRIDGE
      qDebug("%s: datachunk has id of %d (cookie=%d)", Q_FUNC_INFO,
             cookie, packetid);
#endif
      // Add a cookie for the sent data.
      cookies_.insert(cookie, packetid);

      // TODO start a timer to detect send timeouts.
    }
    else
    {
      // Otherwise, queue the datachunk and its corresponding id
      pendingDataChunks_.append(qMakePair(packetid, datachunk));
    }
  }
  else
  {
    // Otherwise, queue the datachunk and its corresponding id
    pendingDataChunks_.append(qMakePair(packetid, datachunk));
  }
}

void SwitchboardBridge::fireDataReceived(const QByteArray& datachunk)
{
  const qint32 maxDataChunkSize = properties_["maxDataChunkSize"].toInt();
  if (datachunk.size() > (qint32)(sizeof(Packet::Header) + maxDataChunkSize + 4))
  {
#ifdef P2PDEBUG_TLP_SBBRIDGE
    qDebug("%s: got data chunk whose size (%d) exceeds bridge mtu size=%d bytes -- dropping",
           Q_FUNC_INFO, datachunk.size(), maxDataChunkSize);
#endif
    return;
  }

#ifdef P2PDEBUG_TLP_SBBRIDGE
  qDebug("%s: received data chunk of size (%d) bytes", Q_FUNC_INFO, datachunk.size() - 4);
#endif

  // Signal that we have received a data chunk
  emit dataReceived(datachunk);
}

void SwitchboardBridge::fireDataSent(const quint32 cookie)
{
  if (cookies_.contains(cookie))
  {
    const quint32 packetid = cookies_[cookie];
#ifdef P2PDEBUG_TLP_SBBRIDGE
    qDebug("%s: datachunk sent (pid= %d)", Q_FUNC_INFO, packetid);
#endif
    // Signal that the datachunk which corresponds to
    // the supplied packet id was sent
    emit dataSent(packetid);
    // Removed the correlation cookie from the list.
    cookies_.remove(cookie);
  }
#ifdef P2PDEBUG_TLP_SBBRIDGE
  else
  {
    qDebug("%s: cannot find cookie=%d in list -- ignoring", Q_FUNC_INFO, cookie);
  }
#endif
}

void SwitchboardBridge::fireDataSendFailed(const quint32 cookie)
{
  if (cookies_.contains(cookie))
  {
    const quint32 packetid = cookies_[cookie];
#ifdef P2PDEBUG_TLP_SBBRIDGE
    qDebug("%s: datachunk send failed (pid=%d)", Q_FUNC_INFO, packetid);
#endif
    // Signal that the datachunk which corresponds to
    // the supplied packet id was not sent
    emit dataSendFailed(packetid);
    // Removed the correlation cookie from the list.
    cookies_.remove(cookie);
  }
#ifdef P2PDEBUG_TLP_SBBRIDGE
  else
  {
    qDebug("%s: cannot find cookie=%d in list -- ignoring", Q_FUNC_INFO, cookie);
  }
#endif
}

void SwitchboardBridge::sendPendingDataChunks()
{
  QList<QPair<quint32, QByteArray> >::Iterator item = pendingDataChunks_.begin();
  while (item != pendingDataChunks_.end())
  {
    // Get the serialized packet (datachunk) and packet id.
    quint32 packetid = (*item).first;
    QByteArray datachunk = (*item).second;

    // Try to send the specified byte array.
    qint32 cookie = -1;
    // Signal that we want to send data.
    emit sendDataChunk(datachunk, &cookie);

    // Determine whether the datachunk was sent
    if (cookie > 0)
    {
#ifdef P2PDEBUG_TLP_SBBRIDGE
      qDebug("%s: datachunk has id of %d (pid=%d)", Q_FUNC_INFO, cookie, packetid);
#endif
      // If so, add a cookie for the sent datachunk.
      cookies_.insert(cookie, packetid);
      // Remove the datachunk from the list
      pendingDataChunks_.erase(item);
    }
    else
    {
      // Otherwise, stop.
      break;
    }
  }
}

void SwitchboardBridge::onConnect()
{
  TransportBridge::setState(TransportBridge::TLPTBS_CONNECTED);
  // Determine whether there are any pending data chunks
  // to be sent.
  if (pendingDataChunks_.size() > 0)
  {
    // If so, try to send the data.
    sendPendingDataChunks();
  }

  // Raise the connected event signal.
  emit connected();
}

void SwitchboardBridge::onDisconnect()
{
  TransportBridge::setState(TransportBridge::TLPTBS_DISCONNECTED);
  // Raise the disconnected event signal.
  emit disconnected();
}

}

