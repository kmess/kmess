//
// DirectTransportBridge
//
// Summary: Represents a transport layer bridge which can establish
//   a connection to the remote endpoint directly using specific
//   transport protocols (e.g. TCP).
//
// Authors:
//    Gregg Edghill (gregg.edghill at gmail.com)
//
// Copyright (C) 2006 - 2010, Gregg Edghill
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of this software.
//
// THIS LIBRARY IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR
// MODIFY IT UNDER THE TERMS OF THE GNU LESSER GENERAL PUBLIC
// LICENSE AS PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER
// VERSION 2 OF THE LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.
//

#include "directtransportbridge.h"

namespace PtpTransport
{

DirectTransportBridge::DirectTransportBridge(const QList<QHostAddress> &ipAddresses, const quint16 port, QObject *pobject)
: TransportBridge(pobject)
{
  ipAddresses_ = ipAddresses;
  port_ = port;
}

DirectTransportBridge::~DirectTransportBridge()
{}

const QList<QHostAddress> & DirectTransportBridge::getIpAddresses() const
{
  return ipAddresses_;
}

QList<QHostAddress> & DirectTransportBridge::getIpAddresses()
{
  return ipAddresses_;
}

quint16 DirectTransportBridge::getPort() const
{
  return port_;
}

}

