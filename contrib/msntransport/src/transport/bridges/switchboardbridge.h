//
// SwitchboardBridge class
//
// Authors:
//    Gregg Edghill (gregg.edghill at gmail.com)
//
// Copyright (C) 2006 - 2010, Gregg Edghill
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of this software.
//
// THIS LIBRARY IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR
// MODIFY IT UNDER THE TERMS OF THE GNU LESSER GENERAL PUBLIC
// LICENSE AS PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER
// VERSION 2 OF THE LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.
//

#ifndef CLASS_P2P__TLPSWITCHBOARDBRIDGE_H
#define CLASS_P2P__TLPSWITCHBOARDBRIDGE_H

#include "transportbridge.h"

namespace PtpTransport
{

typedef QList<QPair<quint32, QByteArray> > DataChunkList;

/**
 * @brief Represents a transport layer bridge used to send and received data between transport layers.
 * The switchboard bridge connects the transport layer to the messenger switchboard network.
 *
 * @author Gregg Edghill
 */
class Q_DECL_EXPORT SwitchboardBridge : public TransportBridge
{
  Q_OBJECT

  public:
    /** @brief Creates a new instance of the class Transport Bridge. */
                       SwitchboardBridge(const quint32 bridgeId, QObject *pobject);
    /** @brief Frees resources and performs other cleanup operations. */
    virtual           ~SwitchboardBridge();
    /** @brief When overriden in a derived class, gets the properties of the transport bridge. */
    virtual const QHash<QString, QVariant> & getProperties() const;
    /** @brief When overriden in a derived class, returns a value that uniquely identifies the transport bridge. */
    virtual quint32    getId() const;
    /** @brief Sends the specified byte array to the remote endpoint via the switchboard network. */
    virtual void       send(const QByteArray& datachunk, const quint32 packetid);
    /** @brief Called when data is received. */
    void               fireDataReceived(const QByteArray& datachunk);
    /** @brief Called when data has been sent. */
    void               fireDataSent(const quint32 cookie);
    /** @brief Called when the sending of data has failed. */
    void               fireDataSendFailed(const quint32 cookie);

  protected:
    /** @brief Connects the transport bridge to the remote endpoint. */
    virtual void       onConnect();
    /** @brief Disconnects the transport bridge from the remote endpoint. */
    virtual void       onDisconnect();

  Q_SIGNALS:
    /** @brief Indicates that data is ready to be sent. */
    void               sendDataChunk(const QByteArray& datachunk, qint32 *cookie);

  private:
    /** @brief Sends the data chunks that have not been sent. */
    void               sendPendingDataChunks();

  private:
    QHash<quint32, quint32> cookies_;
    DataChunkList      pendingDataChunks_;
    QHash<QString, QVariant> properties_;

  Q_DISABLE_COPY(SwitchboardBridge)

}; // SwitchboardBridge
}

#endif

