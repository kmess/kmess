//
// TransportBridge class
//
// Authors:
//    Gregg Edghill (gregg.edghill at gmail.com)
//
// Copyright (C) 2006 - 2010, Gregg Edghill
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of this software.
//
// THIS LIBRARY IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR
// MODIFY IT UNDER THE TERMS OF THE GNU LESSER GENERAL PUBLIC
// LICENSE AS PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER
// VERSION 2 OF THE LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.
//

#ifndef CLASS_P2P__TLPTRANSPORTBRIDGE_H
#define CLASS_P2P__TLPTRANSPORTBRIDGE_H

#include <QtCore/QObject>
#include <QtCore/QHash>
#include <QtCore/QVariant>

namespace PtpTransport
{

/**
 * @brief Represents a transport layer bridge used to send and received data between transport layers.
 *
 * Transport bridges are used to connect the transport layers of the local and remote endpoints.
 * They can either provide reliable transmission of data or unreliable transmission of data based
 * on the underlying transport protocol each uses (e.g. TCP, TRUDP). Each transport bridge has a
 * maximum transmission unit (MTU) which defines the size, in bytes, of the largest packet payload
 * (data) that can be sent or received.
 *
 * @code
 *
 *  +------------------------+    logical connection    +------------------------+
 *  |    Transport  Layer    | <----------------------> |    Transport  Layer    |
 *  +------------------------+                          +------------------------+
 *  +------------------------+    logical connection    +------------------------+
 *  |    Transport Bridge    | <----------------------> |    Transport Bridge    |
 *  +------------------------+                          +------------------------+
 *  +--------------+   +-----+                          +--------------+   +-----+  --,
 *  | TRUDP | TURN |   | SWB |                          | TRUDP | TURN |   | SWB |    | transport protocols
 *  |------------------------|                          |------------------------|    | or switchboard
 *  |     UDP      |   TCP   |                          |     UDP      |   TCP   |    |
 *  +------------------------+                          +------------------------+  --'
 *  +------------------------+                          +------------------------+
 *  |           IP           | <======================> |           IP           |
 *  +------------------------+                          +------------------------+
 *
 * @endcode
 *
 * Transport bridges can be categorized into two main groups, indirect and direct.  Direct
 * transport bridges can establish a connection to the remote endpoint directly using various
 * transport protocols (e.g. TCP, Trivial RUDP). An indirect transport bridge does not establish a
 * connection to the remote endpoint directly but instead relays data using another endpoint.
 *
 * For example, the SBBridge relays data to be sent or received using the Switchboard (SWB)
 * server network. Similarly, the TURNv1 based transport bridge is used to relay data to
 * the remote endpoint, using a Traversal Using Relays around NAT (TURN) server, because
 * NAT impedes the establishment of a direct connection and more than likely the NAT device
 * (e.g. router) does not support UPnP (i.e. for port mapping) or UPnP is not enabled.
 *
 * Current supported transport bridges are: TCPv1, TRUDPv1, SBBridge, TURNv1
 *
 * @code
 *
 * Listening: true
 * Conn-Type: Direct-Connect
 * TCP-Conn-Type: Direct-Connect
 * IPv6-global: 2002:****:****::****:****
 * UPnPNat: false
 * Capabilities-Flags: 1
 * IPv4Internal-Addrs: 201.**.**.***
 * IPv4Internal-Port: 5**16
 * IPv6-Addrs: ****:****:****::****:**** ****:*:****:****:***:****:****:****
 * IPv6-Port: 5**17
 * Nat-Trav-Msg-Type: WLX-Nat-Trav-Msg-Direct-Connect-Resp
 * Bridge: TCPv1
 * Nonce: {56FA09A9-C949-449E-8EF1-************}
 * SessionID: 7***3649
 * SChannelState: 0
 *
 *
 * NetID: -472914077
 * Conn-Type: Direct-Connect
 * TCP-Conn-Type: Direct-Connect
 * UPnPNat: false
 * ICF: false
 * IPv6-global:
 * Capabilities-Flags: 1
 * Nat-Trav-Msg-Type: WLX-Nat-Trav-Msg-Direct-Connect-Req
 * Bridges: TRUDPv1 TCPv1 SBBridge TURNv1
 * Hashed-Nonce: {38A801EF-6AC9-9B38-6A25-3A9538303F93}
 * SessionID: 410031283
 * SChannelState: 0
 *
 * @endcode
 *
 * @author Gregg Edghill
 */
class TransportBridge : public QObject
{
  Q_OBJECT

  public:
    /** @brief Enumerates the possible states of a transport layer bridge. */
    enum TLP_TRANSPORT_BRIDGE_STATE
    {
      TLPTBS_CREATED        = 0,
      TLPTBS_CONNECTING     = 2,
      TLPTBS_CONNECTED      = 4,
      TLPTBS_DISCONNECTING  = 8,
      TLPTBS_DISCONNECTED   = 0x10,
      TLPTBS_FAULTED        = 0x80
    };

  public:
    /** @brief Creates a new instance of the class Transport Bridge. */
                       TransportBridge(QObject *pobject);
    /** @brief Frees resources and performs other cleanup operations. */
    virtual           ~TransportBridge();
    /** @brief When overriden in a derived class, gets the properties of the transport bridge. */
    virtual const QHash<QString, QVariant> & getProperties() const = 0;
    /** @brief When overriden in a derived class, returns a value that uniquely identifies the transport bridge. */
    virtual quint32    getId() const = 0;
    /** @brief Gets the state of the transport bridge. */
    TLP_TRANSPORT_BRIDGE_STATE getState() const;

    /** @brief Connects a transport bridge. */
    void               connect();
    /** @brief Disconnects a transport bridge. */
    void               disconnect();
    /** @brief Handles the occurrance of a fault.*/
    void               fault();
    /** @brief When overriden in a derived class, sends the specified byte array. */
    virtual void       send(const QByteArray& tlpheaderanddataifany, const quint32 id) = 0;

  protected:
    /** @brief When implemented in a derived class, connects the transport bridge to the remote endpoint. */
    virtual void       onConnect() = 0;
    /** @brief When implemented in a derived class, disconnects the transport bridge from the remote endpoint. */
    virtual void       onDisconnect() = 0;
    /** @brief When implemented in a derived class, handle the occurrance of a fault.*/
    virtual void       onFault();
    /** @brief Sets the state of the transport bridge. */
    void               setState(TLP_TRANSPORT_BRIDGE_STATE state);

  Q_SIGNALS:
    /** @brief Indicates that the transport bridge is connected. */
    void               connected();
    /** @brief Indicates that the transport bridge is connecting. */
    void               connecting();
    /** @brief Indicates that the transport bridge is disconnected. */
    void               disconnected();
    /** @brief Indicates that the transport bridge is disconnecting. */
    void               disconnecting();
    /** @brief Indicates that an error has occurred on the transport bridge. */
    void               error();
    /** @brief Indicates that a packet has been received on the transport bridge. */
    void               dataReceived(const QByteArray &datachunk);
    /** @brief Indicates that a packet has not been sent on the transport bridge. */
    void               dataSendFailed(const quint32 packetId);
    /** @brief Indicates that a packet has been sent on the transport bridge. */
    void               dataSent(const quint32 packetId);
    /** @brief Indicates that a data exchange time out has occurred on the transport bridge. */
    void               timeout();

  private:
    TLP_TRANSPORT_BRIDGE_STATE state_;

  Q_DISABLE_COPY(TransportBridge)

}; // TransportBridge
}

#endif

