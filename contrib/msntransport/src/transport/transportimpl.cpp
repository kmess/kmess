//
// TransportImpl class
//
// Summary: Represents a transport layer implementation that manages
//    and controls end to end packet transmission
//
// Authors:
//    Gregg Edghill (gregg.edghill at gmail.com)
//
// Copyright (C) 2006 - 2010, Gregg Edghill
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of this software.
//
// THIS LIBRARY IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR
// MODIFY IT UNDER THE TERMS OF THE GNU LESSER GENERAL PUBLIC
// LICENSE AS PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER
// VERSION 2 OF THE LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.
//

#include "transportimpl.h"
#include "msntransportconfig.h"
#include "helper.h"
#include "packetscheduler.h"
#include "packetserializer.h"
#include "bridges/directtransportbridge.h"
#include "bridges/switchboardbridge.h"
#include <QtCore/QBuffer>
#include <QtCore/QDataStream>
#include <QtCore/qendian.h>
#include <QtCore/QRegExp>
#include <QtCore/QTime>
#include <QtDebug>
using namespace PtpInternal;

#ifdef MSNTRANSPORT_DEBUG
  #define P2PDEBUG_TLP
#endif

#define DEFAULT_BRIDGE_ID 0
#define SBBRIDGE_ID 2

namespace PtpTransport
{

TransportImpl::TransportImpl(QObject *pobject) : QObject(pobject)
{
  currentBridgeId_ = DEFAULT_BRIDGE_ID;
  nextBridgeId_ = 64;
  nextPacketId_ = (qint32)(((double)(qrand() * 1.0/0x7FFFFFFF))*(0x186A0 - 0x3104)) + 0x3104;
  // Create the transport layer's packet scheduler
  packetScheduler_ = new PacketScheduler(this);
}

TransportImpl::~TransportImpl()
{
  foreach (PacketQueue *packetqueue, outgoingPackets_.values())
  {
    // Delete all packets left in the packet queue.
    qDeleteAll(*packetqueue);
    // Delete the packet queue.
    delete packetqueue;
  }
  // Clear the list.
  outgoingPackets_.clear();

  // Delete all the packets in the sent packets list.
  foreach (PacketQueue *packetqueue, sentPackets_.values())
  {
    // Delete all packets left in the packet queue.
    qDeleteAll(*packetqueue);
    // Delete the packet queue.
    delete packetqueue;
  }
  // Clear the list.
  sentPackets_.clear();

  // Delete all the datagram reconstruction buffers.
  qDeleteAll(databuffers_);
}

bool TransportImpl::isConnected() const
{
  return currentBridgeId_ != DEFAULT_BRIDGE_ID;
}

TransportBridgeList & TransportImpl::getTransportBridges()
{
  return transportBridges_;
}

PacketQueueList & TransportImpl::getOutgoingPacketQueues()
{
  return outgoingPackets_;
}

PacketQueue * TransportImpl::getSentPacketQueueBy(const quint32 bridgeid) const
{
  PacketQueue *queue = 0l;
  // Check whether there is a sent queue
  // for the bridge id specified
  if (sentPackets_.contains(bridgeid))
  {
    // If so, select the queue.
    queue = sentPackets_[bridgeid];
  }

  return queue;
}

quint32 TransportImpl::nextPacketSequenceNumber()
{
  return ++nextPacketId_;
}

void TransportImpl::addTransportBridge(TransportBridge *bridge)
{
  if (!bridge)
  {
#ifdef P2PDEBUG_TLP
    qDebug("%s: \'bridge\' is null -- baling out", Q_FUNC_INFO);
#endif
    return;
  }

  // Get the bridge id.
  const quint32 bridgeid = bridge->getId();
  // Check whether the bridge has already been addeed to the list
  if (!transportBridges_.contains(bridgeid))
  {
    // If not, add the bridge to the list of transport bridges.
    transportBridges_.insert(bridgeid, bridge);
#ifdef P2PDEBUG_TLP
    qDebug("%s: bridge %d", Q_FUNC_INFO, bridgeid);
#endif
    // Connect the signal/slot.
    QObject::connect(bridge, SIGNAL(connected()), this,
    SLOT(bridge_OnConnected()));
    QObject::connect(bridge, SIGNAL(dataReceived(const QByteArray&)), this,
    SLOT(bridge_OnDataReceived(const QByteArray&)));
    QObject::connect(bridge, SIGNAL(dataSent(const quint32)), this,
    SLOT(bridge_OnDataSent(const quint32)));
    QObject::connect(bridge, SIGNAL(dataSendFailed(const quint32)), this,
    SLOT(bridge_OnDataSendFailed(const quint32)));
    QObject::connect(bridge, SIGNAL(disconnected()), this,
    SLOT(bridge_OnDisconnected()));
    QObject::connect(bridge, SIGNAL(timeout()), this,
    SLOT(bridge_OnFault()));
    QObject::connect(bridge, SIGNAL(error()), this,
    SLOT(bridge_OnFault()));

    //
    // Create the resources for the transport bridge
    //

    // Assign an outgoing packets list to the bridge.
    outgoingPackets_.insert(bridgeid, new PacketQueue());
    // Assign a sent packets list to the bridge
    sentPackets_.insert(bridgeid, new PacketQueue());
  }
}

SwitchboardBridge* TransportImpl::getOrCreateSbBridge()
{
  SwitchboardBridge *bridge = 0;
  if (!transportBridges_.contains(SBBRIDGE_ID))
  {
    // Create the switchboard bridge.
    bridge = new SwitchboardBridge(SBBRIDGE_ID, this);
    // Add the switchboard bridge to the list.
    addTransportBridge(bridge);
  }
  else
  {
    bridge = dynamic_cast<SwitchboardBridge*>(transportBridges_[SBBRIDGE_ID]);
  }

  return bridge;
}

quint32 TransportImpl::nextBridgeId()
{
  return (++nextBridgeId_);
}

void TransportImpl::removeTransportBridge(const quint32 bridgeid)
{
  // Check whether the bridge associated with the specified
  // id is registered and is not the indirect bridge's id.
  if (bridgeid != SBBRIDGE_ID && transportBridges_.contains(bridgeid))
  {
    // If the id of the bridge does not match that of
    // the switchboard bridge, remove the bridge.
    transportBridges_.remove(bridgeid);
#ifdef P2PDEBUG_TLP
    qDebug("%s: bridge %d", Q_FUNC_INFO, bridgeid);
#endif
    //
    // Clean up the resources used by the transport bridge
    //

    // Delete the packet queue.
    delete outgoingPackets_[bridgeid];
    // Remove the packet queue assigned to the bridge.
    outgoingPackets_.remove(bridgeid);
    // Delete the packet queue.
    delete sentPackets_[bridgeid];
    // Remove the packet queue assigned to the bridge.
    sentPackets_.remove(bridgeid);
  }
}

quint32 TransportImpl::selectTransportBridgeToUse()
{
  quint32 bridgeid = DEFAULT_BRIDGE_ID;
  TransportBridgeList::ConstIterator item;
  for (item=transportBridges_.begin(); item != transportBridges_.end(); ++item)
  {
    // Check whether a direct transport bridge is available and is connected
    if (dynamic_cast<DirectTransportBridge*>(*item) && (*item)->getState() == TransportBridge::TLPTBS_CONNECTED)
    {
      // If so, select the transport bridge.
      bridgeid = item.key();
      break;
    }
  }

  // Check whether there were no direct transport
  // bridges available and whether the switchboard
  // bridge is still available.
  if (!bridgeid && transportBridges_.contains(SBBRIDGE_ID))
  {
    // If so, select the switchboard bridge.
    bridgeid = SBBRIDGE_ID;
  }

#ifdef P2PDEBUG_TLP
  qDebug("%s: bridge %d selected", Q_FUNC_INFO, bridgeid);
#endif
  return bridgeid;
}

void TransportImpl::movePacketsBetweenBridgeQueues(const quint32 oldqueueid, const quint32 newqueueid)
{
  if (oldqueueid == newqueueid)
  {
#ifdef P2PDEBUG_TLP
    qDebug("%s: old and new queue IDs are the same -- baling out", Q_FUNC_INFO);
#endif
    return;
  }

  // Gets the queue the packets are being moved from.
  PacketQueue *fromqueue = outgoingPackets_[oldqueueid];
  // Gets the queue the packets are being moved to.
  PacketQueue *toqueue   = outgoingPackets_[newqueueid];

  // Check whether the queue to move the packets from is not empty
  if (!fromqueue->isEmpty())
  {
    // If so, move all packets to the new queue.
#ifdef P2PDEBUG_TLP
    qDebug("%s: moving %d packet(s) from bridge %d's queue to bridge %d's queue",
           Q_FUNC_INFO, fromqueue->count(), oldqueueid, newqueueid);
#endif
    // Create a non-constant iterator for the queue
    QMutableListIterator<Packet*> item(*fromqueue);

    Packet *packet = 0l;
    while(item.hasNext() && item.peekNext() != 0l)
    {
      packet = item.next();
      // Remove the packet from the old queue.
      item.remove();
      // Add the packet to the new queue.
      toqueue->append(packet);
    }
  }
#ifdef P2PDEBUG_TLP
  else
  {
    qDebug("%s: bridge %d\'s queue is empty", Q_FUNC_INFO, oldqueueid);
  }
#endif
}

quint32 TransportImpl::send(const QByteArray& datagram, const quint32 dest, const quint32 correlationId, bool isulpdata)
{
  // Create a new packet to send.
  Packet *packet = createGenericPacket();

  // Get the header field of the packet.
  Packet::Header & h = packet->getHeader();
  // Set the destination of the packet.
  h.destination = dest;
  // Set the data window size of the packet.
  h.blobSize = datagram.size();
  // Write the bytes to the packet payload
  packet->getPayload()->write(datagram);
  // Set the payload size of the packet.
  h.payloadSize = h.blobSize;
  // Set the type of the packet.
  h.type = (quint32) (isulpdata ? Packet::TLPPT_NOTIFY_ULP_DATA
                                : Packet::TLPPT_DATA);
  // Set the correlation info of the packet.
  h.lprcvd = correlationId;

  // Queue the packet.
  queuePacket(packet, currentBridgeId_);

  return h.identifier;
}

void TransportImpl::send(QIODevice *stream, quint32 dest)
{
  // Check whether the supplied stream is valid.
  if (!stream)
  {
#ifdef P2PDEBUG_TLP
    qDebug("%s: \'stream\' is null -- bailing out", Q_FUNC_INFO);
#endif
    // If not, indicate that an error has occurred
    emit error(dest);

    return;
  }

  // Check whether the supplied stream
  // suppported random access.
  if (stream->isSequential())
  {
#ifdef P2PDEBUG_TLP
    qDebug("%s: \'stream\' is sequential -- bailing out", Q_FUNC_INFO);
#endif
    // If not, indicate that an error has occurred
    // because random access is required in the event
    // data is transmited out-of-order
    emit error(dest);

    return;
  }

  // Create a new packet to send.
  Packet *packet = createGenericPacket();

  // Get the header field of the packet.
  Packet::Header & h = packet->getHeader();
  // Set the destination of the packet.
  h.destination = dest;
  // Set the data window size of the packet.
  h.blobSize = stream->size();
  // Write the stream to the packet payload
  packet->setPayload(stream);
  // Set the payload size of the packet.
  h.payloadSize = h.blobSize;
  // Set the type of the packet.
  h.type = (quint32) (Packet::TLPPT_NOTIFY_ULP_DATA |
                      Packet::TLPPT_JUMBO);
  // Set the correlation info of the packet.
  h.lprcvd = rand() % 0xCF56;

  // Queue the packet.
  queuePacket(packet, currentBridgeId_);
}

void TransportImpl::sendBridgeAuthenticationKey(const QUuid& key, const quint32 bridgeid)
{
  // Create a new packet to send.
  Packet *packet = createGenericPacket();

  // Get the header field of the packet.
  Packet::Header & h = packet->getHeader();
  // Set the destination of the packet.
  h.destination = 0;
  // Set the type of the packet.
  h.type = (quint32)Packet::TLPPT_KEY;

  // Write the authentication key to the packet
  Helper::writeQUuidToPacket(packet, key);

#ifdef P2PDEBUG_TLP
  qDebug("%s: key %s", Q_FUNC_INFO, qPrintable(key.toString().toUpper()));
#endif
  // Queue the authentication key packet.
  queuePacket(packet, bridgeid, true);
}

//BEGIN Bridge Event Handling Functions

void TransportImpl::bridge_OnConnected()
{
  TransportBridge *bridge = qobject_cast<TransportBridge*>(sender());
  if (!bridge)
  {
#ifdef P2PDEBUG_TLP
    qDebug("%s: event from non-transport bridge -- ignoring.", Q_FUNC_INFO);
#endif
    return;
  }

  // Get the ID of the transport bridge.
  const quint32 bridgeid = bridge->getId();
#ifdef P2PDEBUG_TLP
  qDebug("%s: bridge (%d) is now connected.", Q_FUNC_INFO, bridgeid);
#endif

  // Determine using type reflection whether the bridge is
  // a switchboard bridge or a direct transport bridge.
  if (dynamic_cast<DirectTransportBridge*>(bridge))
  {
    // If the transport bridge is a direct bridge, we need to send the
    // authentication key to authenticate the use of the bridge.
    QUuid authKey;
    // Get the transport settings associated with this bridge
    QHash<QString, QVariant> & transportInfo = transportInfo_[bridgeid];
    // Check whether hashed nonce is supported
    if (transportInfo.contains("peerHashedNonce"))
    {
      // If so, get our local hashed nonce.
      authKey = QUuid(transportInfo["localHashedNonce"].toString());
#ifdef P2PDEBUG_TLP
      qDebug("%s: hashed nonce supported.", Q_FUNC_INFO);
#endif
    }
    else
    {
      // Otherwise, get the shared nonce.
      authKey = QUuid(transportInfo["sharedNonce"].toString());
    }

    // Send the bridge authentication key (nonce).
    sendBridgeAuthenticationKey(authKey, bridgeid);
  }
  else
  if (dynamic_cast<SwitchboardBridge*>(bridge))
  {
    if (currentBridgeId_ == DEFAULT_BRIDGE_ID)
    {
      currentBridgeId_ = bridge->getId();

      // Signal that the transport layer is connected.
      emit connected();
    }
  }
#ifdef P2PDEBUG_TLP
  else
  {
    qDebug("%s: Unknown bridge type", Q_FUNC_INFO);
  }
#endif
}

void TransportImpl::bridge_OnConnecting()
{
  // TODO Implementation
}

void TransportImpl::bridge_OnDataReceived(const QByteArray& datachunk)
{
  TransportBridge *bridge = qobject_cast<TransportBridge*>(sender());
  if (!bridge)
  {
#ifdef P2PDEBUG_TLP
    qDebug("%s: event from non-transport bridge -- ignoring.", Q_FUNC_INFO);
#endif
    return;
  }

  // Check whether the data received is valid
  if (datachunk.size() < (qint32)sizeof(Packet::Header))
  {
#ifdef P2PDEBUG_TLP
    qDebug("%s: Received TLP data whose size is less than %d bytes -- dropping.", Q_FUNC_INFO,
           sizeof(Packet::Header));
#endif
    return;
  }

  quint32 appId = 0;

  QByteArray headeranddataifany = datachunk;
  QDataStream stream(&headeranddataifany, QIODevice::ReadOnly);
  // Deserialize the packet from the supplied stream.
  Packet* packet = PacketSerializer::readPacket(&stream);
  // Determine using type reflection whether the packet
  // was received via the switchboard bridge.
  if (dynamic_cast<SwitchboardBridge*>(bridge))
  {
    // If so, deserialize the application id that is
    // appended after a serialized packet.
    stream.setByteOrder(QDataStream::BigEndian);
    stream >> appId;
  }

  // Check whether the packet received is valid
  // before dispatching to its intended destination
//  if (!packetinspector_.afterReceive(packet))
//  {
//    return;
//  }

  // Get the header field of the received packet.
  const Packet::Header & h = packet->getHeader();
#ifdef P2PDEBUG_TLP
  qDebug("%s: Received packet (%d) via bridge %d", Q_FUNC_INFO, h.identifier, bridge->getId());
  Packet::dumpPacketInfo(h, true);
#endif

  // TODO Check whether there is a notifier for the destination (session id)
  //      specified in the TLP pacaket header.  If not, send TLPPT_RST.

  // Get the packet type from the packet header.
  const Packet::TLP_PACKET_TYPE packetType = (Packet::TLP_PACKET_TYPE)h.type;

  if ((Packet::TLPPT_NON_ACK_CONTROL_MASK & packetType) != 0)
  {
    // We received a non ACK control packet.
    onNonAcknowledgeControlPacketReceived(packet);
  }
  else
  if (Packet::TLPPT_ACK == packetType)
  {
    // We received an ACK control packet.
#ifdef P2PDEBUG_TLP
    qDebug("%s: Packet %d is ACK to sent packet %d.  Notifying session", Q_FUNC_INFO,
           h.identifier, h.lprcvd);
#endif
    // Remove the now acknowledged packet from the
    // unacknowledged packet list.
    removePacketFromUnacknowledgedList(packet);

    // TODO Notify the signaling layer of the ACK
  }
  else
  if (Packet::TLPPT_KEY == packetType)
  {
    // We received a bridge authentication key packet.

    // Read the authentication key (nonce) from the packet.
    const QUuid key = Helper::readQUuidFromPacket(packet);
    // Process the bridge authentication key received.
    onBridgeAuthenticationKeyReceived(key, bridge->getId());
  }
  else
  if (Packet::TLPPT_SECURE_DATA == packetType)
  {
    // We received a secure channel message data packet.

    // TODO Implementation
  }
  else
  if (Packet::TLPPT_BACK_COMPAT_DATA == packetType || Packet::TLPPT_DATA == packetType)
  {
    // We received a data packet for a specific session

    // A datagram can be chunked depending on the MTU of the transport bridge the
    // datagram was sent on; therefore, we try to reconstruct the datagram if necessary
    // using the data from the packet received.

    const QByteArray datagram = reconstructEntireDatagramFrom(packet);

    if (!datagram.isNull())
    {
      // If the byte array is not null, all data has been received.
#ifdef P2PDEBUG_TLP
      qDebug("%s: Received entire datagram. Sending ACK.", Q_FUNC_INFO);
#endif
      // Send a TLPPT_ACK for the data received.
      sendAcknowledge(h.destination, h.identifier, h.lprcvd, h.blobSize);

      // TODO Notify the signaling layer

      // FIXME remove this once session notifier is coded.
      emit message(datagram, h.identifier, h.lprcvd);
    }
  }
  else
  if (Packet::TLPPT_NOTIFY_ULP == packetType || Packet::TLPPT_NOTIFY_ULP_DATA == packetType ||
     (Packet::TLPPT_NOTIFY_ULP_DATA | Packet::TLPPT_JUMBO) == packetType)
  {
    // We received a data packet for a specific session.
    // Since TLPPT_NOTIFY_ULP was specified the data received
    // should not be reconstructd at this layer.  The ULP will
    // handle all reconstruction of the data.

    // Determine whether this is the last packet to be received.
    bool isLastDatagramChunk = (h.payloadSize + h.offset == h.blobSize);

    // TODO Notify the session layer of the data

    if (isLastDatagramChunk)
    {
      // If this is the last chunk of the datagram,
      // send a TLPPT_ACK control packet.
      sendAcknowledge(h.destination, h.identifier, h.lprcvd, h.blobSize);
    }
  }
  else
  {
    // Otherwise, we have received an unknown packet.
#ifdef P2PDEBUG_TLP
    qDebug("%s: Received unknown packet %d. Sending error.", Q_FUNC_INFO, h.identifier);
#endif
    // Send a TLPPT_ERR control packet.
    sendError(h.destination, h.identifier, h.lprcvd, h.blobSize);
  }

  // Delete the received packet.
  delete packet;
  packet = 0l;
}

void TransportImpl::bridge_OnDataSent(const quint32 packetid)
{
  TransportBridge *bridge = qobject_cast<TransportBridge*>(sender());
  if (!bridge)
  {
#ifdef P2PDEBUG_TLP
    qDebug("%s: event from non-transport bridge -- ignoring.", Q_FUNC_INFO);
#endif
    return;
  }

  // Get the ID of the transport bridge
  const quint32 bridgeid = bridge->getId();
#ifdef P2PDEBUG_TLP
  qDebug("%s: bridge (%d) data send successfull.", Q_FUNC_INFO, bridgeid);
#endif

  Packet *packet = 0l;
  // Get the sent packets queue for the transport bridge.
  PacketQueue * sentpackets = sentPackets_[bridge->getId()];
  // Try to retrieve the sent packet from the queue.
  for(qint32 i=0; i < sentpackets->count(); ++i)
  {
    if (sentpackets->at(i) && sentpackets->at(i)->getHeader().identifier == packetid)
    {
      packet = sentpackets->at(i);
      break;
    }
  }

  if (!packet)
  {
#ifdef P2PDEBUG_TLP
    qDebug("%s: Could not find packet %d -- returning", Q_FUNC_INFO, packetid);
#endif
    return;
  }

  // Get the header field of the packet.
  const Packet::Header & h = packet->getHeader();
  // Get the packet type from the packet header.
  const Packet::TLP_PACKET_TYPE packetType = (Packet::TLP_PACKET_TYPE)h.type;

  // Check what type of packet was sent to
  // determine what to do next.
  if (Packet::TLPPT_ACK == packetType)
  {
#ifdef P2PDEBUG_TLP
    qDebug("%s: Removing sent packet %d ACK to received packet %d",
           Q_FUNC_INFO, h.identifier, h.lprcvd);
#endif
    // Delete the sent packet.
    Helper::deletePacketFromQueue(sentpackets, packet);
  }
  else
  if (Packet::TLPPT_BACK_COMPAT_DATA  == packetType ||
      Packet::TLPPT_DATA == packetType)
  {
#ifdef P2PDEBUG_TLP
    qDebug("%s: Adding sent packet %d to the list of unacknowledged message/data packets",
           Q_FUNC_INFO, h.identifier);
#endif
    // Remove the packet from the sent packets queue.
    sentpackets->removeOne(packet);
    // Add the packet to the unacknowledged packet list.
    addPacketToUnacknowledgedList(packet);
  }
  else
  if (Packet::TLPPT_NON_ACK_CONTROL_MASK & packetType)
  {
#ifdef P2PDEBUG_TLP
    qDebug("%s: Removing sent packet %d non ACK control packet",
           Q_FUNC_INFO, h.identifier);
#endif
    // Delete the sent packet.
    Helper::deletePacketFromQueue(sentpackets, packet);
  }
  else
  {
#ifdef P2PDEBUG_TLP
    qDebug("%s: Removing sent packet %d (chunk %lld..%lld)", Q_FUNC_INFO,
           h.identifier, h.offset, h.offset+h.payloadSize);
#endif
    // Delete the sent packet.
    Helper::deletePacketFromQueue(sentpackets, packet);
  }

  quint32 count = 0;
  PacketQueueList::ConstIterator item;
  // Determine whether there are more packets to schedule.
  for(item = outgoingPackets_.begin(); item != outgoingPackets_.end(); ++item)
  {
    count += item.value()->count();
  }

  if (count > 0)
  {
    // If there are more packets to send and the scheduler
    // has stopped, start scheduling again.
    if (!packetScheduler_->isRunning())
    {
      // Start the packet scheduler.
      packetScheduler_->start();
    }
  }
  else
  {
    // Otherwise, there are no more packets to schedule,
    // stop the packet scheduler.
    packetScheduler_->stop();
  }
}

void TransportImpl::bridge_OnDataSendFailed(const quint32 packetid)
{
  TransportBridge *bridge = qobject_cast<TransportBridge*>(sender());
  if (!bridge)
  {
#ifdef P2PDEBUG_TLP
    qDebug("%s: event from non-transport bridge -- ignoring.", Q_FUNC_INFO);
#endif
    return;
  }

  // Get the ID of the transport bridge
  const quint32 bridgeid = bridge->getId();
#ifdef P2PDEBUG_TLP
  qDebug("%s: bridge (%d) data send failed.", Q_FUNC_INFO, bridgeid);
#endif

  Packet *packet = 0l;
  // Get the sent packets queue for the transport bridge.
  PacketQueue * sentpackets = sentPackets_[bridge->getId()];
  // Try to retrieve the sent packet from the queue.
  for(qint32 i=0; i < sentpackets->count(); ++i)
  {
    if (sentpackets->at(i) && sentpackets->at(i)->getHeader().identifier == packetid)
    {
      packet = sentpackets->at(i);
      break;
    }
  }

  if (!packet)
  {
#ifdef P2PDEBUG_TLP
    qDebug("%s: Could not find packet %d -- returning", Q_FUNC_INFO, packetid);
#endif
    return;
  }

  // Get the header field of the packet.
  const Packet::Header & h = packet->getHeader();
  // Get the packet type from the packet header.
  const Packet::TLP_PACKET_TYPE packetType = (Packet::TLP_PACKET_TYPE)h.type;

#ifdef P2PDEBUG_TLP
  qDebug("%s: packet %d", Q_FUNC_INFO, packetid);
#endif

  // Check what type of packet failed to be
  // sent to determine what to do next.
  if (Packet::TLPPT_BACK_COMPAT_DATA == packetType ||
      Packet::TLPPT_DATA == packetType)
  {
    // Inform the signaling layer of the error.
    emit error(h.destination);
  }
}

void TransportImpl::bridge_OnDisconnected()
{
  TransportBridge *bridge = qobject_cast<TransportBridge*>(sender());
  if (!bridge)
  {
#ifdef P2PDEBUG_TLP
    qDebug("%s: event from non-transport bridge -- ignoring.", Q_FUNC_INFO);
#endif
    return;
  }

  // Get the ID of the transport bridge
  const quint32 bridgeid = bridge->getId();
#ifdef P2PDEBUG_TLP
  qDebug("%s: bridge (%d) is now disconnected.", Q_FUNC_INFO, bridgeid);
#endif

  // Determine using type reflection whether the bridge is
  // a switchboard bridge or a direct transport bridge.
  // Only clean up direct transport bridges.

  if (dynamic_cast<DirectTransportBridge*>(bridge))
  {
    // Disconnect the signal/slot.
    QObject::disconnect(bridge, 0, this, 0);
    // Set the default transport bridge to the
    // best transport bridge avaiable.
    currentBridgeId_ = selectTransportBridgeToUse();
    // Move the packets, if any, from the disconnected
    // bridge's queue to the next best bridge found.
    movePacketsBetweenBridgeQueues(bridgeid, currentBridgeId_);
    // Remove the transport bridge from the list.
    removeTransportBridge(bridgeid);
    // Delete the transport bridge.
    bridge->deleteLater();
    bridge = 0l;
  }
}

void TransportImpl::bridge_OnDisconnecting()
{
  // TODO Implementation
}

void TransportImpl::bridge_OnFault()
{
  TransportBridge *bridge = qobject_cast<TransportBridge*>(sender());
  if (bridge)
  {
#ifdef P2PDEBUG_TLP
    qDebug("%s: disconnecting bridge %d", Q_FUNC_INFO, bridge->getId());
#endif
    // Disconnect the bridge.
    bridge->disconnect();
  }
}

//END

//BEGIN Packet Acknowledge Handling Functions

void TransportImpl::addPacketToUnacknowledgedList(Packet *packet)
{
  if (!packet)
  {
#ifdef P2PDEBUG_TLP
    qDebug("%s: \'packet\' is null -- baling out", Q_FUNC_INFO);
#endif
    return;
  }

  // Get the header field of the packet.
  const Packet::Header & h = packet->getHeader();
#ifdef P2PDEBUG_TLP
  qDebug("%s: packet %d", Q_FUNC_INFO, h.identifier);
#endif

  // FIXME For now, just delete the packet.
  delete packet;

  // Create the wait item and set the timespan for 30 seconds.
//  WaitTimerItem waititem(packet, QTime::currentTime(), 30000);
  // Connect the timer signal/slot
//  QObject::connect(&waititem, SIGNAL(timeout()), this, SLOT(waititem_OnAcknowledgeTimespanExpired()));
  // Add the wait item to the queue.
//  waititemqueue_.addItem(waititem);
  // Start waiting for the ackknowledge for the packet sent
//  waititem.start();
}

void TransportImpl::removePacketFromUnacknowledgedList(Packet *packet)
{
  if (!packet)
  {
#ifdef P2PDEBUG_TLP
    qDebug("%s: \'packet\' is null -- baling out", Q_FUNC_INFO);
#endif
    return;
  }

  // Get the header field of the packet.
  const Packet::Header & h = packet->getHeader();
#ifdef P2PDEBUG_TLP
  qDebug("%s: packet %d", Q_FUNC_INFO, h.lprcvd);
#endif

//  foreach (WaitTimerItem waititem, waititemqueue_)
//  {
//      if (packet->getHeader().lprcvd == h.identifier)
//      {
          // If so, remove the unacknowledged packet
//          waititem.stop();
//            break;
//      }
//  }
}

void TransportImpl::waititem_OnAcknowledgeTimespanExpired()
{
  // Determine whether there are pending packets to be acknowledged.
//  if (waititemqueue_.isEmpty())
//  {
      // If not, wait for next timeout interval
//    return;
//  }

//  foreach(WaitTimerItem waititem, waititemqueue_)
//  {
//      if (waititem.isExpired())
//      {
        // If so, handle the unacknowledged packet
//      }
//  }
}

//END

QBuffer * TransportImpl::getDatagramBufferBy(const quint32 id)
{
  QBuffer *buffer=0l;
  // Determine if the received packet is the first in a series
  // of packets that contain fragmented datagram data.
  if (!databuffers_.contains(id))
  {
    // If the buffer does not already exist, create a new buffer.
    buffer = new QBuffer();
    // Open the memory buffer for writing.
    buffer->open(QIODevice::WriteOnly);
    // Add the buffer to the buffer list.
    databuffers_.insert(id, buffer);
  }
  else
  {
    // Otherwise, retrieve the existing buffer.
    buffer = databuffers_[id];
  }

  return buffer;
}

QByteArray TransportImpl::reconstructEntireDatagramFrom(Packet *packet)
{
  QByteArray datagram;
  // Get the header field of the packet.
  const Packet::Header & h = packet->getHeader();
  // Check whether the entire datagram has been received.
  if (h.payloadSize < h.blobSize)
  {
    // If not, try to reconstruct the datagram
#ifdef P2PDEBUG_TLP
    qDebug("%s: Received datagram chunk [%lld..%lld], packet (%d)", Q_FUNC_INFO,
           h.offset, h.offset+h.payloadSize, h.identifier);
#endif
    // Get the raw data buffer used to reconstruct the datagram.
    QBuffer *databuffer = getDatagramBufferBy(h.identifier);
    // Get the data offset of the data buffer.
    const quint32 dataoffset = databuffer->pos();
    // Check whether the data chunk received is out of order
    if ((quint32)h.offset != dataoffset)
    {
#ifdef P2PDEBUG_TLP
      qDebug("%s: out-of-order packet (session %d, packet %d, offset %lld) expected offset %d", Q_FUNC_INFO,
             h.destination, h.identifier, h.offset, dataoffset);
#endif
      // If so, send a TLPPT_NAK control packet
      sendNonAcknowledge(h.destination, h.identifier, h.lprcvd, h.blobSize);
    }
    else
    {
      // Seek to the offset in the buffer where the
      // received data chunk will be written.
      databuffer->seek(h.offset);
      // Read the data from the packet payload
      QByteArray rawdata = Helper::readDataFromPacket(packet);
      // Write the data into the buffer.
      databuffer->write(rawdata.data(), rawdata.size());

      // Determine if the received packet is the last in a series
      // of packets that contain chunked data.
      if (h.payloadSize + h.offset == h.blobSize)
      {
        // Remove the buffer from the list.
        databuffers_.remove(h.identifier);
        // We assume that the data has been reconstructd.
        datagram = databuffer->buffer();

        // Dispose of the buffer.
        delete databuffer;
        databuffer = 0l;
      }
    }
  }
  else
  {
    // Otherwise, the datagram is not chunked and there
    // is no need to try to reconstruct it.
    datagram = Helper::readDataFromPacket(packet);
  }

  return datagram;
}

//BEGIN Transport Packet Scheduling and Creation Functions

Packet * TransportImpl::createGenericPacket()
{
  // Create a new packet to send.
  Packet *packet = new Packet();
  // Get the header field of the packet.
  Packet::Header & h = packet->getHeader();
  // Set the sequence number of the new packet.
  h.identifier = nextPacketSequenceNumber();

  return packet;
}

void TransportImpl::queuePacket(Packet *packet, const quint32 bridgeId, bool prepend)
{
  if (!packet)
  {
#ifdef P2PDEBUG_TLP
    qDebug("%s: \'packet\' is null -- baling out", Q_FUNC_INFO);
#endif
    return;
  }

  // Get the header field of the packet.
  const Packet::Header & h = packet->getHeader();
  //Check whether the packet is valid before sending
//  if (!packetinspector_.beforeSend(packet))
//  {
  // If not, inform the signaling layer of the error.
//  emit error(h.destination);
  // Delete the packet
//  delete packet;
//  packet = 0l;
//    return;
//  }


  // Determine if there is a registered transport
  // bridge to send the supplied packet.
  if (transportBridges_.contains(bridgeId))
  {
    // If so, get the packet queue assigned to the bridge.
    PacketQueue *queue = outgoingPackets_[bridgeId];
    // Add the packet to the bridge's assigned send queue.
    prepend ? queue->prepend(packet) : queue->append(packet);

#ifdef P2PDEBUG_TLP
    qDebug("%s: Queued packet %d on bridge %d (size %d)", Q_FUNC_INFO,
           h.identifier, bridgeId, (quint32)h.blobSize);
#endif
    if (!packetScheduler_->isRunning())
    {
      // If the packet scheduler is not running,
      // start the scheduler.
      packetScheduler_->start();
    }
  }
  else
  {
    // Otherwise, indicate that there was an error.
#ifdef P2PDEBUG_TLP
    qDebug("%s: Could not queue packet %d for session %d on bridge (%d)'s packet list -- bridge not found.",
           Q_FUNC_INFO, h.identifier, h.destination, bridgeId);
#endif
    // Inform the signaling layer of the error.
    emit error(h.destination);
    // Delete the packet
    delete packet;
    packet = 0l;
  }
}

void TransportImpl::sendPacket(Packet* packet, const quint32 bridgeId)
{
  if (!packet)
  {
#ifdef P2PDEBUG_TLP
    qDebug("%s: \'packet\' is null -- baling out", Q_FUNC_INFO);
#endif
    return;
  }

  // Get the header field of the packet.
  const Packet::Header & h = packet->getHeader();
#ifdef P2PDEBUG_TLP
  qDebug("%s: Sending packet %d via bridge %d", Q_FUNC_INFO, h.identifier, bridgeId);
  Packet::dumpPacketInfo(h, false);
#endif
  // Get the bridge the packet is going to be sent via.
  TransportBridge *bridge = transportBridges_[bridgeId];

  QByteArray datachunk;
  QDataStream stream(&datachunk, QIODevice::WriteOnly);
  // Serialize the packet to the given stream.
  PacketSerializer::writePacket(&stream, packet);

  // Determine using type reflection whether the
  // specified bridge is a switchboard bridge.
  if (dynamic_cast<SwitchboardBridge*>(bridge))
  {
    // If so, write the application id to the stream.
    quint32 appId = 0;
    // TODO Determine app id based on the session notifier type.

    // Serialize the application id to the datachunk.
    stream.setByteOrder(QDataStream::BigEndian);
    stream << appId;
  }

  // Add the packet to the list of sent packets.
  sentPackets_[bridgeId]->append(packet);

  // Send the datachunk via the selected transport bridge.
  bridge->send(datachunk, h.identifier);
}

//END

void TransportImpl::stopSendingDataTo(quint32 destination)
{
#ifdef P2PDEBUG_TLP
  qDebug("%s: enter", Q_FUNC_INFO);
#endif
  QHash<quint32, TransportBridge*>::ConstIterator item;
  for (item = transportBridges_.begin(); item != transportBridges_.end(); ++item)
  {
    // Get the packet queue assigned to the current transport bridge.
    PacketQueue *sendqueue = outgoingPackets_[item.key()];
    // Try to retrieve the packet from the queue.
    for(qint32 i=0; i < sendqueue->count(); ++i)
    {
      Packet *packet = sendqueue->at(i);
      if (packet && packet->getHeader().destination == destination)
      {
#ifdef P2PDEBUG_TLP
        qDebug("%s: removing packet %d from send queue, session %d",
               Q_FUNC_INFO, packet->getHeader().identifier, destination);
#endif
        // Delete the packet.
        Helper::deletePacketFromQueue(sendqueue, packet);
      }
    }
  }
#ifdef P2PDEBUG_TLP
  qDebug("%s: leave", Q_FUNC_INFO);
#endif
}

void TransportImpl::onBridgeAuthenticationKeyReceived(const QUuid& key, const quint32 bridgeid)
{
  // Check whether the authentication key (nonce) was received
  // via the default (switchboard) bridge or via a direct bridge.
  if (SBBRIDGE_ID == bridgeid)
  {
    // If the nonce was received via the switchboard bridge,
    // do nothing as the switchboard bridge does not use
    // authentication.
#ifdef P2PDEBUG_TLP
    qDebug("%s: Got key %s via indirect bridge. Must have not been "
           "sent via direct bridge -- ignoring.", Q_FUNC_INFO,
           qPrintable(key.toString().toUpper()));
#endif

    return;
  }

  QUuid expectedKey;
  QUuid sentKey;
  // Gets the transport information for the bridge.
  QHash<QString, QVariant> & transportInfo = transportInfo_[bridgeid];

  bool supportsHashedNonce = transportInfo.contains("peerHashedNonce");
  // Check whether hashing of the nonce is supported
  if (supportsHashedNonce)
  {
    // If so, the bridge authentication scheme supports hashed nonce keys.
#ifdef P2PDEBUG_TLP
    qDebug("%s: hashed nonce supported.", Q_FUNC_INFO);
#endif
    // Get the expected hashed authentication key.
    expectedKey = QUuid(transportInfo["peerHashedNonce"].toString());
    // Set the authentication key sent by the peer.
    sentKey = Helper::sha1HashQUuid(key);
  }
  else
  {
    // Otherwise, get the shared authentication key.
    expectedKey = QUuid(transportInfo["sharedNonce"].toString());
    // Set the authentication key sent by the peer.
    sentKey = key;
  }
#ifdef P2PDEBUG_TLP
  qDebug("%s: Received nonce %s", Q_FUNC_INFO, qPrintable(sentKey.toString().toUpper()));
#endif

  // Check whether the key received is the same as that
  // assigned to the bridge
  if (sentKey == expectedKey)
  {
    // If so, we have successfully authenticated
    // the transport bridge.
#ifdef P2PDEBUG_TLP
    qDebug("%s: Bridge %d is now authenticated.", Q_FUNC_INFO, bridgeid);
#endif
    // Move the current bridge's packet queue to the newly
    // authenticated bridge's packet queue.
    movePacketsBetweenBridgeQueues(currentBridgeId_, bridgeid);
    // Set the current bridge to the newly authenticated bridge
    currentBridgeId_ = bridgeid;
  }
  else
  {
    // Otherwise, the authentication failed.
#ifdef P2PDEBUG_TLP
    qDebug("%s: Got nonce %s expected %s.  Disconnecting bridge %d", Q_FUNC_INFO,
           qPrintable(sentKey.toString().toUpper()),
           qPrintable(expectedKey.toString().toUpper()),
           bridgeid);
#endif
    // Get the transport bridge that failed authentication.
    TransportBridge *bridge = transportBridges_[bridgeid];
    // Disconnect the transport bridge.
    bridge->disconnect();
  }
}

void TransportImpl::onNonAcknowledgeControlPacketReceived(Packet *packet)
{
  if (!packet)
  {
#ifdef P2PDEBUG_TLP
    qDebug("%s: \'packet\' is null -- baling out", Q_FUNC_INFO);
#endif
    return;
  }

  // Get the header field of the packet.
  const Packet::Header & h = packet->getHeader();
  // Get the packet type from the packet header.
  const Packet::TLP_PACKET_TYPE packetType = (Packet::TLP_PACKET_TYPE)h.type;

  // Check what type of packet was sent to
  // determine what to do next.
  if (Packet::TLPPT_CAN == packetType)
  {
#ifdef P2PDEBUG_TLP
    qDebug("%s: got CAN (session %d) -- canceling data receive", Q_FUNC_INFO, h.destination);
#endif
    // Indicate that an error has occurred
    emit error(h.destination);
  }
  else
  if (Packet::TLPPT_NAK == packetType)
  {
#ifdef P2PDEBUG_TLP
    qDebug("%s: got NAK (packet %d, session %d) -- requesting offset %lld",
           Q_FUNC_INFO, h.lpsent, h.destination, h.lpsize);
#endif

    QHash<quint32, TransportBridge*>::ConstIterator item;
    for (item = transportBridges_.begin(); item != transportBridges_.end(); ++item)
    {
      // Get the packet queue assigned to the current transport bridge.
      PacketQueue *sendqueue = outgoingPackets_[item.key()];
      // Try to retrieve the packet from the queue.
      for(qint32 i=0; i < sendqueue->count(); ++i)
      {
        if (sendqueue->at(i) && sendqueue->at(i)->getHeader().identifier == h.lpsent)
        {
          Packet::Header & h0 = sendqueue->at(i)->getHeader();
          // Set the data offset for the packet
          h0.offset = h.lpsize;

          item = transportBridges_.end();
          break;
        }
      }
    }
  }
  else
  if (Packet::TLPPT_ERR == packetType)
  {
#ifdef P2PDEBUG_TLP
    qDebug("%s: got ERR (packet %d, session %d) -- canceling data send", Q_FUNC_INFO,
           h.lprcvd, h.destination);
#endif
    // Stop all sending of data to this destination.
    stopSendingDataTo(h.destination);
    // Notify the signaling layer that there
    // has been an error.
    emit error(h.destination);
  }
  else
  if (Packet::TLPPT_RAK == packetType)
  {
#ifdef P2PDEBUG_TLP
    qDebug("%s: got RAK (packet %d, session %d)", Q_FUNC_INFO,
           h.lpsent, h.destination);
#endif

    // TODO Implementation
  }
  else
  if (Packet::TLPPT_RST == packetType)
  {
#ifdef P2PDEBUG_TLP
    qDebug("%s: got RST (session %d)", Q_FUNC_INFO,
           h.destination);
#endif
    // Stop all sending of data to this destination.
    stopSendingDataTo(h.destination);
    // Stop all receiving of data from this destination.
//    stopReceivingDataFrom(h.destination);
    // Notify the signaling layer that there
    // has been an error.
    emit error(h.destination);
  }
}

void TransportImpl::sendAcknowledge(const quint32 dest, const quint32 lprcvd, const quint32 lpsent, const quint64 lpsize)
{
#ifdef P2PDEBUG_TLP
  qDebug("%s: enter", Q_FUNC_INFO);
#endif
  // Send a transport ACK control packet.
  sendControlPacket(Packet::TLPPT_ACK, dest, lprcvd, lpsent, lpsize);
#ifdef P2PDEBUG_TLP
  qDebug("%s: leave", Q_FUNC_INFO);
#endif
}

void TransportImpl::sendCancelTransfer(const quint32 dest, const quint32 lprcvd, const quint32 lpsent, const quint64 lpsize)
{
#ifdef P2PDEBUG_TLP
  qDebug("%s: enter", Q_FUNC_INFO);
#endif
  // Send a CANcel transfer control packet.
  sendControlPacket(Packet::TLPPT_CAN, dest, lprcvd, lpsent, lpsize);
#ifdef P2PDEBUG_TLP
  qDebug("%s: leave", Q_FUNC_INFO);
#endif
}

void TransportImpl::sendControlPacket(const Packet::TLP_PACKET_TYPE type, const quint32 dest, const quint32 lprcvd, const quint32 lpsent, const quint64 lpsize)
{
  // Create a new packet to send
  Packet *packet = createGenericPacket();

  // Get the header field of the packet.
  Packet::Header & h = packet->getHeader();
  // Set the destination of the packet.
  h.destination = dest;
  // Set the type of the packet.
  h.type = (quint32)type;
  // Set the correlation info of the packet.
  h.lprcvd = lprcvd;
  h.lpsent = lpsent;
  h.lpsize = lpsize;

  // Queue the control packet.
  queuePacket(packet, currentBridgeId_);
}

void TransportImpl::sendError(const quint32 dest, const quint32 lprcvd, const quint32 lpsent, const quint64 lpsize)
{
#ifdef P2PDEBUG_TLP
  qDebug("%s: enter", Q_FUNC_INFO);
#endif
  // Send a transport ERR control packet.
  sendControlPacket(Packet::TLPPT_ERR, dest, lprcvd, lpsent, lpsize);
#ifdef P2PDEBUG_TLP
  qDebug("%s: leave", Q_FUNC_INFO);
#endif
}

void TransportImpl::sendNonAcknowledge(const quint32 dest, const quint32 lprcvd, const quint32 lpsent, const quint64 lpsize)
{
#ifdef P2PDEBUG_TLP
  qDebug("%s: enter", Q_FUNC_INFO);
#endif
  // Send a transport NAK control packet.
  sendControlPacket(Packet::TLPPT_NAK, dest, lprcvd, lpsent, lpsize);
#ifdef P2PDEBUG_TLP
  qDebug("%s: leave", Q_FUNC_INFO);
#endif
}

void TransportImpl::sendRequestForAcknowledge(const quint32 dest, const quint32 lprcvd, const quint32 lpsent, const quint64 lpsize)
{
#ifdef P2PDEBUG_TLP
  qDebug("%s: enter", Q_FUNC_INFO);
#endif
  // Send a transport RAK control packet.
  sendControlPacket(Packet::TLPPT_RAK, dest, lprcvd, lpsent, lpsize);
#ifdef P2PDEBUG_TLP
  qDebug("%s: leave", Q_FUNC_INFO);
#endif
}

void TransportImpl::sendReset(const quint32 dest, const quint32 lprcvd, const quint32 lpsent, const quint64 lpsize)
{
#ifdef P2PDEBUG_TLP
  qDebug("%s: enter", Q_FUNC_INFO);
#endif
  // Send a transport RST control packet.
  sendControlPacket(Packet::TLPPT_RST, dest, lprcvd, lpsent, lpsize);
#ifdef P2PDEBUG_TLP
  qDebug("%s: leave", Q_FUNC_INFO);
#endif
}

}
