//
// Packet class
//
// Authors:
//    Gregg Edghill (gregg.edghill at gmail.com)
//
// Copyright (C) 2006 - 2010, Gregg Edghill
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of this software.
//
// THIS LIBRARY IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR
// MODIFY IT UNDER THE TERMS OF THE GNU LESSER GENERAL PUBLIC
// LICENSE AS PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER
// VERSION 2 OF THE LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.
//

#ifndef CLASS_P2P__TLPPACKET_H
#define CLASS_P2P__TLPPACKET_H

#include <QtGlobal>
class QIODevice;

namespace PtpTransport
{

/**
 * @brief Represents the unit of communication between transports.
 *
 * A peer to peer v1 transport layer packet consists of a 48-byte header formatted
 * in network byte order (big endian) followed by a payload (data) of variable size.
 *
 * Packets sent or received by the transport layer can be classified into two (2)
 * groups (control packets and data packets)
 *
 * 1. Control packets are used to manage the end-to-end exchange of data
 *    providing error control, acknowledgement and flow control. Control packets
 *    MUST have the data offset, window size and payload size fields set to zero(0),
 *    as no data follows the header.
 * 2. Data packets are used to transport the session layer's data.
 *
 * @code
 *                                                         1
 * 0                           8                           6
 * |   DWORD 0   |   DWORD 1   |   DWORD 2   |   DWORD 3   |
 * +-------------------------------------------------------+
 * | destination | identifier  |        data offset        |
 * |-------------------------------------------------------|
 * |         blob size         | payloadsize | packet type |
 * |-------------------------------------------------------|
 * | lpacketrcvd | lpacketsent |        lpacketsize        |
 * |-------------------------------------------------------|
 * ~                    payload (optional)                 ~
 * +-------------------------------------------------------+
 *
 * @endcode
 *
 *  <tt> destination (4 bytes): </tt> Identifies the receiving upper layer session.
 *  <tt> identifier (4 bytes): </tt> A message identifier used for acknowledgement
 *         tracking.
 *  <tt> data offset (8 bytes): </tt> Byte offset of the data fragment in the original
 *         data.
 *  <tt> blob size (8 bytes): </tt> The size, in bytes, of the upper layer data
 *         before being fragmented (if necessary) in chunks. That is, it specifies the number
 *         of bytes the receiver should expect for the entire blob (i.e. chunk 0 to chunk n).
 *  <tt> payload size (4 bytes): </tt> The size, in bytes, of the attached payload.
 *  <tt> packet type (4 bytes): </tt> The type of data following the header.
 *         MUST be one of the following:
 *
 *         Data         -----------------------------------------------------
 *         - 0x00        UPPER LAYER DATA (backward compatible WLM <14)
 *         - 0x1000000   UPPER LAYER DATA (WLM 14+)
 *         Control      -----------------------------------------------------
 *         - 0x01        NON ACKNOWLEDGE (NAK)
 *         - 0x02        ACKNOWLEDGE  (ACK)
 *         - 0x04        REQUEST FOR ACKNOWLEDGE (RAK)
 *         - 0x08        TRANSPORT RESET (RST), transport layer is fubar
 *         - 0x10        JUMBO
 *         - 0x20        NOTIFY ULP
 *         - 0x40        CANCEL RECEIVE  (CAN), sending node to receiving node
 *         - 0x80        CANCEL SEND     (ERR), receiving node to sending node
 *         - 0x100       BRIDGE AUTHENTICATION NONCE (KEY)
 *         - 0x200       SECURE CHANNEL DATA
 *
 *  <tt> lpacketrcvd (4 bytes): </tt> Has a dual meaning:
 *         1. If the type field is not set to TLPPT_KEY, then this field contains
 *            the value of the ID field in the header of the last packet received by
 *            the sender.
 *         2. If the type field is set to TLPPT_KEY, then this field contains
 *            bytes 0 - 3 of the nonce.
 *  <tt> lpacketsent (4 bytes): </tt> Has a dual meaning:
 *         1. If the type field is not set to TLPPT_KEY, then this field contains
 *            is the value of the ID field in the header of the last packet sent by
 *            the sender.
 *         2. If the type field is set to TLPPT_KEY, then this field contains
 *            bytes 4 - 7 of the nonce.
 *  <tt> lpacketsize (8 bytes): </tt> Has a dual meaning:
 *         1. If the type field is not set to TLPPT_KEY, then this field contains
 *            the value of the payload size field in the header of the last packet
 *            received by the sender.
 *         2. If the type field is set to TLPPT_KEY, then this field contains
 *            bytes 8 - 15 of the nonce.
 *  <tt> payload (variable): </tt> The portion of the data starting at a byte offset
 *         specified in the data offset field of the header if fragmented or the
 *         entire (upper layer) data. There is a limit on the size of a packet's
 *         payload (data) based on the transport bridge the packet will be sent via.
 *         If the data is larger than the bridge's MTU then the data will be
 *         fragmented.
 *
 * @code
 *                                                         1
 * 0                           8                           6
 * |   DWORD 0   |   DWORD 1   |   DWORD 2   |   DWORD 3   |
 * +-------------------------------------------------------+
 * | destination | identifier  |        data offset        |
 * |-------------------------------------------------------|
 * |         blob size         | payloadsize | packet type |
 * |-------------------------------------------------------|
 * |                         nonce                         |
 * +-------------------------------------------------------+
 *
 * @endcode
 *  <tt> Nonce (16 bytes): </tt> A nonce value that the transport layer uses to
 *         authenticate a connecting direct, transport layer bridge.
 *
 * @author Gregg Edghill
 */
class Packet
{
  public:
    /** @brief Enumerates the types of packets that can be sent via a transport. */
    enum TLP_PACKET_TYPE
    {
      // Represents the data type for a blob chunk.  Data should be buffered
      // until all data (entire blob) is received before delivering to the
      // upper layer protocol. Only deliver blob chunk data to ULP as it is
      // received if TLPPT_NOTIFY_ULP or TLPPT_JUMBO is also specified
      TLPPT_BACK_COMPAT_DATA       = 0x00,
      TLPPT_DATA                   = 0x1000000,
      // Used to indicate that the receiving node did not receive
      // the expected data packet in the sequence, and
      // therefore data was received out of order.
      TLPPT_NAK                    = 0x01,
      // Used to indicate that the receiving node successfully
      // received the entire blob data packet.
      TLPPT_ACK                    = 0x02,
      // Used to indicate that no ACK was received by the sending
      // node for transmitted message data; thus, the sending
      // node is requesting that the receiving node indicate
      // whether it received the message data successfully.
      TLPPT_RAK                    = 0x04,
      // Used to ungracefully end all data transfers.
      // Sometime really bad must have happened.
      TLPPT_RST                    = 0x08,
      // Used to indicate that the datagram this packet is
      // a part of is very large.
      TLPPT_JUMBO                  = 0x10,
      // Used to indicate that the ULP should be notified of the
      // data received.  Data should be delivered upon arrival
      // and not buffered at the TLP until all data is received.
      TLPPT_NOTIFY_ULP             = 0x20,
      // Used to indicate that the sending node has cancelled
      // the data transfer.
      TLPPT_CAN                    = 0x40,
      // Used to indicate that an error has occurred at the
      // receiver while receiving the data transfer.
      TLPPT_ERR                    = 0x80,
      // Indicates a direct connection authentication key.
      TLPPT_KEY                    = 0x100,
      // Represents the data type for a secure channel message.
      TLPPT_SECURE_DATA            = 0x200,
      // Represents a non acknowledge control packet
      TLPPT_NON_ACK_CONTROL_MASK   = TLPPT_NAK | TLPPT_RAK |
                                     TLPPT_RST | TLPPT_CAN |
                                     TLPPT_ERR ,
      // Represents data for the upper layer protocol
      TLPPT_NOTIFY_ULP_DATA        = TLPPT_DATA | TLPPT_NOTIFY_ULP
    };

    /** @brief Defines the structure of a packet header. */
    struct Header
    {
      // Indicates the identifier of the destination.
      quint32            destination;
      // Identifies a packet or a series of packet fragments
      quint32            identifier;
      // Indicates the data offset, in bytes, from the beginning of the datagram
      quint64            offset;
      // Indicates the expected datagram size.
      quint64            blobSize;
      // Indicates the size of the payload.
      quint32            payloadSize;
      // Indicates the packet type.
      quint32            type;
      // Indicates the identifier of the last
      // packet received by the sender.
      quint32            lprcvd;
      // Indicates the identifier of the last
      // packet sent by the sender.
      quint32            lpsent;
      // Indicates the data size of the last
      // packet received by the sender.
      quint64            lpsize;
    };

  public:
    /** @brief Creates a new instance of the class Packet. */
    Packet();
    /** @brief Frees resources and performs other cleanup operations. */
    ~Packet();

    /** @brief Gets the packer header. */
    const Header &         getHeader() const;
    /** @brief Gets the packer header. */
    Header &               getHeader();
    /** @brief Gets the packet payload. */
    QIODevice *            getPayload() const;
    /** @brief Sets the packet payload. */
    void                   setPayload(QIODevice *payload);
    /** @brief Gets a value that indicates the size of the packet (header + data). */
    quint32                getSize() const;
    /** @brief Prints a string representation of the packet's header. */
    static void            dumpPacketInfo(const Packet::Header& header, bool inbound);

  private:
    Packet::Header         header_;
    bool                   ownsPayload_;
    QIODevice *            payload_;

}; // Packet
}

#endif

