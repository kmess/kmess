//
// PacketScheduler class
//
// Summary: Represents a transport layer packet scheduler which manages
//    the scheduling of packets and controls overall packet transmission
//    based on priority and packet type (packet shaping).
//
// Authors:
//    Gregg Edghill (gregg.edghill at gmail.com)
//
// Copyright (C) 2006 - 2010, Gregg Edghill
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of this software.
//
// THIS LIBRARY IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR
// MODIFY IT UNDER THE TERMS OF THE GNU LESSER GENERAL PUBLIC
// LICENSE AS PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER
// VERSION 2 OF THE LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.
//

#include "packetscheduler.h"
#include "msntransportconfig.h"
#include "packet.h"
#include "helper.h"
#include "transportimpl.h"
#include "bridges/transportbridge.h"
#include <QtCore/QTimer>
using namespace PtpInternal;

#ifdef MSNTRANSPORT_DEBUG
  #define P2PDEBUG_TLP_SCHEDULER
#endif

namespace PtpTransport
{

PacketScheduler::PacketScheduler(TransportImpl *transport) : QObject(transport), isRunning_(false)
{
  transport_ = transport;

  // Define the packet classes which are used to
  // determine the type of packet to send first.
  createPacketClasses();

  // Create a timer which is used to schedule
  // packets to send
  schedulingtimer_ = new QTimer(this);
  // Connect the timer timeout signal to the
  // scheduler's signal slot.
  QObject::connect(schedulingtimer_, SIGNAL(timeout()), this, SLOT(timer_OnSchedulePacketsToSend()));
  // Set the scheduling interval to 3 seconds
  schedulingtimer_->setInterval(3000);
}

PacketScheduler::~PacketScheduler()
{
}

bool PacketScheduler::isRunning() const
{
  return isRunning_;
}

void PacketScheduler::createPacketClasses()
{
  QList<quint32> controlclass;
  controlclass.append(Packet::TLPPT_NAK);
  controlclass.append(Packet::TLPPT_RAK);
  controlclass.append(Packet::TLPPT_ERR);
  controlclass.append(Packet::TLPPT_RST);
  controlclass.append(Packet::TLPPT_CAN);
  controlclass.append(Packet::TLPPT_ACK);
  controlclass.append(Packet::TLPPT_SECURE_DATA);
  controlclass.append(Packet::TLPPT_KEY);
  // Add the packet class to the list
  packetclasses_[0] = controlclass;

  QList<quint32> messageclass;
  messageclass.append(Packet::TLPPT_BACK_COMPAT_DATA);
  messageclass.append(Packet::TLPPT_DATA);
  // Add the message class to the list
  packetclasses_[1] = messageclass;

  QList<quint32> dataclass;
  dataclass.append(Packet::Packet::TLPPT_NOTIFY_ULP);
  dataclass.append(Packet::TLPPT_NOTIFY_ULP_DATA);
  dataclass.append(Packet::TLPPT_NOTIFY_ULP_DATA | Packet::TLPPT_JUMBO);
  // Add the data class to the list.
  packetclasses_[2] = dataclass;
}

void PacketScheduler::start()
{
  if (isRunning_) return;
  isRunning_ = true;
#ifdef P2PDEBUG_TLP_SCHEDULER
  qDebug("%s: starting packet scheduling", Q_FUNC_INFO);
#endif
  // Start the scheduling timer.
  schedulingtimer_->start();
}

void PacketScheduler::stop()
{
  isRunning_ = false;
#ifdef P2PDEBUG_TLP_SCHEDULER
  qDebug("%s: stopping packet scheduling", Q_FUNC_INFO);
#endif
  // Stop the scheduling timer.
  schedulingtimer_->stop();
}

//BEGIN Timer Event Handling Functions

void PacketScheduler::timer_OnSchedulePacketsToSend()
{
#ifdef P2PDEBUG_TLP_SCHEDULER
  qDebug("%s: enter", Q_FUNC_INFO);
#endif
  QHash<quint32, TransportBridge*>::ConstIterator item;
  // Get the transport layer bridges.
  const QHash<quint32, TransportBridge*> & bridges = transport_->getTransportBridges();
  // Get the transport layer outgoing packet queues.
  const QHash<quint32, PacketQueue*> & queues = transport_->getOutgoingPacketQueues();

  for (item = bridges.begin(); item != bridges.end(); ++item)
  {
    // Get the packet queue assigned to the current transport bridge.
    PacketQueue *queue = queues[item.key()];
    if (queue->isEmpty())
    {
#ifdef P2PDEBUG_TLP_SCHEDULER
      qDebug("%s: No packets to send on bridge %d", Q_FUNC_INFO, item.key());
#endif
      // If the packet queue for the current bridge
      // is empty check the next bridge
      continue;
    }

    // Get the mtu and sending throttle of the current transport bridge.
    const quint32 maxDataChunkSize = (*item)->getProperties().value(QLatin1String("maxDataChunkSize")).toUInt();
    const quint32 maxPendingChunks = (*item)->getProperties().value(QLatin1String("maxPendingChunks")).toUInt();
    // Get the sent packets queue for the current transport bridge.
    PacketQueue *sentPacketsQueue = transport_->getSentPacketQueueBy(item.key());
    // If the packet queue is not empty, try to send the packets.
    while (!queue->isEmpty() && (quint32)sentPacketsQueue->size() < maxPendingChunks)
    {
      // Get the next packet that is ready to be sent.
      Packet *packet = getNextPacket(queue);

      // If the packet payload exceeds the mtu of the transport
      // bridge we have to fragment the payload into chunks.
      if (packet->getHeader().blobSize > maxDataChunkSize)
      {
        Packet *originalPacket = packet;
        // Get the next chunk of the packet to send.
        packet = Helper::getNextPacketChunk(originalPacket, maxDataChunkSize);
        // Check whether this is the last chunk of the packet.
        if (packet->getHeader().offset + packet->getHeader().payloadSize == packet->getHeader().blobSize)
        {
          // If so, remove the original packet from the queue.
          Helper::deletePacketFromQueue(queue, originalPacket);
        }
      }
      else
      {
        // Otherwise, we can send the single packet
        // which contains the entire payload; so,
        // remove the packet from the queue.
        queue->removeOne(packet);
      }

      // Send the packet via the current transport bridge.
      transport_->sendPacket(packet, item.key());
    }
  }

#ifdef P2PDEBUG_TLP_SCHEDULER
  qDebug("%s: leave", Q_FUNC_INFO);
#endif
}

//END

PacketQueue PacketScheduler::selectPacketsBy(const QList<quint32> & packetclass, PacketQueue* queue)
{
  PacketQueue packets;
  QListIterator<Packet*> item(*queue);

  Packet *packet = 0l;
  while(item.hasNext() && item.peekNext() != 0l)
  {
    packet = item.next();
    if (packetclass.contains(packet->getHeader().type))
    {
      // If the packet type matches the specified
      // packet class we are looking for, add the
      // packet to the list.
      packets.append(packet);
    }
  }

  return packets;
}

Packet * PacketScheduler::getNextPacket(PacketQueue *queue)
{
  Packet *packet = 0l;
  // Set the selected packet class to the default (control packets)
  quint8 selectedclass = 0;
  PacketQueue packets;

  // TODO remove this packet filtering algorithm; move to
  //      PacketQueue.enqueue(bool prepend):void function

  // Try to select packets to schedule based on their classification.
  for(quint32 clsid=0; clsid < 3; ++clsid)
  {
    packets = selectPacketsBy(packetclasses_[clsid], queue);
    // Determine whether there are any packets in the queue
    // that can be classified by the current packet class.
    if (packets.count() > 0)
    {
      // If so, use the packet queue to
      // schedule for transmission
      selectedclass = clsid;
      break;
    }
  }

#ifdef P2PDEBUG_TLP_SCHEDULER
  qDebug("%s: scheduling packets of class (%d)", Q_FUNC_INFO, selectedclass);

  foreach(Packet *packet, packets)
  {
    qDebug("%s: packet %d is ready to be sent", Q_FUNC_INFO,
           packet->getHeader().identifier);
  }
#endif

  // Get the number of packets that are ready to be scheduled.
  const qint32 count = packets.count();
  if (count > 0)
  {
    if (selectedclass < 2)
    {
      // If the current packet class is control
      // or message data, get a packet from the
      // queue.  Priority is given to session zero (0)
      // packets.
      bool hasSession0Packets = false;
      QListIterator<Packet*> item(packets);
      while(item.hasNext() && item.peekNext() != 0l)
      {
        Packet *selectablepacket = item.next();
        if (selectablepacket->getHeader().destination == 0)
        {
          // If we have found a session 0 packet, select the packet.
          packet = selectablepacket;
          hasSession0Packets = true;
          break;
        }
      }

      if (!hasSession0Packets)
      {
        // If no session 0 packets were found,
        // just get the first packet from the
        // list.
        packet = packets.first();
      }
    }
    else
    {
      // Otherwise, randomly select a data packet from the queue.
      qint32 random = (qint32)(((double)(qrand() * 1.0/0x7FFFFFFF))*count);
      if (random == count) --random;
      // Get the packet at the specified index.
      packet = packets.at(random);
    }
  }

  return packet;
}

}
