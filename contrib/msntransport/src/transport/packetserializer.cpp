//
// PacketSerializer
//
// Summary: Provides methods to serialize and deserialize a packet
//   in binary format.
//
// Authors:
//    Gregg Edghill (gregg.edghill at gmail.com)
//
// Copyright (C) 2006, Gregg Edghill
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of this software.
//
// THIS LIBRARY IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR
// MODIFY IT UNDER THE TERMS OF THE GNU LESSER GENERAL PUBLIC
// LICENSE AS PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER
// VERSION 2 OF THE LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.
//

#include "packetserializer.h"
#include "packet.h"

namespace PtpTransport
{

Packet * PacketSerializer::readPacket(QDataStream *stream)
{
  Packet *packet = 0l;

  // Check whether there is serialization stream
  // to read the packet content from.
  if (!stream)
  {
    // If not, bail!
    return packet;
  }

  packet = new Packet();
  // Ensure that the byte order of the stream is little endian.
  stream->setByteOrder(QDataStream::LittleEndian);

  Packet::Header & header = packet->getHeader();
  // Read the packet header fields from the stream.
  *stream >> header.destination;
  *stream >> header.identifier;
  *stream >> header.offset;
  *stream >> header.blobSize;
  *stream >> header.payloadSize;
  *stream >> header.type;
  *stream >> header.lprcvd;
  *stream >> header.lpsent;
  *stream >> header.lpsize;

  // Check whether the packet has a payload.
  if (header.payloadSize > 0)
  {
    // If so, read the payload from the serialization stream.
    QByteArray bytes(header.payloadSize, 0x00);
    // Read the packet payload bytes from the stream.
    stream->readRawData(bytes.data(), bytes.size());
    // Get the packet payload.
    QIODevice *payload = packet->getPayload();
    if (payload)
    {
      // Write the bytes to the packet payload.
      payload->write(bytes);
      // Seek to the beginning of the payload.
      payload->seek(0);
    }
  }

  return packet;
}

void PacketSerializer::writePacket(QDataStream *stream, Packet *packet)
{
  // Check whether there is serialization stream
  // to write the packet content to and whether a
  // valid packet has been supplied.
  if (!stream || !packet)
  {
    // If not, bail!
    return;
  }

  // Ensure that the byte order of the stream is little endian.
  stream->setByteOrder(QDataStream::LittleEndian);

  const Packet::Header & header = packet->getHeader();
  // Write the packet header fields to the stream.
  *stream << header.destination;
  *stream << header.identifier;
  *stream << header.offset;
  *stream << header.blobSize;
  *stream << header.payloadSize;
  *stream << header.type;
  *stream << header.lprcvd;
  *stream << header.lpsent;
  *stream << header.lpsize;

  // Check whether the packet has a payload.
  if (header.payloadSize > 0)
  {
    // If so, write the payload to the serialization stream.
    // Get the packet payload.
    QIODevice *payload = packet->getPayload();
    if (payload)
    {
      // Seek to the beginning of the payload bytes.
      payload->seek(0);

      QByteArray bytes(header.payloadSize, 0x00);
      // Read the packet payload bytes into the byte array.
      payload->read(bytes.data(), bytes.size());
      // Write the packet payload bytes to the stream.
      stream->writeRawData(bytes.data(), bytes.size());
    }
  }
}

}

