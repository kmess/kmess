//
// Test Miscellaneous class
//
// Summary:
//
// Authors:
//    Gregg Edghill (gregg.edghill at gmail.com)
//
// Copyright (C) 2009 - 2010, Gregg Edghill
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of this software.
//
// THIS LIBRARY IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR
// MODIFY IT UNDER THE TERMS OF THE GNU LESSER GENERAL PUBLIC
// LICENSE AS PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER
// VERSION 2 OF THE LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.
//

#include "test_miscellaneous.h"
#include <QtCore/QUuid>
#include <QtTest/QtTest>
#include "../src/helper.h"
#include "../src/msnslp/message/realtimeaddress.h"
//#include "../src/storedobjectcontext.h"
//using namespace PtpCollaboration;
using namespace PtpInternal;
using namespace PtpSignaling;

void Test_Miscellaneous::testHashedNonce()
{
  const QUuid uuid("{7356ba62-7de3-4687-a132-dc315e8e50f5}");
  const QUuid expecteduuid("{a1984d80-c440-9353-af81-f8b7eab3862b}");
  QUuid hasheduuid = Helper::sha1HashQUuid(uuid);

  QVERIFY(expecteduuid == hasheduuid);
}

void Test_Miscellaneous::testRealTimeAddressParsingAndAssignment()
{
  QUuid epid("{a1984d80-c440-9353-af81-f8b7eab3862b}");
  RealTimeAddress addresswithepid("user@host.com;{a1984d80-c440-9353-af81-f8b7eab3862b}");

  QVERIFY(QLatin1String("user@host.com") == addresswithepid.getAddress());
  QVERIFY(epid.toString() == addresswithepid.getEndpointId());
  QVERIFY(addresswithepid.isMpopSupported());

  RealTimeAddress addresswithoutepid("user@host.com");

  QVERIFY(QLatin1String("user@host.com") == addresswithoutepid.getAddress());
  QVERIFY(QLatin1String("") == addresswithoutepid.getEndpointId());
  QVERIFY(!addresswithepid.isMpopSupported());

  RealTimeAddress copy = addresswithepid;

  QVERIFY(copy == addresswithepid);
}

void Test_Miscellaneous::testStoredObjectContext()
{
  const QString xml = QLatin1String("<msnobj Creator=\"user@host.com\" Size=\"3009\" Type=\"3\" Location=\"0\" Friendly=\"RABTAEMAMAAwADUANwA1AAAA\" SHA1D=\"Wk55PYYnGomrEnRbNMq2BN8XyAUo=\"/>");
//  StoredObjectContext objectcxt(xml);

//	QVERIFY(QLatin1String("user@host.com") == objectcxt.getCreator());
//	QVERIFY(3009 == objectcxt.getObjectSize());
//	QVERIFY(3 == objectcxt.getType());
//	QVERIFY(QLatin1String("0") == objectcxt.getLocation());
//	QVERIFY(QLatin1String("RABTAEMAMAAwADUANwA1AAAA") == objectcxt.getFriendlyName());
//	QVERIFY(QLatin1String("Wk55PYYnGomrEnRbNMq2BN8XyAUo=") == objectcxt.getDataHash());
}

QTEST_MAIN(Test_Miscellaneous)

#include "test_miscellaneous.moc"
