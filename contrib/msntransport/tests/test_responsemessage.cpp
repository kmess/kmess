//
// Test Messaging class
//
// Summary:
//
// Authors:
//    Gregg Edghill (gregg.edghill at gmail.com)
//
// Copyright (C) 2009 - 2010, Gregg Edghill
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of this software.
//
// THIS LIBRARY IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR
// MODIFY IT UNDER THE TERMS OF THE GNU LESSER GENERAL PUBLIC
// LICENSE AS PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER
// VERSION 2 OF THE LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.
//

#include "test_responsemessage.h"
#include <QtTest/QtTest>
#include "../src/helper.h"
#include "../src/msnslp/message/signalinguri.h"

using namespace PtpInternal;

void TestMessaging_Response::testResponseMessageDeserialize()
{
  SignalingUri fromUri("msnmsgr:localuser@maildomain.com");
  SignalingUri toUri("msnmsgr", "peer@maildomain.com");

  QVERIFY(fromUri != toUri);

  SignalingResponse message(SignalingResponse::SLPRC_UNACCEPTABLE, "Unacceptable", "1.0");

  // Create the via header.
  SignalingHeader viaHeader(QLatin1String("Via"), QLatin1String("MSNSLP/1.0/TLP"));
  QUuid branch("{DB187185-BFB2-4925-AD4C-05A7C0579AA6}");
  viaHeader.addParameter(QLatin1String("branch"), branch.toString().toUpper());

  message.setSignalingHeader("To", QString("<%1>").arg(toUri.toString()));
  message.setSignalingHeader("From", QString("<%1>").arg(fromUri.toString()));
  message.addSignalingHeader(viaHeader);
  message.setSignalingHeader("Call-ID", "{8DF95352-F0B0-4402-A355-7551F691870C}");
  message.setSignalingHeader("Content-Type", "application/x-msnmsgr-session-failure-respbody");
  message.setSignalingHeader("CSeq", 1);
  message.setMessageBody("SessionID: 16152\r\nSChannelState: 0\r\nCapabilities-Flags: 1\r\n\r\n");

  SignalingMessage * deserialized = Helper::parseSignalingMessage(message.getRawMessageByteArray());
  QVERIFY(deserialized != 0l);

  SignalingRequest *request = dynamic_cast<SignalingRequest*>(deserialized);
  QVERIFY(request == 0l);

  SignalingResponse *response = dynamic_cast<SignalingResponse*>(deserialized);
  QVERIFY(response != 0l);

  onResponseMessage(*response);

  // Dispose of the message.

  if (deserialized)
  {
    delete deserialized;
  }
}

void TestMessaging_Response::onResponseMessage(const SignalingResponse& response)
{
  SignalingResponse copy(response);
  qDebug("<<< SLP message BEGIN, %s<<< SLP message END",
    qPrintable(QString::fromUtf8(copy.getRawMessageByteArray())));

  qDebug("response code: %d", copy.getResponseCode());
  qDebug("response phrase: %s", qPrintable(copy.getResponsePhrase()));
  qDebug("Via value w/o parameters: %s", qPrintable(copy.getViaHeader().getValueNoParameters().toString()));
  qDebug("branch: %s", qPrintable(copy.getViaHeader().getParameter("branch").toString()));
  qDebug("version: %s", qPrintable(copy.getVersion()));
}

QTEST_MAIN(TestMessaging_Response)

#include "test_responsemessage.moc"
