//
// Test Miscellaneous class
//
// Authors:
//    Gregg Edghill (gregg.edghill at gmail.com)
//
// Copyright (C) 2009 - 2010, Gregg Edghill
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of this software.
//
// THIS LIBRARY IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR
// MODIFY IT UNDER THE TERMS OF THE GNU LESSER GENERAL PUBLIC
// LICENSE AS PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER
// VERSION 2 OF THE LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.
//

#ifndef CLASS_TEST__MISCELLANEOUS_H
#define CLASS_TEST__MISCELLANEOUS_H

#include <QtCore/QObject>

class Test_Miscellaneous : public QObject
{
  Q_OBJECT

  private slots:
    void testHashedNonce();
    void testRealTimeAddressParsingAndAssignment();
    void testStoredObjectContext();
};

#endif
