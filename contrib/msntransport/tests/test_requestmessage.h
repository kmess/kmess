//
// Test Messaging class
//
// Authors:
//    Gregg Edghill (gregg.edghill at gmail.com)
//
// Copyright (C) 2009 - 2010, Gregg Edghill
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of this software.
//
// THIS LIBRARY IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR
// MODIFY IT UNDER THE TERMS OF THE GNU LESSER GENERAL PUBLIC
// LICENSE AS PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER
// VERSION 2 OF THE LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.
//

#ifndef CLASS_TEST__REQUESTMESSAGING_H
#define CLASS_TEST__REQUESTMESSAGING_H

#include <QtCore/QObject>
#include "../src/msnslp/message/signalingrequest.h"
#include "../src/msnslp/message/signalingresponse.h"
using namespace PtpSignaling;

class TestMessaging_Request : public QObject
{
  Q_OBJECT

  public:
    void onRequestMessage(const SignalingRequest& request);

  private slots:
    void testRequestMessageDeserialize();
};

#endif // MESSAGING_H
