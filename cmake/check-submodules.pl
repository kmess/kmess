#!/usr/bin/perl
use strict;
use warnings;

# This script, written in Perl, checks whether all Git submodules are
# up-to-date.
# It either prints the output "OK", which means that all submodules were OK, or
# it prints a message to show to the user, which describes how to update the
# submodules.

# Written by Sjors Gielen, july 2010, for the KMess project.
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction. No warrenty of ANY KIND are included.

my $cmakedir = `dirname $0`;
1 while( chomp $cmakedir );
chdir( $cmakedir . "/.." );

my @not_ok;
my $unknown_not_ok = 0;

foreach(split /\n/, `git submodule status`)
{
  # All lines should start with a space.
  /^(.)(?:\w+) (.+) ?\(?/;
  if( $1 ne " " )
  {
    if( $2 ) {
      push @not_ok, $2;
    } else {
      ++$unknown_not_ok;
    }
    next;
  }
}

if( @not_ok == 0 && $unknown_not_ok == 0 )
{
  print "OK\n";
  exit;
}

print "The following git submodules are not OK:\n  ";
print join( ", ", @not_ok );
if( @not_ok > 0 && $unknown_not_ok > 0 )
{
  print ", and ";
}
if( $unknown_not_ok > 0 )
{
  print "$unknown_not_ok unknown submodules";
}

print "\nRun \"git submodule update --init\" to update your Git submodules.\n";
