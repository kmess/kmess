#!/bin/sh

cd `dirname $0`/..

# second sed expression fixes Mac OS X 10.4
appver="`egrep 'SET.*KMESS_VERSION' CMakeLists.txt | sed -e 's/.*"\([^"]*\)".*/\1/'`"
date="`date +%Y%m%d`"

# Make sure we have the right repository
if [ ! -f kmess.kdev4 ]; then
  /bin/echo -n "git (unknown)"
  exit
fi

# Display normal version if git is not installed
if [ ! -x `which git 2>/dev/null` ]; then
  /bin/echo -n "$appver"
  exit
fi

# Display subversion version if this is a working copy
gitname=`LANG=C git describe --all HEAD 2>/dev/null | sed -e 's:^heads/::g'`
gitver=`LANG=C git rev-parse --short HEAD 2>/dev/null`

# see if this is not a repository.
if [ -z "$gitver" ]; then
  if [ -z "$KMESS_VER" ]; then
    /bin/echo -n "$appver";
  else
    /bin/echo -n "$KMESS_VER";
  fi
  exit;
fi

# display subversion version and date
/bin/echo -n "$appver-git ($gitname@$gitver >= $date)"
