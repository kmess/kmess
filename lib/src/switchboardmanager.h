/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Sjors Gielen <sjors@kmess.org>                            *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file switchboardmanager.h
 * @brief Creator and handler of MsnSwitchboardConnection instances
 */

#ifndef SWITCHBOARDMANAGER_H
#define SWITCHBOARDMANAGER_H

#include <KMess/Message>
#include <QObject>
#include <QHash>
#include <QStringList>

// Forward declarations
namespace KMess
{
  class Debugger;
  class MsnSession;
  class Message;
};


namespace KMessInternal
{

  class MsnSwitchboardConnection;
  class ChatFactory;
  
  /**
   * The SwitchInfo struct tracks pertinent information about a switchboard connection.
   *
   * This structure is supplied as the argument of newDataSession and newChatSession. From
   * this you should have enough information to handle the connection appropriately.
   */
  struct SwitchInfo
  {
    MsnSwitchboardConnection    *conn;
    QString                     startingHandle;
    QStringList                 participants;
    QList<KMess::Message>       messages;
  };
  
/**
 * @brief Private manager class for MsnSwitchboardConnection instances.
 *
 * This class creates MsnSwitchboardConnection instances.
 *
 * Normally, switchboard
 * connections are used for either data transfers, or chat message transfers.
 * However, sometimes a data switchboard suddenly starts receiving chat
 * messages (for example, KMess 2.0 does this). Therefore, we cannot assume
 * that a switchboard that starts with data messages, will never end up
 * receiving chat messages.
 *
 * This class keeps a close eye on the messages
 * emitted from a switchboard, to create an MsnChat object for a switchboard
 * the moment it emits its first chat message. It will also notify PtpGlue of
 * all new switchboards, so it can catch the P2P data messages emitted by it.
 *
 * @author Sjors Gielen
 * @ingroup Root
 */
class SwitchboardManager : public QObject
{
  friend class KMess::Debugger;

  Q_OBJECT

  public:

    // The constructor
                                    SwitchboardManager(KMess::MsnSession *session, KMessInternal::ChatFactory *chatFactory);
    // The destructor
    virtual                        ~SwitchboardManager( );

  public slots:
    void startSwitchboard( const QString &handle, const QString &server, const quint16 port, const QString &auth, const QString &chatId );
    void slotMessageReceived( KMess::Message message );
    void slotContactJoined( QString contact );
    void slotContactLeft( QString contact );

  signals:
    /** Emitted when this class creates a switchboard. The'res only one other
     * class able to create switchboards: MsnChat.
     */
    void switchboardCreated( KMessInternal::MsnSwitchboardConnection *switchboard );

  private:
    KMess::MsnSession          *session_;
    KMessInternal::ChatFactory *chatFactory_;
    QHash<MsnSwitchboardConnection *, SwitchInfo> connections_;
};



} // namespace KMess



#endif
