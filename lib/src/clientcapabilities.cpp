/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Valerio Pilo <vapclerio@kmess.org>                        *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file clientcapabilities.cpp
 * @brief Client capabilities detection
 */

#include "debug/libkmessdebug.h"

#include <KMess/ClientCapabilities>

#include <QStringList>


using namespace KMess;

// Used to extract only the MSN client version number
#define CAPS_VERSION_MASK     0xF0000000



/**
 * Empty constructor.
 */
ClientCapabilities::ClientCapabilities()
: capabilities_( CF_NONE )
, extendedCapabilities_( CXF_NONE )
{
}



/**
 * Constructor.
 *
 * @param caps A capabilities string
 */
ClientCapabilities::ClientCapabilities( const QString caps )
: capsString_( caps )
{
  if( ! parseCaps( caps, capabilities_, extendedCapabilities_ ) )
  {
    capsString_.clear();
    warning() << "Capabilities parsing failed:" << caps;
  }
}



/**
 * Return the capabilities flags.
 *
 * @return flags
 */
ClientCapabilities::CapabilitiesFlags ClientCapabilities::caps() const
{
  return capabilities_;
}



/**
 * Return the extended capabilities flags.
 *
 * @return flags
 */
ClientCapabilities::CapabilitiesExtendedFlags ClientCapabilities::extCaps() const
{
  return extendedCapabilities_;
}



/**
 * Change the caps with new ones.
 * Returns whether the specified caps are different from the current.
 *
 * @param newCapsString A capabilities string
 * @return True if the given caps are different
 */
bool ClientCapabilities::setCaps( const QString newCapsString )
{
  CapabilitiesFlags         oldCaps         = capabilities_;
  CapabilitiesExtendedFlags oldExtendedCaps = extendedCapabilities_;

  bool parsed = parseCaps( newCapsString, capabilities_, extendedCapabilities_ );

  if( ! parsed )
  {
    warning() << "New caps are not valid, ignoring:" << newCapsString;
    return false;
  }

  capsString_ = newCapsString;

  return ( oldCaps != capabilities_ || oldExtendedCaps != extendedCapabilities_ );
}



/**
 * Change the caps with new ones.
 * Returns whether the specified caps are different from the current.
 *
 * @param newCaps An ORed capabilities flags list
 * @param newExtCaps An ORed extended capabilities flags list
 * @return True if the given caps are different
 */
bool ClientCapabilities::setCaps( const CapabilitiesFlags newCaps, const CapabilitiesExtendedFlags newExtCaps )
{
  CapabilitiesFlags         oldCaps         = capabilities_;
  CapabilitiesExtendedFlags oldExtendedCaps = extendedCapabilities_;

  capabilities_         = newCaps;
  extendedCapabilities_ = newExtCaps;

  // First empty it or it won't be recreated
  capsString_.clear();
  capsString_ = toString();

  return ( oldCaps != capabilities_ || oldExtendedCaps != extendedCapabilities_ );
}



/**
 * Parse a client capabilities string into flags
 *
 * The caps are expressed with two enums. The first contains the main caps,
 * and the other enum is appended to it after a colon: "caps:extcaps" or "12345678:123".
 * If there are no extcaps, the caps can be "12345678:0" or even just "12345678".
 *
 * @param string A capabilities string
 * @param caps Variable where to store the main caps
 * @param extCaps Variable where to store the extended caps
 * @return bool True on success
 */
bool ClientCapabilities::parseCaps( const QString string, CapabilitiesFlags& caps, CapabilitiesExtendedFlags& extCaps )
{
  // Check the string beforehand
  if( string.length() < 3 )
  {
    return false;
  }

  QStringList pair( string.split( ':', QString::KeepEmptyParts ) );

  Q_ASSERT( pair.size() > 0 );

  // Parse the standard capabilities first - on error, the parameters are not changed

  CapabilitiesFlags parsedCaps;
  CapabilitiesExtendedFlags parsedExtCaps;

  bool ok;
  parsedCaps = (CapabilitiesFlags) pair[0].toLong( &ok );
  if( ! ok )
  {
    return false;
  }
  caps = parsedCaps;

  // Parse the new extended caps
  if( pair.size() > 1 )
  {
    parsedExtCaps = (CapabilitiesExtendedFlags) pair[1].toLong( &ok );

    // If the ext caps are not valid, just skip them
    if( ! ok )
    {
      return true;
    }

    extCaps = parsedExtCaps;
  }

  return true;
}



/**
 * Return the capabilities as a string.
 *
 * The protocol requires the caps to be expressed as two 32-bit integers separated by a colon:
 * CF_VERSION_8_5 and CF_SUPPORTS_P2P_VERSION_2 is <code>2147483648:2</code>
 *
 * @return string
 */
QString ClientCapabilities::toString() const
{
  if( ! capsString_.isEmpty() )
  {
    return capsString_;
  }

  // Create and return the string
  return   QString::number( capabilities_, 10 )
         + ':'
         + QString::number( extendedCapabilities_, 10 );
}



/**
 * Gets the messenger client version.
 *
 * return version number structure.
 */
ClientCapabilities::Version ClientCapabilities::getClientVersion() const
{
  Version version;

  switch( capabilities_ & CAPS_VERSION_MASK )
  {
    case CF_VERSION_6_0:  version = Version(  6, 0 ); break;
    case CF_VERSION_6_1:  version = Version(  6, 1 ); break;
    case CF_VERSION_6_2:  version = Version(  6, 2 ); break;
    case CF_VERSION_7_0:  version = Version(  7, 0 ); break;
    case CF_VERSION_7_5:  version = Version(  7, 5 ); break;
    case CF_VERSION_8_0:  version = Version(  8, 0 ); break;
    case CF_VERSION_8_1:  version = Version(  8, 1 ); break;
    case CF_VERSION_8_5:  version = Version(  8, 5 ); break;
    case CF_VERSION_9_0:  version = Version(  9, 0 ); break;
    case CF_VERSION_14_0: version = Version( 14, 0 ); break;
    default:              version = Version(  0, 0 ); break;
  }

  return version;
}



/**
 * Gets a value indicating whether the user is online via the web.
 *
 * @return bool
 */
bool ClientCapabilities::isOnlineViaTheWeb() const
{
  return ( capabilities_ & CF_ONLINE_VIA_WEBIM ) != 0;
}



/**
 * Gets a value indicating whether the user is online via a mobile device.
 *
 * @return bool
 */
bool ClientCapabilities::isOnlineViaMobileDevice() const
{
  return ( capabilities_ & CF_ONLINE_VIA_MOBILE ) != 0;
}



/**
 * Gets whether the client can receive handwritten messages (Ink) in the GIF format.
 *
 * @return bool
 */
bool ClientCapabilities::supportsGifInk() const
{
  return ( capabilities_ & CF_SUPPORTS_GIF_INK ) != 0;
}



/**
 * Gets whether the client can receive handwritten messages (Ink) in the ISF format.
 *
 * @return bool
 */
bool ClientCapabilities::supportsIsfInk() const
{
  return ( capabilities_ & CF_SUPPORTS_ISF_INK ) != 0;
}



/**
 * Gets a value indicating whether the client supports sending large messages.
 *
 * @return bool
 */
bool ClientCapabilities::supportsMessageChunking() const
{
  return ( capabilities_ & CF_SUPPORTS_MESSAGE_CHUNKING ) != 0;
}



/**
 * Gets a value indicating whether the client's capabilities include P2P support.
 *
 * @return bool
 */
bool ClientCapabilities::supportsP2p() const
{
  return ( capabilities_ & CAPS_VERSION_MASK ) != 0;
}


