/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Valerio Pilo <valerio@kmess.org>                          *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file listsmanager.h
 * @brief Keeper of the contacts and groups lists
 */

#ifndef LISTSMANAGER_H
#define LISTSMANAGER_H

#include <KMess/NetworkGlobals>

#include <QStringList>

// Forward declarations
namespace KMess
{
  class ContactListModel;
  class MsnSession;
  class Self;
}


namespace KMessInternal
{


/**
 * @brief Private master class managing contacts lists.
 *
 * This private class keeps the lists of contacts and groups. Whenever new
 * contacts are added or removed, this class takes note and updates the
 * Contact List Model.
 *
 * @author Valerio Pilo
 * @ingroup Root
 */
class ListsManager : public QObject
{
  Q_OBJECT


  public:
                        ListsManager( KMess::MsnSession *session );
    virtual            ~ListsManager();
    KMess::MsnContact*  contact( const QString& handle, bool create = false );
    KMess::MsnGroup*    group( const QString& id ) const;
    const IndexedContactsList contacts() const;
    const IndexedGroupsList   groups() const;
    KMess::ContactListModel* model() const;
    KMess::Self*        self() const;
    

  public slots:
    KMess::MsnContact*  contactAdd( const QString& handle, const QString& friendlyName,
                                    int lists, QStringList groupIds, const QString& guid, KMess::NetworkType network );
    void                contactReadded( const QString &handle );
    void                contactAddToGroup( const QString& handle, const QString& groupId );
    void                contactRemoveFromGroup( const QString& handle, const QString& groupId );
    void                contactRemove( const QString &handle );
    KMess::MsnGroup*    groupAdd( const QString& id, const QString& name );
    void                groupRemove( const QString& id );
    void                groupRename( const QString& id, const QString& newName );

  private: // Private attributes
    /// The hash containing all known contact objects
    IndexedContactsList contacts_;
    /// Hash with the list of user-defined contact groups
    IndexedGroupsList   groups_;
    /// The contact list model object
    KMess::ContactListModel* model_;
    /// The user's representation as a contact
    KMess::Self*             self_;
    /// Pointer to the session we're part of
    KMess::MsnSession*  session_;
    /// A list of unknown contacts.
    IndexedContactsList unknownContacts_;

};



} // namespace KMess



#endif
