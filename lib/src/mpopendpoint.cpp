/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Adam Goossens <adam@kmess.org>                            *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file mpopendpoint.cpp
 */

#include <KMess/MPOPEndpoint>
#include <QDebug>

using namespace KMess;

/**
 * Constructs a new MPOPEndpoint instance.
 *
 * @param contact The MsnContact this endpoint should belong to.
 * @param guid The machine GUID for the endpoint.
 * @param name The name of the endpoint.
 */
MPOPEndpoint::MPOPEndpoint( MsnContact *contact, const QUuid &guid, const QString &name )
 : contact_( contact )
 , guid_ ( guid )
 , name_ ( name )
{
}


/**
 * Return the MsnContact instance that has this endpoint.
 */
MsnContact *MPOPEndpoint::contact() const 
{
  return contact_;
}



/**
 * Return the machine GUID for this endpoint.
 */
QUuid MPOPEndpoint::guid() const
{
  return guid_;
}



/**
 * Return the name of this endpoint, if known.
 */
QString MPOPEndpoint::name() const
{
  return name_;
}



/**
 * Set the machine guid of this endpoint.
 * @param guid The machine GUID to set.
 */
void MPOPEndpoint::setGuid( const QUuid &guid )
{
  guid_ = guid;
}



/**
 * Set the name of this endpoint.
 * @param name The name of this endpoint.
 */
void MPOPEndpoint::setName( const QString &name )
{
  name_ = name;
}



// stream this object into qdebug.
QDebug operator<<( QDebug dbg, const MPOPEndpoint &ep )
{
  dbg.nospace() << "EpName:" << ep.name() << ", Machine GUID:" << ep.guid();
  return dbg.space();
}

