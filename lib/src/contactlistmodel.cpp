/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Mike K. Bennett <mkb137b@hotmail.com>                     *
 * (C) Copyright Diederik van der Boor <diederik@kmess.org>                *
 * (C) Copyright Valerio Pilo <valerio@kmess.org>                          *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file contactlistmodel.cpp
 * @brief A read-only Qt model to represent the contacts list
 */

#include "debug/libkmessdebug.h"
#include "contactlistmodel.h"

#include <KMess/ContactList>
#include <KMess/MsnContact>
#include <KMess/MsnGroup>
#include <KMess/MsnSession>
#include <KMess/ContactListModelItem>

#include <QFile>
#include <QMimeData>


using namespace KMess;



// The constructor
ContactListModel::ContactListModel( MsnSession* session )
: rootNode_( 0 )
, session_( session )
{
  // Create the special groups
  specialGroups_.insert( SpecialGroups::FAVORITES,   new MsnGroup( SpecialGroups::FAVORITES,   true ) );
  specialGroups_.insert( SpecialGroups::INDIVIDUALS, new MsnGroup( SpecialGroups::INDIVIDUALS, true ) );
  specialGroups_.insert( SpecialGroups::ONLINE,      new MsnGroup( SpecialGroups::ONLINE,      true ) );
  specialGroups_.insert( SpecialGroups::OFFLINE,     new MsnGroup( SpecialGroups::OFFLINE,     true ) );
  specialGroups_.insert( SpecialGroups::ALLOWED,     new MsnGroup( SpecialGroups::ALLOWED,     true ) );
  specialGroups_.insert( SpecialGroups::REMOVED,     new MsnGroup( SpecialGroups::REMOVED,     true ) );
  specialGroups_.insert( SpecialGroups::UNKNOWN,     new MsnGroup( SpecialGroups::UNKNOWN,     true ) );

  // Reset also recreates the basic model items
  reset();
}



// The destructor
ContactListModel::~ContactListModel()
{
  debug() << "DESTROYING...";

  // make sure nobody is listening
  this->disconnect();

  // Destroy the model's contents, without regenerating the special groups
  reset( false );

  // The qDeleteAll() macro cannot be used: the MsnContact and MsnGroup
  // destructors are both private, and the macro isn't their friend
  foreach( MsnGroup* group, specialGroups_ )
  {
    delete group;
  }
  specialGroups_.clear();

  debug() << "DESTROYED";
}



// Add a contact to the model
void ContactListModel::contactAdd( MsnContact *newContact )
{
  KMESS_NULL( newContact );
  KMESS_NULL( rootNode_ );

  QList<MsnGroup*> groups = newContact->groups();

  // Add the contact to the special groups
  if( newContact->isFriend() )
  {
    // Add contacts without group to the Individuals group
    if( groups.isEmpty() )
    {
      groups << specialGroups_[ SpecialGroups::INDIVIDUALS ];
    }

    groups << specialGroups_[ ( newContact->isOnline() ? SpecialGroups::ONLINE : SpecialGroups::OFFLINE ) ];
  }
  else
  {
    if ( newContact->isUnknown() )
    {
      groups << specialGroups_[ SpecialGroups::UNKNOWN ];
    }
    else
    {
      groups << specialGroups_[ ( newContact->isAllowed() ? SpecialGroups::ALLOWED : SpecialGroups::REMOVED ) ];
    }
  }

  // Add it to the nodes it belongs to
  foreach( MsnGroup *group, groups )
  {
    ContactListModelItem *groupNode = rootNode_->child( group );
    KMESS_ASSERT( groupNode );
    if( ! groupNode )
    {
      warning() << "Contact's group" << group << "doesn't exist!";
      continue;
    }

    // form a QModelIndex for the group which will parent this contact
    const QModelIndex& groupIdx( index( groupNode->row(), 0, QModelIndex() ) );

    KMESS_ASSERT( groupIdx.isValid() );
    if ( !groupIdx.isValid() )
    {
      warning() << "Formed invalid index for group" << group << "for contact" << newContact << ", not adding it to group";
      continue;
    }

    beginInsertRows( groupIdx, groupNode->childCount(), groupNode->childCount() );

    new ContactListModelItem( newContact, groupNode );

    endInsertRows();
  }
  
  // need to know when it moves between blocked/allowed.
  connect( newContact, SIGNAL( membershipsChanged() ), 
           this,       SLOT  ( contactMove()        ) );

  // need to know when it moves between groups
  connect( newContact, SIGNAL( groupsChanged()      ),
           this,       SLOT  ( contactMove()        ) );

  // need to know when it changes status to move between special groups.
  connect( newContact, SIGNAL( statusChanged() ), 
           this,       SLOT  ( contactUpdate()      ) );
}



// Add a group to the contact list
void ContactListModel::groupAdd( const MsnGroup *group )
{
  KMESS_NULL( group );
  KMESS_NULL( rootNode_ );

  debug() << "Adding group:" << group->id() << "with name:" << group->name();

  // Insert the new group
  beginInsertRows( QModelIndex(), rootNode_->childCount(), rootNode_->childCount() );

  new ContactListModelItem( group, rootNode_ );

  endInsertRows();
}



/**
 * Update a contact's memberships to the ONLINE and OFFLINE special groups when they change
 * status.
 *
 * If \arg contact is null, then sender() will be used.
 * 
 * @param contact The contact which has changed status
 */
void ContactListModel::contactUpdate( MsnContact *contact )
{
  if ( ! contact )
  {
    contact = qobject_cast<MsnContact*>(sender());
  }

  KMESS_NULL( contact );
  KMESS_NULL( rootNode_ );

  ContactListModelItem *onlineGroup, *offlineGroup, *sourceGroup, *destinationGroup;
  onlineGroup = offlineGroup = sourceGroup = destinationGroup = 0;

  onlineGroup = rootNode_->child( specialGroups_.value( SpecialGroups::ONLINE ) );
  offlineGroup = rootNode_->child( specialGroups_.value( SpecialGroups::OFFLINE ) );

  KMESS_ASSERT( onlineGroup );
  KMESS_ASSERT( offlineGroup );

  // if they're no longer a friend then the ONLINE/OFFLINE memberships
  // should have been removed by contactMove().
  // so disregard.
  if ( ! contact->isFriend() )
  {
    return;
  }
  
  // if they're in the Online group, but they're now offline, move them to the offline group.
  // if they're still online, it's a standard status change.
  if ( onlineGroup->child( contact ) )
  {
    if ( contact->isOffline() )
    {
      sourceGroup = onlineGroup;
      destinationGroup = offlineGroup;
    }
    else
    {
      forwardDataChanged( rootNode_->children( contact ) );
      return;
    }
  }
  
  // if they're in the Offline group, but they're now Online, move them to the Online group.
  // if they're still offline, well, who knows what this is.
  if ( offlineGroup->child( contact) )
  {
    if ( contact->isOnline() )
    {
      sourceGroup = offlineGroup;
      destinationGroup = onlineGroup;
    }
    else
    {
      return;
    }
  }
  
  KMESS_ASSERT( sourceGroup );
  KMESS_ASSERT( destinationGroup );
  
  // Move the contact from the first group and put it in the other one
  debug() << "Moving contact" << contact->handle() << "from" << sourceGroup << "to" << destinationGroup;

  ContactListModelItem *contactItem = sourceGroup->child( contact );

  if( ! contactItem )
  {
    warning() << "Unable to find contact" << contact->handle() << "(now online?" << contact->isOnline();
    warning() << "The contact is in groups:" << rootNode_->children( contact );
    KMESS_ASSERT( contactItem );
    return;
  }

  emit layoutAboutToBeChanged();

  // Move the node to the new place
  contactItem->moveTo( destinationGroup );

  emit layoutChanged();
}



/**
 * Get the number of columns present in an index
 *
 * Just returns the root node's number of columns as it doesn't change.
 *
 * @param parent The index to count columns of (ignored)
 * @return int
 */
int ContactListModel::columnCount( const QModelIndex &parent ) const
{
  Q_UNUSED( parent );

  if( rootNode_ == 0 )
  {
    warning() << "Called columnCount() without a root node!";
    return 0;
  }

  return rootNode_->columnCount();
}



/**
 * Return the data contained by an index in the model
 *
 * @param index  Points to the actual data to be returned
 * @param role   Indicates what data from the index needs to be obtained.
 * @see http://doc.trolltech.com/4.3/qt.html#ItemDataRole-enum for available roles
 */
QVariant ContactListModel::data( const QModelIndex &index, int role ) const
{
  const ContactListModelItem *item = static_cast<ContactListModelItem*>( index.internalPointer() );

  // If the index isn't valid or contained non valid data, do nothing
  if( ! index.isValid() || ! item )
  {
    return QVariant();
  }

  return item->data( role );
}



/**
 * Internal private method; Emits the Data Changed signal on one item.
 *
 * This is only an overloaded forwardDataChanged() version.
 *
 * @param item The item which changed
 */
inline void ContactListModel::forwardDataChanged( ContactListModelItem *item )
{
  forwardDataChanged( ModelItemList() << item );
}



/**
 * Internal private method; Emits the Data Changed signal on some items.
 *
 * @internal
 * @param itemList  The list of items which changed
 */
void ContactListModel::forwardDataChanged( ModelItemList itemList )
{
  if( itemList.isEmpty() )
  {
    warning() << "Called with an empty list!";
    return;
  }

  foreach( ContactListModelItem *item, itemList )
  {
    QModelIndex changedIndex;

    if( item->parent() == rootNode_ )
    {
      changedIndex = index( item->row(), 0, QModelIndex() );
    }
    else
    {
      changedIndex = createIndex( item->row(), 0, item );
    }

    emit dataChanged( changedIndex, changedIndex );
  }
}



#if 0
/**
 * Dumps the contact list contents to the debug output
 *
 * This method is exclusively meant for debugging purposes.
 * This method is also recursive. Do not call with any arguments, they are for internal use only.
 *
 * @param start Item in the model where to start from
 * @param depth Current search depth
 */
void ContactListModel::dump( ContactListModelItem *start, int depth ) const
{
#ifdef KMESSDEBUG_CONTACTLISTMODEL
  if( ! rootNode_ )
  {
    debug() << "Contact List model is empty!";
    return;
  }

  bool started = false;
  if( ! start )
  {
    start = rootNode_;
    debug() << "Contact List model stats";
    debug() << "--------------------------------------";
    debug() << "Columns:"            << columnCount();
    debug() << "Rows:"               << rowCount();
    debug() << "Root group Columns:" << rootNode_->columnCount();
    debug() << "Root group Rows:"    << rootNode_->childCount();
    debug();
    debug() << "Contact List model dump";
    debug() << "--------------------------------------";
    debug() << "Root @" << (void*)rootNode_;
    started = true;
  }

  QString spacer( QString().fill( '-', 2 * depth ) + ">" );
  int sortRole = ContactListModelItem::SortRole;

  // Print the current node's contents, and manage children recursively
  for( int i = 0; i < start->childCount(); i++ )
  {
    ContactListModelItem* item = start->child( i );
    int type = item->getType();

    switch( type )
    {
      case ContactListModelItem::ItemContact:
        debug() << spacer << (i+1) << ") Contact:"
                           << item->data( 0 ).toMap()["handle"].toString()
                           << "(" << item->data( sortRole ).toInt() << ") "
                           "@" << (void*)item;
        break;

      case ContactListModelItem::ItemGroup:
        debug() << spacer << (i+1) << ") MsnGroup:"
                           << item->data( 0 ).toMap()["id"].toString()
                           << "(" << item->data( sortRole ).toInt() << ") "
                           "@" << (void*)item;
        dump( item, depth + 1 );
        break;

      default:
        debug() << spacer << "Unknown type of item: " + type << "@" << (void*)item;
        break;
    }
  }

  if( started )
  {
    debug() << "--------------------------------------";
  }
#else
  Q_UNUSED( start );
  Q_UNUSED( depth );
#endif
}
#endif



// Find out if a group contains contacts
bool ContactListModel::isGroupEmpty( const QString &groupId ) const
{
  ContactListModelItem *item = rootNode_->child( session_->contactList()->group( groupId ) );
  if( item == 0 )
  {
    warning() << "Cannot find group" << groupId;
    return false;
  }

  return ( item->childCount() <= 0 );
}




// Find out if a model index contains other indices
bool ContactListModel::hasChildren( const QModelIndex &parent ) const
{
  if( parent.column() > 0 )
  {
    return false;
  }

  if( ! parent.isValid() )
  {
    if( rootNode_ == 0 )
    {
      return false;
    }

    return ( rootNode_->childCount() > 0 );
  }

  const ContactListModelItem *item = static_cast<ContactListModelItem*>( parent.internalPointer() );

  if( ! item )
  {
    return false;
  }

  return ( item->childCount() > 0 );
}



// Find out if the model contains a valid index at the specified position
bool ContactListModel::hasIndex( int row, int column, const QModelIndex &parent ) const
{
  if( ! parent.isValid() )
  {
    if( rootNode_ == 0 )
    {
      return false;
    }

    return (    ( row    >= 0 && row    < rootNode_-> childCount() )
             && ( column >= 0 && column < rootNode_->columnCount() ) );
  }

  if( parent.column() > 0 )
  {
    return false;
  }

  const ContactListModelItem *item = static_cast<ContactListModelItem*>( parent.internalPointer() );

  if( ! item )
  {
    return false;
  }

  return (    ( row    >= 0 && row    < item->childCount() )
           && ( column >= 0 && column < item->columnCount() ) );
}



/**
 * Obtain a model index for a specifically located item
 *
 * @param row     Row of the item
 * @param column  Column of the item
 * @param parent  Parent of the model which contains the row and column. If invalid, the root group is used.
 * @return        An index for the specified item, or an invalid index if it hasn't been found
 */
QModelIndex ContactListModel::index( int row, int column, const QModelIndex &parent ) const
{
  if( ! hasIndex( row, column, parent ) || ! rootNode_ )
  {
    return QModelIndex();
  }

  ContactListModelItem *parentItem;

  if( ! parent.isValid() )
  {
    parentItem = rootNode_;
  }
  else
  {
    parentItem = static_cast<ContactListModelItem*>( parent.internalPointer() );
  }

  ContactListModelItem *childItem = parentItem->child( row );
  if( childItem )
  {
    return createIndex( row, column, childItem );
  }
  else
  {
    return QModelIndex();
  }
}



/**
 * Obtain a model index's parent index
 *
 * @param index  Points to the item to find the parent of
 * @return       An index for the parent of the specified item, or an invalid index if it hasn't been found
 */
QModelIndex ContactListModel::parent( const QModelIndex &index ) const
{
  if( ! index.isValid() )
  {
    return QModelIndex();
  }

  ContactListModelItem *item = static_cast<ContactListModelItem*>( index.internalPointer() );

  if( ! item || item == rootNode_ )
  {
    return QModelIndex();
  }

  ContactListModelItem *parentItem = item->parent();

  if( ! parentItem || parentItem == rootNode_ )
  {
    return QModelIndex();
  }

  return createIndex( parentItem->row(), 0, parentItem );
}



/**
 * Apply to the model a contact's movement through lists and groups
 *
 * This method is called when a contact changes between two lists, for example from blocked to allowed;
 * and when changes group, for example when it is moved or copied from a group to another.
 * First it identifies lists and groups where the contact was and will be; then the changes are applied.
 * The contact's list items are then iteratively moved from the old to the new lists/groups, and any
 * remaining addition and removal is performed.
 *
 * If \arg contact is null, sender() will be used.
 * 
 * @param contact  The contact to update in the model
 */
void ContactListModel::contactMove( MsnContact *contact )
{
  if ( ! contact )
  {
    contact = qobject_cast<MsnContact*>(sender());
  }

  KMESS_NULL( contact );
  KMESS_NULL( rootNode_ );

  // Add the current groups to the nodes in which the contact should now be visible

  ModelItemList newNodes;
  QList<MsnGroup*> groups( contact->groups() );

  foreach( MsnGroup *group, groups )
  {
    ContactListModelItem *item = rootNode_->child( group );
    if( item )
    {
      newNodes << item;
    }
  }

  // Add the current lists to the nodes in which the contact should now be visible
  if( contact->isFriend() )
  {
    // Friends with no groups are displayed in the Individuals group
    if( groups.isEmpty() )
    {
      newNodes << rootNode_->child( specialGroups_.value( SpecialGroups::INDIVIDUALS ) );
    }

    newNodes << rootNode_->child( specialGroups_.value( contact->isOnline() ? SpecialGroups::ONLINE : SpecialGroups::OFFLINE ) );
  }
  else
  {
    if ( contact->isUnknown() )
    {
      newNodes << rootNode_->child( specialGroups_[ SpecialGroups::UNKNOWN ] );
    }
    else
    {
      newNodes << rootNode_->child( specialGroups_.value( contact->isAllowed() ? SpecialGroups::ALLOWED : SpecialGroups::REMOVED ) );
    }
  }

  ModelItemList allGroups( rootNode_->children() );
  foreach( ContactListModelItem *node, allGroups )
  {
    ContactListModelItem *contactItem = node->child( contact );

    // If new nodes doesn't contain current node, remove contact from it if it's in.
    if( ! newNodes.contains( node ) )
    {
      // If we found the contact, remove it
      if( contactItem )
      {
        const QModelIndex& groupIndex( index( node->row(), 0, QModelIndex() ) );
        KMESS_ASSERT( groupIndex.isValid() );
        if ( ! groupIndex.isValid() )
        {
          warning() << "MsnGroup does not exist in the contact list";
          continue;
        }

        beginRemoveRows( groupIndex, contactItem->row(), contactItem->row() );

        delete contactItem;

        endRemoveRows();
      }
    }
    else
    {
      if( ! contactItem )
      {
        const QModelIndex& groupIndex( index( node->row(), 0, QModelIndex() ) );

        beginInsertRows( groupIndex, node->childCount(), node->childCount() );

        new ContactListModelItem( contact, node );

        endInsertRows();
      }
      // else leave it
    }
  }
}



// Specify which drag and drop operations are supported
Qt::DropActions ContactListModel::supportedDropActions() const
{
  return (Qt::DropActions) ( Qt::CopyAction | Qt::MoveAction );
}



// Return the special groups
IndexedGroupsList ContactListModel::specialGroups() const
{
  return specialGroups_;
}



// Remove a group
void ContactListModel::groupRemove( const MsnGroup *group )
{
  ContactListModelItem *nodeToRemove = rootNode_->child( group );

  if( group == 0 || nodeToRemove == 0 )
  {
    warning() << "MsnGroup" << group->id() << "doesn't exist in the contact list.";
    return;
  }

  if( nodeToRemove->childCount() )
  {
    warning() << "MsnGroup" << group->id() << "was not empty!";
    return;
  }

  ContactListModelItem *groupNode = rootNode_->child( group );
  if ( ! groupNode )
  {
    warning() << "MsnGroup" << group->id() << "does not exist in the model!";
    return;
  }

  beginRemoveRows( QModelIndex(), groupNode->row(), groupNode->row() );

  // Remove the group
  delete nodeToRemove;

  // Signal the group has been removed.
  endRemoveRows();
}



// Update a group's details
void ContactListModel::groupUpdate( const MsnGroup* group )
{
  // Signal that the group has changed
  forwardDataChanged( rootNode_->child( group ) );
}



/**
 * Empty the contact list
 *
 * Deletes the contact list's contents and regenerates the special groups and the model.
 * When exiting, the regeneration part can be skipped.
 */
void ContactListModel::reset( bool restore )
{
  // Empty the model structure and reset it
  if( rootNode_ )
  {
    debug() << "Resetting model tree...";

    // Delete everything from the model tree
    int childItemsCount = rootNode_->childCount();

    emit layoutAboutToBeChanged();
    for( int row = 0; row < childItemsCount; row++ )
    {
      ContactListModelItem *item = rootNode_->child( row );
      if( item != 0 )
      {
        continue;
      }
      int childItemsCount2 = item->childCount();
      for( int row2 = 0; row2 < childItemsCount2; row2++ )
      {
        delete item->child( row2 );
      }
    }
    emit layoutChanged();

    // Delete the root node
    delete rootNode_;
    rootNode_ = 0;
  }

  // Do not recreate the groups
  if( ! restore )
  {
    return;
  }

  // Regenerate the model's root node, the only one which contains groups and contacts
  rootNode_ = new ContactListModelItem();

  // Add the special groups to the model
  foreach( const MsnGroup *group, specialGroups_ )
  {
    groupAdd( group );
  }

  KMESS_ASSERT( rootNode_->childCount() == specialGroups_.count() );
}



/**
 * Get the number of rows present in an index
 *
 * @param parent The index to count rows of
 * @return int
 */
int ContactListModel::rowCount( const QModelIndex &parent ) const
{
  ContactListModelItem *parentItem = 0;

  if( rootNode_ == 0 )
  {
    return 0;
  }
  if( ! parent.isValid() )
  {
    parentItem = rootNode_;
  }
  else
  {
    parentItem = static_cast<ContactListModelItem*>( parent.internalPointer() );
  }

  if( ! parentItem )
  {
    return 0;
  }

  return parentItem->childCount();
}

