/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Adam Goossens <fontknocker@kmess.org>                     *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file ptpglue.h
 */

#ifndef PTPGLUE_H
#define PTPGLUE_H

#include <QHash>
#include <KMess/Message>
#include <QStringList>

namespace KMess
{
  class MsnSession;
}

namespace KMessInternal
{

  class MsnSwitchboardConnection;
  
/**
 * This class is a "babysitter" for newly created switchboard invitations - those we receive
 * with a RNG.
 *
 * When a switchboard request is first received we have no way of knowing if it will be
 * used for chat sessions or if it will be used to transfer data. We can deduce what type
 * of switchboard by examining the first few MSGs to cross the switchboard.
 *
 * startSwitchboard() will create a new MsnSwitchboardConnection instance and begin the
 * connection and authorization process. Once authentication completes PtpGlue 
 * will listen for contact join/part messages and any incoming messages.
 *
 * The type of the first few messages to be received will determine the type of the switchboard connection.
 * A Data or Ptp message indicates a Data session; this causes newDataSession() to be emitted.
 *
 * Should a ClientCaps message arrive we can't yet deduce the type of connection, so we keep waiting.
 * 
 * Should a Typing message arrive we know it's a chat session, but we don't inform the user about chat
 * sessions until something "tangible" arrives (like a TextMessage or InkMessage, etc).
 * 
 * Should any other message arrive, we assume this is a chat session switchboard. That
 * will trigger the emitting of "newChatSession".
 *
 * To track an already-created switchboard, use takeOwnership(). The
 * manager will listen for the first incoming message and emit newChatSession/newDataSession
 * as appropriate.
 *
 * Interested parties who wish to take control of a switchboard connection away from the 
 * switchboard manager should call PtpGlue::release(), passing the connection
 * instance. From that point onward the manager is not responsible for the deletion of the connection.
 * Failure to do this will result in the switchboard manager trying to delete the connection
 * upon destruction - this may not be what you want!
 *
 * @author Adam Goossens (fontknocker@kmess.org)
 */
class PtpGlue : public QObject
{
  Q_OBJECT
  
  public:
    PtpGlue( KMess::MsnSession *session );
    ~PtpGlue();

  public slots:
    void    slotSwitchboardCreated( KMessInternal::MsnSwitchboardConnection *connection );

  private slots:
    void    slotMessageReceived( KMess::Message message );

  private:
    KMess::MsnSession                         *session_;
};
};

#endif
