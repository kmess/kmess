/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Valerio Pilo <valerio@kmess.org>                          *
 * (C) Copyright Sjors Gielen <sjors@kmess.org>                            *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file msnchat.cpp
 * @brief Chat session interface
 */

#ifdef __GNUC__
#warning All application-related code is gone after rev 5476. Revert to it to get it back when implementing it.
#warning applications_old and messages_old were deleted in rev 5486.
#endif

#include "debug/libkmessdebug.h"
#include <KMess/MsnChat>
#include <KMess/MsnContact>
#include <KMess/Self>
#include <KMess/MsnSession>
#include <KMess/Utils>
#include <KMess/ContactList>
#include <KMess/ClientCapsMessage>
#include <KMess/TextMessage>
#include <KMess/InkMessage>
#include <KMess/WinkMessage>
#include <KMess/NudgeMessage>
#include <KMess/TypingMessage>
#include <KMess/PtpDataMessage>
#include <KMess/Message>
#include <KMess/ContactList>
#include <KMess/MsnAlgorithms>
#include <KMess/NetworkGlobals>

#include "connections/msnnotificationconnection.h"
#include "connections/msnswitchboardconnection.h"
#include "soap/offlineimservice.h"
#include "listsmanager.h"
#include "ptpglue.h"
#include "switchboardmanager.h"

#include <math.h>

#include <QFile>
#include <QImageReader>

using namespace KMess;

/*
#include <Ptp/Platform>
#include <Ptp/TransportImpl>
#include <Ptp/SlpPeerToPeerEndpoint>
#include <Ptp/SwitchboardBridge>

using namespace PtpSignaling;
using namespace PtpTransport;
*/

/**
 * @brief The constructor
 *
 * @param session The MsnSession this MsnChat belongs to
 * @param handle The first person in this conversation.
 */
MsnChat::MsnChat( KMess::MsnSession *session, const QString &handle )
: QObject()
, switchboard_(0)
, firstContact_( handle )
, lastContact_( handle )
, session_( session )
{
  debug() << "Creating chat";

  offlineImService_ = new KMessInternal::OfflineImService( session_, this );

  // create one instance. keep it for life.
  createSwitchboardConnection();

  connect( offlineImService_,  SIGNAL( sendMessageFailed(QString,MimeMessage) ),
            this,              SIGNAL(     sendingFailed(QString,MimeMessage) ) );
}



/**
 * Creates a new MsnChat instance from a SwitchInfo structure.
 *
 * MsnChat retrieves its starting information from the SwitchInfo structure (starting handle,
 * current participants, switchboard connection, etc etc). This constructor is primarily
 * designed for creating MsnChat instances with switchboard connections that start
 * receiving chat messages.
 *
 * @param session The MsnSession this chat belongs to.
 * @param info The SwitchInfo structure describing initial chat parameters.
 */
MsnChat::MsnChat( KMess::MsnSession *session, KMessInternal::SwitchInfo *info )
: QObject()
, switchboard_( info->conn )
, participants_( info->participants )
, firstContact_( info->startingHandle )
, lastContact_( info->startingHandle )
, session_( session )
{
  debug() << "Creating chat from SwitchInfo. First contact:" << info->startingHandle << ", participants:" << info->participants;

  offlineImService_ = new KMessInternal::OfflineImService( session_, this );

  foreach( KMess::Message msg, info->messages )
  {
    if ( msg.type() != InvalidMessageType )
    {
      gotMessage( msg );
    }
  }

  MsnContact *c = session_->contactList()->contact( info->startingHandle );

  if ( c && c->network() == NetworkMsn )
  {
    createSwitchboardConnection();
  }

  connect( offlineImService_,  SIGNAL( sendMessageFailed(QString,MimeMessage) ),
            this,              SIGNAL(     sendingFailed(QString,MimeMessage) ) );
}

// The destructor
MsnChat::~MsnChat()
{
  debug() << "Destroying chat";

  if ( offlineImService_ )
  {
    delete offlineImService_;
  }

  // Cleanup pending messages
  pendingMessages_.clear();
}



/**
 * @brief Internally request a connection to MSN and connect
 *
 * This method will eventually result in either one of the signals
 * switchboardConnected() or switchboardConnectFailed() being emitted.
 */
void MsnChat::startConnecting()
{
  if( isConnected() )
  {
    return;
  }

  // don't need a switchboard for federated contacts.
  MsnContact *contact = session_->contactList()->contact( firstContact_ );
  if ( contact && contact->isOnline() && contact->network() == NetworkMsn )
  {
    emit requestSwitchboard( this );
  }

  // otherwise, it's an Offline IM session. don't get a SB session.
}



/**
 * If this chat is between you and a federated contact (such as Yahoo!), you
 * cannot invite contacts into the chat.
 *
 * To test for this condition, use this method.
 *
 * @return True if you can invite contacts into this chat.
 */
bool MsnChat::canInviteContacts() const
{
  MsnContact *c = session_->contactList()->contact( firstContact_ );

  KMESS_ASSERT( c );

  if ( c->network() != NetworkMsn )
  {
    return false;
  }

  return true;
}



/**
 * @brief Create and register a new switchboard
 *
 * This method creates a new switchboard connection and sets it for use as the
 * current switchboard connection for this chat.
 */
void MsnChat::createSwitchboardConnection()
{
  //KMESS_ASSERT( switchboard_ == 0 );

  debug() << "Starting new switchboard session";

  // Initialize a new switchboard connection
  if ( ! switchboard_ )
  {
    switchboard_ = new KMessInternal::MsnSwitchboardConnection(session_);
    session_->ptpGlue()->slotSwitchboardCreated(switchboard_);
  }

  // this is worth a fatal error IMO.
  if( ! switchboard_ )
  {
    fatal( "Couldn't initialize a new switchboard connection - out of memory");
    return;
  }

  // Connect the switchboard's signals
  connect( switchboard_, SIGNAL(                   disconnected()                               ),
           this,         SLOT  (          slotSwitchboardClosed()                               ) );
  connect( switchboard_, SIGNAL(        connectionAuthenticated()                               ),
           this,         SLOT  (      slotInviteInitialContacts()                               ) );
  connect( switchboard_, SIGNAL(                connectionReady()                               ),
           this,         SLOT  (           slotSwitchboardReady()                               ) );
  connect( switchboard_, SIGNAL(                  contactJoined(QString)                        ),
           this,         SLOT  (          slotContactJoinedChat(QString)                        ) );
  connect( switchboard_, SIGNAL(                    contactLeft(QString,bool)                   ),
           this,         SLOT  (            slotContactLeftChat(QString,bool)                   ) );
  connect( switchboard_, SIGNAL(                       deleteMe(KMessInternal::MsnSwitchboardConnection*) ),
           this,         SLOT  (         slotSwitchboardDeleted(KMessInternal::MsnSwitchboardConnection*) ) );
  connect( switchboard_, SIGNAL(               invitationFailed()                               ),
           this,         SLOT  (             handleFailedInvite()                               ) );

  // It's not the task of the switchboard to determine what type of message a chat message is.
  connect( switchboard_, SIGNAL(                receivedMessage(KMess::Message)                 ),
           this,         SLOT(                       gotMessage(KMess::Message)                 ) );

  connect( switchboard_, SIGNAL(        sendingFailed( KMess::Message, const QString& ) ),
           this,         SIGNAL(        sendingFailed( KMess::Message, const QString& ) ) );
  connect( switchboard_, SIGNAL(        sendingSucceeded( KMess::Message ) ),
           this,         SIGNAL(        sendingSucceeded( KMess::Message ) ) );
  /*
  connect( switchboard_, SIGNAL(            receivedChatMessage(QString,QString,QFont,QString)  ),
           this,         SLOT  (        slotReceivedChatMessage(QString,QString,QFont,QString)  ) );
  connect( switchboard_, SIGNAL(             receivedClientCaps(QString,QString,QString)        ),
           this,         SLOT  (         slotReceivedClientCaps(QString,QString,QString)        ) );
  connect( switchboard_, SIGNAL(        receivedEmoticonMessage(QString,QHash<QString,QString>) ),
           this,         SLOT  (    slotReceivedEmoticonMessage(QString,QHash<QString,QString>) ) );
  connect( switchboard_, SIGNAL(            receivedDataMessage(QString,MimeMessage)            ),
           this,         SLOT  (        slotReceivedDataMessage(QString,MimeMessage)            ) );
  connect( switchboard_, SIGNAL(            receivedDataMessage(QString,P2PMessage)             ),
           this,         SLOT  (        slotReceivedDataMessage(QString,P2PMessage)             ) );
  connect( switchboard_, SIGNAL(             receivedInkMessage(QString,QString,InkFormat)      ),
           this,         SLOT  (         slotReceivedInkMessage(QString,QString,InkFormat)      ) );
  connect( switchboard_, SIGNAL(              receivedMsnObject(QString,QString)                ),
           this,         SLOT  (          slotReceivedMsnObject(QString,QString)                ) );
  connect( switchboard_, SIGNAL(                  receivedNudge(QString)                        ),
           this,         SLOT  (              slotReceivedNudge(QString)                        ) );
  connect( switchboard_, SIGNAL(     receivedTypingNotification(QString)                        ),
           this,         SLOT  ( slotReceivedTypingNotification(QString)                        ) );
*/
  connect( switchboard_, SIGNAL(                  sendingFailed(const KMess::Message)            ),
           this,         SIGNAL(                  sendingFailed(const KMess::Message)            ) );
  connect( switchboard_, SIGNAL(                    chatWarning(KMess::ChatWarningType,KMess::MsnContact*)        ),
           this,         SIGNAL(                    chatWarning(KMess::ChatWarningType,KMess::MsnContact*)        ) );
  connect( switchboard_, SIGNAL(                  dataSent(const quint32)                       ),
           this,         SLOT  (                  switchboard_OnDataSent(const quint32)         ) );
  connect( switchboard_, SIGNAL(                  dataSendFailed(const quint32)                       ),
           this,         SLOT  (                  switchboard_OnDataSendFailed(const quint32)   ) );
#ifdef __GNUC__
#warning Will pending display pictures be a job for ChatMaster?
#endif
// Probably yes.
/*
  // If this switchboard is a background one, made to request a contact's picture,
  // we can remove the pending request from the list now.
  if( chatInfo.getType() == Chat-gone-Information::CONNECTION_BACKGROUND
  &&  pendingDisplayPictures_.contains( handle ) )
  {
    pendingDisplayPictures_.removeAll( handle );
  }
*/

}



/**
 * @brief Ask to destroy this object.
 *
 * The object will be deleted. QObject will automatically emit signals that
 * it's gone. The appropriate signals will also be emitted from
 * KMess::MsnSession, connect to chatDestroyed().
 */
void MsnChat::destroy()
{
  deleteLater();
}



/**
 * @brief Return the first contact in a chat.
 */
MsnContact *MsnChat::firstContact() const
{
  return session_->contactList_->contact( firstContact_ );
}



/**
 * @brief Handle a failed invite.
 * @todo Fix.
 */
void MsnChat::handleFailedInvite()
{
  if( participants_.isEmpty() )
  {
    debug() << "Failed to invite initial contact to chat, closing connection.";

    switchboard_->closeConnection();
  }
}



/**
 * @brief Invite a new contact into the switchboard server.
 */
void MsnChat::inviteContact( const QString &handle )
{
  MsnContact *c = session_->contactList()->contact( firstContact_ );
  if ( c->network() == NetworkYahoo )
  {
    // forget it - no inviting contacts into Yahoo! chats.
    warning() << "Cannot invite contacts into Yahoo! federated chats.";
    return;
  }

  if( switchboard_ == 0 || !switchboard_->isConnected() )
  {
    debug() << "Added pending invitation for " << handle;
    pendingInvitations_.append( handle );
    return;
  }

  switchboard_->inviteContact( handle );
}



/**
 * @brief This is an overloaded function.
 * @param contact Contact to be invited into this chat.
 */
void MsnChat::inviteContact( const KMess::MsnContact *contact )
{
  inviteContact( contact->handle() );
}



/**
 * @brief Invite the initial contacts into the switchboard server.
 */
void MsnChat::slotInviteInitialContacts()
{
  KMESS_ASSERT( sender() == switchboard_ );

  debug() << "Inviting initial contacts - " << firstContact_ << " and pending invitations:" << pendingInvitations_;

  // MPOP - invite ourselves as well so the other
  // endpoints get into the convo.
  if ( session_->self()->endpoints().count() > 1 )
  {
    debug() << "Inviting our other endpoints into the chat";
    switchboard_->inviteContact( session_->self()->handle() );
  }

  // Now call the other user to the conversation.
  switchboard_->inviteContact( firstContact_ );

  // If there are other pending invites, send them now
  foreach( const QString &handle, pendingInvitations_ )
  {
    switchboard_->inviteContact( handle );
  }
  pendingInvitations_.clear();
}



/**
 * Check if a certain contact is in the chat
 */
bool MsnChat::isContactInChat( const QString& handle ) const
{
  if( participants_.contains( handle ) )
  {
    return true;
  }

  if( participants_.count() == 0 && lastContact_ == handle )
  {
    return true;
  }

  return false;
}



/**
 * This is an overloaded function.
 * @param contact A contact to search for in this chat.
 */
bool MsnChat::isContactInChat( const MsnContact *contact ) const
{
  return isContactInChat( contact->handle() );
}



/**
 * Check if all contacts left
 */
bool MsnChat::isEmpty() const
{
  return participants_.empty();
}



// Check if only the given contact is in the chat
bool MsnChat::isPrivateChatWith(const QString& handle) const
{
  // Also check for last contact, contact can be re-invited to resume the
  // session. Previously, one contact was always left in the participants_
  // list.

  int count = participants_.count();

  if( count == 1 && participants_[0] == handle )
  {
    return true;
  }
  else if( count == 0 && lastContact_ == handle )
  {
    return true;
  }

  return false;
}



/**
 * Check if the switchboard is busy.
 */
bool MsnChat::isBusy() const
{
  if( !isConnected() )
  {
    return false;
  }

  return switchboard_->isBusy();
}



/**
 * Check if this chat is correctly connected to a switchboard server.
 * If not, you can call startSwitchboard() to connect it properly.
 *
 * @see isWaiting()
 * @see startSwitchboard()
 */
bool MsnChat::isConnected() const
{
  // we're connected if switchboard is valid, switchboard is connected and
  // the switchboard is ready for messages to travel across it.
  return ( switchboard_ != 0 &&
           switchboard_->isConnected() &&
           ( switchboard_->isReady() || switchboard_->isBusy() ) );
}



/**
 * Check if this chat is currently connecting to a switchboard server.
 *
 * @see isConnected()
 */
bool MsnChat::isWaiting() const
{
  if( switchboard_ == 0 )
  {
    return false;
  }

  return switchboard_->isReady();
}



void MsnChat::gotMessage( Message message )
{
  message.setChat( this );

  if ( message.type() == PtpDataMessageType )
  {
    PtpDataMessage datamessage(message);

    const QString email = session_->self()->handle();
    const QString destination = datamessage.destination();
    const QString destinationemail = destination.section(';', 0, 0);
    if (destinationemail != email)
    {
      debug() << "Got message not for us; destination=" << destination;
      return;
    }
/*
    RtcSignalingEndpoint *endpoint =
        PtpCollaboration::Platform::instance()->getOrCreateEndpoint(datamessage.peer()->handle());
    TransportImpl *transport = endpoint->getTransport();
    SwitchboardBridge * sbbridge = transport->getOrCreateSbBridge();

    sbbridge->fireDataReceived(datamessage.rawData());
*/
    return;
  }

  debug() << "Notifying listeners of a message arrival of type "
          << message.type();

  emit messageReceived( message );
}



void MsnChat::sbbridge_OnSignalDataToSend(const QByteArray& datachunk, qint32 *cookie)
{
  // Build the ptp data message
  PtpDataMessage message( this, session_->self(), DirectionOutgoing);

  // If the connection is ready, send the message
  if( isConnected() && ! participants_.empty() )
  {
    message.setRawData(datachunk);
    // FIXME Does handle contains email@host.com;{endpoint id}
    // for mpop support??
    message.setDestination(firstContact_);

    qDebug() << Q_FUNC_INFO << "Sending message of type '" << message.field("Content-Type") << "'.";

    // Send and store for acknowledgement.
    qint32 ack = switchboard_->sendMessage( message );
    if (cookie) *cookie = ack;
  }
}

void MsnChat::switchboard_OnDataSendFailed(const quint32 acknumber)
{
  qDebug() << Q_FUNC_INFO << "cookie=" << acknumber << endl;
/*
  // FIXME Does handle contains email@host.com;{endpoint id}
  // for mpop support??
  QString handle = firstContact();
  qDebug() << Q_FUNC_INFO << "notifying " << handle;
  RtcSignalingEndpoint *endpoint =
      PtpCollaboration::Platform::instance()->getOrCreateEndpoint(handle);
  TransportImpl *transport = endpoint->getTransport();
  SwitchboardBridge * sbbridge = transport->getOrCreateSbBridge();

  sbbridge->fireDataSendFailed(acknumber);
  */
}

void MsnChat::switchboard_OnDataSent(const quint32 acknumber)
{
  qDebug() << Q_FUNC_INFO << "cookie=" << acknumber << endl;
  /*
  QString handle = firstContact();
  qDebug() << Q_FUNC_INFO << "notifying " << handle;
  RtcSignalingEndpoint *endpoint =
      PtpCollaboration::Platform::instance()->getOrCreateEndpoint(handle);
  TransportImpl *transport = endpoint->getTransport();
  SwitchboardBridge * sbbridge = transport->getOrCreateSbBridge();

  sbbridge->fireDataSent(acknumber);
  */
}

#ifdef __GNUC__
#warning showMsnObject - wait for P2P stack.
#endif
#if 0
// Display an MSN Object that was received.
void MsnChat::showMsnObject( const QString &handle, const MsnObject &msnObject )
{
  QString fileName;
  QString emoticonCode;

  // Get the contact
  // Note I don't use msnObject.getCreator, because I don't want to trust the other client!
  ContactBase *contact = 0;
  Contact     *msnContact = 0;

  // Get msn object filename
  fileName = KMessConfig::instance()->getMsnObjectFileName( msnObject );

  // Handle the picture
  switch( msnObject.getType() )
  {
    case MsnObject::DISPLAYPIC:
      // Get the real MSN contact from the MSN contact list.
      // Picture is stored in the ContactExtension because it's not an official property of the contact (not stored server-side).
      msnContact = session_->getContactList()->getContact(handle);
      KMESS_NULL( msnContact );
      msnContact->getExtension()->setContactPicturePath( fileName );
#ifdef __GNUC__
#warning fix:
#endif
      // This is code from ChatMaster, which previously did display picture re-fetching.
      // However, it can no longer do the job, because the internals are switched to the network
      // library now. In other words: we also have to redesign who does this fetching.
      // Valerio and Sjors talked about a new better implementation of this (using
      // DownloadableObject and such things) and it sounds pretty tight, so maybe
      // that's going to be the design. Whatever we do, MsnChat can't do display picture
      // re-fetching, but it *does* do MsnObject stuff at this moment.
      //pendingDisplayPictures_.removeAll( handle );
      break;

    case MsnObject::WINK:
      // Show the Wink in the chat window where the original datacast message came from.
      KMESS_NULL( chat );
      //chat->showWink( handle, fileName, msnObject.getFriendly() );
      emit showWink( handle, fileName, msnObject.getFriendly() );
      break;

    case MsnObject::EMOTICON:
    {
      // Inform the contact that a custom emoticon can be parsed.
      contact = session_->contact(handle);
      KMESS_NULL( contact );
      emoticonCode = contact->getEmoticonCode(msnObject.getDataHash());  // always put before addEmoticonFile()!
      contact->addEmoticonFile( msnObject.getDataHash(), fileName );

      // Update all chats where the contact is present
#ifdef __GNUC__
#warning fix:
#endif
      // Can't do this too. A custom emoticon comes before the message it belongs to. Therefore,
      // not all chats should be updated, but only *the window where the message is going to be
      // displayed*. In other words, this should become internal and saved along with the ChatMessage
      // that contains / will contain the new custom emoticon, maybe with some EmoticonMessages
      // in it or whatever. Needs fixing.
      /*
      QList<Chat*> chats( getAllChats() );
      foreach( Chat *chatUpdate, chats )
      {
        if( chatUpdate->isContactInChat( handle ) )
        {
          chatUpdate->updateCustomEmoticon( handle, emoticonCode );
        }
      } */
    }
      break;

    case MsnObject::BACKGROUND:
      // handle background transfers

    case MsnObject::VOICECLIP:
      // handle voicelip transfers

    default:
      warning() << "Unable to handle MsnObject pictures of type '" << msnObject.getType() << "'.";
  }
}
#endif



/**
 * @brief Return the last contact in a chat
 */
MsnContact *MsnChat::lastContact() const
{
  return session_->contactList_->contact( lastContact_ );
}



/**
 * @brief Return a list of the contacts in the chat
 *
 * If nobody is in this chat, the last contact in it will be returned.
 */
QList<MsnContact*> MsnChat::participants() const
{
  // Note this object may contain no contacts at all, and lastContact_
  // has the last one to re-invite.

  QList<MsnContact*> list;
  if( participants_.isEmpty() )
  {
    list.append( session_->contactList_->listsManager()->contact( lastContact_, true /* create */ ) );

    return list;
  }

  foreach( const QString &handle, participants_ )
  {
    list.append( session_->contactList_->listsManager()->contact( handle, true /* create */) );
  }

  return list;
}



// Send a client caps message to the contacts
void MsnChat::sendClientCaps()
{
  // All third-party clients send this message.
  // It also makes debugging easier.
  ClientCapsMessage message ( this, 0 );

  // Check if we are logging chats or not
  QString appName( "libkmess-msn" );
  QString appVer ( LIBKMESS_MSN_VERSION );
  ChatLoggingMode appLogMode  = LoggingInactive;

  // overwrite with app-supplied details.
  if ( ! Utils::getClientName().isEmpty() )
  {
    appName    = Utils::getClientName();
    appVer     = Utils::getClientVersion();
    appLogMode = Utils::getClientLogging();
  }

  message.setClientName( appName );
  message.setClientVersion( appVer );
  message.setChatLoggingMode( appLogMode );

  sendMessage( message );
}



/* void MsnChat::sendMimeMessageWhenReady( MultiPacketMessage &message )
{
  while( ! message.isComplete() )
  {
    sendMimeMessageWhenReady( message.getNextPart() );
  }
} */



// Send a message to the contact(s), or leave it pending until a connection is restored
void MsnChat::sendMessage( Message &message )
{

  MsnContact *firstContact = session_->contactList()->contact( firstContact_ );

  // if this is a federated chat, send it over the NS.
  if ( firstContact && firstContact->network() != KMess::NetworkMsn )
  {
    message.setPeer( firstContact ); // necessary.
    session_->msnNotificationConnection_->sendUUM( message );
    return;
  }

  // If the connection is ready, send the message
  if( isConnected() && ! participants_.empty() )
  {
    debug() << "Sending message of type '" << message.field("Content-Type") << "'.";

    // Send and store for acknowledgement.
    switchboard_->sendMessage( message );

    // Clean cache of old entries, it does not need to run with every storeMessage..() call.
    switchboard_->cleanUnackedMessages();

    return;
  }

  // There's no active connection, store as pending message
  debug() << "Chat is not active, queueing message to send later.";
  debug() << "Status info -- Connected:" << isConnected();

  // Store the message
  pendingMessages_.append( message );

  // if we're still connected, but nobody is in the chat,
  // try to re-invite somebody.
  if ( isConnected() )
  {
    if ( message.hasField( "P2P-Dest" ) && message.field( "P2P-Dest" ) != lastContact_ )
    {
      lastContact_ = message.field( "P2P-Dest" );
    }

    if ( lastContact_.isEmpty() )
    {
      warning() << "Don't have last contact details; cannot restart switchboard.";
      return;
    }

    switchboard_->inviteContact( lastContact_ );
    return;
  }

  MsnContact *contact = session_->contactList()->contact( lastContact_ );

  if ( switchboard_->state() == KMessInternal::MsnSwitchboardConnection::StateConnecting )
  {
    // remember, opening the connection is not instant. we have to wait for
    // QAbstractSocket to inform the switchboard that the connection has been made.
    return;
  }

  if ( switchboard_->isConnected() &&
       switchboard_->state() != KMessInternal::MsnSwitchboardConnection::StateReady )
  {
    // keep waiting - we're probably authorizing or inviting initial contacts.
    return;
  }

  // not connected, not ready, not waiting for a connection.
  // is this is an offline session?
  if ( contact && ! contact->isOnline() )
  {
    // Send offline messages if the contact is not online
    if( lastContact_.isEmpty() )
    {
      warning() << "No last contact info: cannot find a contact to send offline messages to.";
      return;
    }

    // Only send chat messages
    if ( message.type() != TextMessageType )
    {
      debug() << "Ignoring message of type" << message.field("Content-Type") << "in an offline messaging session.";
      pendingMessages_.removeAll( message );
      return;
    }

    debug() << "Sending offline message to" << lastContact_;

    // Send the message (only the text contained in the body)
    offlineImService_->sendMessage( lastContact_, message.body() );

    // The message has been sent as an OIM, remove it to avoid it
    // being sent twice
    pendingMessages_.removeAll( message );

    // sent offline IM.
    return;
  }

  debug() << "Connection is closed, requesting a new chat from the notification server...";

  // We are not connected at all. Request a new switchboard session
  startConnecting();
}



// Send messages that weren't sent because a contact had to be re-called
void MsnChat::sendPendingMessages()
{
  // Do nothing if we are not connected.
  if( ! isConnected() || participants_.isEmpty() )
  {
    debug() << "Skipping message sending, the connection is not ready.";
    return;
  }

  // Send all messages
  for( int i = 0; i < pendingMessages_.count(); ++i )
  {
    debug() << "Sending pending messages, " << ( pendingMessages_.count() - i ) << " remaining.";

    // Send the message
    switchboard_->sendMessage( pendingMessages_[i] );
  }

  // Clean cache of old entries, it does not need to run with every storeMessage..() call.
  switchboard_->cleanUnackedMessages();

  // Clear the pending messages
  pendingMessages_.clear();

}



/**
 * Send a TextMessage to everyone in the chat.
 *
 * The message you wish to "say" is kept in message. The output
 * parameter, messageOut, should be set to a TextMessage instance that
 * you want to hold the TextMessage formed for the message sent.
 *
 * Example:
 * TextMessage msg;
 * msnChat->sendMessage( "Hello world!", &msg );
 *
 * @param message The message to "say" in the chat.
 * @param messageOut A pointer to a valid TextMessage instance that will store
 *                   the TextMessage that represents your message.
 * @param font The font for the message.
 * @param color The color for the message.
 */
void MsnChat::sendMessage( const QString &message, TextMessage *messageOut, QFont font, QColor color )
{
  TextMessage msg( this, 0 );
  msg.setFont( font );
  msg.setColor( color );
  msg.setMessage( message );

  MsnContact *peer = session_->self();

  if ( ! peer )
  {
    warning() << "Self MsnContact is invalid - creating a TextMessage that is not associated with an MsnContact.";
  }

  msg.setPeer( peer );

  if ( messageOut )
  {
    *messageOut = msg;
  }

  sendMessage( msg );
}


#if 0
/**
 * Send a message to the contact(s)
 *
 * If there is at least one custom emoticon in our message, also send a message to tell our contact's clients
 * that they have to download from us the corresponding pictures.
 */
void MsnChat::sendChatMessage( TextMessage &message )
{
  //int maxSendableSingleMessageLength = 1400;

  // Check if any custom emoticon is being sent in the message.
  // This has to be sent first so the receiving client will be aware that
  // there will be custom emoticons in the next message.
#ifdef __GNUC__
#warning do not do this, TextMessage has an EmoticonMessage inside of it
#endif
  // i.e. this code should all be moved to the client etc!
#if 0
  QString code, pictureFile;
  QString emoticonObjects;
  QStringList addedEmoticons;
  int lastPos                             = 0;
  int matchStart                          = 0;
  EmoticonManager *manager                = EmoticonManager::instance();
  const QRegExp&                  emoticonRegExp(   manager->getPattern( true ) );
  const QHash<QString,QString>& emoticonPictures( manager->getFileNames( true ) );
  const QString&               emoticonThemePath( manager->getThemePath( true ) );

  // We'll loop until we reach the end of the string or there are no more emoticons to parse
  if( ! emoticonRegExp.isEmpty() )
  {
    while( true )
    {
      // First find if there's any custom emoticon
      matchStart = emoticonRegExp.indexIn( text, lastPos );
      if( matchStart == -1 )
      {
        break;
      }

      // Find out what emoticon has matched
      code = text.mid( matchStart, emoticonRegExp.matchedLength() );

      // Find where the emoticon code ends and the image corresponding to that code
      lastPos = matchStart + emoticonRegExp.matchedLength();
      pictureFile = emoticonPictures[ code ];

      // Do not add emoticons to the list more than once
      if( addedEmoticons.contains( code ) )
      {
        continue;
      }

      // We cannot send more than 7 different custom emoticons in each message: skip the other ones.
      // TODO add some visual confirmation or message about this.
      if( addedEmoticons.count() >= 7 )
      {
        emit showWarning( WARNING_TOO_MANY_EMOTICONS, 0 );
        break;
      }

      addedEmoticons.append( code );

      // NOTE: Behavior changed since WLM 8+, emoticon data must be sent every time.
      // Before, we could send any emoticon's msnobject just once per session.

      // No match? Strange.. but go on anyways
      if( pictureFile.isEmpty() )
      {
        debug() << "Custom emoticon '" << code << "' not found!";

        continue;
      }

      debug() << "Found custom emoticon '" << code << "' which file is '"
                << ( emoticonThemePath + pictureFile ) << "'." << endl;

      QFile iFile( emoticonThemePath + pictureFile );
      if( ! iFile.open( QIODevice::ReadOnly ) )
     {
        debug() << "Unable to read picture '" <<  pictureFile << "'!";
        iFile.close();
        continue;
      }

      // Read the file and create an MSNObject of the emoticon
      const QByteArray& data( iFile.readAll() );
      iFile.close();
      MsnObject test( session_->sessionSettingString( "AccountHandle" ), pictureFile, QString(), MsnObject::EMOTICON, data );

      // Only divide items between each other
      if( ! emoticonObjects.isEmpty() )
      {
        emoticonObjects += "\t";
      }

      emoticonObjects += code + "\t" + test.objectString();
    }
  }

  // Don't send the message if there is no emoticon to send
  if( ! emoticonObjects.isEmpty() )
  {
    MimeMessage emoticonMessage;
    emoticonMessage.addField("MIME-Version",    "1.0");
    emoticonMessage.addField("Content-Type",    "text/x-mms-emoticon");
    emoticonMessage.setBody( emoticonObjects );

    sendMimeMessageWhenReady( MsnConnection::ACK_NAK_ONLY, emoticonMessage );
  }
#endif

  // Then send the real text message

  // Get the formatting properties and convert them to MSN's IM format
#ifdef __GNUC__
#warning get the font correctly? todo implement: MsnChat::setFont();
#endif
  const QFont&         font( message.font() );
  const QString& fontFamily( QUrl::toPercentEncoding( font.family() ).left( 31 ) ); // Font name cannot be larger than 31 chars
  const QString& color     ( KMess::MsnAlgorithms::convertHtmlColorToMsnColor( message.color().name() ) );

  // Determine effects
  QString effects;
  if( font.bold()      ) effects += 'B';
  if( font.italic()    ) effects += 'I';
  if( font.underline() ) effects += 'U';

  // Determine text direction
#ifdef __GNUC__
#warning todo:
#endif
  //const QString& rtl( message.isRightToLeft() ? "; RL=1" : "" );

  // Create the message
#ifdef __GNUC__
#warning do we need to do this, or does TextMessage do this for us?
#endif
  //message.setField("X-MMS-IM-Format", "FN=" + fontFamily + "; EF=" + effects + "; CO=" + color + "; CS=0; PF=0" + rtl);

  // Send the message
#ifdef __GNUC__
#warning do we need to do this, or does Message do this for us?
#endif
#if 0
  int bodyLength = text.toUtf8().length();
  if( bodyLength > maxSendableSingleMessageLength )
  {
    // Check if the remote clients support the recieving of huge messages.
    bool capable = true;
    ContactBase *contact;
    foreach( const QString &handle, participants_ )
    {
      contact = session_->contact( handle );
      if( ! contact->hasCapability( MSN_CAP_MULTI_PACKET ) )
      {
        capable = false;
        break;
      }
    }

    if( capable )
    {
      // Create a multipacket message
      MultiPacketMessage multiMessage( message );
      sendMimeMessageWhenReady( MsnConnection::ACK_NAK_ONLY, multiMessage );
    }
    else
    {
      // Split the huge message and send all parts
      int pos = 0;
      int prevPos = 0;
      QString messagePart;
      const QString& body( message.getBody() );

      int parts = (int) ceil( ( (float) bodyLength / (float) maxSendableSingleMessageLength ) );

      for( int i = 0; i < parts - 1; i++ )
      {
        prevPos = pos;
        pos += maxSendableSingleMessageLength;

        // search backwards from the new split position.
        int spacePos = body.lastIndexOf( " ", pos );

        messagePart = body.mid( prevPos, pos - prevPos );

        if( spacePos > prevPos )
        {
          // use that one!
          pos++;
        }

        // send messagePart
        sendMimeMessageWhenReady( MsnConnection::ACK_NAK_ONLY, message );
      }

      messagePart = body.mid( pos, body.length() - pos );

      sendMimeMessageWhenReady( MsnConnection::ACK_NAK_ONLY, message );
    }
  }
  else
  {
#endif
    // Send the simple MimeMessage
    sendMessage( message );
  //}
}
#endif


void MsnChat::sendInk( InkMessage &message )
{
  bool capable = true;
  MsnContact *contact;

  // Verify whether all contacts are capable to view this Ink message or not
  foreach( const QString &handle, participants_ )
  {
    contact = session_->contactList()->contact( handle );
    if( !contact->supportsInkType( message.format() ) )
    {
      capable = false;
      debug() << "Contact" << handle << "does not support Ink with type = " << message.format();
      break;
    }
  }

  if( ! capable )
  {
    emit chatWarning( WARNING_INK_UNSUPPORTED_BY_CONTACT, contact );

    // If the only contact in chat doesn't support it, don't send it at all.
    if( participants_.count() == 1 )
    {
      debug() << "None of the contacts support the wink, aborting.";
      return;
    }
  }

  sendMessage( message );

}



/**
 * Send an ink message to all members of this chat.
 *
 * The inkData parameter must contain the raw ISF data that describes the ink.
 *
 * @param format  The ink format which is used for transfer.
 * @param inkData The raw ISF ink data to send to the chat.
 */
void MsnChat::sendInk( KMess::InkFormat format, QByteArray &inkData )
{
  InkMessage msg( this, 0, format );
  msg.setData( inkData );

  sendInk( msg );
}



// Send a nudge to a contact
void MsnChat::sendNudge()
{
#ifdef __GNUC__
  #warning TODO: Nudges to group chats?
#endif

  NudgeMessage nudge( this, 0 );
  sendMessage( nudge );
}



// The user is typing so send a typing message
void MsnChat::sendTypingMessage()
{
  if( participants_.isEmpty() )
  {
    if( lastContact_.isEmpty() )
    {
      debug() << "Not sending typing notification, no contacts in chat, nor one to re-invite.";
      return;
    }


    // When we need to re-invite a contact to send the typing message, see if the contact is offline
    // This avoids repeated "the contact is offline" typing messages.
    MsnContact *contact = session_->contactList()->contact( lastContact_ );
    if( contact == 0 || ! contact->isOnline() )
    {
      debug() << "Not sending typing notification, last contact is offline.";
      return;
    }
  }

  debug() << "Sending typing notification.";

  // Build the typing notification message
  TypingMessage message( this, session_->self() );

  // if disconnected, reconnect to send the typing message
  // (maybe the other contact still has his window open)
  sendMessage( message );
}



// Send wink
void MsnChat::sendWink( WinkMessage &message )
{
  sendMessage( message );
}



#ifdef __GNUC__
#warning sendPendingMessages - todo
#endif
#if 0
/**
 * @brief Send all pending mime messages for the contact.
 * @param handle The contact to send messages for.
 */
void MsnChat::sendPendingMessages( const QString &handle )
{
  KMESS_ASSERT( switchboard_ != 0 );

  debug() << "Sending pending messages for"
           << ( handle.isEmpty() ? "all contacts" : handle ) << "- if any.";

  QList<PendingMimeMessage*> sentMessages;

  bool isPrivateChat = (participants_.size() < 2);

  foreach( PendingMimeMessage *pendingMimeMessage, pendingMimeMessages_ )
  {
    // Ignore messages for other contacts
    if( ! handle.isEmpty()
    &&    pendingMimeMessage->handle != handle )
    {
      // Get next message
      continue;
    }

    // Ignore messages that can only be sent on a private chat.
    if( ! handle.isEmpty()
    &&    pendingMimeMessage->privateChatRequired && ! isPrivateChat )
    {
      debug() << "Switchboard is not a private chat, keeping message in queue.";

      // Get next message
      continue;
    }

    // Check when the connection is busy again
    if( switchboard_->isBusy() )
    {
      debug() << "Switchboard is busy again, keeping remaining messages in the queue.";
      goto cleanup_return;
    }

    // Check whether the contact is still in the chat.
    if( ! handle.isEmpty()
    &&  ( ! isConnected() || ! participants().contains( handle ) ) )
    {
      // getContactsInChat() may be empty, don't re-invite the last contact to deviver the message
      debug() << "Switchboard is not connected or contact left chat, keeping remaining messages in the queue.";
      goto cleanup_return;
    }

    // Send the message
    sendApplicationMessage( pendingMimeMessage->message );
    sentMessages.append( pendingMimeMessage );
  }

cleanup_return:
  // Do deletion out of the loop, just to be sure.
  foreach( PendingMimeMessage *sentMessage, sentMessages )
  {
    pendingMimeMessages_.removeAll( sentMessage );
    delete sentMessage;
  }
}
#endif



// Change the switchboard connection used by the chat window.
// TODO remove!
#ifdef __GNUC__
#warning get switchboard switching working right using this (to-be-private) method
#endif
#if 0
void MsnChat::setSwitchboardConnection( MsnSwitchboardConnection *newConnection )
{
  // Everything is connected already.
  if( getSwitchboardConnection() == 0 )
  {
    debug() << "Switchboard was not set up.";
  }
  else
  {
    debug() << "Switchboard was "
              << ( getSwitchboardConnection() == newConnection ? "already set" : "different" ) << "." << endl;
  }

  if( getSwitchboardConnection() != 0 )
  {
    // Notify that all contacts have left the chat
    if( ! chat_->isEmpty() )
    {
      const QStringList &participants( participants() );

      debug() << "Removing old switchboard's participants from the chat:" << participants;
      foreach( const QString &handle, participants )
      {
        ContactBase* contact = session->getContact( handle );
        if( contact == 0 )
        {
          contact = globalSession->addInvitedContact( handle );
        }

        // NOTE: This slot doesn't use the second parameter
        contactsWidget_->contactLeft( contact, false );
      }
    }

    // Delete all signals between us and the old switchboard
    disconnect( this, 0, getSwitchboardConnection(), 0 );
    disconnect( getSwitchboardConnection(), 0, contactsWidget_, 0 );
    disconnect( getSwitchboardConnection(), 0, this, 0 );

    // We are switching switchboards, clean up
    if( getSwitchboardConnection() != newConnection )
    {
      // Some info of the previous switchboard must be recorded to allow
      // retrieving details about the chat when no switchboard is present
      lastKnownContacts_ = chat_->participants();

      // Also delete the old connection
      getSwitchboardConnection()->deleteLater();
    }
  }

  getSwitchboardConnection() = newConnection;

  // The old connection has been removed, we're done
  if( newConnection == 0 )
  {
    return;
  }

  // Make the new signals connections
  connect( getSwitchboardConnection(), SIGNAL(        contactJoinedChat(ContactBase*)              ),
           contactsWidget_,           SLOT  (            contactJoined(ContactBase*)              ) );
  connect( getSwitchboardConnection(), SIGNAL(          contactLeftChat(ContactBase*,bool)         ),
           contactsWidget_,           SLOT  (              contactLeft(ContactBase*,bool)         ) );
  connect( getSwitchboardConnection(), SIGNAL(            contactTyping(ContactBase*)              ),
           contactsWidget_,           SLOT  (            contactTyping(ContactBase*)              ) );

  connect( getSwitchboardConnection(), SIGNAL(        contactJoinedChat(ContactBase*)              ),
           this,                      SLOT  (            contactJoined(ContactBase*)              ) );
  connect( getSwitchboardConnection(), SIGNAL(          contactLeftChat(ContactBase*,bool)         ),
           this,                      SLOT  (              contactLeft(ContactBase*,bool)         ) );
  connect( getSwitchboardConnection(), SIGNAL(            contactTyping(ContactBase*)              ),
           this,                      SLOT  (            contactTyping(ContactBase*)              ) );
  connect( getSwitchboardConnection(), SIGNAL(              chatMessage(ChatMessage)               ),
           this,                      SLOT  (          receivedMessage(ChatMessage)               ) );
  connect( getSwitchboardConnection(), SIGNAL(            receivedNudge(ContactBase*)              ),
           this,                      SLOT  (        slotReceivedNudge(ContactBase*)              ) );
  connect( getSwitchboardConnection(), SIGNAL(            sendingFailed(QString,MimeMessage)       ),
           this,                      SLOT  (        slotSendingFailed(QString,MimeMessage)       ) );
  connect( getSwitchboardConnection(), SIGNAL(              showWarning(KMessInternal::MsnSwitchboardConnection::WarningType,ContactBase*) ),
           this,                      SLOT  (              showWarning(KMessInternal::MsnSwitchboardConnection::WarningType,ContactBase*) ) );
  connect( getSwitchboardConnection(), SIGNAL(                 deleteMe(KMessInternal::MsnSwitchboardConnection*) ),
           this,                      SLOT  ( setSwitchboardConnection()                          ) );


  // Add to the contacts widget all the contacts that are present in the new switchboard
  const QStringList &participants( participants() );
  debug() << "Adding new switchboard's participants to the chat:" << participants;
  foreach( const QString &handle, participants )
  {
    ContactBase *contact = globalSession->getContact( handle );
    if( contact == 0 )
    {
      contact = globalSession->addInvitedContact( handle );
    }

    contactJoined( contact, true /* don't notify about the join */ );
    contactsWidget_->contactJoined( contact );
  }
}
#endif



/**
 * @brief The switchboard closed.
 *
 * This method cleans up internal state now that the switchboard connection is
 * gone. The method emits disconnected(), which can be used to tell the MsnChat
 * to reconnect.
 */
void MsnChat::slotSwitchboardClosed()
{
/*
  const QString handle = participants_.first();

  RtcSignalingEndpoint *endpoint =
      PtpCollaboration::Platform::instance()->getOrCreateEndpoint(handle);
  TransportImpl *transport = endpoint->getTransport();
  SwitchboardBridge * sbbridge = transport->getOrCreateSbBridge();
  // Disconnect the signal/slot
  QObject::disconnect(sbbridge, 0, this, 0);
  qDebug() << Q_FUNC_INFO << "ssbridge should disconnect." << endl;
  // Disconnect the switchboard bridge.
  sbbridge->disconnect();
*/

  // When closing the session, the contact list may already be gone
  if( session_->contactList() )
  {
    // If there are still contacts, it means contactLeft() was not initiated,
    // and the user closed the chat window earlier.
    if( participants_.count() > 0 )
    {
      // Keep a default for re-connecting.
      lastContact_ = participants_[0];

      // Make sure all contacts are removed, which will also abort their applications in ApplicationList.
      // going in reverse means the lastContact_ set above is always the last to be removed.
      for( int i = participants_.count() - 1; i >=0; i-- )
      {
        emit contactLeft( session_->contactList()->contact( participants_[i] ), false /* not idle timeout */);
      }

      participants_.clear();
    }
  }


  emit disconnected();
}



// The switchboard is being destroyed
void MsnChat::slotSwitchboardDeleted( KMessInternal::MsnSwitchboardConnection *connection )
{
  KMESS_ASSERT( connection == switchboard_ );

  switchboard_ = 0;
}



#if 0
// Determine what to do when a contact changed it's picture.
void MsnChat::slotContactChangedMsnObject( QString handle )
{
  // Avoid trying to download our own display picture, if the user has added him/herself;
  // instead simply use the current one.
  // This method can be called to update the user's own picture in list from two places:
  // 1)on login when the contact is first received, 2)when the user changes the display pic.
  const QString &currentHandle = session_->sessionSettingString( "AccountHandle" );
  if( handle.isEmpty() )
  {
    handle = currentHandle;

    // When the connection starts, this method gets called
    // but the contact list is not ready yet. Cancel.
    if( handle.isEmpty() )
    {
      return;
    }
  }

  if( handle == currentHandle )
  {
#ifdef __GNUC__
#warning fix:
#endif
    /*
    contact->getExtension()->setContactPicturePath( Account::connectedAccount->getDisplayPicture() ); */
    debug() << "Updated user's own display picture";
    return;
  }

  // If the contact no longer likes to display an msn object, remove it
#ifdef __GNUC__
#warning fix:
#endif
  /*
  if( contact->getMsnObject() == 0 )
  {
    debug() << "Contact '" << handle << "' removed its MSN object";

    contact->getExtension()->setContactPicturePath( QString() );
    return;
  }

  MsnObject msnObject( *contact->getMsnObject() );

  // See if the file already exists.
  QString objectFileName( KMessConfig::instance()->getMsnObjectFileName( msnObject ) );
  if( QFile::exists( objectFileName ) )
  {
    bool imageInvalid = QImageReader::imageFormat( objectFileName ).isEmpty();
    if( ! imageInvalid && msnObject.verifyFile( objectFileName ) )
    {
      debug() << "Picture for '" << handle << "' is already downloaded.";

      contact->getExtension()->setContactPicturePath( objectFileName );
      return;
    }
    else
    {
    debug() << "Picture for '" << handle
              << "' is already downloaded, but corrupt "
              << ( imageInvalid ? "(detected by QImageReader)" : "(detected by MsnObject)" )
              << ", deleting it: '" << objectFileName << "'" << endl;
      QFile::remove( objectFileName );

      // If the file was set as picture. reset right away.
      if( contact->getExtension()->getContactPicturePath() == objectFileName )
      {
        warning() << "Corrupt picture was already set, "
                    << "resetting contact picture." << endl;
        contact->getExtension()->setContactPicturePath( QString() );
      }
    }
  }

  // If the contact is active in a chat, download the new picture straight away.
  KMess::MsnChat *msnchat = getExclusiveChatWith( handle );
  if( msnchat != 0 )
  {
    debug() << "Contact '" << handle << "' changed its MSN object.";

    // Automatically decides which switchboard is the best to use,
    // this may change during the transfer.
    startMsnObjectDownload( handle, &msnObject, 0 );
    return;
  }


  // Queue a request to download it later in the background.
  // That method will check for the prerequisites to download it,
  // since that may change between this point and the timedUpdate() call.

  debug() << "Contact '" << handle << "' changed its MSN object, queueing a request for it.";

  // Request the contact's msnobject
  if( ! pendingDisplayPictures_.contains( handle ) )
  {
    pendingDisplayPictures_.append( handle );
  } */
}
#endif



/**
 * @brief called when the switchboard is going to close.
 * @todo remove.
 *
void MsnChat::switchboardCloseLater( bool autoDelete )
{
  ContactBase *contact;
  bool hasAbortingApplications = false;

  // this method is called when the user wants to close the chat window (allowing everything to close nicely).
  // If there are no contacts in the chat, we can close directly.
  if( ! participants_.isEmpty() )
  {
    // There are still contacts.
    // Verify whether they still have applications running.
    // Allow those applications to abort, and report back when the connection can be closed.
    QList<ContactBase*> contactsToRemove;
    foreach( const QString &handle, participants_ )
    {
      contact = globalSession->getContact( handle );
      KMESS_NULL(contact);
      if( contact->hasApplicationList() )
      {
        ApplicationList *appList = contact->getApplicationList();
        bool aborting = appList->contactLeavingChat(this, true);
        if( aborting )
        {
          connect(appList, SIGNAL(     applicationsAborted(QString) ),
                  this,    SLOT  ( slotApplicationsAborted(QString) ) );
          hasAbortingApplications = true;
        }
        else
        {
          contactsToRemove.append(contact);
        }
      }
    }

    // All contacts that don't need any aborting are removed now (not in the iterator loop)
    // The remaining ones are removed in slotApplicationsAborted().
    foreach( ContactBase *contact, contactsToRemove )
    {
      participants_.removeAll( contact->getHandle() );
      contact->removeSwitchboardConnection( switchboard_, true );
    }
  }
  if( hasAbortingApplications )
  {
    // Wait for all applications to abort.
    // Set variables to use in slotApplicationsAborted.
    abortingApplications_ = true;
  }
  else
  {
    // No applications are aborting.
    // Close the connection directly
    switchboard_->closeConnection();

    // Automatically delete ourselves
    if( autoDelete )
    {
      switchboard_->deleteLater();
    }
  }
}
*/



/**
 * @brief Called when a contact joined the conversation.
 * @todo Rename.
 */
void MsnChat::slotContactJoinedChat( const QString &handle )
{
  KMESS_ASSERT( sender() == switchboard_ );

  if ( participants_.contains( handle ) )
  {
    debug() << "Received multiple JOI for" << handle << "; ignoring.";
    return;
  }

  participants_.append( handle );

  // Inform the contact about our version
  sendClientCaps();
/*
  const QString address = handle.section(";", 0, 0);
  const QString email = session_->self()->handle();
  if (address != email)
  {
    RtcSignalingEndpoint *endpoint =
        PtpCollaboration::Platform::instance()->getOrCreateEndpoint(handle);
    TransportImpl *transport = endpoint->getTransport();
    SwitchboardBridge * sbbridge = transport->getOrCreateSbBridge();
    // Connect the signal/slot
    QObject::connect(sbbridge, SIGNAL(sendDataChunk(const QByteArray&, qint32 *)), this,
                     SLOT(sbbridge_OnSignalDataToSend(const QByteArray&, qint32 *)));
    qDebug() << Q_FUNC_INFO << "ssbridge should connect." << endl;
    // Connect the switchboard bridge.
    sbbridge->connect();
  }
*/
  if( pendingMessages_.count() > 0 )
  {
    // Send all pending mime messages, unless there are
    // multiple participants
    // TODO: do we really want to make this check? If there are
    // pending messages, why only send the, if there is only 1 contact?
    if( participants().size() > 1 )
    {
      debug() << "This chat with" << handle << "is a multi-chat, not sending pending messages.";
    }
    else
    {
      debug() << handle << "joined this chat, sending pending messages...";

      sendPendingMessages();
    }
  }

#ifdef __GNUC__
#warning TODO-P2P: MsnObject download of contacts who have joined chat.
#endif
  // TODO how and where do we do this (probably not automatically?):
  /*
  // Check whether the contact picture is outdated.
  // Use getContact() from the MSN contact list to get the msn object.
  const Contact *fullContact = globalSession->getContactList()->getContact( handle );
  if( fullContact != 0 && fullContact->hasP2PSupport() )
  {
    if( fullContact->getMsnObject() != 0 )
    {
      startMsnObjectDownload( fullContact->getHandle(), fullContact->getMsnObject(), 0 );
    }
  }
  */

  // inform the application.
  emit contactJoined( session_->contactList()->listsManager()->contact( handle, true /* create */ ) );
}



/**
 * @brief A contact left the chat.
 * @todo fix.
 */
void MsnChat::slotContactLeftChat( const QString &handle, bool isChatIdle )
{
  // Check if the contact is in the chat..
  participants_.removeAll( handle );

  // update the last contact.
  if ( lastContact_ == handle && participants_.count() > 0)
  {
    lastContact_ = participants_[0];
  }

  // Check if all contacts went away
  if( participants_.count() == 0 )
  {
    // Store contact to have a default when re-connecting.
    lastContact_ = handle;

    // NOTE: Behavior changed since WLM 8+, emoticon data must be sent every time.
    // Reset the list of sent emoticons, so they will be sent again if the chat restarts
    // sentEmoticons_.clear();

    debug() << "last contact left chat, closing connection.";
    // No contacts left in the chat, close connection since it is of little use.
    // - the connection can still be used to 'CAL' the last contact again
    // - when the contact resumes the connection, it uses a different server,
    //   so startChat() needs to reconnect.

    // close the switchboard connection.
    switchboard_->closeConnection();
  }
/*
  const QString address = handle.section(";", 0, 0);
  const QString email = session_->self()->handle();
  if (address != email)
  {
    RtcSignalingEndpoint *endpoint =
        PtpCollaboration::Platform::instance()->getOrCreateEndpoint(handle);
    TransportImpl *transport = endpoint->getTransport();
    SwitchboardBridge * sbbridge = transport->getOrCreateSbBridge();
    // Disconnect the signal/slot
    QObject::disconnect(sbbridge, 0, this, 0);
    qDebug() << Q_FUNC_INFO << "ssbridge should disconnect." << endl;
    // Disconnect the switchboard bridge.
    sbbridge->disconnect();
  }
*/
  emit contactLeft( session_->contactList()->contact( handle ), isChatIdle );
}



/**
 * The switchboard is ready to send more messages.
 */
void MsnChat::slotSwitchboardReady()
{
  KMESS_ASSERT( switchboard_ != 0 );


  // Send all pending messages
  sendPendingMessages();

  // See if the switchboard is still ready.
  // Don't use connection->isBusy() as it will break with the test above when
  // exactly all messages are sent before the connection became busy.
  if( ! pendingMessages_.isEmpty() )
  {
    debug() << "Switchboard is busy again, not notifying applications.";
    return;
  }

  // A switchboard is still available.
  // If the contact applications were paused, resume them now.
  // TODO get a correct way to get an application list
//   ApplicationList *list = session_->applicationMaster()->getApplicationList( getParticipants().at(0) );
//   list->resumeApplications( getParticipants().size() < 2 );
}



// The requested chat session is available, connect to it
void MsnChat::initiateSession( const QString &server, const quint16 &port, const QString &authorization )
{
  // don't recreate it if it already exists
  if ( switchboard_ == 0 )
  {
    createSwitchboardConnection();
  }

  if( switchboard_ == 0 )
  {
    warning() << "Switchboard creation failed: unable to start the session!";
    return;
  }

  if ( switchboard_->isConnected() )
  {
    warning() << "Switchboard is already connected! Disconnecting first.";
    switchboard_->closeConnection();
  }

  // Initialize the switchboard as a user-requested chat
  switchboard_->start( server, port, authorization );

  emit switchboardCreated();
}



/**
 * Received a chat session invitation.
 */
void MsnChat::invitedToChat( const QString &server, const quint16 port, const QString &authorization, const QString &chatId )
{
  if( switchboard_ == 0 )
  {
    warning() << "Switchboard creation failed: unable to respond to invitation!";
    return;
  }

  if ( switchboard_->isConnected() )
  {
    warning() << "Switchboard is already connected! Disconnecting first.";
    switchboard_->closeConnection();
  }

  // Initialize the switchboard as a contact-requested chat
  switchboard_->start( server, port, authorization, chatId );

  emit switchboardCreated();
}


