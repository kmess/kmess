/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Valerio Pilo <valerio@kmess.org>                          *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file contactlistmodelitem.cpp
 */

#include "debug/libkmessdebug.h"

#include <KMess/MsnContact>
#include <KMess/MsnGroup>
#include <KMess/NetworkGlobals>
#include <KMess/ContactListModelItem>

using namespace KMess;



/**
 * Root constructor
 *
 * Creates a root tree node. It can contain only groups.
 */
ContactListModelItem::ContactListModelItem()
 : parentNode_( 0 )
 , object_( 0 )
 , itemType_( ItemRoot )
{
}



/**
 * MsnContact constructor
 *
 * Creates a contact tree leaf. It cannot contain anything.
 */
ContactListModelItem::ContactListModelItem( const MsnContact *contact, ContactListModelItem *parent )
 : parentNode_( parent )
 , object_( contact )
 , itemType_( ItemContact )
{
  // Add this item as a child of the parent
  parent->appendChild( this );
}



/**
 * MsnGroup constructor
 *
 * Creates a group tree node. It can only contain contacts.
 */
ContactListModelItem::ContactListModelItem( const MsnGroup *group, ContactListModelItem *parent )
 : parentNode_( parent )
 , object_( group )
 , itemType_( ItemGroup )
{
  // Add this node as a child of the parent
  parent->appendChild( this );
}



/**
 * Destructor
 *
 * Since the objects associated to the items are directly managed by ListsManager, their deletion
 * is performed there.
 */
ContactListModelItem::~ContactListModelItem()
{
  qDeleteAll( childItems_ );
  childItems_.clear();

  // Delete this item from the parent's children
  if( parentNode_ )
  {
    parentNode_->removeChild( this );
  }
}



/**
 * Add a new child item to this one
 *
 * Insert the given item as a child of this node. A root item can only contain Groups, Groups can only
 * contain Contacts. There cannot be duplicate items within the same parent.
 *
 * @param item  Item to add as child
 */
void ContactListModelItem::appendChild( ContactListModelItem *item )
{
  // Only add groups to the root, and contacts to groups
  if( ( itemType_ == ItemRoot    && item->type() != ItemGroup )
  ||  ( itemType_ == ItemGroup   && item->type() != ItemContact )
  ||    itemType_ == ItemContact )
  {
    warning() << "Cannot insert this item:" << item << "into this one:" << this;
    return;
  }

  // Also, don't add items which are already present
  if( childItems_.contains( item ) )
  {
    warning() << "Tried to add twice item:" << item;
    return;
  }

  childItems_.append( item );
}



/**
 * Return the row-th child of this item
 *
 * @param row  Requested row
 * @return Item at the requested position, or a null pointer
 */
ContactListModelItem *ContactListModelItem::child( int row )
{
  if( row < 0 || row >= childItems_.count() )
  {
    return 0;
  }

  return childItems_.at( row );
}



/**
 * Return a child MsnContact item
 *
 * This method can only be called on a MsnGroup node.
 *
 * @param contact The contact to find
 * @return        Item of the requested contact, or null
 */
ContactListModelItem *ContactListModelItem::child( const MsnContact *contact )
{
  // Prevent incorrect usage
  if( itemType_ != ItemGroup )
  {
    warning() << "Called childContact() on a non-group item:" << this;
    return 0;
  }

  foreach( ContactListModelItem *item, childItems_ )
  {
    if(    item->type() == ItemContact
        && item->object()  == contact )
    {
      return item;
    }
  }

  return 0;
}



/**
 * Return all items in the tree matching a certain contact
 *
 * This method is made so that contacts added in different groups are all found (for example,
 * a MsnContact may be in the Online group and in an user-defined group too).
 * You cannot call this method from a MsnContact leaf. Calling it from a MsnGroup node will return
 * a list with 0 or 1 items, depending if the contact was found or not.
 *
 * @param contact Contact to find
 * @return List of items with the requested MsnContact
 */
ModelItemList ContactListModelItem::children( const MsnContact *contact )
{
  ModelItemList list;

  if( itemType_ == ItemRoot )
  {
    foreach( ContactListModelItem *item, childItems_ )
    {
      list << item->children( contact );
    }
  }
  else if( itemType_ == ItemGroup )
  {
    foreach( ContactListModelItem *item, childItems_ )
    {
      if( item->type() != ItemContact )
      {
        warning() << "Invalid item in group" << this << ":" << item;
        return ModelItemList();
      }

      if( item->object() == contact )
      {
        list << item;
        break; // Only one MsnContact for each MsnGroup :)
      }
    }
  }
  else
  {
    warning() << "Called childContacts() from a contact item:" << this;
    return ModelItemList();
  }

  return list;
}



/**
 * Return a child MsnGroup identified by its ID
 *
 * Can only be called from the root node.
 *
 * @param group Requested group
 * @return Node of the group or null
 */
ContactListModelItem *ContactListModelItem::child( const MsnGroup *group )
{
  // Prevent incorrect usage
  if( itemType_ != ItemRoot )
  {
    warning() << "Called childGroup() on a non-root item:" << this;
    return 0;
  }

  foreach( ContactListModelItem *item, childItems_ )
  {
    if( item->type() != ItemGroup )
    {
      warning() << "Invalid item in root" << this << ":" << item;
      return 0;
    }

    if( item->object() == group )
    {
      return item;
    }
  }

  return 0;
}



// Return all children groups
ModelItemList ContactListModelItem::children()
{
  if( itemType_ != ItemRoot )
  {
    return ModelItemList();
  }

  ModelItemList list;
  foreach( ContactListModelItem *item, childItems_ )
  {
    if( item->type() == ItemGroup )
    {
      list << item;
    }
  }

  return list;
}



/**
 * Return the number of children of this item
 *
 * @return int
 */
int ContactListModelItem::childCount() const
{
  return childItems_.count();
}



/**
 * Return the number of data columns
 *
 * We only have and use one. Different data is returned depending on the given role.
 *
 * @return int
 */
int ContactListModelItem::columnCount() const
{
  return 1;
}



/**
 * Return a collection of data from this item
 *
 * The role selects which different piece of data will be extracted from the item;
 * DisplayRole gets you the full map of item data, SearchRole a string to perform
 * filtering during search. Other roles can be easily added if needed :)
 *
 * @param role  Specify the kind of item data to obtain
 * @return QVariant containing int, QMap or QString for the three columns respectively.
 */
const QVariant ContactListModelItem::data( int role ) const
{
  switch( role )
  {
    // Return a string for full contact list search
    case SearchRole:
    {
      if( itemType_ == ItemContact )
      {
        const MsnContact *contact = static_cast<const MsnContact*>( object_ );
        if( contact == 0 )
        {
          return QVariant( QString() );
        }

        // Enable searching by email, friendly name, personal message and music
        QString searchBy( contact->id()             + ' ' +
                          contact->displayName()   + ' ' +
                          contact->personalMessage() + ' ' +
                          contact->media().toString() );

        return QVariant( searchBy );
      }
      else if( itemType_ == ItemGroup )
      {
        const MsnGroup *group = static_cast<const MsnGroup*>( object_ );
        if( group == 0 )
        {
          return QVariant( QString() );
        }

        return QVariant( group->name() );
      }
      else
      {
        // Avoid crashing without hints
        warning() << "Called data( SearchRole ) on an invalid item: (see below)";
        warning() << this;
        return QVariant( QString() );
      }
    }
    break;

    case DataRole:
    {
      ModelDataList itemData;

      // Fill the returned data map with a selection of useful contact information
      if( itemType_ == ItemContact )
      {
        const MsnContact *contact = static_cast<const MsnContact*>( object_ );
        if( contact == 0 )
        {
          return QVariant( ModelDataList() );
        }

        QString mediaType;

        itemData[ "type"                     ] = ItemContact;
        itemData[ "network"                  ] = contact->network();
        itemData[ "handle"                   ] = contact->id();
        itemData[ "displayName"              ] = contact->displayName();
        itemData[ "personalMessage"          ] = contact->personalMessage();
        itemData[ "mediaString"              ] = contact->media().toString();
        itemData[ "status"                   ] = (int) contact->status();
        itemData[ "isBlocked"                ] = contact->isBlocked();
        itemData[ "hasUser"                  ] = contact->hasUser();

        if( parentNode_->object() )
        {
          const MsnGroup *group = static_cast<const MsnGroup*>( parentNode_->object() );
          if( group != 0 )
          {
            itemData[ "group"            ] = group->id();
            itemData[ "isInSpecialGroup" ] = group->isSpecialGroup();
          }
        }
        else
        {
          itemData[ "group"            ].clear();
          itemData[ "isInSpecialGroup" ] = false;
        }
      }
      else if( itemType_ == ItemGroup )
      {
        // Get the item's group to retrieve its data
        const MsnGroup *group = static_cast<const MsnGroup*>( object_ );
        if( group == 0 )
        {
          return QVariant( ModelDataList() );
        }

        // Find out how many of the group's contacts are online
        uint numOnlineContacts = 0;
        foreach( ContactListModelItem *item, childItems_ )
        {
          if( item->type() != ItemContact )
          {
            continue;
          }

          const MsnContact *contact = static_cast<const MsnContact*>( item->object() );

          if( contact && contact->isOnline() )
          {
            numOnlineContacts++;
          }
        }

        itemData[ "type"           ] = ItemGroup;
        itemData[ "id"             ] = group->id();
        itemData[ "name"           ] = group->name();
        itemData[ "onlineContacts" ] = QString::number( numOnlineContacts );
        itemData[ "totalContacts"  ] = QString::number( childCount() );
        itemData[ "isSpecialGroup" ] = group->isSpecialGroup();
      }
      else if( itemType_ == ItemRoot )
      {
        itemData[ "type"           ] = ItemRoot;
      }
      else
      {
        // Avoid crashing without hints
        warning() << "Called data( DisplayRole ) on an invalid item: (see below)";
        warning() << this;
        return QVariant( ModelDataList() );
      }

      return QVariant( itemData );
    }
    break;

  default:
    // Called with an invalid role, do nothing
    break;
  }

  return QVariant();
}



/**
 * Return the type of the internal stored object
 *
 * @return ItemType
 */
ContactListModelItem::ItemType ContactListModelItem::type() const
{
  return itemType_;
}



/**
 * Move this item to another parent
 *
 * @param newParent   New parent node. Can not be null
 */
void ContactListModelItem::moveTo( ContactListModelItem *newParent )
{
  if( itemType_ == ItemRoot )
  {
    warning() << "Called moveTo() on a root item! Item:" << this;
    return;
  }

  if( newParent == 0 )
  {
    warning() << "Called moveTo() with an empty new parent! Item:" << this;
    return;
  }

  if( parentNode_ == 0 )
  {
    warning() << "Called moveTo() on a parentless item! Item:" << this;
    return;
  }

  // Remove ourselves from the old node
  parentNode_->removeChild( this );

  // Add ourselves to the new node
  newParent->appendChild( this );

  // Change the internal parent pointer
  parentNode_ = newParent;
}



/**
 * Return the stored object
 *
 * The object should be casted accordingly to the result of a getType() call.
 *
 * @return void*
 */
const void *ContactListModelItem::object() const
{
  return object_;
}



/**
 * Get the parent node of this item
 *
 * @return Parent node
 */
ContactListModelItem *ContactListModelItem::parent() const
{
  return parentNode_;
}



/**
 * Remove a child item
 *
 * The child's object itself is not deleted, but is removed from the parent node only.
 * You have to manually call delete on it after removal.
 *
 * @param child  The item to delete
 * @return int   Number of deleted items (usually 0 or 1)
 */
int ContactListModelItem::removeChild( ContactListModelItem *child )
{
  return childItems_.removeAll( child );
}



/**
 * Get this item's position within the parent node
 *
 * @return int
 */
int ContactListModelItem::row() const
{
  if( itemType_ == ItemRoot || ! parentNode_ || parentNode_->childItems_.isEmpty() )
  {
    warning() << "Called row() on a root item or an item with invalid parent! Item:" << this;
    return 0;
  }

  return parentNode_->childItems_.indexOf( const_cast<ContactListModelItem*>( this ) );
}



/**
 * Custom operator to display nodes and items in the debug output
 *
 * @param out   Output debugging stream
 * @param item  Item to print
 * @return QDebug  The debug stream itself
 */
QDebug operator<<( QDebug out, const KMess::ContactListModelItem *item )
{
  QString type;
  const KMess::MsnContact *contact;
  const KMess::MsnGroup *group;

  switch( item->type() )
  {
    case KMess::ContactListModelItem::ItemRoot:
      type = "Root node";
      break;

    case KMess::ContactListModelItem::ItemGroup:
      group = static_cast<const KMess::MsnGroup*>( item->object() );
      if( group )
      {
        type = "MsnGroup node, id " + group->id();
      }
      else
      {
        type = "MsnGroup node, invalid object";
      }
      break;

    case KMess::ContactListModelItem::ItemContact:
      contact = static_cast<const KMess::MsnContact*>( item->object() );

      if( contact )
      {
        type = "MsnContact node, handle " + contact->id();
      }
      else
      {
        type = "MsnContact node, invalid object";
      }

      if( item->parent()->type() == KMess::ContactListModelItem::ItemRoot )
      {
        type += " in root node";
      }
      // Only the root node should have no parent, this is symptom of an error
      else if( ! item->parent() )
      {
        type += " without parent (!)";
      }
      else
      {
        group = static_cast<const KMess::MsnGroup*>( item->parent()->object() );
        if( group )
        {
          type += " in node " + group->id();
        }
        else
        {
          type += " within invalid node (!)";
        }
      }
      break;

    default:
      type = "Unknown node type";
      break;
  }

  out << type << "(at address" << QString::number( (qlonglong)item, 16 ) << ")";

  return out;
}


