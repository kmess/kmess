/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Sjors Gielen <sjors@kmess.org>                            *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file chatfactory.cpp
 * @brief Creator and handler of MsnChat instances
 */

#include "chatfactory.h"
#include "debug/libkmessdebug.h"

#include <KMess/MsnChat>
#include <KMess/Message>
#include <KMess/MsnContact>
#include <KMess/MsnSession>
#include <KMess/YahooMessage>
#include <KMess/Self>

#include <QPointer>


using namespace KMess;
using namespace KMessInternal;



/**
 * @brief The constructor
 *
 * @param session The MsnSession that created this ChatFactory.
 */
ChatFactory::ChatFactory( MsnSession *session )
: QObject()
, session_( session )
{
}



// The destructor
ChatFactory::~ChatFactory()
{
  debug() << "DESTROYING...";

  reset();
}



/**
 * @brief Find a chat with only this contact in it.
 *
 * This method is intended for delivering messages like OIM's. Offline messages
 * are not associated with MsnChats but still have to be delivered to them. If
 * you need to deliver a message that is in some way associated with an MsnChat,
 * DO NOT use this method, but write your code in such a way that you can find
 * out for yourself which MsnChat to deliver to.
 *
 * @param handle The handle to search for.
 * @returns a private chat with that handle, or 0 if none could be found.
 */
KMess::MsnChat *ChatFactory::getExclusiveChatWith( const QString &handle ) const
{
  foreach( QPointer<MsnChat> check, chats_ )
  {
    if( check && check->isPrivateChatWith( handle ) )
    {
      return check;
    }
  }

  return 0;
}



/**
 * Reset the chat factory. In short, delete all MsnChat instances.
 */
void ChatFactory::reset()
{
  // make sure to delete each MsnChat instance.
  foreach( QPointer<MsnChat> chat, chats_ )
  {
    if ( chat )
    {
      delete chat;
    }
  }

  chats_.clear();
}



/**
 * A chat has been destroyed, remove it from the list.
 */
void ChatFactory::slotChatDestroyed()
{
  debug() << "A chat has been destroyed!";

  MsnChat *destroyed = (MsnChat*)sender();

  // remove any known invalid pointers.
  chats_.removeAll( QPointer<MsnChat>( 0 ) );
  
  // and remove the destroyed chat.
  chats_.removeAll( QPointer<MsnChat>(destroyed) );

  emit chatDestroyed( destroyed );
}



/**
 * @brief Create a new MsnChat object.
 *
 * @param handle The initial handle for the chat.
 * @returns A fresh MsnChat instance.
 */
MsnChat *ChatFactory::createChat( QString handle )
{
  SwitchInfo info;
  info.startingHandle = handle;
  info.conn = 0;
  info.participants = QStringList();

  return createChat( info );
}



/**
 * Create a new chat using the given SwitchInfo structure.
 *
 * The SwitchInfo structure contains initial data like the MsnSwitchboardConnection instance,
 * the requesting user's handle, and the current participants. This is mostly used
 * when creating a chat after notification of a chat session from the SwitchboardManager.
 *
 * @param info The SwitchInfo structure that holds chat startup information.
 */
MsnChat *ChatFactory::createChat( KMessInternal::SwitchInfo info )
{
  MsnChat *chat = new MsnChat( session_, &info );
  
  connect( chat, SIGNAL(             destroyed()         ),
           this, SLOT  (     slotChatDestroyed()         ) );
  //connect( chat, SIGNAL( needSwitchboardTokens(KMess::MsnChat*) ),
  //         this, SIGNAL( needSwitchboardTokens(KMess::MsnChat*) ) );
  connect( chat, SIGNAL(    requestSwitchboard(KMess::MsnChat*) ),
           this, SIGNAL(  switchboardRequested(KMess::MsnChat*) ) );
  connect( chat, SIGNAL(       messageReceived(KMess::Message) ),
           this, SIGNAL(   chatMessageReceived(KMess::Message) ) );

  debug() << "Created chat with" << info.startingHandle;
  
  chats_.append( QPointer<MsnChat>( chat ) );

  emit chatCreated( chat );
  
  return chat;
}



/**
 * @brief A message to be delivered was received.
 *
 * Messages can be received from other sources, other than from a Switchboard
 * server. For example, federated contacts (like Yahoo or LCS contacts) send
 * us messages via the Notification server; offline messages are received
 * from a SOAP server.
 *
 * This method first finds a private chat with the message originator. Then, it
 * posts the message to that chat. If no private chat could be found, one will
 * be created.
 *
 * @param message The message to deliver
 */
void ChatFactory::message( KMess::Message message )
{
  if( ! message.isValid() )
  {
    warning() << "Invalid message received! Unable to deliver!";
    warning() << "Message:" << ((void*)&message) << "Peer:" << ((void*)message.peer());
    return;
  }

  QString handle;
  
  // if this is a Yahoo! message:
  //  if the message source is us, look for the destination.
  //  if the message is NOT us, look for the source.
  //
  // we do this because of MPOP.
  if ( message.type() == YahooMessageType )
  {
    YahooMessage msg( message );
    QString source, dest, ourHandle;
    source = msg.sourceHandle();
    dest = msg.destHandle();
    ourHandle = session_->self()->handle();
    
    if ( source == ourHandle )
    {
      handle = dest;
    }
    else
    {
      handle = source;
    }
  }
  else
  {
    handle = message.peer()->handle();
  }
  
  KMess::MsnChat *privateChat = getExclusiveChatWith( handle );
  if( privateChat == 0 )
  {
    // There was no private chat yet, create a new one.
    privateChat = createChat( handle );
  }
  message.setChat( privateChat );

  debug() << "Delivering message to chat...";

  privateChat->gotMessage( message );

  emit chatMessageReceived( message );
}



/**
 * @brief Return a list of MsnChat objects.
 */
const QList<QPointer<MsnChat> > &ChatFactory::getChats() const
{
  return chats_;
}


