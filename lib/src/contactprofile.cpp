/***************************************************************************
         contactprofile.cpp - A representation of a contact's AB entry
                             -------------------
    begin                : Sun 25 Jul 10
    copyright            : (C) 2010 by Adam Goossens
    email                : fontknocker@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <KMess/NetworkGlobals>
#include <KMess/ContactProfile>
#include <KMess/MsnContact>

#include "soap/bindings/abdatatypes.h"
#include "soap/addressbookservice.h"
#include "debug/libkmessdebug.h"

using namespace KMess;
using namespace KMessInternal;

ContactProfile::ContactProfile( AddressBookService *abService, ABContactInfo *other )
: abService_( abService )
, info_( new ABContactInfo() )
{
  if ( other )
  {
    *info_ = *other;
  }

  foreach( ABAnnotation *a, info_->annotations )
  {
    annotations_.insert( a->name, a->value );
  }
}



ContactProfile::~ContactProfile()
{
  delete info_;
}


/**
 * Return the value for a specific annotation.
 */
QString ContactProfile::annotation( const QString &name ) const
{
  foreach( ABAnnotation *a, info_->annotations )
  {
    if ( a->name == name ) return a->value;
  }

  return QString();
}



/**
 * Return a list of all of the annotations provided in the address book for
 * this contact.
 *
 * Applications can use this data to display additional information about a user,
 * if you understand what the annotations mean.
 */
QHash<QString,QString> ContactProfile::annotations() const
{
  return annotations_;
}



long ContactProfile::cid() const
{
  return info_->cid;
}



/**
 * Returns the email address corresponding to the given email type, otherwise,
 * returns NULL.
 * @param type The type of email to return
 */
QString ContactProfile::emailAddress( EmailType type ) const
{
  ABContactEmail *e = _email( type );
  if ( e )
  {
    return e->email;
  }

  return QString();
}



/**
 * Returns all contact email addresses as a QHash.. The keys correspond to
 * values of the EmailType enum, and the values are the corresponding email addresses.
 */
QHash<ContactProfile::EmailType,QString> ContactProfile::emailAddresses() const
{
  QHash<EmailType,QString> temp;
  foreach( ABContactEmail *e, info_->emails )
  {
    temp.insert( _emailStrToType( e->contactEmailType ), e->email );
  }

  return temp;
}



/**
 * Does exactly what it says. Used by friend classes to get the signal out.
 */
void ContactProfile::emitProfileChanged()
{
  emit profileChanged();
}



/**
 * If set, returns the contact's first name.
 */
QString ContactProfile::firstName() const
{
  return info_->firstName;
}



/**
 * If set, returns the contact's middle name.
 */
QString ContactProfile::middleName() const
{
  return info_->middleName;
}



/**
 * If set, returns the contact's last name.
 */
QString ContactProfile::lastName() const
{
  return info_->lastName;
}



/**
 * Return true if the user has a mobile device
 */
bool ContactProfile::hasMobileDevice() const
{
  return info_->isMobileIMEnabled;
}



/**
 * Return true if the user is a messenger contact, otherwise, returns false.
 */
bool ContactProfile::isMessengerUser() const
{
  if ( info_->passportName.isEmpty() || info_->contactType == "Email" )
  {
    ABContactEmail *e = _email( EmailMessenger );
    if ( e )
    {
      return e->isMessengerEnabled;
    }

    return false;
  }
  else
  {
    return info_->isMessengerUser;
  }
}



/**
 * Returns true if this is a passport user. If this returns false, the user is likely a
 * federated contact (i.e., Yahoo contact).
 */
bool ContactProfile::isPassportUser() const
{
  return ! info_->passportName.isEmpty();
}



/**
 * Get the ABContactInfo instance this profile manages.
 *
 * Used by AddressBookService to know what properties need updating.
 */
ABContactInfo *ContactProfile::info() const
{
  return info_;
}



/**
 * Searches for a valid MSN Messenger handle. This could either be the user's passport email or
 * one of their other email addresses that is designated as their Messenger email.
 *
 * If no such email address exists, this method returns a NULL QString.
 *
 * You can assume that if the string returned by this method is valid, then this is a legitimate
 * email address for use in Messenger.
 */
QString ContactProfile::messengerHandle() const
{
  if ( ! info_->passportName.isEmpty() )
  {
    return info_->passportName;
  }

  ABContactEmail *e = _email( EmailMessenger );
  if ( e )
  {
    return e->email;
  }

  return QString();
}



/**
 * If you have set a nickname for this contact, returns the nickname. Otherwise returns QString()
 * @see setNickname
 */
QString ContactProfile::nickname() const
{
  foreach( ABAnnotation *a, info_->annotations )
  {
    if ( a->name == "AB.NickName" )
    {
      return a->value;
    }
  }

  return QString();
}



/**
 * Return the phone number corresponding to the given phone type, if one has been set by the contact.
 * @param type The phone number type to fetch
 */
QString ContactProfile::phoneNumber( PhoneType type ) const
{
  QString strType;

  switch( type )
  {
    case PhonePersonal:
      strType = "ContactPhonePersonal";
      break;

    case PhoneBusiness:
      strType = "ContactPhoneBusiness";
      break;

    case PhoneMobile:
      strType = "ContactPhoneMobile";
      break;

    case PhonePager:
      strType = "ContactPhonePager";
      break;

    case PhoneOther:
      strType = "ContactPhoneOther";
      break;

    case PhoneFax:
      strType = "ContactPhoneFax";
      break;

    default:
      debug() << "Unknown contact phone type:" << type;
      return QString();
  }

  foreach( ABContactPhone *p, info_->phones )
  {
    if ( p->contactPhoneType.toLower() == strType.toLower() )
    {
      return p->number;
    }
  }

  return QString();
}



/**
 * Get a QHash containing all contact phone numbers, indexed by the type of phone number.
 */
QHash<ContactProfile::PhoneType,QString> ContactProfile::phoneNumbers() const
{
  QHash<PhoneType,QString> list;

  foreach( ABContactPhone *p, info_->phones )
  {
    PhoneType type = PhoneUnknown;
    if( p->contactPhoneType == "ContactPhoneBusiness" )
    {
      type = PhoneBusiness;
    }
    else if( p->contactPhoneType == "ContactPhonePersonal" )
    {
      type = PhonePersonal;
    }
    else if( p->contactPhoneType == "ContactPhoneFax" )
    {
      type = PhoneFax;
    }
    else if( p->contactPhoneType == "ContactPhoneMobile" )
    {
      type = PhoneMobile;
    }
    else if( p->contactPhoneType == "ContactPhoneOther" )
    {
      type = PhoneOther;
    }
    else if( p->contactPhoneType == "ContactPhonePager" )
    {
      type = PhonePager;
    }

    list.insert( type, p->number );
  }

  return list;
}


/**
 * Set a value for an annotation, then update the profile.
 * @internal
 */
void ContactProfile::setAnnotation( QString name, QString value )
{
  foreach( ABAnnotation *a, info_->annotations )
  {
    if ( a->name == name )
    {
      if ( a->value != value )
      {
        a->value = value;
        update();
        return;
      }
      else
      {
        debug() << "Annotation" << name << "unchanged. Not updating.";
        return;
      }
    }
  }

  annotations_.insert( name, value );

  info_->annotations.append( new ABAnnotation( name, value ) );
  update();
}



/**
 * Given a type of email address, update the address.
 *
 * If not email address is registered for the given address type a new one will be created.
 * @param type The type of address
 * @param email The email address to update or insert
 */
void ContactProfile::setEmailAddress( EmailType type, const QString &email )
{
  // this isn't a known email type
  if ( ( type & ( EmailBusiness | EmailMessenger | EmailOther | EmailPersonal | EmailUnknown ) ) != type )
  {
    return;
  }
  
  ABContactEmail *e = _email( type );
  if ( e )
  {
    if ( e->email != email )
    {
      e->email = email;
      update();
    }
  }
  else
  {
    ABContactEmail *e = new ABContactEmail();
    e->email = email;
    e->contactEmailType = _emailTypeToStr( type );
    info_->emails.append( e );
    update();
  }
}



/**
 * Sets your custom nickname for this contact. This nickname will be used
 * in place of the friendly name both here and within WLM.
 */
void ContactProfile::setNickName( const QString &name )
{
  setAnnotation( "AB.NickName", name );
}



/**
 * Set or change the middle name for this contact.
 * @param firstName First name
 */
void ContactProfile::setFirstName( const QString &firstName )
{
  if ( info_->firstName != firstName )
  {
    info_->firstName = firstName;
    update();
  }
}



/**
 * Set or change the last name for this contact
 * @param lastName Last name
 */
void ContactProfile::setLastName( const QString &lastName )
{
  if ( lastName != info_->lastName )
  {
    info_->lastName = lastName;
    update();
  }
}



/**
 * Set or change the middle name for this contact
 * @param middleName Middle name
 */
void ContactProfile::setMiddleName( const QString &middleName )
{
  if ( middleName != info_->middleName )
  {
    info_->middleName = middleName;
    update();
  }
}



/**
 * Change the display name in the address book. This will only work for
 * our personal contact ("Me").
 * @internal
 */
void ContactProfile::setDisplayName( const QString &name )
{
  if ( name != info_->displayName )
  {
    info_->displayName = name;
    update();
  }
}



void ContactProfile::setPhoneNumber( PhoneType type, const QString &phoneNumber )
{
  QString strType;

  switch( type )
  {
    case PhonePersonal:
      strType = "ContactPhonePersonal";
      break;

    case PhoneBusiness:
      strType = "ContactPhoneBusiness";
      break;

    case PhoneMobile:
      strType = "ContactPhoneMobile";
      break;

    case PhonePager:
      strType = "ContactPhonePager";
      break;

    case PhoneOther:
      strType = "ContactPhoneOther";
      break;

    case PhoneFax:
      strType = "ContactPhoneFax";
      break;

    default:
      debug() << "Unknown contact phone type:" << type;
      return;
  }

  bool done = false;

  foreach( ABContactPhone *p, info_->phones )
  {
    if ( p->contactPhoneType == strType )
    {

      // nothing to do.
      if ( p->number == phoneNumber )
      {
        return;
      }

      p->number = phoneNumber;
      done = true;
      break;
    }
  }

  if ( ! done )
  {
    ABContactPhone *newPhone = new ABContactPhone();
    newPhone->contactPhoneType = strType;
    newPhone->number = phoneNumber;
    info_->phones.append( newPhone );
  }

  update();
}



/**
 * @internal
 * Updates the internal profile information. Used on refresh
 * from AB server.
 */
void ContactProfile::setContactInfo( ABContactInfo *info )
{
  *info_ = *info;
  emit profileChanged();
}



/**
 * @internal
 * Changes the isMessengerUser property. Used for deletion of contacts.
 */
void ContactProfile::setIsMessengerUser( bool isMessengerUser )
{
  // non-passport user...
  if ( info_->passportName.isEmpty() )
  {
    ABContactEmail *e = _email( EmailMessenger );
    if ( e )
    {
      e->isMessengerEnabled = isMessengerUser;
      update();
    }
  }
  else
  {
    info_->isMessengerUser = isMessengerUser;
    update();
  }
}



/**
 * Has the address book service update this contact's details with
 * any changes.
 */
void ContactProfile::update()
{
  if ( abService_ )
  {
    abService_->updateProfile( this );
  }
}


QString ContactProfile::_emailTypeToStr( EmailType type ) const
{
  QString strType;
  switch( type )
  {
    case EmailMessenger:
      strType = "Messenger2";
      break;
    case EmailPersonal:
      strType = "ContactEmailPersonal";
      break;
    case EmailBusiness:
      strType = "ContactEmailBusiness";
      break;
    case EmailOther:
      strType = "ContactEmailOther";
      break;
    default:
      return QString();
  };

  return strType;
}



ContactProfile::EmailType ContactProfile::_emailStrToType( const QString &str ) const
{
  EmailType type = EmailUnknown;

  if ( str == "ContactEmailBusiness" )
  {
    type = EmailBusiness;
  }
  else if ( str == "ContactEmailPersonal" )
  {
    type = EmailPersonal;
  }
  else if ( str == "ContactEmailOther" )
  {
    type = EmailOther;
  }
  else if ( str == "Messenger2" )
  {
    type = EmailMessenger;
  }

  return type;
}



ABContactEmail *ContactProfile::_email( EmailType type ) const
{
  QString strType;
  switch( type )
  {
    case EmailMessenger:
      strType = "Messenger2";
      break;
    case EmailPersonal:
      strType = "ContactEmailPersonal";
      break;
    case EmailBusiness:
      strType = "ContactEmailBusiness";
      break;
    case EmailOther:
      strType = "ContactEmailOther";
      break;
    default:
      return NULL;
  };

  foreach( ABContactEmail *e, info_->emails )
  {
    if ( e->contactEmailType == strType )
    {
      return e;
    }
  }

  return NULL;
}
