/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Mike K. Bennett <mkb137b@hotmail.com>                     *
 * (C) Copyright Valerio Pilo <valerio@kmess.org>                          *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file msngroup.cpp
 * @brief Represents a group of contacts, either user-made group or system-made logical assembly of contacts
 */

#include "debug/libkmessdebug.h"

#include <QStringList>

#include <KMess/MsnGroup>
#include <KMess/NetworkGlobals>


using namespace KMess;



// The constructor
MsnGroup::MsnGroup( const QString &id, bool isSpecialGroup, const QString &name )
: id_( id )
, isSpecialGroup_( isSpecialGroup )
{
  if( name.isEmpty() )
  {
    name_ = id;
  }
  else
  {
    name_ = name;
  }
}



// The destructor
MsnGroup::~MsnGroup()
{
  debug() << "DESTROYED. [handle=" << name_ << ", id=" << id_ << "]";
}



/**
 * Return a list of contacts known to be in this group.
 */
QList<MsnContact*> MsnGroup::contacts() const
{
  return contacts_;
}



// Return the group ID
const QString& MsnGroup::id() const
{
  return id_;
}




// Return the group name
const QString& MsnGroup::name() const
{
  return name_;
}



/**
 * Returns true if this groups is empty (i.e., contains no contacts)
 */
bool MsnGroup::isEmpty() const
{
  return contacts_.count() == 0;
}



// Return true if this is a special group
bool MsnGroup::isSpecialGroup() const
{
  return isSpecialGroup_;
}



// Change the group name
void MsnGroup::setName( const QString &newName )
{
  KMESS_ASSERT( ! newName.isEmpty() );
  KMESS_ASSERT( ! isSpecialGroup_ );

  if( ( name_ != newName ) && ( ! newName.isEmpty() ) )
  {
    name_ = newName;
  }
}



/**
 * Returns this group as a string with name and ID.
 */
MsnGroup::operator QString() const
{
  return name_ + " (" + id_ + ')';
}



QDebug operator<<( QDebug dbg, const KMess::MsnGroup *group )
{
  dbg << (QString)(*group);
  return dbg;
}



QDebug operator<<( QDebug dbg, const GroupsList &list )
{
  QStringList output;
  foreach( KMess::MsnGroup *g, list )
  {
    output << (QString)(*g);
  }
  
  dbg << output.join(", ");
  return dbg;
}



QDebug operator<<( QDebug dbg, const IndexedGroupsList &list )
{
  dbg << list.values();
  return dbg;
}
