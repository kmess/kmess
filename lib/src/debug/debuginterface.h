/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Valerio Pilo <valerio@kmess.org>                          *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file debuginterface.h
 * @brief Interface between debugger and network classes
 */

#ifndef DEBUGINTERFACE_H
#define DEBUGINTERFACE_H

#include <KMess/Message>

#include <QObject>
#include <QPointer>


// Forward declarations
namespace KMess
{
  class Debugger;
  class MsnSession;
};


namespace KMessInternal
{
class SoapMessage;


class DebugInterface : public QObject
{
  friend class KMess::Debugger;

  Q_OBJECT


  protected:

    DebugInterface( QObject* parent = 0 );
   ~DebugInterface();

    void debugRequestSent( const SoapMessage* message );
    void debugResponseReceived( const SoapMessage* message );

    void debugDataSent( const QByteArray& data );
    void debugDataReceived( const QByteArray& data );


  private:

    void setDebugger( KMess::Debugger *debugger );


  protected:

    // Internal ref to the debugger instance
    QPointer<KMess::Debugger> debugger_;

};

}; // namespace KMess



#endif
