/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Valerio Pilo <valerio@kmess.org>                          *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file debugger.cpp
 * @brief Class to debug the library
 */

#include "debug/libkmessdebug.h"

#include "connections/msnnotificationconnection.h"
#include "connections/msnswitchboardconnection.h"
#include "soap/addressbookservice.h"
#include "soap/offlineimservice.h"
#include "soap/passportloginservice.h"
#include "soap/roamingservice.h"
#include "soap/soapmessage.h"
#include "chatfactory.h"

#include <KMess/ContactList>
#include <KMess/Debugger>
#include <KMess/MsnChat>
#include <KMess/MsnSession>
#include <KMess/Self>


using namespace KMess;
using namespace KMessInternal;



/**
 * Constructor.
 *
 * @param parentSession Session object which will be debugged
 */
Debugger::Debugger( MsnSession* parentSession )
: QObject()
, session_( parentSession )
{
  if( session_ == 0 )
  {
    warning() << "Unable to start the library debugger without a session!";
    return;
  }

  debug() << "Loading debugger...";

  ChatFactory* cf = session_->chatFactory_;
  MsnNotificationConnection* ns = session_->msnNotificationConnection_;
  ContactList *cl = session_->contactList_;
  
  // Attach the debugger to Notification Connection and all of its SOAP services
  ns->setDebugger( this );
  if( cl->abService_ )  cl->abService_->setDebugger( this );
  if( ns->offlineImService_ )    ns->offlineImService_    ->setDebugger( this );
  if( ns->passportLoginService_ )ns->passportLoginService_->setDebugger( this );
  if( ns->roamingService_ )      ns->roamingService_      ->setDebugger( this );

  // Listen for SOAP clients creation signals to also attach to new services
  connect( ns,  SIGNAL( soapClientCreated(KMessInternal::HttpSoapConnection*)),
           this,SLOT  ( soapClientCreated(KMessInternal::HttpSoapConnection*)));

  // Attach to all active chats
  foreach( MsnChat* chat, cf->chats_ )
  {
    if( chat->switchboard_ )
    {
      chats_.append( chat->switchboard_ );
      chat->switchboard_->setDebugger( this );
    }
  }

  // Listen for switchboard creation signals to also attach to new chats
  connect( cf,   SIGNAL( chatCreated(KMess::MsnChat*) ),
           this, SLOT  ( chatCreated(KMess::MsnChat*) ) );

  debug() << "Debugger started for session:" << session_->self()->handle();
}



/**
 * Destructor.
 */
Debugger::~Debugger()
{
  debug() << "Debugger stopped.";

  chats_.clear();
}



/**
 * Attach to a new switchboard.
 *
 * @param chat New chat to attach to
 */
void Debugger::chatCreated( KMess::MsnChat* chat )
{
  if( ! chat->switchboard_ )
  {
    warning() << "Cannot attach to chats with no switchboard!";
    return;
  }

  chat->switchboard_->setDebugger( this );
}



/**
 * Relay received data to debug.
 *
 * @param object Source object of the log data
 * @param data Raw data
 */
void Debugger::logDataReceived( const QObject* object, const QByteArray& data )
{
  quint32 id;
  QString name;
  LogDataSource source;

  if( ! sourceByObject( object, source, id, name ) )
  {
    warning() << "Could not identify log data sender object:" << sender();
    return;
  }

  emit logData( data, true /* isReceived */, source, id, name );
}



/**
 * Relay sent data to debug.
 *
 * @param object Source object of the log data
 * @param data Raw data
 */
void Debugger::logDataSent( const QObject* object, const QByteArray& data )
{
  quint32 id;
  QString name;
  LogDataSource source;

  if( ! sourceByObject( object, source, id, name ) )
  {
    warning() << "Could not identify log data sender object:" << sender();
    return;
  }

  emit logData( data, false /* isReceived */, source, id, name );
}



/**
 * Relay a received SOAP response to debug.
 *
 * @param object Source object of the log data
 * @param message A SOAP message
 */
void Debugger::logResponseReceived( const QObject* object, const KMessInternal::SoapMessage* message )
{
  quint32 id;
  QString name;
  LogDataSource source;

  if( ! sourceByObject( object, source, id, name ) )
  {
    warning() << "Could not identify log data sender object:" << sender();
    return;
  }

  emit logData( message->getMessage(), true /* isReceived */, source, id, name );
}



/**
 * Relay a sent SOAP request to debug.
 *
 * @param object Source object of the log data
 * @param message A SOAP message
 */
void Debugger::logRequestSent( const QObject* object, const KMessInternal::SoapMessage* message )
{
  quint32 id;
  QString name;
  LogDataSource source;

  if( ! sourceByObject( object, source, id, name ) )
  {
    warning() << "Could not identify log data sender object:" << sender();
    return;
  }

  emit logData( message->getMessage(), false /* isReceived */, source, id, name );
}



/**
 * Send a raw network command.
 *
 * With this method you can send a command to the NS or an SB server.
 * If you want to send data to the Notification Server instead of to a
 * chat's Switchboard, omit the chat argument.
 *
 * @param command Network command
 * @param arguments Arguments for the command, space-separated
 * @param payload Optional payload, if the command supports it
 * @param chat Optional chat to send the data to. If left empty, the data
 *             will be sent to the Notification Server instead
 */
void Debugger::sendCommand( const QString& command, const QString& arguments, const QByteArray& payload, const KMess::MsnChat* chat )
{
  if( ! session_ )
  {
    warning() << "Session is not active. Cannot send command.";
    return;
  }

  // A chat was specified, send the data to it
  if( chat )
  {
    if( ! chat->switchboard_ )
    {
      warning() << "No connection to the Switchboard Server is present for this chat. Cannot send command.";
      return;
    }

    chat->switchboard_->sendCommand( command, arguments, payload );
  }
  // No chat was specified, send the message to the notification server
  else
  {
    if( ! session_->msnNotificationConnection_ )
    {
      warning() << "No connection to the Notification Server is present. Cannot send command.";
      return;
    }

    session_->msnNotificationConnection_->sendCommand( command, arguments, payload );
  }
}



/**
 * Send raw data.
 *
 * With this method you can send raw data to the NS or an SB server.
 * If you want to send data to the Notification Server instead of to a
 * chat's Switchboard, omit the chat argument.
 *
 * @param data Data array
 * @param chat Optional, chat to send the data to. If left empty, the data
 *             will be sent to the Notification Server instead
 */
void Debugger::sendData( const QByteArray& data, const KMess::MsnChat* chat )
{
  if( ! session_ )
  {
    warning() << "Session is not active. Cannot send data.";
    return;
  }

  // A chat was specified, send the data to it
  if( chat )
  {
    if( ! chat->switchboard_ )
    {
      warning() << "No connection to the Switchboard Server is present for this chat. Cannot send data.";
      return;
    }

    chat->switchboard_->writeBinaryData( data );
  }
  // No chat was specified, send the message to the notification server
  else
  {
    if( ! session_->msnNotificationConnection_ )
    {
      warning() << "No connection to the Notification Server is present. Cannot send data.";
      return;
    }

    session_->msnNotificationConnection_->writeBinaryData( data );
  }
}



/**
 * Send a Message.
 *
 * With this method you can send messages to the NS or an SB server.
 * If you want to send a message to the Notification Server instead of to a
 * chat's Switchboard, omit the chat argument.
 *
 * @param message Message to send
 * @param chat Optional, chat to send the message to. If left empty, the message
 *             will be delivered to the Notification Server instead
 */
void Debugger::sendMessage( KMess::Message message, const KMess::MsnChat* chat )
{
  if( ! session_ )
  {
    warning() << "Session is not active. Cannot send message.";
    return;
  }

  // A chat was specified, send the message to it
  if( chat )
  {
    if( ! chat->switchboard_ )
    {
      warning() << "No connection to the Switchboard Server is present for this chat. Cannot send message.";
      return;
    }

    chat->switchboard_->sendMessage( message );
  }
  // No chat was specified, send the message to the notification server
  else
  {
    if( ! session_->msnNotificationConnection_ )
    {
      warning() << "No connection to the Notification Server is present. Cannot send message.";
      return;
    }

    QByteArray rawData( message.contents() );

    session_->msnNotificationConnection_->sendCommand( "MSG", QString(), rawData );
  }
}



/**
 * Send an arbitrary SOAP request.
 *
 * @param endpoint URL where to send the SOAP request
 * @param soapAction SOAP action. Can be empty
 * @param header SOAP request header
 * @param body SOAP request body
 */
void Debugger::sendRequest( const QUrl& endpoint, const QString& soapAction, const QByteArray& header, const QByteArray& body )
{
  if( ! session_ )
  {
    warning() << "Session is not active. Cannot send request.";
    return;
  }

  if( ! session_->msnNotificationConnection_ )
  {
    warning() << "No connection to the Notification Server is present. Cannot send request.";
    return;
  }

  AddressBookService* service = session_->contactList_->abService_;

  KMESS_ASSERT( service );

  // It will be deleted when the SOAP class is done
  SoapMessage *request = new SoapMessage( endpoint.toString(), soapAction, header, body );
  service->sendRequest( request, true /* isUrgent*/ );
}



/**
 * Attach to a new SOAP client.
 *
 * @param client New client to attach to
 */
void Debugger::soapClientCreated( HttpSoapConnection* client )
{
  client->setDebugger( this );
}



/**
 * Retrieve a connection ID and type from an object.
 *
 * @param object Object to 'identify'
 * @param source Logging data source type
 * @param id Connection ID
 * @param name Connection name (usually a contact name or so)
 * @return bool False on error
 */
bool Debugger::sourceByObject( const QObject* object, LogDataSource& source, quint32& id, QString& name )
{
  if( ! session_ || ! object )
  {
    source = SOURCE_UNKNOWN;
    id = 0;
    name.clear();
    return false;
  }

  id = 0;

  if( qobject_cast<const KMessInternal::MsnNotificationConnection*>( object ) != 0 )
  {
    source = SOURCE_NOTIFICATION;
    name = QString( "NS %1 (%2)" ).arg( id ).arg( session_->self()->handle() );
  }
  else if( qobject_cast<const KMessInternal::MsnSwitchboardConnection*>( object ) != 0 )
  {
    if( ! chats_.contains( object ) )
    {
      chats_.append( object );
    }

    id = chats_.indexOf( object );
    source = SOURCE_SWITCHBOARD;

    ChatFactory* cf = session_->chatFactory_;
    if( cf != 0 )
    {
      foreach( MsnChat* chat, cf->chats_ )
      {
        if( ! chat->switchboard_ || chat->switchboard_ != object )
        {
          continue;
        }

        name = QString( "SB %1 (%2)" ).arg( id ).arg( chat->participants_.join( ", " ) );
        break;
      }
    }
  }
  else if( qobject_cast<const KMessInternal::AddressBookService*>( object ) != 0 )
  {
    source = SOURCE_SOAP_ADDRESS_BOOK;
    name = QString( "SOAP AB %1" ).arg( id );
  }
  else if( qobject_cast<const KMessInternal::OfflineImService*>( object ) != 0 )
  {
    source = SOURCE_SOAP_OFFLINE_MESSAGING;
    name = qobject_cast<const OfflineImService*>( object )->objectName();
    id = ( name.indexOf( "receiver" ) != -1 );

    name = QString( "SOAP OIM %1 (%2)" ).arg( id ).arg( name );
  }
  else if( qobject_cast<const KMessInternal::RoamingService*>( object ) != 0 )
  {
    source = SOURCE_SOAP_STORAGE;
    name = QString( "SOAP STOR %1" ).arg( id );
  }
  else if( qobject_cast<const KMessInternal::PassportLoginService*>( object ) != 0 )
  {
    source = SOURCE_SOAP_PASSPORT;
    name = QString( "SOAP PP %1" ).arg( id );
  }
  else
  {
    source = SOURCE_UNKNOWN;
    name = object->metaObject()->className();
  }

  return true;
}


