/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Sjors Gielen <sjors@kmess.org>                            *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file libkmessdebug.h
 * @brief Debug configuration for libkmess-msn
 */

/**
 * @file libkmessdebug.h
 * @brief Debug helping functionality.
 *
 * Throughout the library use <code>debug() << "something";</code> and
 * <code>warning() << "something";</code> for your debug and warning
 * statements. Debug statements will disappear in release builds.
 *
 * To disable debug output for a complete file, you have to define
 * <code>QT_NO_DEBUG_OUTPUT</code> before your include statements. You should
 * have this file as the first include line in all .cpp files. Do not include
 * this file in any of your header files.
 */

#ifndef LIBKMESSDEBUG_H
#define LIBKMESSDEBUG_H

#include "libkmess-msnconfig.h"

// Disable qDebug() and friends completely in release builds
// The Qt debug-disabling defines *must* be present before including <QDebug>,
// or they won't work.
#if ( LIBKMESS_MSN_DEBUG == 0 )
  #ifndef QT_NO_DEBUG_OUTPUT
    #define QT_NO_DEBUG_OUTPUT
  #endif
  // We want to keep warnings even in release mode. However, in case it may be needed,
  // there also is QT_NO_WARNING_OUTPUT
#endif

#include <QDebug>

// Enable more details in the debug output
#if ( LIBKMESS_MSN_DEBUG == 1 )
  #include <QRegExp>
  #define PRETTY_METHOD_NAME  QString( Q_FUNC_INFO ) \
                              .replace( QRegExp( ".*((KMess::)?[a-zA-Z]+::~?[a-zA-Z]+).*" ), \
                                                 "\\1(): " ) \
                              .toAscii().constData()

#else
  #define PRETTY_METHOD_NAME
#endif

// Local debugging defines
#define debug()    qDebug() << PRETTY_METHOD_NAME
#define warning()  qWarning() << PRETTY_METHOD_NAME
#define fatal(msg) qFatal(msg)

#if ( LIBKMESS_MSN_DEBUG == 1 )

  // Q_ASSERT wrapper
  #define KMESS_ASSERT(cond) Q_ASSERT( cond )
  #define KMESS_ASSERT_X(cond, msg) Q_ASSERT_X( cond, PRETTY_METHOD_NAME, msg )

  // Wrapper macro to Q_ASSERT for assert-if-null cases
  #define KMESS_NULL(cond) Q_ASSERT( cond != 0 )

#else

  // Avoid asserts in release versions
  #define KMESS_ASSERT(cond)
  #define KMESS_NULL(cond)

#endif

// a couple of cheeky macros to make life easier
#define CHAIN_SIGNAL( source, target, signal ) connect( source, SIGNAL( signal ), target, SIGNAL( signal ) )
#define DELETE( var ) if ( var ) { delete var; var = 0L; }


#endif  // LIBKMESSDEBUG_H
