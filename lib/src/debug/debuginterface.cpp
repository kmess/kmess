/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Valerio Pilo <valerio@kmess.org>                          *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file debuginterface.cpp
 * @brief Interface between debugger and network classes
 */

#include "debug/debuginterface.h"
#include "debug/libkmessdebug.h"

#include "soap/soapmessage.h"

#include <KMess/Debugger>


using namespace KMessInternal;



DebugInterface::DebugInterface( QObject* parent )
: QObject( parent )
{
}



DebugInterface::~DebugInterface()
{
}



void DebugInterface::debugDataSent( const QByteArray& data )
{
  if( ! debugger_ )
  {
    return;
  }

  debugger_->logDataSent( this, data );
}



void DebugInterface::debugDataReceived( const QByteArray& data )
{
  if( ! debugger_ )
  {
    return;
  }

  debugger_->logDataReceived( this, data );
}




void DebugInterface::debugRequestSent( const SoapMessage* message )
{
  if( ! debugger_ )
  {
    return;
  }

  debugger_->logRequestSent( this, message );
}



void DebugInterface::debugResponseReceived( const SoapMessage* message )
{
  if( ! debugger_ )
  {
    return;
  }

  debugger_->logResponseReceived( this, message );
}



void DebugInterface::setDebugger( KMess::Debugger *debugger )
{
  debug() << "Debugger activated for" << metaObject()->className();

  debugger_ = debugger;
}


