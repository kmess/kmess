/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Mike K. Bennett <mkb137b@hotmail.com>                     *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file msnnotificationconnection.h
 */

#ifndef MSNNOTIFICATIONCONNECTION_H
#define MSNNOTIFICATIONCONNECTION_H

#include "msnconnection.h"
#include "../soap/addressbookservice.h"

#include <QDateTime>
#include <QDomElement>
#include <QList>
#include <QTimer>
#include <KMess/Message>
#include <QPointer>


// Forward declarations
namespace KMess
{
  class MsnChat;
  class MsnSession;
  class Message;
  class OfflineMessage;
  class ContactList;
  class Self;
};


namespace KMessInternal
{
// Forward declarations
class PassportLoginService;
class OfflineImService;
class RoamingService;


/**
 * @brief The primary connection to the MSN server.
 *
 * The notification connection is initialized when the
 * user signs in to the MSN Messenger service.
 * When this connection is dropped, the user is signed off as well.
 * The notification connection is used for the following actions:
 * - managing the contact list
 * - starting new chat sessions
 * - receive invitations for a chat session
 * - receive status updates about contacts, e.g.:
 *   - online/offline changes
 *   - new email
 *   - personal status messages
 *   - now playing information
 *   - new offline-im messages
 *
 * Most protocol commands are echo'ed back to the client.
 * This allows a Model-View-Controller based protocol handling,
 * see MsnConnection::parseCommand() for details.
 *
 * When the connection to the notification server is established,
 * the the PassportLoginService class is used to retrieve an authentication token.
 * This token is provided to the <code>USR</code> command complete the login process.
 *
 * Contacts are organized in multiple "lists".
 * This differs from the "groups" seen in the main window!
 * See the Contact class for information about "lists" and "groups".
 *
 * New chat invitations are handled by the <code>XFR</code> command.
 * The server response contains the login data for a chat session (or "chat room")
 * at a switchboard server. This information is used to start the chat session.
 * The information about new chats is forwarded to the ChatMaster first.
 * This class looks for existing chats with a contact,
 * or opens a new chat window instead when needed.
 * The actual chat session is handled by the MsnSwitchboardConnection class.
 * Once the chat session is created, the other contact
 * is invited to the session at the switchboard server.
 *
 * Some contact commands include an MsnObject argument.
 * This is the "ticket" to request a display picture later,
 * by "inviting" the contact to transfer the data belonging to the MsnObject.
 * Invitations for file and msnobject transfers are not handled by the notification connection however.
 * A chat session needs to be started first. The invitation for the msnobject or file transfer can be sent there.
 *
 * Note that each connection starts with a version exchange.
 * Clients inform the server about the versions of the protocol it supports.
 * The commands documented here may differ from other clients using a different protocol version.
 * Currently KMess uses <code>MSNP15</code> for all the notification connection.
 * New features are added to new protocol versions only.
 * Therefore, something as simple as a 'personal status message' required
 * an upgrade to a complete new protocol version. The <code>UBX</code>
 * command - which contains the personal status message - is only sent as of <code>MSNP11</code>.
 *
 * The <code>CHG</code> and <code>NLN</code> commands include a bitwise flag
 * with the supported client-to-client features. This includes the
 * MSNC version for the invitation system, the type of the client ('normal', 'mobile', 'web messenger') and features like 'winks', 'webcam', 'folder sharing'.
 * By advertising these values, a client promoses the feature is fully supported.
 * Changing the advertised MSNC version changes the type of P2P messages sent by another client.
 * Hence, changing the capabilities value could pose a major impact.
 * The list of known flags can be found in the MsnClientCapabilities enum.
 *
 * @author Mike K. Bennett
 * @author Valerio Pilo
 * @author Antonio Nastasi
 * @ingroup NetworkCore
 */
class MsnNotificationConnection : public MsnConnection
{
  friend class KMess::Debugger;

  Q_OBJECT

  public: // Public methods related to this class
    // The constructor
                         MsnNotificationConnection( KMess::MsnSession* session, KMess::ContactList* contactList );
    // The destructor
                         ~MsnNotificationConnection();
    // Close the connection with the server
    void                 closeConnection();
    // Open a connection to the server
    void                 openConnection();


  public: // Public methods called when user changes one property. They don't be used as slots
    // Change the friendly name of the user.
    void                 changeFriendlyName( const QString& newName );
    // Change the current media of the user.
    void                 changeCurrentMedia();
    // Request a change in the user's status
    void                 changeStatus( const KMess::MsnStatus newStatus );


  public: // Public methods called when user wants to modify his contactlist. They don't be used as slots
    // Use this to logout from everywhere except here.
    void                 logoutEverywhereElse();
    // Request a COMPOSE url
    void                 requestComposeUrl( const QString &toAddress );
    // send a UUN command
    void                 sendUUN( const QString &handle, int type, const QString &body );
    // send a UUM command
    void                 sendUUM( KMess::Message &message );
    // Set the cache file
    void                 setCacheDir( const QString &path );

  public slots:
    // Change the personal message of the user.
    void                 changePersonalMessage();
    // Change a user property
    void                 changeProperty( QString type, QString value );
    // Change a contact property
    void                 changeProperty( QString handle, QString type, QString value );
    // Request a connection to a contact
    void                 inviteToChat( KMess::MsnChat *requester );



  private: // Private methods
    // Change the personal properties
    void                 changePersonalProperties( long cid, int blp );
    // Create the Offline-IM service
    OfflineImService     *createOfflineImService();
    // Create the passport login handler
    PassportLoginService *createPassportLoginService();
    // Create the Roaming service
    RoamingService       *createRoamingService();
    // Go online
    void                 goOnline();
    // Received response to adl command
    void                 gotAdl( const QStringList& command, const QByteArray &payload );
    // Received confirmation of the user's status change
    void                 gotChg( const QStringList& command );
    // Received a challenge from the server
    void                 gotChl( const QStringList& command );
    // Received a version update from the server
    void                 gotCvr( const QStringList& command );
    // Received notice that a contact went offline
    void                 gotFln( const QStringList& command );
    // Received notice that a contact is already online
    void                 gotIln( const QStringList& command );
    // Received MSG command
    void                 gotMsg( const QStringList& command, const QByteArray &payload );
    // Received notice that a contact has changed status
    void                 gotNln( const QStringList& command );
    // Received notice of disconnetion from the server
    void                 gotOut( const QStringList& command );
    // Received a property change (friendly name, phone number, etc..)
    void                 gotPrp(const QStringList& command);
    void                 gotRml( const QStringList &command );
    // Received a chat request from a contact
    void                 gotRng( const QStringList& command );
    // Received details of a contact's personal message
    void                 gotUbx( const QStringList &command, const QByteArray &payload );
    // Received a federated message
    void                 gotUbm( const QStringList &command, const QByteArray &payload );
    // Received a federated message that asks us to do...stuff...
    void                 gotUbn( const QStringList &command, const QByteArray &payload );
    // Received the folder and command info for Hotmail's inbox or compose
    void                 gotUrl( const QStringList& command );
    // Received user authentication information
    void                 gotUsr( const QStringList& command );
    // Received version information
    void                 gotVer( const QStringList& command );
    // Received server transfer information
    void                 gotXfr( const QStringList& command );
    // Send the ADL command
    void                 putAdl( KMess::MsnContact *contact = 0, KMess::MsnMemberships lists = KMess::MSN_LIST_NONE );
    // Send the RML command
    void                 putRml( KMess::MsnContact *contact = 0, KMess::MsnMemberships lists = KMess::MSN_LIST_NONE );
    // Send the personal status and current media fields.
    void                 putUux( bool initialUpdate = false );
    // Send the version command
    void                 putVer();
    // Parse a regular command
    void                 parseCommand( const QStringList& command );
    // Parse an error command
    void                 parseError( const QStringList& command, const QByteArray &payloadData );
    // Parse a payload command
    void                 parsePayloadCommand( const QStringList &command, const QByteArray &payload );


  private slots:
    // The passport login failed, username/password was incorrect
    void                 loginIncorrect();
    // The passport login succeeded
    void                 loginSucceeded();
    // Received the Mail-Data field over SOAP or at the notification connection.
    void                 receivedMailData( const QDomElement& mailData );
    // An Offline-IM message was downloaded
    void                 receivedOfflineIm( const QString &messageId, const QString &from, const QString &to,
                                            const QDateTime &date, const QString &body,
                                            const QString &runId, int sequenceNum );
    void                 sendMailUrlRequest();
    // The authentication process has timed out
    void                 slotConnectionTimeout();
    // Contact added, notify it to contactlist
    void                 slotContactAdded( KMess::MsnContact *contact );
    // A contact was blocked/unblocked. Send RML/ADL.
    void                 slotContactBlockChanged( KMess::MsnContact *contact );
    // Contact deleted
    void                 slotContactDeleted( KMess::MsnContact *contact );
    // Called when address book services retrieved address book list
    void                 slotAddressBookParsed( long cid, int blp );


  private slots: // Private slots related to this class
    // The socket connected, so send the version command.
    void                 slotConnected();


  private: // Private attributes

    struct OfflineImMessage
    {
      QString   messageId;
      QString   from;
      QDateTime date;
      QString   body;
      QString   runId;
      int       sequenceNum;
    };

    // The address book service
    KMess::ContactList        *contactList_;
    // The cache file path
    QString                    cacheDir_;
    // Whether or not the connection has just been established
    bool                       isInitiatingConnection_;
    // Whether or not the user is online
    bool                       isOnline_;
    // A timer to watch over the login process
    QTimer                     loginTimer_;
    // Nonce received by the server on first SSO command
    QByteArray                 nonceBase64_;
    // The received offline messages.
    QList<OfflineImMessage*>    offlineImMessages_;
    // The Offline-IM SOAP client
    OfflineImService          *offlineImService_;
    // The list of unanswered chat session requests
    QMap<int,QPointer<KMess::MsnChat> > openRequests_;
    // The object than handles the passport login
    PassportLoginService      *passportLoginService_;
    // Pending COMPOSE URL requests
    QHash<int,QString>         pendingComposeRequests_;
    // The Roaming SOAP client
    RoamingService            *roamingService_;
    // The pending offline-im messages
    QStringList                pendingOfflineImMessages_;
    // A copy of Self, for ease of access
    KMess::Self               *self_;

  signals:
    // Signal a chat message (used for federated messages and OIM)
    void                 receivedMessage( KMess::Message message );
    // Signal that the connection has been made successfully
    void                 loggedIn();
    // Signal that the connection to the server has failed
    void                 connectionFailed( KMess::StatusMessage reason );
    // Signal that a contact added you
    void                 contactAddedUser( KMess::MsnContact *contact );
    // Signal that adding a contact to a group was successful
    void                 contactAddedToGroup( KMess::MsnContact *contact, KMess::MsnGroup *group );
    // Signal that removing a contact was successful
    void                 contactRemoved( KMess::MsnContact *contact );
    // Signal that a new email has been received
    void                 newEmail( const QString &sender, const QString &subject, bool inInbox, const QString &command, const QString &folder, const QString &url );
    // Signal that a chat session was requested by a contact
    void                 invitedToChat( const QString &handle, const QString &server, const quint16 &port, const QString &auth, const QString &chatId );
    // Signal that a display picture change has been confirmed by the server
    void                 changedDisplayPicture( const QString& );
    // Signal that a friendly name change has been confirmed by the server
    void                 changedFriendlyName( const QString& friendlyName );
    // Signal that a status change has been confirmed by the server
    void                 changedStatus( const KMess::MsnStatus newStatus );
    // Contact has gone online
    void                 contactOnline( KMess::MsnContact *contact, bool initial = false );
    // Contact has gone offline
    void                 contactOffline( KMess::MsnContact *contact );
    // Contact has changed status
    void                 contactChangedStatus( KMess::MsnContact *contact, KMess::MsnStatus newStatus );
    // A COMPOSE url was received
    void                 gotComposeUrl( QString recipient, QString url );
};


};

#endif
