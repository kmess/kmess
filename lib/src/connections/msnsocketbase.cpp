/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Valerio Pilo <valerio@kmess.org>                          *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file msnsocketbase.cpp
 */

#include "msnsocketbase.h"

#include <QAuthenticator>
#include <QNetworkProxy>


using namespace KMessInternal;



/**
 * @brief The constructor
 */
MsnSocketBase::MsnSocketBase( KMess::MsnSession *session, ServerType serverType )
: connected_( false )
, session_( session )
{
  // Set the type of server the derived socket will connect to
  serverType_ = serverType;
}



/**
 * @brief The destructor
 *
 * Does nothing.
 */
MsnSocketBase::~MsnSocketBase()
{
}



/**
 * @brief Return which payload commands are accepted by this connection
 *
 * This method fills the list of 3-letter commands carrying a payload that the connection can
 * send and receive.
 *
 * @param receivable String list filled with all payload commands accepted for reception
 * @param sendable String list filled with all payload commands accepted for sending
 */
void MsnSocketBase::getPayloadCommands( QStringList& receivable, QStringList& sendable )
{
  receivable = receivablePayloadCommands_;
  sendable   = sendablePayloadCommands_;
}



/**
 * @brief Return the type of server connection managed by the socket
 */
MsnSocketBase::ServerType MsnSocketBase::getServerType() const
{
  return serverType_;
}



/**
 * @brief Whether or not the connection is active
 *
 * @returns True when connected, false otherwise.
 */
bool MsnSocketBase::isConnected() const
{
  return ( connected_ );
}



/**
 * @brief Test whether the given command is an error.
 *
 * @param command Command to verify
 * @returns True when it's an error command, false otherwise.
 */
bool MsnSocketBase::isErrorCommand( const QString &command ) const
{
  bool isNumeric = false;
  return ( command.toInt( &isNumeric ) != 0 ) && ( isNumeric == true );
}



/**
 * @brief Test whether the given command carries a payload when received.
 *
 * @param command Command to verify
 * @returns True when it's a payload command on receive, false otherwise.
 */
bool MsnSocketBase::isPayloadCommandOnReceive( const QString &command ) const
{
  return receivablePayloadCommands_.contains( command );
}



/**
 * @brief Test whether the given command must carry a payload when sent.
 *
 * @param command Command to verify
 * @returns True when it's a payload command on send, false otherwise.
 */
bool MsnSocketBase::isPayloadCommandOnSend( const QString &command ) const
{
  return sendablePayloadCommands_.contains( command );
}



/**
 * @brief Specify which accepted commands carry a payload.
 *
 * Within different kinds of connection, received server messages can be different or
 * have different meanings; this method ensures that every kind of connection
 * is able to recognize its own payload commands only.
 *
 * @param receivable String list with all payload commands accepted for reception
 * @param sendable String list with all payload commands accepted for sending
 */
void MsnSocketBase::setPayloadCommands( const QStringList& receivable, const QStringList& sendable )
{
  receivablePayloadCommands_ = receivable;
  sendablePayloadCommands_ = sendable;
}


