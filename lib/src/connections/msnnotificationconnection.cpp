/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Mike K. Bennett <mkb137b@hotmail.com>                     *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file msnnotificationconnection.cpp
 */

#include "msnnotificationconnection.h"
#include "debug/libkmessdebug.h"
#include "contactlistmodel.h"

#include <KMess/ContactList>
#include <KMess/EmailMessage>
#include <KMess/MsnChat>
#include <KMess/MsnContact>
#include <KMess/MsnGroup>
#include <KMess/MsnObject>
#include <KMess/MsnSession>
#include <KMess/OfflineMessage>
#include <KMess/MsnAlgorithms>
#include <KMess/MsnChallengeHandler>
#include <KMess/Utils>
#include <KMess/MPOPEndpoint>
#include <KMess/YahooMessage>
#include <KMess/Self>

#include "../soap/addressbookservice.h"
#include "../soap/passportloginservice.h"
#include "../soap/offlineimservice.h"
#include "../soap/roamingservice.h"
#include "../utils/utils_internal.h"
#include "../listsmanager.h"

#include <QTextDocument>  // for Qt::escape()
#include <QUrl>
#include <QUuid>
#include <QPointer>

// NOTE: NOTIFICATION_HIDE_P2P_SUPPORT was moved to utils/utils.cpp

using namespace KMess;
using namespace KMessInternal;



/**
 * Maximum allowed time between unique server responses
 *
 * KMess will wait at most this number of milliseconds after receiving a server command
 * (any command, in fact). If nothing arrives and this timeout expires, the login process
 * or the connection will be aborted.
 */
#define CONNECTION_TIMEOUT         7000
#define AUTHENTICATION_TIMEOUT     30000

/**
 * @brief The constructor
 *
 * Initializes the login timer and the commands known by this connection.
 *
 * @param session The parent session for this object
 * @param contactList The ContactList object for this connection
 */
MsnNotificationConnection::MsnNotificationConnection( MsnSession *session, ContactList *contactList )
 : MsnConnection( session, MsnSocketBase::SERVER_NOTIFICATION ),
   contactList_( contactList ),
   isInitiatingConnection_(false),
   isOnline_(false),
   offlineImService_(0),
   passportLoginService_(0),
   roamingService_(0),
   self_( session->self() )
{
  setObjectName( "MsnNotificationConnection" );

  // Attach a timer to watch over the login process
  loginTimer_.setSingleShot( true );
  connect( &loginTimer_, SIGNAL( timeout() ),
           this,         SLOT  ( slotConnectionTimeout() ) );
  
  // Determine which payload commands can be received/sent by the Notification Connection
  QStringList receivablePayloadCommands, sendablePayloadCommands;
  receivablePayloadCommands << "MSG"   // Server message
                            << "UBX"   // Contact personal status message (received)
                            << "UBN"
                            << "UBM"   // Yahoo! incoming message
                            << "GCF"   // Server config file
                            << "NOT"   // Msn alerts, msn calendar
                            << "ADL"   // Add List command
                            << "RML";  // used to remove contacts

  sendablePayloadCommands << "MSG"   // Server message
                          << "UBX"   // Contact personal status message (received)
                          << "UUX"   // Contact personal status message (sent)
                          << "QRY"   // Response to the CHL command
                          << "UUN"
                          << "UUM"   // Yahoo! outgoing message
                          << "GCF"   // Server config file
                          << "NOT"   // Msn alerts, msn calendar
                          << "ADL"   // Add List command
                          << "RML";  // used to remove contacts

  // hook up the ABservice.
  connect( contactList->abService_, SIGNAL( contactBlocked  ( KMess::MsnContact * ) ), this, SLOT( slotContactBlockChanged( KMess::MsnContact *) ) );
  connect( contactList->abService_, SIGNAL( contactUnblocked( KMess::MsnContact * ) ), this, SLOT( slotContactBlockChanged( KMess::MsnContact *) ) );
  connect( contactList->abService_, SIGNAL( contactDeleted  ( KMess::MsnContact * ) ), this, SLOT( slotContactDeleted( KMess::MsnContact * ) ) );
  connect( contactList->abService_, SIGNAL( addressBookParsed( long, int ) ), this, SLOT( slotAddressBookParsed( long, int ) ) );
  connect( contactList->abService_, SIGNAL( contactAdded( KMess::MsnContact * ) ), this, SLOT( slotContactAdded( KMess::MsnContact * ) ) );
  
  setPayloadCommands( receivablePayloadCommands, sendablePayloadCommands );
}



/**
 * @brief The destructor
 *
 * Deletes the contact list.
 * The base class closes the connection.
 */
MsnNotificationConnection::~MsnNotificationConnection()
{
  // Remove all open requests.
  openRequests_.clear();

  // Remove all offline im messages
  qDeleteAll( offlineImMessages_ );
  offlineImMessages_.clear();

  // closeConnection() also cleans up a lot too.
  if( isConnected() )
  {
    closeConnection();
  }

  if( passportLoginService_ != 0 )
  {
    // Apparantly, we quit in the middle of a request.
    warning() << "PassportLoginService still set in MsnNotificationConnection destructor.";
  }

  debug() << "DESTROYED.";
}



/**
 * @brief Change the current media of the user.
 *
 * This is used to exchange the "now playing" information.
 * It sends an <code>UUX</code> command to the server.
 * More information about this command can be found
 * at: http://msnpiki.msnfanatic.com/index.php/MSNP11:Changes#UUX
 *
 * Previously, this method took arguments, but it now only calls
 * putUux() which takes the information needed from the global session.
 *
 * @see putUux()
 */
void MsnNotificationConnection::changeCurrentMedia()
{
  // Send personal status and current media
  putUux();
}



/**
 * @brief Change the friendly name of the user.
 *
 * Internally, this method uses changeProperty() to set the <code>MFN</code> property.
 * In MSNP15 the command PRP must be send with SOAP request to avoid nickname will disappear
 * the next time we sign in.
 *
 * @param  newName  The new friendly name.
 */
void MsnNotificationConnection::changeFriendlyName( const QString& newName )
{
  if ( newName.trimmed().isEmpty() )
  {
    warning() << "Tried to set an empty friendly name; ignoring.";
    return;
  }

  // Non verified account cannot change their name and will get disconnected if they try
  if( ! self_->passportVerified() )
  {
    return;
  }

  changeProperty( "MFN", newName );

//   addressBookService_->contactUpdate( AddressBookService::PROPERTY_FRIENDLYNAME, newName );
}



// Change the personal properties
void MsnNotificationConnection::changePersonalProperties( long cid, int blp )
{
  // Send BLP command for privacy settings, please refer to:
  // http://msnpiki.msnfanatic.com/index.php/Command:BLP
  const QString& blpToSend( ( blp == 0 ) ? "BL" : "AL" );
  sendCommand( "BLP", blpToSend );

  // Check if necessary to retrieve information ( like pm, picture ) from msn storage
  // using roaming service
  RoamingService *roamingService = createRoamingService();
  roamingService->getProfile( cid );
}



/**
 * @brief Change the personal message of the user.
 *
 * The personal message typically contains a short status message
 * displayed next to the contact.
 *
 * This method sends the <code>UUX</code> payload command to the server.
 *
 * Previously, this method would take some arguments, but now it only
 * calls putUux(); the personal message is retrieved from the global session.
 *
 * @see putUux().
 */
void MsnNotificationConnection::changePersonalMessage()
{
  putUux();

  createRoamingService()->updateProfile();
}



/**
 * @brief Change a user property
 *
 * It sends the <code>PRP</code> (likely short for property) command to the server.
 *
 * Valid property types are:
 * - <code>MFN</code>: MSN Friendly name
 * - <code>PHH</code>: Phone home
 * - <code>PHW</code>: Phone work
 * - <code>PHM</code>: Phone mobile
 * - <code>MOB</code>: MSN Mobile authorised for others: Y or N
 * - <code>MBE</code>: MSN Mobile enabled
 * - <code>WWE</code>: MSN direct?
 * - <code>HSB</code>: Has blog: <code>1</code> or <code>0</code>
 *
 * This function is also used internally by other methods like changeFriendlyName()
 *
 * @param  type   The property type.
 * @param  value  The new property value, can also be empty.
 */
void MsnNotificationConnection::changeProperty( QString type, QString value )
{
  if( value.isEmpty() )
  {
    sendCommand( "PRP", type );
  }
  else
  {
    value = QUrl::toPercentEncoding( value );

    // To avoid problem with a very long nick
    value.truncate( 387 );

    sendCommand( "PRP", type + ' ' + value );
  }
}



/**
 * @brief Change a contact property
 *
 * It sends the <code>SBP</code> (likely short for set-buddy-property) command to the server.
 *
 * @param  handle  Email address of the contact.
 * @param  type    The property type, e.g. <code>MFN</code>, see the other changeProperty() method.
 * @param  value   The new property value, can also be empty.
 */
void MsnNotificationConnection::changeProperty( QString handle, QString type, QString value )
{
  // Get contact
  const MsnContact *contact = contactList_->contact( handle );
  KMESS_NULL( contact );

  // Avoid disconnects by the server
  if( contact->guid().isEmpty() )
  {
    warning() << "Can't change property, contact GUID is empty"
                << " (contact=" << handle << " property=" << type << ")." << endl;
    return;
  }

  // First guess is that "SBP" means set-buddy-property
  if( value.isEmpty() )
  {
    sendCommand( "SBP", contact->guid() + ' ' + type );
  }
  else
  {
    value = QUrl::toPercentEncoding( value );

    // To avoid problem with a very long nick
    value.truncate( 387 );

    sendCommand( "SBP", contact->guid() + ' ' + type + ' ' + value );
  }
}



/**
 * @brief Change the status of the user.
 *
 * This sends the <code>CHG</code> command to the user.
 * This command also informs contacts the user's MsnObject and client capabilities.
 * The MsnObject contains the meta-data about the user's display picture.
 * The display picture itself is transferred in a chat session.
 *
 * Note this method advertises the supported client features, like 'winks' and 'webcam'.
 *
 * @param  newStatus  The new user status.
 * @todo Possibly rename this to putChg() instead, especially when webcam support makes the capabilities dynamic.
 */
void MsnNotificationConnection::changeStatus( const KMess::MsnStatus newStatus )
{
  if( ! isConnected() )
  {
    warning() << "Attempted to send CHG command, but the client isn't connected";
    return;
  }

  QStringList parameters;

  parameters << InternalUtils::statusToCode( newStatus );
  parameters << Utils::getClientCapabilities().toString();

  const QString &objString( QUrl::toPercentEncoding( self_->msnObject().objectString() ) );

  // Just don't send it if we don't have one
  if( ! objString.isEmpty() )
  {
    parameters << objString;
  }

  sendCommand( "CHG", parameters.join( " " ) );
}



/**
 * @brief Close the connection with the server
 *
 * This disconnects from the server. It also cleans up the
 * state variables and objects associated with the session,
 * e.g. contactlist data, open chat invitations, pending offline-im messages and SOAP clients.
 */
void MsnNotificationConnection::closeConnection()
{
  debug() << "Closing the connection.";

  // Stop the login timer
  loginTimer_.stop();

  // Delete all SOAP clients, and unset local references here.
  deleteSoapClients();
  offlineImService_       = 0;
  passportLoginService_   = 0;
  roamingService_         = 0;
  isOnline_               = false;
  isInitiatingConnection_ = false;

  emit statusEvent( KMess::SessionMessage, KMess::SessionDisconnected );

  // Disconnect from the server.
  // emits the signals so other classes can clean up / save properties.
  disconnectFromServer();

  // Clear all pending offline messages
  pendingOfflineImMessages_.clear();

  // Delete all offline im messages
  qDeleteAll( offlineImMessages_ );
  offlineImMessages_.clear();

  // Delete all open requests
  openRequests_.clear();

  debug() << "Closed the connection.";
}



/**
 * @brief Internal function to create the Offline-IM service
 *
 * This service is used to retrieve the Offline-IM messages.
 * The signals are attached to receivedOfflineIm() and receivedMailData()
 *
 * @returns  The Offline-IM webservice handler
 */
OfflineImService* MsnNotificationConnection::createOfflineImService()
{
  // Extract the 't' and 'p' values from the current authentication ticket string
  // This is done beacuse sendMessage() could has requested one new ticket
  const QStringList& fields( session_->sessionSettingString( "SoapTokenMessenger" ).split('&') );
  QString authT, authP;

  foreach( const QString &field, fields )
  {
    if( field.startsWith( QLatin1String("t=") ) )
    {
      authT = field.mid(2);
    }
    else if( field.startsWith( QLatin1String("p=") ) )
    {
      authP = field.mid(2);
    }
    else
    {
      warning() << "Could not parse authentication field:" << field;
    }
  }

  // Request the first offline-im message
  // TODO reuse the pointer, and set the token like addressbook
  OfflineImService *oimService = new OfflineImService( authT, authP, session_, this );

  // Add the soap client.
  addSoapClient( oimService );

  // Register signals
  connect( oimService, SIGNAL(   messageReceived(QString,QString,QString,QDateTime,QString,QString,int) ),
           this,       SLOT  ( receivedOfflineIm(QString,QString,QString,QDateTime,QString,QString,int) ) );
  connect( oimService, SIGNAL(  metaDataReceived(QDomElement)                                           ),
           this,       SLOT  (  receivedMailData(QDomElement)                                           ) );

  return oimService;
}



/**
 * @brief Internal function to create the passport login handler.
 *
 * This service is used by gotUsr() to retrieve the passport authentication token.
 *
 * @returns  The Passport login webservice handler
 */
PassportLoginService* MsnNotificationConnection::createPassportLoginService()
{
  // Create the login handler.
  if( passportLoginService_ == 0 )
  {
    passportLoginService_ = new PassportLoginService( session_ );

    // Add the soap client.
    addSoapClient( passportLoginService_ );

    // Connect all login notifications
    connect( passportLoginService_, SIGNAL( loginIncorrect() ),
             this,                  SLOT  ( loginIncorrect() ) );
    connect( passportLoginService_, SIGNAL( loginSucceeded() ),
             this,                  SLOT  ( loginSucceeded() ) );
  }

  return passportLoginService_;
}



/**
 * @brief Internal function to create the Roaming Service
 *
 * This service is used to fetch the personal message and picture from the server.
 *
 * @returns  The RoamingService handler
 */
RoamingService* MsnNotificationConnection::createRoamingService()
{
  // Create the login handler.
  if( roamingService_ == 0 )
  {
    roamingService_ = new RoamingService( session_, this );

    // Add the soap client.
    addSoapClient( roamingService_ );
  }

  return roamingService_;
}



/**
 * @brief  Called when the user is ready to go online.
 *
 * This method is called by gotSyn() or gotLst() when
 * the entire contact list data is received.
 * It changes some final things before the user goes online:
 * - set the initial user status, e.g. "online" or "invisible".
 * - ses the initial personal message.
 * - requests other server data, like Hotmail email URL's.
 * Finally, the loggedIn() signal is fired.
 */
void MsnNotificationConnection::goOnline()
{
  KMESS_NULL( session_ );

  // Stop the authentication timer, we're done with that part
  loginTimer_.stop();

  // Start the ping timer: from now on, it's its duty to verify that the connection is still active
  setSendPings( true );

  MsnStatus initialStatus = (MsnStatus) session_->sessionSettingInt( "StatusInitial" );

  debug() << "Setting initial status to" << InternalUtils::statusToCode( initialStatus );

  // Set the personal message, and send the initial endpoint data
  putUux( true );

  // Force to update the msn object and post the current status
  changeStatus( initialStatus );

  // Request the email access URLs (only for Live Mail-enabled accounts)
  sendMailUrlRequest();

  isOnline_ = true;

  // check the logon mode and boot other endpoints if needed.
  if ( self_->logonMode() == LogonSingle )
  {
    logoutEverywhereElse();
  }

  // Notify observers that the server is connected
  emit loggedIn();
}



// Received response to adl command
void MsnNotificationConnection::gotAdl( const QStringList& command, const QByteArray& payload )
{
  // The server responds to a successful ADL command
  // The OK ack is received to reply to initial ADL and to others ADL
  if( command[2] == "OK" )
  {
    // Check if the OK is reply to initial ADL
    if( ! isOnline_ )
    {
      // Go online
      goOnline();

      emit statusEvent( KMess::SessionMessage, KMess::SessionConnected );
    }

    return;
  }

  if( ! payload.isEmpty() )
  {
    QRegExp regexp( ".*n=\"([^\"]+)\".*n=\"([^\"]+)\".*l=\"([^\"]+)\"" );

    int pos = regexp.indexIn( payload );
    int lists = -1;
    QString handle;

    if( pos != -1 )
    {
      handle = regexp.cap(2) + '@' + regexp.cap(1);
      lists = QVariant( regexp.cap(3) ).toInt();
    }

    if( lists == -1 || handle.isEmpty() )
    {
      return;
    }

    MsnContact *contact = contactList_->contact( handle );
    if( contact != 0 )
    {
      MsnMemberships memberships;
      if( lists & MSN_LIST_FRIEND  ) memberships &= MSN_LIST_FRIEND;
      if( lists & MSN_LIST_ALLOWED ) memberships &= MSN_LIST_ALLOWED;
      if( lists & MSN_LIST_BLOCKED ) memberships &= MSN_LIST_BLOCKED;
      if( lists & MSN_LIST_REVERSE ) memberships &= MSN_LIST_REVERSE;
      if( lists & MSN_LIST_PENDING ) memberships &= MSN_LIST_PENDING;
      contact->setMemberships( memberships );
    }
    else
    {
      // create a new "unknown" contact.
      contact = contactList_->listsManager()->contact( handle, true /* create */ );

      // Else if the contact isn't present, ask to user what he want to do.
      debug() << "The contact: " << contact << " has added us to their contact list.";

      emit contactAddedUser( contact );
    }
  }
}



// Received confirmation of the user's status change
void MsnNotificationConnection::gotChg( const QStringList& command )
{
  KMESS_NULL( session_ );

  const QString &newStatus( command[2] );

  debug() << "User's status changed to:" << newStatus;
  
  self_->setStatus( InternalUtils::statusFromCode( newStatus ) );

  // Update the display picture too, if present
  QString msnObjectString;

  if( command.size() == 5 )
  {
    msnObjectString = QUrl::fromPercentEncoding( command[4].toAscii() );
    self_->setMsnObject( KMess::MsnObject( msnObjectString ) );
  }
}



// Received a challenge from the server
void MsnNotificationConnection::gotChl( const QStringList& command )
{
  debug() << "Answering challenge.";

  // Handle challenge query
  MSNChallengeHandler handler;
  const QString& response( handler.computeHash( command[2] ) );

  // Send response
  sendCommand( "QRY", handler.getProductId(), response );
}



// Received a version update from the server
void MsnNotificationConnection::gotCvr( const QStringList& command )
{
  Q_UNUSED( command );
  KMESS_NULL( session_ );

  // Set the connected status to false
  isOnline_ = false;

  // Send the USR command
  sendCommand( "USR", "SSO I " + session_->self()->handle() );
}



// Received notice that a contact went offline
void MsnNotificationConnection::gotFln( const QStringList& command )
{
  KMESS_NULL( session_ );

  QString             handle;
  KMess::NetworkType  networkId;
  QString             clientCaps( command[2] );

  InternalUtils::getHandle( command[1], handle, networkId );

#ifdef __GNUC__
#warning networkId ignored for now
#endif

  debug() << handle << "went offline.";

  MsnContact *contact = contactList_->contact( handle );
  contact->setStatus( OfflineStatus );
  contact->setCapabilities( clientCaps );

  emit contactOffline( contact );
}



// Received notice that a contact was already online
void MsnNotificationConnection::gotIln( const QStringList& command )
{
  KMESS_NULL( session_ );

  QString            handle;
  KMess::NetworkType networkId;

  // The MsnObject can be positioned differently, depending on the command format
  // TODO: Describe the two different cases
  int msnObjectPos = 4;
  if( command.length() == 6 )
  {
    msnObjectPos = 5;
  }

  // Get the contact info from the message

  InternalUtils::getHandle( command[2], handle, networkId );

  MsnStatus      status      = InternalUtils::statusFromCode( command[1] );
  const QString &friendlyName( QUrl::fromPercentEncoding( command[3].toUtf8() ) );
  QString        clientCaps  ( command[4] );
  QString        msnObject   ( QUrl::fromPercentEncoding( command[msnObjectPos].toUtf8() ) );

#ifdef __GNUC__
#warning networkId ignored for now
#endif
  Q_UNUSED( networkId );

  // Check if the received MSN object is empty, a "0" (Kopete), or shorter than the string "<msnobject/>"
  if( msnObject.length() < 12 || msnObject == "0" )
  {
    msnObject.clear();
  }

  if( msnObject.isEmpty() )
  {
    debug() << "Contact" << handle << "was initially connected with status" << command[2]
             << "and no MsnObject";
  }
  else
  {
    debug() << "Contact" << handle << "was initially connected with status" << command[2]
             << "and MsnObject" << msnObject;
  }

  MsnContact *contact = contactList_->contact( handle );
  contact->_setDisplayName( friendlyName );
  contact->setCapabilities( clientCaps );
  contact->setMsnObject( msnObject );
  contact->setStatus( status );

  emit contactOnline( contact, true );
}



/**
 * @brief A message has been received.
 *
 * Messages are in MIME format, and can convey different types of data,
 * like new mail notifications (from Live Mail).
 *
 * @param command The command
 * @param payload Data conveyed by the command
 */
void MsnNotificationConnection::gotMsg( const QStringList& command, const QByteArray &payload )
{
  Q_UNUSED( command );

  Message message = Message::fromData( 0, DirectionIncoming, payload );

  const QString& contentType( message.field( "Content-Type", QString() ) );

  debug() << "Got a" << contentType << "MIME message.";

  // Check the message type
  if ( contentType == "text/x-msmsgsprofile" )
  {
    debug() << "Loading profile information.";

    // The port is byte-swapped
    uint externalPort = message.field( "ClientPort" ).toUInt();
    externalPort      = ( (externalPort & 0x00ff) << 8 ) | ( (externalPort & 0xff00) >> 8 );

    SettingsList settings;
    settings[ "LocalIp"        ] = getLocalIp();
    settings[ "PublicPort"     ] = externalPort;
    settings[ "PublicIp"       ] = message.field( "ClientIP" );
    settings[ "LanguageCode"   ] = message.field( "lang_preference" );
    settings[ "PreferredEmail" ] = message.field( "preferredEmail" );
    settings[ "SessionId"      ] = message.field( "sid" );
    settings[ "Country"        ] = message.field( "country" );
    settings[ "EmailEnabled"   ] = message.field( "EmailEnabled" ) == "1";
    session_->setSessionSettings( settings );
  }
  else if( contentType == "text/x-msmsgsinitialmdatanotification"
       ||  contentType == "text/x-msmsgsinitialemailnotification"
       ||  contentType == "text/x-msmsgsoimnotification" )
  {
    // Parse these messages the same way,
    // the x-msmsgsinitialmdatanotification message is an extended version of x-msmsgsoimnotification
    // description of the fields: http://msnpiki.msnfanatic.com/index.php/MSNP13:Offline_IM

    // Parse the URL fields (msmsgsinitialmdatanotification messages).
    if( message.hasField( "Post-URL" ) )
    {
      session_->setSessionSetting( "UrlWebmail", message.field( "Post-URL" ) );
    }

    // Parse the Mail-Data field.
    const QString& mailDataSrc( message.field( "Mail-Data" ) );
    if( mailDataSrc == "too-large" )
    {
      // After a certain amount of messages, the XML is replaced with "too-large".
      // Use SOAP to request the actual mail data.
      if( offlineImService_ == 0 )
      {
        offlineImService_ = createOfflineImService();
      }
      offlineImService_->getMetaData();
    }
    else
    {
      // Mail-Data was received directly in the message.
      QString error;
      QDomDocument mailData;
      int errorLine = 0, errorColumn = 0;
      if( ! mailData.setContent( mailDataSrc, false /*NamespaceProcessing*/, &error, &errorLine, &errorColumn ) )
      {
        warning() << "Could not parse XML Mail-Data field!";
        warning() << "XML error:" << error << "at line" << errorLine << "column" << errorColumn;
        return;
      }

      receivedMailData( mailData.namedItem("MD").toElement() );
    }
  }
  else if ( contentType == "text/x-msmsgsemailnotification" )
  {
    debug() << "An email was received!"
             << "From:"      << message.field( "From" )
             << "- Subject:" << message.field( "Subject" )
             << "- Folder:"  << message.field( "Dest-Folder" );

    // If a message delta is to the active folder, emails were moved into it, so use a negative deletion
    if ( message.field( "Dest-Folder" ) == "ACTIVE" )
    {
      // Received in the inbox
      session_->setEmailCount( 1 );
    }

    bool isIntoInbox = ( message.field( "Dest-Folder" ) == "ACTIVE" );
    emit newEmail( InternalUtils::decodeRFC2047String( message.field( "From" ) ),
                   InternalUtils::decodeRFC2047String( message.field( "Subject" ) ),
                   isIntoInbox,
                   message.field( "id" ),
                   message.field( "Message-URL" ),
                   message.field( "Post-URL" ) );
  }
  else if( contentType == "text/x-msmsgsactivemailnotification" )
  {
    int messageDelta = message.field( "Message-Delta" ).toInt();

    // If a message delta is from the active folder, emails were deleted
    if ( message.field( "Src-Folder" ) == "ACTIVE" )
    {
      session_->setEmailCount( -1 * messageDelta );
    }
    // If a message delta is to the active folder, emails were moved into it
    else if ( message.field( "Dest-Folder" ) == "ACTIVE" )
    {
      session_->setEmailCount( messageDelta );
    }

    // When deleting Offline-IM messages, this message contains:
    // Src-Folder: .!!OIM
    // Dest-Folder: .!!trAsH
    // Message-Delta: <number of messages deleted>
  }
  else if( contentType == "application/x-msmsgssystemmessage" )
  {
     // This is a maintenance info message from the server.
    if( message.field( "Type" ) == "1" )
    {
      int timeLeft = message.field( "Arg1" ).toInt();
      emit statusEvent( KMess::WarningMessage, KMess::WarningServerGoingDownSoon, timeLeft );
    }
  }
  else
  {
    warning() << "Content-Type" << contentType << "not reconized, message ignored:";
    warning() << payload;
  }
}



/**
 * @brief Handle the <code>NLN</code> command; a contact changed it's status.
 *
 * When a user updates it's status with the <code>CHG</code> command,
 * all it's contact receive a <code>NLN</code> command.
 * KMess sends the <code>CHG</code> command with the changeStatus() method.
 * The <code>CHG</code> allows a contact to change it's name, client capabilities or MsnObject.
 *
 * The capabilities are a bitwise flag. The list of known values
 * are described in the KMess::MsnClientCapabilities enum.
 *
 * The MsnObject is an identifier for the display picture.
 * To download the actual picture, a client needs to initiate a chat,
 * and send an invitation there.
 *
 * @code
<<< NLN NLN 1:user@kmessdemo.org KMess%20Demo 1342210108:0 %3Cmsnobj%20Creator%3D%22user%40kmessdemo.org%22%20Size%3D%2211581%22%20Type%3D%223%22%20Location%3D%22KMess.tmp%22%20Friendly%3D%22AA%3D%3D%22%20SHA1D%3D%223VfOMCTTkuYHDjJhPx4sSlPiM%2Bs%3D%22%20SHA1C%3D%22ULeT2WGsdabLGNP3RgbWYw7ve80%3D%22/%3E
@endcode
 *
 * @param  command  The command arguments.
 *                  - <code>command[1]</code> is the status code, see changeStatus()
 *                  - <code>command[2]</code> is the [networkid]:[contact-handle] pair.
 *                  - <code>command[3]</code> is the contact name, url encoded.
 *                  - <code>command[4]</code> is the [client-capabilities]:[client-extended-capabilities] pair.
 *                  - <code>command[5]</code> is the url-encoded MsnObject XML code.
 */
void MsnNotificationConnection::gotNln( const QStringList& command )
{
  KMESS_NULL( session_ );

  // Get the contact info from the message

  MsnStatus status = InternalUtils::statusFromCode( command[1] );
  const QString &idString( command[2] );
  const QString  clientCaps( command[4] );
  const QString &friendlyName( QUrl::fromPercentEncoding( command[3].toUtf8() ) );

  // If the MsnObject is available, get it
  QString msnObject;
  if( command.size() == 6 )
  {
    msnObject = QUrl::fromPercentEncoding( command[5].toUtf8() );

    // Check if the contact removed his/her picture
    if( msnObject.length() < 12 || msnObject == "0" )
    {
      msnObject.clear();
    }
  }

  if( msnObject.isEmpty() )
  {
    debug() << "Contact" << idString << "went online with status" << command[1]
             << "and no MsnObject";
  }
  else
  {
    debug() << "Contact" << idString << "went online with status" << command[1]
             << "and MsnObject" << msnObject;
  }

  // Update contact status.
  MsnContact *contact = contactList_->contact( idString );

  KMESS_ASSERT( contact );

  MsnStatus lastStatus = contact->status();

  contact->_setDisplayName( friendlyName );
  contact->setCapabilities( clientCaps );
  contact->setMsnObject( msnObject );
  contact->setStatus( status );

  if ( lastStatus == OfflineStatus )
  {
    // contact has gone online
    emit contactOnline( contact );
  }
  else
  {
    // contact has just changed status.
    emit contactChangedStatus( contact, status );
  }

  // we get information about our own status as well...
  if ( idString.contains( session_->self()->handle() ) )
  {
    contact = session_->self();
    contact->_setDisplayName( friendlyName );
    contact->setStatus( status );
  }
}



// Received notice of disconnection from the server
void MsnNotificationConnection::gotOut( const QStringList& command )
{
  // If the first command parameter is "OTH", it means that this account has connected from elsewhere.
  if( command[1] == "OTH" )
  {
    debug() << "Got notice that this account has connected from elsewhere.";

    emit statusEvent( KMess::ErrorMessage, KMess::ErrorConnectedFromElsewhere );

    // pre-empt the server shutting the connection. get in first so we emit the
    // correct disconnection reason
    disconnectFromServer( MsnSocketBase::EmitSignals, KMess::OtherLocationDisconnect );
  }
  else
  {
    debug() << "Received notice of disconnection from the server.";

    closeConnection();
  }
}



// Received a property change (friendly name, phone number, etc..)
void MsnNotificationConnection::gotPrp( const QStringList& command )
{
  const QString& propertyName ( command[2] );
  const QString& propertyValue( QUrl::fromPercentEncoding( command[3].toUtf8() ) );

  if( propertyName == "MFN" )
  {
    self_->_setDisplayName( propertyValue );

    // update the roaming service.
    createRoamingService()->updateProfile();
  }
  else if( propertyName == "MBE" )
  {
    // Unimplemented.
    // indicates whether MSN Mobile is enabled.
  }
  else if( propertyName == "WWE" )
  {
    // Unimplemented
    // indicates whether MSN Direct is enabled.
  }
  else
  {
   warning() << "Received unsupported property name/value: " << propertyName << " " << propertyValue << "!";
  }
}



void MsnNotificationConnection::gotRml( const QStringList &command )
{
   // The server responds to a successful RML command
  if( command[2] == "OK" )
  {
    // TODO Signal the application
  }
}



/**
 * @brief Received a connection request from a contact
 *
 * Format at MSNP15:
 * RNG 1295917726 65.54.228.15:1863 CKI 104194101.9520122 email@domain.com Friendly%20Name U messenger.msn.com 1
*/
void MsnNotificationConnection::gotRng( const QStringList& command )
{
  // Pull the data from the message
  const QString& chatId       ( command[1] );
  const QString& authorization( command[4] );
  const QString& handle       ( command[5].toLower() );

  const QString& serverAndPort( command[2] );
  const QString& server       ( serverAndPort.section( ':', 0, 0 ) );
  const QString& portString   ( serverAndPort.section( ':', 1, 1 ) );

  bool goodPort;
  quint16 port = (quint16) portString.toUInt( &goodPort );

  if( ! goodPort )
  {
    warning() << "Couldn't get port from string:" << serverAndPort;
    return;
  }

  debug() << "Chat session request received from" << handle << "on server"
           << serverAndPort << "(id:" << chatId << "auth:" << authorization << ")";

  // Signal the newly received invitation to ChatFactory
  emit invitedToChat( handle, server, port, authorization, chatId );
}



// received a Yahoo! message
void MsnNotificationConnection::gotUbm( const QStringList &command, const QByteArray &payload )
{
  // upon receipt of a UBM, determine what type of message it holds.
  // Type 1: TextMessage
  // Type 2: TypingMessage
  // Type 3: NudgeMessage
  QString sourceHandle;
  NetworkType sourceNetwork;
  
  QString targetHandle;
  NetworkType targetNetwork;
  
  int messageType;
  
  sourceHandle = command[1];
  sourceNetwork = ( command[2] == "1" ? NetworkMsn : NetworkYahoo );
  
  targetHandle = command[3];
  targetNetwork = ( command[4] == "1" ? NetworkMsn : NetworkYahoo );
  
  messageType = command[5].toInt();
  
  MsnContact *source = contactList_->listsManager()->contact( sourceHandle, true /* create unknown */ );
//   MsnContact *dest = contactList_->listsManager()->contact( targetHandle, true /* create unknown */ );
  
  if ( source->network() != sourceNetwork )
  {
    source->setNetwork( sourceNetwork );
  }

  Message inner = Message::fromData( source, DirectionIncoming, payload );
  
  YahooMessage yMsg( inner );
  yMsg.setSourceHandle( sourceHandle );
  yMsg.setDestHandle( targetHandle );

  emit receivedMessage( yMsg );
}



// UBN 12 account{guid} {1: xml data, 2: SIP invite, 3: MSNP2P SLP data, 4: logout, 6: refresh lists? } [PayloadLength]
void MsnNotificationConnection::gotUbn( const QStringList &command, const QByteArray &payload )
{
  int action = command[2].toInt();
  switch( action )
  {
    case 6:
    {
      QString data( payload );
      if ( data.contains( "type=\"ab\"" ) )
      {
        debug() << "UBN request to refresh address book";
        contactList_->abService_->retrieveAddressBook( AddressBookService::RefreshOnly );
      }
      else if ( data.contains( "type=\"membership\"" ) )
      {
        debug() << "UBN request to refresh memberships";
        contactList_->abService_->retrieveMemberships( AddressBookService::RefreshOnly );
      }
      else
      {
        debug() << "Unknown UBN refresh request";
      }

      break;
    }

    default:
      warning() << "Unknown UBN action:" << action;
      break;
  }
}



// Received information about a contact's personal message
void MsnNotificationConnection::gotUbx( const QStringList &command, const QByteArray &payload )
{
  QString     handle;
  KMess::NetworkType networkId;
  InternalUtils::getHandle( command[1], handle, networkId );

#ifdef __GNUC__
#warning networkId ignored for now
#endif
  Q_UNUSED( networkId );

  debug() << "Got UBX information for contact" << handle;

  MsnContact *contact = contactList_->contact( handle );

  KMESS_NULL( contact );

  if( command[2].toInt() == 0 )
  {
    // Size is 0, no personal message
    contact->_setPersonalMessage( QString() );
  }
  else
  {
    // Load XML parser
    QDomDocument xml;
    if( ! xml.setContent(payload) )
    {
      warning() << "Could not parse UBX data for contact" << handle << ", invalid XML!";
      warning() << "The invalid XML was:" << payload;
    }

    // Parse payload
    const QDomElement&      root( xml.namedItem("Data").toElement() );
    const QString&          psm( root.namedItem("PSM").toElement().text() );
          QString  currentMedia( root.namedItem("CurrentMedia").toElement().text() );
    QString mediaType;

    // endpoint data
    QDomNodeList endpointData = root.elementsByTagName( "EndpointData" );
    QDomNodeList privEndpoint = root.elementsByTagName( "PrivateEndpointData" );
    QList<MPOPEndpoint *> endpoints;

    for( int i = 0; i < endpointData.count(); i++ )
    {
      // firstly: does the contact
      QDomElement node = endpointData.at( i ).toElement();
      QDomElement privNode = privEndpoint.at( i ).toElement();

      QString id = node.attribute( "id" );
      QString caps = node.firstChildElement( "Capabilities" ).toElement().text();

      // now get the private endpoint data.
      QString name = privNode.namedItem( "EpName" ).toElement().text();
      QString idle = privNode.namedItem( "Idle" ).toElement().text();
      QString type = privNode.namedItem( "ClientType" ).toElement().text();

#ifdef __GNUC__
#warning TODO: MPOP: Idle, ClientType, State.
#endif
      endpoints.append( new MPOPEndpoint( contact, QUuid( id ), name ) );

    }

    // old endpoints will be deleted, new ones added, existing ones updated.
    contact->setEndpoints( endpoints );

    MediaData media;
    media.type = MediaNone;
    
    // Append current media if set.
    if( currentMedia.length() > 0 )
    {
      // From http://msnpiki.msnfanatic.com/index.php/MSNP11:Changes:
      //
      // Media string can be something like:
      //  <CurrentMedia>\0Music\01\0{0} - {1}\0Title\0Artist\0Album\0\0</CurrentMedia>
      //  <CurrentMedia>\0Games\01\0Playing {0}\0Game Name\0</CurrentMedia>
      //  <CurrentMedia>\0Office\01\0Office Message\0Office App Name\0</CurrentMedia>
      //
      // The literal "\0" is the separator char.
      // Fields are: application, mediatype, enabled, formatstring, arg1, arg2, arg3

      // Split media string
      const QStringList parts( currentMedia.split( "\\0" ) );

      media.type = InternalUtils::stringToMediaType( parts[1] );
      media.format = parts[3];
      for ( int i = 4; i < parts.count(); i++ )
      {
        media.formatArgs << parts[i];
      }
    }

    // Store personal message
    contact->_setPersonalMessage( psm );
    contact->setMedia( media );
  }
}



/**
 * Process an utility URL from the server.
 *
 * Received the folder and command info for Live Mail's inbox or compose pages.
 * The response is made like this:
 * <code>URL 68 /cgi-bin/compose https://login.live.com/ppsecure/md5auth.srf?lc=1040 2</code>
 * - 68 is the ack number, used to map the received URL to the right URL name from the global
 *   session object (see goOnline())
 * - /cgi-bin/compose is the url command for the specified service
 * - the last url is the website where to perform the authentication.
 *   Note that KMess uses the sha1auth site authentication URL instead of this md5 one,
 *   so storing this last url is useless!
 */
void MsnNotificationConnection::gotUrl( const QStringList& command )
{
  KMESS_NULL( session_ );

  const QString &ackNumber( command[1] );

  // Compare the ack number to find out which url is this
  if( ackNumber == session_->sessionSettingString( "UrlWebmailInbox" ) )
  {
    debug() << "Got URL: Webmail Inbox";
    session_->setSessionSetting( "UrlWebmailInbox", command[2] );
  }
  else
  if( ackNumber == session_->sessionSettingString( "UrlProfile" ) )
  {
    debug() << "Got URL: Profile";
    session_->setSessionSetting( "UrlProfile", command[2] );
  }
  else
  if( pendingComposeRequests_.contains( ackNumber.toInt() ) )
  {
    QString addr = pendingComposeRequests_.take( ackNumber.toInt() );
    if ( ! addr.isEmpty() )
    {
      emit gotComposeUrl( addr, command[2] );
    }
  }
  else
  {
    warning() << "Got unknown URL:" << command[2];
  }
}



/**
 * @brief Received a USR command from the server.
 *
 * USR command for the login. At first we must send the handle to log into.
 * When the server response to first USR, we take the nonce value from it,
 * we encrypt it, and send it back to finish the login process.
 * The key used for encrypting the data (3DES) is taken from Passport
 * SOAP request.
 */
void MsnNotificationConnection::gotUsr( const QStringList& command )
{
  KMESS_NULL( session_ );

  const QString& step( command[2] );

  // This is either asking for authentication or confirming it
  if( step == "SSO" )
  {
    // The initial connection process (network connection establish and version exchange) is over.
    // The MSN servers sometimes fail to warn the clients that something's wrong by their side and
    // they close the connection before the USR. This is a nice workaround: if the connection gets
    // closed before this point, we just try again with HTTP, which usually works.
    isInitiatingConnection_ = false;

    const QString& policy ( command[4] ); // Policy
    nonceBase64_          = command[5].toAscii();   // Base64 nonce

    // Change statusbar
    emit statusEvent( KMess::ProgressMessage, KMess::ProgressAuthenticating );

    // Start the passport based login
    PassportLoginService *passportLoginService = createPassportLoginService();
    passportLoginService->login( policy );
  }
  else if( step == "OK" )
  {
    // The authentication has been confirmed

    // Is the Passport account already verified?
    bool isVerified = ( command[4].toInt() == 1 );
    self_->_setPassportVerified( isVerified );

    if( ! isVerified )
    {
      warning() << "Passport account of the user is not verified yet.";
    }

    // restart the timer, since this might take a while
    // (timer should already be active at this point)
    loginTimer_.start();

    emit statusEvent( KMess::ProgressMessage, KMess::ProgressFetchingAddressBook );
    
    // download the address book and memberships.
    contactList_->fetch( cacheDir_ );
  }
  else
  {
    warning() << "Received unexpected USR step:" << step;
  }
}



// Received version information
void MsnNotificationConnection::gotVer( const QStringList& command )
{
  // If the server supports none of the given protocols the server will respond with a protocol of 0,
  // and will then disconnect.
  if( command[2] == "0" )
  {
    warning() << "The server doesn't support any protocol";
    closeConnection();

    emit statusEvent( KMess::FatalMessage, KMess::FatalNoSupportedProtocols );
    return;
  }

  KMESS_NULL( session_ );

  // Send some fake info about the current version
  // First parameter is the locale-id (0x0409 is U.S. English).
  // TODO FIXME: send correct information - don't lie about the client, and use
  //             the correct locale, picked from these: http://msdn.microsoft.com/en-us/goglobal/bb964664.aspx
  sendCommand( "CVR", "0x0409 Linux 2.6.28 i686 KMess 2.1 kmess-messenger " + self_->handle() );
}



/**
 * @brief Received server transfer information
 *
 * Format at MSNP15:
 * XFR 32 SB 65.54.171.31:1863 CKI 1743299383.52212212.219110167 U messenger.msn.com 1
 */
void MsnNotificationConnection::gotXfr( const QStringList& command )
{
  // Get the id of this transaction
  int transactionId = command[1].toInt();

  // Get the type and address of the server
  const QString& serverType   ( command[2] );
  const QString& serverAndPort( command[3] );
  const QString& server       ( serverAndPort.section( ':', 0, 0 ) );
  const QString& portString   ( serverAndPort.section( ':', 1, 1 ) );

  // Convert the port to an integer
  bool goodPort;
  quint16 port = (quint16) portString.toUInt( &goodPort );

  debug() << "Got" << serverType << "transfer to" << serverAndPort;

  if( ! goodPort )
  {
    debug() << "Got bad port number:" << portString;
    return;
  }

  // We received a transfer request to another notification server
  if( serverType == "NS" )
  {
    debug() << "Disconnecting from old server";

    // Disconnect from the existing server
    disconnectFromServer( MsnSocketBase::NoSignals );

    debug() << "Connecting to new server";

    // Stop the login timer: it'll be restarted while connecting to the new server
    loginTimer_.stop();

    // Set the connection as initiating, connectToServer() is from the parent class
    // and can't do it itself
    isInitiatingConnection_ = true;

    // Connect to the new server.
    connectToServer( server, port );
  }
  else if( serverType == "SB" )
  {
    int noChatsBefore = openRequests_.count();
    KMESS_ASSERT( noChatsBefore > 0 );

    // This is a switchboard server response: this command is a response to
    // a user-requested chat request. Look for the requesting chat
    QPointer<MsnChat> msnChat = openRequests_.value( transactionId, 0 );
    if( msnChat == 0 )
    {
      warning() << "Got an XFR response, but there are no corresponding open requests!";
      openRequests_.take( transactionId );
      return;
    }

    debug() << "Notificating about the new session...";

    // Signal the new connection availability to the requesting chat
    msnChat->initiateSession( server, port, command[5] /* chat authorization token */ );

    // Remove the request from the pending list
    openRequests_.take( transactionId );

    KMESS_ASSERT( openRequests_.count() == ( noChatsBefore - 1 ) );
  }
}



/**
 * @brief Send the ADL command.
 *
 * ADL: 'Add List' command, uses XML to identify each contact, and works as a payload message.
 * Each ADL command may contain up to 150 contacts (a payload of roughly 7500 bytes).
 *
 */
void MsnNotificationConnection::putAdl( KMess::MsnContact *contact, KMess::MsnMemberships lists )
{
  // If handle is present then the adl isn't initial ADL command, so use command for add someone into one list
  if( contact )
  {
    const QStringList& splittedHandle( contact->handle().split( '@' ) );

    const QString payload( "<ml>"
                            "<d n=\"" + splittedHandle[1] + "\">"
                              "<c n=\"" + splittedHandle[0] + "\" l=\"%1\" t=\"%2\" />"
                            "</d>"
                           "</ml>" );

    lists = lists & ( MSN_LIST_FRIEND | MSN_LIST_ALLOWED | MSN_LIST_BLOCKED );
    sendCommand( "ADL", QString(), payload.arg( QString::number( lists ) ).arg( contact->network() ) );
    return;
  }

  // Use the current contact list to make the string to send with initial ADL command
  const ContactsList& contactList( contactList_->contacts() );

  // If the list is empty, send an empty ADL
  if( contactList.isEmpty() )
  {
    sendCommand( "ADL", QString(), QByteArray( "<ml l=\"1\"></ml>" ) );
    return;
  }

  ContactsListIterator i( contactList );
  MsnContact *currentContact = 0;

  QString adlList;
  QString currentAdl;
  bool send = false;

  while( i.hasNext() )
  {
    currentContact = i.next();

    // if we send REVERSE memberships we'll get error 241.
    MsnMemberships list = currentContact->memberships() & ( MSN_LIST_FRIEND | MSN_LIST_BLOCKED | MSN_LIST_ALLOWED );

    // Add to ADL command only user in at least one list
    if( list != 0 )
    {
      // Split the domain from handle
      const QStringList& splittedHandle( currentContact->handle().split( '@' ) );

      currentAdl = "<d n=\"" + splittedHandle[1] + "\">"
                     "<c n=\"" + splittedHandle[0] + "\" "
                        "l=\"" + QString::number( list ) + "\" "
                        "t=\"" + QString::number( currentContact->network() ) + "\" />"
                   "</d>";

      // Check if the buffer is too large ( 7500 bytes maximum )
      if( currentAdl.toLatin1().size() + adlList.toLatin1().size() >= 7450 )
      {
        // Switch to true the send variable and go back to previous element
        send = true;
        i.previous();
      }
      else
      {
        adlList += currentAdl;
      }
    }

    // Send the buffer if the send variable is true or the current contact is the last of the list
    if( send || ! i.hasNext() )
    {
      sendCommand( "ADL", QString(), "<ml l=\"1\">" + adlList + "</ml>" );
      adlList = "";
      send = false;
    }
  }
}



// Send the RML command
void MsnNotificationConnection::putRml( KMess::MsnContact *contact, KMess::MsnMemberships lists )
{
  const QStringList& splittedHandle( contact->handle().split( '@' ) );

  const QString payload( "<ml>"
                          "<d n=\"" + splittedHandle[1] + "\">"
                            "<c n=\"" + splittedHandle[0] + "\" l=\"%1\" t=\"%2\" />"
                          "</d>"
                         "</ml>" );

  lists = lists & ( MSN_LIST_FRIEND | MSN_LIST_ALLOWED | MSN_LIST_BLOCKED );
  sendCommand( "RML", QString(), payload.arg( QString::number( lists ) ).arg( contact->network() ) );
}




/**
 * @brief The passport login succeeded.
 *
 * When this method gets called, the global session object will contain all
 * the needed passport security tokens.
 */
void MsnNotificationConnection::loginSucceeded()
{
  // Delete the login handler
  // Delay deletion so it can complete the method.
  deleteSoapClient( passportLoginService_ );
  passportLoginService_->deleteLater();
  passportLoginService_ = 0;

  // Create the algorithm support class
  QByteArray blobResult;
  KMess::MsnAlgorithms algorithms;
  QByteArray authToken( session_->sessionSettingString( "SoapTokenMessengerClearProof" ).toAscii() );
  int result = algorithms.createAuthBlob( blobResult, nonceBase64_, authToken );

  if( result == KMess::ErrorComputeHashFailed )
  {
    emit statusEvent( KMess::ErrorMessage, KMess::ErrorComputeHashFailed );
    return;
  }

  sendCommand( "USR", "SSO S " +
                      session_->sessionSettingString( "SoapTokenMessengerClear" ) +
                      ' ' +
                      blobResult.toBase64() +
                      ' ' +
                      self_->localEndpoint()->guid() );
}



// The passport login failed, username/password was incorrect
void MsnNotificationConnection::loginIncorrect()
{
  debug() << "Passport login was incorrect.";

  // NOTE: a similar thing also happens in soapRequestFailed(),
  // but for fatal SOAP errors.

  // Stop the timer
  loginTimer_.stop();

  // close the connection
  disconnectFromServer( MsnSocketBase::NoSignals );

  // Emit authentication failure
  emit statusEvent( KMess::ErrorMessage, KMess::ErrorInvalidUserCredentials );
  emit connectionFailed( KMess::ErrorInvalidUserCredentials );

  // Delete the login handler
  // Delay deletion so it can complete the method.
  deleteSoapClient( passportLoginService_ );
  passportLoginService_->deleteLater();
  passportLoginService_ = 0;
}




/**
 * Logout from everywhere else.
 */
void MsnNotificationConnection::logoutEverywhereElse()
{
  sendUUN( session_->self()->handle(), 4, "goawyplzthxbye-nomorempop" );
}



// Open a connection to the server
void MsnNotificationConnection::openConnection()
{
  debug() << "Opening connection.";

  // If the last connection was using HTTP, try again using TCP first.
  switchToTcpSocket();

  emit statusEvent( KMess::ProgressMessage, KMess::ProgressConnecting );

  // Set the flag to avoid an MSN server problem. See gotUsr()
  isInitiatingConnection_ = true;

  // start the timer.
  loginTimer_.setInterval( CONNECTION_TIMEOUT );
  loginTimer_.start();

  // Attempt to connect to the default MSN server
  connectToServer();

  debug() << "Socket started connecting to the server.";
}



// Parse a regular command
void MsnNotificationConnection::parseCommand( const QStringList& command )
{
  debug() << "Parsing command '" << command[0] << "'.";

  // Allow some time until the next message is received
  if( loginTimer_.isActive() )
  {
    loginTimer_.start();
  }

  if ( command[0] == "BLP" )
  {
    // Unimplemented. This is a privacy setting. (buddy-list-policy?)
  }
  else if ( command[0] == "BPR" )
  {
    // Unimplemented. This contains buddy properties.
  }
  else if ( command[0] == "CHG" )
  {
    gotChg( command );  // Status change
  }
  else if ( command[0] == "CHL" )
  {
    gotChl( command );  // Server-side ping, aka 'challenge'
  }
  else if ( command[0] == "CVR" )
  {
    gotCvr( command );  // Client version info
  }
  else if ( command[0] == "FLN" )
  {
    gotFln( command );  // Contact offline
  }
  else if ( command[0] == "ILN" )
  {
    gotIln( command );  // Contact initially online
  }
  else if ( command[0] == "NLN" )
  {
    gotNln( command );  // Contact now online
  }
  else if ( command[0] == "OUT" )
  {
    gotOut( command );  // Disconnection from server
  }
  else if ( command[0] == "PRP" )
  {
    gotPrp( command );  // Contact property change
  }
  else if ( command[0] == "QRY" )
  {
    // Ignored. Indicates the challenge was successful, contains no parameters.
  }
  else if ( command[0] == "RNG" )
  {
    gotRng( command );  // Chat or connection requests
  }
  else if( command[0] == "RML" )
  {
    gotRml( command );
  }
  else if( command[0] == "RFS" )
  {
    // Receveid the request for refresh list
    // so the client reply with adl command
    putAdl();
  }
  else if( command[0] == "UBN" )
  {
    // For the moment is useless
    // Require more investigate
  }
  else if ( command[0] == "SBP" )
  {
    // Unimplemented.
  }
  else if ( command[0] == "SBS" )
  {
    // Ignored. The meaning of this command is unknown, but it's related to the
    // user's mobile credits (MSN Mobile).
    // See: http://msnpiki.msnfanatic.com/index.php/MSNP11:Changes#SBS for details
  }
  else if ( command[0] == "URL" )
  {
    gotUrl( command );  // Hotmail folder info
  }
  else if ( command[0] == "USR" )
  {
    gotUsr( command );  // User authentication information
  }
  else if ( command[0] == "UUX" )
  {
    // Ignored. It's an echo back from our UUX command. Unlike other commands,
    // the UI is updated for this command already before the server confirms.
  }
  else if ( command[0] == "VER" )
  {
    gotVer( command );  // Version information
  }
  else if ( command[0] == "XFR" )
  {
    gotXfr( command );  // Server transfer data
  }
  else
  {
    // No need to bugger the user with strange error messages
    warning() << "Unhandled command!";
    warning() << command.join( " " );

    emit statusEvent( KMess::WarningMessage, KMess::WarningUnknownCommand );
  }
}



// Parse an error command
void MsnNotificationConnection::parseError( const QStringList& command, const QByteArray &payloadData )
{
  // See if the error was from an open switchboard request.
  if( ! openRequests_.isEmpty() )
  {
    int transactionId = command[1].toInt();
    if( openRequests_.value( transactionId, 0 ) != 0 )
    {
      debug() << "Removing failed chat session request";
      openRequests_.take( transactionId );
    }
  }

  // Relay the error detection to the base class
  MsnConnection::parseError( command, payloadData );
}



// Parse a payload command
void MsnNotificationConnection::parsePayloadCommand( const QStringList &command, const QByteArray &payload )
{
  // Identify and handle the command
  if( command[0] == "ADL" )
  {
    gotAdl( command, payload );
  }
  else if( command[0] == "MSG" )
  {
    gotMsg( command, payload );
  }
  else if( command[0] == "UBX" )
  {
    gotUbx( command, payload );
  }
  else if ( command[0] == "UBN" )
  {
    gotUbn( command, payload );
  }
  else if( command[0] == "UUX" )
  {
    // Ignore: acknowledgement of a sent UUX command.
  }
  else if ( command[0] == "UBM" )
  {
    gotUbm( command, payload );
  }
  else if( command[0] == "GCF" )
  {
    // Ignore this
    // Contains policy information, requesting clients
    // to disable certain features or possibly block
    // certain filenames/contents.
    debug() << "Ignoring security configuration file:";
    debug() << QString::fromUtf8( payload.data(), payload.size() );
  }
  else if( command[0] == "NOT" )
  {
    // Ignore "notification" messages coming from MSN Calendar, MSN Alerts, and MSN Mobile.
    debug() << "Ignoring text event notification:";
    debug() << QString::fromUtf8( payload.data(), payload.size() );
  }
  else if ( command[0] == "RML" )
  {
    // Ignore: acknowledgement of removing a contact from the list.
  }
  else
  {
    warning() << "Unhandled payload command!";
    warning() << "(C)" << command.join( " " );
    warning() << "(P)" << QString::fromUtf8( payload.data(), payload.size() );
  }
}



/**
 * @brief Send the personal status and current media fields.
 *
 * This method sends the MSN servers an update of the personal status
 * and media fields.
 *
 * MSNP13 uses an extra field on the UUX command.
 *
 * This method retrieves all information it needs to send from the
 * global session object.
 *
 * @param initialUpdate If true, we will also send the endpoint data
 */
void MsnNotificationConnection::putUux( bool initialUpdate )
{
  QString             machineGuid( self_->localEndpoint()->guid() );
  const QString&        currentPm( self_->personalMessage() );

  MediaData media = self_->media();
  
  // WTF: Encode the MachineGUID to XML. The *weird* thing is that
  // while this is required (otherwise we get a 240 error),
  // the media format *does not* need curly braces to be escaped
  // to XML.
//   machineGuid.replace( '{', "&#x7B;" ).replace( '}', "&#x7D;" );

  bool enabled = ( media.type != MediaNone );
  const QString currentMediaFormat( "\\" + InternalUtils::mediaTypeToString( media.type ) + "\\0" +
                                    QString::number( enabled ) +
                                    "\\0" + media.format + "\\0" +
                                    media.formatArgs.join( "\\0" ) );


  QString xml;

  // Before the normal properties, send the endpoint data
  if( initialUpdate )
  {
    xml = "<EndpointData>"
            "<Capabilities>" + Utils::getClientCapabilities().toString() + "</Capabilities>"
          "</EndpointData>";

    sendCommand( "UUX", QString(), xml );

    xml =  "<PrivateEndpointData>"
            "<EpName>%1</EpName>"
            "<Idle>%2</Idle>"
            "<ClientType>1</ClientType>"
            "<State>%3</State>"
          "</PrivateEndpointData>";

    xml = xml.arg( self_->localEndpoint()->name() ).arg( self_->status() == IdleStatus ? "true" : "false" ).arg( InternalUtils::statusToCode( (MsnStatus)( session_->sessionSettingInt( "StatusInitial") ) ) );
    debug() << xml;
    sendCommand( "UUX", QString(), xml );
  }

  // Send the personal message and the other required data

  xml = "<Data>"
          "<PSM>" + Qt::escape( currentPm ) + "</PSM>"
          "<CurrentMedia>" + Qt::escape( currentMediaFormat ) + "</CurrentMedia>"
          "<MachineGuid>" + machineGuid + "</MachineGuid>"
          "<DDP></DDP>"
          "<SignatureSound></SignatureSound>"
          "<Scene></Scene>"
          "<ColorScheme></ColorScheme>"
        "</Data>";

  sendCommand( "UUX", QString(), xml );
}



/**
 * @brief Send the version command.
 *
 * Send versions of protocol supported by the client
 *
 */
void MsnNotificationConnection::putVer()
{
  // Indicate which protocol versions we support.
  // At the moment, we are working with one MSNP version at once, so we only advertise that.
  // The "CVR0" is seemingly not significant, and could be omitted.

  // TODO Could we send 18,17,16 since the major changes we added from p15 were in p16?
  sendCommand( "VER", "MSNP18 CVR0" );
}





// Received the Mail-Data field over SOAP or at the notification connection.
void MsnNotificationConnection::receivedMailData( const QDomElement& mailData )
{
  KMESS_ASSERT( mailData.tagName() == "MD" );

  QDomNodeList childNodes = mailData.childNodes();
  for( int i = 0; i < childNodes.count(); i++ )
  {
    QDomElement childNode = childNodes.item(i).toElement();
    QString     childName( childNode.nodeName() );

    if( childName == "E" )
    {
      // Found an initial email status notification.
      debug() << "Load initial email information.";

      QDomElement inboxUnread  = childNode.namedItem("IU").toElement();
      QDomElement othersUnread = childNode.namedItem("OU").toElement();

      if( inboxUnread.isNull() || othersUnread.isNull() )
      {
        warning() << "Received email notification, but 'IU' and 'OU' elements are missing!";
      }

      KMESS_ASSERT( ! inboxUnread.isNull()  );
      KMESS_ASSERT( ! othersUnread.isNull() );

      // Unread mail in folders other than the Inbox is ignored.
      session_->setSessionSetting( "EmailCount", inboxUnread.text().toInt() );
    }
    else if( childName == "M" )
    {
      // Using this for-loop, multiple 'M' elements are supported.

      // Extract the message ID.
      QString messageId( childNode.namedItem("I").toElement().text() );
      if( messageId.isEmpty() )
      {
        warning() << "Unable to parse Mail-Data payload, offline-im message-id not found.";
        return;
      }

      debug() << "Received offline-IM notification '" << messageId << "'";

      // Create a SOAP client to request the Offline-IM
      pendingOfflineImMessages_.append(messageId);
    }
    else if( childName == "Q" )
    {
      // Not implemented
    }
    else
    {
      // Warn for unsupported tags.
      warning() << "Unsupported tag '" << childName << "' encountered in Mail-Data payload!";
    }
  }

  // Start a new client if there are pending messages, and no OfflineImService is active
  if( ! pendingOfflineImMessages_.isEmpty() )
  {
    // Initialize on demand
    if( offlineImService_ == 0 )
    {
      offlineImService_ = createOfflineImService();
    }

    // Request the first offline-im message if the current class was
    // not processing already. (otherwise receivedOfflineIm() picks the next one).
    if( offlineImService_->isIdle() )
    {
      offlineImService_->getMessage( pendingOfflineImMessages_.first(), false );
      // Processing continues at receivedOfflineIm()
    }
  }
}



// An Offline-IM message was downloaded
void MsnNotificationConnection::receivedOfflineIm( const QString &messageId, const QString &from, const QString &to,
                                                   const QDateTime &date, const QString &body,
                                                   const QString &runId, int sequenceNum )
{
  debug() << "Received an offline IM message"
            << "from" << from
            << "(runid=" << runId
            << "sequenceNum=" << sequenceNum << ")";

  KMESS_ASSERT( to == self_->handle() );
  Q_UNUSED( to ); // Avoid compiler warning when debugging is off

  // this used to be an if() condition with a status message.
  // That's incorrect. If we require this service it's an error if
  // it doesn't exist.
  KMESS_ASSERT( offlineImService_ );

  // Store message until all messages are received.
  OfflineImMessage *offlineIm = new OfflineImMessage;
  offlineIm->messageId   = messageId;
  offlineIm->body        = body;
  offlineIm->date        = date;
  offlineIm->from        = from;
  offlineIm->runId       = runId;
  offlineIm->sequenceNum = sequenceNum;

  // Order by date
  uint insertPos = 0;
  foreach( OfflineImMessage *nextOfflineIm, offlineImMessages_ )
  {
    if( nextOfflineIm->sequenceNum > sequenceNum )
    {
      debug() << "inserted message" << date << ", #" << sequenceNum
              << "before" << nextOfflineIm->date << ", #" << nextOfflineIm->sequenceNum;
      break;
    }

    insertPos++;
  }

  offlineImMessages_.insert( insertPos, offlineIm );


  // Remove from pending list
  if( pendingOfflineImMessages_.removeAll( messageId ) == 0 )
  {
    warning() << "Could not remove message" << messageId << "from the list!";
  }


  // If there are more messages, request the next one.
  if( ! pendingOfflineImMessages_.isEmpty() )
  {
    debug() << "Requesting the next offline-im message.";

    // Not all messages are received yet, request next
    offlineImService_->getMessage( pendingOfflineImMessages_.first(), false );
    return;
  }


  // All messages received, and sorted,
  // display the messages in the chat windows.

  debug() << "All messages received, displaying messages in chats.";

  MsnContact *contact = 0;
  QString name;
  QString picture;
  QStringList messageIds;

  foreach( OfflineImMessage *offlineIm, offlineImMessages_ )
  {
    messageIds.append( offlineIm->messageId );

    // Get contact details
    // Re-use previous results if message is from the same contact
    if( contact == 0 || contact->id() != offlineIm->from )
    {
      contact = contactList_->contact( offlineIm->from );
      KMESS_NULL( contact );
    }

    // Send out the chat message
    OfflineMessage message( 0 /* not attached to a chat */, contact, offlineIm->body );
    message.setDirection( KMess::DirectionIncoming );
    message.setDateTime( offlineIm->date );
    message.setSequenceNumber( offlineIm->sequenceNum );

    emit receivedMessage( message );
  }

  // Delete all local messages
  qDeleteAll( offlineImMessages_ );
  offlineImMessages_.clear();

  // Request to remove the Offline-IM message from the online storage.
  offlineImService_->deleteMessages( messageIds );
  // Keep reference to SOAP service so the connection is re-used for other messages.
}



/**
 * Request a new chat session
 *
 * Use this method to request a new switchboard session for your MsnChats.
 *
 * @param requester  The MsnChat object to notify when the session can be opened
 */
void MsnNotificationConnection::inviteToChat( MsnChat *requester )
{
  // Verify the incoming data
  if( requester == 0 )
  {
    warning() << "No requesting chat set!";
    return;
  }

  // See if there already is an open chat request
  if( openRequests_.values().contains( requester ) )
  {
    debug() << "A chat session request is already pending for chat:" << requester->participants();
    return;
  }

  // Send the command
  int transactionId = sendCommand( "XFR", "SB" );

  // Store the pending request information so the return value can be handled
  openRequests_.insert( transactionId, QPointer<MsnChat>( requester ) );
}



/**
 * Send a URL COMPOSE request for a particular address.
 * 
 * When the response arrives gotComposeUrl() will be emitted.
 * 
 * @param toAddress Recipient address
 */
void MsnNotificationConnection::requestComposeUrl( const QString &toAddress )
{
  int id = sendCommand( "URL", "COMPOSE " + toAddress );
  pendingComposeRequests_.insert( id, toAddress );
}



/**
 * Request
 *
 * When the account supports email (e.g. Hotmail), request the URLs needed
 * to access it.
 */
void MsnNotificationConnection::sendMailUrlRequest()
{
  // This doesn't give problems with non-email accounts,
  // but restricted accounts return a "710" error for this command.
  if( ! session_->sessionSettingBool( "EmailEnabled" ) )
  {
    return;
  }

  // The ACK number is temporarily stored in place of the URL,
  // so the response can be mapped back to the requested URL
  int ack;

  // Ask for inbox URL
  ack = sendCommand( "URL", "INBOX" );
  session_->setSessionSetting( "UrlWebmailInbox", QString::number( ack ) );

  // Ask for compose URL
  ack = sendCommand( "URL", "COMPOSE some.invalid@kmess.email" );
  session_->setSessionSetting( "UrlWebmailCompose", QString::number( ack ) );

  // Ask the personal profile URL
  int langCode = session_->sessionSettingInt( "LanguageCode" );
  ack = sendCommand( "URL", "PROFILE 0x" + QString::number( ( langCode == 0 ? 1033 : langCode ), 8 ) );
  session_->setSessionSetting( "UrlProfile", QString::number( ack ) );
}




/**
 * Send a UUN command of the given type and body.
 * @param handle Handle for the command
 * @param type Type of UUN message
 * @param body Payload of the message.
 */
void MsnNotificationConnection::sendUUN( const QString &handle, int type, const QString &body )
{
  sendCommand( "UUN", handle + ' ' + QString::number( type ), body );
}



/**
 * Send a UUM (Yahoo! message) to a given user.
 * @param msg The Yahoo! message with corresponding inner message.
 */
void MsnNotificationConnection::sendUUM( KMess::Message &msg )
{
  QString params( msg.peer()->handle() + ' ' + QString::number( msg.peer()->network() ) );
  int msgType = -1;
  
  switch( msg.type() )
  {
    case TextMessageType:
      msgType = 1;
      break;
      
    case TypingMessageType:
      msgType = 2;
      break;
      
    case NudgeMessageType:
      msgType = 3;
      break;
      
    default:
      // can only handle the above 3 types.
      KMESS_ASSERT( msgType != -1 );
      break;
  }

  params += ' ' + QString::number( msgType );

  // done!
  sendCommand( "UUM", params, msg.contents() );
}



/**
 * Set the path to the cache directory.
 * @param path The path to the cache directory.
 */
void MsnNotificationConnection::setCacheDir( const QString &path )
{
  cacheDir_ = path;
}



/**
 * @brief The authentication process has timed out
 *
 * Called by the login timer, when the login timer has expired
 */
void MsnNotificationConnection::slotConnectionTimeout()
{
  debug() << "Connection not successful within time limit.";

  disconnectFromServer( MsnSocketBase::NoSignals );

  emit statusEvent( KMess::ErrorMessage, KMess::ErrorConnectionTimedOut );
  emit connectionFailed( KMess::ErrorConnectionTimedOut );
}



// Contact added, notify it to contactlist
void MsnNotificationConnection::slotContactAdded( KMess::MsnContact *contact )
{
  // Ask the server to add the contact to FL and Allow/Block list.
  putAdl( contact, contact->memberships() );
}



/*
When a contact is blocked/unblocked, send ADL/RML as appropriate.
*/
void MsnNotificationConnection::slotContactBlockChanged( KMess::MsnContact *contact )
{
  if ( contact->isBlocked() )
  {
    putRml( contact, MSN_LIST_ALLOWED );
    putAdl( contact, MSN_LIST_BLOCKED );
  }
  else if ( contact->isAllowed() )
  {
    putRml( contact, MSN_LIST_BLOCKED );
    putAdl( contact, MSN_LIST_ALLOWED );
  }
}



// Contact deleted
void MsnNotificationConnection::slotContactDeleted( KMess::MsnContact *contact )
{
  KMESS_ASSERT( contact );
  
  // Remove it from our FL.
  putRml( contact, MSN_LIST_FRIEND );

  // mark the contact as offline - we'll no longer get updates from them.
  contact->setStatus( OfflineStatus );

  // signal successful removal
  emit contactRemoved( contact );
}



/*
Called when the AB service informs us it's completed retrieval and parsing of the address book.

Here we sent the initial ADL.
*/
void MsnNotificationConnection::slotAddressBookParsed( long cid, int blp )
{
  changePersonalProperties( cid, blp );

  emit statusEvent( KMess::ProgressMessage, KMess::ProgressGoingOnline );

  // get all the pending contacts.
  foreach( MsnContact *c, contactList_->contacts() )
  {
    if ( c->isPending() )
    {
      emit contactAddedUser( c );
    }
  }

  putAdl();
}



/**
 * @brief Called when the connection has been established.
 *
 * This method calls putVer() to start the version exchange.
 */
void MsnNotificationConnection::slotConnected()
{
  debug() << "Connected to server, sending VER.";

  // Start the login timer, allowing the defined maximum time between each
  // received command
  loginTimer_.setInterval( AUTHENTICATION_TIMEOUT );
  loginTimer_.start();

  putVer();
}


