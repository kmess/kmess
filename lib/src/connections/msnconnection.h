/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Mike K. Bennett <mkb137b@hotmail.com>                     *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file msnconnection.h
 */

#ifndef MSNCONNECTION_H
#define MSNCONNECTION_H

#include "debug/debuginterface.h"

#include "msnsocketbase.h"

#include <QHash>


// Forward declarations
namespace KMess
{
  class MsnSession;
  class Debugger;
};


namespace KMessInternal
{
  class HttpSoapConnection;

/**
 * @brief MSN server protocol implementation.
 *
 * The class provides the following facilities:
 * - parsing the raw network data into protocol commands.
 * - sending protocol commands and payloads to the actual connection implementations
 * - built-in handling of multi-packet <code>MSG</code> commands.
 *
 * Data from the basic I/O implementation is received by the dataReceived() slot.
 * This method identifies the data and calls one of the following methods to deliver the message the a derived class:
 * - parseCommand()
 * - parseMimeMessage()
 * - parsePayloadMessage()
 * - isPayloadCommand()
 *
 * These methods are implemented in the derived classes,
 * to handle the action for every command.
 *
 * To send protocol commands to the server, use:
 * - sendCommand()
 * - sendMimeMessage()
 * - sendPayloadCommand()
 *
 * @author Mike K. Bennett
 * @author Valerio Pilo <valerio@kmess.org>
 * @ingroup NetworkCore
 */
class MsnConnection : public DebugInterface
{
  Q_OBJECT

  friend class KMess::MsnSession;
  friend class KMess::Debugger;

  public: // Public methods
    virtual             ~MsnConnection();
    bool                 isConnected() const;

  protected: // Protected methods
                         MsnConnection( KMess::MsnSession *session, MsnSocketBase::ServerType serverType );
    void                 addSoapClient( HttpSoapConnection *client );
    /**
     * @brief Close the connection.
     *
     * This method is called by this class only
     * when the connection needs to be closed.
     */
    virtual void         closeConnection() = 0;
    void                 connectToServer( const QString& server = QString(), const quint16 port = 0 );
    void                 deleteSoapClient( HttpSoapConnection *client );
    void                 deleteSoapClients();
    void                 disconnectFromServer( MsnSocketBase::DisconnectPolicy policy = MsnSocketBase::EmitSignals, KMess::DisconnectReason reason = KMess::UserRequestedDisconnect );
    const QString        getLocalIp() const;
    bool                 isErrorCommand( const QString &command ) const;
    virtual bool         isPayloadCommandOnReceive( const QString &command ) const;
    virtual bool         isPayloadCommandOnSend( const QString &command ) const;
    virtual void         setPayloadCommands( const QStringList&, const QStringList& );
    /**
     * @brief Process a received command
     *
     * A command may look like:
     * @code
ADC 27 AL N=someone@kmessdemo.org
@endcode
     * This is a confirmation of a sent command (with ACK-number 27) that the contact someone@kmessdemo.org was added to the allow (<code>AL</code>) list.
     *
     * The command is always a three-letter acronym, stored in <code>command[0]</code>
     * Arguments are separated by spaces, stored in the remaining <code>command[1..n]</code> elements.
     *
     * Most commands sent with sendCommand() are echo'ed back to the client.
     * This is not only a confirmation, but also allow event-based development
     * (or Model-Viewer-Controller; the server is the model in this situation).
     * Instead of assuming a command like "add contact" succeeds,
     * the client simply waits for a returned <code>ADC</code> command.
     * Hence, the same <code>ADC</code> command is also used to report about new contacts.
     * The client only have to execute that the server intructs to do.
     *
     * When an error occurs, the command is a three-diget code, followed by the ACK-number of the sent command.
     * For example, in response to a <code>CAL</code> command, the server may return code <code>217</code>; "person is offline or invisible".
     * @code
>>> CAL 2 myname@kmessdemo.org
@endcode
     * @code
<<< 217 2
@endcode
     *
     * Some commands, like <code>MSG</code> or <code>UBX</code> are followed by a payload.
     * These commands are handled by parseMimeMessage() or parsePayloadMessage().
     *
     * @param  command  The command and arguments.
     */
    virtual void         parseCommand(const QStringList& command) = 0;
    virtual void         parseError( const QStringList &command, const QByteArray &payloadData );
    /**
     * @brief Process a received message.
     *
     * A incoming message may look like:
     * @code
MSG somecontact@kmessdemo.org KMessDemo 146
@endcode
     * @code
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
X-MMS-IM-Format: FN=Arial; EF=; CO=0; CS=0; PF=0

no prob. How is development doing?
@endcode
     *
     * Notice the MIME-style header, and body below.
     * The <code>Content-Type</code> header indicates the message type,
     * for example: <code>text/plain</code> (a chat message)
     * <code>text/x-mms-emoticon</code> (custom emoticon definition)
     * or <code>text/x-msmsgsemailnotification</code> (new email notification).
     *
     * All MIME messages are encapsulated in the <code>MSG</code> command.
     * This command is not delivered to parseCommand().
     * Instead, this class waits for the payload (the MIME message) to arrive.
     * The complete command and payload is forwarded to this method.
     * When the MIME message was split in a multi-packet message,
     * it's assembled back together before this method is called.
     * The last argument of <code>command</code> can be ignored.
     * It indicates the size of the payload, in this case 146 bytes.
     *
     * @param  command  The MSG command and its arguments
     * @param  payload  The payload associated with the command
     */
    virtual void         parsePayloadCommand( const QStringList& command, const QByteArray &payload ) = 0;
    int                  sendCommand( const QString &command, const QString &arguments, const QString &payload );
    int                  sendCommand( const QString &command, const QString &arguments = QString(), const QByteArray &payload = QByteArray() );
    void                 setSendPings( bool sendPings );
    bool                 switchToHttpSocket();
    bool                 switchToTcpSocket();
    int                  nextTrid();

  protected slots: // Protected slots

    /**
     * @brief Called when the connection attempt is successful.
     *
     * This allows the first command to be sent.
     */
    virtual void         slotConnected() = 0;

  private:  // private methods
    void                 attachToSocketSignals();
    void                 writeBinaryData( const QByteArray& data );

  private slots:
    void                 slotDataReceived( const QStringList &commandLine, const QByteArray &payloadData );
    void                 slotDisconnected( KMess::DisconnectReason reason );

  private: // Private attributes
    /// An acknowledgement number
    int                  ack_;
    /// The persistent soap clients.
    QList<HttpSoapConnection*> soapClients_;
    /// The socket
    MsnSocketBase       *socket_;
    /// Whether or not the fallback HTTP socket is in use
    bool                 useHttpSocket_;

  protected: // Protected attributes
    /// The KMess::MsnSession object
    KMess::MsnSession   *session_;


  signals:
    /// Signal that the server has disconnected
    void                 disconnected( KMess::DisconnectReason reason = KMess::UnexpectedDisconnect );
    /// Signal that a new SOAP client was created
    void                 soapClientCreated(
                              KMessInternal::HttpSoapConnection* );
    /// Signal that a ping to the connection has been sent
    void                 pingSent();
    /// Ask the user to authenticate on a proxy
    void                 proxyAuthenticationRequired(
                              const QNetworkProxy &proxy,
                              QAuthenticator *authenticator );
    /// Signal some change in status
    void                 statusEvent(
                              const KMess::StatusMessageType type,
                              const KMess::StatusMessage message,
                              const QVariant &extraInfo = QVariant() );

};


} // namespace KMess


#endif
