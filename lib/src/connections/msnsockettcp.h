/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Mike K. Bennett <mkb137b@hotmail.com>                     *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file msnsockettcp.h
 */

#ifndef MSNSOCKETTCP_H
#define MSNSOCKETTCP_H

#include "msnsocketbase.h"

#include <QAbstractSocket>
#include <QStringList>
#include <QTimer>

// Forward declarations
class QTcpSocket;



namespace KMessInternal
{

/**
 * @brief Basic I/O functionality for the MSN server protocol over a TCP connection.
 *
 * The class provides the following facilities:
 * - asynchronous connection to the MSN servers.
 * - sending/receiving raw protocol data.
 * - transparent reconstruction of fragmented received payloads.
 *
 * Data from the server is handled by the slotSocketDataReceived() slot.
 * This method buffers and processes the data, until a complete command or payload message is received.
 * Then a signal with the full message contents is forwarded to MsnConnection, which manages it.
 * This class only handles the parsing of socket input, the handling the commands
 * happens in the MsnConnection class.
 *
 * @author Mike K. Bennett
 * @author Valerio Pilo <valerio@kmess.org>
 * @ingroup NetworkCore
 */
class MsnSocketTcp : public MsnSocketBase
{
  Q_OBJECT


  public: // Public methods
    // The constructor
                         MsnSocketTcp( KMess::MsnSession *session, ServerType serverType );
    // The destructor
    virtual             ~MsnSocketTcp();
    // Connect to the given server via the socket
    void                 connectToServer( const QString& server = QString(), const quint16 port = 0 );
    // Disconnect from the server, if connected
    void                 disconnectFromServer( DisconnectPolicy policy = EmitSignals, KMess::DisconnectReason reason = KMess::UserRequestedDisconnect  );
    // Get the IP address of this machine.
    QString              getLocalIp() const;
    // Whether or not the class is connected
    bool                 isConnected() const;
    // Set whether we're sending pings or not (also resets ping timer)
    void                 setSendPings( bool sendPings );
    // Write data to the socket without conversions
    void                 writeBinaryData( const QByteArray& data );


  private slots: // Private slots
    // Called when the connection has been closed
    void                 slotDisconnected();
     // Send a "ping" to the server
    void                 slotSendPing();
    // Read data from the socket
    void                 slotSocketDataReceived();
    // Detect socket errors
    void                 slotSocketError( QAbstractSocket::SocketError errorCode );


  private: // Private attributes
    // Number of errant pings since last good one
    int                  missedPings_;
    // The next payload command
    QStringList          nextPayloadCommand_;
    // The size of the next expected payload message
    int                  nextPayloadSize_;
    // Whether the last ping send got a reply
    bool                 pingReceived_;
    // A timer to regularly "ping" the server
    QTimer               pingTimer_;
    // Are we sending pings?
    bool                 sendPings_;
    // The socket
    QTcpSocket          *socket_;
    // List of accepted commands which carry a data payload
    QStringList          acceptedPayloadCommands_;

};


}; // End of namespace KMess

#endif
