/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Mike K. Bennett <mkb137b@hotmail.com>                     *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file msnsockettcp.cpp
 */

#include "msnsockettcp.h"
#include "debug/libkmessdebug.h"

#include <KMess/MsnSession>
#include <KMess/NetworkGlobals>

#include <QTcpSocket>
#include <QHostAddress>


using namespace KMessInternal;

#ifdef KMESSDEBUG_CONNECTION // Area-specific debug statements
  #define KMESSDEBUG_CONNECTION_SOCKET_TCP
#endif


/**
 * @brief Login timer for DNS name resolution
 *
 * In milliseconds, the time to wait for the msn server name resolution to complete.
 */
#define LOGIN_TIMER_DNS_RESOLVE   10000

/**
 * @brief Login timer for actual server connection
 *
 * In milliseconds, the time to wait for the connection to be established.
 */
#define LOGIN_TIMER_CONNECTION     5000


#define MSN_TCP_HOST "messenger.hotmail.com"
#define MSN_TCP_PORT 1863



/**
 * @brief The constructor
 *
 * Initializes the sockets, buffers and ping timer.
 */
MsnSocketTcp::MsnSocketTcp( KMess::MsnSession *session, ServerType serverType )
: MsnSocketBase( session, serverType )
, missedPings_( 0 )
, nextPayloadSize_( 0 )
, sendPings_( false )
{
  setObjectName( "MsnSocketTcp" );

  // Create the socket
  socket_ = new QTcpSocket( this );

  // TODO: socket timeout settings..

  // Forward the socket's raw event signals for further interpretation
  connect( socket_, SIGNAL(              connected() ),
           this,    SIGNAL(              connected() ) );
  connect( socket_, SIGNAL(           disconnected() ),
           this,    SLOT  (       slotDisconnected() ) );

  // Connect the socket's signals to parse them internally
  connect( socket_, SIGNAL(              readyRead() ),
           this,    SLOT  ( slotSocketDataReceived() ) );
  connect( socket_, SIGNAL(           error(QAbstractSocket::SocketError) ),
           this,    SLOT  ( slotSocketError(QAbstractSocket::SocketError) ) );
  connect( socket_, SIGNAL( proxyAuthenticationRequired(QNetworkProxy,QAuthenticator*) ),
           this,    SIGNAL( proxyAuthenticationRequired(QNetworkProxy,QAuthenticator*) ) );

  // Connect the ping timer to the sendPing slot
  pingTimer_.stop();
  connect( &pingTimer_,       SIGNAL(         timeout() ),
           this,              SLOT  (    slotSendPing() ) );

  KMESS_ASSERT( socket_ != 0 );
}



/**
 * @brief The destructor
 *
 * Closes the connection, and deletes the sockets.
 */
MsnSocketTcp::~MsnSocketTcp()
{
  // Disconnect from the server on exit
  if( isConnected() )
  {
    disconnectFromServer();
  }

  // Delete the socket
  delete socket_;

  debug() << "DESTROYED";
}



/**
 * @brief Connect to the given server via the socket
 *
 * The connection attempt is asynchronous.
 * When the connection is made, connectionSuccess() is called,
 * on error connectionFailed() is called.
 * Connection timeouts are not detected here yet.
 *
 * @param  server  The server hostname or IP address.
 * @param  port    The port to connect to.
 */
void MsnSocketTcp::connectToServer( const QString& server, const quint16 port )
{
  quint16 desiredPort;
  QString desiredServer;

  if( server.isEmpty() && port == 0 )
  {
    desiredServer = MSN_TCP_HOST;
    desiredPort   = MSN_TCP_PORT;
  }
  else
  {
    desiredServer = server;
    desiredPort   = port;
  }

  const QString &override = session_->sessionSettingString( "NotificationServerOverride" );
  if( !override.isEmpty() )
  {
    desiredServer = override;
  }

  KMESS_ASSERT( desiredPort < 32768 );

  debug() << "Connecting to server at " << desiredServer << ":" << desiredPort << ".";
  debug() << "Socket state is " << socket_->state() << ".";

  // Flush any pending data if the socket is closing a previous connection
  socket_->blockSignals( true );
  socket_->flush();

  // Check if we are disconnected (the socket is idle)
  if( socket_->state() != QAbstractSocket::UnconnectedState )
  {
    warning() << "Socket is not disconnected. Already connected? The socket is in state" << socket_->state() << ".";
  }

  // Prepare state vars for the new connection
  connected_       = true;
  nextPayloadSize_ = 0;

  // Close any previous connection (without firing disconnection signals)
  socket_->close();

  socket_->blockSignals( false );

  // Start connecting
  socket_->connectToHost( desiredServer, desiredPort );
}



/**
 * @brief Disconnect from the server
 *
 * This function is called after closeConnection()
 * It empties all buffers, and resets the sockets.
 *
 * @param  policy      If set to MsnSocketBase::NoSignals, the disconnected() signal
 *                     will not be emitted. This is useful for server transfers.
 * @param  reason      Optional. Provide a reason why the disconnect occurred. Defaults to
 *                     UserRequestedDisconnect.
 */
void MsnSocketTcp::disconnectFromServer( DisconnectPolicy policy, KMess::DisconnectReason reason )
{
  // Stop pinging
  setSendPings( false );

  debug() << "Close the socket.";

  // Disconnect the socket
  socket_->flush();
  socket_->blockSignals( true );
  socket_->disconnectFromHost();

  // Just clean up if we are disconnected already
  if( ! connected_ )
  {
    return;
  }

  connected_ = false;

  // Do not signal a disconnection when we are transferring to another gateway
  if( policy == NoSignals )
  {
    return;
  }

  debug() << "Emitting disconnection signal.";

  emit disconnected( reason );
}



/**
 * @brief Return the local IP address of the socket.
 *
 * This is typically a LAN address, unless the system
 * is directly connected to the Internet.
 *
 * @returns The IP address the socket uses at the system.
 */
QString MsnSocketTcp::getLocalIp() const
{
  const QHostAddress& address( socket_->localAddress() );

  return address.toString();
}



/**
 * @brief Return whether or the socket is connected.
 *
 * @returns True when connected, false otherwise.
 */
bool MsnSocketTcp::isConnected() const
{
  return socket_->isValid() && socket_->state() == QAbstractSocket::ConnectedState;
}



// Set whether we're sending pings or not (also resets ping timer)
void MsnSocketTcp::setSendPings( bool sendPings )
{
  sendPings_ = sendPings;
  missedPings_ = 0;

  pingReceived_ = true;

  if ( sendPings_ )
  {
    slotSendPing();

    pingTimer_.stop();
    pingTimer_.start( 30000 );  // 30s ping timer
  }
  else
  {
    pingTimer_.stop();
  }
}



/**
 * @brief Called when the connection has been closed
 *
 * This method is not called when using disconnectFromServer(), because
 * its signals were blocked.
 */
void MsnSocketTcp::slotDisconnected()
{
  // Notify about what's happening
  if( connected_ )
  {
    emit disconnected();
  }
}



// Send a "ping" to the server
void MsnSocketTcp::slotSendPing()
{
  if ( pingReceived_ == false )
  {
    missedPings_ ++;

    emit statusEvent( KMess::WarningMessage, KMess::WarningPingLost, QVariant( missedPings_ ) );
  }
  else
  {
    emit statusEvent( KMess::SessionMessage, KMess::SessionPingReceived );
  }

  pingReceived_ = false;

  // Too many missed pings may have caused the GUI to force a disconnection
  if( ! connected_ )
  {
    return;
  }

  writeBinaryData( QByteArray( "PNG\r\n" ) );

  emit pingSent();
}



/**
 * @brief Called when data is received from the server.
 *
 * This method parses the incoming raw data for the syntax used by the MSN protocol.
 * When a command is only partially received, it's buffered until the whole command is received by subsequent dataReceived() calls.
 *
 * Messages are then signalled back to MsnConnection which perform the parsing.
 * Only <code>PNG</code> commands are handled internally.
 */
void MsnSocketTcp::slotSocketDataReceived()
{
  bool        gotPayloadCommand  = false;
  bool        hasCompletePayload = false;

  QByteArray  payloadData;
  QStringList command;

  while( true )
  {
    // Read the next command if we're not waiting for a payload.
    if( nextPayloadSize_ == 0 )
    {
      // Stop when socket didn't receive a full line yet (Qt does the buffering, thanks Qt4!)
      if( ! socket_->canReadLine() )
      {
        break;
      }

      // Read the next line.
      const QByteArray& rawCommandLine( socket_->readLine() );
      const QString& commandLine( QString::fromUtf8( rawCommandLine.data(), rawCommandLine.size() ) );

      // Parse the command, split in separate fields
      command = commandLine.trimmed().split(' ');

      // See if it's a payload command or an error
      if( isErrorCommand( command[0] ) )
      {
        // If the command contains a payload length parameter, also parse it
        gotPayloadCommand = ( command.size() > 2 );
      }
      else
      {
        gotPayloadCommand = isPayloadCommandOnReceive( command[0] );
      }

      if( gotPayloadCommand )
      {
        nextPayloadSize_    = command[ command.count() - 1 ].toInt();  // Last arg is payload length.
        nextPayloadCommand_ = command;
      }
    }

    // See if we can read the payload
    if( nextPayloadSize_ > 0 )
    {
      // Check if the full payload is received.
      // Otherwise, just wait until "dataReceived" is called again.
      hasCompletePayload = ( socket_->bytesAvailable() >= nextPayloadSize_ );
      if( ! hasCompletePayload )
      {
        break;
      }

      // Full payload received.
      payloadData = socket_->read( nextPayloadSize_ );

      // Copy state from previous loop if needed.
      if( ! gotPayloadCommand )
      {
        gotPayloadCommand = true;
        command           = nextPayloadCommand_;
      }

      // Reset state for next loop.
      nextPayloadSize_ = 0;
    }
    else
    {
      payloadData.clear();
    }


    // Response from the internal ping timer.
    if( command[0] == "QNG" )
    {
      pingReceived_ = true;
      missedPings_  = 0;
    }
    else // All other data
    {
      emit dataReceived( command, payloadData );
    }
  }
}



/**
 * @brief Detect socket errors
 *
 * Closes the connection, and emits a statusEvent() signal with message type
 * KMess::TYPE_ERROR and message one of MESSAGE_ERROR_CONNECTING,
 * MESSAGE_ERROR_DROP, MESSAGE_ERROR_DATA or MESSAGE_ERROR_UNKNOWN.
 *
 * @param  errorCode  The system error.
 */
void MsnSocketTcp::slotSocketError( QAbstractSocket::SocketError errorCode )
{
  warning() << "Received error" << errorCode << "from the socket"
                "(socket state=" << socket_->state() << ").";

  // Ignore errors when disconnected: avoids duplicate error dialogs
  if( ! connected_ )
  {
    return;
  }

  KMess::StatusMessage message;

  // Find the most appropriate kind of error to report
  switch( errorCode )
  {
    case QAbstractSocket::ConnectionRefusedError:
    case QAbstractSocket::HostNotFoundError:
      message = KMess::ErrorConnectingToSocket;
      break;

    case QAbstractSocket::RemoteHostClosedError:
    case QAbstractSocket::NetworkError:
      message = KMess::ErrorConnectionDropped;
      break;

    case QAbstractSocket::DatagramTooLargeError:
    case QAbstractSocket::UnknownSocketError:
      message = KMess::ErrorSocketDataInvalid;
      break;

    default:
      message = KMess::ErrorUnknown;
      break;
  }

  emit statusEvent( KMess::ErrorMessage, message );
}



// Write data to the socket without conversions
void MsnSocketTcp::writeBinaryData( const QByteArray &data )
{
  if( ! isConnected() )
  {
    warning() << "Attempted to write data to a disconnected socket, aborting.";
    return;
  }

  // Write to the socket
  qint64 bytesWritten = socket_->write( data.data(), data.size() );

  if ( bytesWritten != data.size() )
  {
    warning() << "Wanted to write " << data.size() << " bytes to the socket, wrote " << bytesWritten << ".";
  }
}


