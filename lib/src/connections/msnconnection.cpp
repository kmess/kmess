/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Mike K. Bennett <mkb137b@hotmail.com>                     *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file msnconnection.cpp
 */

#include "msnconnection.h"
#include "debug/libkmessdebug.h"

#include "../soap/httpsoapconnection.h"
#include "msnsockettcp.h"
#include "msnsockethttp.h"


using namespace KMessInternal;



/**
 * @brief The constructor
 *
 * Initializes the sockets, buffers and ping timer.
 *
 * @param  session     The MsnSession this object belongs to, to request information.
 * @param  serverType  The type of server connection: TCP or HTTP.
 */
MsnConnection::MsnConnection( KMess::MsnSession *session, MsnSocketBase::ServerType serverType )
: ack_(1)
, useHttpSocket_(false)
, session_( session )
{
  // Create the socket needed for this connection: default to the TCP one
  if( useHttpSocket_ )
  {
    socket_ = new MsnSocketHttp( session, serverType );
  }
  else
  {
    socket_ = new MsnSocketTcp( session, serverType );
  }

  KMESS_ASSERT( socket_ != 0 );

  attachToSocketSignals();
}



/**
 * @brief The destructor
 *
 * Closes the connection, and deletes the sockets.
 */
MsnConnection::~MsnConnection()
{
  // Disconnect from the server on exit
  if( isConnected() )
  {
    disconnectFromServer();
  }

  // Off course, delete the SOAP clients now here too.
  deleteSoapClients();

  // Delete the socket
  delete socket_;

  debug() << objectName() << "- DESTROYED";
}



/**
 * @brief Internal function to track SOAP clients.
 *
 * This method connects the HttpSoapConnection::soapError() signal,
 * and appends the client to the internal list.
 *
 * @param  client  The SOAP client class.
 */
void MsnConnection::addSoapClient( HttpSoapConnection *client )
{
  debug() << "Adding SOAP client to list.";

  // Connect the client's signals
  connect( client, SIGNAL( statusEvent(KMess::StatusMessageType,KMess::StatusMessage,QVariant) ),
           this,   SIGNAL( statusEvent(KMess::StatusMessageType,KMess::StatusMessage,QVariant) ) );

  // Signal the addition
  emit soapClientCreated( client );

  // Add to list so it cleans up when the connection closes
  soapClients_.append( client );
}



/**
 * @brief Connect a socket's signals to our slots
 *
 * Connects the signals of a new socket to this class' slots.
 * This is needed to have one place only for socket connection;
 * since socket connections are made in initialize() and in
 * switchToHttpSocket().
 */
void MsnConnection::attachToSocketSignals()
{
  KMESS_ASSERT( socket_ != 0 );

  // Connect our integrative methods to the raw socket's events signals
  connect( socket_, SIGNAL(                   connected()                                         ),
           this,    SLOT  (               slotConnected()                                         ) );
  connect( socket_, SIGNAL(                disconnected(KMess::DisconnectReason)                  ),
           this,    SLOT  (            slotDisconnected(KMess::DisconnectReason)                  ) );
  connect( socket_, SIGNAL(                dataReceived(QStringList,QByteArray)                   ),
           this,    SLOT  (            slotDataReceived(QStringList,QByteArray)                   ) );
  connect( socket_, SIGNAL(                 statusEvent(KMess::StatusMessageType,KMess::StatusMessage,QVariant) ),
           this,    SIGNAL(                 statusEvent(KMess::StatusMessageType,KMess::StatusMessage,QVariant) ) );
  connect( socket_, SIGNAL(                    pingSent()                                         ),
           this,    SIGNAL(                    pingSent()                                         ) );
  connect( socket_, SIGNAL( proxyAuthenticationRequired(QNetworkProxy,QAuthenticator*)            ),
           this,    SIGNAL( proxyAuthenticationRequired(QNetworkProxy,QAuthenticator*)            ) );
}



/**
 * @brief Connect to the given server via the socket
 *
 * The connection attempt is asynchronous.
 * When the connection is made, the method 'slotConnected' is called, which
 * is a pure virtual method re-implemented by derived classes.
 * On error, disconnected() is emitted.
 * Connection timeouts are not detected here.
 *
 * @param  server  The server hostname or IP address.
 * @param  port    The port to connect to.
 */
void MsnConnection::connectToServer( const QString& server, const quint16 port )
{
  if( server.isEmpty() && port == 0 )
  {
    debug() << objectName() << "- Connecting to the default server.";
  }
  else
  {
    debug() << objectName() << "- Connecting to server at" << server << ":" << port << ".";
  }

  // Start connecting now
  socket_->connectToServer( server, port );

  // Log the data to the debugger, if one is present
  debugDataSent( QString( "CONNECT: " + server + ':' + QString::number( port ) ).toUtf8() );
}



/**
 * @brief Disconnect and delete a SOAP clients.
 *
 * @param  client  The client to delete.
 * @see deleteSoapClients()
 */
void MsnConnection::deleteSoapClient( HttpSoapConnection *client )
{
  soapClients_.removeAll( client );
  client->abort();
}



/**
 * @brief Disconnect and delete all SOAP clients.
 *
 * This method is called by the derived class when all SOAP clients need to be deleted.
 * This is not done automatically in disconnectFromServer() so the derived class
 * is both responsable for creating and deleting the SOAP client objects.
 */
void MsnConnection::deleteSoapClients()
{
  // Disconnect other possibly active SOAP sessions.
  // Use deleteLater() since this method may be called from loginIncorrect()
  debug() << "Closing SOAP sessions.";

  foreach( HttpSoapConnection *soapClient, soapClients_ )
  {
    deleteSoapClient( soapClient );
    soapClient->deleteLater();
  }

  soapClients_.clear();
}



/**
 * @brief Disconnect from the server
 *
 * This function is called after closeConnection()
 * It empties all buffers, and resets the sockets.
 *
 * @param  policy      If set to MsnSocketBase::NoSignals, the disconnected() signal
 *                     will not be emitted. This is useful for server transfers.
 * @param  reason      Optional. Provide a reason why the disconnect occurred. Defaults to
 *                     UserRequestedDisconnect.
 */
void MsnConnection::disconnectFromServer( MsnSocketBase::DisconnectPolicy policy, KMess::DisconnectReason reason )
{
  debug() << objectName() << "- Closing the connection.";

  // Gracefully disconnect
  if( isConnected() )
  {
    sendCommand( "OUT" );
  }

  // Stop pinging
  setSendPings( false );

  socket_->disconnectFromServer( policy, reason );

  // Log the data to the debugger, if one is present
  debugDataSent( "DISCONNECT" );
}



/**
 * @brief Return the local IP address of the socket.
 *
 * This is typically a LAN address, unless the system
 * is directly connected to the Internet.
 *
 * @returns The IP address the socket uses at the system.
 */
const QString MsnConnection::getLocalIp() const
{
  debug() << objectName() << "- Local IP is " << socket_->getLocalIp();

  return socket_->getLocalIp();
}



/**
 * @brief Return whether or the socket is connected.
 * @returns True when connected, false otherwise.
 */
bool MsnConnection::isConnected() const
{
  return socket_->isConnected();
}



/**
 * @brief Return whether the given command is an error command.
 *
 * This method just passes the question to the socket class
 *
 * @returns True when it's an error command, false otherwise.
 */
bool MsnConnection::isErrorCommand( const QString &command ) const
{
  return socket_->isErrorCommand( command );
}



/**
 * @brief Test whether the given command carries a payload when received.
 *
 * @param command Command to verify
 * @returns True when it's a payload command on receive, false otherwise.
 */
bool MsnConnection::isPayloadCommandOnReceive( const QString &command ) const
{
  return socket_->isPayloadCommandOnReceive( command );
}



/**
 * @brief Test whether the given command must carry a payload when sent.
 *
 * @param command Command to verify
 * @returns True when it's a payload command on send, false otherwise.
 */
bool MsnConnection::isPayloadCommandOnSend( const QString &command ) const
{
  return socket_->isPayloadCommandOnSend( command );
}



/**
 * @brief Process a received error command
 *
 * An incoming error command may look like:
 * @code
205 12 65
@endcode
 * @code
<ml><d n="microsoft.com"><c n="messenger" t="1" l="4" /></d></ml>
@endcode
 *
 * or like:
 * @code
207 12
@endcode
 *
 * The first kind has a payload, the second does not. The additional command
 * parameter reveals that there's a payload.
 * We need to be able to parse both kinds, and report the additional data to
 * the user if any is present.
 *
 * The meaning of error codes can be looked up on MSNPiki:
 * http://msnpiki.msnfanatic.com/index.php/Reference:Error_List
 * but please note that at the moment of writing, it's outdated: it is not
 * mentioned there that even errors may contain payload data.
 *
 * @param  command      The received error and it's arguments.
 * @param  payloadData  The message payload which followed the command, if any.
 */
void MsnConnection::parseError( const QStringList &command, const QByteArray &payloadData )
{
  if( command[0].toInt() == 0 )
  {
    warning() << "Invalid error command received:" << command;
    return;
  }

  // Always print error codes, to ease a bit the error resolution
  warning() << "Received error code" << command[0] << "from server.";
  warning() << "Full command:" << command.join( " " );
  if( ! payloadData.isEmpty() )
  {
    warning() << "The error also carries a payload:";
    warning() << payloadData;
  }

  KMess::StatusMessage     message       = (KMess::StatusMessage)( command[0].toInt() ); // default
  KMess::StatusMessageType type          = KMess::ProtocolMessage; // can be changed

  // here, convert a few of these into just warnings.
  switch( command[0].toInt() )
  {
    // warnings
    case KMess::ErrorInvalidUsername:
    case KMess::ErrorUserAlreadyInChat:
    case KMess::ErrorUserNotOnline:
    case KMess::ErrorCallingTooRapidly:
    case KMess::ErrorTooManyChatSessions:
    case KMess::ErrorChangingTooRapidly:
    case KMess::ErrorPassportNotVerified:
    case KMess::WarningServerGoingDownSoon:
    case KMess::ErrorSessionIsOverloaded:
      type = KMess::WarningMessage;
      break;

    default:
      message = KMess::WarningUnknownCommand;
      break;
  }

  // Add some more details to the signal
  QMap<QString,QVariant> info;
  info[ "ErrorNumber" ] = command[0];
  info[ "Payload"     ] = payloadData;

  emit statusEvent( type, message, info );
}



/**
 * @brief Send a payload command to the server.
 *
 * Convenience function with a string instead of a byte array.
 *
 * @param command The command to send
 * @param arguments The arguments for the command, may be an empty string
 * @param payload The payload for the command
 */
int MsnConnection::sendCommand( const QString &command, const QString &arguments, const QString &payload )
{
  // Convert the payload string into an UTF-8 encoded raw string (byte array)
  return sendCommand( command, arguments, payload.toUtf8() );
}



/**
 * @brief Send a payload command to the server.
 *
 * @param command The command to send
 * @param arguments The arguments for the command, may be an empty string
 * @param payload The payload for the command, may be empty
 * @return Integer, the ack number obtained by the sent command
 */
int MsnConnection::sendCommand( const QString &command, const QString &arguments, const QByteArray &payload )
{
  int ack = nextTrid();

  // Build the command
  QString commandLine( command + ' ' + QString::number( ack ) );

  // Add the arguments, if any
  if( ! arguments.isEmpty() )
  {
    commandLine += ' ' + arguments;
  }

  bool isPayloadCmd = isPayloadCommandOnSend( command );
  if( isPayloadCmd )
  {
    commandLine += ' ' + QString::number( payload.size() );
  }

  commandLine += "\r\n";

  // Write the data
  const QByteArray& commandBytes( commandLine.toUtf8() );

  debug() << objectName() << "- >>> (C) " << commandBytes.trimmed();
  if( isPayloadCmd )
  {
    debug() << objectName() << "- >>> (P) " << payload.trimmed();
  }

  if( isPayloadCmd )
  {
    writeBinaryData( commandBytes + payload );
  }
  else
  {
    writeBinaryData( commandBytes );
  }

  // Return the used ack number
  return ack;
}



/**
 * @brief Specify which accepted commands carry a payload.
 *
 * Within different kinds of connection, server messages can be different or
 * have different meanings; this method ensures that every kind of connection
 * is able to recognize its own payload commands only.
 *
 * @param receivable String list with all payload commands accepted for reception
 * @param sendable String list with all payload commands accepted for sending
 */
void MsnConnection::setPayloadCommands( const QStringList& receivable, const QStringList& sendable )
{
  socket_->setPayloadCommands( receivable, sendable );
}



/**
 * @brief Set whether we're sending pings or not.
 *
 * @param sendPings Whether to send ping commands periodically or not
 */
void MsnConnection::setSendPings( bool sendPings )
{
  socket_->setSendPings( sendPings );
}



/**
 * @brief Called when data is received from the server.
 *
 * This method parses the incoming data for the syntax used by the MSN protocol.
 *
 * A parsed message to handled to one of the following methods:
 * - one-line commands are delivered to parseCommand()
 * - <code>MSG</code> commands with MIME payloads are delivered to parseMimeMessage()
 * - other commands with payloads are delivered to parsePayloadMessage().
 *   This uses isPayloadCommand() to detect if a command has a payload.
 */
void MsnConnection::slotDataReceived( const QStringList &commandLine, const QByteArray &payloadData )
{
  // Display the received command data
  debug() << objectName() << "- <<< (C) " << commandLine;
  if( ! payloadData.isEmpty() )
  {
    debug() << objectName() << "- <<< (P) " << payloadData;
  }

  // Log the data to the debugger, if one is present
  debugDataReceived( commandLine.join(" ").toUtf8() + "\r\n" + payloadData );

  // See if it's a normal or payload command, or an error
  if( isErrorCommand( commandLine[0] ) )
  {
    parseError( commandLine, payloadData );
  }
  else if( isPayloadCommandOnReceive( commandLine[0] ) )
  {
    // Have the subclasses parse this payload command
    parsePayloadCommand( commandLine, payloadData );
  }
  else
  {
    // If the payload isn't empty, but the command is not recognized as a payload
    // one, there's surely a problem
     if( ! payloadData.isEmpty() )
     {
       warning() << "Unknown type of payload command received!";
       warning() << "Message dump:" << commandLine;
       warning() << payloadData;
     }

    // It's a normal command, handle it in the subclass
     parseCommand( commandLine );
  }
}



/**
 * @brief Called when the socket is disconnected.
 */
void MsnConnection::slotDisconnected( KMess::DisconnectReason reason )
{
  emit disconnected( reason );

  // Log the data to the debugger, if one is present
  debugDataSent( "DISCONNECTED" );
}



/**
 * @brief If it is possible, switch to the HTTP connection
 *
 * This method checks what connection is being established, and if is a TCP
 * connection, will delete it and re-start connecting using the HTTP method.
 *
 * This is used when the TCP connection process fails.
 */
bool MsnConnection::switchToHttpSocket()
{
  // Check if we're already using HTTP
  if( qobject_cast<MsnSocketHttp*>( socket_ ) )
  {
    debug() << "Cannot switch to HTTP connection. Class is" << socket_->metaObject()->className();
    return false;
  }

  debug() << "Attempting switch to HTTP connection.";

  // Save the socket details toMsnConnection use the same ones for the new socket
  MsnSocketBase::ServerType serverType = socket_->getServerType();

  QStringList receivablePayloadCommands, sendablePayloadCommands;
  socket_->getPayloadCommands( receivablePayloadCommands, sendablePayloadCommands );

  // Close and disable the old server
  socket_->blockSignals( true );
  socket_->disconnectFromServer();
  socket_->deleteLater();

  // Replace it with the new one
  socket_ = new MsnSocketHttp( session_, serverType );
  socket_->setPayloadCommands( receivablePayloadCommands, sendablePayloadCommands );

  // Set the internal socket switch
  useHttpSocket_ = true;

  // Attach its signals to this class
  attachToSocketSignals();

  // Start connecting
  connectToServer();

  return true;
}



/**
 * @brief Switch back to the TCP connection
 *
 * This method checks what connection is being established, and if is an HTTP
 * connection, will delete it and start using again the TCP method.
 *
 * This is used after disconnecting from an HTTP session, to try again with TCP the next time.
 */
bool MsnConnection::switchToTcpSocket()
{
  // Check if we're already using TCP
  if( qobject_cast<MsnSocketTcp*>( socket_ ) )
  {
    debug() << "Cannot switch to TCP connection. Class is" << socket_->metaObject()->className();
    return false;
  }

  debug() << "Attempting to switch back to TCP connection.";

  // Save the socket details to use the same ones for the new socket
  MsnSocketBase::ServerType serverType = socket_->getServerType();

  QStringList receivablePayloadCommands, sendablePayloadCommands;
  socket_->getPayloadCommands( receivablePayloadCommands, sendablePayloadCommands );

  // Close and disable the old server
  socket_->blockSignals( true );
  disconnect( socket_, 0 );
  socket_->disconnectFromServer();
  socket_->deleteLater();

  // Replace it with the new one
  socket_ = new MsnSocketTcp( session_, serverType );
  socket_->setPayloadCommands( receivablePayloadCommands, sendablePayloadCommands );

  // Set the internal socket switch
  useHttpSocket_ = false;

  // Attach its signals to this class
  attachToSocketSignals();

  return true;
}


/**
 * @brief Gets the next transaction id to be used.
 */
int MsnConnection::nextTrid()
{
  return ack_++;
}



/**
 * @brief Write data to the socket without conversions.
 *
 * @param data The data to write
 */
void MsnConnection::writeBinaryData( const QByteArray& data )
{
  // Write to the socket
  // The socket method will check if we are connected or not
  socket_->writeBinaryData( data );

  // Log the data to the debugger, if one is present
  debugDataSent( data );
}


