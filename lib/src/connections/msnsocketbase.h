/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Valerio Pilo <valerio@kmess.org>                          *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file msnsocketbase.h
 */

#ifndef MSNSOCKETBASE_H
#define MSNSOCKETBASE_H

#include <KMess/NetworkGlobals>

#include <QStringList>
#include <QVariant>

// Forward declarations
class QAuthenticator;
class QNetworkProxy;

namespace KMess
{
  class MsnSession;
};


namespace KMessInternal
{

/**
 * @brief Base class and interface for the MSN Socket classes.
 *
 * The class defines the interface expected by MsnConnection for all socket implementations.
 * Currently there is a socket implementation for normal TCP-based connections (MsnSocketTcp)
 * and a socket for MSN-over-HTTP (MsnSocketHttp).
 *
 * @author Mike K. Bennett
 * @author Valerio Pilo <valerio@kmess.org>
 * @ingroup NetworkCore
 */
class MsnSocketBase : public QObject
{
  Q_OBJECT

  public: // Public enumerations
    enum DisconnectPolicy
    {
      EmitSignals,
      NoSignals
    };

    /// Type of server that will be connected from this socket
    enum ServerType
    {
      SERVER_NOTIFICATION     /// The connection is to a notification server
    , SERVER_SWITCHBOARD      /// The connection is to a switchboard server
    };


  public: // Public methods
    // The constructor
                       MsnSocketBase( KMess::MsnSession *session, ServerType serverType );
    // The destructor
                      ~MsnSocketBase();
    // Connect to the given server via the socket
    virtual void       connectToServer( const QString& server = QString(), const quint16 port = 0 ) = 0;
    // Disconnect from the server, if connected
    virtual void       disconnectFromServer( DisconnectPolicy policy = EmitSignals, KMess::DisconnectReason reason = KMess::UserRequestedDisconnect ) = 0;
    // Get the IP address of this machine.
    virtual QString    getLocalIp() const = 0;
    void               getPayloadCommands( QStringList&, QStringList& );
    // Return the type of server connection managed by the socket
    virtual ServerType getServerType() const;
    // Whether or not the connection is active
    virtual bool       isConnected() const;
    // Test whether the given command is an error.
    bool               isErrorCommand( const QString &command ) const;
    virtual bool       isPayloadCommandOnReceive( const QString& ) const;
    virtual bool       isPayloadCommandOnSend( const QString& ) const;
    virtual void       setPayloadCommands( const QStringList&, const QStringList& );
    // Set whether we're sending pings or not
    virtual void       setSendPings( bool sendPings ) = 0;
    // Write data to the socket without conversions
    virtual void       writeBinaryData( const QByteArray& data ) = 0;

  protected:  // Protected properties
    // State variable to detect if the connection is active
    bool               connected_;
    /// List of receivable commands which carry a data payload
    QStringList        receivablePayloadCommands_;
    /// List of sendable commands which carry a data payload
    QStringList        sendablePayloadCommands_;
    // The type of server which we will connect to (notification,switchboard,...)
    ServerType         serverType_;
    KMess::MsnSession *session_;

  signals: // Public signals
    // Signal that the server has connected
    void               connected();
    // Signal that incoming data is ready to be parsed
    void               dataReceived( const QStringList &commandLine, const QByteArray &payloadData );
    // Signal that the server has disconnected
    void               disconnected( KMess::DisconnectReason reason = KMess::UnexpectedDisconnect );
    // Signal that a ping to the connection has been sent
    void               pingSent();
    // Ask the user to authenticate on a proxy
    void               proxyAuthenticationRequired( const QNetworkProxy &proxy, QAuthenticator *authenticator );
    // Signal to send status events
    void               statusEvent( const KMess::StatusMessageType type, const KMess::StatusMessage message, const QVariant &extraInfo = QVariant() );

};

}; // namespace KMess



#endif
