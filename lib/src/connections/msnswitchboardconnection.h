/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Mike K. Bennett <mkb137b@hotmail.com>                     *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file msnswitchboardconnection.h
 */

#ifndef MSNSWITCHBOARDCONNECTION_H
#define MSNSWITCHBOARDCONNECTION_H

#include "msnconnection.h"

#include <QMap>
#include <QPair>
#include <QStringList>

#include <KMess/EmoticonMessage>

// Forward declarations
namespace KMess
{
  class MsnSession;
  class Message;
};


namespace KMessInternal
{

  class SwitchboardManager;

/**
 * A connection to a switchboard server, over which chats are conducted.
 *
 * @author Mike K. Bennett
 * @ingroup NetworkCore
 */
class MsnSwitchboardConnection : public MsnConnection
{
  friend class KMess::Debugger;
  // Only these two classes may create a switchboard connection:
  friend class KMessInternal::SwitchboardManager;
  friend class KMess::MsnChat;

  Q_OBJECT

  friend class KMessTest;


  public: // Public enumerations
    // The supported applications
    enum ApplicationType
    {
      GNOMEMEETING
    , FILETRANSFER
    , MSNREMOTEDESKTOP
    , VOICECONVERSATION
    };

    enum SwitchboardState
    {
      StateConnected,           // socket connected
      StateConnecting,          // socket connecting
      StateDisconnected,        // socket closed
      StateAuthorizing,         // authorizing to SB.
      StateWaitingForContact,   // waiting for someone to join (either IRO or JOI)
      StateReady,               // ready to send messages
      StateBusy                 // waiting to send pending messages.
    };

    // The destructor
    virtual             ~MsnSwitchboardConnection();
    // Close the connection, only for emergency situations
    virtual void         closeConnection();
    // Clean up, close the connection, destroy this object
    void                 closeConnectionLater(bool autoDelete = false);
    // Initialize the object, optionally presetting a contact to reinvite
    bool                 initialize( QString handle = QString() );
    // Invite a contact into the chat
    void                 inviteContact( const QString& handle );
    // Check whether the switchboard is buzy (has too many pending messages)
    bool                 isBusy() const;
    // Check if the switchboard is "ready", ie, we've authorized and invited contacts
    // and are ready to go.
    bool                 isReady() const;
    // Connect to the server, in response to a user request
    void                 start( const QString &ip, quint16 port, const QString &authorization );
    // Connect to the server, in response to a contact invitation
    void                 start( const QString &ip, quint16 port, const QString &authorization, const QString &chatId );
    // Get the state of the switchboard.
    SwitchboardState     state() const;
    // Send a message over the SB
    qint32               sendMessage( KMess::Message &message );
    // Clean the old unacked messages
    void                 cleanUnackedMessages();

  protected slots:
    // The socket connected, so send the version command.
    virtual void         slotConnected();

  private: // Private methods
    // The constructor
                         MsnSwitchboardConnection( KMess::MsnSession* session );
    // Received a positive delivery message.
    void                 gotAck(const QStringList& command);
    // Received notification that a contact is no longer in session.
    void                 gotBye(const QStringList& command);
    // Received the initial roster information for new contacts joining a session.
    void                 gotIro(const QStringList& command);
    // Received notification of a new client in the session.
    void                 gotJoi(const QStringList& command);
    void                 gotMessage( const QStringList& command, const QByteArray &payload );
    // Received a negative acknowledgement of the receipt of a message.
    void                 gotNak(const QStringList& command);
    // Received notification of the termination of a client-server session.
    void                 gotOut(const QStringList& command);
    // Received a client-server authentication message.
    void                 gotUsr(const QStringList& command);

    void                 parseCommand( const QStringList& command );
    void                 parseError( const QStringList& command, const QByteArray &payloadData );
    virtual void         parsePayloadCommand( const QStringList &command, const QByteArray &payload );

  private: // Private attributes
    // The number of ACK messages pending
    int                  acksPending_;
    // The authorization used to connect to the switchboard server
    QString              authorization_;
    // The id of the chat, used for authenticating a contact-started chat
    QString              chatId_;
    // Whether this object is closing it's connection using closeConnectionLater()
    bool                 closingConnection_;
    /// Emoticon set kept for the next chat message
    KMess::EmoticonMessage currentEmoticonMessage_;
    /// Queue of sent messages waiting for an ACK or NAK
    QMap<int,KMess::Message> unAckedMessages_;
    // Whether the session was created due to a contact invite, or by user request
    bool                 isInvitationSession_;
    // Whether the switchboard is ready to send/receive messages.
    bool                 isReady_;
    // Current SB state
    SwitchboardState     state_;

  signals:
    // The switchboard is done authenticating and is ready to invite contacts.
    void                 connectionAuthenticated();
    // The switchboard is now connected
    void                 connectionOpened();
    // The switchboard is ready to send messages
    void                 connectionReady();
    // Signal that a contact joined (or was already in) the chat
    void                 contactJoined( const QString &handle );
    // Signal that a contact left the chat
    void                 contactLeft( const QString &handle, bool isChatIdle );
    // The switchboard is closing, remove it from the Chat Master references
    void                 deleteMe( KMessInternal::MsnSwitchboardConnection *object );
    // The switchboard failed to invite a contact
    void                 invitationFailed();
    // Received a chat message
    void                 receivedMessage( KMess::Message message );
    /// Signal that the delivery of a message has failed
    void                 sendingFailed( const KMess::Message message, const QString &recipient );
    /// Signal that the delivery of a message has succeeded
    void                 sendingSucceeded( const KMess::Message message );
    // Signal that a warning message needs to be displayed in chat
    void                 showWarning( const QString &handle, KMess::ChatWarningType type );

    void                 dataSent(const quint32 ackNumber);
    void                 dataSendFailed(const quint32 ackNumber);
};

} // namespace KMess

#endif
