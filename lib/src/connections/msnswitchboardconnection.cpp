/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Mike K. Bennett <mkb137b@hotmail.com>                     *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file msnswitchboardconnection.cpp
 */

#include "msnswitchboardconnection.h"
#include "debug/libkmessdebug.h"

#include <KMess/MsnSession>
#include <KMess/TextMessage>
#include <KMess/EmoticonMessage>
#include <KMess/TypingMessage>
#include <KMess/MsnAlgorithms>
#include <KMess/ContactList>
#include <KMess/Self>
#include <KMess/MPOPEndpoint>

#include "../soap/offlineimservice.h"

#include <QFile>
#include <QFont>
#include <QRegExp>
#include <QUrl>

#include <math.h>

#ifdef KMESSDEBUG_SWITCHBOARD
  #define KMESSDEBUG_SWITCHBOARD_GENERAL
  #define KMESSDEBUG_SWITCHBOARD_P2P
  #define KMESSDEBUG_SWITCHBOARD_EMOTICONS
  #define KMESSDEBUG_SWITCHBOARD_CONTACTS
  #define KMESSDEBUG_SWITCHBOARD_ACKS
#endif


/**
 * Message expiration time.
 *
 * When a message receives its acknowledgement, the message is forgotten,
 * but if no answer is received, it expires and is considered as not
 * received.
 * This value controls after how many milliseconds a sent message is considered
 * expired.
 *
 * Current value: 300,000, 1000 milliseconds * 60 seconds * 5 minutes
 */
#define MESSAGE_EXPIRATION_TIME   300000

#define MAX_PENDING_MESSAGES 3


using namespace KMess;
using namespace KMessInternal;



// The constructor
MsnSwitchboardConnection::MsnSwitchboardConnection( MsnSession* session )
 : MsnConnection( session, MsnSocketBase::SERVER_SWITCHBOARD )
 , acksPending_( 0 )
 , isInvitationSession_( false )
 , isReady_( false )
 , state_ ( StateDisconnected )
{
  setObjectName( "MsnSwitchboardConnection" );

  // Determine which payload commands can be received by the Switchboard Connection
  const QStringList payloadCommands( "MSG" );
  setPayloadCommands( payloadCommands, payloadCommands );
}



// The destructor
MsnSwitchboardConnection::~MsnSwitchboardConnection()
{
  debug() << "DESTROYING...";

  // Close the connection
  closeConnection();

  emit deleteMe( this );

  debug() << "DESTROYED.";
}



/**
 * Clean the old unacked messages.
 *
 * When the server tries to send a message, it may succeed or fail.
 * If the sent message specifies it, the server will answer with a
 * negative or positive answer. However, when the server doesn't
 * receive anything, no answer comes back: this method assumes
 * non-answered messages as lost, and reports the sending failure.
 */
void MsnSwitchboardConnection::cleanUnackedMessages()
{
  uint listCount = unAckedMessages_.count();

  // Find and remove expired entries
  QMapIterator<int,Message> it( unAckedMessages_ );
  while( it.hasNext() )
  {
    it.next();

    int ack          = it.key();
    Message message = it.value();

    // If the message is expired, remove it
    if( message.time().elapsed() < MESSAGE_EXPIRATION_TIME )
    {
      continue;
    }

    unAckedMessages_.remove( ack );

    // If the message required a confirmation, send it
    // (we assume non-acked messages have been lost)
    if( message.confirmationMode() != ConfirmNever )
    {
      QString sender;
      if( message.hasField( "To" ) )
      {
        sender = message.field( "To" );
      }
      else if( message.hasField( "P2P-Dest" ) )
      {
        sender = message.field( "P2P-Dest" );
      }
      else
      {
        sender = '*';
      }

      emit sendingFailed( message, sender );
    }
  }

  int currentListCount = unAckedMessages_.count();
  debug() << "Removed" << ( listCount - currentListCount ) << "messages:"
           << "kept" << currentListCount << "messages until those expire.";
}



// Close the connection
void MsnSwitchboardConnection::closeConnection()
{
  state_ = StateDisconnected;

  disconnectFromServer();

  debug() << "Session ended";
}



// Received a positive delivery message.
void MsnSwitchboardConnection::gotAck( const QStringList& command )
{
  // Remove the ACKed message from the queue.
  int ackNumber = command[1].toUInt();
  if( ! unAckedMessages_.contains( ackNumber ) )
  {
    warning() << "Received an ACK message but message is not in sent queue;  must be for p2p.";
    emit dataSent(ackNumber);
    return;
  }

  // Get the message from the queue, then remove it
  Message message = unAckedMessages_[ ackNumber ];
  unAckedMessages_.remove( ackNumber );
  acksPending_--;

  debug() << "Received one ACK message, still" << acksPending_ << "unacked.";

  if( message.confirmationMode() == ConfirmAlways )
  {
    emit sendingSucceeded( message );
  }

  // Signal that the switchboard is no longer busy and can accept new application messages.
  if( acksPending_ < MAX_PENDING_MESSAGES-1 )
  {
    state_ = StateReady;
    emit connectionReady();
  }
  else
  {
    state_ = StateBusy;
  }
}



// Received notification that a contact is no longer in session.
void MsnSwitchboardConnection::gotBye( const QStringList& command )
{
  // If there isn't numerical final argument, the BYE was received because contact has left the chat
  // else the conversation times-out.
  bool isChatIdle = ( command.count() == 3 );

  const QString& handle( command[1].toLower() );

  debug() << "Contact left:" << handle << ( isChatIdle ? "(due to time-out)" : "" );

  // Notify the leave
  emit contactLeft( handle, isChatIdle );
}



// Received the initial roster information for new contacts joining a session.
void MsnSwitchboardConnection::gotIro( const QStringList& command )
{
  if( ! isInvitationSession_ )
  {
    warning() << "Received IRO command in an user requested session!";
    return;
  }

  state_ = StateReady;

  QString handle( command[4].toLower() );

#ifdef __GNUC__
  #warning MPOP: Machine GUID/IRO.
#endif
  // TODO: Machine GUID?
  handle = handle.section( ';', 0, 0 );

  // Ignored command arguments: friendly name (command[5]) and capabilities (command[6])

  debug() << "Contact joined:" << handle;

  // Notify the join
  emit contactJoined( handle );
}



// Received notification of a new client in the session.
void MsnSwitchboardConnection::gotJoi( const QStringList& command )
{
  QString handle( command[1].toLower() );

  // Ignored command arguments: friendly name (command[2]) and capabilities (command[3])
#ifdef __GNUC__
  #warning MPOP: Machine GUID/JOI.
#endif
  // TODO: Machine GUID?
  handle = handle.section( ';', 0, 0 );

  if ( handle == session_->sessionSetting( "AccountHandle" ) )
  {
    return; // we get JOI for ourselves now - TODO handle MPOP here too i guess.
  }

  debug() << "Contact joined:" << handle;

  state_ = StateReady;

  // Notify the join
  emit contactJoined( handle );
}



/**
 * A message has been received.
 *
 * @param command The full command line
 * @param payload The message contents
 */
void MsnSwitchboardConnection::gotMessage( const QStringList& command, const QByteArray &payload )
{
  KMESS_ASSERT( command.size() > 1 );

  const QString& contactHandle( command[1] );

  Message message = Message::fromData( session_->contactList()->contact( contactHandle ),
                                        DirectionIncoming,
                                        payload );

  debug() << "Received message of type" << message.type() << "from:" << contactHandle;

  if( message.type() == InvalidMessageType )
  {
    warning() << "Received message of unknown type!";
    warning() << message.contents();
  }

  // Save emoticon messages for the next text message, to associate the
  // emoticon shortcuts in it with the actual MsnObjects
  if( message.type() == EmoticonMessageType )
  {
    debug() << "Saving emoticon message for later...";

    currentEmoticonMessage_ = EmoticonMessage( message );
    return;
  }

  // Associate a message with its emoticons
  if( message.type() == TextMessageType )
  {
    debug() << "Attaching emoticon message to incoming text message...";

    TextMessage textMessage( message );
    textMessage.setEmoticons( currentEmoticonMessage_.emoticons() );
    currentEmoticonMessage_ = Message();
    message = textMessage;
  }

  emit receivedMessage( message );
}



// Received a negative acknowledgement of the receipt of a message.
void MsnSwitchboardConnection::gotNak( const QStringList& command )
{
  // Check if the ACK exists in the map.
  int ackNumber = command[1].toUInt();
  if( ! unAckedMessages_.contains( ackNumber ) )
  {
    warning() << "Received a NAK message but message is not in sent queue; must be for p2p.";
    emit dataSendFailed(ackNumber);
    return;
  }

  // Get the message from the queue, then remove it
  Message message = unAckedMessages_[ ackNumber ];
  unAckedMessages_.remove( ackNumber );
  acksPending_--;

  debug() << "Received one NAK message, still " << acksPending_ << " unacked.";

  if( message.confirmationMode() != ConfirmNever )
  {
    QString sender;
    if( message.hasField( "To" ) )
    {
      sender = message.field( "To" );
    }
    else if( message.hasField( "P2P-Dest" ) )
    {
      sender = message.field( "P2P-Dest" );
    }
    else
    {
      sender = '*';
    }

    emit sendingFailed( message, sender );
  }

  // Signal that the switchboard is no longer busy and can accept new application messages.
  if( acksPending_ < MAX_PENDING_MESSAGES-1 )
  {
    emit connectionReady();
  }
}



// Received notification of the termination of a client-server session.
void MsnSwitchboardConnection::gotOut( const QStringList& command )
{
  Q_UNUSED( command );

  debug() << "Server is closing the connection";

  closeConnection();
}



// Received a client-server authentication message.
void MsnSwitchboardConnection::gotUsr( const QStringList& command )
{
  // This should just be a confirmation
  if( command[2] != "OK" )
  {
    warning() << "Switchboard authentication failed!";
    return;
  }

  debug() << "Authentication successful";

  state_ = StateWaitingForContact;

  emit connectionAuthenticated();
}



// Invite a contact into the chat
void MsnSwitchboardConnection::inviteContact( const QString& handle )
{
  if( handle.isEmpty() )
  {
    warning() << "Empty invitation request!";
    return;
  }

  sendCommand( "CAL", handle );

  debug() << "Inviting contact:" << handle;
}



// Check whether the switchboard is busy (has too many pending messages)
bool MsnSwitchboardConnection::isBusy() const
{
  return state_ == StateBusy;

  // unAckedMessages_ also contains messages what have a "NAK_ONLY" flag.
  // keep a special variable that only lists the normal ACKs.
  //return ( acksPending_ > MAX_PENDING_MESSAGES );
}



/**
 * Return true if the switchboard is ready to send/receive messages, false otherwise.
 *
 * The switchboard becomes ready when all authorization is complete and initial contacts
 * have been invited. If this was not a user-started switchboard session then it
 * becomes ready when we have received at least one IRO.
 */
bool MsnSwitchboardConnection::isReady() const
{
  return state_ == StateReady;
}



// Parse a regular command
void MsnSwitchboardConnection::parseCommand( const QStringList& command )
{
  if( command[0] == "ACK" )
  {
    gotAck( command );
  }
  else if( command[0] == "ANS" )
  {
    // Do nothing.
  }
  else if( command[0] == "BYE" )
  {
    gotBye( command );
  }
  else if( command[0] == "CAL" )
  {
    // Do nothing
  }
  else if( command[0] == "IRO" )
  {
    gotIro( command );
  }
  else if( command[0] == "JOI" )
  {
    gotJoi( command );
  }
  else if( command[0] == "NAK" )
  {
    gotNak( command );
  }
  else if( command[0] == "OUT" )
  {
    gotOut( command );
  }
  else if( command[0] == "USR" )
  {
    gotUsr( command );
  }
  else
  {
    warning() << "Unhandled command!";
    warning() << command.join( " " );
  }
}



// Parse an error command
void MsnSwitchboardConnection::parseError( const QStringList& command, const QByteArray &payloadData )
{
  // TODO: Check if any payload is delivered for these errors: it may contain
  // info about the error. For 215 it may specify which contact was invited
  // twice.
  if ( command[0] == "215" )
  {
    warning() << "A contact was invited twice!";
  }
  else if ( command[0] == "712" )
  {
    warning() << "The SB session is overloaded.";
  }
  else if( command[0] == "216" || command[0] == "217" )
  {
    emit invitationFailed();
  }
  else if ( command[0] == "282" )
  {
    warning() << "bad ptp data message sent.";

    emit dataSendFailed(command[1].toUInt());
  }
  else if ( command[0] == "911" )
  {
    warning() << "Authentication failed.";
  }
  else
  {
    // Relay the error detection to the base class
    MsnConnection::parseError( command, payloadData );
  }
}



// Parse a payload command
void MsnSwitchboardConnection::parsePayloadCommand( const QStringList &command, const QByteArray &payload )
{
  // Identify and handle the command
  if( command[0] == "MSG" )
  {
    gotMessage( command, payload );
  }
  else
  {
    warning() << "Unhandled payload command!";
    warning() << command.join( " " );
    warning() << QString::fromUtf8( payload.data(), payload.size() );
  }
}



/**
 * Send a message to the server, storing the message in queue.
 *
 * @param message The message to send
 */
qint32 MsnSwitchboardConnection::sendMessage( Message &message )
{
  debug() << "Sending message of type:" << message.type();

  ConfirmationMode ackMode = message.confirmationMode();

  // Determine which ack type to request
  QChar ackType = '?';
  switch( ackMode )
  {
    case ConfirmNever:    ackType = 'U'; break;
    case ConfirmFailure:  ackType = 'N'; break;
    case ConfirmAlways:   ackType = 'A'; break;
    case ConfirmPtpData:  ackType = 'D'; break;
  }

  const QByteArray& rawMessage( message.contents() );

  int ack = sendCommand( "MSG", ackType, rawMessage );

  // Don't even store if it doesn't need to be confirmed
  if( ackMode == ConfirmNever )
  {
    return 0;
  }

  // Only update the pending acks list when we'll always require an ack back
  if( ackMode == ConfirmAlways )
  {
    acksPending_++;
    if ( acksPending_ > MAX_PENDING_MESSAGES )
    {
      state_ = StateBusy;
    }
  }

  if ( ackMode != ConfirmPtpData )
  {
    // Create a record for the new unacked message
    unAckedMessages_.insert( ack, message );

    debug() << "Stored message for acknowledgement."
            << "Currently" << unAckedMessages_.count() << "messages stored,"
            << acksPending_ << "need an ACK";
  }

  return ack;
}



// The connection was established, so send the version command.
void MsnSwitchboardConnection::slotConnected()
{
  debug() << "Connected to the server, sending authentication";

  state_ = StateConnected;

  emit connectionOpened();

  state_ = StateAuthorizing;

  QString command;
  QStringList parameters;

  parameters << session_->self()->handle() +
                ';' +
                session_->self()->localEndpoint()->guid();
  parameters << authorization_;

  if( isInvitationSession_ )
  {
    // We need to add the chat ID when answering to a call
    parameters << chatId_;

    // Answer the session request with the authorization
    command = "ANS";
  }
  else
  {
    // Send the initial session authorization
    command = "USR";
  }

  sendCommand( command, parameters.join( " " ) );
}



// Connect to the server, in response to a user request
void MsnSwitchboardConnection::start( const QString &ip, quint16 port, const QString &authorization )
{
  KMESS_ASSERT( session_ != 0 );
  KMESS_ASSERT( !isConnected() );

  state_ = StateConnecting;

  debug() << "Connecting to" << ip << ":" << port << "in response to an user request";

  // Store the authentication information, for the USR command
  unAckedMessages_.clear();
  isInvitationSession_ = false;
  authorization_       = authorization;

  // Connect to the server.
  connectToServer( ip, port );
}



// Connect to the server, in response to a contact invitation
void MsnSwitchboardConnection::start( const QString &ip, quint16 port, const QString &authorization, const QString &chatId )
{
  KMESS_ASSERT( session_ != 0 );
  KMESS_ASSERT( !isConnected() );

  state_ = StateConnecting;

  debug() << "Connecting to" << ip << ":" << port << "in response to an invitation";

  // Store the authentication information, for the USR command
  unAckedMessages_.clear();
  isInvitationSession_ = true;
  authorization_       = authorization;
  chatId_              = chatId;

  // Connect to the server.
  connectToServer( ip, port );
}



/**
 * Return the state of the switchboard.
 */
MsnSwitchboardConnection::SwitchboardState MsnSwitchboardConnection::state() const
{
  return state_;
}
