/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Mike K. Bennett <mkb137b@hotmail.com>                     *
 * (C) Copyright Sjors Gielen <sjors@kmess.org>                            *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file msnsession.cpp
 * @brief Network library interface
 */

#include "debug/libkmessdebug.h"

#include <KMess/MsnAlgorithms>
#include <KMess/MsnContact>
#include <KMess/MsnObject>
#include <KMess/MsnSession>
#include <KMess/NetworkGlobals>
#include <KMess/Utils>
#include <KMess/MPOPEndpoint>
#include <KMess/MsnGroup>
#include <KMess/ContactList>
#include <KMess/Self>

#include "soap/addressbookservice.h"
#include "connections/msnnotificationconnection.h"
#include "utils/utils_internal.h"
#include "chatfactory.h"
#include "listsmanager.h"
#include "ptpglue.h"

#include <QFile>
#include <QBuffer>


// Uncomment this to enable debugging the session settings.
// #define LIBKMESS_MSN_DEBUG_SETTINGS

using namespace KMess;

/*
#include "Ptp/Platform"
using namespace PtpCollaboration;
*/

// The constructor
MsnSession::MsnSession()
 : contactList_(0),
   chatFactory_(0),
   msnNotificationConnection_(0),
   msnObject_(0),
   ptpGlue_(0),
   switchboardManager_(0)
{
  debug() << "Creating session object";

  init();
}



/**
 * Init deletes and re-creates all of our objects and state.
 * 
 * This is called on construction and on disconnect.
 */
void MsnSession::init()
{
  DELETE( contactList_ );
  DELETE( switchboardManager_ );
  DELETE( ptpGlue_ );
  if ( msnNotificationConnection_ )
  {
    msnNotificationConnection_->deleteLater();
  }
  DELETE( chatFactory_ );

  // Start up some required services
  contactList_ = new ContactList( this );
  ptpGlue_ = new KMessInternal::PtpGlue( this );
  msnNotificationConnection_ = new KMessInternal::MsnNotificationConnection( this, contactList_ );
  chatFactory_ = new KMessInternal::ChatFactory( this );
  switchboardManager_ = new KMessInternal::SwitchboardManager( this, chatFactory_ );

  // tie the NS connection to Self()...
  self()->_setNsConnection( msnNotificationConnection_ );

  // chained signals from NS connection
  CHAIN_SIGNAL( msnNotificationConnection_, this, loggedIn() );
  CHAIN_SIGNAL( msnNotificationConnection_, this, connectionFailed(KMess::StatusMessage) );
  CHAIN_SIGNAL( msnNotificationConnection_, this, contactAddedUser(KMess::MsnContact*) );
  CHAIN_SIGNAL( msnNotificationConnection_, this, contactOnline(KMess::MsnContact*) );
  CHAIN_SIGNAL( msnNotificationConnection_, this, contactOffline(KMess::MsnContact*) );
  CHAIN_SIGNAL( msnNotificationConnection_, this, contactChangedStatus(KMess::MsnContact*, KMess::MsnStatus) );
  CHAIN_SIGNAL( msnNotificationConnection_, this, proxyAuthenticationRequired(QNetworkProxy,QAuthenticator*) );
  CHAIN_SIGNAL( msnNotificationConnection_, this, newEmail(QString,QString,bool,QString,QString,QString) );
  CHAIN_SIGNAL( msnNotificationConnection_, this, statusEvent(KMess::StatusMessageType,KMess::StatusMessage,QVariant) );
  CHAIN_SIGNAL( msnNotificationConnection_, this, gotComposeUrl(QString,QString) );

  // non-chained signals; we do some processing before emitting the
  // appropriate signal.
  connect( msnNotificationConnection_, SIGNAL(            loggedIn()        ),
           this,                       SLOT  (      mns_OnLoggedIn()        ) );
  connect( msnNotificationConnection_, SIGNAL(        disconnected(KMess::DisconnectReason)        ),
           this,                       SLOT  (     mns_OnLoggedOut(KMess::DisconnectReason)        ) );

  connect( switchboardManager_, SIGNAL(     switchboardCreated( KMessInternal::MsnSwitchboardConnection* ) ),
           ptpGlue_,            SLOT(   slotSwitchboardCreated( KMessInternal::MsnSwitchboardConnection* ) ) );

  // Connect some signals
  connect( msnNotificationConnection_, SIGNAL(      invitedToChat(QString,QString,quint16,QString,QString)     ),
           switchboardManager_,        SLOT  (   startSwitchboard(QString,QString,quint16,QString,QString)     ) );

  connect( msnNotificationConnection_, SIGNAL(    receivedMessage(KMess::Message)         ),
           chatFactory_,               SLOT  (            message(KMess::Message)         ) );

  CHAIN_SIGNAL( chatFactory_, this,         chatCreated(KMess::MsnChat*) );
  CHAIN_SIGNAL( chatFactory_, this,       chatDestroyed(KMess::MsnChat*) );
  CHAIN_SIGNAL( chatFactory_, this, chatMessageReceived(KMess::Message)  );

  connect( chatFactory_,               SIGNAL( switchboardRequested(KMess::MsnChat* )  ),
           msnNotificationConnection_, SLOT  (         inviteToChat(KMess::MsnChat* ) ) );

  // Initialize the available session settings
  if( sessionSettings_.isEmpty() )
  {
    // The account handle
    sessionSettings_[ "AccountHandle"                ].clear();
    // The account password
    sessionSettings_[ "AccountPassword"              ].clear();
    // Whether the Live account has been verified via mail or not
    sessionSettings_[ "AccountVerified"              ] = false;
    // Whether or not KMess should reply automatically to incoming chat messages
    sessionSettings_[ "AutoReplyEnabled"             ] = false;
    // Whether the account has webmail access and supports email notifications
    sessionSettings_[ "EmailEnabled"                 ] = false;
    // The number of emails in the user's inbox
    sessionSettings_[ "EmailCount"                   ] = 0;
    // Localization code of this account. It's the locale which the Live site uses with this account
    sessionSettings_[ "LanguageCode"                 ].clear();
    // Country name registered with this account.
    sessionSettings_[ "Country"                      ].clear();
    // The machine's local IP address
    sessionSettings_[ "LocalIp"                      ].clear();
    // The logon mode
    sessionSettings_[ "LogonMode"                    ] = LogonMultiple;
    // The override Messenger Notification Server address. Ignored if empty.
    sessionSettings_[ "NotificationServerOverride"   ].clear();
    // The current media being played
    sessionSettings_[ "NowListeningMedia"            ] = QStringList();
    // The key required to send offline messaging requests
    sessionSettings_[ "OfflineMessagingKey"          ].clear();
    // Our public IP address, as seen from the MSN server
    sessionSettings_[ "PublicIp"                     ].clear();
    // Our public port, as seen from the MSN server
    sessionSettings_[ "PublicPort"                   ] = 0;
    // Lowest port used for P2P transfers
    sessionSettings_[ "P2PTransferLowestPort"        ] = 6891;
    // Highest port used for P2P transfers
    sessionSettings_[ "P2PTransferHighestPort"       ] = 6900;
    // The email address considered by the server to be the user's preferred address. No idea when it differs from the handle
    sessionSettings_[ "PreferredEmail"               ].clear();
    // Value which can be used to access Hotmail. Unused with Live Mail (why is this still here?)
    sessionSettings_[ "SessionId"                    ].clear();
    // SOAP redirection server - any requests will be sent to this server port 4430. (If empty, the default SOAP servers will be used.)
    sessionSettings_[ "SoapRedirectionServer"        ].clear();
    // SOAP authentication token: Contacts server
    sessionSettings_[ "SoapTokenContacts"            ].clear();
    // SOAP authentication token: Messenger site auth
    sessionSettings_[ "SoapTokenMessenger"           ].clear();
    // SOAP authentication token: Messenger login ticket
    sessionSettings_[ "SoapTokenMessengerClear"      ].clear();
    // SOAP authentication token: Messenger login binary secret
    sessionSettings_[ "SoapTokenMessengerClearProof" ].clear();
    // SOAP authentication token: Unknown (Live site auth?)
    sessionSettings_[ "SoapTokenMessengerSecure"     ].clear();
    // SOAP authentication token: Passport site ticket
    sessionSettings_[ "SoapTokenPassport"            ].clear();
    // SOAP authentication token: Passport site binary secret
    sessionSettings_[ "SoapTokenPassportProof"       ].clear();
    // SOAP authentication token: Storage REST
    sessionSettings_[ "SoapTokenStorage"             ].clear();
    // Initial status
    sessionSettings_[ "StatusInitial"                ] = OfflineStatus;
    // URL to access the account settings Live site
    sessionSettings_[ "UrlAccount"                   ].clear();
    // URL to access the personal profile at the Live site
    sessionSettings_[ "UrlProfile"                   ].clear();
    // URL to access the Live Mail site
    sessionSettings_[ "UrlWebmail"                   ].clear();
    // URL to access the inbox page at the Live Mail site
    sessionSettings_[ "UrlWebmailInbox"              ].clear();
    // URL to access the new email compose page at the Live Mail site
    sessionSettings_[ "UrlWebmailCompose"            ].clear();
    // Parameter needed to update the user info on the Roaming Service
    sessionSettings_[ "ProfileResourceId"            ].clear();
  }
}



// The destructor
MsnSession::~MsnSession()
{
  debug() << "DESTROYING...";

  // Disconnect first
  if( msnNotificationConnection_->isConnected() )
  {
    msnNotificationConnection_->closeConnection();
  }

  delete contactList_;
  delete chatFactory_;
  delete msnObject_;
  delete msnNotificationConnection_;
  msnNotificationConnection_ = 0;

  delete switchboardManager_;
  delete ptpGlue_;
//  Platform::instance()->dispose();

  debug() << "DESTROYED";
}



/**
 * Return an instance to the contact list manager.
 * 
 * Use this object to manipulate your contact list once connected.
 */
ContactList *MsnSession::contactList() const
{
  return contactList_;
}



/**
 * Return the PtpGlue object that belongs to this session.
 */
KMessInternal::PtpGlue *MsnSession::ptpGlue() const
{
  return ptpGlue_;
}



/**
 * Request a Hotmail URL that will open the compose email page for the given address.
 * 
 * The session must be connected, and must be connected with a Hotmail-based passport address.
 * 
 * If the connected address is not Hotmail based, this method will print a warning and do
 * nothing.
 * 
 * @param toAddress Email address to send email to.
 */
void MsnSession::requestComposeUrl( const QString &toAddress )
{
  if ( ! msnNotificationConnection_ || ! msnNotificationConnection_->isConnected() )
  {
    warning() << "Not connected; ignoring COMPOSE request";
    return;
  }
  
  if ( ! sessionSettingBool( "EmailEnabled" ) )
  {
    warning() << "Connected handle is not Hotmail-enabled; ignoring COMPOSE request.";
    return;
  }

  msnNotificationConnection_->requestComposeUrl( toAddress );
}



// Alter the quantity of unread email
void MsnSession::setEmailCount( int change )
{
  int oldEmailCount = sessionSettingInt( "EmailCount" );

  KMESS_ASSERT( ( oldEmailCount == (  oldEmailCount + change ) ) || ( oldEmailCount == 0 ) );

  setSessionSetting( "EmailCount", oldEmailCount + change );
}



/**
 * Set the full path to the directory that will store address book cache information.
 *
 * It is a very good idea to use this feature since it will result in quicker sign in times.
 * This is because libkmess-msn does not have to read the entire contact list from the server each time, rather,
 * it only requests the most recent changes.
 *
 * If the cache directory does not exist, it will be created.
 *
 * @param path The full path to the caching directory.
 */
void MsnSession::setCacheDir( const QString &path )
{
  msnNotificationConnection_->setCacheDir( path );
}



/**
 * @brief Create tokens for hotmail login
 *
 * The returned string is a token for Hotmail login. It should be given
 * as a POST parameter named "token" to the following URL:
 *   https://login.live.com/ppsecure/sha1auth.srf
 * A "lc" argument can be given to that URL via GET (?lc=..) which is the
 * language code the page should be in.
 *
 * @param folder the folder you need access to.
 */
const QString MsnSession::createHotmailToken( const QString &folder ) const
{
  KMess::MsnAlgorithms algorithms;
  return algorithms.createHotmailToken( sessionSettingString( "SoapTokenPassport" ),
                                        sessionSettingString( "SoapTokenPassportProof" ),
                                        folder );
}



/**
 * @brief Create an empty MsnChat object.
 *
 * The new object will be seeded with the handle you give it. I.e. if the
 * user wants to start chatting using that chat object, this handle will be
 * invited into the chat.
 *
 * The object will not be automatically connected to a MSN Switchboard session.
 */
KMess::MsnChat *MsnSession::createMsnChat( const QString &handle )
{
  MsnChat *chat = chatFactory_->createChat( handle );

  return chat;
}



/**
 * Ensure a setting exists, or crash otherwise.
 *
 * If it doesn't, this method crashes. After calling
 * this method, it's safe to use the setting name.
 *
 * This is a drastic measure to avoid spelling errors in setting names going unnoticed.
 *
 * This method takes a const char* explicitly, because any crashes will make
 * the value of 'name' show up in gdb backtraces, along with what method
 * actually made the request.
 *
 * @param name  Setting name to verify
 */
void MsnSession::ensureSessionSettingExists( const char *name ) const
{
  // There's something wrong if an invalid setting is asked for - quit
  if( ! sessionSettings_.contains( name ) )
  {
    warning() << "Tried to access an incorrect session setting name:" << name;
    abort();
  }
}



/**
 * Retrieve a session setting's value.
 *
 * @param name   Name of the setting to get
 */
QVariant MsnSession::sessionSetting( const QString &name ) const
{
  ensureSessionSettingExists( name.toAscii() );

#ifdef LIBKMESS_MSN_DEBUG_SETTINGS
  debug() << "Retrieving from setting" << name << "the value:" << sessionSettings_[ name ];
#endif

  return sessionSettings_[ name ];
}



/**
 * @brief Return whether the notification connection is currently connected.
 *
 * Warning: Advanced function. Advanced functions are not recommended for use,
 * but can be used by applications to extend MsnSession's functionality. They
 * may result in disconnection, weird bugs, or more serious problems. Use at
 * your own risk, but feel free to report bugs if anything unexpected happens
 * or if you're missing functionality requiring you to use these methods.
 *
 * @see sendCommand()
 * @see sendMimeMessage()
 */
bool MsnSession::isNotificationConnected() const
{
  return msnNotificationConnection_->isConnected();
}



/**
 * @brief Return whether we're connected to the service.
 */
bool MsnSession::isLoggedIn() const
{
  if( ! msnNotificationConnection_ )
  {
    return false;
  }

  return msnNotificationConnection_->isConnected();
}



/**
 * @brief Start connecting to the server.
 *
 * @param handle   The handle
 * @param password The password for the current account
 * @param status   Initial status
 */
void MsnSession::logIn( const QString &handle, const QString &password, const MsnStatus status )
{
  if( handle.isEmpty() || password.isEmpty() )
  {
    warning() << "Attempted to logIn, but handle or password are empty";
    return;
  }
  debug() << "Opening server connection";

  // Set the current account details: handle, password and the initial status
  sessionSettings_[ "AccountHandle"   ] = handle;
  sessionSettings_[ "AccountPassword" ] = password;
  sessionSettings_[ "StatusInitial"   ] = status;

  // Update the user's MsnContact
  self()->setId( handle );

  // Signal that we're now connecting
  emit connecting();

  msnNotificationConnection_->openConnection();
}



/**
 * @brief Log out locally from the MSN Messenger service.
 *
 * @see logOutFrom
 * @see logOutEverywhere
 */
void MsnSession::logOut() const
{
  debug() << "Logging out locally and closing server connection";

  msnNotificationConnection_->closeConnection();
}



/**
 * @brief Logs out all endpoints from the MSN messenger service.
 * @see logOut
 * @see logOutFrom
 */
void MsnSession::logOutEverywhere() const
{
  debug() << "Logging out from all endpoints and closing server connection.";

  // yes, WLM really does send "gtfo" in the body of this command.
  msnNotificationConnection_->sendUUN( self()->handle(), 8, "gtfo" );

  msnNotificationConnection_->closeConnection();
}



/**
 * @brief Log out from a specific endpoint.
 */
void MsnSession::logOutFrom( MPOPEndpoint *endpoint )
{
  debug() << "Logging out from endpoint: " << endpoint;

  QString handle = self()->handle() + ';' + endpoint->guid();

  // again, yes, it really does say this in the body.
  msnNotificationConnection_->sendUUN( handle, 4, "goawyplzthxbye" );
}




/**
 * Return the user's representation as an MsnContact.
 *
 * @return MsnContact*
 */
Self* MsnSession::self() const
{
  KMESS_ASSERT( contactList_ != 0 );

  return contactList_->listsManager()->self();
}



/**
 * @brief Send a raw command to the notification server.
 *
 * Advanced function. See the warning at isNotificationConnected() before you
 * use this method.
 *
 * @see isNotificationConnected()
 * @see sendRawMimeMessage()
 * @param command The command to send
 * @param arguments The command arguments
 * @param payload Optionally, the payload data to send with the request.
 */
void MsnSession::sendRawCommand( QString command, QString arguments, QByteArray payload ) const
{
#ifdef __GNUC__
#warning TODO: Sending raw commands
#endif
  if( payload.isEmpty() )
  {
    //msnNotificationConnection_->sendCommand( command, arguments );
  }
  else
  {
    //msnNotificationConnection_->sendPayloadMessage( command, arguments, payload );
  }
}



/**
 * @brief Send a raw MIME message to the notification server.
 *
 * Advanced function. See the warning at isNotificationConnected() before
 * you use this function.
 *
 * @see isNotificationConnected()
 * @see sendRawCommand()
 * @param ackType the acknowledgement type - 0 for none, 1 for NAK on failure, 2 for NAK on failure and ACK on success, anything else for "special case for P2P data messages".
 * @param message the message to send.
 */
void MsnSession::sendRawMimeMessage( int ackType, const QString &message ) const
{
#ifdef __GNUC__
#warning TODO: Sending raw MIME messages
#endif
#if 0
  AckType realAckType;
  if( ackType == 0 )
  {
    realAckType = ACK_NONE;
  }
  else if( ackType == 1 )
  {
    realAckType = ACK_NAK_ONLY;
  }
  else if( ackType == 2 )
  {
    realAckType = ACK_ALWAYS;
  }
  else
  {
    realAckType = ACK_ALWAYS_P2P;
  }

  msnNotificationConnection_->sendMimeMessage( realAckType, MimeMessage( message) );
#endif
}



/**
 * @brief Change a single session setting.
 *
 * Do not use if you need to change more than one settings.
 * This is just a wrapper around setSessionSettings(), which also emits the required update signals.
 *
 * @param name  Name of the setting to alter
 * @param value New value of the setting
 */
void MsnSession::setSessionSetting( const QString &name, const QVariant &value )
{
  SettingsList list;
  list[ name ] = value;

  setSessionSettings( list );
}



/**
 * Change a list of session settings.
 *
 * This method allows to alter a list of session settings.
 * Only use setSessionSetting() when it is necessary to change a single setting.
 * If there are more ONLY use setSessionSettings(), or you'll send multiple times
 * the MsnSession update signals.
 *
 * @param changes List with the name->value settings to alter
 */
void MsnSession::setSessionSettings( const SettingsList &changes )
{
  bool emitNewEmailSignal = false;

  SettingsListIterator it( changes );
  while( it.hasNext() )
  {
    it.next();

    const QString  &name  = it.key();
    const QVariant &value = it.value();

    ensureSessionSettingExists( name.toAscii() );

    // The setting didn't change value, skip it
    // Not using the [] operator because it creates an empty item if the key is not found
    if( sessionSettings_.value( name ) == value )
    {
#ifdef LIBKMESS_MSN_DEBUG_SETTINGS
      debug() << "Setting" << name << "already has value" << value;
#endif
      continue;
    }

#ifdef LIBKMESS_MSN_DEBUG_SETTINGS
    debug() << "Changing session setting" << name << "to" << value;
#endif

    sessionSettings_[ name ] = value;

    // Manage some settings which require further actions, like
    // calling methods or emitting signals

    if( name == "EmailCount" )
    {
      emitNewEmailSignal = true;
    }
  }

  // Send the signals for the updated settings
  if( emitNewEmailSignal ) emit changedNoEmails();
}



void MsnSession::mns_OnLoggedIn()
{
/*
  QHash<QString, QVariant> settings;
  settings.insert("localUserAddress", sessionSettingString("AccountHandle"));
  settings.insert("localEndpointId", sessionSettingString("MachineGUID"));
  Platform::instance()->startup(settings);
*/
}



/**
 * Emit loggedOut, but then wipe MsnContact/MsnChat instances and start fresh.
 *
 * Also shut down the Transport platform.
 */
void MsnSession::mns_OnLoggedOut( KMess::DisconnectReason reason )
{
  // update status appropriately now that we're offline.
  self()->setStatus( OfflineStatus );

  // give the user one last chance.
  emit loggedOut( reason );

  // reset ourselves.
  init(); 

//  Platform::instance()->shutdown();
}


