/***************************************************************************
                  self.cpp - Our own representation
                             -------------------
    begin                : Sun 08 Aug 2010
    copyright            : (C) 2010 by Adam Goossens
    email                : fontknocker@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "debug/libkmessdebug.h"

#include <KMess/Self>
#include <KMess/NetworkGlobals>
#include <KMess/ContactProfile>
#include <KMess/MPOPEndpoint>
#include <KMess/Utils>

#include <QFile>

#include "connections/msnnotificationconnection.h"

using namespace KMess;

/**
 * Construct a new instance of Self.
 * @internal
 */
Self::Self()
: MsnContact( QString() )
, localEndpoint_( 0 )
, msnObject_( 0 )
{
  localEndpoint_ = new MPOPEndpoint( this, QUuid( Utils::generateGUID() ), "libkmess-msn" );
}



Self::~Self()
{
  delete localEndpoint_;
}



/**
 * Change the currently displayed media.
 * 
 * @see MsnContact::MediaData
 * @param mediaData A MediaData structure describing the media.
 */
void Self::changeMedia( MediaData newMedia )
{
  setMedia( newMedia );
  ns_->changeCurrentMedia();
}



/**
 * Change our status.
 * 
 * You cannot change your status of Offline or Online.
 * @param status The new status to display.
 */
void Self::changeStatus( MsnStatus status )
{
  if ( status == OfflineStatus )
  {
    warning() << "Cannot change status to Offline. Ignoring.";
    return;
  }
  
  ns_->changeStatus( status );
}



/**
 * Returns true if roaming is enabled.
 * 
 * Roaming means you use the same display picture and personal message wherever
 * you sign in.
 * @see setRoaming
 */
bool Self::roamingEnabled() const
{
  QString val = profile()->annotation( "MSN.IM.RoamLiveProperties" );
  return val == "1";
}



/**
 * Returns the URL to the display picture stored on the roaming server.
 * 
 * This value may be a NULL QString() if roaming is disabled.
 */
QString Self::roamingDisplayPictureUrl() const
{
  return roamingDPUrl_;
}



/**
 * Returns the current notification privacy setting.
 * 
 * This determines how MSN reacts when a new contact adds you to their list.
 * @see setNotifyPrivacy
 */
NotifyPrivacy Self::notifyPrivacy() const
{
  QString val = profile()->annotation( "MSN.IM.GTC" );
  if ( val == "1" )
  {
    return PromptOnAdd;
  }
  else if ( val == "2" ) 
  {
    return AutomaticAdd;
  }
  else 
  {
    return NotifyUnknown;
  }
}



/**
 * Return the current message privacy setting.
 * 
 * This determines what happens when people not on your contact list message you.
 * @see setMessagePrivacy
 */
MessagePrivacy Self::messagePrivacy() const
{
  QString val = profile()->annotation( "MSN.IM.BLP" );
  if ( val == "1" )
  {
    return AllExceptBlocked;
  }
  else if ( val == "2" ) 
  {
    return AllowedOnly;
  }
  else 
  {
    return PrivacyUnknown;
  }
}



/**
 * Return the MPOPEndpoint that represents the user on this computer.
 */
MPOPEndpoint *Self::localEndpoint() const
{
  return localEndpoint_;
}



/**
 * Return the current logon mode. If this is LogonMultiple, you can logon at 
 * multiple Points of Presence simultaneously.
 * 
 * @see setLogonMode
 */
LogonMode Self::logonMode() const
{
  QString val = profile()->annotation( "MSN.IM.MPOP" );
  if ( val == "1" )
  {
    return LogonMultiple;
  }
  else
  {
    return LogonSingle;
  }
}



/**
 * True if your passport account has been verified; false otherwise.
 */
bool Self::passportVerified() const
{
  return passportVerified_;
}



/**
 * Change your display name.
 * @param name The new display name
 */
void Self::setDisplayName( const QString &name )
{
  profile()->setDisplayName( name );
  ns_->changeFriendlyName( name );
}



/**
 * Change your display picture.
 * @param path Path to the image on the filesystem.
 */
void Self::setDisplayPicture( const QString &path )
{
  displayPicturePath_ = path;
  updateMsnObject();
}



/**
 * Change your personal message.
 * @see setMedia
 */
void Self::setPersonalMessage( const QString &message )
{
  if ( personalMessage() != message )
  {
    _setPersonalMessage( message );
    ns_->changePersonalMessage();
  }
}



/**
 * Enable or disable display picture and personal message roaming.
 */
void Self::setRoaming( bool enabled )
{
  profile()->setAnnotation( "MSN.IM.RoamLiveProperties", enabled ? "1" : "0" );
}



/**
 * Set the notification privacy mode.
 * 
 * If PromptOnAdd, MSN will ask us what to do with the new contact. If
 * AutomaticAdd contacts will automatically be added to your contact list.
 */
void Self::setNotifyPrivacy( NotifyPrivacy mode )
{
  profile()->setAnnotation( "MSN.IM.GTC", mode == PromptOnAdd ? "1" : "0" );
}



/**
 * Set the message privacy mode.
 * 
 * If AllowedOnly, only contacts on your Allow list can send you messages.
 */
void Self::setMessagePrivacy( MessagePrivacy mode )
{
  profile()->setAnnotation( "MSN.IM.BLP", mode == AllExceptBlocked ? "1" : "0" );
}



/**
 * Set the MPOP logon mode. If LogonMultiple, can logon to multiple points 
 * simultaneously.
 */
void Self::setLogonMode( LogonMode mode )
{
  profile()->setAnnotation( "MSN.IM.MPOP", mode == LogonMultiple ? "1" : "0" );
}



/**
 * @internal
 */
void Self::_setPassportVerified( bool verified )
{
  passportVerified_ = verified;
}



/**
 * @internal
 */
void Self::_setNsConnection( KMessInternal::MsnNotificationConnection *ns )
{
  ns_ = ns;
}



/**
 * @internal
 */
void Self::_setRoamingDisplayPictureUrl( const QString &url )
{
  roamingDPUrl_ = url;
  emit roamingDisplayPictureChanged( url );
}



/**
 * Updates our personal MsnObject.
 */
void Self::updateMsnObject()
{
  debug() << "Changing MsnObject to reflect current displayPicture.";

  // Remove the previous msn object if there is one
  if( msnObject_ )
  {
    delete msnObject_;
    msnObject_ = 0;
  }

  if( ! displayPicturePath_.isEmpty() )
  {
    // Read the image data to a QByteArray
    QFile file( displayPicturePath_ );

    if( ! file.open( QIODevice::ReadOnly ) )
    {
      debug() << "Unable to open the new display picture:" << displayPicturePath_;
      displayPicturePath_.clear();
    }
    else
    {
      const QByteArray data( file.readAll() );
      file.close();

      msnObject_  = new MsnObject( handle(), "KMess.tmp", "", MsnObject::DISPLAYPIC, data );

      debug() << "Changed MsnObject to:" << msnObject_->objectString();
    }
  }
  else
  {
    debug() << "MsnObject removed.";
  }
  
  // force a reset of the MsnObject.
  if ( isOnline() )
  {
    ns_->changeStatus( status() );
  }
}
