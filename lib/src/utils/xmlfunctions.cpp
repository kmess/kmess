/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Diederik van der Boor <vdboor@codingdomain.com>           *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file xmlfunctions.cpp
 */

// Disable debug output for this file
#define QT_NO_DEBUG_OUTPUT

#include "xmlfunctions.h"
#include "debug/libkmessdebug.h"

#include <QStringList>
#include <QTextStream>


using namespace KMessInternal;


// Helper function, get a specific node
QDomNode XmlFunctions::getNode( const QDomNode &rootNode, const QString &path )
{
  KMESS_ASSERT( ! rootNode.isNull() );
  KMESS_ASSERT( ! path.isEmpty()    );

  const QStringList& pathItems( path.split( '/', QString::SkipEmptyParts ) );
  QDomNode           childNode( rootNode.namedItem( pathItems[0] ) );  // can be a null node

  int i = 1;
  while( i < pathItems.count() )
  {
    if( childNode.isNull() )
    {
      break;
    }

    childNode = childNode.namedItem( pathItems[ i ] );
    i++;  // not using for loop so i is always correct for debug() below.
  }

  QString message( "reading node \"" + rootNode.nodeName() + "/" + pathItems.join("/") + "\": "
                   + ( ! childNode.isNull() ? "found" : "not found: " + pathItems[ i - 1 ] ) );
  debug() << message.toLatin1().data();

  return childNode;
}



// Helper function, get the attribute text of a node
QString XmlFunctions::getNodeAttribute( const QDomNode &node, const QString &attribute )
{
  KMESS_ASSERT( ! node.isNull()       );
  KMESS_ASSERT( ! attribute.isEmpty() );

  // Writing this is not funny
  return node.attributes().namedItem( attribute ).toAttr().value();
// node.toElement().attribute( attribute );  does not work for const nodes.
}



// Helper function, get a specific child node
QDomNode XmlFunctions::getNodeChildByKey( const QDomNodeList &childNodes, const QString &keyTagName, const QString &keyValue )
{
  KMESS_ASSERT( ! keyTagName .isEmpty() );
  KMESS_ASSERT( ! keyValue   .isEmpty() );

  for( int i = 0; i < childNodes.count(); i++ )
  {
//    debug() << "node " << childNodes.item(i).nodeName() << "/" << keyTagName
//              << "="     << childNodes.item(i).namedItem(keyTagName).toElement().text() << " == " << keyValue << "?" << endl;

      // If the node has an childname with a certain value... e.g. <childNodes> <item><name>value</name></item> .. </childNodes>
    if( childNodes.item( i ).namedItem( keyTagName ).toElement().text() == keyValue)
    {
        // Return the node
      return childNodes.item( i );
    }
  }

  // Return a null node (is there a better way?)
  return childNodes.item( childNodes.count() );
}



// Helper function, get the text value of a node
QString XmlFunctions::getNodeValue( const QDomNode &rootNode, const QString &path )
{
  KMESS_ASSERT( ! rootNode.isNull() );
  KMESS_ASSERT( ! path.isEmpty()    );

  // Added code to avoid more assertion errors, and trace the cause.
  if( rootNode.isNull() )
  {
    warning() << "Attempted to request '" << path << "' on null root node.";
    return QString();
  }

  // Because writing node.namedItem("childItem").namedItem("child2").toElement().text() is not funny.
  //return getNode( rootNode, path ).firstChild().toText().data();
  return getNode( rootNode, path ).toElement().text();
}



// Helper function, get the source XML of a node.
QString XmlFunctions::getSource( const QDomNode &node, int indent )
{
  QString source;
  QTextStream textStream( &source, QIODevice::WriteOnly );
  node.save( textStream, indent );
  return source;
}

