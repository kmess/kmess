/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Diederik van der Boor <vdboor@codingdomain.com>           *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file xmlfunctions.h
 */

#ifndef XMLFUNCTIONS_H
#define XMLFUNCTIONS_H

#include <QDomNode>


// Forward declarations
class QDomNodeList;


namespace KMessInternal
{

/**
 * @brief Helper functions to make the use of QDom easier.
 *
 * @author Diederik van der Boor
 * @ingroup Utils
 */
class XmlFunctions
{
  public:
    // Helper function, get a specific node
    static QDomNode             getNode(const QDomNode &rootNode, const QString &path);
    // Helper function, get the attribute text of a node
    static QString              getNodeAttribute(const QDomNode &node, const QString &attribute);
    // Helper function, get a specific child node
    static QDomNode             getNodeChildByKey(const QDomNodeList &childNodes,
                                                  const QString &keyTagName, const QString &keyValue);
    // Helper function, get the text value of a node
    static QString              getNodeValue(const QDomNode &rootNode, const QString &path);
    // Helper function, get the source XML of a node.
    static QString              getSource( const QDomNode &node, int indent = 0 );
};

}; // end of namespace KMess

#endif
