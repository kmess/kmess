/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Antonio Nastasi <sifcenter@gmail.com>                     *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file msnalgorithms.cpp
 */

#include "debug/libkmessdebug.h"

#include <KMess/MsnAlgorithms>
#include <KMess/NetworkGlobals>

#include <QByteArray>
#include <QCryptographicHash>
#include <QtEndian>
#include <QDateTime>
#include <QUrl>

#include <gcrypt.h>


using namespace KMess;



/**
 * @brief Convert an html format (#RRGGBB) color to an msn format (BBGGRR)
 *        color
 */
const QString MsnAlgorithms::convertHtmlColorToMsnColor( const QString &color )
{
  KMESS_ASSERT( color.length() == 7 );

  // Get the color components
  const QString& red  ( color.mid(1, 2) );
  const QString& green( color.mid(3, 2) );
  const QString& blue ( color.mid(5, 2) );

  // Reassemble the color
  if ( blue != "00" )
  {
    return blue + green + red;
  }
  else if ( green != "00" )
  {
    return green + red;
  }
  else if ( red != "00" )
  {
    return red;
  }
  else
  {
    return "0";
  }
}



/**
 * @brief Convert and msn format color (BBGGRR) to an html format (#RRGGBB)
 *        color
 */
const QString MsnAlgorithms::convertMsnColorToHtmlColor( const QString &color )
{
  // If the color isn't present, use black
  if( color == "0" )
  {
    return "#000000";
  }

  QString realColor;

  // Remove any character apart from the last six (stripping any initial '#' character)
  if( color.length() > 6 )
  {
    realColor = color.right( 6 );
  }
  // Fill the color out to six characters if needed
  else
  {
    realColor = color.rightJustified( 6, '0' );
  }

  Q_ASSERT( realColor.length() == 6 );

  // Get the color components
  const QString& blue ( realColor.mid( 0, 2 ) );
  const QString& green( realColor.mid( 2, 2 ) );
  const QString& red  ( realColor.mid( 4, 2 ) );

  // Reassemble the components
  return '#' + red + green + blue;
}



/**
 * @brief Compute the authentication blob
 *
 * Used to compute the value of string to send to server for SSO.
 *
 * @param  result  The computed output value
 * @param  nonce   The nonce received from last USR commands and the proof key for authentication
 * @param  token   The token that used to compute the value (in base64)
 */
int KMess::MsnAlgorithms::createAuthBlob( QByteArray &result,
                                          const QByteArray &nonce,
                                          const QByteArray &token )
{
  // Initialize the GCrypt library if it wasn't done yet
  if( !gcry_control( GCRYCTL_INITIALIZATION_FINISHED_P ) )
  {
    gcry_check_version( 0 );

    // We don't need secure memory for this, I guess... Passwords and
    // everything have already been sent.
    gcry_control( GCRYCTL_DISABLE_SECMEM, 0 );
    gcry_control( GCRYCTL_INITIALIZATION_FINISHED, 0 );

    if( !gcry_control( GCRYCTL_INITIALIZATION_FINISHED_P ) )
    {
      warning() << "Failed to initialize GCrypt.";
      return ErrorComputeHashFailed;
    }
  }

  // Create the IV key for triple des encryption
  const unsigned short ivLength = 8;
  char *rawIv = new char[ ivLength ];
  gcry_create_nonce( rawIv, ivLength );
  QByteArray iv( rawIv, ivLength );
  delete [] rawIv;

  // Create the keys and hashes needed
  QByteArray key1, key2, key3;
  QByteArray hash, des3hash;

  // Compute the value to send with USR SSO command
  QString magic1( "WS-SecureConversationSESSION KEY HASH"       );
  QString magic2( "WS-SecureConversationSESSION KEY ENCRYPTION" );

  key1 = QByteArray::fromBase64( token );

  key2 = deriveKey( key1, magic1.toLatin1() );
  if( key2.isEmpty() )
  {
    return ErrorComputeHashFailed;
  }

  key3 = deriveKey( key1, magic2.toLatin1() );
  if( key3.isEmpty() )
  {
    return ErrorComputeHashFailed;
  }

  hash = createHMACSha1( key2, nonce );
  if( hash.isEmpty() )
  {
    return ErrorComputeHashFailed;
  }

  // Compute the 3des encryption
  // The windows api always pads using \x08 so we pad 8 bytes.
  des3hash = createTripleDes ( key3, nonce + QByteArray( 8, '\x08' ), iv );
  if( des3hash.isEmpty() )
  {
    return ErrorComputeHashFailed;
  }

  // Create the required 128 byte padded struct
  MSNPMSNG blob;
  blob.headerSize   = qToLittleEndian( 28 );
  blob.cryptMode    = qToLittleEndian( 1 ); // CRYPT_MODE_CBC
  blob.cipherType   = qToLittleEndian( 0x6603 ); // Triple DES
  blob.hashType     = qToLittleEndian( 0x8004 ); // SHA-1
  blob.ivLength     = qToLittleEndian( iv.size() );
  blob.hashLength   = qToLittleEndian( 20 ); // Length of the HMAC hash
  blob.cipherLength = qToLittleEndian( 72 );

  // Copy the IV Bytes, the hash and chiper value in blob struct
  memcpy( (char*) blob.ivBytes, iv.constData(), iv.size() );
  memcpy( (char*) blob.hashBytes, hash.constData(), hash.size() );
  memcpy( (char*) blob.cipherBytes, des3hash.constData(), des3hash.size() );

  // Create the byte arrays
  QByteArray blobs( (char*) &blob, 128 );
  result = blobs;

  return 0;
}



/**
 * @brief Compute a value to obtain the directly access to hotmail site.
 *
 * @param  passportToken  Passport Token
 * @param  proofToken     Passport proof Token
 * @param  folder         Folder to access
 */
const QString KMess::MsnAlgorithms::createHotmailToken( const QString &passportToken,
                                                  const QString &proofToken,
                                                  const QString &folder )
{
  // Create the nonce
  QByteArray nonce;
  for( int i = 0; i < 24; i++ )
  {
    nonce += (char)( ( rand() % 74 ) + 48 );
  }

  // Create the percent encoded string
  const QString& encodedString( QUrl::toPercentEncoding(
      "<EncryptedData xmlns=\"http://www.w3.org/2001/04/xmlenc#\""
      "Id=\"BinaryDAToken0\" Type=\"http://www.w3.org/2001/04/xmlenc#Element\">"
      " <EncryptionMethodAlgorithm=\"http://www.w3.org/2001/04/xmlenc#tripledes-cbc\"/>"
      " <ds:KeyInfo xmlns:ds=\"http://www.w3.org/2000/09/xmldsig#\">"
      "   <ds:KeyName>http://Passport.NET/STS</ds:KeyName>"
      " </ds:KeyInfo>"
      " <CipherData>"
      "   <CipherValue>" + passportToken + "</CipherValue>"
      " </CipherData>"
      "</EncryptedData>"
      , "", ".-" ) );

  // Create the token
  QString token( "ct="   + QString::number( QDateTime::currentDateTime().toTime_t(), 10 ) +
                 "&rru=" + QUrl::toPercentEncoding( folder ) +
                 "&sru=" + QUrl::toPercentEncoding( folder ) +
                 "&ru="  + QUrl::toPercentEncoding( folder ) +
                 "&bver=4&svc=mail&js=yes&id=2&pl=%3Fid%3D2&da="
                 + encodedString +
                 "&nonce=" + QUrl::toPercentEncoding( nonce.toBase64() ) );

  // Compute the keys with HMAC-Sha1 algorithm
  const QByteArray& key1( QByteArray::fromBase64( proofToken.toLatin1() ) );

  const QByteArray& magic( "WS-SecureConversation" + nonce );

  const QByteArray& key2( deriveKey( key1, magic ) );

  const QByteArray& hash( createHMACSha1( key2, token.toLatin1() ) );

  token += "&hash=" + QUrl::toPercentEncoding( hash.toBase64() );

  return token;
}




/**
 * @brief Compute a triple des encryption.
 *
 * Used to compute the value of string to send to server for SSO.
 *
 * @param  key     The key for compute the encryption
 * @param  secret  The secret data to encryption
 * @param  iv      The initial vector for cbc mode
 */
QByteArray KMess::MsnAlgorithms::createTripleDes( const QByteArray key, const QByteArray secret, const QByteArray &iv )
{
  // Initialize a triple-DES CBC encryption
  gcry_cipher_hd_t handle;
  gcry_cipher_open( &handle, GCRY_CIPHER_3DES, GCRY_CIPHER_MODE_CBC, 0 );

  // Set key and IV
  gcry_cipher_setkey( handle, key.constData(), key.size() );
  gcry_cipher_setiv( handle, iv.constData(), iv.size() );

  // Encrypt the data. The out buffer has to have the same size as the in buffer.
  char *encrypted = new char[ secret.size() ];
  gcry_cipher_encrypt( handle, (void*) encrypted, secret.size(), secret.constData(), secret.size() );

  // Clean up the cipher handle
  gcry_cipher_close( handle );

  QByteArray encryptedByteArray( encrypted, secret.size() );
  delete [] encrypted;
  return encryptedByteArray;
}



// Create a HMACSha1 hash
const QByteArray KMess::MsnAlgorithms::createHMACSha1 ( const QByteArray &keyForHash , const QByteArray &secret )
{
  // The algorithm is definited into RFC 2104
  int blocksize = 64;
  QByteArray key( keyForHash );
  QByteArray opad( blocksize, 0x5c );
  QByteArray ipad( blocksize, 0x36 );

  // If key size is too larg, compute the hash
  if( key.size() > blocksize )
  {
    key = QCryptographicHash::hash( key, QCryptographicHash::Sha1 );
  }

  // If too small, pad with 0x00
  if( key.size() < blocksize )
  {
    key += QByteArray( blocksize - key.size() , 0x00 );
  }

  // Compute the XOR operations
  for(int i=0; i < key.size() - 1; i++ )
  {
    ipad[i] = (char) ( ipad[i] ^ key[i] );
    opad[i] = (char) ( opad[i] ^ key[i] );
  }

  // Append the data to ipad
  ipad += secret;

  // Compute result: hash sha1 of ipad and append the data to opad
  opad += QCryptographicHash::hash( ipad, QCryptographicHash::Sha1 );;

  // Return array contains the result of HMACSha1
  return QCryptographicHash::hash( opad, QCryptographicHash::Sha1 );
}



// Return a derived key with HMACSha1 algorithm
const QByteArray KMess::MsnAlgorithms::deriveKey ( const QByteArray &keyToDerive, const QByteArray &magic )
{
  // create the four hashes needed for the implementation.
  QByteArray hash1, hash2, hash3, hash4, temp;
  QByteArray key(keyToDerive);

  // append the magic string.
  temp.append ( magic );

  // create the HMAC Sha1 hash and assign it.
  hash1 = createHMACSha1 ( key, temp );
  temp.clear();

  // append the first hash with the magic string.
  temp.append ( hash1 + magic );

  // create the HMAC Sha1 hash and assign it.
  hash2 = createHMACSha1 ( key, temp );
  temp.clear();

  // append the first hash.
  temp.append ( hash1 );

  // create the HMAC Sha1 hash and assign it.
  hash3 = createHMACSha1 ( key, temp );
  temp.clear();

  // assign the third hash with the magic string.
  temp.append ( hash3 + magic );
  // create the HMAC Sha1 hash and assign it.
  hash4 = createHMACSha1 ( key, temp );
  temp.clear();

  // return the second hash with the first four bytes of the fourth hash.
  return hash2 + hash4.left ( 4 );
}



void MsnAlgorithms::insertBytes(QByteArray &buffer, const unsigned int value, const int offset)
{
  // Also based on Kopete code. I started with a nice struct,
  // but not all c++ compilers use the same data size for the fields:
  /*
  * "The only restrictions that the language spec places are that
  *  (1) a char is one byte,
  *  (2) char <= short int <= int <= long int <= long long int.
  *  The exact size is completely compiler dependent."
  *
   * gcc on i686-pc-linux-gnu gives these results:
  * - sizeof(int)           = 4
  * - sizeof(long int)      = 4
  * - sizeof(long long int) = 8
  * this beats me.
  *
  * In other words, I'll use bitwise shifts to set bytes manually.
  */

  buffer[offset + 0] = (char) ( value        & 0xFF);
  buffer[offset + 1] = (char) ((value >>  8) & 0xFF);
  buffer[offset + 2] = (char) ((value >> 16) & 0xFF);
  buffer[offset + 3] = (char) ((value >> 24) & 0xFF);
  // I think it's obvious we're copying every byte individually here..
  // However, note the bytes are placed in network order. (big endian)
}



void MsnAlgorithms::insertNonce(QByteArray &buffer, const QString &nonce, const int offset)
{
  // Remove the separators
  // The QString(..) call makes a copy because QString::remove isn't const.
  QString fixedNonce( QString(nonce).remove('-').remove('{').remove('}') );

  KMESS_ASSERT( fixedNonce.length() == 32      );  // 32 hex codes will be saved as 16 bytes.
  KMESS_ASSERT( buffer.size() >= (offset + 16) );  // buffer needs to have 16 bytes of space left.

  // fields 7-9 (ack-msgid/uniqueid/ack-size) will be overwritten with the nonce.
  // When the nonce was {4299E384-8248-4AF6-90E4-5B26A040AC34} as string
  // it will be sent as bytes like: 84E39942-4882-F64A-90E4-5B26A040AC34
  // The order of the first 3 fields is reversed.
  const int noncePos[] = { 3,2,1,0            // Field 1 reversed
                         , 5,4                // Field 2 reversed
                         , 7,6                // Field 3 reversed
                         , 8,9                // field 4
                         , 10,11,12,13,14,15  // field 5
                         };
  for(int i = 0; i < 16; i++)
  {
    buffer[offset + i] = (char)( fixedNonce.mid(noncePos[i] * 2, 2).toUInt(0, 16) );
  }
}



// Copy sort integers into the binary header.
void MsnAlgorithms::insertUtf16String(QByteArray &buffer, const QString &value, int offset)
{
  const unsigned short *utf16Value = value.utf16();
  uint valueLength = value.length();
  for(uint i = 0; i < valueLength; ++i)
  {
    const short utf16Char = utf16Value[i];
    buffer[offset + 0] = (char) ( utf16Char        & 0xFF);
    buffer[offset + 1] = (char) ((utf16Char >>  8) & 0xFF);
    offset += 2;
  }
}



unsigned int MsnAlgorithms::extractBytes( const QByteArray &data, const int offset)
{
  KMESS_ASSERT( data.size() >= offset + 3 );

  // Convert the bytes from network order to a normal int.
  return (((unsigned char) data[offset + 0]      )
        | ((unsigned char) data[offset + 1] <<  8)
        | ((unsigned char) data[offset + 2] << 16)
        | ((unsigned char) data[offset + 3] << 24));
}



quint32 MsnAlgorithms::extractLongBytes(const QByteArray &data, const int offset)
{
  KMESS_ASSERT( data.size() >= offset + 3 );
  // Convert the bytes from network order to a long int.
  return (((unsigned char) data[offset + 0]      )
        | ((unsigned char) data[offset + 1] <<  8)
        | ((unsigned char) data[offset + 2] << 16)
        | ((unsigned char) data[offset + 3] << 24));

        // FIXME: gcc complains that the shift width is larger then the actual data type supports..
        //        as long as the msn servers don't send huge messages, we don't have much to worry..
        //        otherwise cast the data to qint64 first.
//        | ((unsigned char) data[offset + 4] << 32)
//        | ((unsigned char) data[offset + 5] << 40)
//        | ((unsigned char) data[offset + 6] << 48)
//        | ((unsigned char) data[offset + 7] << 56));
}



QString MsnAlgorithms::extractUtf16String( const QByteArray &data, const int offset, int size )
{
  KMESS_ASSERT( data.size() >= offset + size );

  // Avoid copying the null char
  if( data[ offset + size - 1 ] == '\0'
  &&  data[ offset + size - 2 ] == '\0' )
  {
#ifdef KMESSDEBUG_P2PMESSAGE
    debug() << "avoid copying null character to utf16 string";
#endif
    size--;
  }

  return QString::fromUtf16( reinterpret_cast<const ushort *>( data.constData() + offset ), size / 2 );
}

