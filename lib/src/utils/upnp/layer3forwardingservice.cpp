/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Diederik van der Boor <vdboor@codingdomain.com>           *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file layer3forwardingservice.cpp
 */

#include "layer3forwardingservice.h"
#include "debug/libkmessdebug.h"

#include <QStringList>


#ifdef KMESSDEBUG_UPNP
  #define KMESSDEBUG_UPNP_GENERAL
#endif



namespace UPnP
{

// The constructor
Layer3ForwardingService::Layer3ForwardingService(const ServiceParameters &params)
  : Service(params)
{

}


// The destructor
Layer3ForwardingService::~Layer3ForwardingService()
{

}



// Get the device UDN of the default connection service
QString Layer3ForwardingService::getConnectionDeviceUdn() const
{
  return connectionDeviceUdn_;
}



// Get the service ID of the default connection service
QString Layer3ForwardingService::getConnectionServiceId() const
{
  return connectionServiceId_;
}



// The control point received a response to callAction()
void Layer3ForwardingService::gotActionResponse(const QString &responseType, const QMap<QString,QString> &resultValues)
{
  debug() << "Got action response type='" << responseType << "'." << endl;

  // Example:
  //
  //   <m:GetDefaultConnectionServiceResponse xmlns:m="urn:schemas-upnp-org:service:Layer3Forwarding:1" >
  //     <NewDefaultConnectionService>
  //       (there is no white space between these parts!)
  //       uuid:UPnP-SpeedTouch510-1_00-90-D0-8E-A1-6F_WCDpppoa:WANConnectionDevice:1,
  //       urn:upnp-org:serviceId:wanpppc:pppoa
  //     </NewDefaultConnectionService>
  //   </m:GetDefaultConnectionServiceResponse>


  if(responseType == "GetDefaultConnectionServiceResponse" )
  {
    QString     newService   ( resultValues["NewDefaultConnectionService"] );
    QStringList serviceItems = newService.split( ',', QString::SkipEmptyParts );
    QString     uuid;
    QString     urn;

    // Extract the uuid and urn from the NewDefaultConnectionService value
    for( int i = 0; i < serviceItems.count(); i++ )
    {
      QString type( serviceItems[i].section( ':', 0, 0 ) );
      if(type == "uuid")
      {
        // format: uuid:<id>:<class>:<ver>
        connectionDeviceUdn_ = serviceItems[i].section(':', 0, 1);
      }
      else if(type == "urn")
      {
        connectionServiceId_ = serviceItems[i];
      }
      else
      {
        warning() << "Unexpected section '" << type << "' encountered in NewDefaultConnectionService value." << endl;
      }
    }

    debug() << " udn='"       << connectionDeviceUdn_ << "'"
             << " serviceid='" << connectionServiceId_ << "'." << endl;
  }
  else
  {
    warning() << "Unexpected response type '" << responseType << "' encountered." << endl;
  }
}



// Query the Layer3Forwarding service for the default connection service
void Layer3ForwardingService::queryDefaultConnectionService()
{
  callAction("GetDefaultConnectionService");
}


}
