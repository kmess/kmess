/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Diederik van der Boor <vdboor@codingdomain.com>           *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file igdcontrolpoint.cpp
 */

#include "igdcontrolpoint.h"
#include "debug/libkmessdebug.h"

#include "rootservice.h"
#include "layer3forwardingservice.h"
#include "wanconnectionservice.h"


#ifdef KMESSDEBUG_UPNP
  #define KMESSDEBUG_UPNP_GENERAL
#endif



namespace UPnP
{

#define InternetGatewayDeviceType "urn:schemas-upnp-org:device:InternetGatewayDevice:1"
#define Layer3ForwardingType      "urn:schemas-upnp-org:service:Layer3Forwarding:1"


// The constructor
IgdControlPoint::IgdControlPoint(const QString &hostname, quint16 port, const QString &rootUrl)
  : QObject()
  , forwardingService_(0)
  , gatewayAvailable_(false)
  , igdPort_(0)
  , rootService_(0)
  , wanConnectionService_(0)
{
  debug() << "Created control point url='" << hostname << ":" << port << "/" << rootUrl << "'." << endl;
  debug() << "querying services...";

  // Store device url
  igdHostname_ = hostname;
  igdPort_     = port;

  // Query the device for it's services
  rootService_ = new RootService(igdHostname_, igdPort_, rootUrl);
  connect(rootService_, SIGNAL(queryFinished(bool)), this, SLOT(slotDeviceQueried(bool)));
}



// The destructor
IgdControlPoint::~IgdControlPoint()
{
  delete rootService_;
  delete forwardingService_;
  delete wanConnectionService_;

  debug() << "DESTROYED. [host=" << igdHostname_ << ", port=" << igdPort_ << "]";
}



// Return the external IP address
QString IgdControlPoint::getExternalIpAddress() const
{
  // Do not expose  wanConnectionService_;
  if(wanConnectionService_ != 0)
  {
    return wanConnectionService_->getExternalIpAddress();
  }
  else
  {
    return QString();
  }
}



// Initialize the control point
void IgdControlPoint::initialize()
{
  KMESS_ASSERT( rootService_ != 0 );
  KMESS_ASSERT( rootService_->getDeviceType().isNull() );

  rootService_->queryDevice();
}



// Return true if a controlable gateway is available
bool IgdControlPoint::isGatewayAvailable()
{
  return gatewayAvailable_;
}



// The IGD was queried for it's services
void IgdControlPoint::slotDeviceQueried(bool error)
{
  KMESS_ASSERT( rootService_          != 0 );
  KMESS_ASSERT( forwardingService_    == 0 );

  if(! error)
  {
    KMESS_ASSERT( rootService_->getDeviceType() == InternetGatewayDeviceType );

    // Get the Layer3ForwardingService from the retrieved service list
    ServiceParameters params = rootService_->getServiceByType(Layer3ForwardingType);

    if(! params.controlUrl.isNull())
    {
      debug() << "Services found, "
                << "querying service '" << params.serviceId << "' for port mapping service..." << endl;

      // Call the service
      forwardingService_ = new Layer3ForwardingService(params);
      connect(forwardingService_, SIGNAL(queryFinished(bool)), this, SLOT(slotWanConnectionFound(bool)));
      forwardingService_->queryDefaultConnectionService();
    }
    else
    {
      // TODO: error
    }
  }
}



// A WAN connection service was found
void IgdControlPoint::slotWanConnectionFound(bool error)
{
  KMESS_ASSERT( rootService_          != 0 );
  KMESS_ASSERT( forwardingService_    != 0 );
  KMESS_ASSERT( wanConnectionService_ == 0 );

  if(! error)
  {
    // Get the retrieved service description
    QString deviceUrn( forwardingService_->getConnectionDeviceUdn() );
    QString serviceId( forwardingService_->getConnectionServiceId() );
    ServiceParameters params = rootService_->getServiceById(serviceId, deviceUrn);

    if(! params.controlUrl.isNull())
    {
      debug() << "wan/ipconnection service found, "
                << "querying service '" << params.serviceId << "' for external ip address..." << endl;

      // Call the service
      wanConnectionService_ = new WanConnectionService(params);
      connect(wanConnectionService_, SIGNAL(queryFinished(bool)), this, SLOT(slotWanQueryFinished(bool)));
      wanConnectionService_->queryExternalIpAddress();
    }
  }

  // No longer need the forwarding service
  forwardingService_->deleteLater();
  forwardingService_ = 0;
}



// A WAN connection query was finished
void IgdControlPoint::slotWanQueryFinished(bool error)
{
  KMESS_ASSERT( rootService_          != 0 );
  KMESS_ASSERT( wanConnectionService_ != 0 );

  if(! error)
  {
    debug() << "UPnP Gateway Device found.";
    gatewayAvailable_ = true;
  }
  else
  {
    // Just started, the request for the external IP failed. This should succeed, abort portation
    warning() << "Requesting external IP address failed, leaving UPnP Gateway Device untouched.";
  }
}



}  // End of namespace


