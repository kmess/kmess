/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Diederik van der Boor <vdboor@codingdomain.com>           *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file wanconnectionservice.cpp
 */

#include "wanconnectionservice.h"
#include "debug/libkmessdebug.h"

#include <QList>


#ifdef KMESSDEBUG_UPNP
  #define KMESSDEBUG_UPNP_GENERAL
#endif



namespace UPnP
{


// The constructor
WanConnectionService::WanConnectionService(const ServiceParameters &params)
  : Service(params)
  , natEnabled_(false)
{

}


// The destructor
WanConnectionService::~WanConnectionService()
{

}



// Add a port mapping
void WanConnectionService::addPortMapping(const QString &protocol, const QString &remoteHost, int externalPort,
                                          const QString &internalClient, int internalPort, const QString &description,
                                          bool enabled, int leaseDuration)
{
  // TODO: this still needs to be tested
  QMap<QString,QString> arguments;
  arguments["NewProtocol"]               = protocol;
  arguments["NewRemoteHost"]             = remoteHost;
  arguments["NewExternalPort"]           = QString::number(externalPort);
  arguments["NewInternalClient"]         = internalClient;
  arguments["NewInternalPort"]           = QString::number(internalPort);
  arguments["NewPortMappingDescription"] = description;
  arguments["NewEnabled"]                = QString::number(enabled ? 1 : 0);
  arguments["NewLeaseDuration"]          = QString::number(leaseDuration);
  callAction("AddPortMapping", arguments);
}



// Delete a port mapping
void WanConnectionService::deletePortMapping(const QString &protocol, const QString &remoteHost, int externalPort)
{
  // TODO: this still needs to be tested
  QMap<QString,QString> arguments;
  arguments["NewProtocol"]       = protocol;
  arguments["NewRemoteHost"]     = remoteHost;
  arguments["NewExternalPort"]   = QString::number(externalPort);
  callAction("DeletePortMapping", arguments);
}



// Return the external IP address
QString WanConnectionService::getExternalIpAddress() const
{
  return externalIpAddress_;
}



// Return true if NAT is enabled
bool WanConnectionService::getNatEnabled() const
{
  return natEnabled_;
}



// Return the port mappings
const QList<PortMapping*>& WanConnectionService::getPortMappings() const
{
  return portMappings_;
}



// The control point received a response to callAction()
void WanConnectionService::gotActionResponse(const QString &responseType, const QMap<QString,QString> &resultValues)
{
  debug() << "Parsing action response:"
            << " type='" << responseType << "'." << endl;

  // Check the message type
  if(responseType == "GetExternalIPAddressResponse")
  {
    // Get the external IP address from the response
    externalIpAddress_ = resultValues["NewExternalIPAddress"];

    debug() << "externalIp='" << externalIpAddress_ << "'.";
  }
  else if(responseType == "GetNATRSIPStatusResponse")
  {
    // Get the nat status from the response
    natEnabled_ = (resultValues["NewNATEnabled"] == "1");

    debug() << "natEnabled=" << natEnabled_ << ".";
  }
  else if(responseType == "GetGenericPortMappingEntryResponse")
  {
    // Find a place to store the data
    PortMapping *map = new PortMapping;

    // Get the port mapping data from the response
    map->enabled        = (resultValues["NewEnabled"] == "1");
    map->externalPort   =  resultValues["NewExternalPort"].toInt();
    map->internalClient =  resultValues["NewInternalClient"];
    map->internalPort   =  resultValues["NewInternalPort"].toInt();
    map->leaseDuration  =  resultValues["NewLeaseDuration"].toInt();
    map->description    =  resultValues["NewPortMappingDescription"];
    map->protocol       =  resultValues["NewProtocol"];
    map->remoteHost     =  resultValues["NewRemoteHost"];

    // Register the mapping
    portMappings_.append(map);

    debug() << "Got mapping: " << map->protocol << " " << map->remoteHost << ":" << map->externalPort
             << " to " << map->internalClient << ":" << map->internalPort
             << "    max " << map->leaseDuration << "s '" << map->description << "' " << (map->enabled ? "enabled" : "disabled") << endl;
  }
  else
  {
    warning() << "Unexpected response type '" << responseType << "' encountered." << endl;
  }
}



// Query for the external IP address
void WanConnectionService::queryExternalIpAddress()
{
  callAction("GetExternalIPAddress");
}



// Query for the Nat status
void WanConnectionService::queryNatEnabled()
{
  callAction("GetNATRSIPStatus");
}



// Query for a port mapping entry
void WanConnectionService::queryPortMappingEntry(int index)
{
  QMap<QString,QString> arguments;
  arguments["NewPortMappingIndex"] = QString::number(index);
  callAction("GetGenericPortMappingEntry", arguments);
}



} // End of namespace
