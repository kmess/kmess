/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Diederik van der Boor <vdboor@codingdomain.com>           *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file ssdpconnection.h
 */

#ifndef UPNP_SSDPCONNECTION_H
#define UPNP_SSDPCONNECTION_H

#include <QObject>


class QUdpSocket;
class QSocketNotifier;



namespace UPnP
{


/**
 * The Simple Service Discovery Protocol allows UPnP clients
 * to discover UPnP devices on a network.
 * This is achieved by broadcasting a HTTP-like message over UDP.
 * Devices can respond with their location and root service name.
 * The RootService class uses this information to query the device for
 * it's meta information and service list.
 *
 * @author Diederik van der Boor
 * @ingroup NetworkUPnP
 */
class SsdpConnection : public QObject
{
  Q_OBJECT

  public:  // public methods
    // The constructor for action services
                         SsdpConnection();
    // The destructor
    virtual             ~SsdpConnection();

    // Send a broadcast to detect all devices
    void                 queryDevices( quint16 bindPort = 1500 );

  private slots:
    // Data was received by the socket
    void                 slotDataReceived();


  private:
    QUdpSocket          *socket_;

  signals:
    // Called when a query completed
    void                 deviceFound(const QString &hostname, int port, const QString &rootUrl);
};

}

#endif
