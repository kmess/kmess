/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Diederik van der Boor <vdboor@codingdomain.com>           *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file layer3forwardingservice.h
 */

#ifndef UPNP_LAYER3FORWARDINGSERVICE_H
#define UPNP_LAYER3FORWARDINGSERVICE_H

#include "service.h"



namespace UPnP {

/**
 * The Layer3Forwarding service is used to query a Internet Gateway Device (router in UPnP terms)
 * for it's WanConnection service. This can be an instance of the WanIPConnection or WanPPPConnection service,
 * which is implemented by the WanConnectionService class.
 * The WanIPConnection/WanPPPConnection service can be used to configure the external connection settings and port mappings of the router.
 * The Layer3Forwarding service itself is resolved by the RootService class.
 *
 * @author Diederik van der Boor
 * @ingroup NetworkUPnP
 */
class Layer3ForwardingService : public Service
{
  public:  // public methods

    // The constructor
                         Layer3ForwardingService(const ServiceParameters &params);
    // The destructor
    virtual             ~Layer3ForwardingService();

    // Get the device UDN of the default connection service
    QString              getConnectionDeviceUdn() const;
    // Get the service ID of the default connection service
    QString              getConnectionServiceId() const;

    // Query the Layer3Forwarding service for the default connection service
    void                 queryDefaultConnectionService();


  protected:  // protected methods

    // The control point received a response to callAction()
    virtual void         gotActionResponse(const QString &responseType, const QMap<QString,QString> &resultValues);


  private:  // private attributes

    // The device UDN of the default connection service
    QString              connectionServiceId_;
    // The service ID of the default connection service
    QString              connectionDeviceUdn_;
};

}

#endif
