/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Diederik van der Boor <vdboor@codingdomain.com>           *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file manager.cpp
 */

#include "manager.h"
#include "debug/libkmessdebug.h"

#include "igdcontrolpoint.h"
#include "ssdpconnection.h"

#include <QTimer>


#ifdef KMESSDEBUG_UPNP
  #define KMESSDEBUG_UPNP_GENERAL
#endif



namespace UPnP
{

// Set the static variable
Manager* Manager::instance_(0);


// The constructor
Manager::Manager()
  : activeIgdControlPoint_(0)
  , broadcastFailed_(false)
  , ssdpConnection_(0)
  , ssdpTimer_(0)
{

}



// The destructor
Manager::~Manager()
{
  delete ssdpTimer_;
  delete ssdpConnection_;
  instance_ = 0;  // Unregister the instance
}



// Initialize the manager, detect all devices
void Manager::initialize()
{
  KMESS_ASSERT( ssdpConnection_ == 0 );
  debug() << "Initiating a broadcast to detect UPnP devices...";


  // Create the SSDP object to detect devices
  ssdpConnection_ = new SsdpConnection();
  connect(ssdpConnection_, SIGNAL(     deviceFound(QString,int,QString) ) ,
          this,              SLOT( slotDeviceFound(QString,int,QString) ) );

  // Create a timer
  ssdpTimer_      = new QTimer(this);
  connect(ssdpTimer_, SIGNAL(timeout()), this, SLOT(slotBroadcastTimeout()));

  // Start a UPnP broadcast
  broadcastFailed_ = false;
  ssdpConnection_->queryDevices();
  ssdpTimer_->setSingleShot( true );
  ssdpTimer_->start( 2000 );
}



// Return the instance of the manager class
Manager * Manager::instance()
{
  // Create when it's required
  if(instance_ == 0)
  {
    instance_ = new Manager();
    instance_->initialize();
  }

  return instance_;
}



// Return the external IP address
QString Manager::getExternalIpAddress() const
{
  // Do not expose activeIgd_;
  return (activeIgdControlPoint_ != 0 ? activeIgdControlPoint_->getExternalIpAddress() : QString());
}



// Return true if a controlable gateway is available
bool Manager::isGatewayAvailable()
{
  return (activeIgdControlPoint_ != 0 &&
          activeIgdControlPoint_->isGatewayAvailable());
}



// The broadcast failed
void Manager::slotBroadcastTimeout()
{
  KMESS_ASSERT( ! broadcastFailed_ );
  debug() << "Timeout, no broadcast response received!";

  broadcastFailed_ = true;
}



// A device was discovered by the SSDP broadcast
void Manager::slotDeviceFound(const QString &hostname, quint16 port, const QString &rootUrl)
{
  debug() << "Device found, initializing IgdControlPoint to query it.";

  IgdControlPoint *controlPoint = new IgdControlPoint(hostname, port, rootUrl);
  igdControlPoints_.append(controlPoint);

  if(activeIgdControlPoint_ == 0)
  {
    activeIgdControlPoint_ = controlPoint;
    activeIgdControlPoint_->initialize();
  }
}



}  // end of namespace


