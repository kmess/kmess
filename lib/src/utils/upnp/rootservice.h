/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Diederik van der Boor <vdboor@codingdomain.com>           *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file rootservice.h
 */

#ifndef UPNP_ROOTSERVICE_H
#define UPNP_ROOTSERVICE_H

#include "service.h"



namespace UPnP
{

/**
 * The services of a device can be retrieved using the device root service.
 * The URL of the root service is returned by an SSDP broadcast.
 * The root service returns the meta information and list of services the device supports.
 *
 * @author Diederik van der Boor
 * @ingroup NetworkUPnP
 */
class RootService : public Service
{
  public:  // public methods

    // The constructor
                         RootService( const QString &hostname, quint16 port, const QString &rootUrl );
    // The destructor
    virtual             ~RootService();

    // Return the device type
    QString              getDeviceType() const;

    // Return a service from the cached root device entry
    ServiceParameters    getServiceById(const QString &serviceId) const;
    // Return a service from a cached embedded device entry
    ServiceParameters    getServiceById(const QString &serviceId, const QString &deviceUdn) const;
    // Return a service from the cached root device entry
    ServiceParameters    getServiceByType(const QString &serviceType) const;
    // Return a service from a cached embedded device entry
    ServiceParameters    getServiceByType(const QString &serviceType, const QString &deviceUdn) const;

    // Query the device for its service list
    void                 queryDevice();


  protected:  // Protected methods
    // The control point received a response to callInformationUrl()
    virtual void         gotInformationResponse(const QDomNode &response);


  private:  // Private methods
    // Recursively add all devices and embedded devices to the deviceServices_ map
    void                 addDeviceServices(const QDomNode &device);


  private:
    // The device type
    QString                     deviceType_;
    // A collection of all services provided by the device
    QMap<QString,QDomNodeList>  deviceServices_;
    // The hostname of the device
    QString                     hostname_;
    // The port of the device
    quint16                     port_;
    // The udn of the root device
    QString                     rootUdn_;
};

}

#endif
