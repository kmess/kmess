/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Valerio Pilo <valerio@kmess.org>                          *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file utils_internal.h
 */


#ifndef UTILS_INTERNAL_H
#define UTILS_INTERNAL_H

#include <KMess/NetworkGlobals>

// Forward declarations
class QDateTime;

namespace KMess
{
  class ClientCapabilities;
};


namespace KMessInternal
{

/**
 * @brief Utilities not exported in the library API.
 *
 * @author Valerio Pilo
 *
 * @ingroup Network/Utils
 */
class InternalUtils
{


  public: // Static public methods

    // Retrieve the handle and network of a contact from a string of the form "netid:handle"
    static bool getHandle( const QString &string, QString &handle, KMess::NetworkType &network );
    // Combine the handle and network of a contact into a correctly-formed string
    static QString makeHandle( const QString &handle, KMess::NetworkType network );
    // Decode MIME strings like =?iso...=...?=...
    static QString decodeRFC2047String( const QString &string );
    // Convert a media type into a string
    static QString mediaTypeToString( KMess::MediaType type );
    // Convert a status to a three-letter code (like BSY)
    static QString statusToCode( const KMess::MsnStatus status );
    // Convert a three-letter code to a status
    static KMess::MsnStatus statusFromCode( const QString &status );
    // Convert a string into a media type
    static KMess::MediaType stringToMediaType( const QString &str );
    // Parse an RFC 2822 date into an UTC date
    static QDateTime parseRfcDate( const QString &date );

  private: // Private static methods
    // Decode a Quoted-Printable string
    static QByteArray quotedPrintableDecode( const QByteArray& in );

};

}; // end of namespace KMess

#endif // UTILS_INTERNAL_H
