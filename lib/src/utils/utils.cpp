/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Antonio Nastasi <sifcenter@gmail.com>                     *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file utils.cpp
 */

#include "debug/libkmessdebug.h"
#include "utils/utils_internal.h"

#include <KMess/NetworkGlobals>
#include <KMess/Utils>

#include <QByteArray>
#include <QString>
#include <QUuid>
#include <QFile>
#include <QCryptographicHash>

#include <ctime>

using namespace KMess;

// Settings for debugging
#define NOTIFICATION_HIDE_P2P_SUPPORT 0

// Internal variables used by Utils
static ClientCapabilities clientCapabilities_;
static QString            clientName_;
static QString            clientVersion_;
static ChatLoggingMode    clientLogging_ = LoggingInactive;



/**
 * Extract a nonce from a QByteArray of data.
 * @todo documentation
 */
QString Utils::extractNonce(const QByteArray &data, const int offset)
{
  KMESS_ASSERT( data.size() >= offset + 15 );

  const int noncePos[] = { 3,2,1,0            // Field 1 reversed
                         , 5,4                // Field 2 reversed
                         , 7,6                // Field 3 reversed
                         , 8,9                // field 4
                         , 10,11,12,13,14,15  // field 5
                         };

  const char hexMap[] = "0123456789ABCDEF";
  QString hex;
  for(int i = 0; i < 16; i++)
  {
    if( i == 4 || i == 6 || i == 8 || i == 10 )
    {
      hex += '-';
    }
    int upper = (data[offset + noncePos[i]] & 0xf0) >> 4;
    int lower = (data[offset + noncePos[i]] & 0x0f);
    hex += hexMap[upper];
    hex += hexMap[lower];
  }

  return '{' + hex + '}';
}



// Return the hash of a file name using the SHA1 algorithm
QByteArray Utils::generateFileHash( const QString &fileName )
{
  // Read the file's contents into a byte array
  QFile file( fileName );
  if( ! file.open( QIODevice::ReadOnly ) )
  {
    return QByteArray();
  }

  QByteArray fileData( file.readAll() );

  file.close();

  // Retrieve and return the file's hash
  return QCryptographicHash::hash( fileData, QCryptographicHash::Sha1 );
}



/**
 * @brief Generate a random GUID.
 *
 * This is used in MSNP2P for the Call ID and Branch ID.
 *
 * @return A randomly generated GUID value.
 */
QString KMess::Utils::generateGUID()
{
  return QUuid::createUuid().toString().toUpper();
}



/**
 * @brief Generate an random number to use as ID.
 *
 * For use in MSNP2P, the value will not be below 4
 *
 * @return A random value between 4 and RAND_MAX.
 */
quint32 KMess::Utils::generateID()
{
  // seed the RNG properly.
  qsrand( time(NULL) );
  return ( qrand() + 100 );
}



/**
 * Return the client capabilities.
 *
 * @return ClientCapabilities
 */
const ClientCapabilities KMess::Utils::getClientCapabilities()
{
  return clientCapabilities_;
}



/**
 * Return the client chat logging mode.
 *
 * FIXME: Isn't chat logging mode to be set per-chat? ie if I want to disable chat logging
 * for a single chat, with this global method I can only tell I'm not logging anymore for
 * *all* new chats...
 *
 * @return ChatLoggingMode
 */
ChatLoggingMode KMess::Utils::getClientLogging()
{
  return clientLogging_;
}



/**
 * Return the client name.
 *
 * This string will be sent to other clients in chat metadata.
 *
 * @return QString
 */
const QString KMess::Utils::getClientName()
{
  return clientName_;
}



/**
 * Return the client version number.
 *
 * This string will be sent to other clients in chat metadata.
 *
 * @return QString
 */
const QString KMess::Utils::getClientVersion()
{
  return clientVersion_;
}



/**
 * Converts a string with HTML to one with escaped entities
 *
 * Only the main HTML control characters are escaped; the string is made suitable for
 * insertion within an HTML tag attribute.
 * Neither KDE nor Qt have escape/unescape methods this thorough, they only replace the <>&. Annoying.
 *
 * @param string  The string to modify
 * @return        A reference to the modified string
 */
QString &Utils::htmlEscape( QString &string )
{
  string.replace( ';', "&#59;" )
        .replace( '&', "&amp;" )
        .replace( "&amp;#59;", "&#59;" )
        .replace( '<', "&lt;"  )
        .replace( '>', "&gt;"  )
        .replace( '\'', "&#39;" )
        .replace( '"', "&#34;" );  // NOTE: by not using &quot; this result is also usable for XML.

  return string;
}



/**
 * Converts a string constant with HTML to one with escaped entities
 *
 * This version is suitable for use with string constants, as it creates a copy of the original string.
 *
 * @param string  The string to modify
 * @return        Another string with the escaped contents
 */
QString Utils::htmlEscape( const QString &string )
{
  QString copy( string );
  return htmlEscape( copy );
}



/**
 * Converts a string with escaped entities to one with HTML
 *
 * Only the main HTML control characters are unescaped; the string is reverted back to its
 * original state.
 * Neither KDE nor Qt have HTML to text decoding methods. Annoying.
 *
 * @param string  The string to modify
 * @return        A reference to the modified string
 */
QString &Utils::htmlUnescape( QString &string )
{
  string.replace( "&#34;", "\"" )
        .replace( "&#39;", "'"  )
        .replace( "&gt;",  ">"  )
        .replace( "&lt;",  "<"  )
        .replace( "&amp;", "&"  )
        .replace( "&#59;", ";"  );

  return string;
}



/**
 * Converts a string with escaped entities to one with HTML
 *
 * This version is suitable for use with string constants, as it creates a copy of the original string.
 *
 * @param string  The string to modify
 * @return        Another string with the escaped contents
 */
QString Utils::htmlUnescape( const QString &string )
{
  QString copy( string );
  return htmlUnescape( copy );
}



/**
 * Set in the library which capabilities are supported by the application.
 *
 * This is advertised to clients that you chat to.
 * Use this early in the application startup to set which features your application can
 * interpret.
 *
 * By default, the library assumes that the application is NOT capable of supporting
 * any particular set of capabilities.
 * The library itself will advertise some capabilities it internally uses and supports,
 * while the others are left to the application to set.
 * The list of capabilities you can set follows.
 *
 * CF_SUPPORTS_GIF_INK
 * CF_ONLINE_VIA_MOBILE
 * CF_ONLINE_VIA_MSN_EXPLORER
 * CF_SUPPORTS_GIF_INK
 * CF_SUPPORTS_ISF_INK
 * CF_WEBCAM_DETECTED
 * CF_IS_MOBILE_ENABLED
 * CF_IS_MSN_DIRECT_DEVICE_ENABLED
 * CF_MOBILE_MESSAGING_DISABLED
 * CF_ONLINE_VIA_WEBIM
 * CF_MOBILE_DEVICE
 * CF_HAS_MSN_SPACE
 * CF_IS_MEDIA_CENTER_EDITION_USER
 * CF_SUPPORTS_WINKS
 * CF_IS_BOT
 * CF_SUPPORTS_VOICEIM
 * CF_HAS_ONECARE
 * CF_USING_ALIAS
 * CF_SUPPORTS_RTC_VIDEO
 * CF_SUPPORTS_P2P_VERSION_2
 *
 * Setting any capability other than these will have no effect. If you do not set a
 * capability, it will not be advertised and clients will not attempt to perform actions
 * that require that capability (i.e, if you do not advertise CF_SUPPORTS_WINKS you will
 * not receive any winks).
 *
 * @param capabilities The bitwise ORed capabilities supported by your application.
 * @param extendedCapabilities The bitwise ORed extended capabilities supported by your application.
 */
void Utils::setClientCapabilities( ClientCapabilities::CapabilitiesFlags capabilities, ClientCapabilities::CapabilitiesExtendedFlags extendedCapabilities )
{
  ClientCapabilities::CapabilitiesFlags         libraryCaps    = ClientCapabilities::CF_NONE;
  ClientCapabilities::CapabilitiesExtendedFlags libraryExtCaps = ClientCapabilities::CXF_NONE;

  // NOTE: When changing the libraryCaps, all MSNP2P code need to be retested again!
  // The capabilities are like a contract. The other client assumes everything that's promised here will work.

  // Message chuncks are supported
  libraryCaps |= ClientCapabilities::CF_SUPPORTS_MESSAGE_CHUNKING;

  // We can choose to not advertise any protocol-related caps, for P2P testing
#if NOTIFICATION_HIDE_P2P_SUPPORT != 0
  libraryCaps |= ClientCapabilities::CF_VERSION_14_0;
#endif

  ClientCapabilities::CapabilitiesFlags acceptedCaps =
      ClientCapabilities::CF_SUPPORTS_GIF_INK
    | ClientCapabilities::CF_ONLINE_VIA_MOBILE
    | ClientCapabilities::CF_ONLINE_VIA_MSN_EXPLORER
    | ClientCapabilities::CF_SUPPORTS_GIF_INK
    | ClientCapabilities::CF_SUPPORTS_ISF_INK
    | ClientCapabilities::CF_WEBCAM_DETECTED
    | ClientCapabilities::CF_IS_MOBILE_ENABLED
    | ClientCapabilities::CF_IS_MSN_DIRECT_DEVICE_ENABLED
    | ClientCapabilities::CF_MOBILE_MESSAGING_DISABLED
    | ClientCapabilities::CF_ONLINE_VIA_WEBIM
    | ClientCapabilities::CF_MOBILE_DEVICE
    | ClientCapabilities::CF_HAS_MSN_SPACE
    | ClientCapabilities::CF_IS_MEDIA_CENTER_EDITION_USER
    | ClientCapabilities::CF_SUPPORTS_WINKS
    | ClientCapabilities::CF_IS_BOT
    | ClientCapabilities::CF_SUPPORTS_VOICEIM
    | ClientCapabilities::CF_HAS_ONECARE
    | ClientCapabilities::CF_USING_ALIAS;

  ClientCapabilities::CapabilitiesExtendedFlags acceptedExtCaps =
      ClientCapabilities::CXF_SUPPORTS_RTC_VIDEO
    | ClientCapabilities::CXF_SUPPORTS_P2P_VERSION_2;

  // Only keep the caps we accept by ANDing them with our "whitelists"
  clientCapabilities_.setCaps(
    ( libraryCaps | ( acceptedCaps & capabilities ) ),
    ( libraryExtCaps | ( acceptedExtCaps & extendedCapabilities ) ) );
}



/**
 * Use this to inform the library of your client name and version This information is broadcasted to clients that
 * you chat to.
 *
 * If you do not set these details the library will provide its own defaults based on its
 * name and current version.
 *
 * Use setClientCapabilities and setClientLogging to specify your client's logging settings and
 * individual capabilities.
 *
 * @see setClientCapabilties
 * @see setClientLogging
 * @param name Name of your application
 * @param version Version of your application
 */
void Utils::setClientName( const QString &name, const QString &version )
{
  clientName_ = name;
  clientVersion_ = version;
}



/**
 * Use this to inform the library when your chat logging settings change.
 *
 * This information is passed to clients that you chat with.
 *
 * @param loggingMode Your clients current logging mode.
 */
void Utils::setClientLogging( ChatLoggingMode loggingMode )
{
  clientLogging_ = loggingMode;
}
