/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Valerio Pilo <valerio@kmess.org>                          *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file utils_internal.cpp
 */

#include "utils_internal.h"
#include "debug/libkmessdebug.h"

#include <KMess/ClientCapabilities>
#include <KMess/Utils>

#include <ctype.h>

#include <QDateTime>
#include <QStringList>
#include <QTextCodec>


using namespace KMessInternal;



/**
 * @brief Retrieve the handle and network of a contact from a string of the form "netid:handle"
 *
 * @return Pair of handle properties
 */
bool InternalUtils::getHandle( const QString &string, QString &handle, KMess::NetworkType &network )
{
  // Check the string beforehand: verify its length..
  if( string.length() < 3 )
  {
    handle.clear();
    network = KMess::NetworkMsn;

    return false;
  }

  // ..and that it's an ID string and not a simple handle
  if( ! string.contains( ':' ) )
  {
    handle  = string;
    network = KMess::NetworkMsn;

    return true;
  }

  // It's an ID: parse it

  bool ok;
  QStringList pair( string.split( ':', QString::KeepEmptyParts ) );

  KMESS_ASSERT( pair.size() == 2 );

  // Extract the handle
  handle = pair[1].toLower();

  // Extract the network ID
  network = (KMess::NetworkType) pair[0].toInt( &ok );

  // Validate the values
  if( handle.length() <= 5 || ! ok )
  {
    handle.clear();
    network = KMess::NetworkMsn;

    warning() << "Got unrecognizable network ID/handle pair! Full handle:" << string;
    return false;
  }

  return true;
}



// Combine the handle and network of a contact into a correctly-formed string
QString InternalUtils::makeHandle( const QString &handle, KMess::NetworkType network )
{
  return QString::number( network ) +
         ':' +
         handle;
}



/**
 * Given a MediaType, convert it to a string for UUX.
 */
QString InternalUtils::mediaTypeToString( KMess::MediaType type )
{
  switch ( type )
  {
    case KMess::MediaMusic:
      return "Music";
      break;
      
    case KMess::MediaGaming:
      return "Gaming";
      break;
      
    case KMess::MediaOffice:
      return "Office";
      break;
      
    default:
      return "None";
      break;
  }
}


// Decode MIME strings like =?iso...=...?=...
QString InternalUtils::decodeRFC2047String( const QString &string )
{
  QByteArray bytes( string.toUtf8() );

  QString result;
  QByteArray charset;
  char *pos, *beg, *end, *mid=0;
  QByteArray str, cstr, LWSP_buffer;
  char encoding='Q', ch;
  bool valid, lastWasEncodedWord=false;
  const int maxLen=200;
  int i;

  if( ! bytes.contains("=?") )
  {
    if( string.indexOf( '\n' ) == -1 ) return string;
    QString str2( (QChar*)0, string.length() );
    int i = 0;
    while( i < string.length() )
    {
      if (str[i] == '\n')
      {
        str2 += ' ';
        i += 2;
      } else {
        str2 += str[i];
        i++;
      }
    }
    return str2;
  }

  for (pos = bytes.data(); *pos; pos++)
  {
    // line unfolding
    if ( pos[0] == '\r' && pos[1] == '\n' ) {
      pos++;
      continue;
    }
    if ( pos[0] == '\n' )
      continue;
    // collect LWSP after encoded-words,
    // because we might need to throw it out
    // (when the next word is an encoded-word)
    if ( lastWasEncodedWord && ( pos[0] == ' ' || pos[0] == '\t' ) ) {
      LWSP_buffer += pos[0];
      continue;
    }
    // verbatimly copy normal text
    if (pos[0]!='=' || pos[1]!='?')
    {
      result += LWSP_buffer + pos[0];
      LWSP_buffer = 0;
      lastWasEncodedWord = false;
      continue;
    }
    // found possible encoded-word
    beg = pos+2;
    end = beg;
    valid = true;
    // parse charset name
    charset = "";
    for (i=2,pos+=2; i<maxLen && (*pos!='?'&&(*pos==' '||ispunct(*pos)||isalnum(*pos))); ++i)
    {
      charset += *pos;
      pos++;
    }
    if (*pos!='?' || i<4 || i>=maxLen) valid = false;
    else
    {
      // get encoding and check delimiting question marks
      encoding = (char) toupper(pos[1]);
      if (pos[2]!='?' || (encoding!='Q' && encoding!='B'))
        valid = false;
      pos+=3;
      i+=3;
    }
    if (valid)
    {
      mid = pos;
      // search for end of encoded part
      while (i<maxLen && *pos && !(*pos=='?' && *(pos+1)=='='))
      {
        ++i;
        ++pos;
      }
      end = pos+2;//end now points to the first char after the encoded string
      if (i>=maxLen || !*pos) valid = false;
    }
    if (valid)
    {
      // valid encoding: decode and throw away separating LWSP
      ch = *pos;
      *pos = '\0';
      str = QByteArray( mid ).left((int)(mid - pos - 1));
      if (encoding == 'Q')
      {
        // decode quoted printable text
        str.replace( '_', ' ' );
        cstr = quotedPrintableDecode( str );
      }
      else
      {
        // decode base64 text
        cstr = QByteArray::fromBase64( str );
      }

      QTextCodec *codec;
      if( charset.isEmpty() )
      {
        codec = 0;
      }
      else
      {
        codec = QTextCodec::codecForName( charset.toLower() );
      }

      if (!codec)
      {
        result += QString::fromUtf8( cstr );
      }
      else
      {
        result += codec->toUnicode(cstr);
      }

      lastWasEncodedWord = true;

      *pos = ch;
      pos = end -1;
    }
    else
    {
      // invalid encoding, keep separating LWSP.
      //result += "=?";
      //pos = beg -1; // because pos gets increased shortly afterwards
      pos = beg - 2;
      result += LWSP_buffer;
      result += *pos++;
      result += *pos;
      lastWasEncodedWord = false;
    }
    LWSP_buffer = 0;
  }
  return result;
}



// List of valid characters in hex data. Used by quotedPrintableDecode().
static const char hexChars[16] =
{
  '0', '1', '2', '3', '4', '5', '6', '7'
, '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'
};


// A strchr(3) version for broken systems. Used by quotedPrintableDecode().
static int rikFindChar( register const char * _s, const char c )
{
  register const char * s = _s;

  while( true )
  {
    if( (0 == *s) || (c == *s) ) break; ++s;
    if( (0 == *s) || (c == *s) ) break; ++s;
    if( (0 == *s) || (c == *s) ) break; ++s;
    if( (0 == *s) || (c == *s) ) break; ++s;
  }

  return s - _s;
}



/**
 * Decode a Quoted-Printable string.
 *
 * Code taken from KCodecs, copyright of the KDE team, and Rik Hemsley.
 */
QByteArray InternalUtils::quotedPrintableDecode( const QByteArray& in )
{
  QByteArray out;

  if( in.isEmpty() )
  {
    return out;
  }

  char *cursor;
  const char *data;
  const unsigned int length = in.size();

  data = in.data();
  out.resize( length );
  cursor = out.data();

  for( unsigned int i = 0; i < length; i++ )
  {
    char c( in[i] );

    if( '=' == c )
    {
      if( i < length - 2 )
      {
        char c1 = in[i + 1];
        char c2 = in[i + 2];

        if( ('\n' == c1 ) || ( '\r' == c1 && '\n' == c2 ) )
        {
          // Soft line break. No output.
          if( '\r' == c1 )
            i += 2;        // CRLF line breaks
          else
            i += 1;
        }
        else
        {
          // =XX encoded byte.

          int hexChar0 = rikFindChar( hexChars, c1 );
          int hexChar1 = rikFindChar( hexChars, c2 );

          if( hexChar0 < 16 && hexChar1 < 16 )
          {
            *cursor++ = char( (hexChar0 * 16 ) | hexChar1 );
            i += 2;
          }
        }
      }
    }
    else
    {
      *cursor++ = c;
    }
  }

  out.truncate( cursor - out.data() );

  return out;
}



/**
 * Convert a status to a three-letter code (like BSY for Busy)
 *
 * @param status The status to convert
 * @return Three-letter status code
 */
QString InternalUtils::statusToCode( const KMess::MsnStatus status )
{
  // Just return the code
  switch( status )
  {
    case KMess::OnlineStatus:         return QString( "NLN" );
    case KMess::AwayStatus:           return QString( "AWY" );
    //case STATUS_AWAY_AUTOREPLY: return QString( "AWY" ); KMess-specific TODO: remove
    case KMess::BrbStatus:             return QString( "BRB" );
    case KMess::BusyStatus:            return QString( "BSY" );
    case KMess::InvisibleStatus:       return QString( "HDN" );
    case KMess::IdleStatus:            return QString( "IDL" );
    case KMess::OfflineStatus:         return QString( "FLN" );
    case KMess::PhoneStatus:           return QString( "PHN" );
    case KMess::LunchStatus:            return QString( "LUN" );
    default:
      warning() << "Invalid status" << status << "!";
      return QString( "NLN" );
  }
}



/**
 * Convert a three-letter code to a status
 *
 * @param status The status to convert
 *
 * @return MSN status enum value
 */
KMess::MsnStatus InternalUtils::statusFromCode( const QString &status )
{
       if( status == "AWY" ) return KMess::AwayStatus;
  else if( status == "BRB" ) return KMess::BrbStatus;
  else if( status == "BSY" ) return KMess::BusyStatus;
  else if( status == "FLN" ) return KMess::OfflineStatus;
  else if( status == "HDN" ) return KMess::InvisibleStatus;
  else if( status == "IDL" ) return KMess::IdleStatus;
  else if( status == "LUN" ) return KMess::LunchStatus;
  else if( status == "NLN" ) return KMess::OnlineStatus;
  else if( status == "PHN" ) return KMess::PhoneStatus;
  else
  {
    warning() << "Invalid status" << status << "!";
    return KMess::OnlineStatus;
  }
}



/**
 * Given a string from UBX, convert it to the correct media type.
 */
KMess::MediaType InternalUtils::stringToMediaType( const QString &str )
{
  QString compare = str.toLower();
  if ( compare == "gaming" )
  {
    return KMess::MediaGaming;
  }
  else if ( compare == "music" )
  {
    return KMess::MediaMusic;
  }
  else if ( compare == "office" )
  {
    return KMess::MediaOffice;
  }
  else
  {
    return KMess::MediaNone;
  }
}

/**
 * Parse an RFC 2822 date into an UTC date.
 *
 * RFC 2822 dates are, for example: "15 Nov 2005 14:24:27 -0800".
 * The resulting datetime is in UTC format.
 *
 * @param date The string with a date to parse
 * @return QDateTime object, invalid on error
 */
QDateTime InternalUtils::parseRfcDate( const QString &date )
{
  const char shortDay[][4] = {
    "Mon", "Tue", "Wed",
    "Thu", "Fri", "Sat",
    "Sun"
    };
  const char longDay[][10] = {
    "Monday", "Tuesday", "Wednesday",
    "Thursday", "Friday", "Saturday",
    "Sunday"
    };
  const char shortMonth[][4] = {
    "Jan", "Feb", "Mar", "Apr",
    "May", "Jun", "Jul", "Aug",
    "Sep", "Oct", "Nov", "Dec"
    };

  QString str( date.trimmed() );
  if( str.isEmpty() )
  {
    return QDateTime();
  }

  int nyear  = 6;   // indexes within string to values
  int nmonth = 4;
  int nday   = 2;
  int nwday  = 1;
  int nhour  = 7;
  int nmin   = 8;
  int nsec   = 9;

  QStringList parts;

  // Also accept obsolete form "Weekday, DD-Mon-YY HH:MM:SS ±hhmm"
  QRegExp rx( "^(?:([A-Z][a-z]+),\\s*)?(\\d{1,2})(\\s+|-)([^-\\s]+)(\\s+|-)(\\d{2,4})\\s+(\\d\\d):(\\d\\d)(?::(\\d\\d))?\\s+(\\S+)$" );

  if( ! str.indexOf( rx ) )
  {
    // Check that if date has '-' separators, both separators are '-'.
    parts = rx.capturedTexts();
    bool h1 = ( parts[3] == QLatin1String( "-" ) );
    bool h2 = ( parts[5] == QLatin1String( "-" ) );
    if( h1 != h2 )
    {
      return QDateTime();
    }
  }
  else
  {

    // Check for the obsolete form "Wdy Mon DD HH:MM:SS YYYY"
    rx = QRegExp( "^([A-Z][a-z]+)\\s+(\\S+)\\s+(\\d\\d)\\s+(\\d\\d):(\\d\\d):(\\d\\d)\\s+(\\d\\d\\d\\d)$" );
    if( str.indexOf( rx ) )
    {
      return QDateTime();
    }

    nyear  = 7;
    nmonth = 2;
    nday   = 3;
    nwday  = 1;
    nhour  = 4;
    nmin   = 5;
    nsec   = 6;
    parts = rx.capturedTexts();
  }

  bool ok[4];
  int day    = parts[nday].toInt( &ok[0] );
  int year   = parts[nyear].toInt( &ok[1] );
  int hour   = parts[nhour].toInt( &ok[2] );
  int minute = parts[nmin].toInt( &ok[3] );
  if( ! ok[0] || ! ok[1] || ! ok[2] || ! ok[3] )
  {
    return QDateTime();
  }

  int second = 0;
  if( ! parts[nsec].isEmpty() )
  {
    second = parts[nsec].toInt( &ok[0] );
    if( ! ok[0] )
    {
      return QDateTime();
    }
  }

  bool leapSecond = ( second == 60 );
  if( leapSecond )
  {
    second = 59;   // apparently a leap second - validate below, once time zone is known
  }

  int month = 0;
  for( ; month < 12 && parts[nmonth] != shortMonth[month]; ++month ) ;

  int dayOfWeek = -1;
  if( ! parts[nwday].isEmpty() )
  {
    // Look up the weekday name
    while( ++dayOfWeek < 7 && shortDay[dayOfWeek] != parts[nwday] ) ;
    if( dayOfWeek >= 7 )
    for( dayOfWeek = 0; dayOfWeek < 7 && longDay[dayOfWeek] != parts[nwday]; ++dayOfWeek ) ;
  }

  if( month >= 12 || dayOfWeek >= 7 )
  {
    return QDateTime();
  }

  int i = parts[nyear].size();
  if( i < 4 )
  {
    // It's an obsolete year specification with less than 4 digits
    year += ( i == 2 && year < 50 ) ? 2000 : 1900;
  }

  // Parse the UTC offset part
  int offset = 0; // set default to '-0000'
  bool negOffset = false;

  if( parts.count() > 10 )
  {
    rx = QRegExp( "^([+-])(\\d\\d)(\\d\\d)$" );
    if( ! parts[10].indexOf( rx ) )
    {
      // It's a UTC offset ±hhmm
      parts = rx.capturedTexts();
      offset = parts[2].toInt( &ok[0] ) * 3600;
      int offsetMin = parts[3].toInt( &ok[1] );
      if( ! ok[0] || ! ok[1] || offsetMin > 59 )
      {
        return QDateTime();
      }
      offset += offsetMin * 60;
      negOffset = ( parts[1] == QLatin1String( "-" ) );
      if( negOffset )
      {
        offset = -offset;
      }
    }
    else
    {
      // Check for an obsolete time zone name
      QByteArray zone = parts[10].toLatin1();
      if( zone.length() == 1 && isalpha( zone[0] ) && toupper( zone[0] ) != 'J' )
      {
        negOffset = true;    // military zone: RFC 2822 treats as '-0000'
      }
      else if( zone != "UT" && zone != "GMT" )    // treated as '+0000'
      {
        offset = (zone == "EDT")           ? -4*3600
        : (zone == "EST" || zone == "CDT") ? -5*3600
        : (zone == "CST" || zone == "MDT") ? -6*3600
        : (zone == "MST" || zone == "PDT") ? -7*3600
        : (zone == "PST")                  ? -8*3600
        : 0;

        if( ! offset )
        {
          // Check for any other alphabetic time zone
          bool nonalpha = false;
          for( int i = 0, end = zone.size(); i < end && ! nonalpha; ++i )
          nonalpha = ! isalpha( zone[i] );
          if( nonalpha )
          {
            return QDateTime();
          }
          negOffset = true;    // unknown time zone: RFC 2822 treats as '-0000'
        }
      }
    }
  }

  QDate qdate( year, month+1, day );
  if( ! qdate.isValid() )
  {
    return QDateTime();
  }

  QDateTime result( qdate, QTime( hour, minute, second ), Qt::UTC );
  result = result.addSecs( offset );

  // invalid date/time, or weekday doesn't correspond with date
  if( ! result.isValid()
  ||  ( dayOfWeek >= 0 && result.date().dayOfWeek() != dayOfWeek+1 ) )
  {
    return QDateTime();
  }

  if( leapSecond )
  {
    // Validate a leap second time. Leap seconds are inserted after 23:59:59 UTC.
    // Convert the time to UTC and check that it is 00:00:00.
    if( ( hour*3600 + minute*60 + 60 - offset + 86400*5 ) % 86400 ) // (max abs(offset) is 100 hours)
    {
      // the time isn't the last second of the day
      return QDateTime();
    }
  }

  return result;
}


