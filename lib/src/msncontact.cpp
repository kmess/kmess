/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Valerio Pilo <valerio@kmess.org>                          *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file msncontact.cpp
 * @brief A contact's representation
 */

#include "debug/libkmessdebug.h"

#include <KMess/MsnContact>
#include <KMess/NetworkGlobals>
#include <KMess/MPOPEndpoint>
#include <KMess/ContactProfile>

#include <QUuid>

#include "utils/utils_internal.h"


using namespace KMess;



/**
 * The invited contact constructor.
 *
 * An invited contact is completely unknown to us, except for his/her id,
 * friendly name and possibly client capabilities.
 *
 * @param idString The "networkId:email" pair which identifies this contact
 */
MsnContact::MsnContact( const QString& idString )
: id_( idString )
, memberships_( 0 )
, profile_( 0 )
, status_( OfflineStatus )
{
  setId( idString );
  media_.type = MediaNone;
}



/**
 * The regular contact constructor.
 *
 * @param idString The "networkId:email" pair which identifies this contact
 * @param displayName the display name this contact will be initialized with
 * @param lists The membership lists of this contact, bitwise-AND'ed together.
 *              See MsnMemberships for more information.
 * @param groupIds The MsnGroup groups this contact is in, by their group GUID's.
 * @param guid The GUID string for this contact.
 *
 * @see MsnMemberships
 */
MsnContact::MsnContact( const QString& idString, const QString& displayName,
                        int lists, QList<MsnGroup*> groups, const QString& guid )
: displayName_( displayName )
, groups_( groups )
, guid_( guid )
, id_( idString )
, profile_( 0 )
, status_( OfflineStatus )
{
  setId( idString );
  media_.type = MediaNone;

  // Add the contact to the membership lists it belongs to
  memberships_ = (MsnMemberships) ( lists & ( MSN_LIST_FRIEND | MSN_LIST_ALLOWED | MSN_LIST_BLOCKED | MSN_LIST_REVERSE | MSN_LIST_PENDING ) );
}



/**
 * The destructor.
 */
MsnContact::~MsnContact()
{
  qDeleteAll( endpoints_ );

  if ( profile_ )
  {
    delete profile_;
  }
  
  debug() << "Destroying contact id:" << id_;
}



/**
 * Set the MPOPEndpoints for this contact.
 *
 * Emits endpointCreated/endpointDestroyed for any endpoints that were created or
 * destroyed. If an endpoint already exists, its data is updated.
 *
 * @param endpoints The list of MPOPEndpoint instances.
 */
void MsnContact::setEndpoints( QList<MPOPEndpoint *> endpoints )
{
  QList<QUuid> deletedEndpoints = endpoints_.keys();

  foreach( MPOPEndpoint *ep, endpoints )
  {
    // if the end point already exists, update its data
    if ( deletedEndpoints.contains( ep->guid() ) )
    {
      deletedEndpoints.removeAll( ep->guid() );

      MPOPEndpoint *existingEp = endpoints_.value( ep->guid() );

      debug() << "Updated MPOP endpoint" << ep->guid() << " (name:" << ep->name() << ") for contact " << handle();

      // copy/update data.
      *existingEp = *ep;
    }
    else
    {
      // this is a new end point. just insert it.
      debug() << "Added MPOP endpoint " << ep->guid() << " (name:" << ep->name() << ") to contact" << handle();

      endpoints_.insert( ep->guid(), ep );
    }
  }

  // at this point deletedEndpoints contains the guids that for
  // endpoints that no longer exist. remove them.
  foreach( const QUuid &id, deletedEndpoints )
  {
    MPOPEndpoint *ep = endpoints_.take( id );

    debug() << "Deleted MPOP endpoint" << ep->guid() << " (name:" << ep->name() << ") from contact " << handle();

    delete ep;
  }

  emit endpointsChanged();
}



/**
 * @brief Returns the capabilities of the contact's client.
 *
 * @return The client's capabilities.
 */
const ClientCapabilities MsnContact::capabilities() const
{
  return capabilities_;
}



/**
 * Return a list of the known MPOP endpoints for this contact.
 */
QHash<QUuid,MPOPEndpoint*> MsnContact::endpoints() const
{
  return endpoints_;
}



/**
 * Return the contact email
 *
 * @return QString
 */
const QString& MsnContact::handle() const
{
  return email_;
}



/**
 * Return the contact's friendly name
 */
const QString& MsnContact::displayName() const
{
  if( displayName_.isEmpty() )
  {
    return email_;
  }

  return displayName_;
}



/**
 * Return TRUE if the contact has the given capability, FALSE otherwise.
 * @return bool
 */
bool MsnContact::hasCapability( ClientCapabilities::CapabilitiesFlags capability ) const
{
  return ( capabilities_.caps() & capability );
}



/**
 * Return the list of user-defined groups where the contact can be found
 *
 * @return QStringList
 */
QList<MsnGroup*> MsnContact::groups() const
{
  return groups_;
}



/**
 * Return the GUID assigned by the server to this contact
 *
 * @return QString
 */
const QString& MsnContact::guid() const
{
  return guid_;
}



/**
 * Return the contact's full ID
 *
 * @return QString
 */
const QString& MsnContact::id() const
{
  return id_;
}



/**
 * @brief Return the current media of the contact.
 *
 * Using this method, you can retrieve the media the contact is currently
 * watching, listening to, or playing. The MediaData instance returned contains
 * everything you need to display the user's media string.
 *
 * @returns The current media, as a MediaData structure
 * @see MediaData
 */
MediaData MsnContact::media() const
{
  return media_;
}



/**
 * Retrieve the membership lists where this contact is located.
 */
MsnMemberships MsnContact::memberships() const
{
  return memberships_;
}



/**
 * @brief Return the MsnObject of this contact.
 *
 * Note: the MsnObject is an advanced part of the MSN protocol. If possible,
 * you are recommended to use the other methods in this and other classes.
 */
const MsnObject &MsnContact::msnObject() const
{
  return msnObject_;
}



/**
 * Return the ID of the network the contact is registered on
 *
 * @return NetworkType
 */
NetworkType MsnContact::network() const
{
  return network_;
}



/**
 * Return the personal message (PSM) of the contact.
 *
 * @return The personal message
 */
const QString &MsnContact::personalMessage() const
{
  return personalMessage_;
}



/**
 * Provides access to a contact's profile information.
 * 
 * A contact must be added to Windows Live address book in order to have
 * a valid ContactProfile. For all other users this method will return NULL.
 */
ContactProfile *MsnContact::profile() const
{
  return profile_;
}



/**
 * Set the capabilities of this MsnContact.
 */
void MsnContact::setCapabilities( ClientCapabilities capabilities )
{
  capabilities_ = capabilities;
}



/**
 * Set the friendly name of this MsnContact.
 */
void MsnContact::_setDisplayName( const QString &displayName )
{
  displayName_ = displayName;

  emit displayNameChanged();
}



/**
 * Change the list of groups this contact is part of.
 *
 * @param groups A list of MsnGroup instances, one per group membership
 */
void MsnContact::setGroups( QList<MsnGroup*> groups )
{
  if ( groups_ != groups )
  {
    groups_ = groups;
    emit groupsChanged();
  }
}



/**
 * Set the GUID.
 *
 * @param guid The new GUID value.
 */
void MsnContact::setGuid( const QString &guid )
{
  guid_ = guid;
}



/**
 * Set the ID of this MsnContact.
 *
 * The ID consists of the network ID and the handle. For example,
 * '1:foo@example.org' is a valid handle on the MSN network.
 */
void MsnContact::setId( const QString &idString )
{
  id_ = idString;

  bool isIdValid = KMessInternal::InternalUtils::getHandle( idString, email_, network_ );

  debug() << "Setting MsnContact to" << ( isIdValid ? "valid" : "invalid" ) << "id:" << idString;

  if( ! isIdValid )
  {
    network_ = NetworkMsn;
    email_  = idString;
  }
}



/**
 * Set the membership lists where this contact is located.
 *
 * @param memberships The new list of memberships
 */
void MsnContact::setMemberships( const MsnMemberships memberships )
{
  if ( memberships_ != memberships )
  {
    memberships_ = memberships;

    emit membershipsChanged();
  }
}



/**
 * Set the current media of the user.
 */
void MsnContact::setMedia( MediaData media )
{
  if ( media_ != media )
  {
    media_ = media;
    emit mediaChanged();
  }
}



/**
 * Set the MSN Object of this contact.
 */
void MsnContact::setMsnObject( const MsnObject& msnObject )
{
  msnObject_ = msnObject;
  emit msnObjectChanged();
}



/**
 * Set the network type of this contact. May be one of either NetworkYahoo or NetworkMsn.
 *
 * This must be set correctly for basic contact functions to work (chat, add, block, remove, etc).
 *
 * @param network The network type.
 */
void MsnContact::setNetwork( NetworkType network )
{
  network_ = network;
}



/**
 * Change the personal message (PSM) of the contact.
 *
 * @param message The new personal message
 */
void MsnContact::_setPersonalMessage( const QString &message )
{
  if ( personalMessage_ != message )
  {
    personalMessage_ = message;
    emit personalMessageChanged();
  }
}



/**
 * Set the contact profile instace. Emits profileChanged().
 */
void MsnContact::setProfile( ContactProfile *profile )
{
  profile_ = profile;
  emit profileChanged();
}



/**
 * Change the status of this contact.
 *
 * @param status The new status
 */
void MsnContact::setStatus( MsnStatus status )
{
  debug() << "Setting status of contact" << id_ << "to" << KMessInternal::InternalUtils::statusToCode( status );

  MsnStatus prevStatus = status_;
  status_ = status;

  if ( status_ == OfflineStatus )
  {
    emit contactOffline();
  }
  else if ( status_ == OnlineStatus && prevStatus == OfflineStatus )
  {
    emit contactOnline();
  }

  // also emit changedStatus, the generic "something changed to contact status" signal.
  emit statusChanged();
}



/**
 * Return the current status of the contact.
 *
 * @return Status
 */
MsnStatus MsnContact::status() const
{
  return status_;
}



/**
 * Return whether the contact supports a certain Ink type.
 *
 * This is just a convenience method over contact->hasCapability().
 *
 * @param format Specified Ink format
 * @return bool
 */
bool MsnContact::supportsInkType( InkFormat format ) const
{
  const ClientCapabilities::CapabilitiesFlags caps = capabilities_.caps();

  switch( format )
  {
    case FORMAT_GIF:
      return ( caps & ClientCapabilities::CF_SUPPORTS_GIF_INK );
    case FORMAT_ISF:
      return ( caps & ClientCapabilities::CF_SUPPORTS_ISF_INK );
    default:
      warning() << "Unknown Ink format" << format << "specified!";
      return false;
  }
}



/**
 * Return whether the contact's client supports MSNP2P.
 *
 * This is just a convenience method over contact->hasCapability().
 *
 * @return bool
 */
bool MsnContact::supportsP2P() const
{
  return capabilities_.supportsP2p();
}



/**
 * Cast this MsnContact to a QString containing handle() and network type.
 */
MsnContact::operator QString() const
{
  return handle() + " (Network: " + (network_ == NetworkYahoo ? "Yahoo" : "MSN") + ')';
}



QDebug operator<<( QDebug dbg, const KMess::MsnContact *contact )
{
  dbg << (QString)(*contact);
  return dbg;
}


QDebug operator<<( QDebug dbg, const ContactsList &list )
{
  QStringList output;
  foreach( KMess::MsnContact *c, list )
  {
    output << (QString)(*c);
  }
  
  dbg << output.join(", ");
  return dbg;
}


QDebug operator<<( QDebug dbg, const IndexedContactsList &list )
{
  dbg << list.values();
  return dbg;
}
