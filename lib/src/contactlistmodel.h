/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Mike K. Bennett <mkb1237b@hotmail.com>                    *
 * (C) Copyright Diederik van der Boor <diederik@kmess.org>                *
 * (C) Copyright Valerio Pilo <valerio@kmess.org>                          *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file contactlistmodel.h
 * @brief A read-only Qt model to represent the contacts list
 */

#ifndef CONTACTLISTMODEL_H
#define CONTACTLISTMODEL_H

#include <KMess/NetworkGlobals>

#include <QAbstractItemModel>
#include <QHash>



namespace KMess
{

// Forward declarations
class ContactListModelItem;
class MsnSession;

// Custom types
typedef QMap<QString, QVariant> ModelDataList;
typedef QList<ContactListModelItem *> ModelItemList;



/**
 * @brief Data class for the contact list.
 *
 * This class stores the state of the current contact list, it does not update the server.
 * The MsnNotificationConnection class sends update requests to the server.
 * When the server acknowledges the change, it will be updated here.
 * The user interface classes repond to the signals of this class to update their view.
 *
 * The instance of this class is shared between the MsnNotificationConnection class,
 * and re-used when the user connects with a different account.
 *
 * @author Mike K. Bennett
 * @author Valerio Pilo
 * @ingroup Contact
 */
class ContactListModel : public QAbstractItemModel
{
  Q_OBJECT

  public: // Public methods
    // The constructor
                         ContactListModel( MsnSession* session );
    // The destructor
    virtual             ~ContactListModel();
    // Restore the orginal state of the contact list (empty it)
    void                 reset( bool restore = true );

  public slots:
    // Insert a contact into the model
    void                 contactAdd( KMess::MsnContact *newContact );
    // Forward from a contact that it moved
    void                 contactMove( KMess::MsnContact *contact = 0 );
    // Update a contact's details
    void                 contactUpdate( KMess::MsnContact *contact = 0 );
    // Add a group to the contact list
    void                 groupAdd( const KMess::MsnGroup *group );
    // Remove a group
    void                 groupRemove( const KMess::MsnGroup *group );
    // Update a group's details
    void                 groupUpdate( const KMess::MsnGroup *group );

  public:  // Public model methods
    // Get the number of columns present in an index
    int                  columnCount( const QModelIndex &parent = QModelIndex() ) const;
    // Return the data contained by an index in the model
    QVariant             data( const QModelIndex &index, int role ) const;
    // Dumps the contact list contents to the debug output
    //void                 dump( ContactListModelItem *start = 0, int depth = 1 ) const;
    // Find out if a group is empty (ie, has no children)
    bool                 isGroupEmpty( const QString &groupId ) const;
    // Find out if a model index contains other indices
    bool                 hasChildren( const QModelIndex &parent ) const;
    // Find out if the model contains a valid index at the specified position
    bool                 hasIndex( int row, int column, const QModelIndex &parent = QModelIndex() ) const;
    // Obtain a model index for a specificly located item
    QModelIndex          index( int row, int column, const QModelIndex &parent = QModelIndex() ) const;
    // Obtain a model index's parent index
    QModelIndex          parent( const QModelIndex &index ) const;
    // Get the number of rows present in an index
    int                  rowCount( const QModelIndex &parent = QModelIndex() ) const;
    // Return the special groups
    IndexedGroupsList    specialGroups() const;
    // Specify which drag and drop operations are supported
    Qt::DropActions      supportedDropActions() const;

  private: // Private methods
    // Emits the Data Changed signal on one item
    void                 forwardDataChanged( ContactListModelItem *item );
    // Emits the Data Changed signal on some items
    void                 forwardDataChanged( ModelItemList itemList );

  private: // Private attributes
    // The root node which makes up the contact list tree
    ContactListModelItem *rootNode_;
    /// Session this object lives in
    MsnSession*           session_;
    /// The list of convenience groups used by the model to categorize the contacts
    IndexedGroupsList     specialGroups_;
};



} // Namespace kmess



#endif
