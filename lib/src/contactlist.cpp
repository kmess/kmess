/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Mike K. Bennett <mkb137b@hotmail.com>                     *
 * (C) Copyright Sjors Gielen <sjors@kmess.org>                            *
 * (C) Copyright Adam Goossens <fontknocker@kmess.org>                     *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file contactlist.cpp
 * @brief Interface to the Windows Live Address Book service
 */

#include <KMess/ContactList>
#include <KMess/MsnSession>
#include <KMess/MsnContact>
#include <KMess/MsnGroup>

#include "contactlistmodel.h"
#include "soap/addressbookservice.h"
#include "listsmanager.h"
#include "debug/libkmessdebug.h"

using namespace KMess;


// The constructor
ContactList::ContactList( MsnSession *session )
 : listsManager_( new KMessInternal::ListsManager( session ) ),  
   session_( session )
{
  debug() << "Creating contact list object";

  abService_ = new KMessInternal::AddressBookService( session_, listsManager_ );

  // chained signals from the address book service.
  connect( abService_, SIGNAL( contactAdded( KMess::MsnContact* ) ),
           this,       SIGNAL( contactAdded( KMess::MsnContact* ) ) );

  connect( abService_, SIGNAL( contactAddedToGroup( KMess::MsnContact*, KMess::MsnGroup* ) ),
           this,       SIGNAL( contactAddedToGroup( KMess::MsnContact*, KMess::MsnGroup* ) ) );

  connect( abService_, SIGNAL( contactRemovedFromGroup( KMess::MsnContact*, KMess::MsnGroup* ) ),
           this,       SIGNAL( contactRemovedFromGroup( KMess::MsnContact*, KMess::MsnGroup* ) ) );

  connect( abService_, SIGNAL( contactAdded( KMess::MsnContact* ) ),
           this,       SIGNAL( contactAdded( KMess::MsnContact* ) ) );

  connect( abService_, SIGNAL( groupAdded( KMess::MsnGroup* ) ),
           this,       SIGNAL( groupAdded( KMess::MsnGroup* ) ) );

  connect( abService_, SIGNAL( groupRemoved( KMess::MsnGroup* ) ),
           this,       SIGNAL( groupRemoved( KMess::MsnGroup* ) ) );

  connect( abService_, SIGNAL( groupRenamed( KMess::MsnGroup* ) ),
           this,       SIGNAL( groupRenamed( KMess::MsnGroup* ) ) );
}



// The destructor
ContactList::~ContactList()
{
  debug() << "DESTROYING...";

  // Delete the contacts lists
  delete listsManager_;
  delete abService_;

  debug() << "DESTROYED";
}



/**
 * @brief Request the service to add a contact to the friends list.
 *
 * It's possible to add a contact to multiple groups.
 *
 * If the contact wasn't previously known he/she will be added as a friend.
 * If the contact was previously known, he/she will no longer appear in the
 * allowed or removed contacts group, but in the friends list.
 * The contact should exist already at some list before for this to happen
 * (e.g. the block list or friends list).
 *
 * @param contact The MsnContact instance that represents the user.
 * @param groups A QList of MsnGroup instances, each one being a group the user will be added to. May be empty.
 */
void ContactList::addContact( const KMess::MsnContact *contact, const QList<MsnGroup*> &groups )
{
  KMESS_ASSERT( contact );

  if( ! contact )
  {
    warning() << "Attempted to add NULL contact.";
    return;
  }

  debug() << "Adding contact: " << contact;

  abService_->addContact( contact, groups );
}



/**
 * This is an overloaded function.
 *
 * @param  handle   Email address of the contact.
 * @param  network  The network of the contact (Yahoo or Msn).
 * @param  groups Groups where to add the contact. May be empty.
 */
void ContactList::addContact( const QString &handle, const KMess::NetworkType network, const QList<MsnGroup*> &groups )
{
  if( handle.isEmpty() )
  {
    warning() << "Attempted to add a contact with empty handle";
    return;
  }

  abService_->addContact( handle, network, groups );
}



/**
 * @brief Request the service to add a contact to another group.
 *
 * @param contact Pointer to the contact to add.
 * @param group Pointer to the group that the user will be added to.
 */
void ContactList::addContactToGroup( const MsnContact *contact, const MsnGroup *group )
{
  KMESS_ASSERT( contact );
  KMESS_ASSERT( group );

  if( ! contact || ! group )
  {
    warning() << "Attempted to add a contact to group, but either the contact or the group is NULL.";
    return;
  }
  
  if ( group->isSpecialGroup() )
  {
    warning() << "Cannot manually add contact to special group. This is handled automatically.";
    return;
  }

  debug() << "Adding contact:" << contact << "to group:" << group;

  abService_->addContactToGroup( contact, group );
}



/**
 * @brief Request the service to block a contact.
 *
 * @param contact Contact to block.
 */
void ContactList::blockContact( const KMess::MsnContact *contact )
{
  KMESS_ASSERT( contact );

  if( ! contact )
  {
    warning() << "Attempted to block a NULL contact";
    return;
  }

  debug() << "Blocking contact:" << contact;
  
  abService_->blockContact( contact );
}



/**
 * Requests a fetch of the address book from the address book service.
 * 
 * Any changes are merged into the local contact list, and addressBookParsed()
 * is emitted.
 * 
 * @param cacheDir The full path to where the cache files can be located.
 */
void ContactList::fetch( const QString &cacheDir )
{
  abService_->fetchAddressBook( cacheDir );
}



/**
 * @brief Request the service to move a contact between two groups.
 *
 * @param contact    The contact that is being moved.
 * @param oldGroup   MsnGroup for the source group
 * @param newGroup   MsnGroup for the destination group
 */
void ContactList::moveContact( const KMess::MsnContact *contact, const KMess::MsnGroup *oldGroup, const KMess::MsnGroup *newGroup )
{
  KMESS_ASSERT( contact );
  KMESS_ASSERT( oldGroup );
  KMESS_ASSERT( newGroup );

  if( ! contact || !oldGroup || ! newGroup )
  {
    warning() << "Must have valid MsnContact and MsnGroup instances to move contact between groups.";
    return;
  }

  // you can't move to a special group - they don't exist on the server.
  if ( newGroup->isSpecialGroup() )
  {
    warning() << "Cannot manually move to special groups; this is handled automatically.";
    return;
  }

  debug() << "Moving contact:" << contact << "from group" << oldGroup << "to group" << newGroup;

  abService_->addContactToGroup( contact, newGroup );

  if ( oldGroup )
  {
    abService_->removeContactFromGroup( contact, oldGroup );
  }  
}



/**
 * @brief Request the service to unblock a contact.
 *
 * @param contact MsnContact instance for the contact.
 */
void ContactList::unblockContact( const KMess::MsnContact *contact )
{
  KMESS_ASSERT( contact );

  if( ! contact )
  {
    warning() << "Attempted to unblock a contact with NULL MsnContact instance.";
    return;
  }

  debug() << "Unblocking contact:" << contact;

  abService_->unblockContact( contact );
}




/**
 * Unknown profiles are profiles on the address book that are not associated with Passport in any way.
 * 
 * These profiles do not exist on your contact list. This method provides you a way to peruse this 
 * list.
 */
QList<ContactProfile*> ContactList::unknownProfiles() const
{
  return abService_->unknownProfiles();
}



/**
 * @brief Request the service to remove a contact from the friends list.
 *
 * By setting the \arg action parameter to RemoveAndBlock the contact will be
 * blocked before being removed. The default action is to remove the contact only.
 *
 * @param  contact  MsnContact of the contact.
 * @param  action   One of RemoveContactAction values. If RemoveAndBlock, the contact is blocked before being removed.
 */
void ContactList::removeContact( KMess::MsnContact *contact, RemoveContactOption option, RemoveContactAction action )
{
  KMESS_ASSERT( contact );

  if( ! contact )
  {
    warning() << "Attempted to remove a contact with NULL MsnContact instance.";
    return;
  }
  debug() << "Removing contact:" << contact << ". Messenger Only = " << ( option == RemoveFromMessenger ) << ". Blocking = " << ( action == RemoveAndBlock );

  abService_->deleteContact( contact, option, action );
}



/**
 * @brief Request the service to remove a contact from a group.
 *
 * @param contact  MsnContact to remove from a group.
 * @param group    MsnGroup to remove from.
 */
void ContactList::removeContactFromGroup( const KMess::MsnContact *contact, const KMess::MsnGroup *group )
{
  KMESS_ASSERT( contact );
  KMESS_ASSERT( group );

  if( ! contact || ! group )
  {
    warning() << "Attempted to remove a contact from group but either the contact or group is NULL.";
    return;
  }

  if ( group->isSpecialGroup() )
  {
    warning() << "Cannot manually remove contact from a special group. This is handled automatically.";
    return;
  }

  debug() << "Removing contact:" << contact << "from group:" << group;

  abService_->removeContactFromGroup( contact, group );
}


/**
 * Get a list with all known contacts.
 *
 * @return QHash
 */
const ContactsList ContactList::contacts() const
{
  return listsManager_->contacts().values();
}



// Return a list of all known groups
GroupsList ContactList::groups() const
{
  return listsManager_->groups().values();
}



/**
 * Creates an interator that will iterate over all contacts on the given membership list.
 * 
 * By default, will iterate over all contacts on the friend list.
 * 
 * The iterator operates on a copy so changes to the list during iteration will
 * not appear.
 * 
 * Delete the iterator when you are finished.
 */
QListIterator<MsnContact*> *ContactList::iterator( MsnMembership membership )
{  
  // totally cheating. if only QListIterator could be customised.
  QList<MsnContact*> contacts;
  foreach( MsnContact *c, listsManager_->contacts().values() )
  {
    if ( c->memberships() & membership )
    {
      contacts.append( c );
    }
  }

  // this is fine - implicit sharing means "contacts"
  // will exist post-return.
  return new QListIterator<MsnContact*>( contacts );
}



// Return a list of all known groups
IndexedGroupsList ContactList::groupsIndexedList() const
{
  return listsManager_->groups();
}



// Return the contact list's model
QAbstractItemModel* ContactList::contactListModel()
{
  return listsManager_->model();
}



/**
 * @brief Retrieve a MsnContact object through this session.
 *
 * The lists of known and unknown MsnContacts are checked.
 *
 * @param handle The handle of the contact
 */
KMess::MsnContact *ContactList::contact( const QString &handle ) const
{
  return listsManager_->contact( handle );
}



/**
 * @brief Retrieve a MsnGroup object through this session.
 *
 * The list of MsnGroups is checked. If this id does not exist
 * in the list yet, a new MsnGroup is created and stored.
 *
 * @param id The id of the group
 */
KMess::MsnGroup *ContactList::group( const QString &id ) const
{
  return listsManager_->group( id );
}




/**
 * @brief Request the service to create a group.
 *
 * @param name Name of the group.
 */
void ContactList::addGroup( const QString& name )
{
  KMESS_ASSERT( ! name.isEmpty() );

  if( name.isEmpty() )
  {
    warning() << "Attempted to add a group with empty name";
    return;
  }
  debug() << "Creating new group:" << name;

  abService_->addGroup( name );
}



/**
 * @brief Request the service to remove a group.
 *
 * @param group The group to be removed.
 */
void ContactList::removeGroup( const KMess::MsnGroup *group )
{
  KMESS_ASSERT( group );

  if( ! group )
  {
    warning() << "Attempted to remove a group with NULL MsnGroup.";
    return;
  }
  debug() << "Removing group:" << group;
  abService_->deleteGroup( group );
}



/**
 * @brief Request the service to rename a group.
 *
 * @param group   The group to be renamed.
 * @param newName New name
 */
void ContactList::renameGroup( const KMess::MsnGroup *group, const QString &newName )
{
  KMESS_ASSERT( group );
  KMESS_ASSERT( ! newName.isEmpty() );

  if( ! group || newName.isEmpty() )
  {
    warning() << "Need valid MsnGroup and a valid name to rename a group.";
    return;
  }
  debug() << "Renaming group:" << group << "to" << newName;
  abService_->renameGroup( group, newName );
}



/**
 * @internal
 * @brief Returns an instance to the ListsManager. API users should not use this method.
 */
KMessInternal::ListsManager *ContactList::listsManager() const
{
  return listsManager_;
}



/**
 * @internal
 * @brief Returns an instance to the AddressBookService. API users should not use this method.
 */
KMessInternal::AddressBookService *ContactList::abService() const
{
  return abService_;
}
