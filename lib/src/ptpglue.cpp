/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Adam Goossens <fontknocker@kmess.org>                     *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file ptpglue.cpp
 */

#include "ptpglue.h"
#include "debug/libkmessdebug.h"
#include "connections/msnswitchboardconnection.h"

using namespace KMessInternal;
using namespace KMess;

PtpGlue::PtpGlue( MsnSession *session )
: QObject()
, session_( session )
{
  KMESS_ASSERT( session );
}

/**
 * Destroys the manager. Also destroys any unclaimed switchboard connections.
 */
PtpGlue::~PtpGlue()
{
}



/**
 * When a data/P2P message comes in, give it through to the transport library.
 *
 * @param message KMess::Message from the switchboard.
 */
void PtpGlue::slotMessageReceived( KMess::Message message )
{
  MsnSwitchboardConnection *conn = qobject_cast<MsnSwitchboardConnection*>(sender());
  KMESS_ASSERT( conn );
  
  if(message.type() == DataMessageType
  || message.type() == PtpDataMessageType)
  {
    // TODO give it to the library
  }
}



/**
 * @brief Listen for data messages on a new switchboard.
 *
 * When a switchboard is created, it's possible that data messages will arrive
 * over this switchboard - in particular because data messages may arrive over
 * _any_ switchboard, not just the one the data session started on.
 *
 * @param switchboard The new switchboard to listen to.
 */
void PtpGlue::slotSwitchboardCreated( KMessInternal::MsnSwitchboardConnection *switchboard )
{
  connect( switchboard, SIGNAL(    receivedMessage(KMess::Message)),
           this,        SLOT(  slotMessageReceived(KMess::Message)));
}
