/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Valerio Pilo <valerio@kmess.org>                          *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file listsmanager.cpp
 * @brief Keeper of the contacts and groups lists
 */

#include "listsmanager.h"
#include "debug/libkmessdebug.h"
#include "contactlistmodel.h"

#include <KMess/MsnContact>
#include <KMess/MsnGroup>
#include <KMess/MsnSession>
#include <KMess/Self>

using namespace KMess;
using namespace KMessInternal;



/**
 * @brief The constructor
 *
 * @param session The MsnSession that created this ListsManager.
 */
ListsManager::ListsManager( MsnSession *session )
: QObject()
, session_( session )
{
  // Initialize the contact list model
  model_ = new ContactListModel( session_ );

  debug() << "Initializing self contact.";

  // Initialize the user's MsnContact
  self_ = new Self();
}



/// The destructor
ListsManager::~ListsManager()
{
  foreach( MsnContact* contact, contacts_ )
  {
    delete contact;
  }
  contacts_.clear();

  foreach( MsnContact* contact, unknownContacts_ )
  {
    delete contact;
  }
  unknownContacts_.clear();

  foreach( MsnGroup* group, groups_ )
  {
    delete group;
  }

  delete self_;
  delete model_;
}



/**
 * @brief Register a contact.
 *
 * Whenever the server notifies us of the presence of a new contact,
 * this method is called.
 * Usually, those notifications happen in two cases:
 * 1) While logging in, the list of already known contacts is registered.
 *    This method gets called once for each contact to register.
 * 2) In response to the addition of a contact by the user, the server
 *    tells us that the new contact is now in our list.
 */
KMess::MsnContact *ListsManager::contactAdd( const QString &handle, const QString &friendlyName, int lists,
                               QStringList groupIds,  const QString &guid, KMess::NetworkType network )
{
  KMESS_ASSERT( ! handle.isEmpty() );

  // Check if the contact already exists
  if( contacts_.contains( handle ) )
  {
    warning() << "Attempted to add contact" << handle << "twice!";
    return 0;
  }

  MsnContact *contact = 0;

  if ( handle == self_->handle() )
  {
    // it's ourselves.
    self_->setMemberships( (MsnMemberships)lists );
    self_->_setDisplayName( friendlyName );
    self_->setGuid( guid );
    self_->setNetwork( KMess::NetworkMsn ); // always, duh.
    contact = self_;

    QList<MsnGroup*> groups;
    foreach( QString groupId, groupIds )
    {
      MsnGroup *g = groups_.value( groupId );
      g->contacts_.append( self_ );
      groups << g;
    }

    contact->setGroups( groups );
    model_->contactAdd( contact );
    return contact;
  }
  
  if ( unknownContacts_.contains( handle ) )
  {
    contact = unknownContacts_.take( handle );
  }
  else 
  {
    // Create the new contact
    contact = new MsnContact( handle, friendlyName, lists, QList<MsnGroup*>(), guid );
  }

  contact->setMemberships( (MsnMemberships)lists );
  contact->_setDisplayName( friendlyName );
  contact->setGuid( guid );
  contact->setNetwork( network );

  QList<MsnGroup*> groups;
  foreach( QString groupId, groupIds )
  {
    MsnGroup *g = groups_.value( groupId );
    g->contacts_.append( self_ );
    
    groups << g;
  }

  contact->setGroups( groups );
  
  contacts_.insert( handle.toLower(), contact );

  debug() << "Adding to list contact" << *contact << "within groups" << groupIds;

  // Also add it to the model
  model_->contactAdd( contact );
  
  return contact;
}



/**
 * @brief Copy a contact into a group.
 *
 * This is called whenever the server notifies us that a group now contains
 * another contact.
 *
 * Usually, those notifications happen in two cases:
 * 1) While logging in, the list of already known groups is registered.
 *    This method gets called once for each group to register.
 * 2) In response to the creation of a group by the user, the server
 *    tells us that the new group is now available.
 *
 * @param handle Contact to add to group
 * @param groupId Group where to add the contact to
 */
void ListsManager::contactAddToGroup( const QString& handle, const QString& groupId )
{
  // Verify that the contact exists
  MsnContact* currentContact = contact( handle );
  KMESS_ASSERT( currentContact );
  if( ! currentContact )
  {
    warning() << "Attempted to add unknown contact" << handle << "to group" << groupId << "!";
    return;
  }

  MsnGroup *newGroup = group( groupId );
  KMESS_ASSERT( newGroup );
  
  debug() << "Adding contact" << currentContact << "to group" << newGroup;

  QList<MsnGroup*> groups( currentContact->groups() );

  // Append the new group to the list only if it wasn't there already
  if( ! groups.contains( newGroup ) )
  {
    groups << newGroup;
    currentContact->setGroups( groups );
  }

  KMess::MsnGroup *g = groups_.value( groupId );
  if ( ! g->contacts_.contains( currentContact ) )
  {
    g->contacts_.append( currentContact );
  }

  // Update the model
  model_->contactMove( currentContact );
}




/**
 * @brief A contact that was previously removed from the FL has been readded.
 *
 * @param handle The contact that has been readded.
 */
void ListsManager::contactReadded( const QString &handle )
{
  MsnContact *currentContact = contact( handle );
  if ( ! currentContact )
  {
    warning() << "Attempted to re-add unknown contact" << handle;
    return;
  }

  // not much to do here really, just inform the model.
  model_->contactMove( currentContact );
}



/**
 * @brief Remove a contact from the forward list.
 *
 * Called when the server confirms that the removal of a contact from the forward
 * list has been successful. Instructs the model to move the contact into the
 * appropriate groups.
 *
 * @param handle Contact to remove from the forward list.
 */
void ListsManager::contactRemove( const QString &handle )
{
  MsnContact *currentContact = contact( handle );
  if ( ! currentContact )
  {
    warning() << "Attempted to remove unknown contact" << handle;
    return;
  }

  // remove the contact from all groups.
  currentContact->setGroups( QList<MsnGroup*>() );

  // trigger a move within the model - will cause the contact to be added
  // into either the Allowed or Removed special group.
  model_->contactMove( currentContact );

  debug() << "Contact" << handle << "removed from list.";
}



/**
 * @brief Remove a contact from a group.
 *
 * This is called whenever the server notifies us that a group does not contain
 * a certain contact anymore.
 *
 * @param handle Contact to remove from group
 * @param groupId Group to remove from the contact's groups
 */
void ListsManager::contactRemoveFromGroup( const QString& handle, const QString& groupId )
{
  // Verify that the contact exists
  MsnContact* currentContact = contact( handle );

  KMESS_ASSERT( currentContact );
  if( ! currentContact )
  {
    warning() << "Attempted to remove unknown contact" << handle << "from group" << groupId << "!";
    return;
  }

  MsnGroup *oldGroup = group( groupId );
  KMESS_ASSERT( oldGroup );
  
  debug() << "Removing contact" << currentContact << "from group" << oldGroup;

  QList<MsnGroup*> groups( currentContact->groups() );

  // Remove the group from the list
  if( groups.contains( oldGroup) )
  {
    groups.removeAll( oldGroup );
    currentContact->setGroups( groups );
  }

  MsnGroup *g = groups_.value( groupId );
  g->contacts_.removeAll( currentContact );

  // Update the model
  model_->contactMove( currentContact );
}



/**
 * @brief Register a group.
 *
 * Whenever the server notifies us of the presence of a new group,
 * this method is called.
 * Usually, those notifications happen in two cases:
 * 1) While logging in, the list of already known groups is registered.
 *    This method gets called once for each group to register.
 * 2) In response to the creation of a group by the user, the server
 *    tells us that the new group is now available.
 */
KMess::MsnGroup *ListsManager::groupAdd( const QString& id, const QString& name )
{
  if( groups_.contains( id ) )
  {
    warning() << "There already is a group with id:" << id;
    warning() << "Old group's name:" << groups_[ id ] << "- New group's name:" << name;
    return 0;
  }

  // Register the new group

  // Create the new contact
  MsnGroup* group = new MsnGroup( id, false, name );
  groups_.insert( id, group );

  // Also add it to the model
  model_->groupAdd( group );
  
  return group;
}



/**
 * @brief Delete a group.
 *
 * This method is called when the server confirms the deletion of a group.
 *
 * @param id ID of the group to remove
 */
void ListsManager::groupRemove( const QString& id )
{
  if( ! groups_.contains( id ) )
  {
    warning() << "There is no group with id:" << id;
    return;
  }

  // Also remove it from the model
  model_->groupRemove( groups_.value( id ) );

  // Delete the group
  MsnGroup *g = groups_.take( id );

  // bye bye!
  delete g;
}



/**
 * @brief Alter an existing group.
 *
 * This method is called when the server changes the name of a group.
 *
 * @param id The ID of the group to change
 * @param newName The new name for the group
 */
void ListsManager::groupRename( const QString& id, const QString& newName )
{
  if( ! groups_.contains( id ) )
  {
    warning() << "There is no group with id:" << id;
    return;
  }

  MsnGroup *group = groups_.value( id );

  // Avoid unnecessary updates
  if( group->name() == newName )
  {
    return;
  }

  group->setName( newName );

  // Signal the change to the model
  model_->groupUpdate( group );
}



/**
 * @brief Find a contact.
 *
 * This method is used to retrieve a contact object by its handle or GUID.
 * It can also retrieve the user's MsnContact if given the user's handle.
 *
 * If the contact is completely unknown (ie, has zero list memberships) and the create
 * parameter is true, then a new MsnContact instance will be created and returned. This instance
 * is considered an "unknown contact". If the user adds this contact to the contact list
 * then it becomes a "known contact". A common scenario is group chats where an unknown
 * contact is invited in.
 *
 * @param search The handle/GUID to search for
 * @param create If true, if an existing MsnContact instance for that handle cannot be found, creates and returns a new one.
 * @returns An MsnContact object, or 0 if it could not be found
 */
KMess::MsnContact* ListsManager::contact( const QString &search, bool create )
{
  KMESS_ASSERT( ! search.isEmpty() );

  // Firstly, strip out any network ID information.
  QString handle = search;

  if ( handle.contains(':') )
  {
    handle = handle.section( ':', 1 );
  }

  // strip out machine guid.
  handle = handle.section( ';', 0, 0 );

  // match by handle?`
  if( contacts_.contains( handle.toLower() ) )
  {
    return contacts_.value( handle.toLower() );
  }

  // no match from the CL...is it "self"? ie, us?
  if( handle == self_->handle() )
  {
    return self_;
  }
  
  // is it an unknown contact?
  if ( unknownContacts_.contains( handle.toLower() ) )
  {
    return unknownContacts_.value( handle.toLower() );
  }
  
  // no luck searching by handle. search by GUID now.
  if ( handle == session_->self()->guid() )
  {
    return self_;
  }
  
  foreach( MsnContact* contact, contacts_ )
  {
    if( contact->guid() == handle )
    {
      return contact;
    }
  }

  // completely unseen before. should we create a new instance for it?
  if ( ! create )
  {
    // no - return failure.
    return 0;
  }

  // Contact is completely unknown. Create a new MsnContact instance.
  // This is a completely empty instance and has zero list memberships - which makes it "unknown".
  MsnContact *newContact = new MsnContact( search );
  unknownContacts_.insert( newContact->handle().toLower(), newContact );

  return newContact;
}



/**
 * @brief Get the entire list of contacts.
 *
 * @returns The QList containing all known contacts
 */
const IndexedContactsList ListsManager::contacts() const
{
  return contacts_;
}



/**
 * @brief Find a group.
 *
 * This method is used to retrieve a group by its id.
 *
 * @param id The id to search for
 * @returns The group, or 0 if it could not be found
 */
MsnGroup* ListsManager::group( const QString &id ) const
{
  IndexedGroupsList groups = groups_;
  groups.unite( model_->specialGroups() );
  return groups.value( id, 0 );
}



/**
 * @brief Get the entire list of groups.
 *
 * @returns The QHash containing all known groups
 */
const IndexedGroupsList ListsManager::groups() const
{
  IndexedGroupsList groups = groups_;
  groups.unite( model_->specialGroups() );
  return groups;
}



/**
 * @brief Retrieve the contact list model.
 *
 * This can be used to access the QAbstractItemModel representing the contact list.
 *
 * @return The contact list model object
 */
ContactListModel* ListsManager::model() const
{
  return model_;
}



/**
 * Return the user's representation as an MsnContact.
 *
 * @return MsnContact*
 */
Self* ListsManager::self() const
{
  return self_;
}
