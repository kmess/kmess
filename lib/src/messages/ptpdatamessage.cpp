/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Gregg Edghill <gregg.edghill at gmail.com>                *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file ptpdatamessage.cpp
 * @brief Message containing data for a p2p application
 */

#include <KMess/PtpDataMessage>

using namespace KMess;



/**
 * Default constructor
 */
PtpDataMessage::PtpDataMessage()
: Message( 0, DirectionOutgoing, 0, ConfirmPtpData, PtpDataMessageType )
{
  Message::setField(QLatin1String("MIME-Version"), QLatin1String("1.0"));
  Message::setContentType(QLatin1String("application/x-msnmsgrp2p"));
}



/**
* Constructor.
*
* Use it to create an app data message.
*/
PtpDataMessage::PtpDataMessage( MsnChat* chat, MsnContact* peer, MessageDirection direction )
: Message( peer, direction, chat, ConfirmPtpData, PtpDataMessageType )
{
  Message::setField(QLatin1String("MIME-Version"), QLatin1String("1.0"));
  Message::setContentType(QLatin1String("application/x-msnmsgrp2p"));
}



/**
* Copy constructor from a plain Message.
*
* @param other Message to clone
*/
PtpDataMessage::PtpDataMessage( const Message &other )
: Message( other )
{
  // Set the base class properties
  d_ptr->confirmationMode_ = ConfirmPtpData;
  d_ptr->messageType_ = PtpDataMessageType;
}



/**
* Destructor.
*/
PtpDataMessage::~PtpDataMessage()
{
}



/**
* Gets the destination address of the ptp data message.
*/
const QString PtpDataMessage::destination() const
{
  return Message::field(QLatin1String("P2P-Dest"));
}



/**
* Gets the message body data.
*/
QByteArray PtpDataMessage::rawData() const
{
  return d_ptr->body_;
}



/**
* Gets the source address of the ptp data message.
*/
const QString PtpDataMessage::source() const
{
  return Message::field(QLatin1String("P2P-Src"));
}



/**
* Sets the message body data.
*/
void PtpDataMessage::setRawData(const QByteArray& rawdatabytearray)
{
  d_ptr->body_ = rawdatabytearray;
}



/**
* Sets the source address of the ptp data message.
*/
void PtpDataMessage::setDestination(const QString& destination)
{
  Message::setField(QLatin1String("P2P-Dest"), destination);
}



/**
* Sets the source address of the ptp data message.
*/
void PtpDataMessage::setSource(const QString& source)
{
  Message::setField(QLatin1String("P2P-Src"), source);
}

