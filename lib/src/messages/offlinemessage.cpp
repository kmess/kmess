/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Valerio Pilo <valerio@kmess.org>                          *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file offlinemessage.cpp
 * @brief Offline message
 */

#include "debug/libkmessdebug.h"

#include <KMess/MsnContact>
#include <KMess/MsnSession>
#include <KMess/ContactList>
#include <KMess/OfflineMessage>
#include <KMess/Self>

#include "../utils/utils_internal.h"


using namespace KMess;


namespace KMess
{
  /// The private fields of the OfflineMessage clas.
  class OfflineMessagePrivate : public MessagePrivate
  {
    public:
      OfflineMessagePrivate() {};
      OfflineMessagePrivate( const MessagePrivate &other ) : MessagePrivate( other ) {}
      virtual MessagePrivate *clone()
      {
        return new OfflineMessagePrivate( *this );
      }

    public:
      QString      recipient;           ///< Email of the message recipient
      QString      sender;              ///< Email of the message sender
      quint32      sequenceNumber;      ///< Offline Message transfer sequence number
  };
};


/**
 * Default constructor.
 */
OfflineMessage::OfflineMessage()
: Message( 0, DirectionOutgoing, 0, ConfirmNever, OfflineMessageType, QString(), new OfflineMessagePrivate )
{
}



/**
 * Constructor.
 *
 * Use it to create an offline message.
 */
OfflineMessage::OfflineMessage( MsnChat* chat, MsnContact* peer, const QString body )
: Message( peer, DirectionOutgoing, chat, ConfirmNever, OfflineMessageType, QString(), new OfflineMessagePrivate )
{
  K_D(OfflineMessage);

  d->isValid_ = true;
  d->body_ = body.toUtf8();
  d->sequenceNumber = 0;
}



/**
 * Copy constructor from a plain Message.
 *
 * @param other Message to clone
 */
OfflineMessage::OfflineMessage( const Message &other )
: Message( other )
{
  if ( other.type() == OfflineMessageType )
  {
    return;
  }

  d_ptr = new OfflineMessagePrivate( *( d_ptr.data() ) );

  K_D(OfflineMessage);

  // Offline messages have their body encoded in base64. Message will however decode it for us.

  d->sender    = KMessInternal::InternalUtils::decodeRFC2047String( field( "From" ) );
  d->recipient = KMessInternal::InternalUtils::decodeRFC2047String( field( "To" ) );

  d->sequenceNumber = field( "X-OIM-Sequence-Num" ).toUInt();

  // You must call identifyPeers() to validate the message peers
  d->isValid_ = false;
}



/**
 * Destructor.
 */
OfflineMessage::~OfflineMessage()
{
}



/**
 * Identify who are the recipient and sender of this message.
 *
 * This uses a MsnSession object to identify the peer contact and the user.
 *
 * @param session The session object
 */
void OfflineMessage::identifyPeers( MsnSession *session )
{
  K_D(OfflineMessage);

  d->isValid_ = false;

  if( d->sender.isEmpty() && d->recipient.isEmpty() )
  {
    warning() << "Invalid offline message peers!";
    warning() << "Sender:" << d->sender << "recipients:" << d->recipient;
    return;
  }

  const QString sessionHandle( session->self()->handle() );

  if( d->sender == sessionHandle )
  {
    setPeer( session->contactList()->contact( d->recipient ) );
  }
  else
  if( d->recipient == sessionHandle )
  {
    setPeer( session->contactList()->contact( d->sender ) );
  }
  else
  {
    warning() << "Contacts mismatch! The user is neither the sender or the recipient of the message!";
    warning() << "Sender:" << d->sender << "Recipient:" << d->recipient;
    return;
  }


  d->isValid_ = ( d->peer_ != 0 );
}



/**
 * Get the message the contact sent, or we are about to send.
 */
const QString OfflineMessage::message() const
{
  K_D(OfflineMessage);

  return QString::fromUtf8( d->body_ );
}



/**
 * Get the email address of the peer who is receiving the offline message.
 *
 * @return QString
 */
const QString& OfflineMessage::recipient() const
{
  K_D(OfflineMessage);

  return d->recipient;
}



/**
 * Get the email address of the peer who is sending the offline message.
 *
 * @return QString
 */
const QString& OfflineMessage::sender() const
{
  K_D(OfflineMessage);

  return d->sender;
}



/**
 * Return the sequence number assigned to this message.
 *
 * @return quint32
 */
quint32 OfflineMessage::sequenceNumber() const
{
  K_D(OfflineMessage);

  return d->sequenceNumber;
}



/**
 * Set the message we are about to send.
 */
void OfflineMessage::setMessage( const QString &msg )
{
  K_D(OfflineMessage);

  d->body_ = msg.toUtf8();
}



/**
 * Change the recipient contact of this message.
 *
 * @param recipient The recipient MsnContact
 */
void OfflineMessage::setRecipient( MsnContact* recipient )
{
  K_D(OfflineMessage);

  d->recipient = recipient->handle();

  if( d->direction_ == DirectionOutgoing )
  {
    setPeer( recipient );
  }
}



/**
 * Change the sender contact of this message.
 *
 * @param sender The sender MsnContact
 */
void OfflineMessage::setSender( MsnContact *sender )
{
  K_D(OfflineMessage);

  d->sender = sender->handle();

  if( d->direction_ == DirectionIncoming )
  {
    setPeer( sender );
  }
}



/**
 * Change the message's sequence number
 *
 * @param number The new sequence number
 */
void OfflineMessage::setSequenceNumber( const quint32 number )
{
  K_D(OfflineMessage);

  d->sequenceNumber = number;
}


