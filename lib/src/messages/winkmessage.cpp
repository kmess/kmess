/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Valerio Pilo <valerio@kmess.org>                          *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file winkmessage.cpp
 * @brief Message with a wink
 */

#include <KMess/WinkMessage>

using namespace KMess;


namespace KMess
{
  /// Private fields of the WinkMessage class.
  class WinkMessagePrivate : public MessagePrivate
  {
    public:
      WinkMessagePrivate() {}
      WinkMessagePrivate( const MessagePrivate &other ) : MessagePrivate( other ) {}
      virtual MessagePrivate *clone()
      {
        return new WinkMessagePrivate( *this );
      }
      
      QString winkObjectString;
  };
};


/**
 * Default constructor
 */
WinkMessage::WinkMessage()
: Message( 0, DirectionOutgoing, 0, ConfirmNever, WinkMessageType, QString(), new WinkMessagePrivate )
{
}



/**
 * Constructor.
 *
 * Use it to create a wink message.
 */
WinkMessage::WinkMessage( MsnChat* chat, MsnContact* peer )
: Message( peer, DirectionOutgoing, chat, ConfirmNever, WinkMessageType, QString(), new WinkMessagePrivate )
{
}



/**
 * Copy constructor from a plain Message.
 *
 * @param other Message to clone
 */
WinkMessage::WinkMessage( const Message &other )
: Message( other )
{
  if ( other.type() == WinkMessageType )
  {
    return;
  }
  
  d_ptr = new WinkMessagePrivate( *( d_ptr.data() ) );
}



/**
 * Destructor.
 */
WinkMessage::~WinkMessage()
{
}



/**
 * Set the wink data in this object.
 */
void WinkMessage::setWinkObjectString( const QString &msnobject )
{
  K_D(WinkMessage);
  
  d->winkObjectString = msnobject;
}


/**
 * Get the wink data from this object.
 */
const QString &WinkMessage::winkObjectString() const
{
  K_D(WinkMessage);
  
  return d->winkObjectString;
}
