/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Mike K. Bennett <mkb137b@hotmail.com>                     *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file message_p.cpp
 */

#include "debug/libkmessdebug.h"

#include <KMess/Message>

#include <KMess/NetworkGlobals>

#include <QByteArray>
#include <QRegExp>
#include <QStringList>
#include <QTextCodec>
#include <QDateTime>

namespace KMess
{

// The constructor: use references to the parent Message, to allow Message subclasses
// to directly manipulate these two members
MessagePrivate::MessagePrivate()
: chat_( 0 )
, confirmationMode_ ( ConfirmNever )
, direction_ ( DirectionOutgoing )
, isValid_( false )
, messageType_ ( InvalidMessageType )
, peer_( 0 )
, dateTime_ ( QDateTime::currentDateTime() )
{
}



/**
 * Copy constructor for another MessagePrivate instance.
 */
MessagePrivate::MessagePrivate( const MessagePrivate &other )
 : QSharedData( other )
 , body_( other.body_ )
 , header_ ( other.header_ )
 , chat_ ( other.chat_ )
 , confirmationMode_ ( other.confirmationMode_ )
 , direction_ ( other.direction_ )
 , isValid_ ( other.isValid_ )
 , messageType_ ( other.messageType_ )
 , peer_ ( other.peer_ )
 , dateTime_ ( other.dateTime_ )
{
}



/**
 * Creates a clone of this private data.
 */
MessagePrivate *MessagePrivate::clone()
{
  return new MessagePrivate( *this );
}



/*
// The constructor that parses a message
MessagePrivate::MessagePrivate(const QString &message)
{
#ifdef KMESSDEBUG_MESSAGEPRIVATE
  debug() << "Parsing the sub message";
#endif
  parseMessage( message );
}



// The constructor that parses a message
MessagePrivate::MessagePrivate(const QByteArray &message)
{
  // we get the header without the binary data that comes after
  int nullPos = message.indexOf('\0');
  int endMime = message.indexOf("\r\n\r\n");
  QString messageHeader( QString::fromUtf8(message.data(), (nullPos == -1) ? endMime : nullPos ) );

  // Regexps to parse the header
  QRegExp rx1("Content-Type: ([A-Za-z0-9$/!*\\-]*)");
  QRegExp rx2("Message-ID: ([^\r\n]+)");

  // Get the Content-Type header
  QString contentType;
  if( rx1.indexIn( messageHeader ) != -1 )
  {
    contentType = rx1.cap(1);
  }

  // Some messages should be preserved as binary, so take care of that.
  // - p2p messages have a 48 byte header.
  // - multi-packet messages can be split between a double byte utf-8 char.
  bool isP2P         = ( contentType == "application/x-msnmsgrp2p" );
  bool isMultiPacket = ( rx2.indexIn( messageHeader ) != -1 );
  if( isP2P || isMultiPacket )
  {
#ifdef KMESSDEBUG_MESSAGEPRIVATE
    if( isP2P )
    {
      debug() << "Parsing the message, extracting binary p2p body.";
    }
    else if( isMultiPacket )
    {
      debug() << "Parsing the message, preserving multi-packet body as binary.";
    }
#endif

    int bodyStart = endMime + 4; // 2 newlines
    int bodySize  = message.size() - bodyStart;

    // Extract the Mime fields from the message
    QString mimeData( QString::fromUtf8( message.data(), bodyStart ) );
    parseMessage( mimeData );

    // Extract the binary data from the message.
    // Copy to our own managed copy.
    binaryBody_ = QByteArray( message.data() + bodyStart, bodySize );
  }
  else
  {
#ifdef KMESSDEBUG_MESSAGEPRIVATE
    debug() << "Parsing the message.";
#endif

    // This is a normal plain-text message.
    // The fromUtf8 call is required to convert Unicode characters properly! (like Chinese)
    parseMessage( QString::fromUtf8( message.data(), message.size()) );
  }
}
*/


/*
// The copy constructor
MessagePrivate::MessagePrivate(const MessagePrivate& other)
{
#ifdef KMESSDEBUG_MESSAGEPRIVATE
  debug() << "copy constructor";
#endif

  // Get the body of the other message
  header_     = other.header_;
  values_     = other.values_;
  body_       = other.body_;
  binaryBody_ = other.binaryBody_;

  // QValueList is implicitly shared, so the assign operation
  // does not create another copy. It uses "copy on write".

  // QByteArray is explicitly shared, but it's not a problem here,
  // since we don't modify the array data of binaryBody_.

#ifdef KMESSTEST
  // Make sure the ascii-zero characters are copied correctly:
  KMESS_ASSERT( binaryBody_.size() == other.binaryBody_.size() );
  KMESS_ASSERT( header_.size()     == other.header_.size() );
#endif
}*/



// The destructor
MessagePrivate::~MessagePrivate()
{
}


// Change a field, or add it if not existent
void MessagePrivate::setField( const QString& field, const QString& value )
{
  header_[ field ] = value;
}



// Return the message fields as a big string
QByteArray MessagePrivate::getFields() const
{
  QByteArray message;

  // Get the fields and values
  FieldMapIterator it( header_ );
  while( it.hasNext() )
  {
    it.next();

    message.append( it.key().toUtf8() + ": " + it.value().toUtf8() + "\r\n" );
  }

  return message;
}



// Return the message body as a string
QByteArray MessagePrivate::getBody() const
{
  QByteArray message;

  // If the content is not in plain text, decode it
  if( hasField( "Content-Transfer-Encoding" ) )
  {
    if( getValue( "Content-Transfer-Encoding" ) == "base64" )
    {
      message.append( QByteArray::fromBase64( body_ ) );
    }
    else
    {
      warning() << "Unknown transfer encoding" << getValue( "Content-Transfer-Encoding" ) << "!";
      message += body_;
    }
  }
  else
  {
    message.append( body_ );
  }

  return message;
}



// Return the entire message as a big string
QByteArray MessagePrivate::getMessage() const
{
  QByteArray message( getFields() + "\r\n" + getBody() );

  // for messages with an empty body must use two \r\n sequences.
  // This fixes typing notifications not showing in WLM 2009.
  if( body_.isEmpty() )
  {
    message += "\r\n";
  }

  return message;
}



// Get one parameter of a value that has multiple parameters
QString MessagePrivate::getSubValue( const QString& field, const QString& subField ) const
{
  int left, right;

  // Get the value referred to by the field
  QString value( getValue( field ) );

  if( value.isEmpty() )
  {
    return QString();
  }

  // If the subfield isn't specified, then get whatever is at the start of the message until the
  //  first semicolon or the end of the line
  if ( subField.isEmpty() )
  {
    left = 0;
  }
  else
  {
    // The left of the parameter is "subField=", the right is the next semicolon or the end of the line
    left = value.indexOf( subField + '=' );
    if ( left >= 0 )
    {
      left += subField.length() + 1;
    }
  }

  if ( left >= 0 )
  {
    right = value.indexOf( ";", left );
    if ( right < 0 )
    {
      right = value.length();
    }

    // Get the parameter
    return value.mid( left, ( right - left ) );
  }

  return QString();
}



// Get a value given a field
QString MessagePrivate::getValue( const QString& field ) const
{
  if( ! header_.contains( field ) )
  {
    warning() << "This message contained no field" << field << "!";
  }

  return header_.value( field, QString() );
}



// Test whether a given field exists in the message header
bool MessagePrivate::hasField( const QString& field ) const
{
  return header_.contains( field );
}


// Parse the message into type, body, and fields and values
void MessagePrivate::parseMessage( const QByteArray& message, bool &ok )
{
  QByteArray   head;
  QString      field, value, lastField;
  QStringList  lines;

  if( message.isEmpty() )
  {
    ok = false;
    return;
  }

  // Split the message into head and body
  // Split the head into its various lines
  splitMessage( head, body_, message );
  splitHead( lines, head );

  if( lines.isEmpty() )
  {
    ok = false;
    return;
  }

  // Split all the lines into field and value
  for( int i = 0; i < lines.count(); i++ )
  {
    if( lines[i].startsWith('\t') )
    {
#ifdef KMESSTEST
      KMESS_ASSERT( lastField == "Received" );
#endif
      // Addition to parse email headers, with Received: headers that continue at
      // the next line
      header_[ lastField ] += "\r\n" + lines[ i ].mid( 2 );
    }
    else if( !lines[i].isEmpty() )
    {
      splitLine( field, value, lines[i] );
      if ( ! field.isEmpty() )
      {
        // Add the fields and values to the lists
        setField( field, value );
      }
      lastField = field;
    }
  }

  ok = true;
}



// Set the body of the message. The QString is converted
// to a UTF8-encoded QByteArray.
void MessagePrivate::setBody( const QString &body )
{
  body_ = body.toUtf8();
}



// Split a line between field and value
void MessagePrivate::splitLine(QString& field, QString& value, const QString& line) const
{
  int index;
  index = line.indexOf(":");
  if ( index >= 0 )
  {
    field = line.left( index );
    if ( ( index + 1 ) < (int)line.length() )
    {
      value = line.right( line.length() - index - 2 );
    }
    else
    {
      value.clear();
    }
  }
  else
  {
    warning() << "Couldn't split line '" << line << "'";
  }
}



// Split the message head into components and store it in the string list
void MessagePrivate::splitHead(QStringList& stringList, const QString& head) const
{
  stringList = head.split( "\n" );

  for( int i = 0; i < stringList.count(); i++ )
  {
    stringList[i] = stringList[i].trimmed();
  }
}



// Split a message into head and body
void MessagePrivate::splitMessage( QByteArray& head, QByteArray& body, const QByteArray& message ) const
{
  int index;
  int count = 4;


  // The message is split into head and body at "\r\n\r\n"
  index = message.indexOf( "\r\n\r\n" );
  if( index < 0 )
  {
    index = message.indexOf( "\n\n" );
    count = 2;
  }

  if( index < 0 )
  {
    head = message;
    body.clear();
  }
  else
  {
    // Keep one newline (\r\n or \n) at the end
    head = message.left( index + (count / 2) );
    // Everything from the end of the newlines
    body = message.mid( index + count );
  }
}

}; // namespace KMess
