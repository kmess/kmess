/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Valerio Pilo <valerio@kmess.org>                          *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file voicemessage.cpp
 * @brief Message with a recorded audio clip
 */

#include <KMess/VoiceMessage>

using namespace KMess;



/**
 * Default constructor.
 */
VoiceMessage::VoiceMessage()
: Message( 0, DirectionOutgoing, 0, ConfirmNever, VoiceMessageType )
{
}



/**
 * Constructor.
 *
 * Use it to create a voice clip message.
 */
VoiceMessage::VoiceMessage( MsnChat* chat, MsnContact* peer )
: Message( peer, DirectionOutgoing, chat, ConfirmNever, VoiceMessageType )
{
}



/**
 * Copy constructor from a plain Message.
 *
 * @param other Message to clone
 */
VoiceMessage::VoiceMessage( const Message &other )
: Message( other )
{
}



/**
 * Destructor.
 */
VoiceMessage::~VoiceMessage()
{
  //delete d;
}


