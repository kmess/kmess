/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Valerio Pilo <valerio@kmess.org>                          *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file emailmessage.cpp
 * @brief Message with email data
 */

#include "debug/libkmessdebug.h"

#include <KMess/EmailMessage>

using namespace KMess;



/**
 * Default constructor.
 */
EmailMessage::EmailMessage()
: Message( 0, DirectionOutgoing, 0, ConfirmNever, EmailMessageType )
{
}



/**
 * Copy constructor from a plain Message.
 *
 * @param other Message to clone
 */
EmailMessage::EmailMessage( const Message &other )
: Message( other )
{
  if ( other.type() == EmailMessageType )
  {
    // nothing to do here. dptr already cloned in Message copy ctor.
    return;
  }

  K_D(Message);

  // Set the base class properties
  d->confirmationMode_ = ConfirmAlways;
  d->messageType_ = EmailMessageType;

  // The body contains more MIME headers, so parse it as if it were a new
  // message; this copies the headers in the body into the message header
  // data. With this, the fields can be accessed by using the field()
  // method.
  bool ok;
  d->parseMessage( d->body_, ok );

  if( ok )
  {
    d->body_.clear();
  }
}



/**
 * Destructor.
 */
EmailMessage::~EmailMessage()
{
}


