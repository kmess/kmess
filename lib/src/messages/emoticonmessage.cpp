/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Valerio Pilo <valerio@kmess.org>                          *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file emoticonmessage.cpp
 * @brief A message containing emoticon definitions for a chat message
 */

#include "debug/libkmessdebug.h"

#include <KMess/EmoticonMessage>

#include <QStringList>


using namespace KMess;

namespace KMess
{
  class EmoticonMessagePrivate : public MessagePrivate
  {
    public:
      EmoticonMessagePrivate() {}
      EmoticonMessagePrivate( const MessagePrivate &other ) : MessagePrivate( other ) {}
      virtual MessagePrivate *clone()
      {
        return new EmoticonMessagePrivate( *this );
      }

    public:
      /// Map of emoticons
      MsnObjectMap           emoticons_;
  };
};

/**
 * Default constructor.
 */
EmoticonMessage::EmoticonMessage()
: Message(0, DirectionOutgoing, 0, ConfirmFailure, EmoticonMessageType, QString(), new EmoticonMessagePrivate )
{
}



/**
 * Constructor.
 *
 * It should not be needed to create this kind of message: just create
 * a TextMessage, call setEmoticons() on it, then send it.
 */
EmoticonMessage::EmoticonMessage( MsnChat* chat, MsnContact* peer )
: Message( peer, DirectionOutgoing, chat, ConfirmFailure, EmoticonMessageType, QString(), new EmoticonMessagePrivate )
{
}



/**
 * Copy constructor from a plain Message.
 *
 * @param other Message to clone
 */
EmoticonMessage::EmoticonMessage( const Message &other )
: Message( other )
{
  if ( other.type() == EmoticonMessageType )
  {
    // nothing to do here. dptr already cloned in Message copy ctor.
    return;
  }

  d_ptr = new EmoticonMessagePrivate( *( d_ptr.data() ) );
  K_D(EmoticonMessage);

  // Set the base class properties
  d->confirmationMode_ = ConfirmFailure;
  d->messageType_ = EmoticonMessageType;


  // Emoticon data consists of a tab-separated list of pairs,
  // each formed by an emoticon definition, another tab, and the related msn object:
  // [Shortcut] TAB [MSN Object] TAB [Shortcut] TAB [MSN Object] TAB ...
  // |first emoticon|     |second emoticon|
  QString emoticonCode;
  QString msnObjectData;

  if( d->body_.trimmed().isEmpty() )
    return;

  // Extract emoticon definitions and msn objects
  const QList<QByteArray> msnObjects( d->body_.trimmed().split( '\t' ) );

  QListIterator<QByteArray> it( msnObjects );
  while( it.hasNext() )
  {
    emoticonCode = it.next();

    // If the number of fields is odd, this custom emoticons list contains errors
    if( ! it.hasNext() )
    {
      warning() << "Ignoring emoticon message with an unexpected format: odd number of fields!";
      break;
    }

    msnObjectData = it.next();

    // Perform a simple syntax check
    if( msnObjectData.length() < 20 )
    {
      warning() << "Ignoring emoticon message with an unexpected format: object too small!";
      break;
    }

    debug() << "Adding emoticon with code" << emoticonCode;

    // Store the emoticon code for the contact
    d->emoticons_[ emoticonCode ] = msnObjectData;
  }
}



/**
 * Destructor.
 */
EmoticonMessage::~EmoticonMessage()
{
}



/**
 * Add an emoticon to the mapping.
 *
 *
 * @param shortcut Text shortcut. The shortcuts are truncated to seven
 *                 characters to maintain compatibility with Live Messenger.
 * @param msnObject The corresponding emoticon's MsnObject text representation.
 */
void EmoticonMessage::addEmoticon( const QString &shortcut, const QString msnObject )
{
  K_D(EmoticonMessage);

  // Verify the shortcut
  if( shortcut.isEmpty() )
  {
    warning() << "Cannot add emoticon to EmoticonMessage: no shortcut selected!";
    return;
  }

  // Just a little check on the object
  if( msnObject.length() < 20 )
  {
    warning() << "Cannot add emoticon to EmoticonMessage: object too small!";
    return;
  }

  d->emoticons_[ shortcut.left( shortcutTruncateLength ) ] = msnObject;
}



/**
 * Return the shortcuts to emoticon mapping.
 *
 * The map contains the emoticons which will be found in another message's text.
 * The mapping is done by shortcut, each emoticon is represented with an MsnObject.
 *
 * @see MsnObject
 * @return MsnObjectMap
 */
const MsnObjectMap& EmoticonMessage::emoticons() const
{
  K_D(EmoticonMessage);
  return d->emoticons_;
}



/**
 * Change all the shortcuts to emoticon mappings.
 *
 * @param emoticons The new emoticon map
 */
void EmoticonMessage::setEmoticons( const MsnObjectMap& emoticons )
{
  K_D(EmoticonMessage);
  d->emoticons_ = emoticons;
}
