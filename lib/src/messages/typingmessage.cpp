/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Valerio Pilo <valerio@kmess.org>                          *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file typingmessage.cpp
 * @brief An "user is typing" message
 */

#include <KMess/TypingMessage>
#include <KMess/MsnContact>

using namespace KMess;


namespace KMess
{
  /// The private properties of the TypingMessage class
  class TypingMessagePrivate : public MessagePrivate
  {
    public:
      TypingMessagePrivate() {}
      TypingMessagePrivate( const MessagePrivate &other ) : MessagePrivate( other ) {}
      virtual MessagePrivate *clone()
      {
        return new TypingMessagePrivate( *this );
      }
      
      MsnContact* contact;    ///< Contact who is typing
      QTime       time;       ///< Time of message creation (both when receiving or sending)
  };
};


/**
 * Default constructor.
 */
TypingMessage::TypingMessage()
: Message( 0, DirectionOutgoing, 0, ConfirmFailure, TypingMessageType, "text/x-msmsgscontrol", new TypingMessagePrivate )
{
}



/**
 * Constructor.
 *
 * Use it to create an "User is typing" message.
 */
TypingMessage::TypingMessage( MsnChat* chat, MsnContact* peer )
: Message( peer, DirectionOutgoing, chat, ConfirmFailure, TypingMessageType, "text/x-msmsgscontrol", new TypingMessagePrivate )
{
  K_D(TypingMessage);
  
  d->contact = 0; // TODO: Use peer_ if incoming, Me if outgoing
  d->time.start();
};



/**
 * Copy constructor from a plain Message.
 *
 * @param other Message to clone
 */
TypingMessage::TypingMessage( const Message &other )
: Message( other )
{
  if ( other.type() == TypingMessageType )
  {
    return;
  }
  
  d_ptr = new TypingMessagePrivate( *( d_ptr.data() ) );
  
  K_D(TypingMessage);  

  // Set the base class properties
  d->confirmationMode_ = ConfirmFailure;
  d->messageType_ = TypingMessageType;

  const QString &typingContact( other.field( "TypingUser", QString() ) );

  // FIXME Check if typingContact and peer_ are the same
  Q_UNUSED( typingContact );

  d->time.start();
}



/**
 * Destructor.
 */
TypingMessage::~TypingMessage()
{
}



/**
 * Return whether the typing message has expired.
 *
 * The time used to calculate the expiration is based on the message
 * creation time. You can access that value directly, with time().
 * The expiration time is the same of Live Messenger.
 *
 * @return bool
 */
bool TypingMessage::isExpired() const
{
  K_D(TypingMessage);
  
  return ( d->time.elapsed() > expirationTime );
}



/**
 * Return the time passed since creation of the message.
 *
 * The message expires in a certain amount of seconds (see the 'expirationTime'
 * static class value and isExpired() ).
 * With this method the expiration time can be manipulated (and if needed, reset).
 *
 * @see expirationTime
 * @see isExpired()
 * @return QTime
 */
QTime& TypingMessage::time()
{
  K_D(TypingMessage);
  
  return d->time;
}



/**
 * Return the contact who is currently typing.
 *
 * @return MsnContact
 */
MsnContact* TypingMessage::contact() const
{
  K_D(TypingMessage);
  
  return d->contact;
}



/**
 * Set required fields for this TypingMessage.
 */
void TypingMessage::prepare()
{
  K_D(TypingMessage);
  
  setField( "TypingUser", d->peer_->handle() );
}
