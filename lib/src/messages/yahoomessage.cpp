/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Adam Goossens <adam@kmess.org>                            *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file yahoomessage.cpp
 * @brief Message containing Yahoo! data
 */

#include <KMess/YahooMessage>

using namespace KMess;


class YahooMessagePrivate : public MessagePrivate
{
  public:
    YahooMessagePrivate() {}
    YahooMessagePrivate( const MessagePrivate &other ) : MessagePrivate( other ) {}

    virtual MessagePrivate *clone()
    {
      return new YahooMessagePrivate( *this );
    }

  public:
    Message innerMessage;
    QString sourceHandle;
    QString destHandle;
};
  
/**
 * Default constructor.
 */
YahooMessage::YahooMessage()
: Message( 0, DirectionIncoming, 0, ConfirmNever, YahooMessageType, QString(), new YahooMessagePrivate )
{
}



/**
 * Constructor.
 *
 * Use it to create a nudge message.
 */
YahooMessage::YahooMessage( MsnChat* chat, MsnContact* peer )
: Message( peer, DirectionOutgoing, chat, ConfirmNever, NudgeMessageType, QString() )
{
}



/**
 * Copy constructor from a plain Message.
 *
 * @param other Message to clone
 */
YahooMessage::YahooMessage( const Message &other )
: Message( other )
{
  if ( other.type() == YahooMessageType )
  {
    return;
  }
  
  d_ptr = new YahooMessagePrivate( *( d_ptr.data() ) );
  
  K_D( YahooMessage );
  d->messageType_ = YahooMessageType;
  d->innerMessage = other;
}



/**
 * Destructor.
 */
YahooMessage::~YahooMessage()
{
  // delete d;
}



/**
 * Retrieve the inner message.
 */
KMess::Message YahooMessage::innerMessage() const
{
  K_D(YahooMessage);
  
  return d->innerMessage;
}



/**
 * Sets the inner message held by this Yahoo! message.
 *
 * The inner message is what is actually sent to the contact.
 * @param other The inner message.
 */
void YahooMessage::setInnerMessage( Message other )
{
  K_D(YahooMessage);
  
  d->innerMessage = other;
  d->isValid_ = other.isValid();
}



/**
 * The source handle is the user that sent the message.
 * @param handle Handle of the originating user.
 */
void YahooMessage::setSourceHandle( const QString &handle )
{
  K_D(YahooMessage);
  d->sourceHandle = handle;
}



/**
 * The destination handle is the destination user.
 * @param handle The destination user's handle.
 */
void YahooMessage::setDestHandle( const QString &handle )
{
  K_D(YahooMessage);
  d->destHandle = handle;
}



/**
 * Get the source user's handle
 */
QString YahooMessage::sourceHandle() const
{
  K_D(YahooMessage);
  return d->sourceHandle;
}



/**
 * Get the destination user's handle.
 */
QString YahooMessage::destHandle() const
{
  K_D(YahooMessage);
  return d->destHandle;
}
