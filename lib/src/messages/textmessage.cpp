/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Valerio Pilo <valerio@kmess.org>                          *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file textmessage.cpp
 * @brief A text chat message
 */

#include "debug/libkmessdebug.h"

#include <KMess/MsnAlgorithms>
#include <KMess/TextMessage>

#include <QUrl>


using namespace KMess;


namespace KMess
{
  class TextMessagePrivate : public MessagePrivate
  {
    public:
      TextMessagePrivate() {}
      TextMessagePrivate( const MessagePrivate &other ) : MessagePrivate( other ) {}

      virtual MessagePrivate *clone()
      {
        return new TextMessagePrivate( *this );
      }

    public:
      QColor        color;      ///< Font color to apply to the message text
      QFont         font;       ///< Font and attributes of the message text
      MsnObjectMap  emoticons;  ///< Map of emoticons used in the message
      QString       peerName;   ///< Custom friendly name of the peer contact for this message
  };
};


/**
 * Default constructor.
 */
TextMessage::TextMessage()
: Message( 0, DirectionOutgoing, 0, ConfirmAlways, TextMessageType, "text/plain; charset=UTF-8", new TextMessagePrivate )
{
  K_D(TextMessage);

  d->color = Qt::black;
}



/**
 * Constructor.
 *
 * Use it to create a common chat message.
 */
TextMessage::TextMessage( MsnChat* chat, MsnContact* peer )
: Message( peer, DirectionOutgoing, chat, ConfirmAlways, TextMessageType, "text/plain; charset=UTF-8", new TextMessagePrivate )
{
  K_D(TextMessage);

  // Set class properties
  d->color = Qt::black;
}



/**
 * Copy constructor from a plain Message.
 *
 * The properties are taken from the raw message data.
 *
 * @param other Message to clone
 */
TextMessage::TextMessage( const Message &other )
: Message( other )
{
  if ( other.type() == TextMessageType )
  {
    // nothing to do here. dptr already cloned in Message copy ctor.
    return;
  }

  // must create a new TextMessagePrivate and have it copy all of the
  // previous data from MessagePrivate.
  d_ptr = new TextMessagePrivate( *( d_ptr.data() ) );

  K_D(TextMessage);

  // Set the base class properties
  d->confirmationMode_ = ConfirmAlways;
  d->messageType_ = TextMessageType;

  if( ! hasField( "X-MMS-IM-Format" ) )
  {
    return;
  }

  // Get the font and color from the format string
  QString         family( field( "X-MMS-IM-Format", "FN" ) );
  const QString& effects( field( "X-MMS-IM-Format", "EF" ) );
  QString          color( field( "X-MMS-IM-Format", "CO" ) );
  QString          peer( field( "P4-Context" ) );

  family = QUrl::fromPercentEncoding( family.toUtf8() );
  d->color = QColor( KMess::MsnAlgorithms::convertMsnColorToHtmlColor( color ) );

  if ( hasField( "P4-Context" ) )
  {
    d->peerName = field( "P4-Context" );
  }

  d->font.setFamily   ( family );
  d->font.setBold     ( effects.contains( "B" ) );
  d->font.setItalic   ( effects.contains( "I" ) );
  d->font.setUnderline( effects.contains( "U" ) );
  d->font.setStrikeOut( effects.contains( "S" ) );
}



/**
 * Destructor.
 */
TextMessage::~TextMessage()
{
}



/**
 * Return the color of the message.
 *
 * @return QColor
 */
const QColor& TextMessage::color() const
{
  K_D(TextMessage);

  return d->color;
}



/**
 * Return the shortcuts to emoticons mapping of this message.
 *
 * The map contains the emoticons contained within the message text.
 * The mapping is done by shortcut, each one has its own MsnObject.
 *
 * @see MsnObject
 * @return MsnObject
 */
const MsnObjectMap& TextMessage::emoticons() const
{
  K_D(TextMessage);

  return d->emoticons;
}



/**
 * Return the font of the message.
 *
 * @return QFont
 */
const QFont& TextMessage::font() const
{
  K_D(TextMessage);

  return d->font;
}



/**
 * Return the text of the chat message.
 *
 * @return QString
 */
QString TextMessage::message() const
{
  K_D(TextMessage);

  return QString::fromUtf8( d->body_ );
}



/**
 * Return the peer's custom friendly name.
 *
 * @see setPeerName()
 * @return QString
 */
const QString& TextMessage::peerName() const
{
  K_D(TextMessage);

  return d->peerName;
}



/**
 * Return the color of the message.
 *
 * @return QColor
 */
void TextMessage::setColor( const QColor& color )
{
  K_D(TextMessage);

  d->color = color;
}



/**
 * Change all the shortcuts to emoticon mappings.
 *
 * @param emoticons The new emoticon map
 */
void TextMessage::setEmoticons( const MsnObjectMap& emoticons )
{
  K_D(TextMessage);

  d->emoticons = emoticons;
}



/**
 * Return the font of the message.
 *
 * @return QFont
 */
void TextMessage::setFont( const QFont& font )
{
  K_D(TextMessage);

  d->font = font;
}



/**
 * Return the text of the chat message.
 *
 * @return QString
 */
void TextMessage::setMessage( const QString& message )
{
  K_D(TextMessage);

  d->body_ = message.toUtf8();

  // Determine if the message is valid
  d->isValid_ = ( ! d->body_.isEmpty() );
}



/**
 * Set a custom friendly name for the peer.
 *
 * Setting this property changes the friendly name of the message sender
 * (which is the user when sending, and the contact when receiving).
 *
 * @param peerName New custom friendly name for the peer
 */
void TextMessage::setPeerName( const QString& peerName )
{
  K_D(TextMessage);

  if( peerName.isEmpty() )
  {
    debug() << "Cannot set an empty peer name!";
    return;
  }

  d->peerName = peerName;
}



/**
 * Prepare this message for sending. Set the appropriate headers and
 * content-type as required.
 */
void TextMessage::prepare()
{
  K_D(TextMessage);
  QString effects;
  if ( d->font.bold() )      effects += 'B';
  if ( d->font.italic() )    effects += 'I';
  if ( d->font.underline() ) effects += 'U';
  if ( d->font.strikeOut() ) effects += 'S';

  setField( "X-MMS-IM-Format", "FN=" + QUrl::toPercentEncoding( d->font.family() ) +
                              "; EF=" + effects +
                              "; CO=" + MsnAlgorithms::convertHtmlColorToMsnColor( d->color.name() ) +
                              "; CS=0" +
                              "; PF=" + QString::number( d->font.pointSize() ) );

  // set P4-Context if we need to
  if ( ! d->peerName.isEmpty() )
  {
    setField( "P4-Context", d->peerName );
  }
}
