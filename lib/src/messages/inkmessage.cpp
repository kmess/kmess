/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Valerio Pilo <valerio@kmess.org>                          *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file inkmessage.cpp
 * @brief Message with an handwritten drawing
 */

#include <KMess/InkMessage>
using namespace KMess;


namespace KMess
{
  class InkMessagePrivate : public MessagePrivate
  {
    public:
      InkMessagePrivate() {}
      InkMessagePrivate( const MessagePrivate &other ) : MessagePrivate( other ) {}
      virtual MessagePrivate *clone()
      {
        return new InkMessagePrivate( *this );
      }
      
      InkFormat              format_;
      QByteArray             data_;
  };
};


/**
 * Default constructor.
 */
InkMessage::InkMessage()
: Message( 0, DirectionOutgoing, 0, ConfirmNever, InkMessageType, QString(), new InkMessagePrivate )
{
  setFormat( FORMAT_ISF );
}



/**
 * Constructor.
 *
 * Use it to create an handwritten message.
 */
InkMessage::InkMessage( MsnChat* chat, MsnContact* peer, InkFormat format )
: Message( peer, DirectionOutgoing, chat, ConfirmNever, InkMessageType, QString(), new InkMessagePrivate )
{
  setFormat( format );
}



/**
 * Copy constructor from a plain Message.
 *
 * @param other Message to clone
 */
InkMessage::InkMessage( const Message &other )
: Message( other )
{
  if ( other.type() == InkMessageType )
  {
    return;
  }

  d_ptr = new InkMessagePrivate( *( d_ptr.data() ) );

  K_D(InkMessage);
  
  // Set the base class properties
  d->confirmationMode_ = ConfirmNever;
  d->messageType_ = InkMessageType;
  
  // what type of ink data is it? check content-type
  QString contentType = d->getValue("Content-Type");
  if ( contentType == "application/x-ms-ink" )
  {
    d->format_ = FORMAT_ISF;
  }
  else
  {
    d->format_ = FORMAT_GIF;
  }
  
  // parse out the raw data.
  d->data_ = QByteArray::fromBase64( QString( d->body_ ).section( ':', 1 ).toAscii() );
}



/**
 * Destructor.
 */
InkMessage::~InkMessage()
{
}



/**
 * Returns the Ink data as a QByteArray.
 *
 * Use format() to determine what format this data is ink (ISF or GIF).
 */
const QByteArray &InkMessage::data() const
{
  K_D(InkMessage);
  return d->data_;
}



/**
 * Return the format of this InkMessage (FormatIsf or FormatGif)
 *
 * Use this value to determine how to process the data returned by data().
 */
InkFormat InkMessage::format() const
{
  K_D(InkMessage);
  return d->format_;
}



/**
 * Assign the raw data to this InkMessage.
 *
 * @param data Raw data describing the ink, can be either a fortified-GIF or ISF drawing.
 */
void InkMessage::setData( const QByteArray &data )
{
  K_D(InkMessage);
  d->data_ = data;
  
  d->setBody( "base64:" + data.toBase64() );
}



/**
 * Set the internal format of the data of this InkMessage.
 *
 * This can either by FORMAT_ISF, to indicate the data is in Ink Serialized Format, or
 * FORMAT_GIF, to indicate a fortified-GIF.
 *
 * @param format The format of the data of this InkMessage.
 */
void InkMessage::setFormat( InkFormat format )
{
  K_D(InkMessage);
  d->format_ = format;
  
  if ( format == FORMAT_GIF )
  {
    setContentType( "image/gif" );
  }
  else
  {
    setContentType( "application/x-ms-ink" );
  }
}
