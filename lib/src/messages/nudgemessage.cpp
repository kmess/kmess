/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Valerio Pilo <valerio@kmess.org>                          *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file nudgemessage.cpp
 * @brief Message containing a nudge
 */

#include <KMess/NudgeMessage>

using namespace KMess;



/**
 * Default constructor.
 */
NudgeMessage::NudgeMessage()
: Message( 0, DirectionOutgoing, 0, ConfirmNever, NudgeMessageType, "text/x-msnmsgr-datacast" )
{
  K_D(Message);
  d->setBody("ID: 1");
}



/**
 * Constructor.
 *
 * Use it to create a nudge message.
 */
NudgeMessage::NudgeMessage( MsnChat* chat, MsnContact* peer )
: Message( peer, DirectionOutgoing, chat, ConfirmNever, NudgeMessageType, "text/x-msnmsgr-datacast" )
{
  K_D(Message);
  d->setBody("ID: 1\r\n");
}



/**
 * Copy constructor from a plain Message.
 *
 * @param other Message to clone
 */
NudgeMessage::NudgeMessage( const Message &other )
: Message( other )
{
  K_D(Message);
  
  // Set the base class properties
  d->confirmationMode_ = ConfirmNever;
  d->messageType_ = NudgeMessageType;
}



/**
 * Destructor.
 */
NudgeMessage::~NudgeMessage()
{
  // delete d;
}

