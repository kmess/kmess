/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Valerio Pilo <valerio@kmess.org>                          *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file message.cpp
 * @brief The most basic kind of message
 */

#include "debug/libkmessdebug.h"

#include <KMess/Message>
#include <KMess/ActionMessage>
#include <KMess/ClientCapsMessage>
#include <KMess/EmoticonMessage>
#include <KMess/InkMessage>
#include <KMess/EmailMessage>
#include <KMess/NudgeMessage>
#include <KMess/OfflineMessage>
#include <KMess/PtpDataMessage>
#include <KMess/TextMessage>
#include <KMess/TypingMessage>
#include <KMess/VoiceMessage>
#include <KMess/WinkMessage>

using namespace KMess;


/**
 * Template specialization for QSharedDataPointer::clone().
 *
 * Calls the virtual method MessagePrivate::clone().
 */
template<>
MessagePrivate *QSharedDataPointer<MessagePrivate>::clone()
{
  return d->clone();
}



/**
 * Default constructor. Not intended to be used and provided only for
 * compatibility.
 *
 * Creates a new, invalid message type. Although you can technically create
 * instances of this class there is little point. Create an instance of a subclass
 * instead.
 */
Message::Message()
{
  d_ptr = new MessagePrivate();
}



/**
 * Constructor.
 *
 * It is not publicly accessible, because there is no point
 * in creating such a simple message type.
 *
 * Use one of its subclasses instead.
 *
 * @param peer Contact with which the message was exchanged with
 * @param direction Whether this message is being received or sent
 * @param chat MsnChat to which this Message belongs.
 * @param mode Confirmation mode for this message
 * @param type Type of Message
 * @param contentType The value of the Content-Type header
 * @param privData MessagePrivate instance.
 */
Message::Message( MsnContact* peer, MessageDirection direction, MsnChat *chat, ConfirmationMode mode, MessageType type, QString contentType, MessagePrivate *privData )
{
  if ( ! privData )
  {
    privData = new MessagePrivate();
  }

  d_ptr = privData;

  privData->peer_ = peer;
  privData->direction_ = direction;
  privData->chat_ = chat;
  privData->confirmationMode_ = mode;
  privData->messageType_ = type;

  if ( ! contentType.isEmpty() )
  {
    setField( "Content-Type", contentType );
  }
}



/**
 * Copy constructor.
 *
 * @param other Message to clone
 */
Message::Message( const Message &other )
: d_ptr( other.d_ptr )
{
}



/**
 * Destructor.
 *
 * It is not publicly accessible, because there is no point
 * in creating such a simple message type.
 *
 * Use one of its subclasses instead.
 */
Message::~Message()
{
}



/**
 * Parse and append more data to the message header.
 *
 * Useful when generating or parsing a message incrementally.
 *
 * @param header The header bytes to append
 */
void Message::appendToHeader( const QByteArray& header )
{
  K_D(Message);

  QStringList headersList;
  d->splitHead( headersList, QString::fromUtf8( header ) );

  QStringListIterator it( headersList );
  while( it.hasNext() )
  {
    QString field, value;

    d->splitLine( field, value, it.next() );

    if( field.isEmpty() )
    {
      continue;
    }

    d->header_[ field ] = value;
  }
}



/**
 * Parse and append more data to the message body.
 *
 * Useful when generating or parsing a message incrementally.
 *
 * @param body The body bytes to append
 */
void Message::appendToBody( const QByteArray& body )
{
  K_D(Message);

  if( body.isEmpty() )
  {
    return;
  }

  d->body_.append( body );
}



/**
 * Return the message body.
 *
 * @return QByteArray
 */
QByteArray Message::body() const
{
  K_D(Message);

  return d->getBody();
}



/**
 * Return the chat which owns this message.
 *
 * If this message doesn't belong to any chat yet, this will return 0.
 *
 * @return MsnChat*, or zero if the message has no owner
 */
MsnChat* Message::chat() const
{
  K_D(Message);

  return d->chat_;
}



/**
 * Return the current delivery confirmation mode.
 *
 * It is used to determine if the library should notify the application
 * whether this message could or could not be sent.
 *
 * @see the ConfirmationMode enum
 * @return ConfirmationMode
 */
ConfirmationMode Message::confirmationMode() const
{
  K_D(Message);

  return d->confirmationMode_;
}



/**
 * Return the full message contents.
 *
 * The returned byte array contains both the header and the
 * body of the message.
 *
 * @return QByteArray
 */
QByteArray Message::contents()
{
  K_D(Message);

  prepare();
  return d->getMessage();
}



/**
 * Return the date when this message was received or sent.
 * @see time()
 * @see dateTime()
 */
QDate Message::date() const
{
  K_D(Message);

  return d->dateTime_.date();
}



/**
 * Return the date and time when this message was received or sent.
 * @see date()
 * @see time()
 */
const QDateTime &Message::dateTime() const
{
  K_D(Message);

  return d->dateTime_;
}



/**
 * Return the value of a single field from the header.
 *
 * The returned value is empty both if the field was empty and if did not exist.
 *
 * @param name The field name
 * @return QString
 */
QString Message::field( const QString& name ) const
{
  K_D(Message);

  return d->getValue( name );
}



/**
 * Return part of the value of a field from the header.
 *
 * This method is useful for fields composed like this:
 * @code Content-Type: text/x-msmsgsinitialmdatanotification; charset=UTF-8 @endcode
 * The subfield argument can be used to select which part of the field needs
 * to be obtained; in the example above, "charset".
 *
 * If you only want the actual field value, and not its attributes, just use
 * QString() for the subField argument.
 *
 * The returned value is empty both if the field was empty and if did not exist.
 *
 * @return QString
 */
QString Message::field( const QString& name, const QString& subFieldName ) const
{
  K_D(Message);

  return d->getSubValue( name, subFieldName );
}



/**
 * Return the mapping of header fields.
 *
 * @return FieldMap
 */
const FieldMap& Message::fields() const
{
  K_D(Message);

  return d->header_;
}



/**
 * Parse a message from a byte array
 *
 * @param peer Contact to send the message to, or receive the message from
 * @param direction Indicates if the message is incoming or outgoing
 * @param data The bytes to be parsed
 * @return Message subclass, or an invalid Message
 */
Message Message::fromData( MsnContact* peer, MessageDirection direction, const QByteArray &data )
{
  Message tempMessage( peer, direction );

  debug() << "Parsing MIME message";

  tempMessage.parseMimeMessage( data );

  const QString &mimeType( tempMessage.field( "Content-Type", QString() ) );

  if( mimeType.isEmpty() )
  {
    debug() << "Message has no content type, returning invalid message";

    return tempMessage;
  }

  // Recognize known mimetypes, and clone this message into a specialized one.
  if( mimeType == "text/plain" )
  {
    if( tempMessage.hasField( "X-OIM-Message-Type" ) &&
        tempMessage.field( "X-OIM-Message-Type" ) == "OfflineMessage" )
    {
      debug() << "Message is an offline message";

      return OfflineMessage( tempMessage );
    }
    else
    {
      debug() << "Message is a text message";

      return TextMessage( tempMessage );
    }
  }
  else
  if( mimeType == "text/x-msmsgscontrol" )
  {
    debug() << "Message is a typing message";
    return TypingMessage( tempMessage );
  }
  else
  if( mimeType == "text/x-msnmsgr-datacast" )
  {
    tempMessage.appendToHeader( tempMessage.body() );

    debug() << "Message is a datacast message of type" << tempMessage.field( "ID" ) << ", analyzing it...";

    switch( tempMessage.field( "ID" ).toInt() )
    {
      case 1: // Nudge
        debug() << "...the message is a nudge.";
        return NudgeMessage( tempMessage );
        break;

      case 2: // Wink
        debug() << "...the message is a wink.";
        return WinkMessage( tempMessage );
        break;

      case 3: // Voice Clip
        debug() << "...the message is a voice clip.";
        return VoiceMessage( tempMessage );
        break;

      case 4: // Action Message
        debug() << "...the message is an action message.";
        return ActionMessage( tempMessage );
        break;

      default: // Completely unknown message type
        debug() << "...the message is of unknown type!";
        break;
    }
  }
  else
  if( mimeType == "text/x-mms-emoticon" || mimeType == "text/x-mms-animemoticon" )
  {
    debug() << "Message is an emoticon message";
    return EmoticonMessage( tempMessage );
  }
  else
  if( mimeType == "image/gif" || mimeType == "application/x-ms-ink" )
  {
    debug() << "Message is an ink message";
    return InkMessage( tempMessage );
  }
  else
  if( mimeType == "text/x-clientcaps" )
  {
    debug() << "Message is a client caps message";
    return ClientCapsMessage( tempMessage );
  }
  else
  if( mimeType == "text/x-keepalive" )
  {
    debug() << "Message is a keepalive message";
  }
  else
  if( mimeType == "application/x-msnmsgrp2p" )
  {
    debug() << "Message is a peer to peer data message";
    return PtpDataMessage( tempMessage );
  }
  else
  if( mimeType == "text/x-msmsgsinitialmdatanotification"
  ||  mimeType == "text/x-msmsgsinitialemailnotification"
  ||  mimeType == "text/x-msmsgsactivemailnotification"
  ||  mimeType == "text/x-msmsgsoimnotification" )
  {
    debug() << "Message is an email or OIM data message";
    return EmailMessage( tempMessage );
  }
  else
  if( mimeType == "text/x-msmsgsprofile" )
  {
    debug() << "Message contains a Messenger profile's data, returning as a simple Message";
  }
  else
  {
    debug() << "Message is of unknown type!";
  }

  return tempMessage;
}



/**
 * Return whether an header is present or not.
 *
 * @param field Field to verify
 * @return bool
 */
bool Message::hasField( const QString& field ) const
{
  K_D(Message);

  return d->header_.contains( field );
}



/**
 * Return the message header.
 *
 * @return QByteArray
 */
QByteArray Message::header() const
{
  K_D(Message);

  return d->getFields();
}



/**
 * Return whether the message is incoming (it is traveling from a contact
 * to the user) or outgoing (traveling from the user to a contact or contacts).
 *
 * Library-generated messages are always incoming.
 *
 * To find out who is the sender or the receiver of the message,
 * use peer().
 *
 * @see peer()
 * @return bool
 */
bool Message::isIncoming() const
{
  K_D(Message);

  return d->direction_ == DirectionIncoming;
}



/**
 * Return whether the message contains useable data or not.
 *
 * For example, an invalid message can be obtained from incorrect binary data.
 *
 * @return bool
 */
bool Message::isValid() const
{
  K_D(Message);

  return d->isValid_;
}



/**
 * Parse a MIME message and store it in this message
 *
 * @param message The data to parse
 */
void Message::parseMimeMessage( const QByteArray& message )
{
  K_D(Message);

  d->parseMessage( message, d->isValid_ );
}



/**
 * Return the contact with which this message is being transferred.
 *
 * - For incoming messages, peer() always returns the contact who has sent
 *   the message to the user. Library-generated messages are always incoming
 *   and have no sender: peer() returns 0 in that case.
 * - For outgoing messages, peer() returns the recipient of the message.
 *   Broadcast messages (messages for all contacts) have no recipient:
 *   peer() returns 0 in this case.
 *
 * To find out whether the message is incoming or outgoing, use isIncoming().
 *
 * @see isIncoming()
 * @return MsnContact*, or zero if the message is broadcast
 */
MsnContact* Message::peer() const
{
  K_D(Message);

  return d->peer_;
}



/**
 * This method is called just before a message is
 * sent. Subclasses should implement the method to allow setting
 * of message-specific properties such as the content-type and any
 * other headers that are necessary.
 *
 * This method may get called repeatedly.
 *
 * The default implementation of this method does nothing.
 */
void Message::prepare()
{
}



/**
 * Return the time when this message was received or sent.
 * @see date()
 * @see dateTime()
 */
QTime Message::time() const
{
  K_D(Message);

  return d->dateTime_.time();
}



/**
 * Set the confirmation mode for this message.
 *
 * This property can be set for outgoing messages to enforce a
 * certain kind of delivery report from being done on it.
 * Each Message subclass has its distinctive mode: for example
 * chat messages, which are important, use ConfirmAlways; less
 * important messages like nudges instead could use ConfirmFailure.
 *
 * You can override the default behavior by using this method.
 *
 * Setting this property for incoming messages makes no sense.
 *
 * @see the Message subclasses
 * @see ConfirmationMode
 * @param mode The new confirmation mode for this message
 * @return bool
 */
void Message::setConfirmationMode( const ConfirmationMode mode )
{
  K_D(Message);

  d->confirmationMode_ = mode;
}



/**
 * Set the Content-Type of this message.
 */
void Message::setContentType( const QString type )
{
  K_D(Message);

  d->setField( "Content-Type", type );
}



/**
 * Change the date and time of the message.
 *
 * @param dateTime the new date/time of reception (or sending)
 */
void Message::setDateTime( const QDateTime dateTime )
{
  K_D(Message);

  d->dateTime_ = dateTime;
}



/**
 * Set the direction of this message.
 * @param direction Message direction.
 */
void Message::setDirection( const MessageDirection direction )
{
  K_D(Message);

  d->direction_ = direction;
}



/**
 * Set the chat this message belongs to.
 * @param chat MsnChat instance this message belongs to.
 */
void Message::setChat( MsnChat *chat )
{
  K_D(Message);

  d->chat_ = chat;
}



/**
 * Set the peer of this message.
 * @param peer Message peer.
 */
void Message::setPeer( MsnContact *peer )
{
  K_D(Message);

  d->peer_ = peer;
}



/**
 * Set a message field.
 *
 * @param field Field name
 * @param value Field value
 */
void Message::setField( const QString& field, const QString& value )
{
  K_D(Message);

  d->setField( field, value );
}



/**
 * Return which type of message this is.
 *
 * Usually pointers to Message are passed around: you can recognize the
 * real type of the messages by looking at this property.
 *
 * @see MessageType
 * @return MessageType
 */
MessageType Message::type() const
{
  K_D(Message);

  return d->messageType_;
}
