/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Valerio Pilo <valerio@kmess.org>                          *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file actionmessage.cpp
 * @brief Message with a plugin action
 */

#include <KMess/ActionMessage>

using namespace KMess;



/**
 * Default constructor.
 */
ActionMessage::ActionMessage()
: Message( 0, DirectionOutgoing, 0, ConfirmNever, ActionMessageType )
{
}



/**
 * Constructor.
 *
 * Use it to create an action message.
 */
ActionMessage::ActionMessage( MsnChat* chat, MsnContact* peer )
: Message( peer, DirectionOutgoing, chat, ConfirmNever, ActionMessageType )
{ 
}



/**
 * Copy constructor from a plain Message.
 *
 * @param other Message to clone
 */
ActionMessage::ActionMessage( const Message &other )
: Message( other )
{
  // Set the base class properties
  d_ptr->confirmationMode_ = ConfirmNever;
  d_ptr->messageType_ = ActionMessageType;
}



/**
 * Destructor.
 */
ActionMessage::~ActionMessage()
{
}


