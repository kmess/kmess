/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Valerio Pilo <valerio@kmess.org>                          *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file clientcapsmessage.cpp
 * @brief Message containing extra data about a client
 */

#include "debug/libkmessdebug.h"

#include <KMess/ClientCapsMessage>

using namespace KMess;

namespace KMess
{
  /**
  * Private data for ClientCapsMessage.
  */
  class ClientCapsMessagePrivate : public MessagePrivate
  {
    public:
      ClientCapsMessagePrivate() {}
      ClientCapsMessagePrivate( const MessagePrivate &other ) : MessagePrivate( other ) {}
      virtual MessagePrivate *clone()
      {
        return new ClientCapsMessagePrivate( *this );
      }

      /// The way how the client is logging the active chat's messages
      ChatLoggingMode        chatLoggingMode_;
      /// Third-party client name
      QString                clientName_;
      /// Third-party client version
      QString                clientVersion_;
  };
};


/**
 * Default constructor.
 */
ClientCapsMessage::ClientCapsMessage()
: Message( 0, DirectionOutgoing, 0, ConfirmNever, ClientCapsMessageType, "text/x-clientcaps", new ClientCapsMessagePrivate )
{
}



/**
 * Constructor.
 *
 * Use it to create a Client Caps message.
 */
ClientCapsMessage::ClientCapsMessage( MsnChat* chat, MsnContact* peer )
: Message( peer, DirectionOutgoing, chat, ConfirmNever, ClientCapsMessageType, "text/x-clientcaps", new ClientCapsMessagePrivate )
{
}



/**
 * Destructor
 */
ClientCapsMessage::~ClientCapsMessage()
{
}



/**
 * Copy constructor from a plain Message.
 *
 * @param other Message to clone
 */
ClientCapsMessage::ClientCapsMessage( const Message &other )
: Message( other )
{
  if ( other.type() == ClientCapsMessageType )
  {
    return;
  }

  // now, assign a new ClientCapsMessagePrivate
  // that duplicates the old MessagePrivate data.
  d_ptr = new ClientCapsMessagePrivate( *( d_ptr.data() ) );

  K_D(ClientCapsMessage);

  // Set the base class properties
  d->confirmationMode_ = ConfirmNever;
  d->messageType_ = ClientCapsMessageType;

  // The message body contains the fields, in MIME form.
  appendToHeader( d->body_ );

  // The body isn't useful anymore, clear it up
  d->body_.clear();

  /*
   * Example message:
   * @code
   * Client-Name: Client-Name/Version-Major.Version-Minor
   * Chat-Logging: Y
   * @endcode
   *
   * Possible values for Chat-Logging:
   * - Y: Nonsecure logging is enabled
   * - S: log is encrypted
   * - N: not logging
   */

  // Extract the name and version
  const QString& clientFullName( field( "Client-Name" ) );

  d->clientName_    = clientFullName.section( '/', 0, 0 );
  d->clientVersion_ = clientFullName.section( '/', 0, 1 );

  // Then retrieve the chat logging mode
  const QString &clientLoggingMode( field( "Chat-Logging" ) );

  if( clientLoggingMode == "Y" )
  {
    d->chatLoggingMode_ = LoggingActive;
  }
  else
  if( clientLoggingMode == "S" )
  {
    d->chatLoggingMode_ = LoggingActiveEncrypted;
  }
  else
  if( clientLoggingMode == "N" )
  {
    d->chatLoggingMode_ = LoggingInactive;
  }
  else
  {
    d->chatLoggingMode_ = LoggingModeUnknown;
  }
}



/**
 * Return the 3rd party client's chat logging mode.
 *
 * @return QString
 */
ChatLoggingMode ClientCapsMessage::chatLoggingMode() const
{
  K_D(ClientCapsMessage);
  return d->chatLoggingMode_;
}



/**
 * Return the 3rd party client name.
 *
 * @return QString
 */
const QString& ClientCapsMessage::clientName() const
{
  K_D(ClientCapsMessage);
  return d->clientName_;
}



/**
 * Return the 3rd party client version.
 *
 * @return QString
 */
const QString& ClientCapsMessage::clientVersion() const
{
  K_D(ClientCapsMessage);
  return d->clientVersion_;
}



/**
 * Set the chat logging mode.
 *
 * @see ChatLoggingMode
 * @param mode Chat logging mode
 */
void ClientCapsMessage::setChatLoggingMode( const ChatLoggingMode mode )
{
  K_D(ClientCapsMessage);
  d->chatLoggingMode_ = mode;
}



/**
 * Set the 3rd party client name.
 *
 * @param clientName Client name, like "KMess"
 */
void ClientCapsMessage::setClientName( const QString& clientName )
{
  K_D(ClientCapsMessage);
  d->clientName_ = clientName;
}



/**
 * Set the 3rd party client version.
 *
 * @param clientVersion Client version, like "1.0" or "2.1 beta"
 */
void ClientCapsMessage::setClientVersion( const QString& clientVersion )
{
  K_D(ClientCapsMessage);
  d->clientVersion_ = clientVersion;
}



/**
 * Prepare capabilities message for sending.
 */
void ClientCapsMessage::prepare()
{
  K_D(ClientCapsMessage);

  QChar logMode = 'N';
  switch( d->chatLoggingMode_ )
  {
    case LoggingActive:
      logMode = 'Y';
      break;

    case LoggingActiveEncrypted:
      logMode = 'S';
      break;

    default:
      logMode = 'N';
      break;
  }

  d->setBody( "Client-Name: " + d->clientName_ + '/' + d->clientVersion_ + "\r\nChat-Logging: " + logMode );
}

