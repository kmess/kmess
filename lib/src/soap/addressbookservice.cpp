/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Antonio Nastasi <sifcenter@gmail.com>                     *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file addressbookservice.cpp
 */

#include "addressbookservice.h"
#include "debug/libkmessdebug.h"
#include "../listsmanager.h"
#include "addressbookhelper.h"

#include <KMess/MsnSession>
#include <KMess/NetworkGlobals>
#include <KMess/Utils>
#include <KMess/MsnGroup>
#include <KMess/MsnContact>
#include <KMess/ContactProfile>
#include <KMess/Self>

#include <QDomDocument>
#include <QDomElement>
#include <QDomNode>
#include <QFile>
#include <QTextCodec>
#include <QDir>

#include "../utils/xmlfunctions.h"
#include "soapmessage.h"


using namespace KMess;
using namespace KMessInternal;


#ifdef KMESSDEBUG_HTTPSOAPCONNECTION
  #define KMESSDEBUG_ADDRESSBOOKSERVICE
#endif


/**
 * @brief URL of the Address Book Service
 */
#define SERVICE_URL_ADDRESSBOOK           "https://omega.contacts.msn.com/abservice/abservice.asmx"

/**
 * @brief URL of the Address Book Sharing Service
 */
#define SERVICE_URL_ADDRESSBOOK_SHARING   "https://omega.contacts.msn.com/abservice/SharingService.asmx"


#define CONTACTS_API "Contacts"

/**
 * @brief The constructor
 */
AddressBookService::AddressBookService( KMess::MsnSession *session, ListsManager *listsManager, QObject *parent )
: PassportLoginService( session, parent )
{
  listsManager_ = listsManager;
  
  connect( &updateTimer_, SIGNAL( timeout() ), this, SLOT( doProfilesUpdate() ) );
}



/**
 * @brief The destructor
 */
AddressBookService::~AddressBookService()
{
  qDeleteAll( contacts_ );
}



/**
 * This is an overloaded function.
 * @param handle Contact handle.
 * @param network Contact network (NetworkMsn or NetworkYahoo).
 * @param groups List of groups you wish the contact to be added to. Can be empty.
 */
void AddressBookService::addContact( const QString &handle, KMess::NetworkType network, const QList<MsnGroup*> &groups )
{
  if ( handle.isEmpty() ||
       ( network & ( KMess::NetworkMsn | KMess::NetworkYahoo ) ) == 0 )
  {
    warning() << "Must have valid handle and valid network type.";
    return;
  }
  
  addNewContact( handle, network, groups );
}


/**
 * Adds a new contact to the address book.
 *
 * If the contact is on the pending list then they are first added to our Reverse list.
 *
 * @param contact MsnContact you wish to add.
 * @param groups A QList of the groups you wish to have the contact added to. This can be empty.
 */
void AddressBookService::addContact( const KMess::MsnContact *contact, const QList<MsnGroup*> &groups )
{
  KMESS_ASSERT( contact );

  addNewContact( contact->handle(), contact->network(), groups );
}



/**
 * @brief Add a new or already known contact to the Address Book.
 *
 * To add support for Yahoo contacts it may be needed to use the FQY NS command,
 * as it is used to determine the real type (or types!) of an address.
 */
void AddressBookService::addNewContact( const QString &handle, KMess::NetworkType network, const QList<MsnGroup*> &groups )
{
  MsnContact *contact = listsManager_->contact( handle );
  
  // contacts already on the AB, set their IsMessengerUser property to
  // true. that's all we need to do. those *not* on the AB need a full ABContactAddRequest.
  if ( contact && contact->profile() )
  {
    if ( ! contact->profile()->isMessengerUser() )
    {
      contact->profile()->setIsMessengerUser( true );
      contact->setMemberships( contact->memberships() | KMess::MSN_LIST_FRIEND );

      emit contactAdded( contact );
      return;
    }
  }
  
  if ( contact && contact->isPending() )
  {
    // first add them to the RL. then, once that is confirmed, add them to the AB proper.
    addContactToList( contact, KMess::MSN_LIST_REVERSE );
    removeContactFromList( contact, KMess::MSN_LIST_PENDING );
  }
  
  ABContactAddRequest req;
  
  ABContact *c = new ABContact();
  
  req.enableAllowListManagement = true;
  
  c->contactInfo.contactType = "Regular";

  if ( network == KMess::NetworkYahoo )
  {
    ABContactEmail *e = new ABContactEmail();
    e->contactEmailType = "Messenger2";
    e->email = handle;
    e->isMessengerEnabled = true; // better be!
    e->capability = network; // should be "32" for "Yahoo"
    e->propertiesChanged = "Email IsMessengerEnabled Capability";

    c->contactInfo.emails.append( e );
  }
  else
  {
    c->contactInfo.passportName = handle;
    c->contactInfo.isMessengerUser = true;

    // TODO: invitations?
  }
  
  req.contacts.append( c );
  
  QStringList groupsId;
  foreach ( KMess::MsnGroup *g, groups )
  {
    groupsId << g->id();
  }
  
  MessageData data;
  data.type  = "ContactAdd";
  data.value = QStringList() << handle << QString::number( network ) << groupsId;

  sendSecureRequest( req.message( data ), CONTACTS_API );
}



void AddressBookService::addContactToGroup( const KMess::MsnContact *contact, const KMess::MsnGroup *group )
{
  KMESS_ASSERT( contact );
  KMESS_ASSERT( group );
  
  ABGroupContactAddRequest req;
  
#ifdef __GNUC__
  #warning TODO: Can we do multiple group adds in one call?
#endif

  req.groupIds.append( group->id() );
  req.contactId = contact->guid();
  
  MessageData data;
  data.type  = "ContactAddToGroup";
  data.value = group->id();

  sendSecureRequest( req.message( data ), "Contacts" );
}



void AddressBookService::addGroup( const QString &name )
{
  ABGroupAddRequest req;
  req.groupInfo.name = KMess::Utils::htmlEscape( name );
  req.groupInfo.annotations.append( new ABAnnotation( "MSN.IM.Display", "1" ) );
  
  MessageData data;
  data.type  = "GroupAdd";
  data.value = name;

  sendSecureRequest( req.message( data ), CONTACTS_API );
}



// Block contact
void AddressBookService::blockContact( const KMess::MsnContact *contact )
{
  KMESS_ASSERT( contact );
  
  if ( contact->isAllowed() )
  {
    removeContactFromList( contact, MSN_LIST_ALLOWED );
  }
  
  if ( ! contact->isBlocked() )
  {
    addContactToList( contact, KMess::MSN_LIST_BLOCKED );
  }
}



/**
 * @brief Request creation of a new address book.
 *
 * Newly created accounts have no address book. This method
 * requests the creation of a new one.
 */
void AddressBookService::createAddressBook()
{
  ABAddRequest req;
  req.abInfo.ownerEmail = session_->sessionSettingString( "AccountHandle" );
  
  sendSecureRequest( req.message(), CONTACTS_API );
}



/**
 * Update a given contact profile on the server.
 */
void AddressBookService::updateProfile( const ContactProfile *profile )
{
  KMESS_ASSERT( profile );

  // don't send immediately - give it 1s.
  // that gives existing code a chance to put more changes into the 
  // contact instance above, preventing us sending a lot of 
  // ABContactUpdate requests at once.
  if ( ! profilesToUpdate_.contains( const_cast<ContactProfile*>( profile ) ) )
  {
    profilesToUpdate_.append( const_cast<ContactProfile*>( profile ) );
  }

  if ( ! updateTimer_.isActive() )
  {
    updateTimer_.setSingleShot( false );
    updateTimer_.start( 1000 ); // 1s.
  }
}



/**
 * Performs the update of the address book by comparing
 * the most recent cached profile information against the current
 * information. Generates the update request based on the 
 * differences and then sends it.
 */
void AddressBookService::doProfilesUpdate()
{
  foreach( ContactProfile *profile, profilesToUpdate_ )
  {
    profilesToUpdate_.removeAll( profile );

    // existing information.
    ABContact *abc = NULL;
    MsnContact *contact = NULL;
    
    // find out who is associated with this profile
    foreach ( MsnContact *c, contacts_.keys() )
    {
      if ( c->profile() == profile )
      {
        contact = c;
        abc = contacts_.value( c );
        break;
      }
    }

    KMESS_ASSERT( abc );
    KMESS_ASSERT( contact );

    // the updated information
    ABContactInfo *info = profile->info();

    ABContactUpdateRequest req;
    
    ABContactInfo *old = &(abc->contactInfo);
    ContactProfile oldProfile( NULL, old );

    // fresh instance that we will send to the server
    ABContact *c = new ABContact();

    // if we're updating our personal record, set ContactType appropriately.
    if ( contact == listsManager_->self() )
    {
      c->contactInfo.contactType = "Me";
    }
    else
    {
      c->contactId = abc->contactId;
    }

    req.contacts.append( c );

    QStringList propertiesChanged;
    
    // display name
    if ( old->displayName != info->displayName )
    {
      propertiesChanged << "DisplayName";
      c->contactInfo.displayName = info->displayName;
    }

    // first, middle, last name
    if ( old->firstName != info->firstName )
    {
      propertiesChanged << "FirstName";
      c->contactInfo.firstName = info->firstName;
    }
    if ( old->middleName != info->middleName )
    {
      propertiesChanged << "MiddleName";
      c->contactInfo.middleName = info->middleName;
    }
    if ( old->lastName != info->lastName )
    {
      propertiesChanged << "LastName";
      c->contactInfo.lastName = info->lastName;
    }

    // annotations - update or insert
    foreach( ABAnnotation *a, info->annotations )
    {
      bool done = false;
      
      foreach( ABAnnotation *a2, old->annotations )
      {
        if ( a2->name == a->name )
        {
          done = true;
          if ( a2->value != a->value )
          {
            c->contactInfo.annotations.append( new ABAnnotation( *a ) );
            break;
          }
        }
      }
      
      if ( ! done )
      {
        c->contactInfo.annotations.append( new ABAnnotation( *a ) );
      }
    }

    if ( c->contactInfo.annotations.count() > 0 )
    {
      propertiesChanged << "Annotation";
    }
    
    // Phone numbers
    foreach( ABContactPhone *p, info->phones )
    {
      bool done = false;
      
      foreach( ABContactPhone *p2, old->phones )
      {
        if ( p->contactPhoneType == p2->contactPhoneType )
        {
          done = true;
          if ( p->number != p2->number )
          {
            c->contactInfo.phones.append( new ABContactPhone( *p ) );
            break;
          }
        }
      }
      
      if ( ! done )
      {
        c->contactInfo.phones.append( new ABContactPhone( *p ) );
      }
    }
    
    if ( c->contactInfo.phones.count() > 0 )
    {
      propertiesChanged << "ContactPhone";
    }

    // this is an email contact
    if ( ! oldProfile.isPassportUser() )
    {
      // if this is an email member, and the IsMessengerUser property
      // changes, check the corresponding email address and update that
      // if necessary.
      if ( oldProfile.isMessengerUser() != profile->isMessengerUser() )
      {
        ABContactEmail *msgrEmail = profile->_email( ContactProfile::EmailMessenger );
        if ( msgrEmail )
        {
          ABContactEmail *e = new ABContactEmail( *msgrEmail );
          propertiesChanged << "ContactEmail";
          e->propertiesChanged = "IsMessengerEnabled";
          c->contactInfo.emails.append( e );
        }
      }
    }
    else
    {
      // IsMessengerUser...
      if ( old->isMessengerUser != info->isMessengerUser )
      {
        c->contactInfo.isMessengerUser = info->isMessengerUser;
        propertiesChanged << "IsMessengerUser";
      }
    }

    // if something has actually changed, send it away.
    if ( propertiesChanged.count() > 0 )
    {
      c->propertiesChanged = propertiesChanged.join( " " );

      // copy the new stuff into the cache.
      *old = *info;
      
      MessageData data;
      data.type = "ContactUpdate";
      data.value = propertiesChanged;

      // Timer is used for non-messenger AB contacts.
      req.setPartnerScenario( info->isMessengerUser ? "ContactSave" : "Timer" );

      sendSecureRequest( req.message( data ), CONTACTS_API );

      // TODO: once the request is confirmed, we need to request the new changes to the address book.
      // retrieveAdddressBook() will do.
    }
    else
    {
      debug() << "No changes for contact" << contact << "; not sending update.";
    }
  }
}

void AddressBookService::deleteContact( KMess::MsnContact *contact, KMess::RemoveContactOption option, KMess::RemoveContactAction action )
{
  KMESS_ASSERT( contact );

  if ( action == KMess::RemoveAndBlock )
  {
    blockContact( contact );
  }

  // we take different paths depending on if the user chose to remove from msgr or WL...
  // for removing from messenger, all we need to do is set the "isMessengerUser" (for Passport users) or the
  // "isMessengerEnabled" (for Yahoo) property to "false". They remain on the WL address book, but are not
  // considered MSN contacts. by default, WLM does this unless you specify otherwise.
  //
  // for removing from the AB entirely, it's the sequence we've always known - send the ABContactDeleteRequest.
  // that will remove them not just from MSN, but hotmail too.
  //
  if ( option == KMess::RemoveFromMessenger )
  {
    contact->profile()->setIsMessengerUser( false );
    contact->setMemberships( contact->memberships() ^ ( contact->memberships() & KMess::MSN_LIST_FRIEND ) );

    // normally we'd wait for a confirmation response but that adds extra complexity since all we get
    // back is a successful "ContactUpdate" reply - doesn't tell us anything about what was updated and why. 
    // assume the change will be valid and signal the deletion now.
    emit contactDeleted( contact );
  }
  else
  {
    ABContactDeleteRequest req;
    ContactIdType *t = new ContactIdType();
    t->contactId = contact->guid();
    req.contacts.append( t );
    
    MessageData data;
    data.type  = "ContactDelete";
    data.value = contact->guid();

    sendSecureRequest( req.message( data ), CONTACTS_API );
    
    // we're going to do a WLM-esque thing now - assume the deletion will work and
    // emit the deletion signal now. worst case the delete fails and the contact re-appears on next
    // login.
    emit contactDeleted( contact );
  }
}



void AddressBookService::deleteGroup( const KMess::MsnGroup *group )
{
  KMESS_ASSERT( group );
  
  ABGroupDeleteRequest req;
  
  req.groupIds.append( group->id() );
  
  MessageData data;
  data.type  = "GroupDelete";
  data.value = group->id();

  sendSecureRequest( req.message( data ), CONTACTS_API );
}



/**
 * Retrieves the address book and memberships from the server and caches them
 * in the given cachedir.
 *
 * @param cacheDir Directory for membership/address book cache data.
 */
void AddressBookService::fetchAddressBook( const QString &cacheDir )
{
  // ensure the cache is usable.
  QDir dir( cacheDir );

  if ( ! dir.exists() ) 
  {
    QDir temp( dir );
    temp.cdUp(); // up to the parent...
    temp.mkpath( dir.dirName() ); // create the cache folder.
  }
  
  if ( ! cacheDir.isEmpty() && dir.exists() )
  {
    membershipCache_.cacheFile = cacheDir + "/membership_cache";
    addressbookCache_.cacheFile = cacheDir + "/addressbook_cache";
  }
  else
  {
    warning() << "Cache dir" << cacheDir << "does not exist or is unreadable. Caching will not be used.";
    membershipCache_.cacheFile.clear();
    addressbookCache_.cacheFile.clear();
  }
  
  // ask for the memberships and address book.
  // 
  retrieveMemberships( FullFetch );
}



/**
 * Reads the address book cache file and loads it into addressBookCache_.cacheDocument.
 */
void AddressBookService::readCache( ABCacheData &data, const QString &nodeName )
{
  if ( data.cacheFile.isEmpty() )
  {
    return;
  }

  QFile fCache( data.cacheFile );
 
  if ( ! fCache.open( QIODevice::ReadOnly ) )
  {
    return;
  }

  // cache is open. read.
  if ( ! data.cacheDocument.setContent( &fCache ) )
  {
    warning() << "Cache file" << data.cacheFile << "could not be parsed.";
    return;
  }

  debug() << "Cache file" << data.cacheFile << "opened and parsed OK.";
  
  if ( data.cacheDocument.isNull() )
  {
    // cache is either empty or invalid.
    return;
  }
  
  // get a timestamp out of it if possible.
  QDateTime lastChange;
  QString ts;
  
  ts = XmlFunctions::getNodeValue( data.cacheDocument, nodeName );
  lastChange = QDateTime::fromString( ts.left( ts.lastIndexOf("-") ), Qt::ISODate );

  // QDateTime doesn't handle the "-8:00" that MS adds to the end of the ISODate.
  data.cacheTimestamp = lastChange;
  data.cacheTimezone = ts.mid( ts.lastIndexOf("-") );
}



void AddressBookService::renameGroup( const KMess::MsnGroup *group, const QString &name )
{
  KMESS_ASSERT( group );

  ABGroupUpdateRequest req;
  
  ABGroup *g = new ABGroup();
  g->groupId = group->id();
  g->groupInfo.name = KMess::Utils::htmlEscape( name );
  g->propertiesChanged = "GroupName";
  
  req.groups.append( g );
  
  MessageData data;
  data.type  = "GroupRename";
  data.value = QStringList() << group->id() << name;

  sendSecureRequest( req.message( data ), CONTACTS_API );
}



void AddressBookService::removeContactFromGroup( const KMess::MsnContact *contact, const KMess::MsnGroup *group )
{
  KMESS_ASSERT( contact );
  KMESS_ASSERT( group );
  
  ABGroupContactDeleteRequest req;
  req.groupIds.append( group->id() );
  
  ABContact *c = new ABContact();
  c->contactId = contact->guid();
  req.contacts.append( c );
  
  MessageData data;
  data.type  = "ContactDeleteFromGroup";
  data.value = QStringList() << contact->guid() << group->id();

  sendSecureRequest( req.message( data ), CONTACTS_API );
}



/**
 * @brief Retrieve the address book.
 *
 * I've read about a possible small alternative to the current "ABFindAll".
 * It is:
 * <code>
GET email@domain.com/LiveContacts/?Filter=LiveContacts(Contact(ID,IMAddress)) HTTP/1.1
Host: livecontacts.live.com
Cookie: MSPAuth=..
</code>
 * It goes on with the headers as usual. Must be tested, I don't know if it works, or
 * if the response is identical to that of the current AbFindAll.
 */
void AddressBookService::retrieveAddressBook( FetchType type )
{
  // step 1: load the cache data.
  readCache( addressbookCache_, "/ABFindAllResult/ab/lastChange" );
  
  ABFindAllRequest req;
  
  if ( addressbookCache_.cacheTimestamp.isValid() )
  {
    req.deltasOnly = true;
    req.lastChange = addressbookCache_.cacheTimestamp.toString( Qt::ISODate ) + addressbookCache_.cacheTimezone;
  }
  
  MessageData data;
  data.type = "AddressBookRequest";
  data.value = type;
  sendSecureRequest( req.message( data ), CONTACTS_API );
}



/**
 * @brief Retrieve the membership lists
 */
void AddressBookService::retrieveMemberships( FetchType type )
{
  // TODO: caching of multiple services?
  // TODO: this will only work with a single service in the cache.
  //       maybe have multiple cache files for multiple services?
  //       or I'll have to smarten the logic? I don't know yet.
  //       dotMSN takes the most recent service LastChange value. 
  readCache( membershipCache_, "/FindMembershipResult/Services/Service/LastChange" );

  ABFindMembershipRequest req;
  req.serviceTypes.append( "Messenger" );
  
  if ( membershipCache_.cacheTimestamp.isValid() )
  {
    req.deltasOnly = true;
    req.lastChange = membershipCache_.cacheTimestamp.toString( Qt::ISODate ) + membershipCache_.cacheTimezone;
  }
  else
  {
    req.deltasOnly = false;
  }

//   req.serviceTypes.append( "Invitation" );
//   req.serviceTypes.append( "SocialNetwork" );
//   req.serviceTypes.append( "Space" );
//   req.serviceTypes.append( "Profile" );

  MessageData data;
  data.type = "MembershipRequest";
  data.value = type;
  sendSecureRequest( req.message( data ), CONTACTS_API );
}



/**
 * @brief Retrieve the Dynamic Items: Gleams
 */
void AddressBookService::retrieveGleams()
{
  
  ABFindAllRequest req;
  req.dynamicItemView = "Gleam";
  req.dynamicItemLastChange = "0001-01-01T00:00:00.0000000-08:00";
  
  sendSecureRequest( req.message(), CONTACTS_API );
}



// Parse the address book
void AddressBookService::parseAddressBook( const QDomElement &deltaBody, FetchType type )
{
  debug() << "Parsing address book.";
  long cid;
  int blp;
  
  // first thing we have to do is merge the cached data with the deltas.
  ABFindAllResponse *response = AddressBookHelper::mergeAddressBook( addressbookCache_, deltaBody );
  
  // save cache.
  QDomDocument doc;
  doc.setContent( SoapUtils::arg( "ABFindAllResult", response ) );
  addressbookCache_.cacheDocument = doc;
  saveCache( addressbookCache_ );

  // for a refresh only, don't process the entire cache again.
  // just go over the changes.
  if ( type == RefreshOnly )
  {
    delete response; // clear the cache pointer; we don't need it.

    response = new ABFindAllResponse();
    response->parse( deltaBody );
  }

  foreach( ABGroup *g, response->groups )
  {
    if ( g->groupId.isEmpty() || g->groupInfo.name.isEmpty() )
    {
      continue;
    }
    
    MsnGroup *group = listsManager_->group( g->groupId );
    if ( group )
    {
      if ( g->fDeleted )
      {
        listsManager_->groupRemove( g->groupId );
      }
      else
      {
        listsManager_->groupRename( g->groupId, g->groupInfo.name );
      }
    }
    else
    {
      listsManager_->groupAdd( g->groupId, g->groupInfo.name );
    }
  }
 
  QList<MsnContact*> deletedContacts;
  QList<ABContact*> newContacts;
  QList<ABContact*> updatedContacts;
  foreach( ABContact *c, response->contacts )
  {
    ContactProfile temp( this, &(c->contactInfo) );

    if ( c->contactInfo.contactType == "Me" )
    {
      c->contactInfo.isMessengerUser = true; // override this property.
      ContactProfile *p = listsManager_->self()->profile();
      if ( p )
      {
        p->setContactInfo( &(c->contactInfo) );
      }
      else
      {
        listsManager_->self()->setProfile( new ContactProfile( this, &(c->contactInfo) ) );
        p = listsManager_->self()->profile();
      }

      contacts_.insert( listsManager_->self(), c );
      response->contacts.removeAll( c );
      
      cid = p->cid();
      blp = p->annotations()[ "MSN.IM.BLP" ].toInt();
      continue;
    }
    
    QString handle;
    
    handle = temp.messengerHandle();
    
    if ( handle.isEmpty() )
    {
      // this isn't a problem. these users are not federated contacts and are not associated
      // with a Windows Live ID. consequently they can't be on the contact list.
      // for now, we can't change their profile data via this API.
      unknownProfiles_.append( new ContactProfile( NULL, &(c->contactInfo) ) );
      continue;
    }

    // grab the existing contact, or create a new one.
    MsnContact *contact = listsManager_->contact( handle );

    if ( ! contact && ! c->fDeleted )
    {
      newContacts.append( c );
    }
    else
    {
      if ( c->fDeleted )
      {
        deletedContacts.append( contact );
        updatedContacts.removeAll( c );
      }
      else
      {
        deletedContacts.removeAll( contact );
        updatedContacts.append( c );
      }
    }
    
    response->contacts.removeAll( c );
  }
  
  // process new contacts
  foreach( ABContact *c, newContacts )
  {
    ContactProfile temp( NULL, &(c->contactInfo) );

    // this could be a non-messenger user in our AB, or, a new messenger user
    // added by another endpoint that we haven't yet got membership details for.
    
    KMess::NetworkType network = KMess::NetworkUnknown;
    network = ( temp.isPassportUser() ? KMess::NetworkMsn : KMess::NetworkYahoo );
    
    MsnContact *contact = listsManager_->contactAdd( temp.messengerHandle(), 
                                          c->contactInfo.displayName, 
                                          temp.isMessengerUser() ? KMess::MSN_LIST_FRIEND : KMess::MSN_LIST_NONE, 
                                          c->contactInfo.groupIds, 
                                          c->contactId,
                                          network );

    contact->setProfile( new ContactProfile( this, &(c->contactInfo) ) );

    // only inform about new users via this signal if we're refreshing the AB.
    if ( temp.isMessengerUser() && type == RefreshOnly )
    {
      emit contactAdded( contact );
    }
    
    contacts_.insert( contact, c );
  }
  
  // process deleted contacts
  foreach( MsnContact *c, deletedContacts )
  {
    listsManager_->contactRemove( c->handle() );
    c->setMemberships( c->memberships() ^ ( c->memberships() & MSN_LIST_FRIEND ) );

    emit contactDeleted( c );
    
    ABContact *old = contacts_.take( c );
    if ( old )
    {
      delete old;
      old = 0;
    }
  }
  
  // process updated contacts.
  foreach( ABContact *c, updatedContacts )
  {
    ContactProfile temp( this, &(c->contactInfo) );
    
    MsnContact *contact = listsManager_->contact( temp.messengerHandle() );

    bool wasMsgrUser = contact->profile() && contact->profile()->isMessengerUser();

    // update the contact profile
    if ( contact != session_->self() )
    {
      ContactProfile *p = contact->profile();
      if ( p )
      {
        p->setContactInfo( &(c->contactInfo) );
      }
      else
      {
        contact->setProfile( new ContactProfile( this, &(c->contactInfo) ) );
      }
    }

    // if they weren't a messenger user before, but suddenly are now, they're a new contact.
    bool newContact = ! wasMsgrUser && temp.isMessengerUser();
    
    // don't change the display name if the contact is online.
    // more accurate information comes from NLN commands.
    if ( ! contact->isOnline() )
    {
      contact->_setDisplayName( c->contactInfo.displayName );
    }

    // update the guid - needed if we've (really) deleted then re-added a contact to the AB.
    contact->setGuid( c->contactId );

    // only contacts who are messenger users get marked as friends.
    // for those who aren't messenger users, make sure we remove any existing friend list membership.
    if ( temp.isMessengerUser() )
    {
      contact->setMemberships( contact->memberships() | KMess::MSN_LIST_FRIEND );
    }
    else
    {
      contact->setMemberships( contact->memberships() ^ ( contact->memberships() & KMess::MSN_LIST_FRIEND ) );
    }

    QList<MsnGroup*> groups;
    foreach( QString groupId, c->contactInfo.groupIds )
    {
      MsnGroup *g = listsManager_->group( groupId );
      if ( g ) groups << g;
    }

    // update groups - this will emit a signal to trigger the model move.
    contact->setGroups( groups );
    
    if ( newContact && type == RefreshOnly )
    {
      emit contactAdded( contact );
    }
    
    contacts_.insert( contact, c );
  }

  debug() << "Address book successfully parsed: found" << contacts_.count() << "contacts and" << response->groups.count() << "groups.";

  // Signal that the of address book has been parsed
  if ( type == FullFetch )
  {
    emit addressBookParsed( cid, blp );
  }

  delete response;
}



// Parse the membership lists
void AddressBookService::parseMembershipLists( const QDomElement &deltas, FetchType type )
{
  ABFindMembershipResult *response = AddressBookHelper::mergeMemberships( membershipCache_, deltas );

  QDomDocument doc;
  doc.setContent( SoapUtils::arg( "FindMembershipResult", response ) );
  membershipCache_.cacheDocument = doc;
  saveCache( membershipCache_ );

  if ( type == RefreshOnly )
  {
    delete response; // clear the cache pointer; not needed now.

    response = new ABFindMembershipResult();
    response->parse( deltas );
  }
  
  if ( response->services.count() == 0 )
  {
    delete response;
    return;
  }
  
  foreach( ServiceType *service, response->services )
  {
    KMess::MsnMemberships memberships;
    
    foreach( ABMembership *membership, service->memberships )
    {
      if     ( membership->memberRole == "Allow" ) memberships = KMess::MSN_LIST_ALLOWED;
      else if( membership->memberRole == "Block" ) memberships = KMess::MSN_LIST_BLOCKED;
      else if( membership->memberRole == "Reverse" ) memberships = KMess::MSN_LIST_REVERSE;
      else if( membership->memberRole == "Pending" ) memberships = KMess::MSN_LIST_PENDING;

      foreach( ABBaseMember *member, membership->members )
      {
        RoleInfo info;

        info.displayName = member->displayName;

        if( member->type == "Passport" )
        {
          ABPassportMember *p = (ABPassportMember *)member;
          info.handle = p->passportName;
          info.network = KMess::NetworkMsn;
        }
        else if ( member->type == "Email" )
        {
          ABEmailMember *e = (ABEmailMember *)member;
          info.handle = e->email;
          info.network = KMess::NetworkYahoo; // for now.
        }
        else
        {
          warning() << "Unhandled member type:" << member->type;
          continue;
        }
        
        if ( info.handle == "messenger@microsoft.com" )
        {
          continue; // skip?
        }
        
        if ( memberships_.contains( info.handle ) )
        {
          info.memberships = memberships_.value( info.handle ).memberships;
        }

        // handle the case of removal from a membership
        if ( member->deleted )
        {
          info.memberships ^= memberships;
        }
        else
        {
          info.memberships |= memberships;
        }
        
        memberships_.insert( info.handle, info );
      }
    }
  }
  
  // now, go through the completed memberships and update/create contacts
  foreach( RoleInfo info, memberships_ )
  {
    // if the contact exists, change its memberships.
    // if it doesn't exist, create it.
    MsnContact *c = listsManager_->contact( info.handle );

    if ( c )
    {
      // don't forget to keep the friend list membership...
      c->setMemberships( info.memberships | ( c->memberships() & KMess::MSN_LIST_FRIEND ) );
    }
    else
    {
      c = listsManager_->contactAdd( info.handle, info.displayName, info.memberships, QStringList(), QString(), info.network );
    }
    
    // user has themselves on their contact list, this is the initial login,
    // force adding self() to listsManager (and thus the model too)
    if ( c == session_->self() && type == FullFetch )
    {
      listsManager_->contactAdd( info.handle, info.displayName, info.memberships, QStringList(), QString(), KMess::NetworkMsn );
    }

    // inform the user of pending requests
    if ( c->memberships() & KMess::MSN_LIST_PENDING )
    {
      emit newPendingContact( c );
    }
  }
  
  // first login, get the address book automatically.
  if ( type == FullFetch )
  {
    retrieveAddressBook( FullFetch );
  }

  delete response;
}



/**
 * @brief  Parse a SOAP error message.
 *
 * This method interprets incoming fault messages from the server.
 *
 * @param  message   The message containing a soap fault conXML node which contains the SOAP fault.
 */
void AddressBookService::parseSecureFault( SoapMessage *message )
{
  // Should never happen
  if( ! message->isFaultMessage() )
  {
    warning() << "Incorrect fault detected on incoming message";
    return;
  }

  // Get the type of error
  QString faultCode  ( message->getFaultCode()        );
  QString faultString( message->getFaultDescription() );
  QString errorCode  ( XmlFunctions::getNodeValue( message->getFault(), "detail/errorcode" ).toLower() );

  // Get the message data
  MessageData messageData( message->getData() );
  QString     type       ( messageData.type   );
  QVariant    info       ( messageData.value  );

  // See which fault we received
  if( faultCode == "soap:Client" && ! errorCode.isEmpty() )
  {
    // We forgot to check if the email was valid:
    if( errorCode == "bademailargument" )
    {
      QString email( info.toStringList().first() );

      warning() << "Malformed email address found by the server:" << email;

      emit statusEvent( KMess::ErrorMessage, KMess::ErrorInvalidUsername, email );
    }
    // The email we tried to add is not an address registered on Passport
    else if( errorCode == "invalidpassportuser" )
    {
      QString email( info.toStringList().first() );

      emit statusEvent( KMess::ErrorMessage, KMess::ErrorInvalidUser, email );
    }
    // We messed up something in the header. Our bad, sorry!
    else if( errorCode == "invalidapplicationheader" )
    {
      warning() << "Got InvalidApplicationHeader - this should not happen!";
      emit statusEvent( KMess::ProtocolMessage, KMess::ErrorSoapInternal, message->getFaultDescription() );
    }
    // An argument name was too long
    else if( errorCode == "badargumentlength" )
    {
      emit statusEvent( KMess::ErrorMessage, KMess::ErrorGroupNameTooLong );
    }
    // This is a new account which doesn't have an address book. Create one
    else if( errorCode == "abdoesnotexist" )
    {
      debug() << "Address book does not exist: creating new one.";
      createAddressBook();
      return;
    }
    else if ( errorCode == "fullsyncrequired" || errorCode == "invalidsynctimestamp" )
    {
      debug() << "Service has told us we require a full " << type;
      
      ABCacheData *cacheData;
      if ( type == "MembershipRequest" )
      {
        cacheData = &membershipCache_;
      }
      else
      {
        cacheData = &addressbookCache_;
      }
      
      // wipe the cache file.
      if ( ! QFile( cacheData->cacheFile ).remove() )
      {
        warning() << "Unable to remove cache file " << cacheData->cacheFile << "; caching will be ignored until this is resolved.";
        cacheData->cacheFile.clear();
      }

      cacheData->cacheDocument = QDomDocument();
      cacheData->cacheTimestamp = QDateTime();

      if ( type == "MembershipRequest" )
      {
        retrieveMemberships();
      }
      else
      {
        retrieveAddressBook();
      }
    }
    else
    {
      // Do nothing, the HttpSoapConnection debug output should suffice
    }
  }
  // There was another unspecified error with our SOAP request:
  else
  {
    emit statusEvent( KMess::ErrorMessage, KMess::ErrorSoapInternal, message->getFaultDescription() );
  }
}



/**
 * @brief Parse the result of the response from the server
 *
 * We can parse two different soap replies:
 * 1) the membership lists that contains the lists of contacts and them
 *    respective roles: Allow, Block, Reverse, Pending.
 * 2) the address book list that contains the groups and the contacts with them friendlyname, id etc..
 *
 * @param message the SOAP response message.
 * @see PassportLoginService::parseSoapResult().
 */
void AddressBookService::parseSecureResult( SoapMessage *message )
{
  QDomElement body( message->getBody().toElement() );
  QString     resultName( body.localName() );

  if( ! body.firstChildElement( "FindMembershipResponse" ).isNull() )
  {
    parseMembershipLists( body.firstChildElement( "FindMembershipResponse" ).firstChildElement( "FindMembershipResult" ), (FetchType)( message->getData().value.toInt() ) );
    return;
  }
  else if( ! body.firstChildElement( "ABFindAllResponse" ).isNull() )
  {
    parseAddressBook( body.firstChildElement( "ABFindAllResponse" ).firstChildElement( "ABFindAllResult" ), (FetchType)( message->getData().value.toInt() ) );
    return;
  }
  else if( ! body.firstChildElement( "DeleteMemberResponse" ).isNull() )
  {
    // Contact removed from Membership list from selected Role
    return;
  }
  else if( ! body.firstChildElement( "ABAddResponse" ).isNull() )
  {
    // New address book was created: proceed with it
    debug() << "Address book successfully created: retrying to obtain membership lists.";
    retrieveMemberships( FullFetch );
    return;
  }

  // Get the message's data
  MessageData data( message->getData() );

  // contact added to address book - make sure they're on the friend list
  // and send a request for their profile information.
  if( data.type == "ContactAdd" )
  {
    // A contact was added to the AB
          QStringList  info     ( data.value.toStringList() );
    const QString&     handle   ( info.takeFirst() );
    NetworkType        network = (NetworkType) ( info.takeFirst().toInt() );
    const QStringList& groupsId ( info );
    const QString&     contactId( body.elementsByTagName( "guid" ).item(0).toElement().text() );
    
    // If the contact is valid, signal that the contact was added to AB
    if( ! contactId.isEmpty() && ! handle.isEmpty() )
    {
      MsnContact *c = listsManager_->contact( handle );
      
      if ( ! c )
      {
        listsManager_->contactAdd( handle, 
                                   handle, 
                                   MSN_LIST_FRIEND | MSN_LIST_ALLOWED, 
                                   QStringList(), contactId, network );

        c = listsManager_->contact( handle );
      }
      else
      {
        // only change here is to ensure that the contact's memberships reflect
        // their presence on our friend list
        c->setMemberships( c->memberships() | MSN_LIST_FRIEND );
      }

      foreach( const QString &groupId, groupsId )
      {
        addContactToGroup( c, listsManager_->group( groupId ) );
      }
      
      // ask for their profile by refreshing AB and memberships
      // profile may have changed, so might have their memberships.
      retrieveMemberships( RefreshOnly );
      retrieveAddressBook( RefreshOnly );

      emit contactAdded( c );
    }
  }
  else if( data.type == "ContactDelete" )
  {
    // A contact was deleted from the AB
    // we've already signalled the deletion via contactDeleted.
    const QString& contactId( data.value.toString() );

    if( ! contactId.isEmpty() )
    {
      MsnContact *c = listsManager_->contact( contactId );
      KMESS_ASSERT( c );

      c->setMemberships( c->memberships() ^ ( c->memberships() & MSN_LIST_FRIEND ) );
      
      listsManager_->contactRemove( c->handle() );
    }
  }
  /*
  If a contact is added to the Allowed or Blocked list, then
  emit contactBlocked/contactUnblocked as appropriate.
  */
  else if ( data.type.contains( "ListAdd" ) )
  {
    QString handle = data.value.toString();
    
    KMess::MsnContact *c = listsManager_->contact( handle );
    KMESS_ASSERT( c ); // c must be valid!

    KMess::MsnMemberships lists = c->memberships();

    if ( data.type == "ListAdd-" + QString::number( KMess::MSN_LIST_BLOCKED ) )
    {      
      c->setMemberships( ( lists | KMess::MSN_LIST_BLOCKED ) ^ ( lists & KMess::MSN_LIST_ALLOWED ) );
      emit contactBlocked( c );
    }
    else if ( data.type == "ListAdd-" + QString::number( KMess::MSN_LIST_ALLOWED ) )
    {
      c->setMemberships( ( lists | KMess::MSN_LIST_ALLOWED) ^ ( lists & KMess::MSN_LIST_BLOCKED ) );
      emit contactUnblocked( c );
    }
  }
  else if( data.type == "ContactAddToGroup" )
  {
    // A contact was added to a group

    const QString& groupId  ( data.value.toString() );
    const QString& contactId( body.elementsByTagName( "guid" ).item(0).toElement().text() );

    if( ! contactId.isEmpty() && ! groupId.isEmpty() )
    {
      MsnGroup *g = listsManager_->group( groupId );
      MsnContact *c = listsManager_->contact( contactId );
      
      KMESS_ASSERT( g );
      KMESS_ASSERT( c );
      
      listsManager_->contactAddToGroup( contactId, groupId );
      
      emit contactAddedToGroup( c, g );
    }
  }
  else if( data.type == "ContactDeleteFromGroup" )
  {
    // A contact was removed from a group

    const QStringList& info     ( data.value.toStringList() );
    const QString&     contactId( info.value( 0 ) );
    const QString&     groupId  ( info.value( 1 ) );

    if( ! contactId.isEmpty() && ! groupId.isEmpty() )
    {
      MsnGroup *g = listsManager_->group( groupId );
      MsnContact *c = listsManager_->contact( contactId );
      
      KMESS_ASSERT( g );
      KMESS_ASSERT( c );
      
      listsManager_->contactRemoveFromGroup( contactId, groupId );
      
      emit contactRemovedFromGroup( c, g );
    }
  }
  else if( data.type == "GroupAdd" )
  {
    // A group was added
    const QString& name   ( data.value.toString() );
    const QString& groupId( body.elementsByTagName( "guid" ).item(0).toElement().text() );

    if( ! groupId.isEmpty() && ! name.isEmpty() )
    {
      listsManager_->groupAdd( groupId, name );
      
      MsnGroup *g = listsManager_->group( groupId );
      KMESS_ASSERT( g );
      
      emit groupAdded( g );
    }
  }
  else if( data.type == "GroupDelete" )
  {
    // A group was removed
    const QString& groupId( data.value.toString() );

    if( ! groupId.isEmpty() )
    {
      emit groupRemoved( listsManager_->group( groupId ) );
      listsManager_->groupRemove( groupId );
    }
  }
  else if( data.type == "GroupRename" )
  {
    // A group was renamed
    const QStringList& info   ( data.value.toStringList() );
    const QString&     groupId( info.value( 0 ) );
    const QString&     name   ( info.value( 1 ) );

    if( ! groupId.isEmpty() && ! name.isEmpty() )
    {
      MsnGroup *g = listsManager_->group( groupId );
      
      KMESS_ASSERT( g );
      
      emit groupRenamed( g );
    }
  }
  else if ( data.type == "ContactUpdate" )
  {
    // we added/deleted a contact. the memberships will change automatically, but
    // we need to retrieve them.
    if ( data.value.toStringList().contains( "IsMessengerUser" ) ||
         data.value.toStringList().contains( "IsMessengerEnabled" ) )
    {
      retrieveMemberships( RefreshOnly );
    }
  }
  else
  {
    warning() << "Response data not recognized:" << data.type;
  }
}



// Save cache to a file.
// Call this after you're done with cacheDoc_, because it will be thrown away after this
// method finishes.
void AddressBookService::saveCache( ABCacheData &data )
{
  QFile cache( data.cacheFile );
  
  if ( ! cache.open( QIODevice::WriteOnly ) )
  {
    warning() << "Cannot open the cache file (" << data.cacheFile << ") for writing; address book caching will not occur.";
    return;
  }
  
  // put the last save information in the file...
  // tell QDomDocument to dump itself to the cache file...
  QTextStream stream( &cache );
  stream.setCodec( QTextCodec::codecForName( "UTF-8" ) );
  stream.setAutoDetectUnicode( true );
  data.cacheDocument.save( stream, 2 );  
  cache.close();
  
  debug() << "Wrote cache to" << data.cacheFile;

  // done!
}


// Unblock contact
void AddressBookService::unblockContact( const KMess::MsnContact *contact )
{
  KMESS_ASSERT( contact );
   
  if ( contact->isBlocked() )
  {
    removeContactFromList( contact, MSN_LIST_BLOCKED );
  }
  
  if ( ! contact->isAllowed() )
  {
    addContactToList( contact, KMess::MSN_LIST_ALLOWED );
  }
}



/*
 * Unknown profiles are ones that belong to address book contacts who themselves are in no
 * way connected to Messenger. Since these contacts can't become MsnContacts, we store them here.
 * 
 * If a user wants to dig through them, so be it.
 */
QList<KMess::ContactProfile*> AddressBookService::unknownProfiles() const
{
  return unknownProfiles_;
}



/**
 * Adds a contact to the given membership role.
 *
 * @param contact MsnContact you wish to add.
 * @param role The role you wish to add the contact to.
 */
void AddressBookService::addContactToList( const KMess::MsnContact *contact, KMess::MsnMembership role )
{
  KMESS_ASSERT( contact );
  
  AddMemberRequest req;
  
  req.memberships.append( createMembership( contact, role ) );
  
  MessageData data;
  data.type = "ListAdd-" + QString::number( role );
  data.value = contact->handle();
  
  // have to use ContactSave partner scenario for changing the RL.
  if ( role == KMess::MSN_LIST_REVERSE )
  {
    req.setPartnerScenario( "ContactSave" );
  }
  
  sendSecureRequest( req.message( data ), CONTACTS_API );
}



void AddressBookService::removeContactFromList( const KMess::MsnContact *contact, KMess::MsnMembership role )
{
  KMESS_ASSERT( contact );
  
  DeleteMemberRequest req;
  
  req.memberships.append( createMembership( contact, role ) );
  
  MessageData data;
  data.type = "ListRemove-" + QString::number( role );
  data.value = contact->handle();
  
  // have to use ContactSave partner scenario for changing the RL.
  if ( role == KMess::MSN_LIST_REVERSE )
  {
    req.setPartnerScenario( "ContactSave" );
  }
  
  sendSecureRequest( req.message( data ), CONTACTS_API );
}



/**
 * Creates an ABMembership structure which represents a membership update for the given contact in a role
 * @param contact The contact you wish to update
 * @param role The MsnMembership you wish to add the contact to.
 */
ABMembership *AddressBookService::createMembership( const KMess::MsnContact *contact, KMess::MsnMembership role )
{
  ABMembership *m = new ABMembership();
  ABBaseMember *mem;
  
  switch( role )
  {
    case KMess::MSN_LIST_ALLOWED:
      m->memberRole = "Allow";
      break;
      
    case KMess::MSN_LIST_BLOCKED:
      m->memberRole = "Block";
      break;
      
    case KMess::MSN_LIST_REVERSE:
      m->memberRole = "Reverse";
      break;
      
    case KMess::MSN_LIST_PENDING:
      m->memberRole = "Pending";
      break;
      
    default:
      warning() << "Cannot handle membership role:" << role;
      return 0;
  }
  
  if ( contact->network() == KMess::NetworkMsn )
  {
    mem = new ABPassportMember();
    ABPassportMember *p = (ABPassportMember *)mem;
    
    p->passportName = contact->handle();
    p->state = "Accepted";
  }
  else
  {
    mem = new ABEmailMember();
    ABEmailMember *e = (ABEmailMember *)mem;
    e->email = contact->handle();
    e->state = "Accepted";
  }
  
  m->members.append( mem );
  m->membershipIsComplete = true;
  return m;
}

