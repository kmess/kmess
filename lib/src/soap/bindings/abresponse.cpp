/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Adam Goossens <adam@kmess.org>                            *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file abresponse.cpp
 */

#include <QString>

#include "abbinding.h"
#include "soaputils.h"
#include "../../debug/libkmessdebug.h"
  
using namespace KMessInternal::SoapUtils;
using namespace KMessInternal;

/**********************************************
ABFindAllResponse
***********************************************/
ABFindAllResponse::~ABFindAllResponse()
{
  qDeleteAll( groups );
  qDeleteAll( contacts );
}


QString ABFindAllResponse::toString() const
{
  QString body;
  body += arg<ABGroup*>( "groups", "Group", groups );
  body += arg<ABContact*>( "contacts", "Contact", contacts );
  
  QString ab;
  ab += arg( "abId", abId );
  ab += arg( "abInfo", abInfo );
  ab += arg( "lastChange", lastChange );
  ab += arg( "dynamicItemLastChanged", dynamicItemLastChanged );
  ab += arg( "createDate", createDate );
  ab += arg( "propertiesChanged", propertiesChanged );
  
  body += arg( "ab", ab, QString(), NoEscape );
  return body;
}

ABFindAllResponse::operator QString() const
{
  return toString();
}

void ABFindAllResponse::parse( const QDomNode &node )
{
  parseSoapTypeList<ABGroup>( node, "groups", "Group", groups );
  parseSoapTypeList<ABContact>( node, "contacts", "Contact", contacts );
  
  const QDomNode &ab = node.toElement().firstChildElement( "ab" );
  parseString( ab, "abId", abId );
  parseSoapType( ab, "abInfo", abInfo );
  parseString( ab, "lastChange", lastChange );
  parseString( ab, "dynamicItemLastChanged", dynamicItemLastChanged );
  parseString( ab, "createDate", createDate );
  parseString( ab, "propertiesChanged", propertiesChanged );
}



/**********************************************
ABFindMembershipResult
***********************************************/
ABFindMembershipResult::~ABFindMembershipResult()
{
  qDeleteAll( services );
}


QString ABFindMembershipResult::toString() const
{
  QString body;
  body += arg<ServiceType*> ( "Services", "Service", services );
  body += arg ( "OwnerNamespace", ownerNamespace );
  return body;
}

ABFindMembershipResult::operator QString() const
{
  return toString();
}

void ABFindMembershipResult::parse( const QDomNode &node )
{
  parseSoapTypeList<ServiceType>( node, "Services", "Service", services );
  parseSoapType( node, "OwnerNamespace", ownerNamespace );
}
