/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Adam Goossens <adam@kmess.org>                            *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file abrequests.h
 */

#ifndef ABREQUESTS_H
#define ABREQUESTS_H

#include <QByteArray>
#include <QList>

#include "abdatatypes.h"
#include "../soapmessage.h"

namespace KMessInternal
{

class ABRequest
{
  protected:
    ABRequest( QString method, QString partnerScenario, QString url );
    
  public:
    virtual ~ABRequest() {}

    SoapMessage *message( MessageData data = MessageData() );
    void setPartnerScenario( const QString &partnerScenario );

  protected:
    virtual QString body() = 0;
    
  private:
    QString createCommonHeader();

  private:
    QString method_;
    QString scenario_;
    QString action_;
    QString url_;
};

class ABContactAddRequest : public ABRequest
{
  public:
    ABContactAddRequest();
    virtual ~ABContactAddRequest();

  protected:
    virtual QString body();
    
  public:
    QList<ABContact*> contacts;
    bool              enableAllowListManagement;
};

class ABFindAllRequest : public ABRequest
{
  public:
    ABFindAllRequest();
    virtual ~ABFindAllRequest() {}
    
  protected:
    virtual QString body();
    
  public:
    bool    deltasOnly;
    QString lastChange;
    QString dynamicItemView;
    QString dynamicItemLastChange;
};

class ABFindMembershipRequest : public ABRequest
{
  public:
    ABFindMembershipRequest();
    virtual ~ABFindMembershipRequest() {}
    
  protected:
    virtual QString body();
    
  public:
    QList<QString> serviceTypes;
    QString        lastChange;
    bool           deltasOnly;

};


class ABGroupContactAddRequest : public ABRequest
{
  public:
    ABGroupContactAddRequest();
    virtual ~ABGroupContactAddRequest() {}
    
  protected:
    virtual QString body();
    
  public:
    QList<QString>  groupIds;
    QString         contactId;
};


class ABAddRequest : public ABRequest
{
  public:
    ABAddRequest();
    
  protected:
    virtual QString body();
    
  public:
    ABInfo  abInfo;
};



class ABGroupAddRequest : public ABRequest
{
  public:
    ABGroupAddRequest();
    
  protected:
    virtual QString body();
    
  public:
    ABGroupInfo groupInfo;
};


class ABGroupUpdateRequest : public ABRequest
{
  public:
    ABGroupUpdateRequest();
    virtual ~ABGroupUpdateRequest();

  protected:
    virtual QString body();
    
  public:
    QList<ABGroup*> groups;
};

class ABContactUpdateRequest : public ABRequest
{
  public:
    ABContactUpdateRequest();
    virtual ~ABContactUpdateRequest();

  protected:
    virtual QString body();
    
  public:
    QList<ABContact*> contacts;
};

class ABContactDeleteRequest : public ABRequest
{
  public:
    ABContactDeleteRequest();
    virtual ~ABContactDeleteRequest();

  protected:
    virtual QString body();
    
  public:
    QList<ContactIdType*> contacts;
};

class ABGroupContactDeleteRequest : public ABRequest
{
  public:
    ABGroupContactDeleteRequest();
    virtual ~ABGroupContactDeleteRequest();
    
  protected:
    virtual QString body();
    
  public:
    QList<QString> groupIds;
    QList<ABContact*> contacts;
};


class ABGroupDeleteRequest : public ABRequest
{
  public:
    ABGroupDeleteRequest();
    
  protected:
    virtual QString body();
    
  public:
    QList<QString> groupIds;
};


class AddMemberRequest : public ABRequest
{
  public:
    AddMemberRequest();
    ~AddMemberRequest();

  protected:
    virtual QString body();
    
  public:
    HandleType serviceHandle;
    QList<ABMembership*> memberships;
};


class DeleteMemberRequest : public ABRequest
{
  public:
    DeleteMemberRequest();
    ~DeleteMemberRequest();

  protected:
    virtual QString body();
    
  public:
    HandleType serviceHandle;
    QList<ABMembership*> memberships;
};

}; // namespace

#endif
