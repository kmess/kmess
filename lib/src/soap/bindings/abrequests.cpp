/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Adam Goossens <adam@kmess.org>                            *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file abrequests.cpp
 */

#include <QString>

#include "abbinding.h"
#include "soaputils.h"
#include "../../debug/libkmessdebug.h"

/**
 * @brief URL of the Address Book Service
 */
#define SERVICE_URL_ADDRESSBOOK           "https://omega.contacts.msn.com/abservice/abservice.asmx"

/**
 * @brief URL of the Address Book Sharing Service
 */
#define SERVICE_URL_ADDRESSBOOK_SHARING   "https://omega.contacts.msn.com/abservice/SharingService.asmx"

using namespace KMessInternal;
using namespace KMessInternal::SoapUtils;

ABRequest::ABRequest( QString method, QString partnerScenario, QString url )
: method_( method )
, scenario_( partnerScenario )
, url_ ( url )
{
  scenario_ = scenario_.isEmpty() ? "Initial" : scenario_;
};

SoapMessage* ABRequest::message( MessageData data )
{
  QString bodyStr;
  bodyStr += '<' + method_ + " xmlns=\"http://www.msn.com/webservices/AddressBook\">\n";
  bodyStr += "<abId>00000000-0000-0000-0000-000000000000</abId>\n";
  bodyStr += body();
  bodyStr += "</" + method_ + '>';
  
  return new SoapMessage( url_,
                          "http://www.msn.com/webservices/AddressBook/" + method_,
                          createCommonHeader(),
                          bodyStr,
                          data );
}


/**
 * @brief Create the common header for the soap requests.
 */
QString ABRequest::createCommonHeader()
{
  return "<ABApplicationHeader xmlns=\"http://www.msn.com/webservices/AddressBook\">\n"
         "  <ApplicationId xmlns=\"http://www.msn.com/webservices/AddressBook\">\n"
         "    996CDE1E-AA53-4477-B943-2BE802EA6166\n"
         "  </ApplicationId>\n"
         "  <IsMigration xmlns=\"http://www.msn.com/webservices/AddressBook\">false</IsMigration>\n"
         "  <PartnerScenario xmlns=\"http://www.msn.com/webservices/AddressBook\">\n"
         + scenario_ +
         "  </PartnerScenario>\n"
         "</ABApplicationHeader>\n"
         "<ABAuthHeader xmlns=\"http://www.msn.com/webservices/AddressBook\">\n"
         "  <ManagedGroupRequest xmlns=\"http://www.msn.com/webservices/AddressBook\">false</ManagedGroupRequest>\n"
         "  <TicketToken />\n" // Gets filled in by the base class
         "</ABAuthHeader>";
}


/**
 * Change the partner scenario for this request.
 * Mostly useful for the Add/Delete contact from list
 * actions which must operate either on the "ContactSave"
 * scenario or the "BlockUnblock" scenario.
 *
 * @param partnerScenario The new partnerScenario to use.
 */
void ABRequest::setPartnerScenario( const QString &partnerScenario )
{
  scenario_ = partnerScenario;
}


/***********************************
 START ABCONTACTADD
************************************/

ABContactAddRequest::ABContactAddRequest()
: ABRequest( "ABContactAdd", "ContactSave", SERVICE_URL_ADDRESSBOOK )
{
}



ABContactAddRequest::~ABContactAddRequest()
{
  qDeleteAll( contacts );
}



QString ABContactAddRequest::body()
{
  QString body;
  body += arg<ABContact*>( "contacts", "Contact", contacts );
  body += arg( "EnableAllowListManagement", enableAllowListManagement );
  return body;
}


/***********************************
 START ABFINDALL
************************************/

ABFindAllRequest::ABFindAllRequest()
: ABRequest( "ABFindAll", "", SERVICE_URL_ADDRESSBOOK )
{
  deltasOnly = false;
}

QString ABFindAllRequest::body()
{
  QString body;
  body += arg( "abView", QString( "MessengerClient8" ) );
  body += arg( "deltasOnly", deltasOnly );
  body += arg( "lastChange", lastChange, "0001-01-01T00:00:00.0000000-08:00" );
  body += arg( "dynamicItemVIew", dynamicItemView );
  body += arg( "dynamicItemLastChange", QString( "0001-01-01T00:00:00.0000000-08:00" ) );
  return body;
}


/***********************************
 START ABFINDMEMBERSHIP
************************************/
ABFindMembershipRequest::ABFindMembershipRequest()
: ABRequest( "FindMembership", "", SERVICE_URL_ADDRESSBOOK_SHARING )
{
}


QString ABFindMembershipRequest::body()
{
  QString body;
  body += arg( "serviceFilter", arg( "Types", "ServiceType", serviceTypes ), QString(), NoEscape );  
  body += arg( "deltasOnly", deltasOnly );
  body += arg( "lastChange", lastChange, "0001-01-01T00:00:00.0000000-08:00" );
  return body;
}


/***********************************
 START ABGroupContactAddRequest
************************************/
ABGroupContactAddRequest::ABGroupContactAddRequest()
: ABRequest( "ABGroupContactAdd", "GroupSave", SERVICE_URL_ADDRESSBOOK )
{
}


QString ABGroupContactAddRequest::body()
{
  QString body;
  body += arg( "groupFilter", arg( "groupIds", "guid", groupIds ), QString(), NoEscape ); 
  body += arg( "contacts", arg( "Contact", arg( "contactId", contactId ), QString(), NoEscape ), QString(), NoEscape );
  
  return body;
}


/***********************************
 START ABAdd
************************************/
ABAddRequest::ABAddRequest()
: ABRequest( "ABAdd", "", SERVICE_URL_ADDRESSBOOK )
{
}


QString ABAddRequest::body()
{
  return abInfo.toString();
}


/***********************************
 START ABGroupAdd
************************************/
ABGroupAddRequest::ABGroupAddRequest()
: ABRequest( "ABGroupAdd", "GroupSave", SERVICE_URL_ADDRESSBOOK )
{
}


QString ABGroupAddRequest::body()
{
  QString body;
  body += arg( "groupAddOptions", arg( "fRenameOnMsgrConflict", false ), QString(), NoEscape );
  body += arg( "groupInfo", arg( "GroupInfo", groupInfo ), QString(), NoEscape );
  
  return body;
}


/***********************************
 START ABGroupUpdate
************************************/
ABGroupUpdateRequest::ABGroupUpdateRequest()
: ABRequest( "ABGroupUpdate", "GroupSave", SERVICE_URL_ADDRESSBOOK )
{
}



ABGroupUpdateRequest::~ABGroupUpdateRequest()
{
  qDeleteAll( groups );
}



QString ABGroupUpdateRequest::body()
{
  QString body;

  body += arg<ABGroup*>( "groups", "Group", groups );
  
  return body;
}


/***********************************
 START ABContactUpdate
************************************/
ABContactUpdateRequest::ABContactUpdateRequest()
: ABRequest( "ABContactUpdate", "Timer", SERVICE_URL_ADDRESSBOOK )
{
}



ABContactUpdateRequest::~ABContactUpdateRequest()
{
  qDeleteAll( contacts );
}



QString ABContactUpdateRequest::body()
{
  QString body;

  body += arg<ABContact*>( "contacts", "Contact", contacts );
  
  return body;
}


/***********************************
 START ABContactDelete
************************************/
ABContactDeleteRequest::ABContactDeleteRequest()
: ABRequest( "ABContactDelete", "Timer", SERVICE_URL_ADDRESSBOOK )
{
}



ABContactDeleteRequest::~ABContactDeleteRequest()
{
  qDeleteAll( contacts );
}



QString ABContactDeleteRequest::body()
{
  QString body;
  body += arg( "contacts", "Contact", contacts );
  return body;
}


/***********************************
 START ABGroupContactDelete
************************************/
ABGroupContactDeleteRequest::ABGroupContactDeleteRequest()
: ABRequest( "ABGroupContactDelete", "GroupSave", SERVICE_URL_ADDRESSBOOK )
{
}


ABGroupContactDeleteRequest::~ABGroupContactDeleteRequest()
{
  qDeleteAll( contacts );
}


QString ABGroupContactDeleteRequest::body()
{
  QString body;

  body += arg( "groupFilter", arg( "groupIds", "guid", groupIds ), QString(), NoEscape ); 
  body += arg<ABContact*>( "contacts", "Contact", contacts );
  
  return body;
}


/***********************************
 START ABGroupDelete
************************************/
ABGroupDeleteRequest::ABGroupDeleteRequest()
: ABRequest( "ABGroupDelete", "GroupSave", SERVICE_URL_ADDRESSBOOK )
{
}


QString ABGroupDeleteRequest::body()
{
  QString body;

  body += arg( "groupFilter", arg( "groupIds", "guid", groupIds ), QString(), NoEscape ); 
  
  return body;
}



/***********************************
 START AddMemberRequest
************************************/
AddMemberRequest::AddMemberRequest()
: ABRequest( "AddMember", "BlockUnblock", SERVICE_URL_ADDRESSBOOK_SHARING )
{
  // defaults.
  serviceHandle.id = 0;
  serviceHandle.type = "Messenger";
  serviceHandle.foreignId = "";
}



AddMemberRequest::~AddMemberRequest()
{
  qDeleteAll( memberships );
}



QString AddMemberRequest::body()
{
  QString body;

  body += arg( "serviceHandle", serviceHandle );
  body += arg( "memberships", "Membership", memberships );
  
  return body;
}


/***********************************
 START DeleteMemberRequest
************************************/
DeleteMemberRequest::DeleteMemberRequest()
: ABRequest( "DeleteMember", "BlockUnblock", SERVICE_URL_ADDRESSBOOK_SHARING )
{
  // defaults.
  serviceHandle.id = 0;
  serviceHandle.type = "Messenger";
  serviceHandle.foreignId = "";
}



DeleteMemberRequest::~DeleteMemberRequest()
{
  qDeleteAll( memberships );
}


QString DeleteMemberRequest::body()
{
  QString body;

  body += arg( "serviceHandle", serviceHandle );
  body += arg( "memberships", "Membership", memberships );
  
  return body;
}
