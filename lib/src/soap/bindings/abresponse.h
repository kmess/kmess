/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Adam Goossens <adam@kmess.org>                            *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file abresponse.h
 */

#ifndef ABRESPONSE_H
#define ABRESPONSE_H

#include <QByteArray>
#include <QList>

#include "abdatatypes.h"
#include "../soapmessage.h"

namespace KMessInternal
{

/**
 * Represents the result of an ABFindAll request.
 */
struct ABFindAllResponse : public SoapType
{
  QList<ABGroup*>       groups;
  QList<ABContact*>     contacts;
  QString               abId;
  ABInfo                abInfo;
  QString               lastChange;
  QString               dynamicItemLastChanged;
  QString               createDate;
  QString               propertiesChanged;
  
  ~ABFindAllResponse();
  virtual QString toString() const;
  virtual operator QString() const;
  virtual void parse( const QDomNode &node );
};



/**
 * Represents the result of an ABFindAll request.
 */
struct ABFindMembershipResult : public SoapType
{
  QList<ServiceType*>   services;
  OwnerNamespaceType    ownerNamespace;
  
  ~ABFindMembershipResult();
  virtual QString toString() const;
  virtual operator QString() const;
  virtual void parse( const QDomNode &node );
};

}; // namespace

#endif
