/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Adam Goossens <adam@kmess.org>                            *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file abdatatypes.cpp
 */

#include "abdatatypes.h"
#include "soaputils.h"

#include "../../debug/libkmessdebug.h"
#include "../../utils/xmlfunctions.h"

#include <KMess/Utils>

using namespace KMessInternal;
using namespace KMessInternal::SoapUtils;

QString argMember( QString elementName, ABBaseMember *member );
QString argMember( QString elementName, ABBaseMember *member )
{
  return QString( "<%1 xsi:type=\"%2\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">%3</%1>" ).arg( elementName ).arg( member->type + "Member" ).arg( member->toString() );
}

/**********************************************
ABAnnotation
***********************************************/
ABAnnotation::ABAnnotation( QString annoteName, QString annoteValue )
: name( annoteName ),
  value( annoteValue )
{
}


QString ABAnnotation::toString() const
{
  QString body;

  body += arg( "Name", name );
  body += arg( "Value", value );

  return body;
}


void ABAnnotation::parse( const QDomNode &node )
{
  parseString( node, "Name", name );
  parseString( node, "Value", value );
}


/**********************************************
ABContact
***********************************************/
QString ABContact::toString() const
{
  QString body;

  body += arg( "contactId", contactId );
  body += arg( "contactInfo", contactInfo );
  body += arg( "propertiesChanged", propertiesChanged );
  body += arg( "fDeleted", fDeleted );
  body += arg( "CreateDate", createDate );
  body += arg( "lastChange", lastChange );

  return body;
}



void ABContact::parse( const QDomNode &node )
{
  parseString   ( node, "contactId", contactId );
  parseSoapType ( node, "contactInfo", contactInfo );
  parseString   ( node, "propertiesChanged", propertiesChanged );
  parseBool     ( node, "fDeleted", fDeleted );
  parseString   ( node, "CreateDate", createDate );
  parseString   ( node, "lastChange", lastChange );
}


/**********************************************
ABContactInfo
***********************************************/
ABContactInfo::ABContactInfo()
: isMessengerUser( false )
{
}



ABContactInfo::~ABContactInfo()
{
  qDeleteAll( emails );
  qDeleteAll( phones );
  qDeleteAll( annotations );
}



QString ABContactInfo::toString() const
{
  QString body;

  body += arg<ABContactEmail*>( "emails", "ContactEmail", emails );
  body += arg<ABContactPhone*>( "phones", "ContactPhone", phones );
  body += arg<ABAnnotation*>( "annotations", "Annotation", annotations );
  body += arg<QString>( "groupIds", "guid", groupIds );
  body += arg( "contactType", contactType );
  body += arg( "quickName", quickName );
  body += arg( "firstName", firstName );
  body += arg( "lastName", lastName );
  body += arg( "MiddleName", middleName );
  body += arg( "passportName", passportName );
  body += arg( "displayName", displayName );
  body += arg( "CID", cid );
  body += arg( "isNotMobileVisible", isNotMobileVisible );
  body += arg( "isMobileIMEnabled", isMobileIMEnabled );
  body += arg( "isMessengerUser", isMessengerUser );
  body += arg( "isFavorite", isFavorite );
  body += arg( "hasSpace", hasSpace );
  body += arg( "propertiesChanged", propertiesChanged );
  

  return body;
}



void ABContactInfo::parse( const QDomNode &node )
{
  parseSoapTypeList<ABContactEmail> ( node, "emails", "ContactEmail", emails );
  parseSoapTypeList<ABContactPhone> ( node, "phones", "ContactPhone", phones );
  parseList                         ( node, "groupIds", "guid", groupIds );
  parseSoapTypeList<ABAnnotation>( node, "annotations", "Annotation", annotations );
  parseString( node, "contactType", contactType );
  parseString( node, "quickName", quickName );
  parseString( node, "firstName", firstName );
  parseString( node, "lastName", lastName );
  parseString( node, "MiddleName", middleName );
  parseString( node, "passportName", passportName );
  parseString( node, "displayName", displayName );
  parseLong( node, "CID", cid );
  parseBool( node, "isNotMobileVisible", isNotMobileVisible );
  parseBool( node, "isMobileIMEnabled", isMobileIMEnabled );
  parseBool( node, "isMessengerUser", isMessengerUser );
  parseBool( node, "isFavorite", isFavorite );
  parseBool( node, "hasSpace", hasSpace );
  parseString( node, "propertiesChanged", propertiesChanged );
}


/**********************************************
ABContactPhone
***********************************************/
QString ABContactPhone::toString() const
{
  QString body;
  body += arg( "contactPhoneType", contactPhoneType );
  body += arg( "number", number );
  body += arg( "isMessengerEnabled", isMessengerEnabled );
  return body;
}


void ABContactPhone::parse( const QDomNode &node )
{
  parseString( node, "contactPhoneType", contactPhoneType );
  parseString( node, "number", number );
  parseBool  ( node, "isMessengerEnabled", isMessengerEnabled );
}


/**********************************************
ABContactEmail
***********************************************/
QString ABContactEmail::toString() const
{
  QString body;
  body += arg( "email", email );
  body += arg( "contactEmailType", contactEmailType );
  body += arg( "isMessengerEnabled", isMessengerEnabled );
  body += arg( "Capability", capability );
  body += arg( "propertiesChanged", propertiesChanged );
  return body;
}



void ABContactEmail::parse( const QDomNode &node )
{
  parseString( node, "contactEmailType", contactEmailType );
  parseBool  ( node, "isMessengerEnabled", isMessengerEnabled );
  parseLong  ( node, "Capability", capability );
  parseString( node, "propertiesChanged", propertiesChanged );
  parseString( node, "email", email );
}


/**********************************************
ABGroupInfo
***********************************************/
ABGroupInfo::~ABGroupInfo()
{
  qDeleteAll( annotations );
}



QString ABGroupInfo::toString() const
{
  QString body;
  
  body += arg( "groupType", groupType, "C8529CE2-6EAD-434d-881F-341E17DB3FF8" );
  body += arg<ABAnnotation*>( "annotations", "Annotation", annotations );
  body += arg( "name", name );
  body += arg( "IsNotMobileVisible", isNotMobileVisible );
  body += arg( "IsPrivate", isPrivate );
  body += arg( "IsFavorite", isFavorite );
  body += arg( "fMessenger", fMessenger );
  return body;
}



void ABGroupInfo::parse( const QDomNode &node )
{
  parseString( node, "groupType", groupType, "C8529CE2-6EAD-434d-881F-341E17DB3FF8" );
  parseSoapTypeList<ABAnnotation>( node, "annotations", "Annotation", annotations );
  parseString( node, "name", name );
  parseBool( node, "IsNotMobileVisible", isNotMobileVisible );
  parseBool( node, "IsPrivate", isPrivate );
  parseBool( node, "IsFavorite", isFavorite );
  parseBool( node, "fMessenger", fMessenger );
}


/**********************************************
ABMembership
***********************************************/
ABMembership::~ABMembership()
{
  qDeleteAll( members );
}

QString ABMembership::toString() const
{
  QString body;
  body += arg( "MemberRole", memberRole );
  
  QString memberXml;

  foreach( ABBaseMember *member, members )
  {
    memberXml += argMember( "Member", member );
  }
  
  body += arg( "Members", memberXml, QString(), NoEscape );
  body += arg( "MembershipIsComplete", membershipIsComplete );
  return body;
}



void ABMembership::parse( const QDomNode &node )
{
  parseString   ( node, "MemberRole",           memberRole );
  
  const QDomNodeList memberList = node.toElement().elementsByTagName( "Member" );
  for( int i = 0; i < memberList.count(); i++ )
  {
    const QDomNode &m = memberList.at( i );

    QString type = XmlFunctions::getNodeAttribute( m, "xsi:type" );
    type = ( type.isEmpty() ? XmlFunctions::getNodeAttribute( m, "type" ) : type );
    
    if ( type.isEmpty() )
    {
      warning() << "Got empty type for member.";
      continue;
    }
    
    ABBaseMember *mem = 0;
    
    if ( type == "PassportMember" )
    {
      mem = new ABPassportMember();
    }
    else if ( type == "EmailMember" )
    {
      mem = new ABEmailMember();
    }
    else
    {
      warning() << "Unknown member type:" << type;
    }
//     else if ( type == "Circle" )
//     {
//       mem = new ABCircleMember();
//     }
    
    if ( mem )
    {
      mem->parse( m );
      members.append( mem );
    }
  }
  
  parseBool     ( node, "MembershipIsComplete", membershipIsComplete );
}



/**********************************************
ABBaseMember
***********************************************/
ABBaseMember::ABBaseMember()
: membershipId( 0 )
{
}

QString ABBaseMember::toString() const
{
  QString body;
  body += arg( "MembershipId",          (long)membershipId );
  body += arg( "Type",                  type );
  body += arg( "DisplayName",           displayName );
  body += arg( "State",                 state );
  body += arg( "Deleted",               deleted );
  body += arg( "LastChanged",           lastChanged );
  body += arg( "JoinedDate",            joinedDate );
  body += arg( "ExpirationDate",        expirationDate );
  body += arg( "Changes",               changes );
  return body;
}



void ABBaseMember::parse( const QDomNode &node )
{
  parseLong  ( node, "MembershipId",         membershipId );
  parseString( node, "Type",                 type );
  parseString( node, "DisplayName",          displayName );
  parseString( node, "State",                state );
  parseBool  ( node, "Deleted",              deleted );
  parseString( node, "LastChanged",          lastChanged );
  parseString( node, "JoinedDate",           joinedDate );
  parseString( node, "ExpirationDate",       expirationDate );
  parseString( node, "Changes",              changes );
}



/**********************************************
ABPassportMember
***********************************************/
ABPassportMember::ABPassportMember()
: ABBaseMember()
{
  type = "Passport";
  passportId = 0;
  cid = 0;
}

QString ABPassportMember::handle() const
{
  return passportName;
}

QString ABPassportMember::toString() const
{
  QString body = ABBaseMember::toString();
  body += arg( "PassportName",          passportName );
  body += arg( "IsPassportNameHidden",  isPassportNameHidden );
  body += arg( "PassportId",            passportId );
  body += arg( "CID",                   cid );
  body += arg( "PassportChanges",       passportChanges );
  body += arg( "LookedupByCID",         lookedUpByCID );
  return body;
}



void ABPassportMember::parse( const QDomNode &node )
{
  ABBaseMember::parse( node );
  parseString( node, "PassportName",         passportName );
  parseBool  ( node, "IsPassportNameHidden", isPassportNameHidden );
  parseLong  ( node, "PassportId",           passportId );
  parseLong  ( node, "CID",                  cid );
  parseString( node, "PassportChanges",      passportChanges );
  parseBool  ( node, "LookedupByCID",        lookedUpByCID );
}



/**********************************************
ABEmailMember
***********************************************/
ABEmailMember::ABEmailMember()
{
  type = "Email";
}

QString ABEmailMember::handle() const
{
  return email;
}

QString ABEmailMember::toString() const
{
  QString body = ABBaseMember::toString();
  body += arg( "Email", email );
  return body;
}



void ABEmailMember::parse( const QDomNode &node )
{
  ABBaseMember::parse( node );
  parseString( node, "Email", email );
}




/**********************************************
ABGroup
***********************************************/
QString ABGroup::toString() const
{
  QString body;
  body += arg( "groupId", groupId );
  body += arg( "groupInfo", groupInfo );
  body += arg( "fDeleted", fDeleted );
  body += arg( "lastChange", lastChange );
  body += arg( "propertiesChanged", propertiesChanged );
  
  return body;
}



void ABGroup::parse( const QDomNode &node )
{
  parseString   ( node, "groupId", groupId );
  parseSoapType ( node, "groupInfo", groupInfo );
  parseBool     ( node, "fDeleted", fDeleted );
  parseString   ( node, "lastChange", lastChange );
  parseString   ( node, "propertiesChanged", propertiesChanged );
}


/**********************************************
ABInfo
***********************************************/
QString ABInfo::toString() const
{
  QString body;
  body += arg( "name", name );
  body += arg( "MigratedTo", migratedTo );
  body += arg( "ownerPuid", ownerPuid );
  body += arg( "OwnerCID", ownerCID );
  body += arg( "ownerEmail", ownerEmail );
  body += arg( "fDefault", fDefault );
  body += arg( "joinedNamespace", joinedNamespace );
  body += arg( "IsBot", isBot );
  body += arg( "IsParentManaged", isParentManaged );
  body += arg( "AccountTierLastChanged", accountTierLastChanged );
  body += arg( "SubscribeExternalPartner", subscribeExternalPartner );
  body += arg( "NotifyExternalPartner", notifyExternalPartner );
  body += arg( "AddressBookType", addressBookType );
  body += arg( "MessengerApplicationServiceCreated", messengerApplicationServiceCreated );
  body += arg( "IsBetaMigrated", isBetaMigrated );
  return body;
}



void ABInfo::parse( const QDomNode &node )
{
  parseString( node, "name", name );
  parseLong( node, "MigratedTo", migratedTo );
  parseLong( node, "ownerPuid", ownerPuid );
  parseLong( node, "OwnerCID", ownerCID );
  parseString( node, "ownerEmail", ownerEmail );
  parseBool( node, "fDefault", fDefault );
  parseBool( node, "joinedNamespace", joinedNamespace );
  parseBool( node, "IsBot", isBot );
  parseBool( node, "IsParentManaged", isParentManaged );
  parseString( node, "AccountTierLastChanged", accountTierLastChanged );
  parseBool( node, "SubscribeExternalPartner", subscribeExternalPartner );
  parseBool( node, "NotifyExternalPartner", notifyExternalPartner );
  parseString( node, "AddressBookType", addressBookType );
  parseBool( node, "MessengerApplicationServiceCreated", messengerApplicationServiceCreated );
  parseBool( node, "IsBetaMigrated", isBetaMigrated );
}



/**********************************************
HandleType
***********************************************/
HandleType::HandleType()
{
  type = "Messenger";
  id = 0;
  foreignId = "";
}

QString HandleType::toString() const
{
  QString body;
  body += arg( "Id", id );
  body += arg( "Type", type );
  body += arg( "ForeignId", foreignId, QString(), AllowEmpty );
  return body;
}



void HandleType::parse( const QDomNode &node )
{
  parseLong( node, "Id", id );
  parseString( node, "Type", type );
  parseString( node, "ForeignId", foreignId );
}



/**********************************************
InfoType
***********************************************/
QString InfoType::toString() const
{
  QString body;
  body += arg( "Handle", handle );
  body += arg( "DisplayName", displayName );
  body += arg( "InverseRequired", inverseRequired );
  body += arg( "AuthorizationCriteria", authorizationCriteria );
  body += arg( "RSSUrl", rssURL );
  body += arg( "IsBot", isBot );
  return body;
}



void InfoType::parse( const QDomNode &node )
{
  parseSoapType( node, "Handle", handle );
  parseString( node, "DisplayName", displayName );
  parseBool( node, "InverseRequired", inverseRequired );
  parseString( node, "AuthorizationCriteria", authorizationCriteria );
  parseString( node, "RSSUrl", rssURL );
  parseBool( node, "IsBot", isBot );
}


/**********************************************
ServiceType
***********************************************/
QString ServiceType::toString() const
{
  QString body;
  body += arg( "Info", info );
  body += arg<ABMembership*>( "Memberships", "Membership", memberships );
  body += arg( "Changes", changes );
  body += arg( "LastChange", lastChange );
  body += arg( "Deleted", deleted );
  return body;
}



void ServiceType::parse( const QDomNode &node )
{
  parseSoapType( node, "Info", info );
  parseSoapTypeList<ABMembership>( node, "Memberships", "Membership", memberships );
  parseString( node, "Changes", changes );
  parseString( node, "LastChange", lastChange );
  parseBool( node, "Deleted", deleted );
}



/**********************************************
OwnerNamespaceType
***********************************************/
QString OwnerNamespaceType::toString() const
{
  QString body;
  body += arg( "Changes", changes );
  body += arg( "LastChange", lastChange );
  body += arg( "CreateDate", createDate );
  return body;
}



void OwnerNamespaceType::parse( const QDomNode &node )
{
  parseString( node, "Changes", changes );
  parseString( node, "LastChange", lastChange );
  parseString( node, "CreateDate", createDate );
}


/**********************************************
ContactIdType
***********************************************/
QString ContactIdType::toString() const
{
  QString body;
  body += arg( "contactId", contactId );
  return body;
}



void ContactIdType::parse( const QDomNode &node )
{
  parseString( node, "contactId", contactId );
}
