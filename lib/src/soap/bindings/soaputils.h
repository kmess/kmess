/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Adam Goossens <adam@kmess.org>                            *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file soaputils.h
 */

#ifndef SOAPUTILS_H
#define SOAPUTILS_H

#include <QString>
#include <QList>
#include <QDomNode>

#include "../../utils/xmlfunctions.h"

namespace KMessInternal
{

class SoapType
{
  protected:
    SoapType() {};

  public:
    virtual ~SoapType() 
    {
    }
    
    virtual QString toString() const = 0;
    virtual void parse( const QDomNode &node ) = 0;
};


namespace SoapUtils
{
  enum ArgFlags
  {
    None,
    AllowEmpty,
    NoEscape
  };
  
  QString arg( QString elementName, QString val, QString defaultValue = QString(), ArgFlags flags = (ArgFlags)0 );  
  QString arg( QString elementName, long val );
  QString arg( QString elementName, bool val );
  
  // these two don't escape their SoapType arguments
  // that's ok, since the SoapTypes themselves escape their data.
  QString arg( QString elementName, SoapType *ptr );
  QString arg( const QString elementName, const SoapType &type );

  template <class T> QString arg( QString listElement, QString itemElement, QList<T> list );

  template <class T> void clonePtrList( T sourceList, T &destList );

  void parseString( const QDomNode &parent, QString element, QString &target, QString defaultValue = QString() );
  void parseLong  ( const QDomNode &parent, QString element, long &target );
  void parseBool  ( const QDomNode &parent, QString element, bool &target );
  void parseSoapType( const QDomNode &parent, QString element, SoapType &target );
  template <class T>
  void parseSoapTypeList  ( const QDomNode &parent, QString startElement, QString listElement, QList<T> &target );
  void parseList  ( const QDomNode &parent, QString startElement, QString listElement, QList<QString> &target );
  
  template <class T>
  void clonePtrList( QList<T*> sourceList, QList<T*> &destList )
  {
    destList.clear();

    foreach( T *t, sourceList )
    {
      T *n = new T( *t );
      destList.append( n );
    }
  }
  
  // these two have to be here because of C++ template compiler wierdness.
  template <class T>
  QString arg( QString listElement, QString itemElement, QList<T> list )
  {
    if ( list.count() == 0 ) return QString();
    
    QString res;
    
    foreach( const T &t, list )
    {
      res += arg( itemElement, t );
    }
    
    return arg( listElement, res, QString(), NoEscape );
  }

  template <class T>
  void parseSoapTypeList( const QDomNode &parent, QString startElement, QString subElementName, QList<T*> &target )
  {
    if ( parent.isNull() ) return;
    
    const QDomNodeList &nodes = XmlFunctions::getNode( parent, startElement ).toElement().elementsByTagName( subElementName );
    
    for( int i = 0; i < nodes.count(); i++ )
    {
      const QDomNode &n = nodes.at( i );
      T *t = new T();
      t->parse( n );
      target.append( t );
    }
  }
};

};

#endif
