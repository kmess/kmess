/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Adam Goossens <adam@kmess.org>                            *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file soaputils.cpp
 */

#include "soaputils.h"
#include "../../utils/xmlfunctions.h"
#include <KMess/Utils>

#include <QString>
#include <QList>
#include <QDomNode>

namespace KMessInternal
{
   
namespace SoapUtils
{
  void parseString( const QDomNode &parent, QString element, QString &target, QString defaultValue )
  {
    if ( parent.isNull() )
    {
      target = defaultValue;
      return;
    }
    
    target = XmlFunctions::getNodeValue( parent, element );
    if ( target.isEmpty() )
    {
      target = defaultValue;
    }
  }

  void parseLong( const QDomNode &parent, QString element, long &target )
  {
    if ( parent.isNull() ) return;
    
    target = XmlFunctions::getNodeValue( parent, element ).toLong();
  }
  
  void parseBool( const QDomNode &parent, QString element, bool &target )
  {
    if ( parent.isNull() ) return;
    
    target = XmlFunctions::getNodeValue( parent, element ).toLower() == "true" ? true : false;
  }

  void parseList( const QDomNode &parent, QString startElement, QString subElementName, QList<QString> &target )
  {
    if ( parent.isNull() ) return;
    const QDomNodeList &nodes = XmlFunctions::getNode( parent, startElement ).toElement().elementsByTagName( subElementName );
    
    for( int i = 0; i < nodes.count(); i++ )
    {
      const QDomNode &n = nodes.at( i );
      target.append( n.toElement().text() );
    }
  }

  void parseSoapType( const QDomNode &parent, QString element, SoapType &target )
  {
    if ( parent.isNull() ) return;
    target.parse( XmlFunctions::getNode( parent, element ) );
  }

  QString arg( QString elementName, QString value, QString defaultValue, ArgFlags flags )
  {
    value = ( value.isEmpty() ? defaultValue : value );
    
    if ( value.isEmpty() && ( flags & AllowEmpty ) )
    {
      return QString( "<%1/>\n" ).arg( elementName );
    }
    else if ( ! value.isEmpty() )
    {
      return QString( "<%1>%2</%1>\n" ).arg( elementName ).arg( (flags & NoEscape ) ? value : KMess::Utils::htmlEscape( value ) );
    }
    else
    {
      return QString();
    }
  }

  QString arg( QString elementName, bool value )
  {
    return arg( elementName, QString( value ? "true" : "false" ) );
  }
  
  QString arg( QString elementName, long value )
  {
    return arg( elementName, QString::number( value ) );
  }
  
  QString arg( QString elementName, SoapType *p )
  {
    return arg( elementName, p->toString(), QString(), NoEscape );
  }
  
  QString arg( const QString elementName, const SoapType &type )
  {
    return arg( elementName, type.toString(), QString(), NoEscape );
  }
};

};
