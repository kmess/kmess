/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Adam Goossens <adam@kmess.org>                            *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file abdatatypes.h
 */

#ifndef ABDATATYPES_H
#define ABDATATYPES_H

#include "soaputils.h"

#include <QDebug>
#include <QString>
#include <QList>
#include <QStringList>
#include <QHash>
#include <QDomNode>

#define COPY( var ) var = other.var

namespace KMessInternal
{

  using namespace SoapUtils;
  
struct ABInfo : public SoapType
{
  QString   name;
  long      migratedTo;
  long      ownerPuid;
  long      ownerCID;
  QString   ownerEmail;
  bool      fDefault;
  bool      joinedNamespace;
  bool      isBot;
  bool      isParentManaged;
  QString   accountTierLastChanged;
  bool      subscribeExternalPartner;
  bool      notifyExternalPartner;
  QString   addressBookType;
  bool      messengerApplicationServiceCreated;
  bool      isBetaMigrated;
  
  virtual QString toString() const;
  
  virtual void parse( const QDomNode &node );
};



struct ABBaseMember : public SoapType
{
  protected:
    ABBaseMember();

  public:
    long      membershipId;
    QString   type;
    QString   displayName;
    QString   state;
    bool      deleted;
    QString   lastChanged;
    QString   joinedDate;
    QString   expirationDate;
    QString   changes;
    
    virtual QString handle() const = 0;
    virtual ABBaseMember *clone() = 0;

    virtual QString  toString() const;
    virtual void     parse( const QDomNode &node );
};


struct ABPassportMember : public ABBaseMember
{
  ABPassportMember();
  
  QString   passportName;
  bool      isPassportNameHidden;
  long      passportId;
  long      cid;
  QString   passportChanges;
  bool      lookedUpByCID;
  
  virtual QString handle() const;
  
  virtual ABBaseMember *clone()
  {
    return new ABPassportMember( *this );
  }
  
  virtual QString  toString() const;
  virtual void     parse( const QDomNode &node );
};


struct ABEmailMember : public ABBaseMember
{
  QString email;
  
  ABEmailMember();
  
  virtual QString handle() const;
  virtual ABBaseMember *clone()
  {
    return new ABEmailMember( *this );
  }
  
  virtual QString  toString() const;
  virtual void     parse( const QDomNode &node );
};



struct ABMembership : public SoapType
{
  QString               memberRole;
  QList<ABBaseMember*>  members;
  bool                  membershipIsComplete;

  ABMembership() {}

  ABMembership( const ABMembership &other )
  {
    *this = other;
  }
  
  ABMembership &operator=( const ABMembership &other )
  {
    COPY( memberRole );
    foreach( ABBaseMember *m, other.members )
    {
      members.append( m->clone() );
    }
    COPY( membershipIsComplete );
    
    return *this;
  }
  
  virtual void parse( const QDomNode &node );
  virtual QString toString() const;
  
  virtual ~ABMembership();
};


struct ABAnnotation : public SoapType
{
  QString name;
  QString value;
  
  ABAnnotation( QString name = QString(), QString value = QString() );

  virtual QString toString() const;
  
  virtual void parse( const QDomNode &node );
};


struct ABGroupInfo : public SoapType
{
  QList<ABAnnotation*>      annotations;
  QString                   groupType;
  QString                   name;
  bool                      isNotMobileVisible;
  bool                      isPrivate;
  bool                      isFavorite;
  bool                      fMessenger;

  ABGroupInfo() {}

  ABGroupInfo( const ABGroupInfo &other )
  {
    *this = other;
  }
  
  ABGroupInfo &operator=( const ABGroupInfo &other )
  {
    clonePtrList<ABAnnotation>( other.annotations, annotations );
    COPY( groupType );
    COPY( name );
    COPY( isNotMobileVisible );
    COPY( isPrivate );
    COPY( isFavorite );
    COPY( fMessenger );
    
    return *this;
  }
  
  virtual ~ABGroupInfo();
  virtual QString toString() const;
  virtual void parse( const QDomNode &node );
  
};


/**
 * Contains information on a group.
 */
struct ABGroup : public SoapType
{
  QString                   groupId;
  ABGroupInfo               groupInfo;
  bool                      fDeleted;
  QString                   lastChange;
  QString                   propertiesChanged;

  virtual QString toString() const;
  virtual void       parse( const QDomNode &node );
};


/**
 * Contains information on email addressed for a contact.
 */
struct ABContactEmail : public SoapType
{
  QString               contactEmailType;
  QString               email;
  bool                  isMessengerEnabled;
  long                  capability;
  bool                  messengerEnabledExternally;
  QString               propertiesChanged;

  virtual QString toString() const;
  virtual void parse( const QDomNode &node );
  
};


/**
 * Holds information about a contact's phone number
 */
struct ABContactPhone : public SoapType
{
  QString               contactPhoneType;
  QString               number;
  bool                  isMessengerEnabled;

  virtual QString toString() const;
  virtual void parse( const QDomNode &node );
  
};


/**
* Holds all the detailed information of a contact parsed from the address book.
*/
struct ABContactInfo : public SoapType
{
  QList<ABContactEmail*> emails;
  QList<ABAnnotation*>   annotations;
  QList<ABContactPhone*> phones;
  QList<QString>        groupIds;
  QString               contactType;
  QString               firstName;
  QString               lastName;
  QString               middleName;
  QString               quickName;
  QString               passportName;
  QString               displayName;
  long                  cid;
  bool                  isNotMobileVisible;
  bool                  isMobileIMEnabled;
  bool                  isMessengerUser;
  bool                  isFavorite;
  bool                  hasSpace;
  QString               propertiesChanged;

  ABContactInfo();

  ABContactInfo( const ABContactInfo &other )
  {
    *this = other;
  }
  
  ABContactInfo &operator=( const ABContactInfo &other )
  {
    if ( other.emails.count() > 0 )
    {
      qDeleteAll( emails );
      clonePtrList<ABContactEmail>( other.emails, emails );
    }
    if ( other.phones.count() > 0 )
    {
      qDeleteAll( phones );
      clonePtrList<ABContactPhone>( other.phones, phones );
    }
    if ( other.annotations.count() > 0 )
    {
      qDeleteAll( annotations );
      clonePtrList<ABAnnotation>( other.annotations, annotations );
    }
    
    COPY( groupIds );
    COPY( contactType );
    COPY( quickName );
    COPY( passportName );
    COPY( displayName );
    COPY( firstName );
    COPY( middleName );
    COPY( lastName );
    COPY( cid );
    COPY( isNotMobileVisible );
    COPY( isMobileIMEnabled );
    COPY( isMessengerUser );
    COPY( isFavorite );
    COPY( hasSpace );
    COPY( propertiesChanged );
    
    return *this;
  }
  
  virtual ~ABContactInfo();
  virtual QString toString() const;
  virtual void parse( const QDomNode &node );
  
};

/**
 * Represents a contact parsed from the address book.
 */

struct ABContact : public SoapType
{
  QString       contactId;
  ABContactInfo contactInfo;
  QString       propertiesChanged;
  bool          fDeleted;
  QString       createDate;
  QString       lastChange;

  virtual QString toString() const;
  virtual void parse( const QDomNode &node );
  
};



/**
 * Represents handle information used in many AB calls.
 */
struct HandleType : public SoapType
{
  long          id;
  QString       type;
  QString       foreignId;

  HandleType();
  virtual QString toString() const;
  virtual void parse( const QDomNode &node );
  
};



/**
 * Holds extra information about a service.
 */
struct InfoType : public SoapType
{
  HandleType    handle;
  QString       displayName;
  bool          inverseRequired;
  QString       authorizationCriteria;
  QString       rssURL;
  bool          isBot;
  
  virtual QString toString() const;
  virtual void parse( const QDomNode &node );
  
};



/**
 * Holds the membership information for a requested service type.
 */
struct ServiceType : public SoapType
{
  InfoType              info;
  QList<ABMembership*>  memberships;
  QString               changes;
  QString               lastChange;
  bool                  deleted;
  
  ServiceType() {}

  ServiceType( const ServiceType &other )
  {
    *this = other;
  }
  
  ServiceType &operator=( const ServiceType &other )
  {
    COPY( info );
    clonePtrList<ABMembership>( other.memberships, memberships );
    COPY( changes );
    COPY( lastChange );
    COPY( deleted );
    
    return *this;
  }
  
  virtual QString toString() const;
  virtual void parse( const QDomNode &node );
  
  virtual ~ServiceType()
  {
    qDeleteAll( memberships );
  }
};


/**
 * Holds the membership information for a requested service type.
 */
struct OwnerNamespaceType: public SoapType
{
  QString               changes;
  QString               lastChange;
  QString               createDate;
  
  virtual QString toString() const;
  virtual void parse( const QDomNode &node );
  
};


/**
 * Holds a contact ID GUID.
 */
struct ContactIdType : public SoapType
{
  QString               contactId;

  virtual QString toString() const;
  virtual void parse( const QDomNode &node );
};

}; // namespace
#endif
