/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Antonio Nastasi <sifcenter@gmail.com>                     *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file roamingservice.cpp
 */

#include "roamingservice.h"
#include "debug/libkmessdebug.h"

#include <KMess/MsnSession>
#include <KMess/Utils>
#include <KMess/Self>

#include "../utils/xmlfunctions.h"
#include "soapmessage.h"


using namespace KMessInternal;


/**
 * @brief URL of the Storage Service
 */
#define SERVICE_URL_STORAGE_SERVICE  "https://storage.msn.com/storageservice/SchematizedStore.asmx"



// Constructor
RoamingService::RoamingService( KMess::MsnSession *session, QObject *parent )
: PassportLoginService( session, parent )
{
}



// Destructor
RoamingService::~RoamingService()
{
}



// Create the common header for this service
QString RoamingService::createCommonHeader() const
{
  QString header( "<StorageApplicationHeader xmlns=\"http://www.msn.com/webservices/storage/w10\">\n"
                    "<ApplicationID>Messenger Client 9.0</ApplicationID>\n"
                    "<Scenario>RoamingSeed</Scenario>\n"
                  "</StorageApplicationHeader>\n"
                  "<StorageUserHeader xmlns=\"http://www.msn.com/webservices/storage/w10\">\n"
                    "<Puid>0</Puid>\n"
                    "<TicketToken />\n" // Filled in by the base class
                  "</StorageUserHeader>\n" );
  return header;
}



// Get the profile for given contact
void RoamingService::getProfile( long cid )
{
  QString body( "<GetProfile xmlns=\"http://www.msn.com/webservices/storage/w10\">\n"
                  "<profileHandle>\n"
                    "<Alias>\n"
                      "<Name>" + QString::number( cid ) + "</Name>\n"
                      "<NameSpace>MyCidStuff</NameSpace>\n"
                    "</Alias>\n"
                    "<RelationshipName>MyProfile</RelationshipName>\n"
                  "</profileHandle>\n"
                  "<profileAttributes>\n"
                    "<ResourceID>true</ResourceID>\n"
                    "<DateModified>true</DateModified>\n"
                    "<ExpressionProfileAttributes>\n"
                      "<ResourceID>true</ResourceID>\n"
                      "<DateModified>true</DateModified>\n"
                      "<DisplayName>true</DisplayName>\n"
                      "<DisplayNameLastModified>true</DisplayNameLastModified>\n"
                      "<PersonalStatus>true</PersonalStatus>\n"
                      "<PersonalStatusLastModified>true</PersonalStatusLastModified>\n"
                      "<StaticUserTilePublicURL>true</StaticUserTilePublicURL>\n"
                      "<Photo>true</Photo>\n"
                      "<Flags>true</Flags>\n"
                    "</ExpressionProfileAttributes>\n"
                  "</profileAttributes>\n"
                "</GetProfile>\n" );

  sendSecureRequest( new SoapMessage( SERVICE_URL_STORAGE_SERVICE,
                                      "http://www.msn.com/webservices/storage/w10/GetProfile",
                                      createCommonHeader(),
                                      body ),
                     "Storage" );
}



// Create a profile
void RoamingService::createProfile()
{
  QString body( "<CreateProfile xmlns=\"http://www.msn.com/webservices/storage/w10\">\n"
                "<profile>\n"
                "<ExpressionProfile>\n"
                "<PersonalStatus/>\n"
                "<RoleDefinitionName>ExpressionProfileDefault</RoleDefinitionName>\n"
                "</ExpressionProfile>\n"
                "</profile>\n"
                "</CreateProfile>" );

  sendSecureRequest( new SoapMessage( SERVICE_URL_STORAGE_SERVICE,
                                      "http://www.msn.com/webservices/storage/w10/CreateProfile",
                                      createCommonHeader(),
                                      body ),
                     "Storage" );
}



// update the profile of the current account
void RoamingService::updateProfile()
{
  QString body( "<UpdateProfile xmlns=\"http://www.msn.com/webservices/storage/w10\">\n"
                "<profile>\n"
                "<ResourceID>" + session_->sessionSettingString( "ProfileResourceId" ) + "</ResourceID>\n"
                "<ExpressionProfile>\n"
                "<FreeText>Update</FreeText>\n"
                "<DisplayName>" + KMess::Utils::htmlEscape( session_->self()->displayName() ) + "</DisplayName>\n"
                "<PersonalStatus>" + KMess::Utils::htmlEscape( session_->self()->personalMessage() ) + "</PersonalStatus>\n"
                "<Flags>0</Flags>\n"
                "</ExpressionProfile>\n"
                "</profile>\n"
                "</UpdateProfile>\n");

  sendSecureRequest( new SoapMessage( SERVICE_URL_STORAGE_SERVICE,
                                      "http://www.msn.com/webservices/storage/w10/UpdateProfile",
                                      createCommonHeader(),
                                      body ),
                     "Storage" );
}



// Parse the SOAP fault
void RoamingService::parseSecureFault( SoapMessage *message )
{
  // Should never happen
  if( ! message->isFaultMessage() )
  {
    warning() << "Incorrect fault detected on incoming message";
    return;
  }

  // Get the type of error
  QString faultCode( message->getFaultCode() );
  QString errorCode( XmlFunctions::getNodeValue( message->getFault(), "detail/errorcode" ) );

  // See which fault we received
  if( faultCode == "soap:Client" && ! errorCode.isEmpty() )
  {
    // The profile does not exist
    if( errorCode == "ItemDoesNotExist" )
    {
      debug() << "The profile of the user does not exist: creating one.";
      createProfile();
      return;
    }
    else if( errorCode == "ItemAlreadyExists" )
    {
      warning() << "The profile of the user already exists";
    }
  }
  else
  {
    warning() << "Invalid web service request:" << message->getFaultDescription();
  }
}



// The connection received the full response
void RoamingService::parseSecureResult( SoapMessage *message )
{
  QDomElement body( message->getBody().toElement() );
  QString resultName( body.firstChildElement().localName() );

  if( resultName == "GetProfileResponse" )
  {
    processProfileResult( body );
  }
  else if( resultName == "UpdateProfileResponse" )
  {
    return;
  }
  else if( resultName == "CreateProfileResponse" )
  {
    QString resourceId( XmlFunctions::getNodeValue( body, "CreateProfileResponse" ) );
    session_->setSessionSetting( "ProfileResourceId", resourceId );

    // Empty profile was created, update it
    updateProfile();
  }
  else
  {
    warning() << "Unknown response type" << resultName << "received from endpoint" << message->getEndPoint();
  }
}



// Process the getProfile response
void RoamingService::processProfileResult( const QDomElement &body )
{
  const QDomNode expressionProfile( XmlFunctions::getNode( body, "GetProfileResponse/GetProfileResult/ExpressionProfile" ) );

  // adam (23 Dec 10): started seeing occurrences of the GetProfileRequest returning a valid (as in 200 OK),
  // but invalid response (i.e., contains no ExpressionProfile data or, worse, not even a Resource ID)
  // work around is to create a new profile.
  if ( expressionProfile.isNull() )
  {
    createProfile();
    return;
  }

  QString displayName( XmlFunctions::getNodeValue( expressionProfile, "DisplayName" ) );
  const QDomNodeList streams( XmlFunctions::getNode( expressionProfile, "Photo" ).toElement().elementsByTagName( "DocumentStream" ) );
  QString fullPictureUrl;
  for( int i = 0; i < streams.count(); i++ )
  {
    const QDomNode node( streams.at(i) );
    QString name( XmlFunctions::getNodeValue( node, "DocumentStreamName" ) );
    if ( name == "UserTileStatic" )
    {
      fullPictureUrl = XmlFunctions::getNodeValue( node, "PreAuthURL" );
      break;
    }
  }

  QString pictureLastModified( XmlFunctions::getNodeValue( expressionProfile, "Photo/DateModified" ) );
  // This message also contains DisplayName

  const QString resourceId( XmlFunctions::getNodeValue( expressionProfile, "ResourceID" ) );
  session_->setSessionSetting( "ProfileResourceId", resourceId );

  const QString personalStatus( XmlFunctions::getNodeValue( expressionProfile, "PersonalStatus" ) );
  session_->self()->_setPersonalMessage( personalStatus );
  session_->self()->_setRoamingDisplayPictureUrl( fullPictureUrl );
  session_->self()->setDisplayName( displayName );

  debug() << "Received friendly name from Storage:" << displayName;
  debug() << "Received personalMessage from Storage:" << personalStatus;
  debug() << "Received displayPicture location from server:" << fullPictureUrl;

  emit receivedPictureUrl( QUrl( fullPictureUrl ) );
}


