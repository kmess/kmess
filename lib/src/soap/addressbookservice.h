/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Antonio Nastasi <sifcenter@gmail.com>                     *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file addressbookservice.h
 */

#ifndef ADDRESSBOOK_H
#define ADDRESSBOOK_H

#include "passportloginservice.h"
#include "bindings/abbinding.h"

#include <QHash>
#include <QStringList>
#include <QDomDocument>
#include <QDateTime>

// Forward declarations
namespace KMess
{
  class MsnSession;
  class ContactProfile;
};


namespace KMessInternal
{

class ListsManager;
class ABFindAllResponse;

struct RoleInfo
{
  QString handle;
  QString displayName;
  KMess::MsnMemberships memberships;
  KMess::NetworkType network;
};


/**
 * Holds particulars about caching for either Membership data or AB data.
 */
struct ABCacheData
{
  QString       cacheFile;
  QDateTime     cacheTimestamp;
  QString       cacheTimezone;
  QDomDocument  cacheDocument;
};


/**
 * @brief Soap actions for retrieve the address book and membership lists.
 *
 *
 * @author Antonio Nastasi
 * @ingroup NetworkSoap
 */
class AddressBookService : public PassportLoginService
{
  Q_OBJECT
  
  public:
    enum FetchType
    {
      FullFetch,
      RefreshOnly
    };

  public:
    // The constructor
                        AddressBookService( KMess::MsnSession *session, ListsManager *listsManager, QObject *parent = 0 );
    // The destructor
    virtual            ~AddressBookService();
    // Add contact to the AB by using a specific handle and network type.
    void                addContact( const QString &handle, KMess::NetworkType network, const QList<KMess::MsnGroup*> &groups = QList<KMess::MsnGroup*>() );
    // Add a contact to the AB
    void                addContact( const KMess::MsnContact *contact, const QList<KMess::MsnGroup*> &groups = QList<KMess::MsnGroup*>() );
    // Add contact to group
    void                addContactToGroup( const KMess::MsnContact *contact, const KMess::MsnGroup *group );
    // Add group
    void                addGroup( const QString &name );
    // Block contact
    void                blockContact( const KMess::MsnContact *contact );
    // Delete contact
    void                deleteContact( KMess::MsnContact *contact, KMess::RemoveContactOption option = KMess::RemoveFromMessenger, KMess::RemoveContactAction action = KMess::RemoveOnly );
    // Delete group
    void                deleteGroup( const KMess::MsnGroup *group );
    // Retrieve the address book *and* membership lists using a given cache directory
    void                fetchAddressBook( const QString &cacheDir = QString() );
    // Refresh the address book (downloads changes from the server and applies them)
    void                retrieveMemberships( FetchType type = FullFetch );
    // Refresh the membership lists (downloads changes from the server and applies them)
    void                retrieveAddressBook( FetchType type = FullFetch );
    // Rename group
    void                renameGroup( const KMess::MsnGroup *group, const QString &name );
    // Delete contact from group
    void                removeContactFromGroup( const KMess::MsnContact *contact, const KMess::MsnGroup *group );
    // Unblock contact
    void                unblockContact( const KMess::MsnContact *contact );
    // A list of all address book profiles that exist, but don't belong to a user associated with Messenger in any way.
    QList<KMess::ContactProfile*> unknownProfiles() const;
    // Update a contact's address book
    void                updateProfile( const KMess::ContactProfile *profile );

  private: // private methods
    // Internal: add a contact to a list.
    void                addContactToList( const KMess::MsnContact *c, KMess::MsnMembership role );
    // Internal: Add contact to AB. Performs the request.
    void                addNewContact( const QString &handle, KMess::NetworkType network, const QList<KMess::MsnGroup*> &groups = QList<KMess::MsnGroup*>() );
    // Request creation of a new address book (for new accounts)
    void                createAddressBook();
    // Create an ABMembership update request.
    ABMembership       *createMembership( const KMess::MsnContact *contact, KMess::MsnMembership role );
    // Parse the address book
    void                parseAddressBook( const QDomElement &body, FetchType type );
    // Parse the membership lists
    void                parseMembershipLists( const QDomElement &body, FetchType type );
    // Parse a SOAP error message.
    void                parseSecureFault( SoapMessage *message );
    // Parse the result of the response from the server
    void                parseSecureResult( SoapMessage *message );
    // Reads the cache for a given cache data object. lastChange is the path to the "lastChange" XML element 
    // with the change timestamp.
    void                readCache( ABCacheData &data, const QString &lastChangePath );
    // Internal: remove a contact from a list.
    void                removeContactFromList( const KMess::MsnContact *c, KMess::MsnMembership role  );
    // Retrieve the Dynamic Items: Gleams
    void                retrieveGleams();
    // Save all cache
    void                saveCache( ABCacheData &data );

  private slots:
    // called after 1s to actually do the update
    void                doProfilesUpdate();
    
  private: // private data
    ABCacheData         membershipCache_;
    ABCacheData         addressbookCache_;
    QHash<QString,RoleInfo> memberships_;
    ListsManager       *listsManager_;
    QHash<KMess::MsnContact*, ABContact*>  contacts_;    
    QTimer                      updateTimer_;
    QList<KMess::ContactProfile*>   unknownProfiles_;
    QList<KMess::ContactProfile*>   profilesToUpdate_;

  signals: // Contact Address Book signals
    // Contact was added
    void                contactAdded( KMess::MsnContact *contact );
    // Contact was added to group
    void                contactAddedToGroup( KMess::MsnContact *contact, KMess::MsnGroup *group );
    // Contact was blocked
    void                contactBlocked( KMess::MsnContact *contact );
    // Contact was deleted
    void                contactDeleted( KMess::MsnContact *contact );
    // Contact was delete from group
    void                contactRemovedFromGroup( KMess::MsnContact *contact, KMess::MsnGroup *group );
    // Contact was unblocked
    void                contactUnblocked( KMess::MsnContact *contact );

  signals: // Group Address Book signals
    // Group was added
    void                groupAdded( KMess::MsnGroup *group );
    // Group was removed
    void                groupRemoved( KMess::MsnGroup *group );
    // Group was renamed
    void                groupRenamed( KMess::MsnGroup *group );

  signals: // Generic Address Book signals
    // A new contact was found after an address book refresh
    void                newContact( KMess::MsnContact *contact );
    // A new pending contact was found. User must decide.
    void                newPendingContact( KMess::MsnContact *contact );
    // The address book parsing and contact adding is complete.
    void                addressBookParsed( long cid, int blp );
};

}; // end of namespace KMess

#endif
