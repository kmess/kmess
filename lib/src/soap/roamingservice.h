/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Antonio Nastasi <sifcenter@gmail.com>                     *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file roamingservice.h
 */

#ifndef ROAMINGSERVICE_H
#define ROAMINGSERVICE_H

#include "passportloginservice.h"


// Forward declarations
class QNetworkReply;


namespace KMessInternal
{

/**
 * @brief Soap actions for retrieve the address book and membership lists.
 *
 *
 * @author Antonio Nastasi
 * @ingroup NetworkSoap
 */
class RoamingService : public PassportLoginService
{
  Q_OBJECT

  public:
    // Constructor
                    RoamingService( KMess::MsnSession *session, QObject *parent );
    // Destructor
                   ~RoamingService();
    // Get the profile for given contact
    void            getProfile( long cid );
    // Create a profile
    void            createProfile();
    // update the profile of the current account
    void            updateProfile();

  private: // Private methods
    // Create the common header for this service
    QString         createCommonHeader() const;
    // Parse the SOAP fault
    void            parseSecureFault( SoapMessage *message );
    // The connection received the full response
    void            parseSecureResult( SoapMessage *message );
    // Process the getProfile response
    void            processProfileResult( const QDomElement &body );

  signals:
    // Received the display picture URL from the server
    void            receivedPictureUrl( QUrl displayPictureUrl );
};

}; // End of namespace KMess

#endif
