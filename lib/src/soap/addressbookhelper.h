/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Adam Goossens <adam@kmess.org>                            *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file addressbookhelper.h
 */

#ifndef ADDRESSBOOKHELPER_H
#define ADDRESSBOOKHELPER_H

#include <QDomNode>
#include "addressbookservice.h"

namespace KMessInternal
{

class ABFindAllResponse;
class ABFindMembershipResult;
class ServiceType;

/**
 * This class just provides a handy home for the merge methods.
 */
class AddressBookHelper
{
  public:
    static  ABFindAllResponse         *mergeAddressBook( ABCacheData &data, const QDomNode &response );
    static  ABFindMembershipResult  *mergeMemberships( ABCacheData &data, const QDomNode &response );
    
  private:
    static void mergeServiceMemberships( ServiceType *cService, ServiceType *dService );
};

};

#endif
