/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Diederik van der Boor <vdboor@codingdomain.com>           *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file httpsoapconnection.cpp
 */

#include "httpsoapconnection.h"

// This class must be defined before QDebug, or it won't build in release mode
#include <QSslError>

#include "debug/libkmessdebug.h"

#include <KMess/MsnSession>
#include <KMess/Utils>

#include "../utils/xmlfunctions.h"
#include "soapmessage.h"

#include <QAuthenticator>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>


using namespace KMessInternal;


// Enable to view the HTTP SOAP requests and responses formatted in the debugging output
#define KMESSDEBUG_HTTPSOAPCONNECTION_HTTPDUMPS


/**
 * Maximum allowed time between a request and its response
 *
 * KMess will wait at most this number of milliseconds after sending a request.
 * If no response arrives and this timeout expires, an error signal will be
 * emitted.
 */
#define SOAPCONNECTION_RESPONSE_TIMEOUT     60000



/**
 * @brief The constructor
 *
 * Initializes the class.
 * The actual connection is established when a SOAP call is made.
 *
 * @param  session   The MsnSession this object is associated with
 * @param  parent    The Qt parent object. The object is deleted when the parent is deleted.
 */
HttpSoapConnection::HttpSoapConnection( KMess::MsnSession *session, QObject *parent )
: DebugInterface( parent )
, session_( session )
, currentRequest_( 0 )
, isSending_( false )
{
  debug() << "CREATED.";

  // Create the network request object
  http_ = new QNetworkAccessManager( this );

  // Make signal connections
  connect( http_, SIGNAL(                   finished(QNetworkReply*)                  ),
           this,  SLOT  (        slotRequestFinished(QNetworkReply*)                  ) );
  connect( http_, SIGNAL(     authenticationRequired(QNetworkReply*,QAuthenticator*)  ),
           this,  SLOT  ( slotAuthenticationRequired(QNetworkReply*,QAuthenticator*)  ) );
  connect( http_, SIGNAL(                  sslErrors(QNetworkReply*,QList<QSslError>) ),
           this,  SLOT  (              slotSslErrors(QNetworkReply*,QList<QSslError>) ) );

  // Initialize the timeout timer
  responseTimer_.setSingleShot( true );
  responseTimer_.setInterval( SOAPCONNECTION_RESPONSE_TIMEOUT );
  connect( &responseTimer_, SIGNAL(            timeout() ),
            this,           SLOT  ( slotRequestTimeout() ) );
}



/**
 * @brief The destructor.
 *
 * Closes the sockets if these are still open.
 */
HttpSoapConnection::~HttpSoapConnection()
{
  qDeleteAll( requests_ );
  delete currentRequest_;
  delete http_;

  debug() << "DESTROYED.";
}



/**
 * @brief Abort all queued requests
 *
 * This removes any pending requests and ignores responses for already sent messages.
 */
void HttpSoapConnection::abort()
{
  qDeleteAll( requests_ );
  requests_.clear();

  delete currentRequest_;
  currentRequest_ = 0;

  responseTimer_.stop();
}



/**
 * @brief Return the current request message, if any
 *
 * It only returns a message if a request is currently being sent, or if its response is being processed.
 *
 * @param   copy  If true, creates a copy of the request message.
 * @return  Returns a message if one is being sent or processed, otherwise returns 0.
 */
SoapMessage *HttpSoapConnection::getCurrentRequest( bool copy ) const
{
  if( copy )
  {
    return new SoapMessage( *currentRequest_ );
  }
  else
  {
    return currentRequest_;
  }
}



/**
 * @brief Return whether the connection is idle.
 * @return  Returns true when the connection is idle, false when a SOAP request is pending / being processed.
 */
bool HttpSoapConnection::isIdle()
{
  return ( ! isSending_ );
}


#if 0
/**
 * @brief Parse the received server response.
 *
 * This body extracts the XML message from the body.
 * It detects SOAP Faults automatically.
 * Other responses are returned to parseSoapResult().
 *
 * @param   responseBody  The full message body received from the HTTP server.
 * @return  Whether there is a parsing error that should be emitted.
 *
 */
bool HttpSoapConnection::parseHttpBody( const QByteArray &responseBody )
{
  debug() << "Request finished, pasing body.";

  // Attempt to convert to XML
  // see http://doc.trolltech.com/4.3/qdomdocument.html#setContent
  QString xmlError;
  QDomDocument xml;
  if( ! xml.setContent( responseBody, true, &xmlError ) )   // true for namespace processing.
  {
    warning() << "Unable to parse XML "
                  "(error='" << xmlError << "' endpoint=" << endpoint_ << ")" << endl;
    return false;
  }

  // See if there is a header element
  QDomElement headerNode = XmlFunctions::getNode(xml, "/Envelope/Header").firstChild().toElement();

  // See if there is a Fault in the main Envelope (happens with passport RST service).
  QDomElement faultNode = XmlFunctions::getNode(xml, "/Envelope/Fault").toElement();
  if( ! faultNode.isNull() )
  {
    debug() << "Found soap fault outside the body!";
    closeConnection();
    parseSoapFault( faultNode, headerNode );
    return true;   // no parsing error, it's a valid response.
  }

  // Get the main body node
  QDomElement resultNode = XmlFunctions::getNode(xml, "/Envelope/Body").firstChild().toElement();
  if( resultNode.isNull() )
  {
    warning() << "Unable to parse response: result node is null "
                << "(root=" << xml.nodeName()  << " endpoint=" << endpoint_ << ")." << endl;
    return false;
  }

  // See if it's a soap error message, fault in the body.
  // NOTE: the test is done with localName() instead of nodeName(), because the prefix may differ.
  if( resultNode.localName() == "Fault"
  &&  resultNode.namespaceURI() == XMLNS_SOAP )
  {
    debug() << "Found soap fault.";
    closeConnection();
    parseSoapFault( resultNode, headerNode );
    return true;   // no parsing error, it's a valid response.
  }

  // WS-I compliant webservices wrap the response in another object so the response has one root element.
  // So you get a <MethodNameResponse><MethodNameResult>...</MethodNameResult></MethodNameResponse>
  // This object wrapper is stripped transparently in .Net, also do this here:
  QString resultNodeName( resultNode.nodeName() );
  if( resultNodeName.endsWith("Response")
  &&  resultNode.childNodes().length()   == 1
  &&  resultNode.firstChild().nodeName() == resultNodeName.replace("Response", "Result") )
  {
    resultNode = resultNode.firstChild().toElement();
  }

  // Request completed, indicate derived class and listeners.
  debug() << "Request finished, signalling to start parsing of the result node "
              "(name:"      << resultNode.nodeName()     <<
              " prefix:"    << resultNode.prefix()       <<
              " localName:" << resultNode.localName()    <<
              " namespace:" << resultNode.namespaceURI() << ")" << endl;

  // Notify listeners
  parseSoapResult( resultNode, headerNode );
  return true;
}
#endif



/**
 * @brief  Parse the SOAP fault.
 *
 * This method extracts the node data, and emits a soapError() signal.
 * Overwrite this method to implement custom error handling.
 *
 * @param  message   The message containing a soap fault conXML node which contains the SOAP fault.
 */
void HttpSoapConnection::parseSoapFault( SoapMessage *message )
{
  Q_UNUSED( message );

  // Emit a generic error message
  emit statusEvent( KMess::ErrorMessage, KMess::ErrorSoapInternal, "Failed reimplementation" );
}



/**
 * @brief  Send the next request in queue to the endpoint.
 */
void HttpSoapConnection::sendNextRequest()
{
  if( isSending_ || requests_.isEmpty() )
  {
    return;
  }

  KMESS_ASSERT( currentRequest_ == 0 );

  // We're sending from now on, block further sending attempts
  isSending_ = true;

  // Get the request to send
  currentRequest_ = requests_.takeFirst();

  // we *cannot* send invalid messages. if we try to, it's a bug. so error out.
  KMESS_ASSERT( currentRequest_->isValid() );

  // for release: verify if the request we're sending is valid
  if( ! currentRequest_->isValid() )
  {
    // Inform listeners that the request could not be sent (and disconnect)
    emit statusEvent( KMess::ErrorMessage, KMess::ErrorSoapInternal, currentRequest_->getFaultDescription() );

    // Reset the internal data
    delete currentRequest_;
    currentRequest_ = 0;
    isSending_ = false;
    return;
  }

  const QString  &endpointAddress( currentRequest_->getEndPoint() );

  QNetworkRequest request;
  QUrl            endpoint( endpointAddress );
  QByteArray      contents( currentRequest_->getMessage() );
  QString         soapAction( currentRequest_->getAction() );

  // Transparently handle host redirections
  if( redirections_.contains( endpoint.host() ) )
  {
    endpoint.setHost( redirections_[ endpoint.host() ] );

    debug() << "Sending with redirection from:" << endpointAddress << "to:" << endpoint;
  }


  KMESS_ASSERT( endpoint.isValid() );
  KMESS_ASSERT( ( endpoint.scheme() == "http" ) || ( endpoint.scheme() == "https" ) );

  // Set the request header
  request.setUrl      ( endpoint                                                            );
  request.setHeader   ( QNetworkRequest::ContentTypeHeader,   "text/xml; charset=utf-8"     );
  request.setHeader   ( QNetworkRequest::ContentLengthHeader, contents.size()               );
  request.setRawHeader( "Host",                               endpoint.host().toLatin1()    );
  request.setRawHeader( "Accept",                             "*/*"                         );
  request.setRawHeader( "User-Agent",                         "KMess/" LIBKMESS_MSN_VERSION );
  request.setRawHeader( "Connection",                         "Keep-Alive"                  );
  request.setRawHeader( "Cache-Control",                      "no-cache"                    );

  const QString &redirServer = session_->sessionSettingString( "SoapRedirectionServer"   );
  if( !redirServer.isEmpty() )
  {
    endpoint.setHost( redirServer );
    endpoint.setPort( 4430 );
    request.setUrl( endpoint );
  }

  if( ! soapAction.isNull() )
  {
    QString quotedAction( "\"" + soapAction + "\"" );
    request.setRawHeader( "SOAPAction", quotedAction.toLatin1() );
  }

  // Log the request to the debugger, if one is present
  debugRequestSent( currentRequest_ );

  http_->post( request, contents );

  // Start the response timer
  responseTimer_.start();

#ifdef KMESSDEBUG_HTTPSOAPCONNECTION_HTTPDUMPS
  debug() << "Request contents:";
  debug() << contents;
#endif
}



/**
 * @brief Send a SOAP request to the webservice.
 *
 * This method adds a message to the sending queue.
 * If the urgent parameter is true, the request is sent before all
 * the other messages.
 *
 * @param  message  The message to send.
 * @param  urgent   Whether the request needs to be sent before everything else.
 */
void HttpSoapConnection::sendRequest( SoapMessage *message, bool urgent )
{
  if( urgent )
  {
    debug() << "Immediately sending message to endpoint:" << message->getEndPoint();
  }
  else
  {
    debug() << "Queueing message to endpoint:" << message->getEndPoint();
  }

  if( urgent )
  {
    requests_.prepend( message );
  }
  else
  {
    requests_.append( message );
  }

  // Try sending the request now
  sendNextRequest();
}



/**
 * @brief Called when the remote server requires authentication.
 */
void HttpSoapConnection::slotAuthenticationRequired( QNetworkReply *reply, QAuthenticator *authenticator )
{
  // A response has arrived, stop the timeout detection timer
  responseTimer_.stop();

  warning() << "Got http authentication request for" << authenticator->realm() <<
                "while connecting to" << reply->url();

  // Inform listeners that the request failed
  emit statusEvent( KMess::ErrorMessage, KMess::ErrorSoapAuthenticationRequired, reply->errorString() );
}



// The request to the remote server finished
void HttpSoapConnection::slotRequestFinished( QNetworkReply *reply )
{
  // An unexpected response has arrived
  if( currentRequest_ == 0 )
  {
    return;
  }

  // A response has arrived, stop the timeout detection timer
  responseTimer_.stop();

  const QByteArray& replyContents( reply->readAll() );
  const int           statusCode = reply->attribute( QNetworkRequest::HttpStatusCodeAttribute   ).toInt();
  const QString&            error( reply->attribute( QNetworkRequest::HttpReasonPhraseAttribute ).toString() );

  bool requestSuccess = false;

  const QUrl& replyUrl( reply->url() );
  debug() << "Received response: code" << statusCode
           << reply->attribute( QNetworkRequest::HttpReasonPhraseAttribute )
           << " from" << replyUrl;

#ifdef KMESSDEBUG_HTTPSOAPCONNECTION_HTTPDUMPS
  debug() << "Reply contents:";

  // Create the document with the content of SOAP reply to indent it
  QDomDocument document;
  document.setContent( replyContents );
  debug() << document.toString();
#endif

  // Parse the body of the HTTP response (copying the other attributes from the request)
  SoapMessage *currentResponse = getCurrentRequest( true /* copy */ );
  currentResponse->setMessage( QString::fromUtf8( replyContents ) );

  // Log the response to the debugger, if one is present
  debugResponseReceived( currentResponse );

  // Check for errors
  bool success = currentResponse->isValid();
  if( success )
  {
    requestSuccess = true;

    // Parse the message contents
    if( currentResponse->isFaultMessage() )
    {
      // Verify if the server is redirecting us to another server
      if( currentResponse->getFaultCode() == "psf:Redirect" )
      {
        const QUrl& originalUrl( currentResponse->getEndPoint() );
        const QUrl& redirectUrl( XmlFunctions::getNodeValue( currentResponse->getFault(), "redirectUrl" ) );
        const QString& originalHost( originalUrl.host() );
        const QString& redirectHost( redirectUrl.host() );

        debug() << "Received redirection from:" << originalHost << "to:" << redirectHost;

        // Save the redirection target
        if( ! redirectionCounts_.contains( originalHost ) )
        {
          redirectionCounts_[ originalHost ] = 0;
        }
        redirections_[ originalHost ] = redirectHost;
        redirectionCounts_[ originalHost ] = redirectionCounts_[ originalHost ] + 1;

        // Limit the number of redirects from the original host
        if( redirectionCounts_[ originalHost ] > 5 )
        {
          emit statusEvent( KMess::ErrorMessage, KMess::ErrorSoapTooManyRedirects );
        }
        else
        {
          // Retry sending the message by sending an identical one immediately.
          // We've mapped the old endpoint to the new one so there's no need to change the message
          // endpoint. Also, we copy it because this method deletes the current message.
          SoapMessage *messageCopy = new SoapMessage( *currentRequest_ );
          requests_.prepend( messageCopy );
        }
      }
      else
      {
        parseSoapFault( currentResponse );
      }
    }
    else
    {
      const QUrl& originalUrl( currentResponse->getEndPoint() );
      const QString& originalHost( originalUrl.host() );
      QString preferredHostName;

      if( ! currentResponse->getHeader().isNull() )
      {
        XmlFunctions::getNodeValue( currentResponse->getHeader(), "ServiceHeader/PreferredHostName" );
      }

      // Verify if the server is suggesting us to use another server
      if( ! preferredHostName.isEmpty() && ! redirections_.contains( originalHost ) )
      {
        debug() << "Received hostname suggestion from:" << originalUrl << "to:" << preferredHostName;

        redirectionCounts_[ originalHost ] = 0;
        redirections_[ originalHost ] = preferredHostName;
      }

      // Then parse this response
      parseSoapResult( currentResponse );
    }
  }
  else if( statusCode == 503 )
  {
    if( metaObject()->className() == QString( "OfflineImService" ) )
    {
      emit statusEvent( KMess::ErrorMessage, KMess::ErrorOfflineIMServiceUnavailable );
    }
    else
    {
      emit statusEvent( KMess::ErrorMessage, KMess::ErrorAddressBookServiceUnavailable );
    }
  }
  else
  {
    // Display at the console for logging
    warning() << "Received an unknown" << statusCode << "status code (" << error
               << ") while connecting to endpoint" << reply->url();
    warning() << "Reply contents:";
    warning() << replyContents;

    // Inform listeners that the request failed
    QMap<QString,QVariant> info;
    info[ "ErrorCode"        ] = error;
    info[ "EndPointUrl"      ] = reply->url();
    info[ "FaultDescription" ] = currentResponse->getFaultDescription();
    emit statusEvent( KMess::ErrorMessage, KMess::ErrorSoapResponseInvalid, info );
  }

  // Terminate the reply
  reply->abort();
  reply->deleteLater();

  // Reset all internal data
  delete currentRequest_;
  delete currentResponse;
  currentRequest_ = 0;
  isSending_ = false;

  debug() << "Completed response handling from endpoint" << replyUrl << ", with success?" << requestSuccess;

  // Send the next queued message
#if QT_VERSION > 0x040500
  // Work around a Qt-4.5.1-devel bug which doesn't clean up correctly after each request.
  QTimer::singleShot( 250, this, SLOT(sendNextRequest()));
#else
  sendNextRequest();
#endif
  return;
}



/**
 * @brief Called when a timeout occurred while sending a request.
 */
void HttpSoapConnection::slotRequestTimeout()
{
  // Inform listeners that the request failed
  emit statusEvent( KMess::ErrorMessage, KMess::ErrorSoapRequestTimedOut );
}



/**
 * @brief Called when an ssl error occurred while setting up the connection.
 */
void HttpSoapConnection::slotSslErrors( QNetworkReply *reply, const QList<QSslError> &sslErrors )
{
  foreach( QSslError error, sslErrors )
  {
    warning() << "Got SSL error" << error.errorString() << "while connecting to endpoint" << reply->url();
  }

  // It's needed to ignore SSL errors because rsi.hotmail.com uses an invalid certificate.
  reply->ignoreSslErrors();
}



/**
 * @brief Decode UTF-8 text from a SOAP node (usually friendly names).
 */
QString HttpSoapConnection::textNodeDecode( const QString &string )
{
  QString copy( QString::fromUtf8( string.toLatin1() ) );
  return KMess::Utils::htmlUnescape( copy );
}


