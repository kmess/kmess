/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Adam Goossens <adam@kmess.org>                            *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file addressbookhelper.cpp
 */

#include "addressbookhelper.h"
#include "../debug/libkmessdebug.h"
using namespace KMessInternal;


/*
 This method merges the cached contact list data with the deltas retrieved from the SOAP server.
 
 It does this by iterating over all the group and contact deltas separately, locating their equivalent
 in the cache and either updating or deleting it as necessary. If a delta has no equivalent in the cache
 it's considered "new" and so is added to the cache DOM.
 
 As a last step, we simply copy the <ab> node from the server SOAP and replace our local one. That
 updates our cache timestamp for us.
*/
ABFindAllResponse *AddressBookHelper::mergeAddressBook( ABCacheData &cache, const QDomNode &deltas )
{
  ABFindAllResponse *cResult = new ABFindAllResponse();
  
  if ( cache.cacheDocument.documentElement().isNull() )
  {
    cResult->parse( deltas );
    return cResult;
  }
  
  ABFindAllResponse *dResult = new ABFindAllResponse();
  
  cResult->parse( cache.cacheDocument.documentElement() );
  dResult->parse( deltas );

  debug() << cResult->contacts.count();
  debug() << cResult->groups.count();

  QHash<QString, ABGroup*> cachedGroups;
  QHash<QString, ABContact*> cachedContacts;
  
  foreach( ABGroup *cGroup, cResult->groups )
  {
    if ( ! cachedGroups.contains( cGroup->groupId ) )
    {
      cachedGroups.insert( cGroup->groupId, cGroup );
    }
  }
  
  foreach( ABContact *cContact, cResult->contacts )
  {
    if ( ! cachedContacts.contains( cContact->contactId ) )
    {
      cachedContacts.insert( cContact->contactId, cContact );
    }
  }
  
  // ok, now process the deltas.
  foreach( ABGroup *dGroup, dResult->groups )
  {
    ABGroup *cGroup = cachedGroups.value( dGroup->groupId );
    
    // if no cached group, this is "new" to the cache. append it.
    if ( ! cGroup )
    {
      cResult->groups.append( dGroup );
      dResult->groups.removeAll( dGroup );
      continue;
    }
    
    // group in the cache. is the delta marked deleted? if so, remove the cached
    // group.
    if ( dGroup->fDeleted )
    {
      cResult->groups.removeAll( cGroup );
      delete cGroup;
    }
    else
    {
      // not marked deleted. do a copy.
      (*cGroup) = (*dGroup);
    }
  }
  
  // now process the contacts....
  foreach( ABContact *dContact, dResult->contacts )
  {
    ABContact *cContact = cachedContacts.value( dContact->contactId );
    
    if ( ! cContact && ! dContact->fDeleted )
    {
      cResult->contacts.append( dContact );
      dResult->contacts.removeAll( dContact );
      continue;
    }

    if ( dContact->fDeleted )
    {
      cResult->contacts.removeAll( cContact );
      delete cContact;
    }
    else
    {
      // contact wasn't deleted. do a straight copy to overwrite it.
      *cContact = *dContact;
    }
  }
  
  // update the last change data...
  cResult->lastChange = dResult->lastChange;
  cResult->abInfo = dResult->abInfo;

  delete dResult;
  return cResult;

  // magical.
}


ABFindMembershipResult   *AddressBookHelper::mergeMemberships( ABCacheData &cache, const QDomNode &deltas )
{
  ABFindMembershipResult *cResult = new ABFindMembershipResult();
  if ( cache.cacheDocument.documentElement().isNull() )
  {
    cResult->parse( deltas );
    return cResult;
  }
  
  ABFindMembershipResult *dResult = new ABFindMembershipResult();

  cResult->parse( cache.cacheDocument.documentElement() ); // FindMembershipResult
  dResult->parse( deltas );
  
  // used for fast lookup of cache services in thABGe loop.
  QHash<QString,ServiceType*> cachedServices;
  foreach( ServiceType *service, cResult->services )
  {
    if ( ! cachedServices.contains( service->info.handle.type ) )
    {
      cachedServices.insert( service->info.handle.type, service );
    }
  }
  
  foreach( ServiceType *dService, dResult->services )
  {
    ServiceType *cService = cachedServices.value( dService->info.handle.type, 0 );
    
    // this service is unknown in the cache, so append it.
    if ( ! cService && ! dService->deleted )
    {
      cResult->services.append( dService );
      continue;
    }
    
    // service is known in cache, so either delete or update.
    if ( dService->deleted )
    {
      cResult->services.removeAll( cService );
      delete cService;
    }
    else
    {
      mergeServiceMemberships( cService, dService );
      cService->lastChange = dService->lastChange;
    }
  }
  
  cResult->ownerNamespace = dResult->ownerNamespace;
  return cResult;
}

void AddressBookHelper::mergeServiceMemberships( ServiceType *cService, ServiceType *dService )
{
  QHash<QString,ABMembership*> cacheMemberships;
  
  // used for fast lookups of the membership role.
  foreach( ABMembership *membership, cService->memberships )
  {
    if ( ! cacheMemberships.contains( membership->memberRole ) )
    {
      cacheMemberships.insert( membership->memberRole, membership );
    }
  }
  
  QHash<QString,QStringList> listAdditions;
  
  // for each membership in the delta service, grab the corresponding
  // cache membership and update it.
  foreach( ABMembership *dMembership, dService->memberships )
  {
    ABMembership *cMembership = cacheMemberships.value( dMembership->memberRole, 0 );
    
    // membership doesn't exist, so it's new in the cache. append it.
    if ( ! cMembership )
    {
      cService->memberships.append( dMembership );
      continue;
    }
    
    // used for fast lookup of members in this membership role.
    QHash<int,ABBaseMember*> cacheMembers;
    foreach( ABBaseMember *m, cMembership->members )
    {
      if ( ! cacheMembers.contains( m->membershipId ) )
      {
        cacheMembers.insert( m->membershipId, m );
      }
    }
    
    // now for each member in the delta membership, find its
    // corresponding cache member and either update it, delete it, or
    // if necessary append a new one.
    foreach( ABBaseMember *dMember, dMembership->members )
    {
      ABBaseMember *cMember = cacheMembers.value( dMember->membershipId, 0 );
      // if doesn't exist in cache, new to membership, so append and continue.
      if ( ! cMember && ! dMember->deleted )
      {
        cMembership->members.append( dMember );
        
        // if we're adding to the allow or block lists....
        if ( dMembership->memberRole == "Allow" || dMembership->memberRole == "Block" )
        {
          // ...make sure this contact isn't on the opposing list in the cache.
          // test each member in the opposing list, if we get a match, remove the
          // member from the list.
          QString dHandle = dMember->handle();
          ABMembership *opposite = ( dMembership->memberRole == "Allow" ) ? cacheMemberships.value( "Block" ) : cacheMemberships.value( "Allow" );
          foreach( ABBaseMember *m, opposite->members )
          {
            if ( m->handle() == dHandle )
            {
              opposite->members.removeAll( m );
              delete m;
            }
          }
        }

        // must remove the member from the delta list now.
        dMembership->members.removeAll( dMember );

        continue;
      }
      
      // is in cache, so delete if deleted, otherwise copy to update.
      if ( dMember->deleted )
      {
        cMembership->members.removeAll( cMember );
        delete cMember;
      }
      else
      {
        // NOTE: I am assuming that typeof(cMember) == typeof(dMember) !!!
        *cMember = *dMember;
      }
    }
  }
}
