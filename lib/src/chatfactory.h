/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Sjors Gielen <sjors@kmess.org>                            *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file chatfactory.h
 * @brief Creator and handler of MsnChat instances
 */

#ifndef CHATFACTORY_H
#define CHATFACTORY_H

#include <KMess/Message>
#include <KMess/NetworkGlobals>

#include <QPointer>

#include "switchboardmanager.h"

// Forward declarations
namespace KMess
{
  class Message;
  class MsnChat;
  class MsnSession;
  class Debugger;
};


namespace KMessInternal
{

  class MsnSwitchboardConnection;
  
/**
 * @brief Private master class for MsnChat objects.
 *
 * This private object creates and manages MsnChat objects. It does not instruct
 * the MsnChat objects to do anything; they are their own standalone entities.
 * This class simply creates the objects on request, keeps a list of them, and
 * destroys them when they are done.
 *
 * @author Sjors Gielen
 * @ingroup Root
 */
class ChatFactory : public QObject
{
  friend class KMess::Debugger;

  Q_OBJECT


  public:

    // The constructor
                                    ChatFactory( KMess::MsnSession *session );
    // The destructor
    virtual                        ~ChatFactory( );
    // Create a new MsnChat object and return it
    KMess::MsnChat                 *createChat( QString handle );
    // Find a chat with only this contact in it (do we need this call here?)
    //MsnChat                        *findChatWithContact() const;
    // Get the list of MsnChat objects
    const QList< QPointer<KMess::MsnChat> > &getChats() const;
    // Get an exclusive chat with a contact
    KMess::MsnChat                 *getExclusiveChatWith( const QString &handle ) const;

  public slots: 
    // should be called when a MsnChat needs to be created for a switchboard.
    KMess::MsnChat                 *createChat( KMessInternal::SwitchInfo info );
    void                            message( KMess::Message message );
    // reset the factory (delete all MsnChat instances)
    void                            reset();

  private slots:
    void                            slotChatDestroyed();

  private: // Private attributes
    QList< QPointer<KMess::MsnChat> > chats_;
    KMess::MsnSession                *session_;

  signals:
    void                            needSwitchboardTokens( KMess::MsnChat *chat );
    void                            chatCreated( KMess::MsnChat *chat );
    void                            chatDestroyed( KMess::MsnChat *chat );
    void                            chatMessageReceived( KMess::Message message );
    void                            switchboardRequested( KMess::MsnChat *chat );

};



} // namespace KMess



#endif
