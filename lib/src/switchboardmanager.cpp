/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Sjors Gielen <sjors@kmess.org>                            *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file switchboardmanager.cpp
 * @brief Creator and handler of MsnSwitchboardConnection instances
 */

#include "switchboardmanager.h"
#include "connections/msnswitchboardconnection.h"
#include "debug/libkmessdebug.h"
#include "chatfactory.h"


using namespace KMess;
using namespace KMessInternal;



/**
 * @brief The constructor
 */
SwitchboardManager::SwitchboardManager(KMess::MsnSession *session, KMessInternal::ChatFactory *chatFactory)
: QObject()
, session_(session)
, chatFactory_(chatFactory)
{
  KMESS_ASSERT(session);
  KMESS_ASSERT(chatFactory);
}



// The destructor
SwitchboardManager::~SwitchboardManager()
{
  debug() << "DESTROYING...";
  qDeleteAll( connections_.keys() );
  connections_.clear();
}



/**
 * When a non-data message comes in, create an MsnChat around the switchboard,
 * and stop listening to its signals.
 *
 * @param message KMess::Message from the switchboard.
 */
void SwitchboardManager::slotMessageReceived( KMess::Message message )
{
  MsnSwitchboardConnection *conn = qobject_cast<MsnSwitchboardConnection*>(sender());
  KMESS_ASSERT( conn );
  KMESS_ASSERT( connections_.keys().contains( conn ) );
  // KMESS_ASSERT(there is no msnchat with this switchboard yet)
  
  SwitchInfo info = connections_.value( conn );
  info.messages << message;

  switch( message.type () )
  {
    case DataMessageType:       // ignore data and p2p messages
    case PtpDataMessageType:
    case ClientCapsMessageType: // not enough info - could be data or text.
    case TypingMessageType:     // definitely text, but the user doesn't yet need to know.
      return;

    default: ;
  }

  // It's a chat message, create MsnChat and forget it
  chatFactory_->createChat(info);
  disconnect(conn, 0, this, 0);
}



void SwitchboardManager::slotContactJoined( QString contact )
{
  MsnSwitchboardConnection *conn = qobject_cast<MsnSwitchboardConnection*>( sender() );
  
  KMESS_ASSERT( conn );
  KMESS_ASSERT( connections_.contains( conn ) );
  
  SwitchInfo info = connections_.value( conn );
  if ( ! info.participants.contains( contact ) )
  {
    info.participants << contact;
  }
  
  // overwrite.
  connections_.insert( conn, info );
}



void SwitchboardManager::slotContactLeft( QString contact )
{
  MsnSwitchboardConnection *conn = qobject_cast<MsnSwitchboardConnection*>( sender() );
  
  KMESS_ASSERT( conn );
  KMESS_ASSERT( connections_.contains( conn ) );
  
  SwitchInfo info = connections_.value( conn );
  info.participants.removeAll( contact );
  
  // overwrite.
  connections_.insert( conn, info );
}



/**
 * This can be called by MsnNotificationConnection and will create a 
 * new MsnSwitchboardConnection instance and start the authentication process.
 *
 * @param handle Handle of the contact who requested the connection.
 * @param server Server hostname/IP.
 * @param port Server port
 * @param auth Authorization string
 * @param chatId Chat ID.
 */
void SwitchboardManager::startSwitchboard( const QString &handle, const QString &server, const quint16 port, const QString &auth, const QString &chatId )
{
  KMESS_ASSERT( ! server.isEmpty() );
  KMESS_ASSERT( port != 0 );
  KMESS_ASSERT( ! auth.isEmpty() );
  KMESS_ASSERT( ! chatId.isEmpty() );
  
  MsnSwitchboardConnection *conn = new MsnSwitchboardConnection( session_ );
  
  SwitchInfo info;
  info.startingHandle = handle;
  info.conn = conn;
  
  connections_.insert( conn, info );

  // three things we need to track: first message, contacts that join and contacts that leave.
  connect( conn, SIGNAL( receivedMessage( KMess::Message ) ),
           this,   SLOT( slotMessageReceived( KMess::Message ) ) );
  connect( conn, SIGNAL( contactJoined( QString ) ),
           this,   SLOT( slotContactJoined( QString ) ) );
  connect( conn, SIGNAL( contactLeft( QString,bool ) ),
           this,   SLOT( slotContactLeft( QString ) ) );
  
  emit switchboardCreated(conn);

  conn->start( server, port, auth, chatId );
}
