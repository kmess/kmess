# Find LibKMess-Msn
# Find the KMess MSN Network library and includes
# This module defines
#  LIBKMESS_MSN_FOUND       - If false, do not try to use LibKMess-Msn
#  LIBKMESS_MSN_INCLUDE_DIR - where to find msnsession.h and friends
#  LIBKMESS_MSN_LIBRARIES   - the libraries needed to use LibKMess-Msn
#  LIBKMESS_MSN_LIBRARY     - Where to find the LibKMess-Msn library (generally not used)

# Most of these paths were copied from FindSDL.cmake. They are very much complete, but if
# anything is still missing, let us know!
FIND_PATH( LIBKMESS_MSN_INCLUDE_DIR "KMess/MsnSession"
  HINTS
  $ENV{LIBKMESSDIR}/include
  PATH_SUFFIXES KMess
  PATHS
  ~/Library/Frameworks
  /Library/Frameworks
  /usr/local/include
  /usr/include
  /sw/include # Fink
  /opt/local/include # DarwinPorts
  /opt/csw/include # Blastwave
  /opt/include

  # If MsnSession is in /usr/include/KMess/MsnSession, we want
  # /usr/include as KMESS_INCLUDE_DIR, not /usr/include/KMess.
  # Therefore we search for this last:
  /usr/local/include/KMess
  /usr/include/KMess
  /sw/include/KMess # Fink
  /opt/local/include/KMess # DarwinPorts
  /opt/csw/include/KMess # Blastwave
  /opt/include/KMess
)
#MESSAGE("LIBKMESS_MSN_INCLUDE_DIR is ${LIBKMESS_MSN_INCLUDE_DIR}")

FIND_LIBRARY( LIBKMESS_MSN_LIBRARY
  NAMES kmess-msn
  HINTS
  $ENV{LIBKMESSDIR}
  PATH_SUFFIXES lib64 lib
  PATHS
  /usr/local
  /usr
  /sw
  /opt/local
  /opt/csw
  /opt
)
#MESSAGE("LIBKMESS_MSN_LIBRARY is ${LIBKMESS_MSN_LIBRARY}")

# handle the QUIETLY and REQUIRED arguments and set LIBKMESS_MSN_FOUND to TRUE if
# all listed variables are TRUE
INCLUDE( FindPackageHandleStandardArgs )
FIND_PACKAGE_HANDLE_STANDARD_ARGS( LIBKMESS_MSN DEFAULT_MSG LIBKMESS_MSN_LIBRARY LIBKMESS_MSN_INCLUDE_DIR )

IF( LIBKMESS_MSN_FOUND )
  SET( LIBKMESS_MSN_LIBRARIES ${LIBKMESS_MSN_LIBRARY} )
ENDIF( LIBKMESS_MSN_FOUND )
