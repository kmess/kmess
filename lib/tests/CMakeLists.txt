set( EXECUTABLE_OUTPUT_PATH ${CMAKE_CURRENT_BINARY_DIR} )

FIND_PACKAGE( KDE4 REQUIRED )

# Make sure KDE4 is installed
IF( NOT KDE4_FOUND )
  MESSAGE( FATAL_ERROR "\n"
  "  The KDE Development Platform was not found.\n"
  "  You need the KDE Development Platform (version 4) to be installed to \n"
  "  run the libkmess-msn tests." )
ENDIF( NOT KDE4_FOUND )

INCLUDE( KDE4Defaults )
ADD_DEFINITIONS( ${QT_DEFINITIONS} ${KDE4_DEFINITIONS} )

include_directories(${QT_INCLUDES} ${CMAKE_CURRENT_SOURCE_DIR}/../include )
SET( LIBKMESS_MSN_LIBRARIES kmess-msn )

MACRO(LIBKMESS_UNIT_TESTS)
  FOREACH(_testname ${ARGN})
    kde4_add_unit_test(${_testname} NOGUI ${_testname}.cpp)
    target_link_libraries(${_testname} ${LIBKMESS_MSN_LIBRARIES}
                                       ${QT_QTTEST_LIBRARY}
                                       ${QT_QTCORE_LIBRARY}
                                       ${LIBXML2_LIBRARIES}
                                       ${LIBXSLT_LIBRARIES} )
  ENDFOREACH(_testname)
ENDMACRO(LIBKMESS_UNIT_TESTS)

LIBKMESS_UNIT_TESTS(
  msnalgorithmstest
  contactprofiletest
  textmessagetest
)
