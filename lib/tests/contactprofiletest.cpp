#include <KMess/ContactProfile>
#include <QtTest/QtTest>
#include "../src/soap/bindings/abdatatypes.h"

namespace KMess
{
  
class ContactProfileTest : public QObject
{
  Q_OBJECT
  
private slots:
  void phoneNumbers();
  void emailAddresses();
  void emailMessengerUser();
  void passportMessengerUser();
  void messengerHandle();
};

};

using namespace KMess;
using namespace KMessInternal;

void ContactProfileTest::phoneNumbers()
{
  ContactProfile profile( NULL, NULL );
  
  profile.setPhoneNumber( ContactProfile::PhonePersonal, "1234567890" );
  QCOMPARE( profile.phoneNumbers().count(), 1 );
  QCOMPARE( profile.phoneNumber( ContactProfile::PhonePersonal ), QString( "1234567890" ) );
  
  profile.setPhoneNumber( ContactProfile::PhonePersonal, "0987654321" );
  QCOMPARE( profile.phoneNumbers().count(), 1 );
  QCOMPARE( profile.phoneNumber( ContactProfile::PhonePersonal ), QString( "0987654321" ) );
  
  profile.setPhoneNumber( (ContactProfile::PhoneType)(12345), "11223344" );
  QCOMPARE( profile.phoneNumbers().count(), 1 );
  QCOMPARE( profile.phoneNumber( (ContactProfile::PhoneType)(12345) ), QString() );

  profile.setPhoneNumber( ContactProfile::PhoneFax, "1-2-3-4-5" );
  QCOMPARE( profile.phoneNumbers().count(), 2 );
  QCOMPARE( profile.phoneNumber( ContactProfile::PhoneFax ), QString( "1-2-3-4-5" ) );
}

void ContactProfileTest::emailAddresses()
{
  ContactProfile profile( NULL, NULL );
  
  profile.setEmailAddress( ContactProfile::EmailMessenger, "foo@bar.com" );
  QCOMPARE( profile.emailAddresses().count(), 1 );
  QCOMPARE( profile.emailAddress( ContactProfile::EmailMessenger ), QString( "foo@bar.com" ) );
  
  profile.setEmailAddress( ContactProfile::EmailPersonal, "bar@baz.com" );
  QCOMPARE( profile.emailAddresses().count(), 2 );
  QCOMPARE( profile.emailAddress( ContactProfile::EmailPersonal ), QString( "bar@baz.com" ) );
  
  profile.setEmailAddress( ContactProfile::EmailPersonal, "bar@foo.com" );
  QCOMPARE( profile.emailAddresses().count(), 2 );
  QCOMPARE( profile.emailAddress( ContactProfile::EmailPersonal ), QString( "bar@foo.com" ) );
  
  profile.setEmailAddress( ( ContactProfile::EmailType )(12345), "bogus@email.com" );
  QCOMPARE( profile.emailAddresses().count(), 2 );
  QCOMPARE( profile.emailAddress( ( ContactProfile::EmailType )(12345) ), QString() );
}

void ContactProfileTest::emailMessengerUser()
{
  ABContactInfo info;
  info.isMessengerUser = true; // this value is supposed to be ignored for non-passport contacts
  
  ContactProfile profile( NULL, &info );
  
  QCOMPARE( profile.isMessengerUser(), false ); // no passport name, so the value comes from the email addresses

  // set a couple email addies, one which is a messenger email.
  profile.setEmailAddress( ContactProfile::EmailMessenger, "foo@live.com" );
  profile.setEmailAddress( ContactProfile::EmailPersonal, "bar@baz.com" );
  
  QCOMPARE( profile.isMessengerUser(), false );
  
  profile.setIsMessengerUser( true );
  
  QCOMPARE( profile.isMessengerUser(), true);
}


void ContactProfileTest::passportMessengerUser()
{
  ContactProfile profile( NULL, NULL );
  
  profile.info_->passportName = "my@passport.com";

  QCOMPARE( profile.isMessengerUser(), false );
  
  profile.setIsMessengerUser( true );

  QCOMPARE( profile.isMessengerUser(), true );
}


void ContactProfileTest::messengerHandle()
{
  ContactProfile profile( NULL, NULL );
  
  profile.setEmailAddress( ContactProfile::EmailMessenger, "foo@bar.com" );
  profile.setEmailAddress( ContactProfile::EmailPersonal, "bar@baz.com" );
  
  QCOMPARE( profile.messengerHandle(), QString( "foo@bar.com" ) );
  
  profile.setEmailAddress( ContactProfile::EmailMessenger, QString() );
  
  QCOMPARE( profile.messengerHandle(), QString() );
  
  // i don't like digging into implementation, but i have to.
  // there's no public way to set the passport name, nor should there be.
  profile.info_->passportName = "my@passport.com";

  QCOMPARE( profile.messengerHandle(), QString( "my@passport.com" ) );
}

QTEST_MAIN(ContactProfileTest);

#include "contactprofiletest.moc"
