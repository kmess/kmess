/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Sjors Gielen <sjors@kmess.org>                            *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file msnalgorithmstest.cpp
 */

#include "msnalgorithmstest.h"
#include "KMess/MsnAlgorithms"
#include <QtTest/QtTest>

void MsnAlgorithmsTest::convertHtmlColorToMsnColor()
{
    // Normal usage
    QCOMPARE( KMess::MsnAlgorithms::convertHtmlColorToMsnColor( "#AABBCC" ),
                                                        QString("CCBBAA") );

    // Colors that don't exist
    QCOMPARE( KMess::MsnAlgorithms::convertHtmlColorToMsnColor( "#AABB00" ),
                                                        QString("BBAA"  ) );
    QCOMPARE( KMess::MsnAlgorithms::convertHtmlColorToMsnColor( "#AA0000" ),
                                                        QString("AA"    ) );
    QCOMPARE( KMess::MsnAlgorithms::convertHtmlColorToMsnColor( "#000000" ),
                                                        QString("0"     ) );

    // Tricky values
    QCOMPARE( KMess::MsnAlgorithms::convertHtmlColorToMsnColor( "#AA00CC" ),
                                                        QString("CC00AA") );
    QCOMPARE( KMess::MsnAlgorithms::convertHtmlColorToMsnColor( "#00BBCC" ),
                                                        QString("CCBB00") );
    QCOMPARE( KMess::MsnAlgorithms::convertHtmlColorToMsnColor( "#0000CC" ),
                                                        QString("CC0000") );

    // Making sure every hex character works well
    QCOMPARE( KMess::MsnAlgorithms::convertHtmlColorToMsnColor( "#012345" ),
                                                        QString("452301") );
    QCOMPARE( KMess::MsnAlgorithms::convertHtmlColorToMsnColor( "#6789AB" ),
                                                        QString("AB8967") );
    QCOMPARE( KMess::MsnAlgorithms::convertHtmlColorToMsnColor( "#CDEF00" ),
                                                        QString("EFCD"  ) );
}

void MsnAlgorithmsTest::convertMsnColorToHtmlColor()
{
    // Normal usage
    QCOMPARE( KMess::MsnAlgorithms::convertMsnColorToHtmlColor( "AABBCC" ),
                                                        QString("#CCBBAA") );

    // Colors that don't exist
    QCOMPARE( KMess::MsnAlgorithms::convertMsnColorToHtmlColor( "AABB"   ),
                                                        QString("#BBAA00") );
    QCOMPARE( KMess::MsnAlgorithms::convertMsnColorToHtmlColor( "AA"     ),
                                                        QString("#AA0000") );
    QCOMPARE( KMess::MsnAlgorithms::convertMsnColorToHtmlColor( "0"      ),
                                                        QString("#000000") );

    // Tricky values
    QCOMPARE( KMess::MsnAlgorithms::convertMsnColorToHtmlColor( "CC00AA" ),
                                                        QString("#AA00CC") );
    QCOMPARE( KMess::MsnAlgorithms::convertMsnColorToHtmlColor( "CCBB00" ),
                                                        QString("#00BBCC") );
    QCOMPARE( KMess::MsnAlgorithms::convertMsnColorToHtmlColor( "CC0000" ),
                                                        QString("#0000CC") );

    // Making sure every hex character works well
    QCOMPARE( KMess::MsnAlgorithms::convertMsnColorToHtmlColor( "452301" ),
                                                        QString("#012345") );
    QCOMPARE( KMess::MsnAlgorithms::convertMsnColorToHtmlColor( "AB8967" ),
                                                        QString("#6789AB") );
    QCOMPARE( KMess::MsnAlgorithms::convertMsnColorToHtmlColor( "EFCD"   ),
                                                        QString("#CDEF00") );
}

void MsnAlgorithmsTest::createAuthBlob()
{
    QByteArray resultBA;
    QFETCH( QByteArray, token );
    QFETCH( QByteArray, nonce );

    int result = KMess::MsnAlgorithms::createAuthBlob(resultBA, nonce, token);
    QCOMPARE( result, 0 );
    // Auth blobs have a "random factor" in them (gcry_create_nonce).
    // Therefore you can not check whether a token and nonce give the correct
    // output, but at least you can check for some common stuff...
    QVERIFY(  resultBA.toBase64().startsWith(
                                  "HAAAAAEAAAADZgAABIAAAAgAAAAUAAAASAAAA" ) );
}

void MsnAlgorithmsTest::createAuthBlob_data()
{
    QTest::addColumn<QByteArray>("token");
    QTest::addColumn<QByteArray>("nonce");

    QTest::newRow("test1") << QByteArray("p9RDbsxYbSMiIlbw+t7w8oXuyPJ+Me4B")
                           << QByteArray("7KF2zTMh+hCJZ4AUon4iK0IST6BJXoTqLAk"
                                         "aVbYTQLb99M4moevcHaOEUpIV/z4m");

    QTest::newRow("test2") << QByteArray("pU0AVqTF0k7IdTwnqQzCIRYNgMkUPTuS")
                           << QByteArray("tVEHwYP0QGHEz9Yb+Y5+y1EJFIJp3Sk1Bgb"
                                         "aR5HZWZnpC7TzDXvdrMDBf9gOdUhb");

    QTest::newRow("test3") << QByteArray("HIBdmBmuafWTs0QOmijigMaVgiqA3G/V")
                           << QByteArray("gGxXKoDaqXMZq4kvvtjRxS/q3DxPEe6xEJ0"
                                         "C9RMEiu9EaHc1wLQh/XiqXA9/xpRo");
}

QTEST_MAIN(MsnAlgorithmsTest)
#include "msnalgorithmstest.moc"
