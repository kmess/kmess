/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Sjors Gielen <sjors@kmess.org>                            *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file textmessagetest.cpp
 */

#include "textmessagetest.h"
#include <QtTest/QtTest>
#include <KMess/TextMessage>
#include <KMess/Message>
#include <KMess/EmoticonMessage>

void TextMessageTest::initTestCase()
{
}

void TextMessageTest::createTestMessage()
{
  KMess::TextMessage tm;
  tm.setMessage("Hello World");
  tm.setPeerName("Some Guy");

  QCOMPARE(tm.message(), QString("Hello World"));
  QCOMPARE(tm.peerName(), QString("Some Guy"));
}

void TextMessageTest::castTestMessage()
{
  KMess::Message m;
  {
    KMess::TextMessage tm;
    tm.setMessage("Hello World");
    tm.setPeerName("Some Guy");
    MsnObjectMap emoticons;
    emoticons[";d"] = "<msnobj Creator=\"dazjorz@dazjorz.com\" Size=\"43594\""
                    " Type=\"2\" Location=\"lAPv_6CM+_yL7Hn+A0oomR2T6DI=.png\""
                    " Friendly=\"AA==\" SHA1D=\"lAPv/6CM+/yL7Hn+A0oomR2T6DI=\""
                    " SHA1C=\"CNvBCeuPsdFNyoe3zUDINpKdXB4=\"/>";
    tm.setEmoticons(emoticons);
    m = tm;
  }

  KMess::TextMessage tm_( m );
  QCOMPARE(tm_.message(), QString("Hello World"));
  QCOMPARE(tm_.peerName(), QString("Some Guy"));
  QCOMPARE(tm_.emoticons().size(), 1);
  QString emoticon = tm_.emoticons().value(";d");
  QVERIFY(emoticon.startsWith("<msnobj"));
  QVERIFY(emoticon.contains("lAPv_6CM+"));
  QVERIFY(emoticon.contains("lAPv/6CM+"));
  QVERIFY(emoticon.endsWith("XB4=\"/>"));
}

void TextMessageTest::cleanupTestCase()
{
}

QTEST_MAIN(TextMessageTest)
#include "textmessagetest.moc"
