/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Mike K. Bennett <mkb137b@hotmail.com>                     *
 * (C) Copyright Sjors Gielen <sjors@kmess.org>                            *
 * (C) Copyright Adam Goossens <fontknocker@kmess.org>                     *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file contactlist.h
 * @brief Interface to the Windows Live address book
 */

#ifndef CONTACTLIST_H
#define CONTACTLIST_H

#include <KMess/NetworkGlobals>
#include <QListIterator>

// Forward declarations
namespace KMessInternal
{
  class AddressBookService;
  class ListsManager;
  class MsnNotificationConnection;
};


class QAbstractItemModel;

namespace KMess
{
class MsnContact;
class MsnSession;
class Debugger;
class MsnChat; // TODO: remove.
class ContactProfile;

/**
 * @brief Interface class for manipulating the contact list.
 * 
 * This is the means to manipulate a signed in contact list.
 * 
 * @author Adam Goossens
 * @todo Create appropriate documentation
 */
class ContactList : public QObject
{
  friend class MsnSession;
  friend class KMessInternal::MsnNotificationConnection;
  friend class KMess::Debugger;
  friend class KMess::MsnChat; // TODO: remove.

  Q_OBJECT

  private:
    // The constructor
                             ContactList( MsnSession *session );

  public:
    // The destructor
    virtual                 ~ContactList();

  public slots:
    // Request the service to add a contact to the friends list
    void                     addContact( const KMess::MsnContact *contact, const QList<MsnGroup*> &groups = QList<MsnGroup*>() );
    void                     addContact( const QString &handle, const KMess::NetworkType network = NetworkMsn, const QList<MsnGroup *> &groups = QList<MsnGroup*>() );
    // Request the service to add a contact to another group
    void                     addContactToGroup( const KMess::MsnContact *contact, const KMess::MsnGroup *group );
    // Request the service to add a new group
    void                     addGroup( const QString &name );    
    // Request the service to allow a contact to see the user's status
    void                     unblockContact( const KMess::MsnContact *contact );
    // Request the service to block a contact
    void                     blockContact( const KMess::MsnContact *contact );
    // Return a contact by handle
    KMess::MsnContact       *contact( const QString &handle ) const;
    // Get a list with all contacts
    const ContactsList       contacts() const;
    // Return the contact list's model
    QAbstractItemModel*      contactListModel();
    // Fetch the contact list from the server
    void                     fetch( const QString &cacheDir );
    // Get a group from its id
    MsnGroup*                group( const QString &id ) const;
    // Return a list of all known groups
    GroupsList               groups() const;
    // Return a list of all known groups, indexed by group ID.
    IndexedGroupsList        groupsIndexedList() const;
    // Create an interator to iterator over a given membership list.
    QListIterator<MsnContact*> *iterator( MsnMembership membership = MSN_LIST_FRIEND );
    // Request the service to move a contact between two groups
    void                     moveContact( const KMess::MsnContact *contact, const KMess::MsnGroup *oldGroup, const KMess::MsnGroup *newGroup );
    // Request the service to remove a contact from the friends list
    void                     removeContact( KMess::MsnContact *contact, RemoveContactOption option = RemoveFromMessenger, RemoveContactAction action = RemoveOnly );
    // Request the service to remove a contact from a group
    void                     removeContactFromGroup( const KMess::MsnContact *contact, const KMess::MsnGroup *group );
    // Request the service to remove a group
    void                     removeGroup( const KMess::MsnGroup *group );
    // Request the service to rename a group
    void                     renameGroup( const KMess::MsnGroup *group, const QString &newName );
    // List of unknown profiles
    QList<ContactProfile*>   unknownProfiles() const;

  private:
    // internal.
    KMessInternal::ListsManager *listsManager() const;
    KMessInternal::AddressBookService *abService() const;

  private: // Private attributes
    // The address book service
    KMessInternal::AddressBookService *abService_;
    // The account's contacts/groups list manager
    KMessInternal::ListsManager *listsManager_;
    // MsnSession instance
    MsnSession *session_;

  signals: // Public signals
    // There was a problem manipulating the contact list
    void                     error( KMess::StatusMessage message );
    // Signal that the addition of a contact was successful
    void                     contactAdded( KMess::MsnContact *contact );
    // Signal that adding a contact to a group was successful
    void                     contactAddedToGroup( KMess::MsnContact *contact, KMess::MsnGroup *group );
    // Signal that a contact was blocked
    void                     contactBlocked( KMess::MsnContact *contact );
    // Signal that a contact was removed from a group
    void                     contactRemovedFromGroup( KMess::MsnContact *contact, KMess::MsnGroup *group );
    // Signal that a contact was removed successfully.
    void                     contactRemoved( KMess::MsnContact *contact );
    // Signal that a contact was unblocked
    void                     contactUnblocked( KMess::MsnContact *contact );
    // Signal that a group was added OK.
    void                     groupAdded( KMess::MsnGroup *group );
    // Signal that a group was removed OK
    void                     groupRemoved( KMess::MsnGroup *group );
    // Signal that a group was renamed OK
    void                     groupRenamed( KMess::MsnGroup *group );
};



} // namespace KMess



#endif
