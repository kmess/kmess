/***************************************************************************
       contactprofile.h - A representation of a contact's AB entry
                             -------------------
    begin                : Sun 25 Jul 10
    copyright            : (C) 2010 by Adam Goossens
    email                : fontknocker@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CONTACTPROFILE_H
#define CONTACTPROFILE_H

#include <QHash>
#include <QObject>

namespace KMessInternal
{
  class ABContactInfo;
  class AddressBookService;
  class MsnNotificationConnection;
  class ABContactEmail;
}

namespace KMess
{

class Self;
class MsnContact;

/**
 * @brief A representation (and means to change) address book data for a contact
 *
 * The MSN Address Book provides means for storing custom information about a
 * contact such as a nickname, custom name, phone numbers, etc. This class
 * gives you a means to view that data.
 * 
 * You can modify certain address book properties here. Properties you can modify
 * include a comment and a nickname.
 */
class ContactProfile : public QObject
{
  friend class MsnContact;
  friend class KMessInternal::MsnNotificationConnection;
  friend class KMessInternal::AddressBookService;
  friend class ContactProfileTest;
  friend class Self;

  Q_OBJECT

  public:
    /**
     * The various phones that a contact can set numbers for.
     */
    enum PhoneType
    {
      PhoneUnknown,
      PhonePersonal,
      PhoneBusiness,
      PhoneMobile,
      PhonePager,
      PhoneOther,
      PhoneFax
    };

    /**
     * The various types of email addresses a contact can have.
     */
    enum EmailType
    {
      EmailUnknown,          /// This is not a recognised email address
      EmailMessenger,        /// This is the email address used as their Messenger account
      EmailPersonal,         /// This is a contact's personal email address
      EmailBusiness,         /// This is a contact's business (work) email address
      EmailOther             /// This is an extra email address for the contact
    };
    
  private:
    ContactProfile( KMessInternal::AddressBookService *abService = 0L, KMessInternal::ABContactInfo *other = 0L );

  public:
    ~ContactProfile();

  public:
//     bool        autoUpdate() const;
    long        cid() const;                // passport only. everyone else is -1.
    QString     comment() const;
    bool        hasMobileDevice() const;
    bool        isMessengerUser() const;
    bool        isPassportUser() const;
    QString     phoneNumber( PhoneType type ) const;
    QHash<PhoneType,QString> phoneNumbers() const;
    QString     emailAddress( EmailType type ) const;
    QHash<EmailType,QString> emailAddresses() const;
    QString     messengerHandle() const;
    QString     nickname() const;
    QString     firstName() const;
    QString     lastName() const;
    QString     middleName() const;

  // setters
  public:
    QHash<QString,QString> annotations() const;
//     void        setAutoUpdate( bool autoUpdate );
    void        setComment( const QString &comment );
    void        setNickName( const QString &nickName );
    void        setFirstName( const QString &firstName );
    void        setMiddleName( const QString &middleName );
    void        setLastName( const QString &lastName );
    void        setPhoneNumber( PhoneType type, const QString &phoneNumber );
    void        setEmailAddress( EmailType type, const QString &email );
  private:  // internal methods.
    QString     annotation( const QString &name ) const;
    KMessInternal::ABContactEmail *_email( EmailType type ) const;
    EmailType       _emailStrToType( const QString &str ) const;
    QString         _emailTypeToStr( EmailType type ) const;
    void        emitProfileChanged();
    KMessInternal::ABContactInfo *info() const;
    void        setContactInfo( KMessInternal::ABContactInfo *info );
    void        setAnnotation( QString name, QString value );
    void        setDisplayName( const QString &name );
    void        setIsMessengerUser( bool isMessenger );
    void        update();

  protected:
    KMessInternal::AddressBookService *abService_;
    KMessInternal::ABContactInfo      *info_; // this is where the data is stored.

  private:
    QHash<QString,QString> annotations_;

  signals:
    // emitted when the profile data is altered.
    void        profileChanged();
};

} // namespace KMess

#endif
