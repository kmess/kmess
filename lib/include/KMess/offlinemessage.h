/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Valerio Pilo <valerio@kmess.org>                          *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file offlinemessage.h
 * @brief Offline message
 */

#ifndef KMESS_OFFLINEMESSAGE_H
#define KMESS_OFFLINEMESSAGE_H

#include "message.h"

#include <KMess/NetworkGlobals>

#include <QDebug>
#include <QTime>



namespace KMess {

  // Forward declarations
  class MsnContact;
  class MsnSession;
  class OfflineMessagePrivate;



  /**
   * @class OfflineMessage
   *
   * An Offline-IM Message.
   */
  class OfflineMessage : public Message
  {

    public: // Public constructors/destructors
                             OfflineMessage();
                             OfflineMessage( KMess::MsnChat* chat, KMess::MsnContact* peer, const QString body );
                             OfflineMessage( const Message &other );
      virtual               ~OfflineMessage();

    public: // Public methods
      void                   identifyPeers( MsnSession *session );
      const QString          message() const;
      const QString&         recipient() const;
      const QString&         sender() const;
      quint32                sequenceNumber() const;
      void                   setMessage( const QString &body );
      void                   setRecipient( MsnContact *recipient );
      void                   setSender( MsnContact *sender );
      void                   setSequenceNumber( const quint32 number );

    private: // Private properties
      OfflineMessagePrivate *d;
  };



}; // namespace KMess



#endif // KMESS_OFFLINEMESSAGE_H
