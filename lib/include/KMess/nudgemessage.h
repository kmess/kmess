/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Valerio Pilo <valerio@kmess.org>                          *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file nudgemessage.h
 * @brief Message containing a nudge
 */

#ifndef KMESS_NUDGEMESSAGE_H
#define KMESS_NUDGEMESSAGE_H

#include "message.h"

#include <KMess/NetworkGlobals>

#include <QDebug>
#include <QTime>



namespace KMess {


  /**
   * @class NudgeMessage
   *
   * A nudge message.
   *
   * Receiving a nudge usually implies a chat message about having received one,
   * some shaking of the chat window, and a sound.
   *
   * They're used to grab attention from the contact(s).
   */
  class NudgeMessage : public Message
  {

    public: // Public constructors/destructors
                             NudgeMessage();
                             NudgeMessage( MsnChat *chat, MsnContact* peer );
                             NudgeMessage( const Message &other );
      virtual               ~NudgeMessage();

    public: // Public methods
      // TODO

  };



}; // namespace KMess



#endif // KMESS_NUDGEMESSAGE_H
