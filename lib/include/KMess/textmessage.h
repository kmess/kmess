/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Valerio Pilo <valerio@kmess.org>                          *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file textmessage.h
 * @brief A text chat message
 */

#ifndef KMESS_TEXTMESSAGE_H
#define KMESS_TEXTMESSAGE_H

#include "message.h"

#include <KMess/NetworkGlobals>

#include <QColor>
#include <QDebug>
#include <QFont>
#include <QString>



namespace KMess {

  class TextMessagePrivate;


  /**
   * @class TextMessage
   *
   * A text chat message.
   *
   * Chat messages have formatting and emoticons.
   */
  class TextMessage : public Message
  {
    public: // Public constructors/destructors
                             TextMessage();
                             TextMessage( MsnChat *chat, MsnContact* peer );
                             TextMessage( const Message &other );
      virtual               ~TextMessage();

    public: // Public methods
      const QColor&          color() const;
      const MsnObjectMap&    emoticons() const;
      const QFont&           font() const;
      QString                message() const;
      const QString&         peerName() const;
      void                   setColor( const QColor& color );
      void                   setEmoticons( const MsnObjectMap& emoticons );
      void                   setFont( const QFont& font );
      void                   setMessage( const QString& message );
      void                   setPeerName( const QString& peerName );

    protected:
      virtual void           prepare();

    private: // Private properties
  };



}; // namespace KMess



#endif // KMESS_TEXTMESSAGE_H
