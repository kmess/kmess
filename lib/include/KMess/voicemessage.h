/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Valerio Pilo <valerio@kmess.org>                          *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file voicemessage.h
 * @brief Message with a recorded audio clip
 */

#ifndef KMESS_VOICEMESSAGE_H
#define KMESS_VOICEMESSAGE_H

#include "message.h"

#include <KMess/NetworkGlobals>

#include <QDebug>
#include <QTime>



namespace KMess {

  class VoiceMessagePrivate;


  /**
   * @class VoiceMessage
   *
   * A voice clip message.
   */
  class VoiceMessage : public Message
  {

    public: // Public constructors/destructors
                             VoiceMessage();
                             VoiceMessage( MsnChat *chat, MsnContact* peer );
                             VoiceMessage( const Message &other );
      virtual               ~VoiceMessage();

    public: // Public methods
      // TODO

    private: // Private properties

  };



}; // namespace KMess



#endif // KMESS_VOICEMESSAGE_H
