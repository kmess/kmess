/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Valerio Pilo <valerio@kmess.org>                          *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file debugger.h
 * @brief Class to debug the library
 */

#ifndef DEBUGGER_H
#define DEBUGGER_H

#include <KMess/Message>

#include <QObject>
#include <QPointer>
#include <QUrl>


namespace KMessInternal
{
  class HttpSoapConnection;
  class MsnSwitchboardConnection;
  class SoapMessage;
};

namespace KMess
{
// Forward declarations
class Message;
class MsnChat;
class MsnSession;


/**
 * A debugger class.
 *
 * With this, it is possible to interact with the library internals.
 * Many methods allow simple passive listening of the library activity,
 * but other methods also allow interaction, for more complex debugging
 *.activities.
 *
 * WARNING: Handle with care.
 *
 * @author Valerio Pilo <valerio@kmess.org>
 */
class Debugger : public QObject
{
  Q_OBJECT


  public:

    enum LogDataSource
    {
      SOURCE_UNKNOWN
    , SOURCE_NOTIFICATION
    , SOURCE_SWITCHBOARD
    , SOURCE_SOAP_ADDRESS_BOOK
    , SOURCE_SOAP_STORAGE
    , SOURCE_SOAP_PASSPORT
    , SOURCE_SOAP_OFFLINE_MESSAGING
    };


  public:

    Debugger( KMess::MsnSession* parentSession );
   ~Debugger();

    void sendCommand( const QString&, const QString&, const QByteArray&, const KMess::MsnChat* = 0 );
    void sendData( const QByteArray&, const KMess::MsnChat* = 0 );
    void sendMessage( KMess::Message, const KMess::MsnChat* = 0 );
    void sendRequest( const QUrl&, const QString&, const QByteArray&, const QByteArray& );


  public: // TODO Move into a pvt class

    void logDataReceived( const QObject*, const QByteArray& );
    void logDataSent( const QObject*, const QByteArray& );

    void logResponseReceived( const QObject*, const KMessInternal::SoapMessage* );
    void logRequestSent( const QObject*, const KMessInternal::SoapMessage* );


  private slots:

    void chatCreated( KMess::MsnChat* );
    void soapClientCreated( KMessInternal::HttpSoapConnection* );


  private:

    bool sourceByObject( const QObject*, KMess::Debugger::LogDataSource&, quint32&, QString& );


  private:

    QPointer<KMess::MsnSession> session_;
    QList<const QObject*> chats_;


  signals:

    /**
     * MSN Protocol data logger.
     *
     * Connect to this signal to receive all incoming and outgoing MSNP data from the
     * library. Everything passes through here: all traffic from Notification, all
     * Switchboards, and even SOAP is logged.
     *
     * @param data A textual representation of the data
     * @param isReceived If true, the data is incoming, else it's outgoing
     * @param source Type of data source which received or sent this data
     * @param connectionId An id for the connection, this is unique per source type.
     *                     For unique connections, this is always 0.
     * @param name A name for this connection. In fact it is an human-readable ID.
     */
    void logData( const QByteArray& data, bool isReceived, const KMess::Debugger::LogDataSource source, const quint32 connectionId, const QString& name );


};

}; // namespace KMess



#endif // DEBUGGER_H
