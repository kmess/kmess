/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Valerio Pilo <valerio@kmess.org>                          *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file clientcapabilities.h
 * @brief Client capabilities detection
 */

#ifndef CLIENTCAPABILITIES_H
#define CLIENTCAPABILITIES_H

#include <QString>


namespace KMess
{



/**
 * @brief Parsing and retrieval of MSNP clients capabilities.
 *
 * Client capabilities are flags determining which actions or features are
 * supported by a certain MSNP client.
 *
 *
 * @author Valerio Pilo <valerio@kmess.org>
 * @author Gregg Edghill <edghill@kde.org>
 */
class ClientCapabilities
{

  public:
    /**
     * Client capabilities flags.
     *
     * These flags describe the possible features supported by a client.
     * Some, however, describe a particular state the client currently has or not, for
     * example <code>CF_ONLINE_VIA_MOBILE</code>.
     */
    enum CapabilitiesFlag
    {
      CF_NONE                               = 0x00000000
    , CF_ONLINE_VIA_MOBILE                  = 0x00000001
    , CF_ONLINE_VIA_MSN_EXPLORER            = 0x00000002
    , CF_SUPPORTS_GIF_INK                   = 0x00000004
    , CF_SUPPORTS_ISF_INK                   = 0x00000008
    , CF_WEBCAM_DETECTED                    = 0x00000010
    , CF_SUPPORTS_MESSAGE_CHUNKING          = 0x00000020
    , CF_IS_MOBILE_ENABLED                  = 0x00000040
    , CF_IS_MSN_DIRECT_DEVICE_ENABLED       = 0x00000080
    , CF_MOBILE_MESSAGING_DISABLED          = 0x00000100
    , CF_ONLINE_VIA_WEBIM                   = 0x00000200
    , CF_MOBILE_DEVICE                      = 0x00000400
    , CF_ONLINE_VIA_FEDERATED_GATEWAY       = 0x00000800
    , CF_HAS_MSN_SPACE                      = 0x00001000
    , CF_IS_MEDIA_CENTER_EDITION_USER       = 0x00002000
    , CF_SUPPORTS_DIRECTIM                  = 0x00004000
    , CF_SUPPORTS_WINKS                     = 0x00008000
    , CF_SUPPORTS_SHARED_SEARCH             = 0x00010000
    , CF_IS_BOT                             = 0x00020000
    , CF_SUPPORTS_VOICEIM                   = 0x00040000
    , CF_P2P_SUPPORTS_SECURECHANNEL         = 0x00080000
    , CF_SUPPORTS_SIP_INVITE                = 0x00100000
    , CF_SUPPORTS_MULTIPARTY_MEDIA          = 0x00200000
    , CF_SUPPORTS_SHARED_DRIVE              = 0x00400000
    , CF_SUPPORTS_PAGE_MODE_MESSAGING       = 0x00800000
    , CF_HAS_ONECARE                        = 0x01000000
    , CF_P2P_SUPPORTS_TURN                  = 0x02000000
    , CF_P2P_SUPPORTS_BOOTSTRAPPING_VIA_UUN = 0x04000000
    , CF_USING_ALIAS                        = 0x08000000
    , CF_VERSION_6_0                        = 0x10000000
    , CF_VERSION_6_1                        = 0x20000000
    , CF_VERSION_6_2                        = 0x30000000
    , CF_VERSION_7_0                        = 0x40000000 // Messenger 7.0 caps 0x4000C024: ink-gif, multi-packet, direct-im, winks
    , CF_VERSION_7_5                        = 0x50000000
    , CF_VERSION_8_0                        = 0x60000000
    , CF_VERSION_8_1                        = 0x70000000 // Messenger 8.1 caps 0x765DC02C: ink-gif, (ink-isf), multi-packet, direct-im, winks, shared-search, voice-clips, secure-channel, sip, folder-sharing, turn, uun
    , CF_VERSION_8_5                        = 0x80000000 // Messenger 8.5 caps 0x865DC02C: ink-gif, ink-isf, multi-packet, direct-im, winks, shared-search, voice-clips, secure-channel, sip, folder-sharing
    , CF_VERSION_9_0                        = 0x90000000 // Messenger 9.0 caps 0x963CC03C
    , CF_VERSION_14_0                       = 0xA0000000
    };
    Q_DECLARE_FLAGS( CapabilitiesFlags, CapabilitiesFlag )


    /**
     * Client extended capabilities flags.
     *
     * These flags are an addition to the CapabilitiesFlags.
     * Probably the MSN protocol development team also had the caps into an enum, because they
     * seem to have had no more available values for new caps and resorted to another enum
     */
    enum CapabilitiesExtendedFlag
    {
      CXF_NONE                         = 0x00000000
    , CXF_SUPPORTS_RTC_VIDEO           = 0x10000000  ///< The client supports RTC Video
    , CXF_SUPPORTS_P2P_VERSION_2       = 0x20000000  ///< Client supports P2P v2
    };
    Q_DECLARE_FLAGS( CapabilitiesExtendedFlags, CapabilitiesExtendedFlag )

    /**
     * Container for the client version number.
     */
    struct Version
    {
      int major;
      int minor;

      /**
       * Constructor for a client version number.
       * @param vMajor Major version number
       * @param vMinor Minor version number
       */
      Version( int vMajor = 0, int vMinor = 0 )
      {
        major = vMajor;
        minor = vMinor;
      }

      /**
       * Convert the version into a string.
       * @return QString
       */
      QString toString()
      {
        return QString::number( major ) + "." + QString::number( minor );
      }
    };


  public:

    ClientCapabilities();
    ClientCapabilities( const QString );
    CapabilitiesFlags caps() const;
    CapabilitiesExtendedFlags extCaps() const;
    bool setCaps( const QString );
    bool setCaps( const CapabilitiesFlags, const CapabilitiesExtendedFlags = CXF_NONE );
    QString toString() const;
    Version getClientVersion() const;
    bool isOnlineViaTheWeb() const;
    bool isOnlineViaMobileDevice() const;
    bool supportsGifInk() const;
    bool supportsIsfInk() const;
    bool supportsMessageChunking() const;
    bool supportsP2p() const;


  protected:

    static bool parseCaps( const QString string, CapabilitiesFlags&, CapabilitiesExtendedFlags& );


  private:

    CapabilitiesFlags capabilities_;
    CapabilitiesExtendedFlags extendedCapabilities_;
    QString capsString_;


};

Q_DECLARE_OPERATORS_FOR_FLAGS( ClientCapabilities::CapabilitiesFlags )
Q_DECLARE_OPERATORS_FOR_FLAGS( ClientCapabilities::CapabilitiesExtendedFlags )

}; // namespace KMess



#endif
