/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Adam Goossens <adam@kmess.org>                            *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file mpopendpoint.h
 */

#ifndef MPOPENDPOINT_H
#define MPOPENDPOINT_H

#include <QUuid>
#include <QString>

namespace KMess
{

  class MsnContact;

/**
 * This class represents a contact end point with Multiple-Points-of-Presence (MPOP).
 *
 * It is made up of an MsnContact, a Machine GUID (which uniquely identifies the endpoint)
 * and an end point name (provided by the client).
 *
 */
class MPOPEndpoint
{
  public:
    MPOPEndpoint( MsnContact *contact, const QUuid &guid, const QString &name );
    
    MsnContact *contact() const;
    QUuid       guid() const;
    QString     name() const;
    
    void        setContact( MsnContact *contact );
    void        setGuid( const QUuid &guid );
    void        setName( const QString &name );

    /**
     * Compare another MPOPEndpoint instance to this one. 
     * They are equal if they have the same guid.
     *
     * @param other Other MPOPEndpoint instance.
     */
    bool operator==( const MPOPEndpoint &other ) const
    {
      // same guid? same endpoint.
      return other.guid() == guid();
    }

    /**
     * Copy the data from one MPOPEndpoint instance to this one.
     * @param other The MPOPEndpoint instance to copy from.
     */
    MPOPEndpoint &operator=( const MPOPEndpoint &other )
    {
      contact_ = other.contact_;
      guid_ = other.guid_;
      name_ = other.name_;
      
      return *this;
    }

  private:
    MsnContact *contact_;
    QUuid       guid_;
    QString     name_;
};

};

#endif
