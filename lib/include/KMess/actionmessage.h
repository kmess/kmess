/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Valerio Pilo <valerio@kmess.org>                          *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file actionmessage.h
 * @brief Message with a plugin action
 */

#ifndef KMESS_ACTIONMESSAGE_H
#define KMESS_ACTIONMESSAGE_H

#include "message.h"

#include <KMess/NetworkGlobals>

#include <QDebug>
#include <QTime>



namespace KMess {



  /**
   * @class ActionMessage
   *
   * A plugin action message.
   */
  class ActionMessage : public Message
  {

    public: // Public constructors/destructors
            ActionMessage();
            ActionMessage( MsnChat *chat, MsnContact* peer );
            ActionMessage( const Message &other );
    virtual ~ActionMessage();

    public: // Public methods
      // TODO
  };



}; // namespace KMess



#endif // KMESS_ACTIONMESSAGE_H
