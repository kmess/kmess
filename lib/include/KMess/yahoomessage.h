/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Adam Goossens <adam@kmess.org>                            *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file yahoomessage.h
 * @brief Message containing Yahoo! data
 */

#ifndef KMESS_YAHOOMESSAGE_H
#define KMESS_YAHOOMESSAGE_H

#include "message.h"

#include <KMess/NetworkGlobals>

#include <QDebug>
#include <QTime>



namespace KMess {


  /**
   */
  class YahooMessage : public Message
  {

    public: // Public constructors/destructors
                             YahooMessage();
                             YahooMessage( MsnChat *chat, MsnContact* peer );
                             YahooMessage( const Message &other );
      virtual               ~YahooMessage();

    public: // Public methods
      QString destHandle() const;
      Message innerMessage() const;
      void  setInnerMessage( Message message );
      void  setSourceHandle( const QString &handle );
      void  setDestHandle( const QString &handle );
      QString sourceHandle() const;
  };



}; // namespace KMess



#endif // KMESS_YAHOOMESSAGE_H
