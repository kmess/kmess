/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Mike K. Bennett <mkb137b@hotmail.com>                     *
 * (C) Copyright Sjors Gielen <sjors@kmess.org>                            *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file msnsession.h
 * @brief Network library interface
 */

#ifndef MSNSESSION_H
#define MSNSESSION_H

#include <KMess/NetworkGlobals>
#include <KMess/Message>

#include <QStringList>
#include <QVariant>


class QAbstractItemModel;
class QAuthenticator;
class QNetworkProxy;


// Forward declarations
namespace KMessInternal
{
  class AddressBookService;
  class MsnNotificationConnection;
  class ChatFactory;
  class ListsManager;
  class PtpGlue;
  class SwitchboardManager;
  class MsnSwitchboardConnection;
};


namespace KMess
{
class Message;
class MsnChat;
class MsnContact;
class MsnObject;
class MPOPEndpoint;
class ContactList;
class Self;

/**
 * @brief Data class for session information.
 *
 * In KMess itself, this class is created at start and is used to log in and
 * out of Live Messenger. It was once called CurrentAccount, when it was a copy
 * of the Account that was currently logged in, but we have split that class in
 * two and all account data should now be in Account, while all volatile
 * session data should be in MsnSession.
 *
 * This class stores session-related data while the user is connected. This
 * includes the contact list state. All that data is discarded when the account
 * disconnects.
 *
 * @author Mike K. Bennett
 * @author Valerio Pilo
 * @author Sjors Gielen
 * @ingroup Root
 * @todo update all CurrentAccount/MsnSession documentation
 */
class MsnSession : public QObject
{
  friend class Debugger;
  friend class MsnChat;
  Q_OBJECT

  public:
    // The constructor
                             MsnSession();
    // The destructor
    virtual                 ~MsnSession();
    // Return the ContactList object
    ContactList             *contactList() const;
    // Generate a token to log in to Hotmail
    const QString            createHotmailToken( const QString &folder ) const;
    // Retrieve a session setting's value.
    QVariant                 sessionSetting( const QString &name ) const;
    // Return whether we're connected to the service
    bool                     isLoggedIn() const;
    // Start connecting to the server
    void                     logIn( const QString &handle, const QString &password, const MsnStatus status = OnlineStatus );
    // Log out locally from the service
    void                     logOut() const;
    // Log out from the service, including all other endpoints.
    void                     logOutEverywhere() const;
    // Log out from a specific endpoint
    void                     logOutFrom( MPOPEndpoint *endpoint );
    // Request a URL for composing email to that contact
    void                     requestComposeUrl( const QString &toAddress );
    // Set the cache file for contact list data
    void                     setCacheDir( const QString &path );
    // Alter the quantity of unread email
    void                     setEmailCount( int change );
    // Change a single session setting.
    void                     setSessionSetting( const QString &name, const QVariant &value );
    // Change a list of session settings.
    void                     setSessionSettings( const SettingsList &changes );
    // Return the user's representation as an MsnContact
    Self*                    self() const;

    // These advanced functions are not recommended for use, but can be
    // used by applications to extend MsnSession's functionality.
    // They may result in disconnection, weird bugs, or more serious
    // problems. Use at your own risk, but feel free to report bugs if
    // anything unexpected happens or if you're missing functionality requiring
    // you to use these methods.
    bool                     isNotificationConnected() const;
    void                     sendRawCommand( QString command, QString argument, QByteArray payload = QByteArray() ) const;
    void                     sendRawMimeMessage( int ackType, const QString &message ) const;
    KMessInternal::PtpGlue  *ptpGlue() const;

  public:  // Public inline methods
    // Retrieve a setting's value in different forms
    inline bool              sessionSettingBool  ( const QString &name ) const { return sessionSetting( name ).toBool();   };
    inline int               sessionSettingInt   ( const QString &name ) const { return sessionSetting( name ).toInt();    };
    inline QString           sessionSettingString( const QString &name ) const { return sessionSetting( name ).toString(); };

  public slots: // Public slots - Use these to request changes to the server
    // Create a new empty MsnChat object
    KMess::MsnChat          *createMsnChat( const QString &handle );

  private: // Private methods
    // Ensure a setting exists, or crash otherwise
    void                     ensureSessionSettingExists( const char *name ) const;
    // Initialise objects and signals
    void                     init();
    // Return the lists manager. INTERNAL.
    KMessInternal::ListsManager *listsManager() const;
    // Keep the MsnObject up to date with the chosen display picture
    void                     updateMsnObject();

  private slots: // Private slots
    void                     mns_OnLoggedIn();
    void                     mns_OnLoggedOut( KMess::DisconnectReason reason );

  private: // Private attributes
    ContactList                 *contactList_;
    // The library MsnChat factory
    KMessInternal::ChatFactory  *chatFactory_;
    // The connection to the msn notification server
    KMessInternal::MsnNotificationConnection *msnNotificationConnection_;
    // MsnObject representing our display picture if we have one
    MsnObject               *msnObject_;
    // The name-to-value hash of session settings
    SettingsList             sessionSettings_;
    // The glue between data switchboards and the transport library
    KMessInternal::PtpGlue  *ptpGlue_;
    // The class that holds ownership of switchboards, and requests MsnChat for them
    KMessInternal::SwitchboardManager *switchboardManager_;

  signals: // Public signals
    /**
     * Your MSN session was ended. The \arg reason argument provides an explanation as to why the session ended.
     * 
     * Note this signal is not emitted for failed connection attempts - for notification of that, see
     * connectionFailed().
     */
    void                     loggedOut( KMess::DisconnectReason reason );
    // The session has started
    void                     loggedIn();
    // Authentication has failed
    void                     connectionFailed( KMess::StatusMessage reason );
    // Signal that the number of emails has changed
    void                     changedNoEmails();
    // Signal that the user's status has changed
    void                     changedStatus();
    // Signal that a contact added you
    void                     contactAddedUser( KMess::MsnContact *contact );
    // Signal that a new email has been received
    void                     newEmail(QString sender, QString subject, bool inInbox, QString command, QString folder, QString url);
    // Ask the user to authenticate on a proxy
    void                     proxyAuthenticationRequired( const QNetworkProxy &proxy, QAuthenticator *authenticator );
    /**
     * @brief Something happened.
     *
     * This signal is emitted from all around the library, when something
     * happened, good or bad.
     * @param type The type of signal
     * @param message A further specification of what happened.
     * @param extraInfo Some extra information, depends on type or message.
     */
    void                     statusEvent( const KMess::StatusMessageType type, const KMess::StatusMessage message, const QVariant &extraInfo = QVariant() ) const;
    // A new MsnChat object has been created
    void                     chatCreated( KMess::MsnChat *chat );
    // A MsnChat object has been destroyed
    void                     chatDestroyed( KMess::MsnChat *chat );
    // An active MsnChat has received a message
    void                     chatMessageReceived( KMess::Message message );
    // We're connecting to the Live service
    void                     connecting();
    // Contact has gone online
    void                     contactOnline( KMess::MsnContact *contact, bool initial = false );
    // Contact has gone offline
    void                     contactOffline( KMess::MsnContact *contact );
    // Contact has changed status
    void                     contactChangedStatus( KMess::MsnContact *contact, KMess::MsnStatus newStatus );
    //! A Hotmail COMPOSE url was received for an address
    void                     gotComposeUrl( QString recipient, QString url );
};



} // namespace KMess



#endif
