/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Valerio Pilo <valerio@kmess.org>                          *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file inkmessage.h
 * @brief Message with an handwritten drawing
 */

#ifndef KMESS_INKMESSAGE_H
#define KMESS_INKMESSAGE_H

#include "message.h"

#include <KMess/NetworkGlobals>

#include <QDebug>
#include <QTime>



namespace KMess {



  /**
   * @class InkMessage
   *
   * An handwritten message.
   *
   * It can be a GIF image (with embedded ISF data), or an ISF drawing.
   */
  class InkMessage : public Message
  {

    public: // Public constructors/destructors
                             InkMessage();
                             InkMessage( MsnChat *chat, MsnContact* peer, InkFormat format = FORMAT_ISF );
                             InkMessage( const Message &other );
      virtual               ~InkMessage();

    public: // Public methods
      InkFormat              format() const;
      void                   setFormat( InkFormat );
      const QByteArray      &data() const;
      void                   setData( const QByteArray& );
  };



}; // namespace KMess



#endif // KMESS_INKMESSAGE_H
