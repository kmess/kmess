/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Gregg Edghill <gregg.edghill at gmail.com>                *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file ptpdatamessage.h
 * @brief Message containing data for a p2p application
 */

#ifndef KMESS_PTPDATAMESSAGE_H
#define KMESS_PTPDATAMESSAGE_H

#include "message.h"

#include <KMess/NetworkGlobals>

#include <QDebug>
#include <QTime>



namespace KMess {



  /**
  * @class PtpDataMessage
  *
  * An application data message.
  *
  */
  class PtpDataMessage : public Message
  {

    public: // Public constructors/destructors
                             PtpDataMessage();
                             PtpDataMessage( MsnChat *chat, MsnContact* peer, MessageDirection direction );
                             PtpDataMessage( const Message &other );
      virtual               ~PtpDataMessage();

      const QString          destination() const;
      QByteArray             rawData() const;
      const QString          source() const;
      void                   setRawData(const QByteArray& rawdatabytearray);
      void                   setDestination(const QString& destination);
      void                   setSource(const QString& source);
  };



}; // namespace KMess



#endif // KMESS_PTPDATAMESSAGE_H
