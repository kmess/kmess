/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Valerio Pilo <valerio@kmess.org>                          *
 * (C) Copyright Sjors Gielen <sjors@kmess.org>                            *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file msnchat.h
 * @brief Chat session interface
 */

#ifndef MSNCHAT_H
#define MSNCHAT_H

#include <KMess/NetworkGlobals>
#include <KMess/Message>

#include <QStringList>
#include <QFont>
#include <QColor>

// Forward declarations
class MsnObject;
// TODO remove:
class KMessTest;

namespace KMessInternal
{
  class MsnSwitchboardConnection;
  class OfflineImService;
  class ChatFactory;
  class MsnNotificationConnection;
  class MsnSwitchboardConnection;
  struct SwitchInfo;
}


namespace KMess {

// Forward declarations
class TextMessage;
class InkMessage;
class WinkMessage;
class MsnContact;
class MsnSession;



/**
 * @brief Conversation session management class.
 *
 * This class is the interface used for chatting.
 *
 * @author Valerio Pilo
 * @author Sjors Gielen
 * @ingroup Root
 */
class MsnChat : public QObject
{
  Q_OBJECT

  friend class ::KMessTest;
  friend class Debugger;
  friend class KMessInternal::ChatFactory;
  friend class KMessInternal::MsnNotificationConnection;

  public:

    bool                            canInviteContacts() const;
    ContactsList                    participants() const;
    ContactsList                    typingContacts() const;
    bool                            isContactInChat( const MsnContact *contact ) const;
    bool                            isContactInChat( const QString& handle ) const;
    bool                            isEmpty() const;
    bool                            isPrivateChatWith( const QString& handle ) const;
    bool                            isConnected() const;
    bool                            isWaiting() const;
    bool                            isBusy() const;
    MsnContact                     *firstContact() const;
    MsnContact                     *lastContact() const;

  public slots:
    void                            startConnecting();
    void                            inviteContact( const KMess::MsnContact *contact );
    void                            inviteContact( const QString& handle );

    // Send various messages:
    void                            sendMessage( const QString &message, KMess::TextMessage *messageOut = 0L, QFont font = QFont(), QColor color = Qt::black );
    void                            sendMessage( KMess::Message& );
    void                            sendInk( KMess::InkMessage& );
    void                            sendInk( KMess::InkFormat format, QByteArray &inkData );
    void                            sendNudge();
    void                            sendTypingMessage();
    void                            sendWink( KMess::WinkMessage& );
    void                            switchboard_OnDataSendFailed(const quint32);
    void                            switchboard_OnDataSent(const quint32);
    void                            destroy();

  private slots: // Private slots
    void                            slotSwitchboardClosed();
    void                            slotContactJoinedChat( const QString& handle );
    void                            slotContactLeftChat( const QString& handle, bool isChatIdle );
    void                            slotInviteInitialContacts();
    void                            handleFailedInvite();
    void                            slotSwitchboardDeleted( KMessInternal::MsnSwitchboardConnection *connection );
    void                            slotSwitchboardReady();
    void                            gotMessage( KMess::Message );

    void                            sbbridge_OnSignalDataToSend(const QByteArray& datachunk, qint32 *cookie);

  private: // Private methods
                                    MsnChat( MsnSession *session, const QString &handle );
                                    MsnChat( MsnSession *session, KMessInternal::SwitchInfo *info );
    virtual                        ~MsnChat();
    void                            createSwitchboardConnection();
    void                            sendClientCaps();
    void                            sendPendingMessages();

    void                            initiateSession( const QString &server, const quint16 &port, const QString &authorization );
    void                            invitedToChat( const QString &server, const quint16 port, const QString &authorization, const QString &chatId );

  private: // Private attributes
    KMessInternal::MsnSwitchboardConnection *switchboard_;
    QStringList                     participants_;
    QStringList                     pendingInvitations_;
    QString                         firstContact_;
    QString                         lastContact_;
    QList<Message>                  pendingMessages_;
    MsnSession                     *session_;
    KMessInternal::OfflineImService *offlineImService_;

  signals:
    /// A message could not be sent
    void                            sendingFailed( KMess::Message message, const QString& recipient );
    /// A message was sent
    void                            sendingSucceeded( KMess::Message message );
    /// A contact joined the conversation
    void                            contactJoined( KMess::MsnContact* contact );
    /// A contact left the conversation
    void                            contactLeft( KMess::MsnContact* contact, bool isChatIdle );
    /// A contact is now typing
    void                            contactTyping( KMess::MsnContact* contact );
    /// Something bad happened, signal a warning
    void                            chatWarning( KMess::ChatWarningType, KMess::MsnContact* contact );
    /// The connection to the MSN chat server was lost
    void                            disconnected();
    /// A chat server connection was established
    void                            switchboardCreated();
    /// Ask the Notification connection to initiate a new chat server connection with the contact(s)
    void                            requestSwitchboard( KMess::MsnChat *chat );
    /// A message was received from a remote end
    void                            messageReceived( KMess::Message message );

    // TODO remove:
    void                            showWink( const QString&, const QString&, const QString& );
};



} // namespace KMess



#endif
