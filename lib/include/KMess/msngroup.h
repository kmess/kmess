/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Mike K. Bennett <mkb137b@hotmail.com>                     *
 * (C) Copyright Valerio Pilo <valerio@kmess.org>                          *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file msngroup.h
 * @brief Represents a group of contacts, either user-made group or system-made logical assembly of contacts
 */

#ifndef MSNGROUP_H
#define MSNGROUP_H

#include <QObject>
#include <QString>

#include <KMess/NetworkGlobals>

// Forward declarations
namespace KMessInternal
{
  class ListsManager;
};


namespace KMess
{

/**
 * @brief Data class for group information.
 *
 * This class is only used to store group information.
 * the information is changed by the MsnNotificationConnection
 * and user interface classes. The user interface classes
 * respond to the signals sent by this class to update their view.
 * In the contact list view, this class is represented by the GroupListViewItem class.
 *
 * @author Mike K. Bennett
 * @author Valerio Pilo
 */
class MsnGroup : public QObject
{
  Q_OBJECT
  
  friend class ContactListModel; // For special groups
  friend class KMessInternal::ListsManager;

  public: // Public methods
    // A list of the contacts in this group
    QList<MsnContact*>   contacts() const;
    // Return the group ID
    const QString&       id() const;
    // Return the group name
    const QString&       name() const;
    // Return true if the group is empty, false otherwise.
    bool                 isEmpty() const;
    // Return true if this is a special group
    bool                 isSpecialGroup() const;
    // Change the group name
    void                 setName( const QString& newName );

    // cast to a QString for printing.
    operator QString() const;

  protected: // Private constructor/destructor
    // The constructor
                         MsnGroup( const QString &id, bool isSpecialGroup = false, const QString &name = QString() );
    // The destructor
    virtual             ~MsnGroup();

  private: // Private attributes
    // The group id
    QString              id_;
    // True if the group is a special group
    bool                 isSpecialGroup_;
    // The group name
    QString              name_;
    // The contacts in this group
    QList<MsnContact*>   contacts_;
};



}; // namespace KMess

// handy debugging operators.
QDebug operator<<( QDebug dbg, const KMess::MsnGroup *group );
QDebug operator<<( QDebug dbg, const GroupsList &list );
QDebug operator<<( QDebug dbg, const IndexedGroupsList &list );

#endif
