/***************************************************************************
                 self.h - A Contact representation of the user
                             -------------------
    begin                : Sun 25 Jul 10
    copyright            : (C) 2010 by Adam Goossens
    email                : fontknocker@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SELF_H
#define SELF_H

#include <KMess/MsnContact>

namespace KMessInternal
{
  class ListsManager;
  class MsnNotificationConnection;
  class AddressBookService;
  class RoamingService;
};

namespace KMess
{
class MPOPEndpoint;


/**
 * @brief An MsnContact representation of ourselves and our account.
 * 
 * This class provides you some additional methods to configure things like your
 * MPOP logon mode, privacy settings and test if your passport is verified.
 * 
 * Only one instance of this class is created and persists for the lifetime of your
 * connection to MSN.
 */
class Self : public MsnContact
{
  Q_OBJECT

  // Make friends with the classes which need to change the contact details
  friend class KMessInternal::ListsManager;
  friend class KMessInternal::MsnNotificationConnection;
  friend class MsnSession;
  friend class KMessInternal::AddressBookService;
  friend class KMessInternal::RoamingService;

  public:
    ~Self();
    
    void           changeMedia( MediaData media );
    void           changeStatus( MsnStatus status );
    MPOPEndpoint  *localEndpoint() const;
    LogonMode      logonMode() const;
    MessagePrivacy messagePrivacy() const;
    NotifyPrivacy  notifyPrivacy() const;
    bool           passportVerified() const;
    bool           roamingEnabled() const;
    QString        roamingDisplayPictureUrl() const;

    void           setDisplayName( const QString &name );
    void           setDisplayPicture( const QString &path );
    void           setPersonalMessage( const QString &message );
    void           setRoaming( bool enabled );
    void           setNotifyPrivacy( NotifyPrivacy mode );
    void           setMessagePrivacy( MessagePrivacy mode );
    void           setLogonMode( LogonMode mode );

  signals:
    //! Emitted when the display picture kept on the roaming server has changed
    void           roamingDisplayPictureChanged( QString newUrl );

  private:
    Self();

  private:
    void          _setNsConnection( KMessInternal::MsnNotificationConnection *ns );
    void          _setPassportVerified( bool verified );
    void          _setRoamingDisplayPictureUrl( const QString &url );
    void          updateMsnObject();

  private: // vars
    KMessInternal::MsnNotificationConnection *ns_;
    // The local MPOP endpoint
    MPOPEndpoint *localEndpoint_;
    // MsnObject representing our display picture if we have one
    MsnObject    *msnObject_;
    bool          passportVerified_;
    QString       displayPicturePath_;
    QString       roamingDPUrl_;
};


} // namespace KMess

// debugging operators that come in handy.
QDebug operator<<( QDebug dbg, const KMess::MsnContact *contact );
QDebug operator<<( QDebug dbg, const ContactsList &list );
QDebug operator<<( QDebug dbg, const IndexedContactsList &list );

#endif
