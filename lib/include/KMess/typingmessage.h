/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Valerio Pilo <valerio@kmess.org>                          *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file typingmessage.h
 * @brief An "user is typing" message
 */

#ifndef KMESS_TYPINGMESSAGE_H
#define KMESS_TYPINGMESSAGE_H

#include "message.h"

#include <KMess/NetworkGlobals>

#include <QDebug>
#include <QTime>



namespace KMess {

  
  class TypingMessagePrivate;


  /**
   * @class TypingMessage
   *
   * A message which notifies that someone (user or contact) is currently typing.
   *
   * Typing messages have an emission time, so that their validity can be monitored:
   * the official client expires the message after 15 seconds.
   */
  class TypingMessage : public Message
  {
    public: // Public constructors/destructors
                             TypingMessage();
                             TypingMessage( MsnChat *chat, MsnContact* peer );
                             TypingMessage( const Message &other );
      virtual               ~TypingMessage();

    public: // Public methods
      bool                   isExpired() const;
      QTime&                 time();
      MsnContact*            contact() const;

    protected:
      virtual void           prepare();
      
    public: // Public static values
      /// Expiration time, in seconds
      static const quint8    expirationTime = 15;

    private: // Private properties
      TypingMessagePrivate  *d;
  };



}; // namespace KMess



#endif // KMESS_TYPINGMESSAGE_H
