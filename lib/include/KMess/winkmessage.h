/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Valerio Pilo <valerio@kmess.org>                          *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file winkmessage.h
 * @brief Message with a wink
 */

#ifndef KMESS_WINKMESSAGE_H
#define KMESS_WINKMESSAGE_H

#include "message.h"

#include <KMess/NetworkGlobals>

#include <QDebug>
#include <QTime>



namespace KMess {

  class WinkMessagePrivate;


  /**
   * @class WinkMessage
   *
   * A wink.
   */
  class WinkMessage : public Message
  {

    public: // Public constructors/destructors
                             WinkMessage();
                             WinkMessage( MsnChat *chat, MsnContact* peer );
                             WinkMessage( const Message &other );
      virtual               ~WinkMessage();

    public: // Public methods
      void                   setWinkObjectString( const QString& );
      const QString&         winkObjectString() const;

    private: // Private properties
      WinkMessagePrivate    *d;
  };



}; // namespace KMess



#endif // KMESS_INKMESSAGE_H
