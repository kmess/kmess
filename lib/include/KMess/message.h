/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Valerio Pilo <valerio@kmess.org>                          *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file message.h
 * @brief The most basic kind of message
 */

/*!\file message.h
 \brief The header file for the Message class, which is the base class of all Messages.
 */

#ifndef KMESS_MESSAGE_H
#define KMESS_MESSAGE_H

#include <KMess/NetworkGlobals>

#include <QDebug>
#include <QString>
#include <QDate>
#include <QTime>
#include <QDateTime>
#include <QSharedData>
#include <QSharedDataPointer>

namespace KMess {

 /**
  * This macro will bring into scope a d variable which is of type ClassMessagePrivate.
  *
  * \code
  * K_D(TypingMessage);
  * d->foo(); // d is of type TypingMessagePrivate *
  * \endcode
  *
  * If this is done in a non-const method, copy-on-write may be performed.
  */
  #define K_D(Class) Class##Private *d = (Class##Private *)(d_ptr.data())

  // Forward declarations
  class MsnChat;
  class MsnContact;

  /**
  * @brief Represents the Private data for all Message instances.
  *
  * All Message subclasses that require private data should create their
  * own class that inherits from MessagePrivate. This will give you access
  * to copy-on-write and reference counting thanks to Qt's QSharedData and
  * QSharedDataPointer classes.
  *
  * For an example implementation, see TextMessage.
  */
  class MessagePrivate : public QSharedData
  {
    public:
      // The constructor
                    MessagePrivate();
                    MessagePrivate( const MessagePrivate &other );
      // The destructor
      virtual        ~MessagePrivate();
      /// Sets the field in the message, or adds it if it doesn't exist.
      void           setField( const QString& field, const QString& value );
      // Return the message body as a string
      QByteArray     getBody() const;
      // Return the message fields as a big string
      QByteArray     getFields() const;
      // Return the entire message as a big data array
      QByteArray     getMessage() const;
      // Get a sub-value of a value that has multiple parameters
      QString        getSubValue(const QString& field, const QString& subField = QString()) const;
      // Get a value given a field
      QString        getValue(const QString& field) const;
      // Test whether a given field exists in the message header
      bool           hasField(const QString& field) const;
      // Parse the message into type, body, and fields and values
      void           parseMessage( const QByteArray& message, bool &ok );
      // Set the body of the message.
      void           setBody( const QString& body );
      // Split the message header and store it in the string list
      void           splitHead( QStringList& stringList, const QString& head ) const;
      // Split a line between field and value
      void           splitLine( QString& field, QString& value, const QString& line ) const;
      // Split a message into head and body
      void           splitMessage( QByteArray& head, QByteArray& body, const QByteArray& message) const;
      // Clone this instance.
      virtual MessagePrivate *clone();

    public: // Public attributes
      /// The message body
      QByteArray           body_;
      /// Block of headers
      FieldMap             header_;
      /// Owner of this message
      MsnChat*             chat_;
      /// Delivery reporting mode
      ConfirmationMode     confirmationMode_;
      /// Whether this message is arriving or departing
      MessageDirection     direction_;
      /// Whether this message contains useable data
      bool                 isValid_;
      /// Type of Message subclass
      MessageType          messageType_;
      /// Contact with which the message is being exchanged
      MsnContact*          peer_;
      /// The moment when the message was sent or received
      QDateTime            dateTime_;
  };

  /**
   * @class Message
   *
   * @brief The base class for all messages received and sent from the library.
   *
   * The most basic kind of message.
   *
   * Messages are transferred between MSN clients through any of the various
   * bridges. This class is the simplest representation of a message, and is
   * only used as a base.
   *
   * All specialized message types inherit from this: you should not create a
   * Message instance yourself. Create an instance of the appropriate subclasses
   * instead.
   *
   * An example of the copy-on-write and reference counting follows:
   *
   * \code
   * TextMessage message;
   * message.setMessage( "Hello world!" );
   * message.setFont( QFont("Sans Serif") );
   *
   * TextMessage message2 = message;
   * message2 == message; // true. both vars reference the same shared data.
   *
   * message2.setColor( Qt::blue );
   *
   * message2 == message; // false. when executing setColor a copy-on-write was performed,
   *                      // giving message2 its own copy of the shared data.
   *
   * // when message and message2 go out of scope, providing they aren't
   * // copied anywhere, their private data will automatically be deleted since their
   * // reference counts will be zero.
   * \endcode
   */
  class Message
  {
    public: // Public constructors/destructors
                           Message();
                           Message( const Message &other );
      virtual             ~Message();

    public: // Public methods
      QByteArray           body() const;
      MsnChat*             chat() const;
      ConfirmationMode     confirmationMode() const;
      QByteArray           contents();
      QString              field( const QString& name ) const;
      QString              field( const QString& name, const QString& subFieldName ) const;
      const FieldMap&      fields() const;
      bool                 hasField( const QString& field ) const;
      QByteArray           header() const;
      bool                 isIncoming() const;
      bool                 isValid() const;
      MsnContact*          peer() const;
      const QDateTime&     dateTime() const;
      QDate                date() const;
      QTime                time() const;
      MessageType          type() const;
      void                 setConfirmationMode( const ConfirmationMode mode = ConfirmAlways );
      void                 setDateTime( const QDateTime dateTime );
      void                 setField( const QString& field, const QString& value );
      void                 setPeer( MsnContact *peer );
      void                 setChat( MsnChat *chat );
      void                 setDirection( const MessageDirection direction );

    public: // public operators
      /**
       * Two Message instances are equal if they reference the same data.
       */
      bool operator==( const Message &other ) const
      {
        return other.d_ptr == d_ptr;
      }


    public: // Public static methods
      static Message       fromData( MsnContact* peer, MessageDirection direction, const QByteArray &data );

    protected: // Protected constructors/destructors
                           Message( MsnContact* peer, MessageDirection direction, MsnChat *chat = 0, ConfirmationMode mode = ConfirmAlways, MessageType type = InvalidMessageType, QString contentType = QString(), MessagePrivate *privData = 0L );

    protected: // Protected methods
      void                 appendToHeader( const QByteArray& header );
      void                 appendToBody( const QByteArray& body );
      void                 parseMimeMessage( const QByteArray& message );
      void                 setContentType( const QString contentType );
      virtual void         prepare();

    protected: // Private properties
      /**
       * The QSharedDataPointer object that manages the MessagePrivate instance.
       *
       * QSharedDataPointer reference counts the private data, deleting it when it is
       * no longer referenced by any other classes. Also QSharedDataPointer will perform
       * copy-on-write when the private data is changed and the reference count is greater than 1.
       *
       * To retrieve a pointer to the private data, use the K_D macro. This macro will
       * handle the casting and declaration of a local variable for you.
       */
      QSharedDataPointer<MessagePrivate>      d_ptr;
  };



}; // namespace KMess



#endif // KMESS_MESSAGE_H
