/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Valerio Pilo <valerio@kmess.org>                          *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file contactlistmodelitem.h
 */

#ifndef CONTACTLISTMODELITEM_H
#define CONTACTLISTMODELITEM_H

#include <QString>
#include <QVariant>


// Forward declarations
namespace KMess { class ContactListModelItem; };

// Custom operator to display nodes and items in the debug output
QDebug operator<<( QDebug s, const KMess::ContactListModelItem *item );



namespace KMess
{

// Forward declarations
class ContactListModelItem;
class MsnContact;
class MsnGroup;

// Custom types
typedef QMap<QString, QVariant> ModelDataList;
typedef QList<ContactListModelItem*> ModelItemList;



/**
 * This class represent a Contact List node or leaf in the Model/View paradigm.
 *
 * The Model is a way to universally represent data which could be reused later by different views:
 * for example, the contact list, or a list of contacts available to be invited in a chat. The data is
 * organized in form of a tree, with a root element containing the user's groups, and the contacts within
 * the groups.
 * The Model Item is an element of the tree. It can assume the role of a root element, of a node (a group) or
 * of a contact (a leaf).
 * The data output by the model items can be easily expanded for future uses.
 *
 * @author Valerio Pilo <valerio@kmess.org>
 */
class ContactListModelItem
{
  public:  // Public enumerations
    enum ItemType
    {
      ItemRoot     = -1      // This element should be unique, it contains no object
    , ItemGroup    =  1      // Groups have this item type
    , ItemContact  =  2      // Contacts have this type
    };
    enum DataRoles
    {
      DataRole     = Qt::DisplayRole  // Role used to retrieve the actual object data
    , SearchRole   = Qt::UserRole + 1 // Role used to obtain searching data
    };


  public:  // Public methods
    // Contact constructor, creates a root item
                                  ContactListModelItem();
    // Contact constructor, creates a Contact item
    explicit                      ContactListModelItem( const MsnContact *contact, ContactListModelItem *parent = 0 );
    // Group constructor, creates a Group item
    explicit                      ContactListModelItem( const MsnGroup *group, ContactListModelItem *parent = 0 );
    // Destructor
                                 ~ContactListModelItem();

    // Return a child item identified by its row
    ContactListModelItem         *child( int row );
    // Return a child contact
    ContactListModelItem         *child( const MsnContact *contact );
    // Return a child group
    ContactListModelItem         *child( const MsnGroup *group );
    // Return all children groups
    ModelItemList                 children();
    // Return all children contact
    ModelItemList                 children( const MsnContact *contact );
    // Return the number of children of this item
    int                           childCount() const;
    // Return the number of data columns
    int                           columnCount() const;
    // Return a collection of data from this item
    const QVariant                data( int role ) const;
    // Return the type of the internal stored object
    ItemType                      type() const;
    // Move this item to another parent
    void                          moveTo( ContactListModelItem *newParent );
    // Return the stored object
    const void                   *object() const;
    // Get the parent node of this item
    ContactListModelItem         *parent() const;
    // Get this item's position within the parent node
    int                           row() const;

  private:  // Private methods
    // Add a new child item
    void                          appendChild( ContactListModelItem *child );
    // Remove a child item
    int                           removeChild( ContactListModelItem *child );

  private:  // Private properties
    // The internal list of the item's children
    ModelItemList                 childItems_;
    // Pointer to the parent node
    ContactListModelItem         *parentNode_;
    // Pointer to this item's data
    const void                   *object_;
    // Type of the contained data
    ItemType                      itemType_;
};



}; // namespace KMess



#endif
