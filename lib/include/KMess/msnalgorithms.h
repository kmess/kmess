/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Antonio Nastasi <sifcenter@gmail.com>                     *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file msnalgorithms.h
 */


#ifndef MSNALGORITHMS_H
#define MSNALGORITHMS_H

#include <QtCore/QObject>

class QByteArray;


namespace KMess 
{
  class MsnAlgorithms
  {
    public:
      // Create the authentication blob to sent by USR command
      static int                  createAuthBlob( QByteArray &result,
                                                  const QByteArray &nonce64,
                                                  const QByteArray &token );
      // Create a HMACSha1 hash
      static const QByteArray     createHMACSha1( const QByteArray &keyForHash,
                                                  const QByteArray &secret );
      // Compute the string for live hotmail access
      static const QString        createHotmailToken(
                                                  const QString &passportToken,
                                                  const QString &proofToken,
                                                  const QString &folder );
      // Return a derived key with HMACSha1 algorithm
      static const QByteArray     deriveKey( const QByteArray &keyToDerive,
                                             const QByteArray &magic );
      // Convert an html format (#RRGGBB) color to an msn format (BBGGRR) color
      static const QString        convertHtmlColorToMsnColor(
                                                  const QString &color );
      // Convert and msn format color (BBGGRR) to an html format (#RRGGBB) color
      static const QString        convertMsnColorToHtmlColor(
                                                  const QString& color);
      // Insert bytes into a QByteArray
      static void       insertBytes( QByteArray &buffer,
                                     const unsigned int value,
                                     const int offset );
      static void       insertShortBytes( QByteArray &buffer,
                                          const unsigned short value,
                                          const int offset );
      static void       insertNonce( QByteArray &buffer, const QString &nonce,
                                     const int offset = 32 );
      static void       insertUtf16String( QByteArray &buffer,
                                           const QString &value, int offset );
      // Extract bytes from a QByteArray
      static unsigned int extractBytes( const QByteArray &buffer,
                                        const int offset );
      static quint32      extractLongBytes( const QByteArray &buffer,
                                            const int offset );
      static QString      extractUtf16String( const QByteArray &buffer,
                                              const int offset,
                                              const int size );

   private: // Private methods
      // Create triple des encryption
      static QByteArray  createTripleDes( const QByteArray key,
                                          const QByteArray secret,
                                          const QByteArray& iv );
    private: // Private structs
     struct MSNPMSNG {
      quint32 headerSize;
      quint32 cryptMode;
      quint32 cipherType;
      quint32 hashType;
      quint32 ivLength;
      quint32 hashLength;
      quint32 cipherLength;

      unsigned char ivBytes[8];
      unsigned char hashBytes[20];
      unsigned char cipherBytes[72];
    };
  };
}

#endif // MSNALGORITHMS_H
