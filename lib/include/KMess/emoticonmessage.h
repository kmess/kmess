/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Valerio Pilo <valerio@kmess.org>                          *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file emoticonmessage.h
 * @brief A message containing emoticon definitions for a chat message
 */

#ifndef KMESS_EMOTICONMESSAGE_H
#define KMESS_EMOTICONMESSAGE_H

#include "message.h"

#include <KMess/NetworkGlobals>

#include <QDebug>
#include <QTime>



namespace KMess {



  /**
   * @class EmoticonMessage
   *
   * A message which contains a mapping between shortcuts and emoticons used in a
   * chat message.
   *
   * An EmoticonMessage is usually received just before a TextMessage, and will contain
   * a list of emoticons which will be present in the upcoming message.
   * The map contains MsnObjects of emoticons, mapped by the text shortcut
   * present in the message.
   */
  class EmoticonMessage : public Message
  {
    public: // Public constructors/destructors
                             EmoticonMessage();
                             EmoticonMessage( MsnChat *chat, MsnContact* peer );
                             EmoticonMessage( const Message &other );
      virtual               ~EmoticonMessage();

    public: // Public methods
      void                   addEmoticon( const QString &shortcut, const QString msnObject );
      const MsnObjectMap&    emoticons() const;
      void                   setEmoticons( const MsnObjectMap& emoticons );

    public: // Public static values
      /// Maximum size of an emoticon's text shortcut, in characters
      static const quint8    shortcutTruncateLength = 7;
  };



}; // namespace KMess



#endif // KMESS_EMOTICONMESSAGE_H
