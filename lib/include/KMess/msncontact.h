/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Valerio Pilo <valerio@kmess.org>                          *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file msncontact.h
 * @brief A contact's representation
 */

#ifndef MSNCONTACT_H
#define MSNCONTACT_H

#include <KMess/ClientCapabilities>
#include <KMess/MsnObject>
#include <KMess/NetworkGlobals>

#include <QStringList>
#include <QUuid>

namespace KMessInternal
{
  class ListsManager;
  class MsnNotificationConnection;
  class AddressBookService;
};

namespace KMess
{
// Forward declarations
class MPOPEndpoint;
class ContactProfile;

/**
 * Contains data pertaining to the current media a contact is 
 * listening to or playing.
 */
struct MediaData
{
  MediaType   type;
  QString     format;
  QStringList formatArgs;
  
  /**
   * Compares two media data structs for equality.
   */
  bool operator ==( const MediaData &other )
  {
    return ( other.type == type && other.format == format && other.formatArgs == formatArgs );
  }
  
  /**
   * Compares two media data structs for inequality.
   */
  bool operator !=( const MediaData &other )
  {
    return ! ( *this == other );
  }

  /**
   * Uses the format string and arguments to create a usable media string
   * in the format the user intended.
   */
  QString toString()
  {
    QString mediaStr = format;
    for( int i = 0; i < formatArgs.count(); i++ )
    {
      mediaStr = mediaStr.replace( '{' + QString::number(i) + '}', formatArgs[i] );
    }
    
    return mediaStr;
  }
  
  /**
   * Casts this MediaData to a QString by calling toString()
   */
  operator QString()
  {
    return toString();
  }
};

/**
 * @brief The library's representation of an MSN Contact.
 *
 * A contact is anyone who is present in one or more of your lists: contacts can be friends,
 * blocked people, people who have added you, bots, favorites, etc.
 *
 * Contacts are identified by a network ID and an email address. The network
 * id + email pair is called a contact ID.
 *
 * @author Valerio Pilo
 * @ingroup Root
 */
class MsnContact : public QObject
{
  Q_OBJECT

  // Make friends with the classes which need to change the contact details
  friend class KMessInternal::ListsManager;
  friend class KMessInternal::MsnNotificationConnection;
  friend class MsnSession;
  friend class KMessInternal::AddressBookService;

  public: // Public methods
    const ClientCapabilities capabilities() const;
    QHash<QUuid,MPOPEndpoint*> endpoints() const;
    const QString&     handle() const;
    const QString&     displayName() const;
    QList<MsnGroup*>   groups() const;
    const QString&     guid() const;
    bool               hasCapability( ClientCapabilities::CapabilitiesFlags capability ) const;
    const QString&     id() const;
    NetworkType        network() const;
    MediaData          media() const;
    MsnMemberships     memberships() const;
    const MsnObject&   msnObject() const;
    const QString&     personalMessage() const;
    ContactProfile    *profile() const;
    MsnStatus          status() const;
    bool               supportsInkType( InkFormat ) const;
    bool               supportsP2P() const;

  public: // Operator overloads.
    operator QString() const;

  public: // Public inline methods
    /**
    * Return whether the contact has the user in his/her contact list.
    * @return bool
    */
    inline bool        hasUser() const { return ( memberships_ & MSN_LIST_REVERSE ); }
    /**
    * Return whether the contact was allowed to see the user's presence.
    * @return bool
    */
    inline bool        isAllowed() const { return ( memberships_ & MSN_LIST_ALLOWED ); }
    /**
    * Return whether the contact was blocked.
    * @return bool
    */
    inline bool        isBlocked() const { return ( memberships_ & MSN_LIST_BLOCKED ); }
    /**
    * Return whether the contact is a friend.
    * @return bool
    */
    inline bool        isFriend() const { return ( memberships_ & MSN_LIST_FRIEND ); }
    /**
    * Return whether the contact has not accepted our friendship request.
    * @return bool
    */
    inline bool        isPending() const { return ( memberships_ & MSN_LIST_PENDING ); }
    /**
    * Return whether the contact is currently offline.
    * @return bool
    */
    bool               isOffline() const { return ( status_ == OfflineStatus ); }
    /**
    * Return whether the contact is currently online.
    * @return bool
    */
    bool               isOnline() const { return ( status_ != OfflineStatus ); }
    /**
     * Return whether this contact is "unknown", i.e., they do not exist on your contact list nor in the 
     * Allowed, Blocked or Pending lists of your address book.
     *
     * This occurs in two cases. Firstly, the contact is a contact in your Windows Live address book whom you have
     * not added to (or previously removed from) your Messenger contact list, or, secondly, they may be an 
     * invited user, in a chat, whom you have never seen before.
     * 
     * If you add this user to your contact list their list of memberships will change and the
     * user will no longer be unknown.
     *
     * @return bool
     */
    bool               isUnknown() const { return ( ( memberships_ & ( MSN_LIST_ALLOWED | MSN_LIST_BLOCKED | MSN_LIST_PENDING | MSN_LIST_FRIEND ) ) == 0 ); }

  signals:
    // Signal that the contact's endpoints have changed.
    void                 endpointsChanged();
    // Signal that the display name of this contact has changed
    void                 displayNameChanged();
    // Signal that the contact may have moved to a different group
    void                 groupsChanged();
    // Signal that the contact has changed memberships
    void                 membershipsChanged();
    // Signal that the contact display picture has changed
    void                 displayPictureChanged();
    // Signal that the contact's msnobject has changed
    void                 msnObjectChanged();
    // Signal that the contact has changed status (could be online, offline, away, busy, etc).
    void                 statusChanged();
    // Signal that the contact may have changed it's personal message
    void                 personalMessageChanged();
    // Signal if the contact goes offline
    void                 contactOffline();
    // Signal if the contact goes online
    void                 contactOnline();
    // Signal that the contact's profile has changed
    void                 profileChanged();
    // Signal that the contact's current media has changed
    void                 mediaChanged();

  protected:
                       MsnContact( const QString& );
    virtual           ~MsnContact();

  private: // Private constructors/destructors
                       MsnContact( const QString&, const QString&, int,
                                   const QList<MsnGroup*>, const QString& );



  protected: // Protected methods
    void               setCapabilities( ClientCapabilities );
    void               setEndpoints( QList<MPOPEndpoint *> );
    void               _setDisplayName( const QString& );
    void               setGroups( QList<MsnGroup*> groups );
    void               setGuid( const QString& );
    void               setId( const QString& );
    void               setMemberships( const MsnMemberships );
    void               setMsnObject( const MsnObject& );
    void               setNetwork( NetworkType network );
    void               setStatus( MsnStatus );
    void               _setPersonalMessage( const QString& );
    void               setMedia( MediaData data );
    void               setProfile( ContactProfile *profile );

  private:
    /// The capabilities of this contact
    ClientCapabilities capabilities_;
    /// The known MPOP endpoints for this contact (provided by their UBX info)
    QHash<QUuid, MPOPEndpoint*> endpoints_;
    /// Email address of the contact
    QString        email_;
    /// The current friendly name, automatically updated by the library
    QString        displayName_;
    /// The IDs of the user groups where this contact is present
    QList<MsnGroup*> groups_;
    /// The contact's GUID, if any; used by the server only
    QString        guid_;
    /// Full ID string, stored for convenience
    QString        id_;
    /// The current media of the contact
    MediaData      media_;
    /// Membership lists of the contact
    MsnMemberships memberships_;
    /// The MsnObject of this contact
    MsnObject      msnObject_;
    /// Network where the contact is registered
    NetworkType    network_;
    /// The personal status (PSM) message
    QString        personalMessage_;
    /// The contact's profile
    ContactProfile *profile_;
    /// Current status of the contact
    MsnStatus      status_;
};


} // namespace KMess

// debugging operators that come in handy.
QDebug operator<<( QDebug dbg, const KMess::MsnContact *contact );
QDebug operator<<( QDebug dbg, const ContactsList &list );
QDebug operator<<( QDebug dbg, const IndexedContactsList &list );

#endif
