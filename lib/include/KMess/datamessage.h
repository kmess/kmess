/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Valerio Pilo <valerio@kmess.org>                          *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file datamessage.h
 * @brief A message with binary data
 */

#ifndef KMESS_DATAMESSAGE_H
#define KMESS_DATAMESSAGE_H

#include "message.h"

#include <KMess/NetworkGlobals>



namespace KMess {



  /**
   * @class DataMessage
   *
   * A message containing binary data.
   */
  class DataMessage : public Message
  {
    public: // Public constructors/destructors
                             DataMessage();
                             DataMessage( MsnChat *chat, MsnContact* peer );
      virtual               ~DataMessage();

    public: // Public methods

    private: // Private properties
  };



}; // namespace KMess



#endif // KMESS_DATAMESSAGE_H
