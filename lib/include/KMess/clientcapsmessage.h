/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Valerio Pilo <valerio@kmess.org>                          *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file clientcapsmessage.h
 * @brief Message containing extra data about a client
 */

#ifndef KMESS_CLIENTCAPSMESSAGE_H
#define KMESS_CLIENTCAPSMESSAGE_H

#include "message.h"

#include <KMess/NetworkGlobals>

#include <QDebug>
#include <QTime>



namespace KMess {


  /**
   * @class ClientCapsMessage
   *
   * A message containing info about a contact's third-party client.
   *
   * This kind of message is not sent and ignored when received by Messenger.
   *
   * Only third-party clients exchange these between them.
   */
  class ClientCapsMessage : public Message
  {
    public: // Public constructors/destructors
                             ClientCapsMessage();
                             ClientCapsMessage( MsnChat *chat, MsnContact* peer );
                             ClientCapsMessage( const Message &other );
      virtual               ~ClientCapsMessage();

    public: // Public methods
      ChatLoggingMode        chatLoggingMode() const;
      const QString&         clientName() const;
      const QString&         clientVersion() const;
      void                   setChatLoggingMode( const ChatLoggingMode mode );
      void                   setClientName( const QString& clientName );
      void                   setClientVersion( const QString& clientVersion );
      
    protected:
      virtual void           prepare();
  };



}; // namespace KMess



#endif // KMESS_CLIENTCAPSMESSAGE_H
