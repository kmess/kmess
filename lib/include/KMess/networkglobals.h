/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Valerio Pilo <valerio@kmess.org>                          *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file networkglobals.h
 * @brief Globally used definitions
 */

#ifndef NETWORKGLOBALS_H
#define NETWORKGLOBALS_H

#include <QVariant>



/**
 * Network Library globally useful datatypes and structures.
 *
 * In this include file are listed all the data types and structures which
 * will be used by the publicly accessible classes of the library.
 */


/**
 * Typedefs
 */
#if ! defined( KMESS_DATA_TYPES )
#define KMESS_DATA_TYPES

  // Forward declarations
  namespace KMess { class MsnContact; class MsnGroup; };

  // Used in contacts management
  typedef QList<KMess::MsnContact*>                     ContactsList;
  typedef QListIterator<KMess::MsnContact*>             ContactsListIterator;
  typedef QHash<QString, KMess::MsnContact*>            IndexedContactsList;
  typedef QHashIterator<QString, KMess::MsnContact*>    IndexedContactsListIterator;
  
  // Used in groups management
  typedef QList        <KMess::MsnGroup*>         GroupsList;
  typedef QListIterator<KMess::MsnGroup*>         GroupsListIterator;
  typedef QHash        <QString,KMess::MsnGroup*> IndexedGroupsList;
  typedef QHashIterator<QString,KMess::MsnGroup*> IndexedGroupsListIterator;
  // Used in emoticon handling
  typedef QMap        <QString,QString> MsnObjectMap;
  typedef QMapIterator<QString,QString> MsnObjectMapIterator;

  // Used by Message and its subclasses
  typedef QMap        <QString,QString> FieldMap;
  typedef QMapIterator<QString,QString> FieldMapIterator;

  // Easy usage of setting groups
  typedef QHash        <QString,QVariant> SettingsList;
  typedef QHashIterator<QString,QVariant> SettingsListIterator;

#endif



namespace KMess {



/**
 * Known types of network supported by MSN
 */
enum NetworkType
{
  NetworkUnknown = -1
, NetworkMsn   =  1
, NetworkYahoo = 32
};



/**
 * @brief Membership lists
 */
enum MsnMembership
{
  MSN_LIST_NONE    = 0, /// The contact is a standard Windows Live contact in our AB and is on no list
  MSN_LIST_FRIEND  = 1, /// The contact is visible in the contact list (was added to FL list)
  MSN_LIST_ALLOWED = 2, /// The contact is only allowed                (was added to AL list)
  MSN_LIST_BLOCKED = 4, /// The contact is blocked                     (was added to BL list)
  MSN_LIST_REVERSE = 8, /// The contact has you on his/hers list       (added by server to RL list)
  MSN_LIST_PENDING = 16 /// The contact is on a pending list, should be added to AL/FL or BL
};
Q_DECLARE_FLAGS( MsnMemberships, MsnMembership )
Q_DECLARE_OPERATORS_FOR_FLAGS( MsnMemberships )



/**
 * Types of status messages which can be output.
 */
enum StatusMessageType
{
  SessionMessage      /// Session messages representing general session events like connection, disconnection, ping receipt, etc.
, ProgressMessage     /// A connection event was issued, like "Authentication complete" or "Downloading contact list"
, WarningMessage      /// Something went wrong, but nothing serious.
, ProtocolMessage     /// An error was reported in MSNP. This is considered an internal error.
, ErrorMessage        /// Usually a protocol-level error.
, FatalMessage        /// A significant error happened.
};



/**
 * Types of emittable chat-related warnings
 *
 * FIXME Remove UNSUPPORTED_* warnings, they're going to be handled by the application
 * FIXME Move the other warnings into the StatusMessage enum.
 */
enum ChatWarningType
{
  WARNING_UNSUPPORTED_VOICECLIP      /// A voice clip has been received
, WARNING_UNSUPPORTED_ACTIONMESSAGE  /// An action message has been received
, WARNING_UNSUPPORTED_UNKNOWN        /// An unknown message type has been received
, WARNING_INK_UNSUPPORTED_BY_CONTACT /// One or more contacts cannot receive the Ink message
, WARNING_CONNECTION_DROP            /// The connection has been dropped
, WARNING_TOO_MANY_EMOTICONS         /// There were too many emoticons in the last message
};



/**
 * Status Messages.
 *
 * These enum values represent all the possible messages which the library can output.
 * The KMess::MsnSession::statusEvent() signal is the only place to connect to if you want to parse
 * messages, warnings and errors incoming from the library.
 */
enum StatusMessage
{
/// Session messages
  SessionPingReceived = 0              /// A 'ping' was sent to the server and the answer was received
, SessionConnected                     /// The online presence session has started
, SessionDisconnected                  /// The online presence session has ended

/// Progress messages
, ProgressConnecting                  /// The connection process was started
, ProgressAuthenticating              /// The connection was established, now starting to authenticate
, ProgressFetchingAddressBook         /// The user was authenticated, now starting to download the address book and memberships
, ProgressGoingOnline                 /// The address book was downloaded, now preparing to go online

/// Warnings
, WarningPingLost                     /// A 'ping' was sent to the server but no answer was received
, WarningUnknownCommand               /// Received an unknown command
, ErrorOfflineIMServiceUnavailable    /// The Offline Messaging SOAP server is not available now
, ErrorAddressBookServiceUnavailable  /// The Live Service SOAP server is not available now

/// Generic, socket and connection errors
, ErrorUnknown                          /// An unspecified error has occurred
, ErrorConnectingToSocket               /// There was an error while establishing the connection
, ErrorConnectingToGateway              /// There was an error while establishing the HTTP connection
, ErrorConnectionDropped                /// The connection was dropped
, ErrorSocketDataInvalid                     /// An error within the data was received
, ErrorConnectionTimedOut                    /// The connection was not successful within the time limit
, ErrorConnectedFromElsewhere                /// The current account has connected from elsewhere, disconnecting us
, ErrorInvalidUserCredentials                /// The authentication/login failed due to wrong username or password
, ErrorComputeHashFailed                     /// The hash could not be computed

/// Error messages about server issues
, ErrorInternal                        /// Internal KMess error (we did something the server didn't like)
, ErrorSoapInternal                    /// SOAP error: internal error - we did something the SOAP server didn't like
, ErrorSoapResponseInvalid             /// SOAP error: the webservice response was not valid
, ErrorSoapRequestTimedOut             /// SOAP error: the webservice request was not successful within the time limit
, ErrorSoapAuthenticationRequired      /// SOAP error: the webservice had required an authentication (usually shouldn't)
, ErrorSoapTooManyRedirects            /// SOAP error: the webservice has redirected us too many times
, ErrorServiceUnavailable              /// The LiveMessenger service is unavailable

/**
 * Error messages reported by the server.
 *
 * The errors are taken from these two sources:
 * http://msnp-sharp.googlecode.com/svn/trunk/MSNP-Sharp15/MSNPSharp/enums.cs
 * http://msnpiki.msnfanatic.com/index.php/Reference:Error_List
 */



// syntax error in a command
, ErrorSyntax = 200
// invalid parameter in a command
, ErrorInvalidCommandParameter = 201
// invalid contact network
, ErrorInvalidContactNetwork = 204
// invalid user (tried to add a non-Passport account)
, ErrorInvalidUser = 205
// missing domain name
, ErrorInvalidUserDomain = 206
// the user is already logged in
, ErrorAlreadyLoggedIn = 207
// the specified username is invalid: the SB sends it in response to CAL, the NS in response to ADL
, ErrorInvalidUsername = 208
// the full username specified is invalid, or your Passport account has not been confirmed yet
, ErrorInvalidFullUsername = 209
// user's contact list is full
, ErrorContactListFull = 2010
// invalid name request (invalid parameter for the SBP command)
, ErrorInvalidNameRequest = 213
// user is already present there
, ErrorUserAlreadyInChat = 215
// user is not present on the list
, ErrorUserNotOnList = 216
// user is not online
, ErrorUserNotOnline = 217
// already in stated mode
, ErrorUserAlreadyInMode = 218
// user is in an opposite (conflicting) list (ie. contacts can't be both in the Allow and Block lists)
, ErrorUserInOppositeList = 219
// Your contact list has too many groups; you are allowed to only have at most 30
, ErrorTooManyGroups = 223
// invalid group: you cannot make changes to this group
, ErrorInvalidGroup = 224
// the contact is not in this group
, ErrorContactNotInGroup = 225
// The group is not empty (i.e. the client tried to remove it anyways)
, ErrorGroupNotEmpty = 227
// The group name is already present in your contact list
, ErrorGroupAlreadyExists = 228
// group name too long. it cannot be longer than 61 bytes
, ErrorGroupNameTooLong = 229
// if the <d/> domain element was specified in the <ml/> list, at least one <c/> contact must be present
, ErrorEmptyDomainElement = 240
// the adl/rml commands accept fl(1)/al(2)/bl(4) but not rl(8)/pl(16)
, ErrorInvalidMembershipList = 241
// switchboard server request failed
, ErrorSwitchboardRequestFailed = 280
// transfer to switchboard server failed
, ErrorSwitchboardTransferFailed = 281
// p2p _error: MSNSLP/Direct connection error, connection failed
, ErrorP2PError = 282
// required field is missing
, ErrorMissingRequiredField = 300
// user is not logged in
, ErrorNotLoggedIn = 302
// error accessing contact list (402)
, ErrorContactListInaccessible = 402
// denied access to the service due to invalid account permissions e.g. trying the beta service without an allowed beta account
, ErrorInvalidAccountPermissions = 420
// internal server error
, ErrorInternalServerError = 500
// database server error
, ErrorDatabaseServerError = 501
// that command is disabled
, ErrorCommandDisabled = 502
// ups failure (509)
, ErrorUPSFailure = 509
// file operation failed
, ErrorFileOperationFailed = 510
// the account was banned
, ErrorAccountBanned = 511
// memory allocation failure
, ErrorMemoryAllocationFailed = 520
// response to a server challenge failed
, ErrorChallengeResponseFailed = 540
// server is busy (600)
, ErrorServerIsBusy = 600
// server is unavailable, seen in response to USR
, ErrorServerIsUnavailable = 601
// name server is down
, ErrorNameServerDown = 602
// database connection failed
, ErrorDatabaseConnectionFailed = 603
// server is going down
, ErrorServerGoingDown = 604
// The server is going down soon
, WarningServerGoingDownSoon = 605
// connection creation failed
, ErrorCouldNotCreateConnection = 700
// bad parameters sent for a CVR or URL command
, ErrorBadParameters = 710
// write is blocking
, ErrorWriteIsBlocking = 711
// session is overloaded
, ErrorSessionIsOverloaded = 712
// calling too rapidly (too many CAL commands)
, ErrorCallingTooRapidly = 713
// too many open chat sessions
, ErrorTooManyChatSessions = 714
// unexpected command: sent in response to a PRP setting an invalid phone type of three or less characters.
// Also sent in response to a display name change (with PRP) on an unverified Passport account.
, ErrorUnexpectedCommand = 715
// bad friend file name
, ErrorBadFriendFile = 717
// changing name too rapidly; also seen when sending too many MSNP2P packets over the switchboard without waiting for ACKs
, ErrorChangingTooRapidly = 800
// server is too busy
, ErrorServerTooBusy = 910
// A ticket token was incorrect, the authentication failed
, ErrorTicketTokenIncorrect = 911
// action is not allowed when user is offline
, ErrorNotAllowedWhileOffline = 913
// new contacts are not accepted
, ErrorNotAcceptingNewContacts = 920
// kids passport account without parental consent
, ErrorKidsPassportNoParentalConsent = 923
// passport account not yet verified, happens when changing name on recently verified accounts
, ErrorPassportNotVerified = 924
// bad ticket for USR command
, ErrorBadTicket = 928
// account not on this server, seen when using a cached NS address
, ErrorAccountNotOnThisServer = 931


/// Fatal error messages

, FatalNoSupportedProtocols = 9999 /// The server doesn't support *any* of the protocols we can talk
};



/// Supported ink transfer formats
enum InkFormat
{
  FORMAT_GIF    /// The transferred Ink data uses the GIF (Graphics Interchange Format) format
, FORMAT_ISF    /// The transferred Ink data uses the ISF (Ink Serialized Format) format
};



/**
 * Delivery reporting mode for Messages.
 *
 * Each Message has a delivery reporting mode, which determines how the
 * application will react when the message was sent successfully
 * and/or when the message could not be sent.
 *
 * @see the Message class.
 */
enum ConfirmationMode
{
  ConfirmAlways   ///< Report both delivery success and failure.
, ConfirmNever    ///< Do not send delivery reports.
, ConfirmFailure  ///< Only report delivery failure.
, ConfirmPtpData
};



/**
 * Types of messages.
 *
 * @see the Message class.
 */
enum MessageType
{
  InvalidMessageType     ///< Invalid (or unknown type) message.
, ActionMessageType      ///< A plugin action message.
, ClientCapsMessageType  ///< A message containing third-party client info.
, EmoticonMessageType    ///< A message with emoticon definitions for a text message.
, InkMessageType         ///< An hand-written message.
, NudgeMessageType       ///< A nudge.
, OfflineMessageType     ///< An offline message.
, PtpDataMessageType     ///< A peer to peer data message.
, TextMessageType        ///< A normal chat message.
, TypingMessageType      ///< A notification message for the "user is typing..." event.
, VoiceMessageType       ///< A message with a voice recording.
, WinkMessageType        ///< A message with a wink.
, DataMessageType        ///< A data message.
, EmailMessageType       ///< A message containing email notification data.
, YahooMessageType       ///< A message to/from a Yahoo! contact
};



/**
  * Represents the direction of the message - either Incoming (from a peer)
  * or outgoing (sent to a peer)
  */
enum MessageDirection
{
  DirectionIncoming,
  DirectionOutgoing
};



/**
 * The available MSN Statuses
 * TODO: rewrite code in such a way that MSNSTATUS_NUMBER is not necessary anymore
 */
enum MsnStatus
{
    OnlineStatus        = 0  /// User is online
  , BusyStatus               /// User is busy
  , AwayStatus               /// User is away
  , IdleStatus               /// User is idle
  , BrbStatus                /// User is away for a bit
  , PhoneStatus              /// User is on the phone
  , LunchStatus              /// User is eating
  , InvisibleStatus          /// User is not visible to others
  , OfflineStatus            /// User is not connected (keep this as the second last item of the enum)
  , MSNSTATUS_NUMBER         /// Internal, do not use (keep this as the last item of the enum)
};


/**
 * Special contact flags
 */
enum Flags
{
    FlagNone         = 0
  , FlagBlocked
  , FlagWebMessenger
  , FlagMobile
};



/**
 * Special predefined groups.
 */
namespace SpecialGroups
{
  const QString FAVORITES   = "{KMess::Favorites}";
  const QString INDIVIDUALS = "{KMess::Individuals}";
  const QString ONLINE      = "{KMess::Online}";
  const QString OFFLINE     = "{KMess::Offline}";
  const QString ALLOWED     = "{KMess::Allowed}";
  const QString REMOVED     = "{KMess::Removed}";
  const QString UNKNOWN     = "{KMess::Unknown}";
};


/**
 * The possibly logging modes that can be provided by a client
 * as part of their capabilities message.
 */
enum ChatLoggingMode
{
  LoggingModeUnknown      ///< The client did not specify if and how it is logging the chat
, LoggingActive           ///< The client is recording the chat
, LoggingActiveEncrypted  ///< The client is recording the chat and encrypting the log
, LoggingInactive         ///< The client is not recording the chat
};



/**
 * The logon mode when using MPOP. The default is LogonMultiple.
 */
enum LogonMode
{
  LogonMultiple,     /// Do not automatically sign out from all other endpoints on login.
  LogonSingle        /// Automatically sign out from all other endpoints on login.
};



/**
 * A disconnect can be caused by:
 * <ol>
 * <li>Calling MsnSession::logOut()</li>
 * <li>Signing in from another location (with MPOP disabled), or forcing this endpoint to logout, and</li>
 * <li>An unexpected connection loss</li>
 * </ol>
 * This enum gives you a way to determine which of the above scenarios caused a disconnect.
 */
enum DisconnectReason
{
  UserRequestedDisconnect,  /// The user chose to disconnect via MsnSession::logOut().
  UnexpectedDisconnect,     /// The disconnection was unexpected (due to error or connection loss).
  OtherLocationDisconnect,  /// The user has signed on at another location that does not support MPOP.
};


/**
 * Various types of media that a user can be using.
 */
enum MediaType
{
  MediaMusic,     /// The user is listening to music.
  MediaGaming,    /// The user is playing a game.
  MediaOffice,     /// The user is providing an out-of-office message.
  MediaNone       /// The user is not currently using any media.
};



/**
 * Specifies the ways that MSN will handle new contacts.
 */
enum NotifyPrivacy
{
  NotifyUnknown,      /// The value is unknown
  PromptOnAdd,        /// Ask the user what to do when added
  AutomaticAdd        /// Automatically add the user to your contact list
};


/**
 * Specifies what users are permitted to send you messages.
 */
enum MessagePrivacy
{
  PrivacyUnknown,     /// The value is unknown
  AllExceptBlocked,   /// Any user, except those on the Block list, can send you messages
  AllowedOnly         /// Only users on the Allowed list can send you messages
};



/**
 * Specifies how far the removal of a contact should go.
 * 
 * The default is "RemoveFromMessenger", which removes the contact from your
 * Messenger contact list but leaves it in your Windows Live address book.
 * 
 * If you specify RemoveFromWindowsLive, the contact is removed from both
 * Messenger *and* your Windows Live address book. The contact will be unavailable
 * from all Windows Live-based applications, including Hotmail.
 */
enum RemoveContactOption
{
  RemoveFromMessenger,
  RemoveFromWindowsLive
};

/**
 * Specifies additional parameters when removing a contact.
 * 
 * If RemoveAndBlock is used a contact will be blocked before being removed.
 */
enum RemoveContactAction
{
  RemoveAndBlock,
  RemoveOnly
};

}; // end of namespace KMess

#endif // # ifndef NETWORKGLOBALS_H
