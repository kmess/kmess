/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Gregg Edghill <greg.edghill@gmail.com>                    *
 * (C) Copyright The Kopete developers <kopete-devel@kde.org>              *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file msnchallengehandler.h
 * @brief Computes a msn challenge response hash key.
 */

#ifndef MSNCHALLENGEHANDLER_H
#define MSNCHALLENGEHANDLER_H

#include <QString>
#include <QVector>



namespace KMess
{


/**
 * Provides a simple way to compute a msn challenge response hash key.
 *
 * Portions taken from:
 * http://msnpiki.msnfanatic.com/index.php/MSNP11:Challenges
 *
 * @author Gregg Edghill
 * @ingroup NetworkCore
 */
class MSNChallengeHandler
{
  public:
                    MSNChallengeHandler();
                    ~MSNChallengeHandler();

    /**
    * Computes the response hash string for the specified challenge string.
    */
    const QString   computeHash( const QString& challengeString );

    /**
    * Returns the product id used by the challenge handler.
    */
    const QString&  getProductId() const;

  private: // Private method

    /**
    * Creates a 64-bit hash key.
    */
    qint64          createHashKey( const QVector<qint32>& md5Integers, const QVector<qint32>& challengeIntegers );

    /**
    * Swaps the bytes in a hex string.
    */
    const QString   hexSwap( const QString& in );

  private: // Private attributes

    QString         productKey_;
    QString         productId_;
};

}; // namespace KMess



#endif
